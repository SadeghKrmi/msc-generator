### What is Msc-generator?
Msc-generator is a program that parses textual chart descriptions and produces graphical output in a variety of file formats, or as an object, which can be embedded in documents, such as Word or PowerPoint. It currently supports three kinds of charts: 
1. Message Sequence Charts (MSCs, this is where the name of the tool comes from);
2. generic graphs in the DOT language of graphviz; and
3. experimental Block Diagrams.

Msc-generator aims to provide a simple text language that is clear to create, edit and understand, and which can be transformed into images. It is a potential alternative to mouse-based editing tools, such as Microsoft Visio. 

- Since version 2.2 Msc-generator also contains a Windows GUI implementing
  signalling charts as OLE embedded objects. This allows inclusion of both
  chart source and the rendered graphics together in a Word or Powerpoint document.
- In version 3.0, a new drawing engine was built, which can handle not only boxes
  but bezier curves.
- Since version 5.0 Msc-generator supports graphs via graphviz, using the exact
  DOT language with some addition, such as shadows, double lines and chart designs.
- At version 6.0 a block diagram language is added. It allows you to formally
  specify block alignment and containment.
- Version 7.0 sports a Linux and MacOS GUI that can be started using
  `msc-gen --gui`. This GUI also works on Windows, but does not allow
  embedding charts into Word or PowerPoint documents. It is possible, however,
  to embed the chart text into a PNG output file.
- Version 8.0 introduced a new way of embedding charts into Office documents
  while allowing editing them later.

A full on-line help and language reference can be found at [https://msc-generator.gitlab.io/msc-generator/](https://msc-generator.gitlab.io/msc-generator/).

### Chart types
The signalling chart part is heavily extended and completely rewritten version of the 0.08 version of Michael C McTernan’s mscgen with a number of enhancements. The command-line syntax is compatible to that of mscgen, so any tool  integrated with mscgen can also be used with Msc-generator. Since version 4.5 Msc-generator also contains an mscgen compatibility mode, which aims to interpret mscgen chart descriptions in a fully backwards compatible manner.
 
The graph part uses the graphviz library to lay out graphs. It uses the DOT language to describe charts, with a few extensions, such as the ability to collapse subgraphs or add shadows.
 
Block Diagrams use a language similar in logic to the above two. The language aims to capture the relation among blocks to lay them out in relation to one another.

### Download and install
Msc-generator builds on lex, yacc, graphviz, glpk, cairo, miniz, SDL2 and ImGui. On Windows, there is an MFC GUI that enables you to insert charts into Word or PowerPoint documents and edit them with a double-click. A Linux/Mac and Windows port is maintained for both a command-line utility and a GUI (so on Windows you have 2 GUIs). 
- Windows binaries can be downloaded from the Releases page at [GitLab](https://gitlab.com/msc-generator/msc-generator/-/packages).
- A Debian Linux package is maintained by Gábor Németh that can be installed via  `sudo apt install msc-generator`.
The `msc-generator-nox` package excludes a GUI (and all its dependencies) and there is also `msc-generator-doc`.
For Ubuntu releases up to the year 2021 unmaintained packages are available at this [Launchpad PPA](https://launchpad.net/~homar/+archive/ubuntu/ppa).
- A MacOS homebrew package is maintained by Péter Mátray, install via
`brew install msc-generator`

