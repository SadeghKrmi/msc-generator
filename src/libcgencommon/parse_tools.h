/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file parse_tools.h Utilities for parsing.
 * @ingroup libcgencommon_files
 * This file is used if we do parsing for language and not for Color Syntax Highlight.
 * Need to include this for the right YYLTYPE */

#ifndef PARSE_TOOLS_H
#define PARSE_TOOLS_H

#include "error.h"

namespace gsl
{
//
// owner - also defined in cgen_context.h!!
//
// owner<T> is designed as a bridge for code that must deal directly with owning pointers for some reason
//
// T must be a pointer type
// - disallow construction from any type other than pointer type
//
template <class T, class = std::enable_if_t<std::is_pointer<T>::value>>
using owner = T;

}


#ifdef C_S_H_IS_COMPILED
#error Do not include parse_tools.h for CSH compilation
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
/** The yacc location type (for normal, non-csh parsing).
 * Note that this must be a POD type as it will be part of a union.*/
struct YYLTYPE
{
    int first_line;
    int first_column;
    int last_line;
    int last_column;
};
#define YYLTYPE_IS_DECLARED 1
#define YYLTYPE_IS_TRIVIAL 1
#endif

void language_jump_line(YYLTYPE *loc);
void language_process_block_comments(const char *s, YYLTYPE *lex_loc);
char *remove_head_tail_whitespace(char *s);
std::string cppprocess_colon_string(const char *s, FileLineCol loc, YYLTYPE *lex_loc, bool two_colons = false);
inline gsl::owner<char*> process_colon_string(const char *s, FileLineCol loc, YYLTYPE *lex_loc, bool two_colons = false)
{ return strdup(cppprocess_colon_string(s, loc, lex_loc, two_colons).c_str()); }
char* process_multiline_qstring(const char *s, FileLineCol loc, YYLTYPE *lex_loc);


#endif //PARSE_TOOLS_H
