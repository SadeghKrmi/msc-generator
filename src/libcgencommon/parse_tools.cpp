/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file parse_tools.cpp Helpers for parsers.
 * @ingroup libcgencommon_files  */

#define _CRT_NONSTDC_NO_WARNINGS
#include <cstring>
#include <string>
#include "utf8utils.h"
#include "stringparse.h"
#include "parse_tools.h"

/** Move `loc` to the beginning of the next line*/
void language_jump_line(YYLTYPE *loc)
{
    loc->last_line = loc->first_line+1;
    loc->last_column=1; //yacc column numbering starts at 1
};

/** Parse block comment and adjust line numbers in 'lex_loc'. 
 * Standalone '\ n' and '\ r' count as newline, but combined
 * '\ n\ r' counts as a single one.*/
void language_process_block_comments(const char *s, YYLTYPE *lex_loc)
{
    lex_loc->last_column = lex_loc->first_column;
    lex_loc->last_line = lex_loc->first_line;
    while (*s) {
        if (s[0]==0x0d && s[1]==0x0a)
            s += 2;
        else if (s[0]==0x0d || s[0]==0x0a)
            s++;
        else {
            s++;  
            continue;
        }
        lex_loc->last_column = 1; //yacc column numbering starts at 1
        lex_loc->last_line++;
    }
};



/** In-place removal of whitespace.
 * Remove heading and trailing whitespace, by
 * returning a new head of string and adding a '0' to the end of it.*/
char *remove_head_tail_whitespace(char *s)
{
    size_t a=0;
    char *r;
    while (s[a]==' ' || s[a]=='\t') a++;
    r = s+a;
    a=strlen(r);
    while (a>0 && (r[a-1]==' ' || r[a-1]=='\t')) a--;
    r[a] = '\0';
    return r;
}

/** Preprocess a colon-initiated label.
 * If 'two_colons' is true, the label starts with two colons (graph language)
 * We do all the following:
 * - Remove heading and trailing whitespace.
 * - Replace any internal CR or CRLF (and surrounding whitespaces) to "\n".
 * - Remove comments between # and lineend, except if # is preceeded by an odd
 *   number of backslashes.
 * - Update `lex_loc` to point to the end of the token
 * - Insert \0x2(file,line,col,reason;...) escapes where needed we changed the length of the
 *   preceeding string, so that if we generate an error to any escapes thereafter
 *   those will have the right location in the input file. To cater for multiple inclusions
 *   (e.g., procedure replays) we include a FileLineCol, which may be more than one level.
 * All the while take potential utf-8 characters into account: we count characters, not bytes in location.
 *
 * The function copies the result to new memory and the caller shall free().*/
std::string cppprocess_colon_string(const char *s, FileLineCol loc, YYLTYPE *lex_loc, bool two_colons)
{
    std::string ret;
    const size_t original_line = loc.line;
    int old_pos = two_colons ? 2 : 1; //actually s begins with the colon(s), we skip that
    loc.col += old_pos;

    while (1) {
        //the current line begins at old_pos
        int end_line   = old_pos;
        int start_line = old_pos;
        //find the end of the line
        while (s[end_line]!=0 && s[end_line]!=10 && s[end_line]!=13)
            end_line++;
        //store the ending char to later see how to proceed
        char ending = s[end_line];
        //skip over the heading whitespace at the beginning of the line
        while (s[start_line]==' ' || s[start_line]=='\t')
            start_line++;
        //find the last non-whitespace in the line
        int term_line = end_line-1;
        //term_line can be smaller than start_line here if line is empty
        while (term_line>=start_line && (s[term_line]==' ' || s[term_line]=='\t'))
            term_line--;
        //Generate a \l(file,line,col) escape and append
        //We have only stepped over whitespace, no UTF-8 characters: we can just add "start_line-old_pos"
        ret += FileLineCol(loc, loc.line, loc.col + (start_line-old_pos)).Print();
        //now append the line (without the whitespaces)
        //but remove anything after a potential #
        bool wasComment = false;
        bool emptyLine = true;
        while (start_line<=term_line) {
            //count number of consecutive \s
            unsigned num_of_backsp = 0;
            while(start_line<=term_line && s[start_line] != '#') {
                if (s[start_line] == '\\') num_of_backsp++;
                else num_of_backsp = 0;
                ret += s[start_line++];
                emptyLine = false;
            }
            //if we hit a # leave rest of line only if not preceeded by even number of \s
            if (s[start_line] == '#') {
                if (num_of_backsp%2) {
                    ret += s[start_line++]; //step over escaped #
                    emptyLine = false;
                } else {
                    wasComment = true;
                    break;
                }
            }
        }
        //We may have copied spaces before the comment, we skip those
        while (ret.length()>0 && (ret[ret.length()-1]==' ' || ret[ret.length()-1]=='\t'))
            ret.erase(ret.length()-1);
        //if ending was a null we are done with processing all lines
        if (!ending) {
            //consider the utf-8 characters
            //end_line may equal old_pos, if the string is empty
            if (end_line>old_pos)
                loc.col += UTF8len(std::string_view(s+old_pos, end_line - old_pos));
            break;
        }
        //append ESCAPE_CHAR_SOFT_NEWLINE escape for msc-generator,
        //but only if not an empty first line
        //append "\" + ESCAPE_CHAR_SOFT_NEWLINE if line ended with odd number of \s
        if (!emptyLine || original_line != loc.line) {
            //add a space for empty lines, if line did not contain a comment
            if (emptyLine && !wasComment )
                ret += ' ';
            //test for how many \s we have at the end of the line
            int pp = (int)ret.length()-1;
            while (pp>=0 && ret[pp]=='\\') pp--;
            //if odd, we insert an extra '\' to keep lines ending with \s
            if ((ret.length()-pp)%2==0) ret += '\\';
            ret += "\\" ESCAPE_STRING_SOFT_NEWLINE;
        }
        //Check for a two character CRLF, skip over the LF, too
        if (ending == 13 && s[end_line+1] == 10) end_line++;
        old_pos = end_line+1;

        //Now advance loc
        loc.line++;
        loc.col = 1;
    }
    //move lex's position
    lex_loc->last_line = int(loc.line);
    lex_loc->last_column = int(loc.col);
    return ret;
}

/** Preprocess multiline quoted strings.
 * We do all the following:
 * - skip heading and trailing quotation mark and whitespace inside
 * - Replace any internal CR or CRLF (and surrounding whitespaces) to "\n".
 * - Update `lex_loc` to point to the end of the token
 * - Insert \0x2(file,line,col,reason;...) escapes where needed we changed the length of the
 *   preceeding string, so that if we generate an error to any escapes thereafter
 *   those will have the right location in the input file. To cater for multiple inclusions
 *   (e.g., procedure replays) we include a FileLineCol, which may be more than one level.
 * All the while take potential utf-8 characters into account: we count characters, not bytes in location.
 *
 * The function copies the result to new memory and the caller shall free().*/
char* process_multiline_qstring(const char *s, FileLineCol loc, YYLTYPE *lex_loc)
{
    std::string ret;
    const size_t original_line = loc.line;
    int old_pos = 1; //actually s begins with the quotation mark, we skip that
    loc.col += 1;

    while (1) {
        //the current line begins at old_pos
        int end_line = old_pos;
        int start_line = old_pos;
        //find the end of the line
        while (s[end_line]!=0 && s[end_line]!=10 && s[end_line]!=13)
            end_line++;
        //store the ending char to later see how to proceed
        char ending = s[end_line];
        //skip over the heading whitespace at the beginning of the line
        while (s[start_line]==' ' || s[start_line]=='\t')
            start_line++;
        //find the last non-whitespace in the line
        int term_line = end_line-1;
        //if we are at the very end, ignore trailing quotation mark
        if (ending==0) term_line--;
        //term_line can be smaller than start_line here if line is empty
        while (term_line>=start_line && (s[term_line]==' ' || s[term_line]=='\t'))
            term_line--;
        //Generate a \l(file,line,col) escape and append
        //We have only stepped over whitespace, no UTF-8 characters: we can just add "start_line-old_pos"
        ret += FileLineCol(loc, loc.line, loc.col + (start_line-old_pos)).Print();
        //now append the line (without the whitespaces)
        ret.append(s+start_line, s+term_line+1);
        //if ending was a null we are done with processing all lines
        if (!ending) {
            //consider the utf-8 characters
            loc.col += UTF8len(std::string_view(s+old_pos, end_line - old_pos -1));
            break;
        }
        //append ESCAPE_CHAR_SOFT_NEWLINE escape for msc-generator,
        //but only if not an empty first line
        //append "\" + ESCAPE_CHAR_SOFT_NEWLINE if line ended with odd number of \s
        if (start_line<=term_line || original_line != loc.line) {
            //add a space for empty lines, if line did not contain a comment
            if (start_line>term_line)
                ret += ' ';
            //test for how many \s we have at the end of the line
            int pp = (int)ret.length()-1;
            while (pp>=0 && ret[pp]=='\\') pp--;
            //if odd, we insert an extra '\' to keep lines ending with \s
            if ((ret.length()-pp)%2==0) ret += '\\';
            ret += "\\" ESCAPE_STRING_SOFT_NEWLINE;
        }
        //Check for a two character CRLF, skip over the LF, too
        if (ending == 13 && s[end_line+1] == 10) end_line++;
        old_pos = end_line+1;

        //Now advance loc
        loc.line++;
        loc.col = 1;
    }
    //move lex's position
    lex_loc->last_line = int(loc.line);
    lex_loc->last_column = int(loc.col);
    return strdup(ret.c_str());
}



