/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file utf8utils.h Utilities for international characters.
* @ingroup libcgencommon_files  */



#ifndef UTF8UTILS_H
#define UTF8UTILS_H

#include <cstdint>
#include <string>
#include <cstddef>
 
/** Test if the string is a valid utf8 or not.*/
bool IsValidUTF8(std::string_view utf8) noexcept;

/** Converts a series of 2-byte UTF16 characters into an UTF-8 string.
*  If length is not specified we search for a trailing (2-byte-long) zero,
*  else we use the length (in bytes). On odd length, we truncate last
*  character. */
std::string ConvertFromUTF16_to_UTF8(std::string_view utf16);

/** Converts a series of 2-byte UTF16 characters into an UTF-8 string.*/
inline std::string ConvertFromUTF16_to_UTF8(std::wstring_view utf16)
{ return ConvertFromUTF16_to_UTF8(std::string_view(reinterpret_cast<const char*>(utf16.data()), 2*utf16.length())); }

/** Converts a UTF-8 string into a series of UTF-16 characters.
*  If length is not specified we search for a trailing zero,
*  else we use the length (in bytes). On an error we ignore the resulting
*  characters. */
std::wstring ConvertFromUTF8_to_UTF16(std::string_view utf8, bool include_BOM=false);

/** Auto-detect if text is UTF8 or not. If not, convert as if it were UTF16.
* if no conversion is done we do NOT add a trailing zero, in case len is specified.
*  If length is not specified we search for a trailing (2-byte-long) zero,
*  else we use the length (in bytes). On odd length, we truncate last
*  character. The result is returned in 'text'. We allocate new memory for result,
*  and deallocate the received memory - or leave it if we need not convert.
*  Returns true if there was a conversion, false if text was already Utf8.
*  If 'force_as_unicode' is set, we always convert and return true.*/
bool ConvertToUTF8(std::string &text, bool force_as_unicode);

extern const unsigned char numTrailingBytesForUTF8[256];

/** How many bytes follow this byte in legal UTF8 for a single char?*/
inline unsigned char UTF8TrailingBytes(char c) noexcept
{
    return numTrailingBytesForUTF8[(unsigned char)c];
}

/** Returns how many characters an UTF-8 string (of a certain byte-length) contains.
 * Terminating partial characters, (e.g., a 3-byte, when we have only 2 left)
 * are counted as full.*/
inline size_t UTF8len(std::string_view text) noexcept
{
    size_t ret = 0;
    for (auto i=0U; i<text.length(); ret++)
        i += UTF8TrailingBytes(text[i])+1;
    return ret;
}

/** Returns how many characters an UTF-8 string (zero terminated) contains. 
 * Terminating partial characters, (e.g., a 3-byte, when we have only 2 left)
 * are counted as full.*/
inline size_t UTF8len(const char *text) noexcept
{
    if (text==nullptr) return 0;
    const char *s = text;
    while (*s)
        s += UTF8TrailingBytes(*s)+1;
    return s-text;
}

/** Find the first byte of character 'char_index'. In case of a too short string, we return -1.
 * In case the string terminates with a partial character (e.g., a 3-byte, when we have only 2 left),
 * and the requested character is exactly this, we silently return the byte position of its first byte.*/
inline ptrdiff_t GetUTF8ByteIndex(std::string_view utf8, size_t char_index) noexcept
{
    size_t i;
    for (i = 0; i<utf8.length() && char_index>0; char_index--) 
        i += UTF8TrailingBytes(utf8[i])+1;
    return char_index ? -1 /*too short string*/ : ptrdiff_t(i);
}


#endif
