/** @file version.h Containing version macros.
 * @ingroup libmscgen_files */
#pragma once
/** The major version of libmscgen.*/
#define LIBMSCGEN_MAJOR 8
/** The minor version of libmscgen.*/
#define LIBMSCGEN_MINOR 2
/** The micro version of libmscgen.*/
#define LIBMSCGEN_SUPERMINOR 0
