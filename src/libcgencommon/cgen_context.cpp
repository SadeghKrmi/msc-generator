/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file cgen_context.cpp The definition basic context related classes, procedures and inclusion support.
 * @ingroup libcgencommon_files */

#define _CRT_NONSTDC_NO_DEPRECATE //For visual studio to allow strdup
#include <cstring>
#include <cstdlib>                                       
#include <algorithm>
#include "cgen_context.h"

//This cannot be in the header, as strdup() is rejected by MSVC unless 
//_CRT_NONSTDC_NO_DEPRECATE is defined, which we dont want in a header
multi_segment_string::multi_segment_string(const char* a)
    : str(strdup(a)), multi(false), had_param(false), had_error(false) {}

/** Return 1 if the two strings fulfill the 'op' and 0 if not. 
 * Return 2 if any parameter is in error.*/
int multi_segment_string::Compare(ECompareOperator op, const multi_segment_string & o) const
{
    if (had_error || o.had_error) return 2;
    const int res = strcmp(str ? str : "", o.str ? o.str : "");
    switch (op) {
    default: 
    case ECompareOperator::INVALID:          return 2;
    case ECompareOperator::SMALLER:          return res<0;
    case ECompareOperator::SMALLER_OR_EQUAL: return res<=0;
    case ECompareOperator::EQUAL:            return res==0;
    case ECompareOperator::NOT_EQUAL:        return res!=0;
    case ECompareOperator::GREATER_OR_EQUAL: return res>=0;
    case ECompareOperator::GREATER:          return res>0;
    }
}
