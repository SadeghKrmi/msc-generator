/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file language_misc.h Helpers for parsers.
 * This file is included by parsers both for color syntax and language compilation
 * @ingroup libcgencommon_files  */

#if !defined(PARSERHELPER_H)
#define PARSERHELPER_H

#include "utf8utils.h"

#ifndef HAVE_UNISTD_H
#define YY_NO_UNISTD_H
extern int isatty(int);
#endif

#define YY_NO_INPUT 1

#ifndef YYMSC_RESULT_TYPE
#error You must set YYMSC_RESULT_TYPE before you include parserhelper.
#endif

/** One input buffer we scan from*/
struct input_buffer_state
{
    const char *buf = nullptr;         ///<The input file we scan (for the current buffer)                       
    size_t      pos = 0;               ///<The position in the input file (next char to read into the current buffer)
    const char *lex_buff_end = nullptr;///<The place in lex's bowels after which we copied the last batch. This byte corresponds to buff+pos in our input.
    size_t      length = 0;            ///<The length of the input file
    input_buffer_state() noexcept = default;
    input_buffer_state(const char *b, size_t l) noexcept : buf(b), pos(0), lex_buff_end(b), length(l) {}
    const char *GetInputPosFrom_yytext(const char *_yytext) const noexcept { return buf + pos - (lex_buff_end - _yytext); }
};

#ifdef C_S_H_IS_COMPILED

/** Parser and lexer related stuff, the 'extra' for all chart types.
* Derive your own and call it 'xxx_parse_param_csh'.*/
struct base_parse_parm_csh
{
    void              *yyscanner;  ///<The flex scanner
    input_buffer_state buff;       ///<The input sources to scan from
    YYMSC_RESULT_TYPE *RESULT;     ///<The result of the scanning. A Chart or a Csh descendant.
};

#define YY_INPUT(buffer, res, max_size)             \
do {                                                \
    input_buffer_state *pp =                        \
        &YYGET_EXTRA(yyscanner)->buff;              \
    if (pp->pos >= pp->length)                      \
        res = YY_NULL;                              \
        else                                        \
    {                                               \
        res = int(pp->length - pp->pos);            \
        res > max_size ? res = max_size : 0;        \
        memcpy(buffer, pp->buf + pp->pos, res);     \
        pp->pos += res;                             \
    }                                               \
} while (0)

#define YY_USER_ACTION do {                                          \
    yylloc->first_pos = yylloc->last_pos+1;                          \
    /* convert token length from byte-length to character-length */  \
    yylloc->last_pos = yylloc->last_pos +                            \
          unsigned(UTF8len(std::string_view(yytext, yyleng)));       \
    } while(0);

#define YYRHSLOC(Rhs, K) ((Rhs)[K])

#define YYLLOC_DEFAULT(Current, Rhs, N)                \
do                                                     \
  if (N) {                                             \
    (Current).first_pos = YYRHSLOC (Rhs, 1).first_pos; \
    (Current).last_pos  = YYRHSLOC (Rhs, N).last_pos;  \
  } else {                                             \
    (Current).first_pos = (Current).last_pos   =       \
      YYRHSLOC (Rhs, 0).last_pos;                      \
  }                                                    \
while (0)

#else

/** Parser and lexer related stuff, the 'extra' for all chart types.
* Derive your own and call it 'xxx_parse_param'.*/
struct base_parse_parm
{
    /** The state parsing may reach an opening '{' brace.*/
    enum class EScopeOpenMode
    {
        NORMAL,      ///<Nothing special (Used also by procedure definitions. There the fact that we parse a procedure can be captured by bison rules.)
        PROC_REPLAY  ///<A procedure invocation preceeded the brace, which is then from the text of the proc body
    };

    void              *yyscanner;             ///<The flex scanner
    std::vector<input_buffer_state> buffs;    ///<The stack of input sources to scan from. back() is topmost, the current
    YYMSC_RESULT_TYPE *RESULT;                ///<The result of the scanning. A Chart or a Csh descendant.
    FileLineCol        pos_stack;             ///<The stack of positions - only for non-csh parsing.
    ParameterContext   last_procedure_params; ///<Place parameter list of a procedure definition header here to be available to the body
    const Procedure   *last_procedure;        ///<After parsing a procedure header put the looked-up procedure here.
    EScopeOpenMode     open_context_mode;     ///<What was there before a scope opening?
    base_parse_parm() : yyscanner(nullptr), RESULT(nullptr), pos_stack(0, 1, 0, EInclusionReason::TOP_LEVEL), 
        last_procedure(nullptr), open_context_mode(EScopeOpenMode::NORMAL) {}
};



//Read in 512-byte chunks
//We use this low value to make progress reporting more frequent
#define YY_READ_BUF_SIZE 512

#define YY_INPUT(buffer, res, max_size)             \
do {                                                \
    base_parse_parm *pp = YYGET_EXTRA(yyscanner);   \
    if (pp->buffs.size()==0) {                      \
        res = 0;                                    \
        break;                                      \
    }                                               \
    input_buffer_state &ibs = pp->buffs.back();     \
    if (ibs.pos==0) {                               \
        yylloc->last_line = int(pp->pos_stack.line);\
        yylloc->last_column = int(pp->pos_stack.col);\
    }                                               \
    if (ibs.pos >= ibs.length) {                    \
        res = YY_NULL;                              \
    } else {                                        \
        res = int(ibs.length - ibs.pos);            \
        res > max_size ? res = max_size : 0;        \
        memcpy(buffer, ibs.buf + ibs.pos, res);     \
        YYGET_EXTRA(yyscanner)->RESULT->            \
            Progress.DoneParse(res);                \
        ibs.pos += res;                             \
        ibs.lex_buff_end = buffer+res;              \
    }                                               \
} while (0)

#define YY_USER_ACTION do {                                           \
    yylloc->first_line = yylloc->last_line;                           \
    yylloc->first_column = yylloc->last_column+1;                     \
	/* convert token length from byte-length to character-length */   \
    yylloc->last_column = yylloc->first_column +                      \
            unsigned(UTF8len(std::string_view(yytext, yyleng))) - 1;  \
    } while(0);


/** A macro creating a FileLineColRange both starting and ending at `A`.*/
#define CHART_POS(A) CHART_POS2(A,A)
/** A macro creating a FileLineColRange starting at `A` and ending at `B`.*/
// yacc column numbers start with 1 - we substract one here
#define CHART_POS2(A, B) FileLineColRange(                                          \
            FileLineCol(YYGET_EXTRA(yyscanner)->pos_stack, (A).first_line, (A).first_column - 1), \
            FileLineCol(YYGET_EXTRA(yyscanner)->pos_stack, (B).last_line,  (B).last_column  - 1))
/** A macro giving you the beginning position of a yyloc.*/
#define CHART_POS_START(A) FileLineCol(YYGET_EXTRA(yyscanner)->pos_stack, (A).first_line, (A).first_column - 1)
/** A macro giving you the position just after a yyloc.*/
#define CHART_POS_AFTER(A) FileLineCol(YYGET_EXTRA(yyscanner)->pos_stack, (A).last_line, (A).last_column)

#endif /* C_S_H_IS_COMPILED */


#endif //PARSERHELPER_H
