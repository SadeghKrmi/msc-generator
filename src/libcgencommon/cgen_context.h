/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file cgen_context.cpp The declaration of basic context related classes, procedures and inclusion support.
 * @ingroup libcgencommon_files */


#if !defined(CONTEXT_H)
#define CONTEXT_H

#include <cstring>
#include <string>

namespace gsl {
//
// owner - also defined in parse_tools.h!!
//
// owner<T> is designed as a bridge for code that must deal directly with owning pointers for some reason
//
// T must be a pointer type
// - disallow construction from any type other than pointer type
//
template <class T, class = std::enable_if_t<std::is_pointer<T>::value>>
using owner = T;

}

/** Enumerates ways to create a context */
enum class EContextCreate
{
    EMPTY, ///<Create an empty context
    PLAIN, ///<Create a context set to the 'plain' design
    CLEAR, ///<Initialize only base members to sane (empty) values. No default colors or styles to be added.
};

/** Enumerates parsing behaviour for a given context*/
enum class EContextParse
{
    NORMAL,      ///<We normally parse the context. This is how we parse main text.                      
    REPARSING,   ///<We are currently replaying a stored procedure.
    SKIP_CONTENT ///<In this context should skip what we read. We do not check style/color/procedure/design/marker/shape names and also ignore any elements generated. We also block design/shape definitions as they are global and procedure definitions as well. Such input text is either conditional and we dont need it (ifthenelse) or will later be reparsed (e.g., proc defs.).
};

class ContextParams
{
protected:
    bool is_full;                   ///<True if the context contains a setting for all chart options and a complete value for all default styles.
    const EContextParse parse_mode; ///<Determines, what to observe when parsing input text in this context.
    ContextParams(bool f, EContextParse p) : is_full(f), parse_mode(p), starts_procedure(false) {}
    ContextParams(const ContextParams &o) = default;
    ContextParams(ContextParams &&o) = default;
public:
    /** True, if this context is the top-level content of a procedure (either 
     * recording or replaying). Used to delineate procedure parameters.
     * That is, any parameter name shall only be searched up to where it was defined.
     * In other words, no parameter of a procedure shall be visible in a called procedure.*/
    bool starts_procedure;
    virtual ~ContextParams() = default;
    bool IsFull() const { return is_full; }
    bool Reparsing() const { return parse_mode==EContextParse::REPARSING; }
    bool SkipContent() const { return parse_mode==EContextParse::SKIP_CONTENT; }
    EContextParse GetParseMode() const { return parse_mode; }
};


/** Enumerates string comparison operators*/
enum class ECompareOperator
{
    INVALID = 0,
    SMALLER,            ///< '<'
    SMALLER_OR_EQUAL,   ///< '<='
    EQUAL,
    NOT_EQUAL,
    GREATER_OR_EQUAL,   ///< '=>'
    GREATER,            ///< '>'
};

/** A class holding a string during parsing that possibly comes from concatenation operator (+)
 * It must be a POD as it will be part of bison's union. */
struct multi_segment_string
{
    char *str;       ///<the null terminated string owned by this structure
    bool multi;      ///<True if the string actually had a concatenation operator
    bool had_param;  ///<True if the string had a parameter in it
    bool had_error;  ///<True if there was an error (+sing at end, bad parameter name, etc.)

    template <typename ... Strings>
    inline size_t CombinedLen(multi_segment_string &a, Strings ... strings)
    { return CombinedLen(strings...) + (a.str ? strlen(a.str) : 0); }

    template <typename ... Strings>
    inline size_t CombinedLen(gsl::owner<char *> a, Strings ... strings)
    { return (a ? strlen(a) : 0) + CombinedLen(strings...); }

    template <typename ... Strings>
    inline size_t CombinedLen(std::string_view a, Strings ... strings)
    { return a.length() + CombinedLen(strings...); }

    template <typename ... Strings>
    inline void CopyTo(char *to, multi_segment_string &a, Strings ... strings)
    {
        if (a.str) {
            memcpy(to, a.str, strlen(a.str));
            CopyTo(to+strlen(a.str), strings...);
            free(a.str);
        } else
            CopyTo(to, strings...);
        multi = true;
        had_param |= a.had_param;
        had_error |= a.had_error;
    }

    template <typename ... Strings>
    inline void CopyTo(char *to, gsl::owner<char *> a, Strings ... strings)
    {
        if (a) {
            memcpy(to, a, strlen(a));
            CopyTo(to+strlen(a), strings...);
            free(a);
        } else
            CopyTo(to, strings...);
        multi = true;
    }

    template <typename ... Strings>
    inline void CopyTo(char *to, const char *a, Strings ... strings)
    {
        if (a) {
            memcpy(to, a, strlen(a));
            CopyTo(to+strlen(a), strings...);
        } else
            CopyTo(to, strings...);
        multi = true;
    }

    template <typename ... Strings>
    inline void CopyTo(char *to, std::string_view a, Strings ... strings)
    {
        memcpy(to, a.data(), a.length());
        CopyTo(to+a.length(), strings...);
        multi = true;
    }

    template <typename ... Strings>
    inline void CombineThemToMe(multi_segment_string &a, Strings ... strings)
    {
        const size_t len = CombinedLen(a, strings...);
        str = (char*)malloc(len+1);
        multi = false;
        had_param = false;
        had_error = false;
        CopyTo(str, a, strings...);
        str[len] = 0;
    }

    template <typename ... Strings>
    inline void CombineThemToMe(gsl::owner<char *> a, Strings ... strings)
    {
        const size_t len = CombinedLen(a, strings...);
        str = (char*)malloc(len+1);
        multi = false;
        had_param = false;
        had_error = false;
        CopyTo(str, a, strings...);
        str[len] = 0;
    }

    template <typename ... Strings>
    inline void CombineThemToMe(const char *a, Strings ... strings)
    {
        const size_t len = CombinedLen(a, strings...);
        str = (char*)malloc(len+1);
        multi = false;
        had_param = false;
        had_error = false;
        CopyTo(str, a, strings...);
        str[len] = 0;
    }

    template <typename ... Strings>
    inline void CombineThemToMe(std::string_view a, Strings ... strings)
    {
        const size_t len = CombinedLen(a, strings...);
        str = (char*)malloc(len+1);
        multi = false;
        had_param = false;
        had_error = false;
        CopyTo(str, a, strings...);
        str[len] = 0;
    }
    void RemoveSpaces() noexcept {
        //group spaces to the end and set the first such grouped element to null (for null termination)
        *std::remove_if(str, str+strlen(str), [](const char c) noexcept {return c==' ' || c=='\t'; }) = 0;
    }
    int Compare(ECompareOperator op, const multi_segment_string&o) const;
    multi_segment_string() noexcept = default;
    explicit multi_segment_string(gsl::owner<char *> a) noexcept 
        : str(a), multi(false), had_param(false), had_error(false) {}
    explicit multi_segment_string(const char* a);
    explicit multi_segment_string(std::nullptr_t) noexcept
        : str(nullptr), multi(false), had_param(false), had_error(false) {}
};

template<>
inline size_t multi_segment_string::CombinedLen(std::string_view a)
{ return a.length(); }

template<>
inline size_t multi_segment_string::CombinedLen(multi_segment_string &a)
{ return a.str ? strlen(a.str) : 0; }

template<>
inline size_t multi_segment_string::CombinedLen(gsl::owner<char *> a)
{ return a ? strlen(a) : 0; }

template <>
inline void multi_segment_string::CopyTo(char *to, multi_segment_string &a)
{
    if (a.str) {
        memcpy(to, a.str, strlen(a.str));
        free(a.str);
    }
    multi = false;
    had_param = false;
    had_error = false;
}

template <>
inline void multi_segment_string::CopyTo(char *to, gsl::owner<char *> a)
{
    if (a) {
        memcpy(to, a, strlen(a));
        free(a);
    }
    multi = false;
    had_param = false;
    had_error = false;
}

template <>
inline void multi_segment_string::CopyTo(char *to,  const char *a)
{
    if (a) {
        memcpy(to, a, strlen(a));
    }
    multi = false;
    had_param = false;
    had_error = false;
}

template <>
inline void multi_segment_string::CopyTo(char *to, std::string_view a)
{
    memcpy(to, a.data(), a.length());
    multi = false;
    had_param = false;
    had_error = false;
}


struct MultiString
{
    std::string str;
    bool had_error = false;
    MultiString() = default;
    MultiString(std::string_view s) : str(s) {}
    MultiString(std::string &&s) : str(std::move(s)) {}
    MultiString(const MultiString &) = default;
    MultiString(MultiString &&) = default;
    MultiString &operator=(const MultiString&) = default;
    MultiString &operator=(MultiString&&) = default;
    operator std::string_view() const { return str; }
    operator std::string&() { return str; }
    
    template <typename ... Strings>
    inline size_t CombinedLen(std::string_view a, Strings ... strings)
    { return CombinedLen(strings...) + a.length(); }

    template <typename ... Strings>
    inline size_t CombinedLen(const MultiString &a, Strings ... strings)
    { return CombinedLen(strings...) + a.str.length(); }

    template <typename ... Strings>
    inline size_t CombinedLen(char, Strings ... strings)
    { return CombinedLen(strings...)+1; }

    template <typename ... Strings>
    inline void CopyTo(char *to, std::string_view a, Strings ... strings)
    {
        if (a.length()) {
            memcpy(to, a.data(), a.length());
            CopyTo(to+a.length(), strings...);
        } else
            CopyTo(to, strings...);
    }

    template <typename ... Strings>
    inline void CopyTo(char *to, const MultiString &a, Strings ... strings)
    {
        memcpy(to, a.str.data(), a.str.length());
        CopyTo(to+a.str.length(), strings...);
        had_error |= a.had_error;
    }

    template <typename ... Strings>
    inline void CopyTo(char *to, char a, Strings ... strings)
    {
        *to = a;
        CopyTo(to+1, strings...);
    }

    template <typename ... Strings>
    inline void CombineThemToMe(const MultiString &a, Strings ... strings)
    {
        const size_t len = CombinedLen(a, strings...);
        str.resize(len);
        had_error = false;
        CopyTo(str.data(), a, strings...);
    }

    template <typename ... Strings>
    inline void CombineThemToMe(std::string_view a, Strings ... strings)
    {
        const size_t len = CombinedLen(a, strings...);
        str.resize(len);
        had_error = false;
        CopyTo(str.data(), a, strings...);
    }

    template <typename ... Strings>
    inline void CombineThemToMe(char a, Strings ... strings)
    {
        const size_t len = CombinedLen(a, strings...);
        str.resize(len);
        had_error = false;
        CopyTo(str.data(), a, strings...);
    }

    void RemoveSpaces();
    int Compare(ECompareOperator op, const MultiString &o) const;

};


template<>
inline size_t MultiString::CombinedLen(std::string_view a)
{ return a.length(); }

template<>
inline size_t MultiString::CombinedLen(const MultiString & a)
{ return a.str.length(); }

template<>
inline size_t MultiString::CombinedLen(char)
{ return 1; }

template <>
inline void MultiString::CopyTo(char *to, const MultiString &a)
{
    memcpy(to, a.str.data(), a.str.length());
    had_error = a.had_error;
}

template <>
inline void MultiString::CopyTo(char *to, std::string_view a)
{
    memcpy(to, a.data(), a.length());
}

template <>
inline void MultiString::CopyTo(char *to, char a)
{
    *to = a;
}

#endif //CONTEXT_H

