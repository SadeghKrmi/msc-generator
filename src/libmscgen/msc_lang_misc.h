/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/* This file is included by parsers both for color syntax and language compilation */

#ifndef MSCPARSERHELPER_H
#define MSCPARSERHELPER_H


#include "language_misc.h"
#include "mscstyle.h"

#ifdef C_S_H_IS_COMPILED
    //Use stock parse_parm - good enough for us.
    typedef base_parse_parm_csh msc_parse_parm_csh;

    /** Actual 'parse_param' structure (descendant of base_parse_param), shall be defined by chart.*/
    #define YY_EXTRA_TYPE msc_parse_parm_csh *

    #undef YY_USER_ACTION
    #define YY_USER_ACTION do {                                                                 \
        yylloc->first_pos = yylloc->last_pos+1;                                                 \
	    /* convert token length from byte-length to character-length */                         \
        yylloc->last_pos = yylloc->last_pos + UTF8len(std::string_view(yytext, yyleng));        \
        /* Set initial condition according to our compatibility mode*/                          \
        /* (usually changes only once after the 'msc {...}' is detected or at the beginning) */ \
        BEGIN(YYGET_EXTRA(yyscanner)->csh->mscgen_compat ==                                     \
            EMscgenCompat::FORCE_MSCGEN ? LEX_STATE_MSCGEN_COMPAT : INITIAL);                   \
        } while(0);

#else

    //Use stock parse_parm - good enough for us.
    typedef base_parse_parm msc_parse_parm;

    /** Actual 'parse_param' structure (descendant of base_parse_param), shall be defined by chart.*/
    #define YY_EXTRA_TYPE msc_parse_parm *

    #undef YY_USER_ACTION //from language_misc.h - we have an extra thing to do
    #define YY_USER_ACTION do {                                                                       \
        yylloc->first_line = yylloc->last_line;                                                       \
        yylloc->first_column = yylloc->last_column+1;                                                 \
	    /* convert token length from byte-length to character-length */                               \
        yylloc->last_column = yylloc->first_column + UTF8len(std::string_view(yytext, yyleng)) - 1;   \
        /* Set initial condition according to our compatibility mode*/                                \
        /* (usually changes only once after the 'msc {...}' is detected or at the beginning) */       \
        BEGIN(YYGET_EXTRA(yyscanner)->chart->mscgen_compat == EMscgenCompat::FORCE_MSCGEN ? LEX_STATE_MSCGEN_COMPAT : INITIAL); \
        } while(0);

    void MscPushFlex(base_parse_parm &pp, const char *text, size_t len, YYLTYPE *old_pos, const FileLineCol &new_pos, EInclusionReason reason);

#endif 


#endif