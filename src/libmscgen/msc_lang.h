/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_MSC_MSC_LANG_H_INCLUDED
# define YY_MSC_MSC_LANG_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int msc_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    TOK_EOF = 0,                   /* TOK_EOF  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    TOK_STRING = 258,              /* TOK_STRING  */
    TOK_QSTRING = 259,             /* TOK_QSTRING  */
    TOK_NUMBER = 260,              /* TOK_NUMBER  */
    TOK_DASH = 261,                /* TOK_DASH  */
    TOK_EQUAL = 262,               /* TOK_EQUAL  */
    TOK_COMMA = 263,               /* TOK_COMMA  */
    TOK_TILDE = 264,               /* TOK_TILDE  */
    TOK_SEMICOLON = 265,           /* TOK_SEMICOLON  */
    TOK_PLUS = 266,                /* TOK_PLUS  */
    TOK_PLUS_EQUAL = 267,          /* TOK_PLUS_EQUAL  */
    TOK_PIPE_SYMBOL = 268,         /* TOK_PIPE_SYMBOL  */
    TOK_OCBRACKET = 269,           /* TOK_OCBRACKET  */
    TOK_CCBRACKET = 270,           /* TOK_CCBRACKET  */
    TOK_OSBRACKET = 271,           /* TOK_OSBRACKET  */
    TOK_CSBRACKET = 272,           /* TOK_CSBRACKET  */
    TOK_OPARENTHESIS = 273,        /* TOK_OPARENTHESIS  */
    TOK_CPARENTHESIS = 274,        /* TOK_CPARENTHESIS  */
    TOK_PARAM_NAME = 275,          /* TOK_PARAM_NAME  */
    TOK_ASTERISK = 276,            /* TOK_ASTERISK  */
    TOK_MSC = 277,                 /* TOK_MSC  */
    TOK_COLON_STRING = 278,        /* TOK_COLON_STRING  */
    TOK_COLON_QUOTED_STRING = 279, /* TOK_COLON_QUOTED_STRING  */
    TOK_STYLE_NAME = 280,          /* TOK_STYLE_NAME  */
    TOK_COLORDEF = 281,            /* TOK_COLORDEF  */
    TOK_COLORDEF_NAME_NUMBER = 282, /* TOK_COLORDEF_NAME_NUMBER  */
    TOK_REL_TO = 283,              /* TOK_REL_TO  */
    TOK_REL_FROM = 284,            /* TOK_REL_FROM  */
    TOK_REL_BIDIR = 285,           /* TOK_REL_BIDIR  */
    TOK_REL_X = 286,               /* TOK_REL_X  */
    TOK_REL_DASH_X = 287,          /* TOK_REL_DASH_X  */
    TOK_REL_MSCGEN = 288,          /* TOK_REL_MSCGEN  */
    TOK_SPECIAL_ARC = 289,         /* TOK_SPECIAL_ARC  */
    TOK_EMPH = 290,                /* TOK_EMPH  */
    TOK_EMPH_PLUS_PLUS = 291,      /* TOK_EMPH_PLUS_PLUS  */
    TOK_COMMAND_HEADING = 292,     /* TOK_COMMAND_HEADING  */
    TOK_COMMAND_NUDGE = 293,       /* TOK_COMMAND_NUDGE  */
    TOK_COMMAND_NEWPAGE = 294,     /* TOK_COMMAND_NEWPAGE  */
    TOK_COMMAND_DEFSHAPE = 295,    /* TOK_COMMAND_DEFSHAPE  */
    TOK_COMMAND_DEFCOLOR = 296,    /* TOK_COMMAND_DEFCOLOR  */
    TOK_COMMAND_DEFSTYLE = 297,    /* TOK_COMMAND_DEFSTYLE  */
    TOK_COMMAND_DEFDESIGN = 298,   /* TOK_COMMAND_DEFDESIGN  */
    TOK_COMMAND_DEFPROC = 299,     /* TOK_COMMAND_DEFPROC  */
    TOK_COMMAND_REPLAY = 300,      /* TOK_COMMAND_REPLAY  */
    TOK_COMMAND_SET = 301,         /* TOK_COMMAND_SET  */
    TOK_COMMAND_INCLUDE = 302,     /* TOK_COMMAND_INCLUDE  */
    TOK_COMMAND_BIG = 303,         /* TOK_COMMAND_BIG  */
    TOK_COMMAND_BOX = 304,         /* TOK_COMMAND_BOX  */
    TOK_COMMAND_PIPE = 305,        /* TOK_COMMAND_PIPE  */
    TOK_COMMAND_MARK = 306,        /* TOK_COMMAND_MARK  */
    TOK_COMMAND_MARK_SRC = 307,    /* TOK_COMMAND_MARK_SRC  */
    TOK_COMMAND_MARK_DST = 308,    /* TOK_COMMAND_MARK_DST  */
    TOK_COMMAND_PARALLEL = 309,    /* TOK_COMMAND_PARALLEL  */
    TOK_COMMAND_OVERLAP = 310,     /* TOK_COMMAND_OVERLAP  */
    TOK_COMMAND_INLINE = 311,      /* TOK_COMMAND_INLINE  */
    TOK_VERTICAL = 312,            /* TOK_VERTICAL  */
    TOK_VERTICAL_SHAPE = 313,      /* TOK_VERTICAL_SHAPE  */
    TOK_AT = 314,                  /* TOK_AT  */
    TOK_LOST = 315,                /* TOK_LOST  */
    TOK_AT_POS = 316,              /* TOK_AT_POS  */
    TOK_SHOW = 317,                /* TOK_SHOW  */
    TOK_HIDE = 318,                /* TOK_HIDE  */
    TOK_ACTIVATE = 319,            /* TOK_ACTIVATE  */
    TOK_DEACTIVATE = 320,          /* TOK_DEACTIVATE  */
    TOK_BYE = 321,                 /* TOK_BYE  */
    TOK_START = 322,               /* TOK_START  */
    TOK_BEFORE = 323,              /* TOK_BEFORE  */
    TOK_END = 324,                 /* TOK_END  */
    TOK_AFTER = 325,               /* TOK_AFTER  */
    TOK_COMMAND_VSPACE = 326,      /* TOK_COMMAND_VSPACE  */
    TOK_COMMAND_HSPACE = 327,      /* TOK_COMMAND_HSPACE  */
    TOK_COMMAND_SYMBOL = 328,      /* TOK_COMMAND_SYMBOL  */
    TOK_COMMAND_NOTE = 329,        /* TOK_COMMAND_NOTE  */
    TOK_COMMAND_COMMENT = 330,     /* TOK_COMMAND_COMMENT  */
    TOK_COMMAND_ENDNOTE = 331,     /* TOK_COMMAND_ENDNOTE  */
    TOK_COMMAND_FOOTNOTE = 332,    /* TOK_COMMAND_FOOTNOTE  */
    TOK_COMMAND_TITLE = 333,       /* TOK_COMMAND_TITLE  */
    TOK_COMMAND_SUBTITLE = 334,    /* TOK_COMMAND_SUBTITLE  */
    TOK_COMMAND_TEXT = 335,        /* TOK_COMMAND_TEXT  */
    TOK_SHAPE_COMMAND = 336,       /* TOK_SHAPE_COMMAND  */
    TOK_JOIN = 337,                /* TOK_JOIN  */
    TOK_IF = 338,                  /* TOK_IF  */
    TOK_THEN = 339,                /* TOK_THEN  */
    TOK_ELSE = 340,                /* TOK_ELSE  */
    TOK_MSCGEN_RBOX = 341,         /* TOK_MSCGEN_RBOX  */
    TOK_MSCGEN_ABOX = 342,         /* TOK_MSCGEN_ABOX  */
    TOK_UNRECOGNIZED_CHAR = 343    /* TOK_UNRECOGNIZED_CHAR  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 86 "msc_lang.yy"

    gsl::owner<char*>                                          str;
    const char*                                                input_text_ptr;
    int                                                        condition; //0:false, 1:true, 2:had_error
    ECompareOperator                                           compare_op;
    multi_segment_string                                       multi_str;
    gsl::owner<CHAR_IF_CSH(ArcBase)*>                          arcbase;
    gsl::owner<CHAR_IF_CSH(ArcList)*>                          arclist;
    gsl::owner<CHAR_IF_CSH(Arrow)*>                            arcarrow;
    gsl::owner<CHAR_IF_CSH(Vertical)*>                         arcvertarrow;
    gsl::owner<CHAR_IF_CSH(Box)*>                              arcbox;
    gsl::owner<CHAR_IF_CSH(Pipe)*>                             arcpipe;
    gsl::owner<CHAR_IF_CSH(BoxSeries)*>                        arcboxseries;
    gsl::owner<CHAR_IF_CSH(PipeSeries)*>                       arcpipeseries;
    gsl::owner<CHAR_IF_CSH(ParallelBlocks)*>                   arcparallel;
    EArcSymbol                                                 arcsymbol;
    gsl::owner<CHAR_IF_CSH(EntityAppHelper)*>                  entitylist;
    gsl::owner<CHAR_IF_CSH(Attribute)*>                        attrib;
    gsl::owner<CHAR_IF_CSH(AttributeList)*>                    attriblist;
    gsl::owner<CHAR_IF_CSH(VertXPos)*>                         vertxpos;
    gsl::owner<CHAR_IF_CSH(ExtVertXPos)*>                      extvertxpos;
    gsl::owner<CHAR_IF_CSH(NamePair)*>                         namerel;
    gsl::owner<std::list<std::string>*>                        stringlist;
    CHAR_IF_CSH(ESide)                                         eside;
    gsl::owner<CHAR_IF_CSH(ArrowSegmentData)*>                 arcsegdata;
    Vertical::EVerticalShape                                   vshape;
    gsl::owner<CHAR_IF_CSH(ArcTypePlusDir)*>                   arctypeplusdir;
    ShapeElement::Type                                         shapecommand;
    gsl::owner<CHAR_IF_CSH(Shape)*>                            shape;
    gsl::owner<CHAR_IF_CSH(ShapeElement)*>                     shapeelement;
    gsl::owner<CHAR_IF_CSH(ArrowEnding)*>                      arrowending;
    gsl::owner<CHAR_IF_CSH(Procedure)*>                        procedure;
    CHAR_IF_CSH(const Procedure)*                              cprocedure;
    gsl::owner<CHAR_IF_CSH(ProcParamDef)*>                     procparamdef;
    gsl::owner<CHAR_IF_CSH(ProcParamDefList)*>                 procparamdeflist;
    gsl::owner<CHAR_IF_CSH(ProcParamInvocation)*>              procparaminvoc;
    gsl::owner<CHAR_IF_CSH(ProcParamInvocationList)*>          procparaminvoclist;
    gsl::owner<CHAR_IF_CSH(ProcDefParseHelper<AttributeList>)*>procdefhelper;

#line 192 "msc_lang.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif




int msc_parse (YYMSC_RESULT_TYPE &RESULT, void *yyscanner);


#endif /* !YY_MSC_MSC_LANG_H_INCLUDED  */
