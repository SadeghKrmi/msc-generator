/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file msc_parse_tools.cpp Utilities for parsing.
 * @ingroup libmscgen_files
 */

//We do not compile csh, so define the below macro as a no-op
#include "cgen_attribute.h"  //for case insensitive compares
#define CHAR_IF_CSH(A) A
#include "msc.h" //required by msc_lang.h to define YYSTYPE
using namespace msc;
#define YYMSC_RESULT_TYPE MscChart
#define RESULT msc
#include "msc_lang.h"
//parse_tools.h requires YYLTYPE, this is why we must include language.h
//plus YYSTYPE is also required by language2.h
#include "msc_parse_tools.h"
#include "msc_lang_misc.h"
#define YY_HEADER_EXPORT_START_CONDITIONS
#include "msc_lang2.h" //Needs parse_param from msc_lang_misc.h

//define isatty - only once, so we put it into this file
#ifndef HAVE_UNISTD_H
int isatty (int) {return 0;}
#endif

using namespace msc;

/** Converts 'emphasis' (a deprecated style name) to 'box' and emits warning if doing so.
 * We return 'box' or 'emptybox' if the input style name was 'emphasis' or
 * 'emptyemphasis' and generate a warning. Else we return `style`.*/
string msc::ConvertEmphasisToBox(const string &style, const FileLineCol &loc, MscChart &msc)
{
    if (style != "emphasis" && style != "emptyemphasis")
        return style;
    const char *newname = style == "emphasis" ? "box" : "emptybox";
    msc.Error.Warning(loc,
        "Stylename '" + style + "' is deprecated, using " + newname + " instead.");
    return newname;
}

/** The error handling function required by yacc.
 * The TOK_XXX names are substituted for more
 *  understandable values that make more sense to the user and
 * the error is added to `msc`.
 * @param [in] loc The location of the error.
 * @param msc The chart to add the error to.
 * @param yyscanner The state of the parser
 * @param [in] str The message we need to beautify and emit into `msc`.*/
void msc_error(YYLTYPE*loc, MscChart &msc, void *yyscanner, const char *str)
{
    static const std::pair<string, string> tokens[] = {
    std::pair<string,string>("unexpected", "Unexpected"),
    std::pair<string,string>("TOK_REL_SOLID_TO", "'->'"),
    std::pair<string,string>("TOK_REL_SOLID_FROM", "'<-'"),
    std::pair<string,string>("TOK_REL_SOLID_BIDIR", "'<->'"),
    std::pair<string,string>("TOK_REL_DOUBLE_TO", "'=>'"),
    std::pair<string,string>("TOK_REL_DOUBLE_FROM", "'<='"),
    std::pair<string,string>("TOK_REL_DOUBLE_BIDIR", "'<=>'"),
    std::pair<string,string>("TOK_REL_DASHED_TO", "'>>'"),
    std::pair<string,string>("TOK_REL_DASHED_FROM", "'<<'"),
    std::pair<string,string>("TOK_REL_DASHED_BIDIR", "'<<>>'"),
    std::pair<string,string>("TOK_REL_DOTTED_TO", "'>'"),
    std::pair<string,string>("TOK_REL_DOTTED_FROM", "'<'"),
    std::pair<string,string>("TOK_REL_DOTTED_BIDIR", "'<>'"),
    std::pair<string,string>("TOK_EMPH", "'..', '--', '=='"),
    std::pair<string,string>("TOK_SPECIAL_ARC", "'...', '---'"),
    std::pair<string,string>("syntax error, ", ""),
    std::pair<string,string>(", expecting $end", ""),
    std::pair<string,string>("$end", "end of input"),
    std::pair<string,string>("TOK_OCBRACKET", "'{'"),
    std::pair<string,string>("TOK_CCBRACKET", "'}'"),
    std::pair<string,string>("TOK_OSBRACKET", "'['"),
    std::pair<string,string>("TOK_CSBRACKET", "']'"),
    std::pair<string,string>("TOK_EQUAL", "'='"),
    std::pair<string,string>("TOK_DASH", "'-'"),
    std::pair<string,string>("TOK_PLUS", "'+'"),
    std::pair<string,string>("TOK_COMMA", "','"),
    std::pair<string,string>("TOK_SEMICOLON", "';'"),
    std::pair<string,string>("TOK_MSC", "'msc'"),
    std::pair<string,string>("TOK_BOOLEAN", "'yes', 'no'"),
    std::pair<string,string>("TOK_COMMAND_HEADING", "'heading'"),
    std::pair<string,string>("TOK_COMMAND_NUDGE", "'nudge'"),
    std::pair<string,string>("TOK_COMMAND_NEWPAGE", "'newpage'"),
    std::pair<string,string>("TOK_COMMAND_DEFCOLOR", "'defcolor'"),
    std::pair<string,string>("TOK_COMMAND_DEFSTYLE", "'defstyle'"),
    std::pair<string,string>("TOK_COMMAND_DEFDESIGN", "'defdesign'"),
    std::pair<string,string>("TOK_COMMAND_BIG", "'block'"),
    std::pair<string,string>("TOK_COMMAND_PIPE", "'pipe'"),
    std::pair<string,string>("TOK_COMMAND_MARK", "'mark'"),
    std::pair<string,string>("TOK_COMMAND_PARALLEL", "'parallel'"),
    std::pair<string,string>("TOK_VERTICAL", "'vertical'"),
    std::pair<string,string>("TOK_AT", "'at'"),
    std::pair<string,string>("TOK_AT_POS", "'left', 'right' or 'center'"),
    std::pair<string,string>("TOK_SHOW", "'show'"),
    std::pair<string,string>("TOK_HIDE", "'hide'"),
    std::pair<string,string>("TOK_BYE", "'bye'"),
    std::pair<string,string>("TOK_COMMAND_VSPACE", "'vspace'"),
    std::pair<string,string>("TOK_COMMAND_HSPACE", "'hspace'"),
    std::pair<string,string>("TOK_COMMAND_SYMBOL", "'symbol'"),
    std::pair<string,string>("TOK_COMMAND_NOTE", "'note'"),
    std::pair<string,string>("TOK_COMMAND_COMMENT", "'comment'"),
    std::pair<string,string>("TOK_COLON_STRING", "':'"),  //just say colon to the user
    std::pair<string,string>("TOK_COLON_QUOTED_STRING", "':'"),  //just say colon to the user
    std::pair<string,string>("TOK_NUMBER", "number"),
    std::pair<string,string>("TOK_STRING", "string"),
    std::pair<string,string>("TOK_EStyleType::NAME", "style name"),
    std::pair<string,string>("TOK_QSTRING", "quoted string")};

    const size_t tokArrayLen = sizeof(tokens) / sizeof(std::pair<string, string>);
    string msg(str);

    string once_msg;

    //replace tokens in string. We assume
    //-each toke appears only once
    //-replaced strings will not be mistaken for a a token
    for (unsigned i=0; i<tokArrayLen; i++) {
        string::size_type pos = 0;
        //Search for the current token
        pos = msg.find(tokens[i].first, pos);
        //continue if not found
        if (pos == string::npos) continue;
        //if msg continues with an uppercase letter or _ we are not matching
        char next = msg.c_str()[pos+tokens[i].first.length()];
        if ((next<='Z' && next>='A') || next == '_') continue;

        //Ok, token found, create substitution
        string ins = tokens[i].second;
        //special comment for unexpected symbols
        //special handling for numbers and strings
        if (i>tokArrayLen-5) {
            string::size_type exppos = msg.find("expecting");
            //If we replace what was unexpected, use actual token text
            if (pos < exppos) {
                if (i==tokArrayLen-1)
                    ins += ": " + string(msc_get_text(yyscanner));
                else
                    ins += ": '" + string(msc_get_text(yyscanner)) + "'";
                if (i==tokArrayLen-2) {
                    string hint(msc_get_text(yyscanner));
                    string::size_type pos2 = hint.find_first_not_of("abcdefghijklmnopqrstuvwxyz");
                    hint.insert(pos2," ");
                    once_msg = "Try splitting it with a space: '"+hint+"'.";
                }
            } else {
                ins = "a <" + ins + ">";
            }
        }
        //replace token
        msg.replace(pos, tokens[i].first.length(), ins);
    }
    string::size_type pos = msg.rfind("', '");
    if (pos != string::npos) {
        msg.erase(pos+1, 1);
        msg.insert(pos+1, " or");
    }
    msg.append(".");
#define YYGET_EXTRA msc_get_extra
    msc.Error.Error(CHART_POS_START(*loc), msg, once_msg);
};
