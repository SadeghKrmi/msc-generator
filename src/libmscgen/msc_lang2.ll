%option reentrant noyywrap nounput
%option bison-bridge bison-locations
%s LEX_STATE_MSCGEN_COMPAT LEX_STATE_MSCGEN_COMPAT_BOX

%{
    /*
        This file is part of Msc-generator.
        Copyright (C) 2008-2022 Zoltan Turanyi
        Distributed under GNU Affero General Public License.

        Msc-generator is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        Msc-generator is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
    */

#include <stdio.h>
#include <string.h>
#include <iostream>

#ifndef HAVE_UNISTD_H
#define YY_NO_UNISTD_H
extern int isatty(int);
#endif

#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #include "msccsh.h"
    #include "commands.h" //MSC_* defs and CommandNote and Shapes in entity.h
    using namespace msc;
    #define YYMSC_RESULT_TYPE MscCsh
    #define RESULT csh
    #define YYGET_EXTRA msccsh_get_extra
    #define YYLTYPE_IS_DECLARED  //If we scan for color syntax highlight use this location type (YYLTYPE)
    #define YYLTYPE CshPos
    #define CHAR_IF_CSH(A) char
    #include "msc_csh_lang.h"   //Must be after language_misc.h
    #include "msc_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
#else
    #define C_S_H (0)
    #include "msc.h"
    using namespace msc;
    #define YYMSC_RESULT_TYPE MscChart
    #define RESULT chart
    #define YYGET_EXTRA msc_get_extra
    #define CHAR_IF_CSH(A) A
    #include "msc_lang.h"
    #include "msc_parse_tools.h"
    #include "msc_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined

    //this function must be in the lex file, so that lex replaces the "yy" in yy_create_buffer and
    //yypush_buffer_state to the appropriate chart-specific prefix.
    void MscPushFlex(base_parse_parm &pp, const char *text, size_t len, YYLTYPE *old_pos, const FileLineCol &new_pos, EInclusionReason reason)
    {
        if (text==nullptr || len==0) return;
        pp.buffs.emplace_back(text, len);
        pp.pos_stack.line = old_pos->first_line;
        pp.pos_stack.col = old_pos->first_column-1; //not sure why -1 is needed
        pp.pos_stack.Push(new_pos);
        pp.pos_stack.reason = reason;
        yypush_buffer_state(yy_create_buffer( nullptr, YY_BUF_SIZE, pp.yyscanner ), pp.yyscanner);
        pp.chart->Progress.RegisterParse(len);
    }
#endif

%}

%%

 /* Newline characters in all forms accepted */
\x0d\x0a     %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

\x0a         %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

\x0d         %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

 /* # starts a comment last until end of line */
#[^\x0d\x0a]* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #endif
%}

 /* // starts a comment last until end of line */
\/\/[^\x0d\x0a]* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #endif
%}

 /* / * .. * / block comments*/
\/\*([^\*]*(\*[^\/])?)*\*\/ %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #else
    language_process_block_comments(yytext, yylloc);
  #endif
%}

 /* / * ... unclosed block comments */
\/\* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
    yyget_extra(yyscanner)->csh->AddCSH_Error(*yylloc, "Unpaired beginning of block comment '/" "*'.");
  #else
    yyget_extra(yyscanner)->chart->Error.Error(CHART_POS_START(*yylloc),
         "Unpaired beginning of block comment '/" "*'.");
  #endif
%}


[ \t]+    /* ignore whitespace */;

 /* These shape definition keywords are case sensitive */
M	yylval_param->shapecommand = ShapeElement::MOVE_TO; return TOK_SHAPE_COMMAND;
L	yylval_param->shapecommand = ShapeElement::LINE_TO; return TOK_SHAPE_COMMAND;
C	yylval_param->shapecommand = ShapeElement::CURVE_TO; return TOK_SHAPE_COMMAND;
E	yylval_param->shapecommand = ShapeElement::CLOSE_PATH; return TOK_SHAPE_COMMAND;
S	yylval_param->shapecommand = ShapeElement::SECTION_BG; return TOK_SHAPE_COMMAND;
T	yylval_param->shapecommand = ShapeElement::TEXT_AREA; return TOK_SHAPE_COMMAND;
H	yylval_param->shapecommand = ShapeElement::HINT_AREA; return TOK_SHAPE_COMMAND;
P	yylval_param->shapecommand = ShapeElement::PORT; return TOK_SHAPE_COMMAND;

 /* This is used for mscgen style loss arrow symbol -x and x-*/
<LEX_STATE_MSCGEN_COMPAT>{
    X|x                          yylval_param->str = strdup(yytext);  return TOK_REL_X;
    -[xX]/[ \x0d\x0a\t\[\;\,]    return TOK_REL_DASH_X;
}

 /* These keywords are case insensitive */
(?i:msc)       yylval_param->str = strdup(yytext); return TOK_MSC;
(?i:heading)   yylval_param->str = strdup(yytext); return TOK_COMMAND_HEADING;
(?i:nudge)     yylval_param->str = strdup(yytext); return TOK_COMMAND_NUDGE;
(?i:defshape)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFSHAPE;
(?i:defcolor)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFCOLOR;
(?i:defstyle)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFSTYLE;
(?i:defdesign) yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFDESIGN;
(?i:defproc)   yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFPROC;
(?i:replay)    yylval_param->str = strdup(yytext); return TOK_COMMAND_REPLAY;
(?i:set)       yylval_param->str = strdup(yytext); return TOK_COMMAND_SET; 
(?i:include)   yylval_param->str = strdup(yytext); return TOK_COMMAND_INCLUDE;
(?i:newpage)   yylval_param->str = strdup(yytext); return TOK_COMMAND_NEWPAGE;
(?i:block)     yylval_param->str = strdup(yytext); return TOK_COMMAND_BIG;
(?i:box)       if (YY_START == LEX_STATE_MSCGEN_COMPAT) BEGIN(LEX_STATE_MSCGEN_COMPAT_BOX); yylval_param->str = strdup(yytext); return TOK_COMMAND_BOX;
(?i:pipe)      yylval_param->str = strdup(yytext); return TOK_COMMAND_PIPE;
(?i:mark)      yylval_param->str = strdup(yytext); return TOK_COMMAND_MARK;
(?i:parallel)  yylval_param->str = strdup(yytext); return TOK_COMMAND_PARALLEL;
(?i:overlap)   yylval_param->str = strdup(yytext); return TOK_COMMAND_OVERLAP;
(?i:vertical)  %{
    yylval_param->str = strdup(yytext);
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->hint_vertical_shapes = true;
  #endif
    return TOK_VERTICAL;
%}

(?i:start)     yylval_param->str = strdup(yytext); return TOK_START;
(?i:before)    yylval_param->str = strdup(yytext); return TOK_BEFORE;
(?i:end)       yylval_param->str = strdup(yytext); return TOK_END;
(?i:after)     yylval_param->str = strdup(yytext); return TOK_AFTER;
(?i:at)        yylval_param->str = strdup(yytext); return TOK_AT;
(?i:lost)      yylval_param->str = strdup(yytext); return TOK_LOST;
(?i:left)      yylval_param->str = strdup(yytext); return TOK_AT_POS;
(?i:center)    yylval_param->str = strdup(yytext); return TOK_AT_POS;
(?i:right)     yylval_param->str = strdup(yytext); return TOK_AT_POS;
(?i:show)      yylval_param->str = strdup(yytext); return TOK_SHOW;
(?i:hide)      yylval_param->str = strdup(yytext); return TOK_HIDE;
(?i:activate)  yylval_param->str = strdup(yytext); return TOK_ACTIVATE;
(?i:deactivate) yylval_param->str= strdup(yytext); return TOK_DEACTIVATE;
(?i:bye)       yylval_param->str = strdup(yytext); return TOK_BYE;
(?i:vspace)    yylval_param->str = strdup(yytext); return TOK_COMMAND_VSPACE;
(?i:hspace)    yylval_param->str = strdup(yytext); return TOK_COMMAND_HSPACE;
(?i:symbol)    yylval_param->str = strdup(yytext); return TOK_COMMAND_SYMBOL;
(?i:note)      yylval_param->str = strdup(yytext); return TOK_COMMAND_NOTE;
(?i:comment)   yylval_param->str = strdup(yytext); return TOK_COMMAND_COMMENT;
(?i:endnote)   yylval_param->str = strdup(yytext); return TOK_COMMAND_ENDNOTE;
(?i:footnote)  yylval_param->str = strdup(yytext); return TOK_COMMAND_FOOTNOTE;
(?i:title)     yylval_param->str = strdup(yytext); return TOK_COMMAND_TITLE;
(?i:subtitle)  yylval_param->str = strdup(yytext); return TOK_COMMAND_SUBTITLE;
(?i:text)      yylval_param->str = strdup(yytext); return TOK_COMMAND_TEXT;
(?i:brace)     yylval_param->str = strdup(yytext); return TOK_VERTICAL_SHAPE;
(?i:bracket)   yylval_param->str = strdup(yytext); return TOK_VERTICAL_SHAPE;
(?i:range)     yylval_param->str = strdup(yytext); return TOK_VERTICAL_SHAPE;
(?i:pointer)   yylval_param->str = strdup(yytext); return TOK_VERTICAL_SHAPE;
(?i:rbox)      yylval_param->str = strdup(yytext); return TOK_MSCGEN_RBOX;
(?i:abox)      yylval_param->str = strdup(yytext); return TOK_MSCGEN_ABOX;
(?i:join)      yylval_param->str = strdup(yytext); return TOK_JOIN;
(?i:if)        yylval_param->str = strdup(yytext); return TOK_IF;
(?i:then)      yylval_param->str = strdup(yytext); return TOK_THEN;
(?i:else)      yylval_param->str = strdup(yytext); return TOK_ELSE;

\.\.\.   yylval_param->arcsymbol = EArcSymbol::DIV_DISCO;       return TOK_SPECIAL_ARC;// ...
---      yylval_param->arcsymbol = EArcSymbol::DIV_DIVIDER;     return TOK_SPECIAL_ARC;// ---
\|\|\|   yylval_param->arcsymbol = EArcSymbol::DIV_VSPACE;      return TOK_SPECIAL_ARC;// |||
-\>      yylval_param->arcsymbol = EArcSymbol::ARC_SOLID;       return TOK_REL_TO;     // ->
\<-      yylval_param->arcsymbol = EArcSymbol::ARC_SOLID;       return TOK_REL_FROM;   // <-
\<-\>    yylval_param->arcsymbol = EArcSymbol::ARC_SOLID_BIDIR; return TOK_REL_BIDIR;  // <->
=\>      yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE;      return TOK_REL_TO;     // =>
\<=      yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE;      return TOK_REL_FROM;   // <=
\<=\>    yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE_BIDIR;return TOK_REL_BIDIR;  // <=>
\>\>     yylval_param->arcsymbol = EArcSymbol::ARC_DASHED;      return TOK_REL_TO;     // >>
\<\<     yylval_param->arcsymbol = EArcSymbol::ARC_DASHED;      return TOK_REL_FROM;   // <<
\<\<\>\> yylval_param->arcsymbol = EArcSymbol::ARC_DASHED_BIDIR;return TOK_REL_BIDIR;  // <<>>
\>       yylval_param->arcsymbol = EArcSymbol::ARC_DOTTED;      return TOK_REL_TO;     // >
\<       yylval_param->arcsymbol = EArcSymbol::ARC_DOTTED;      return TOK_REL_FROM;   // <
\<\>     yylval_param->arcsymbol = EArcSymbol::ARC_DOTTED_BIDIR;return TOK_REL_BIDIR;  // <>
=\>\>    yylval_param->arcsymbol = EArcSymbol::ARC_DBLDBL;      return TOK_REL_TO;     // =>>
\<\<=    yylval_param->arcsymbol = EArcSymbol::ARC_DBLDBL;      return TOK_REL_FROM;   // <<=
\<\<=\>\> yylval_param->arcsymbol = EArcSymbol::ARC_DBLDBL_BIDIR;return TOK_REL_BIDIR; // <<=>>
==\>     yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE2;      return TOK_REL_TO;    // ==>
\<==     yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE2;      return TOK_REL_FROM;  // <==
\<==\>   yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE2_BIDIR;return TOK_REL_BIDIR; // <==>

 /* The box symbols in Msc-generator*/
<INITIAL>{
    --       yylval_param->arcsymbol = EArcSymbol::BOX_SOLID;       return TOK_EMPH;       // --
    \+\+     yylval_param->arcsymbol = EArcSymbol::BOX_DASHED;      return TOK_EMPH_PLUS_PLUS;// ++
    \.\.     yylval_param->arcsymbol = EArcSymbol::BOX_DOTTED;      return TOK_EMPH;       // ..
    ==       yylval_param->arcsymbol = EArcSymbol::BOX_DOUBLE;      return TOK_EMPH;       // ==
}
 /* The box symbols in Msc-generator, temporarily used after a BOX keyword
  * we switch back to compat mode after.*/
<LEX_STATE_MSCGEN_COMPAT_BOX>{
    --       BEGIN(LEX_STATE_MSCGEN_COMPAT); yylval_param->arcsymbol = EArcSymbol::BOX_SOLID;       return TOK_EMPH;       // --
    \+\+     BEGIN(LEX_STATE_MSCGEN_COMPAT); yylval_param->arcsymbol = EArcSymbol::BOX_DASHED;      return TOK_EMPH_PLUS_PLUS;// ++
    \.\.     BEGIN(LEX_STATE_MSCGEN_COMPAT); yylval_param->arcsymbol = EArcSymbol::BOX_DOTTED;      return TOK_EMPH;       // ..
    ==       BEGIN(LEX_STATE_MSCGEN_COMPAT); yylval_param->arcsymbol = EArcSymbol::BOX_DOUBLE;      return TOK_EMPH;       // ==
}
 /* The mscgen compatibility mode of interpreting these. */
<LEX_STATE_MSCGEN_COMPAT>{
    --       yylval_param->arcsymbol = EArcSymbol::ARC_SOLID_BIDIR;  return TOK_REL_MSCGEN; // --
    \:\:     yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE2_BIDIR;return TOK_REL_MSCGEN; // ::
    \.\.     yylval_param->arcsymbol = EArcSymbol::ARC_DOTTED_BIDIR; return TOK_REL_MSCGEN; // ..
    ==       yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE_BIDIR; return TOK_REL_MSCGEN; // ==
    \:\>     yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE2;      return TOK_REL_TO;     // :>
    \<\:     yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE2;      return TOK_REL_FROM;   // <:
    \<\:\>   yylval_param->arcsymbol = EArcSymbol::ARC_DOUBLE2_BIDIR;return TOK_REL_BIDIR;  // <:>
}

\+=      return TOK_PLUS_EQUAL;
-        return TOK_DASH;
\+       return TOK_PLUS;
=        return TOK_EQUAL;
\~       return TOK_TILDE;
 /* For these we switch back form box mode to mscgen mode */
,        if (YY_START == LEX_STATE_MSCGEN_COMPAT_BOX) BEGIN(LEX_STATE_MSCGEN_COMPAT); return TOK_COMMA;
\;       if (YY_START == LEX_STATE_MSCGEN_COMPAT_BOX) BEGIN(LEX_STATE_MSCGEN_COMPAT); return TOK_SEMICOLON;
\[       if (YY_START == LEX_STATE_MSCGEN_COMPAT_BOX) BEGIN(LEX_STATE_MSCGEN_COMPAT); return TOK_OSBRACKET;
\]       return TOK_CSBRACKET;
\(       return TOK_OPARENTHESIS;
\)       return TOK_CPARENTHESIS;
\*       return TOK_ASTERISK;
\|       return TOK_PIPE_SYMBOL;

\{       %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->input_text_ptr = nullptr;
  #else
    yylval_param->input_text_ptr = yyget_extra(yyscanner)->buffs.back().GetInputPosFrom_yytext(yytext);
  #endif
    return TOK_OCBRACKET;
%}

\}       %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->input_text_ptr = nullptr;
  #else
    yylval_param->input_text_ptr = yyget_extra(yyscanner)->buffs.back().GetInputPosFrom_yytext(yytext);
  #endif
    return TOK_CCBRACKET;
%}


 /* We need to list only those style names, which are not conforming to
  * TOK_STRING above. */
pipe--        yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
pipe\+\+      yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
pipe==        yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
pipe\.\.      yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
block-\>      yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
block\>       yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
block\>\>     yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
block=\>      yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
vertical--    yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
vertical\+\+  yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
vertical==    yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
vertical\.\.  yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
vertical-\>   yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
vertical\>\>  yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
vertical\>    yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;
vertical=\>   yylval_param->str=strdup(yytext); return TOK_STYLE_NAME;


 /* This is a colon-quoted string, finished by a quotation mark (UTF-8 allowed)
 ** : "<string>"
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*\" %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    {
    /* after whitespaces we are guaranteed to have a tailing and heading quot */
    char *s = remove_head_tail_whitespace(yytext+1);
    /* s now points to the heading quotation marks.
    ** Now get rid of both quotation marks */
    std::string str(s+1);
    str.erase(str.length()-1);
    /* Calculate the position of the string and prepend a location escape */
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s+1 - yytext));
    yylval_param->str = strdup((pos.Print() + str).c_str());
    }
  #endif
    return TOK_COLON_QUOTED_STRING;
%}

 /* This is a colon-quoted string, finished by a newline (trailing context) (UTF-8 allowed)
 ** : "<string>$
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*  %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
  #else
    {
    /* after whitespaces we are guaranteed to have a heading quot */
    const char *s = remove_head_tail_whitespace(yytext+1);
    // s now points to heading quotation mark
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s - yytext));
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    /* Advance pos beyond the leading quotation mark */
    pos.col++;
    yylval_param->str = strdup((pos.Print() + (s+1)).c_str());
    }
  #endif
    return TOK_COLON_QUOTED_STRING;
%}

 /* This is a non quoted colon-string
 ** : <string>
 ** terminated by any of: [ { or ;
 ** Honors escaping of the above via a backslash. (UTF-8 allowed)
 ** Can contain quotation marks (escaped or unescaped), but can not start with it
 ** If it contains a hashmark, unescaped [ { or ; is allowed till the end of the line
 ** (representing a commented section inside a label)
 ** Not available in mscgen compatibility mode. There we use the one below
 *  \:[\t]*(((#[^\x0d\x0a]*)|[^\"\;\[\{\\]|(\\.))((#[^\x0d\x0a]*)|[^\;\[\{\\]|(\\.))*(\\)?|\\)
 * \:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*
 */
<INITIAL>\:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc);
  #endif
    return TOK_COLON_STRING;
%}

 /* This is a non quoted colon-string, where no '>' or ':' follows the initial colon
 ** : <string>
 ** terminated by any of: [ { or ;
 ** Honors escaping of the above via a backslash. (UTF-8 allowed)
 ** Can contain quotation marks (escaped or unescaped), but can not start with it
 ** If it contains a hashmark, unescaped [ { or ; is allowed till the end of the line
 ** (representing a commented section inside a label)
 ** Used only in mscgen compatibility mode to separate from :> and :: arrow symbols.
 *  \:[\t]*(((#[^\x0d\x0a]*)|[^\"\;\[\{\\]|(\\.))((#[^\x0d\x0a]*)|[^\;\[\{\\]|(\\.))*(\\)?|\\)
 * \:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*
 */
<LEX_STATE_MSCGEN_COMPAT>\:(([ \t]*#[^\x0d\x0a]*|[ \t]+[^\"\;\{\[\\#\ \t]|[ \t]*(\\[^\x0d\x0a])|[^\"\;\{\[\\#\ \t\>\:]))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc);
  #endif
    return TOK_COLON_STRING;
%}

 /* This is a degenerate non quoted colon-string
 ** a colon followed by a solo escape or just a colon
 */
\:[ \t]*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
   #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc);
  #endif
    return TOK_COLON_STRING;
%}


 /* A simple quoted string, that can have escaped quotation marks inside. (UTF-8 allowed)*/
\"([^\\\"\x0d\x0a]|(\\\")|\\\\|\\[^\\\"\x0d\x0a])*\" %{
    yylval_param->str = strdup(yytext+1);
    yylval_param->str[strlen(yylval_param->str) - 1] = '\0';
    return TOK_QSTRING;
%}

 /* A simple quoted string, missing a closing quotation mark (UTF-8 allowed)*/
\"([^\\\"\x0d\x0a]|(\\\")|\\\\|\\[^\\\"\x0d\x0a])* %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext+1);
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
#else
    {
    yylval_param->str = strdup(yytext+1);
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column);
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    }
  #endif
    return TOK_QSTRING;
%}

 /* A simple quoted string with line breaks inside (UTF-8 allowed)
  *  Just for msc-gen compatibility */
<LEX_STATE_MSCGEN_COMPAT>\"([^\\\"]|(\\\")|\\\\|\\[^\\\"])*\" %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext+1);
    yylval_param->str[strlen(yylval_param->str) - 1] = '\0';
  #else
    yylval_param->str = process_multiline_qstring(yytext, CHART_POS_START(*yylloc), yylloc);
  #endif
    return TOK_QSTRING;
%}


 /* Numbers */
[+\-]?[0-9]+\.?[0-9]*  %{
    yylval_param->str = strdup(yytext);
    return TOK_NUMBER;
%}

 /* Parameter names: $ followed by a string not ending with a dot. We allow any non ASCII UTF-8 character through.
  * A single '$" also matches.*/
\$[A-Za-z_\x80-\xff]?([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])* %{
    yylval_param->str = strdup(yytext);
    return TOK_PARAM_NAME;
%}

 /* Parameter names: $$ representing a value unique to the actual invocation.*/
($$) %{
    yylval_param->str = strdup(yytext);
    return TOK_PARAM_NAME;
%}


 /* Strings not ending with a dot. We allow any non ASCII UTF-8 character through. */
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])* %{
    yylval_param->str = strdup(yytext);
    return TOK_STRING;
%}

 /* Strings ending with a dot, not followed by a second dot .
  * We allow any non ASCII UTF-8 character through.
  * Two dots one after another shall be parsed a '..' into TOK_EMPH*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*\./[^\.] %{
    yylval_param->str = strdup(yytext);
    return TOK_STRING;
%}

 /* Color definitions. We allow any non ASCII UTF-8 character in the color name. */
 /* string+-number*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*(\.)?[\+\-][0-9]+(\.[0-9]*)? %{
    yylval_param->str = strdup(yytext);
    return TOK_COLORDEF_NAME_NUMBER;
%}

 /* string+-number,number*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*(\.)?[\+\-][0-9]+(\.[0-9]*)?\,[0-9]+(\.[0-9]*)? %{
    yylval_param->str = strdup(yytext);
    return TOK_COLORDEF;
%}

 /* string,number[+-number]. We allow any non ASCII UTF-8 character in 'string'.*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*(\.)?\,[0-9]+(\.[0-9]*)?([\+\-][0-9]+(\.[0-9]*)?)? %{
    yylval_param->str = strdup(yytext);
    return TOK_COLORDEF;
%}

 /* number,number,number[,number] */
[0-9]+(\.[0-9]*)?\,[0-9]+(\.[0-9]*)?\,[0-9]+(\.[0-9]*)?(\,[0-9]+(\.[0-9]*)?)? %{
    yylval_param->str = strdup(yytext);
    return TOK_COLORDEF;
%}

<<EOF>> %{
  #ifdef C_S_H_IS_COMPILED
    return TOK_EOF;
  #else
    {
    auto &pp = *YYGET_EXTRA(yyscanner);
    if (pp.buffs.size()==0) {
        yyterminate();
        return TOK_EOF;
    }
    pp.buffs.pop_back();
    if (pp.buffs.size()==0) {
        yyterminate();
        return TOK_EOF;
    }
    //else just continue scanning the buffer above
    yypop_buffer_state(yyscanner);
    pp.pos_stack.Pop();
    yylloc->last_line = pp.pos_stack.line;
    yylloc->last_column = pp.pos_stack.col-1;
    }
  #endif
%}

 /* Ignore (terminating) zero */
\0

 /* Save any unrecognized character in 'condition' for debugging' */
. yylval->condition = yytext[0]; return TOK_UNRECOGNIZED_CHAR;

%%

/* END OF FILE */
