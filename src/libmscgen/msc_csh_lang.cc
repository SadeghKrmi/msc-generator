#define COLOR_SYNTAX_HIGHLIGHT 1 /* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         msccsh_parse
#define yylex           msccsh_lex
#define yyerror         msccsh_error
#define yydebug         msccsh_debug
#define yynerrs         msccsh_nerrs

/* First part of user prologue.  */
#line 15 "msc_lang.yy"

/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT //so that subsequent definition in msc_csh_lang2.h causes no warning
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #define YYMSC_RESULT_TYPE MscCsh
    #define RESULT csh
    #include "msccsh.h"
    #include "cgen_shapes.h"
    #define YYGET_EXTRA msccsh_get_extra
    #define YYLTYPE_IS_DECLARED
    #define YYLTYPE CshPos
    #define CHAR_IF_CSH(A) char
#else
    #define C_S_H (0)
    #define YYMSC_RESULT_TYPE MscChart
    #define RESULT chart
    #define YYGET_EXTRA msc_get_extra
    #define CHAR_IF_CSH(A) A
#endif

#include "commands.h"//For AttrNames and AttrValues
#include "msc.h" //For AttrNames and AttrValues

using namespace msc;


#line 123 "msc_csh_lang.cc"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "msc_csh_lang.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* TOK_EOF  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_TOK_STRING = 3,                 /* TOK_STRING  */
  YYSYMBOL_TOK_QSTRING = 4,                /* TOK_QSTRING  */
  YYSYMBOL_TOK_NUMBER = 5,                 /* TOK_NUMBER  */
  YYSYMBOL_TOK_DASH = 6,                   /* TOK_DASH  */
  YYSYMBOL_TOK_EQUAL = 7,                  /* TOK_EQUAL  */
  YYSYMBOL_TOK_COMMA = 8,                  /* TOK_COMMA  */
  YYSYMBOL_TOK_TILDE = 9,                  /* TOK_TILDE  */
  YYSYMBOL_TOK_SEMICOLON = 10,             /* TOK_SEMICOLON  */
  YYSYMBOL_TOK_PLUS = 11,                  /* TOK_PLUS  */
  YYSYMBOL_TOK_PLUS_EQUAL = 12,            /* TOK_PLUS_EQUAL  */
  YYSYMBOL_TOK_PIPE_SYMBOL = 13,           /* TOK_PIPE_SYMBOL  */
  YYSYMBOL_TOK_OCBRACKET = 14,             /* TOK_OCBRACKET  */
  YYSYMBOL_TOK_CCBRACKET = 15,             /* TOK_CCBRACKET  */
  YYSYMBOL_TOK_OSBRACKET = 16,             /* TOK_OSBRACKET  */
  YYSYMBOL_TOK_CSBRACKET = 17,             /* TOK_CSBRACKET  */
  YYSYMBOL_TOK_OPARENTHESIS = 18,          /* TOK_OPARENTHESIS  */
  YYSYMBOL_TOK_CPARENTHESIS = 19,          /* TOK_CPARENTHESIS  */
  YYSYMBOL_TOK_PARAM_NAME = 20,            /* TOK_PARAM_NAME  */
  YYSYMBOL_TOK_ASTERISK = 21,              /* TOK_ASTERISK  */
  YYSYMBOL_TOK_MSC = 22,                   /* TOK_MSC  */
  YYSYMBOL_TOK_COLON_STRING = 23,          /* TOK_COLON_STRING  */
  YYSYMBOL_TOK_COLON_QUOTED_STRING = 24,   /* TOK_COLON_QUOTED_STRING  */
  YYSYMBOL_TOK_STYLE_NAME = 25,            /* TOK_STYLE_NAME  */
  YYSYMBOL_TOK_COLORDEF = 26,              /* TOK_COLORDEF  */
  YYSYMBOL_TOK_COLORDEF_NAME_NUMBER = 27,  /* TOK_COLORDEF_NAME_NUMBER  */
  YYSYMBOL_TOK_REL_TO = 28,                /* TOK_REL_TO  */
  YYSYMBOL_TOK_REL_FROM = 29,              /* TOK_REL_FROM  */
  YYSYMBOL_TOK_REL_BIDIR = 30,             /* TOK_REL_BIDIR  */
  YYSYMBOL_TOK_REL_X = 31,                 /* TOK_REL_X  */
  YYSYMBOL_TOK_REL_DASH_X = 32,            /* TOK_REL_DASH_X  */
  YYSYMBOL_TOK_REL_MSCGEN = 33,            /* TOK_REL_MSCGEN  */
  YYSYMBOL_TOK_SPECIAL_ARC = 34,           /* TOK_SPECIAL_ARC  */
  YYSYMBOL_TOK_EMPH = 35,                  /* TOK_EMPH  */
  YYSYMBOL_TOK_EMPH_PLUS_PLUS = 36,        /* TOK_EMPH_PLUS_PLUS  */
  YYSYMBOL_TOK_COMMAND_HEADING = 37,       /* TOK_COMMAND_HEADING  */
  YYSYMBOL_TOK_COMMAND_NUDGE = 38,         /* TOK_COMMAND_NUDGE  */
  YYSYMBOL_TOK_COMMAND_NEWPAGE = 39,       /* TOK_COMMAND_NEWPAGE  */
  YYSYMBOL_TOK_COMMAND_DEFSHAPE = 40,      /* TOK_COMMAND_DEFSHAPE  */
  YYSYMBOL_TOK_COMMAND_DEFCOLOR = 41,      /* TOK_COMMAND_DEFCOLOR  */
  YYSYMBOL_TOK_COMMAND_DEFSTYLE = 42,      /* TOK_COMMAND_DEFSTYLE  */
  YYSYMBOL_TOK_COMMAND_DEFDESIGN = 43,     /* TOK_COMMAND_DEFDESIGN  */
  YYSYMBOL_TOK_COMMAND_DEFPROC = 44,       /* TOK_COMMAND_DEFPROC  */
  YYSYMBOL_TOK_COMMAND_REPLAY = 45,        /* TOK_COMMAND_REPLAY  */
  YYSYMBOL_TOK_COMMAND_SET = 46,           /* TOK_COMMAND_SET  */
  YYSYMBOL_TOK_COMMAND_INCLUDE = 47,       /* TOK_COMMAND_INCLUDE  */
  YYSYMBOL_TOK_COMMAND_BIG = 48,           /* TOK_COMMAND_BIG  */
  YYSYMBOL_TOK_COMMAND_BOX = 49,           /* TOK_COMMAND_BOX  */
  YYSYMBOL_TOK_COMMAND_PIPE = 50,          /* TOK_COMMAND_PIPE  */
  YYSYMBOL_TOK_COMMAND_MARK = 51,          /* TOK_COMMAND_MARK  */
  YYSYMBOL_TOK_COMMAND_MARK_SRC = 52,      /* TOK_COMMAND_MARK_SRC  */
  YYSYMBOL_TOK_COMMAND_MARK_DST = 53,      /* TOK_COMMAND_MARK_DST  */
  YYSYMBOL_TOK_COMMAND_PARALLEL = 54,      /* TOK_COMMAND_PARALLEL  */
  YYSYMBOL_TOK_COMMAND_OVERLAP = 55,       /* TOK_COMMAND_OVERLAP  */
  YYSYMBOL_TOK_COMMAND_INLINE = 56,        /* TOK_COMMAND_INLINE  */
  YYSYMBOL_TOK_VERTICAL = 57,              /* TOK_VERTICAL  */
  YYSYMBOL_TOK_VERTICAL_SHAPE = 58,        /* TOK_VERTICAL_SHAPE  */
  YYSYMBOL_TOK_AT = 59,                    /* TOK_AT  */
  YYSYMBOL_TOK_LOST = 60,                  /* TOK_LOST  */
  YYSYMBOL_TOK_AT_POS = 61,                /* TOK_AT_POS  */
  YYSYMBOL_TOK_SHOW = 62,                  /* TOK_SHOW  */
  YYSYMBOL_TOK_HIDE = 63,                  /* TOK_HIDE  */
  YYSYMBOL_TOK_ACTIVATE = 64,              /* TOK_ACTIVATE  */
  YYSYMBOL_TOK_DEACTIVATE = 65,            /* TOK_DEACTIVATE  */
  YYSYMBOL_TOK_BYE = 66,                   /* TOK_BYE  */
  YYSYMBOL_TOK_START = 67,                 /* TOK_START  */
  YYSYMBOL_TOK_BEFORE = 68,                /* TOK_BEFORE  */
  YYSYMBOL_TOK_END = 69,                   /* TOK_END  */
  YYSYMBOL_TOK_AFTER = 70,                 /* TOK_AFTER  */
  YYSYMBOL_TOK_COMMAND_VSPACE = 71,        /* TOK_COMMAND_VSPACE  */
  YYSYMBOL_TOK_COMMAND_HSPACE = 72,        /* TOK_COMMAND_HSPACE  */
  YYSYMBOL_TOK_COMMAND_SYMBOL = 73,        /* TOK_COMMAND_SYMBOL  */
  YYSYMBOL_TOK_COMMAND_NOTE = 74,          /* TOK_COMMAND_NOTE  */
  YYSYMBOL_TOK_COMMAND_COMMENT = 75,       /* TOK_COMMAND_COMMENT  */
  YYSYMBOL_TOK_COMMAND_ENDNOTE = 76,       /* TOK_COMMAND_ENDNOTE  */
  YYSYMBOL_TOK_COMMAND_FOOTNOTE = 77,      /* TOK_COMMAND_FOOTNOTE  */
  YYSYMBOL_TOK_COMMAND_TITLE = 78,         /* TOK_COMMAND_TITLE  */
  YYSYMBOL_TOK_COMMAND_SUBTITLE = 79,      /* TOK_COMMAND_SUBTITLE  */
  YYSYMBOL_TOK_COMMAND_TEXT = 80,          /* TOK_COMMAND_TEXT  */
  YYSYMBOL_TOK_SHAPE_COMMAND = 81,         /* TOK_SHAPE_COMMAND  */
  YYSYMBOL_TOK_JOIN = 82,                  /* TOK_JOIN  */
  YYSYMBOL_TOK_IF = 83,                    /* TOK_IF  */
  YYSYMBOL_TOK_THEN = 84,                  /* TOK_THEN  */
  YYSYMBOL_TOK_ELSE = 85,                  /* TOK_ELSE  */
  YYSYMBOL_TOK_MSCGEN_RBOX = 86,           /* TOK_MSCGEN_RBOX  */
  YYSYMBOL_TOK_MSCGEN_ABOX = 87,           /* TOK_MSCGEN_ABOX  */
  YYSYMBOL_TOK_UNRECOGNIZED_CHAR = 88,     /* TOK_UNRECOGNIZED_CHAR  */
  YYSYMBOL_YYACCEPT = 89,                  /* $accept  */
  YYSYMBOL_msc_with_bye = 90,              /* msc_with_bye  */
  YYSYMBOL_eof = 91,                       /* eof  */
  YYSYMBOL_msc = 92,                       /* msc  */
  YYSYMBOL_top_level_arclist = 93,         /* top_level_arclist  */
  YYSYMBOL_msckey = 94,                    /* msckey  */
  YYSYMBOL_braced_arclist = 95,            /* braced_arclist  */
  YYSYMBOL_defproc = 96,                   /* defproc  */
  YYSYMBOL_defprochelp1 = 97,              /* defprochelp1  */
  YYSYMBOL_defprochelp2 = 98,              /* defprochelp2  */
  YYSYMBOL_defprochelp3 = 99,              /* defprochelp3  */
  YYSYMBOL_defprochelp4 = 100,             /* defprochelp4  */
  YYSYMBOL_scope_open_proc_body = 101,     /* scope_open_proc_body  */
  YYSYMBOL_scope_close_proc_body = 102,    /* scope_close_proc_body  */
  YYSYMBOL_proc_def_arglist_tested = 103,  /* proc_def_arglist_tested  */
  YYSYMBOL_proc_def_arglist = 104,         /* proc_def_arglist  */
  YYSYMBOL_proc_def_param_list = 105,      /* proc_def_param_list  */
  YYSYMBOL_proc_def_param = 106,           /* proc_def_param  */
  YYSYMBOL_procedure_body = 107,           /* procedure_body  */
  YYSYMBOL_set = 108,                      /* set  */
  YYSYMBOL_arclist_maybe_no_semicolon = 109, /* arclist_maybe_no_semicolon  */
  YYSYMBOL_arclist = 110,                  /* arclist  */
  YYSYMBOL_arc_with_parallel_semicolon = 111, /* arc_with_parallel_semicolon  */
  YYSYMBOL_proc_invocation = 112,          /* proc_invocation  */
  YYSYMBOL_proc_param_list = 113,          /* proc_param_list  */
  YYSYMBOL_proc_invoc_param_list = 114,    /* proc_invoc_param_list  */
  YYSYMBOL_proc_invoc_param = 115,         /* proc_invoc_param  */
  YYSYMBOL_include = 116,                  /* include  */
  YYSYMBOL_overlap_or_parallel = 117,      /* overlap_or_parallel  */
  YYSYMBOL_arc_with_parallel = 118,        /* arc_with_parallel  */
  YYSYMBOL_arc = 119,                      /* arc  */
  YYSYMBOL_titlecommandtoken = 120,        /* titlecommandtoken  */
  YYSYMBOL_hspace_location = 121,          /* hspace_location  */
  YYSYMBOL_comp = 122,                     /* comp  */
  YYSYMBOL_condition = 123,                /* condition  */
  YYSYMBOL_ifthen_condition = 124,         /* ifthen_condition  */
  YYSYMBOL_else = 125,                     /* else  */
  YYSYMBOL_ifthen = 126,                   /* ifthen  */
  YYSYMBOL_full_arcattrlist_with_label_or_number = 127, /* full_arcattrlist_with_label_or_number  */
  YYSYMBOL_dash_or_dashdash = 128,         /* dash_or_dashdash  */
  YYSYMBOL_entityrel = 129,                /* entityrel  */
  YYSYMBOL_markerrel_no_string = 130,      /* markerrel_no_string  */
  YYSYMBOL_entity_command_prefixes = 131,  /* entity_command_prefixes  */
  YYSYMBOL_optlist = 132,                  /* optlist  */
  YYSYMBOL_opt = 133,                      /* opt  */
  YYSYMBOL_entitylist = 134,               /* entitylist  */
  YYSYMBOL_entity = 135,                   /* entity  */
  YYSYMBOL_first_entity = 136,             /* first_entity  */
  YYSYMBOL_styledeflist = 137,             /* styledeflist  */
  YYSYMBOL_styledef = 138,                 /* styledef  */
  YYSYMBOL_stylenamelist = 139,            /* stylenamelist  */
  YYSYMBOL_shapedef = 140,                 /* shapedef  */
  YYSYMBOL_shapedeflist = 141,             /* shapedeflist  */
  YYSYMBOL_shapeline = 142,                /* shapeline  */
  YYSYMBOL_colordeflist = 143,             /* colordeflist  */
  YYSYMBOL_color_def_string = 144,         /* color_def_string  */
  YYSYMBOL_color_string = 145,             /* color_string  */
  YYSYMBOL_colordef = 146,                 /* colordef  */
  YYSYMBOL_designdef = 147,                /* designdef  */
  YYSYMBOL_scope_open_empty = 148,         /* scope_open_empty  */
  YYSYMBOL_designelementlist = 149,        /* designelementlist  */
  YYSYMBOL_designelement = 150,            /* designelement  */
  YYSYMBOL_designoptlist = 151,            /* designoptlist  */
  YYSYMBOL_designopt = 152,                /* designopt  */
  YYSYMBOL_parallel = 153,                 /* parallel  */
  YYSYMBOL_optional_box_keyword = 154,     /* optional_box_keyword  */
  YYSYMBOL_box_list = 155,                 /* box_list  */
  YYSYMBOL_mscgen_box = 156,               /* mscgen_box  */
  YYSYMBOL_mscgen_boxlist = 157,           /* mscgen_boxlist  */
  YYSYMBOL_first_box = 158,                /* first_box  */
  YYSYMBOL_first_pipe = 159,               /* first_pipe  */
  YYSYMBOL_pipe_list_no_content = 160,     /* pipe_list_no_content  */
  YYSYMBOL_pipe_list = 161,                /* pipe_list  */
  YYSYMBOL_emphrel = 162,                  /* emphrel  */
  YYSYMBOL_boxrel = 163,                   /* boxrel  */
  YYSYMBOL_mscgen_emphrel = 164,           /* mscgen_emphrel  */
  YYSYMBOL_mscgen_boxrel = 165,            /* mscgen_boxrel  */
  YYSYMBOL_vertxpos = 166,                 /* vertxpos  */
  YYSYMBOL_empharcrel_straight = 167,      /* empharcrel_straight  */
  YYSYMBOL_vertrel_no_xpos = 168,          /* vertrel_no_xpos  */
  YYSYMBOL_vertrel = 169,                  /* vertrel  */
  YYSYMBOL_arcrel = 170,                   /* arcrel  */
  YYSYMBOL_arrow_with_specifier_incomplete = 171, /* arrow_with_specifier_incomplete  */
  YYSYMBOL_arrow_with_specifier = 172,     /* arrow_with_specifier  */
  YYSYMBOL_special_ending = 173,           /* special_ending  */
  YYSYMBOL_arcrel_arrow = 174,             /* arcrel_arrow  */
  YYSYMBOL_arcrel_to = 175,                /* arcrel_to  */
  YYSYMBOL_arcrel_from = 176,              /* arcrel_from  */
  YYSYMBOL_arcrel_bidir = 177,             /* arcrel_bidir  */
  YYSYMBOL_relation_to_cont_no_loss = 178, /* relation_to_cont_no_loss  */
  YYSYMBOL_relation_from_cont_no_loss = 179, /* relation_from_cont_no_loss  */
  YYSYMBOL_relation_bidir_cont_no_loss = 180, /* relation_bidir_cont_no_loss  */
  YYSYMBOL_relation_to = 181,              /* relation_to  */
  YYSYMBOL_relation_from = 182,            /* relation_from  */
  YYSYMBOL_relation_bidir = 183,           /* relation_bidir  */
  YYSYMBOL_relation_to_cont = 184,         /* relation_to_cont  */
  YYSYMBOL_relation_from_cont = 185,       /* relation_from_cont  */
  YYSYMBOL_relation_bidir_cont = 186,      /* relation_bidir_cont  */
  YYSYMBOL_extvertxpos = 187,              /* extvertxpos  */
  YYSYMBOL_extvertxpos_no_string = 188,    /* extvertxpos_no_string  */
  YYSYMBOL_symbol_type_string = 189,       /* symbol_type_string  */
  YYSYMBOL_symbol_command_no_attr = 190,   /* symbol_command_no_attr  */
  YYSYMBOL_symbol_command = 191,           /* symbol_command  */
  YYSYMBOL_note = 192,                     /* note  */
  YYSYMBOL_comment_command = 193,          /* comment_command  */
  YYSYMBOL_comment = 194,                  /* comment  */
  YYSYMBOL_colon_string = 195,             /* colon_string  */
  YYSYMBOL_full_arcattrlist_with_label = 196, /* full_arcattrlist_with_label  */
  YYSYMBOL_full_arcattrlist = 197,         /* full_arcattrlist  */
  YYSYMBOL_arcattrlist = 198,              /* arcattrlist  */
  YYSYMBOL_arcattr = 199,                  /* arcattr  */
  YYSYMBOL_vertical_shape = 200,           /* vertical_shape  */
  YYSYMBOL_entity_string_single = 201,     /* entity_string_single  */
  YYSYMBOL_reserved_word_string = 202,     /* reserved_word_string  */
  YYSYMBOL_symbol_string = 203,            /* symbol_string  */
  YYSYMBOL_alpha_string_single = 204,      /* alpha_string_single  */
  YYSYMBOL_string_single = 205,            /* string_single  */
  YYSYMBOL_tok_param_name_as_multi = 206,  /* tok_param_name_as_multi  */
  YYSYMBOL_multi_string_continuation = 207, /* multi_string_continuation  */
  YYSYMBOL_entity_string_single_or_param = 208, /* entity_string_single_or_param  */
  YYSYMBOL_entity_string = 209,            /* entity_string  */
  YYSYMBOL_alpha_string = 210,             /* alpha_string  */
  YYSYMBOL_string = 211,                   /* string  */
  YYSYMBOL_scope_open = 212,               /* scope_open  */
  YYSYMBOL_scope_close = 213               /* scope_close  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;


/* Second part of user prologue.  */
#line 208 "msc_lang.yy"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <iostream>

#ifdef C_S_H_IS_COMPILED
    #include "msc_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "msc_csh_lang2.h"  //Needs parse_param from msc_lang_misc.h
    /* yyerror
     *  Error handling function.  Do nothing for CSH */
    void yyerror(YYLTYPE* /*loc*/, Csh & /*csh*/, void * /*yyscanner*/, const char * /*str*/) {}
#else
    #include "msc_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "msc_lang2.h"      //Needs parse_param from msc_lang_misc.h
    /* Use verbose error reporting such that the expected token names are dumped */
    //#define YYERROR_VERBOSE
    #include "msc_parse_tools.h" //includes yyerror()
#endif

#ifdef C_S_H_IS_COMPILED
void MscCshParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#else
void MscParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#endif
{
    std::remove_pointer<YY_EXTRA_TYPE>::type pp;
    pp.RESULT = &RESULT;
#ifdef C_S_H_IS_COMPILED
    pp.buff.buf = buff;
    pp.buff.length = len;
    pp.buff.pos = 0;
    msccsh_lex_init(&pp.yyscanner);
    msccsh_set_extra(&pp, pp.yyscanner);
    msccsh_parse(RESULT, pp.yyscanner);
    msccsh_lex_destroy(pp.yyscanner);
#else
    pp.buffs.emplace_back(buff, len);
    pp.pos_stack.file = RESULT.current_file;
    msc_lex_init(&pp.yyscanner);
    msc_set_extra(&pp, pp.yyscanner);
    msc_parse(RESULT, pp.yyscanner);
    msc_lex_destroy(pp.yyscanner);
#endif
}


#line 419 "msc_csh_lang.cc"


#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  274
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3696

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  89
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  125
/* YYNRULES -- Number of rules.  */
#define YYNRULES  600
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  656

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   343


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   259,   259,   264,   265,   274,   286,   296,   315,   330,
     342,   360,   361,   371,   386,   396,   409,   433,   442,   454,
     466,   478,   490,   501,   514,   528,   567,   586,   604,   616,
     627,   637,   646,   654,   663,   678,   694,   705,   721,   732,
     756,   765,   776,   786,   798,   809,   819,   830,   841,   857,
     875,   894,   916,   940,   954,   967,   985,  1004,  1023,  1039,
    1059,  1077,  1091,  1103,  1104,  1119,  1136,  1145,  1153,  1168,
    1181,  1199,  1216,  1240,  1259,  1282,  1308,  1328,  1346,  1362,
    1384,  1395,  1422,  1431,  1442,  1452,  1464,  1475,  1485,  1496,
    1505,  1518,  1528,  1544,  1562,  1573,  1586,  1601,  1601,  1601,
    1603,  1604,  1631,  1648,  1657,  1670,  1686,  1705,  1727,  1747,
    1769,  1790,  1821,  1846,  1872,  1907,  1925,  1937,  1952,  1972,
    1987,  1995,  2012,  2028,  2042,  2056,  2077,  2096,  2107,  2118,
    2125,  2132,  2136,  2155,  2179,  2195,  2213,  2229,  2247,  2264,
    2287,  2294,  2301,  2315,  2332,  2346,  2363,  2383,  2398,  2416,
    2439,  2462,  2479,  2499,  2521,  2547,  2572,  2588,  2609,  2623,
    2640,  2641,  2642,  2643,  2660,  2682,  2701,  2723,  2740,  2762,
    2771,  2771,  2773,  2774,  2800,  2822,  2839,  2843,  2847,  2851,
    2855,  2860,  2868,  2881,  2904,  2920,  2948,  2969,  2983,  2997,
    3018,  3049,  3070,  3084,  3096,  3110,  3126,  3140,  3168,  3184,
    3201,  3202,  3213,  3224,  3224,  3226,  3242,  3257,  3266,  3285,
    3297,  3312,  3326,  3336,  3354,  3354,  3354,  3354,  3356,  3366,
    3385,  3398,  3413,  3436,  3461,  3481,  3509,  3528,  3556,  3576,
    3582,  3594,  3607,  3630,  3648,  3672,  3692,  3716,  3736,  3761,
    3783,  3784,  3794,  3807,  3825,  3836,  3860,  3875,  3891,  3916,
    3934,  3946,  3961,  3983,  4004,  4027,  4039,  4052,  4066,  4081,
    4093,  4106,  4115,  4125,  4141,  4169,  4193,  4210,  4228,  4257,
    4285,  4315,  4348,  4349,  4360,  4375,  4375,  4377,  4377,  4378,
    4388,  4416,  4445,  4466,  4485,  4525,  4568,  4580,  4581,  4592,
    4607,  4625,  4640,  4658,  4660,  4661,  4671,  4681,  4688,  4709,
    4731,  4749,  4775,  4801,  4818,  4837,  4846,  4862,  4873,  4890,
    4907,  4907,  4918,  4926,  4944,  4960,  4980,  5000,  5016,  5023,
    5037,  5046,  5060,  5074,  5093,  5103,  5119,  5129,  5148,  5155,
    5167,  5175,  5190,  5208,  5226,  5241,  5263,  5274,  5290,  5312,
    5313,  5322,  5322,  5324,  5343,  5358,  5374,  5384,  5384,  5384,
    5384,  5386,  5419,  5441,  5465,  5489,  5514,  5537,  5562,  5606,
    5633,  5660,  5689,  5718,  5738,  5747,  5757,  5766,  5778,  5786,
    5794,  5803,  5824,  5841,  5859,  5868,  5885,  5896,  5911,  5929,
    5937,  5948,  5955,  5969,  5980,  5999,  6014,  6031,  6052,  6069,
    6086,  6107,  6126,  6136,  6150,  6161,  6173,  6186,  6201,  6225,
    6225,  6225,  6226,  6254,  6275,  6326,  6341,  6372,  6389,  6399,
    6415,  6434,  6449,  6465,  6482,  6502,  6523,  6539,  6554,  6572,
    6591,  6606,  6622,  6639,  6659,  6680,  6696,  6711,  6729,  6748,
    6764,  6779,  6796,  6814,  6834,  6850,  6865,  6882,  6882,  6883,
    6883,  6884,  6884,  6886,  6895,  6904,  6913,  6925,  6934,  6943,
    6952,  6964,  6973,  6982,  6991,  7003,  7011,  7023,  7035,  7050,
    7058,  7070,  7082,  7097,  7105,  7117,  7129,  7146,  7147,  7161,
    7178,  7197,  7197,  7197,  7199,  7227,  7260,  7298,  7323,  7352,
    7374,  7398,  7426,  7447,  7465,  7473,  7487,  7516,  7536,  7556,
    7580,  7587,  7594,  7602,  7618,  7633,  7641,  7650,  7658,  7666,
    7678,  7686,  7689,  7700,  7711,  7723,  7736,  7748,  7760,  7772,
    7785,  7792,  7801,  7813,  7836,  7855,  7875,  7895,  7914,  7931,
    7947,  7958,  7973,  7981,  7993,  8000,  8001,  8002,  8009,  8010,
    8010,  8010,  8011,  8011,  8011,  8011,  8012,  8012,  8012,  8013,
    8013,  8013,  8014,  8014,  8014,  8015,  8015,  8016,  8016,  8016,
    8016,  8016,  8017,  8017,  8017,  8017,  8017,  8018,  8018,  8018,
    8018,  8018,  8019,  8019,  8019,  8020,  8020,  8021,  8021,  8022,
    8022,  8022,  8022,  8022,  8023,  8023,  8023,  8025,  8046,  8068,
    8089,  8100,  8109,  8120,  8122,  8122,  8125,  8158,  8170,  8175,
    8179,  8181,  8185,  8190,  8191,  8195,  8200,  8201,  8205,  8210,
    8239
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "TOK_EOF", "error", "\"invalid token\"", "TOK_STRING", "TOK_QSTRING",
  "TOK_NUMBER", "TOK_DASH", "TOK_EQUAL", "TOK_COMMA", "TOK_TILDE",
  "TOK_SEMICOLON", "TOK_PLUS", "TOK_PLUS_EQUAL", "TOK_PIPE_SYMBOL",
  "TOK_OCBRACKET", "TOK_CCBRACKET", "TOK_OSBRACKET", "TOK_CSBRACKET",
  "TOK_OPARENTHESIS", "TOK_CPARENTHESIS", "TOK_PARAM_NAME", "TOK_ASTERISK",
  "TOK_MSC", "TOK_COLON_STRING", "TOK_COLON_QUOTED_STRING",
  "TOK_STYLE_NAME", "TOK_COLORDEF", "TOK_COLORDEF_NAME_NUMBER",
  "TOK_REL_TO", "TOK_REL_FROM", "TOK_REL_BIDIR", "TOK_REL_X",
  "TOK_REL_DASH_X", "TOK_REL_MSCGEN", "TOK_SPECIAL_ARC", "TOK_EMPH",
  "TOK_EMPH_PLUS_PLUS", "TOK_COMMAND_HEADING", "TOK_COMMAND_NUDGE",
  "TOK_COMMAND_NEWPAGE", "TOK_COMMAND_DEFSHAPE", "TOK_COMMAND_DEFCOLOR",
  "TOK_COMMAND_DEFSTYLE", "TOK_COMMAND_DEFDESIGN", "TOK_COMMAND_DEFPROC",
  "TOK_COMMAND_REPLAY", "TOK_COMMAND_SET", "TOK_COMMAND_INCLUDE",
  "TOK_COMMAND_BIG", "TOK_COMMAND_BOX", "TOK_COMMAND_PIPE",
  "TOK_COMMAND_MARK", "TOK_COMMAND_MARK_SRC", "TOK_COMMAND_MARK_DST",
  "TOK_COMMAND_PARALLEL", "TOK_COMMAND_OVERLAP", "TOK_COMMAND_INLINE",
  "TOK_VERTICAL", "TOK_VERTICAL_SHAPE", "TOK_AT", "TOK_LOST", "TOK_AT_POS",
  "TOK_SHOW", "TOK_HIDE", "TOK_ACTIVATE", "TOK_DEACTIVATE", "TOK_BYE",
  "TOK_START", "TOK_BEFORE", "TOK_END", "TOK_AFTER", "TOK_COMMAND_VSPACE",
  "TOK_COMMAND_HSPACE", "TOK_COMMAND_SYMBOL", "TOK_COMMAND_NOTE",
  "TOK_COMMAND_COMMENT", "TOK_COMMAND_ENDNOTE", "TOK_COMMAND_FOOTNOTE",
  "TOK_COMMAND_TITLE", "TOK_COMMAND_SUBTITLE", "TOK_COMMAND_TEXT",
  "TOK_SHAPE_COMMAND", "TOK_JOIN", "TOK_IF", "TOK_THEN", "TOK_ELSE",
  "TOK_MSCGEN_RBOX", "TOK_MSCGEN_ABOX", "TOK_UNRECOGNIZED_CHAR", "$accept",
  "msc_with_bye", "eof", "msc", "top_level_arclist", "msckey",
  "braced_arclist", "defproc", "defprochelp1", "defprochelp2",
  "defprochelp3", "defprochelp4", "scope_open_proc_body",
  "scope_close_proc_body", "proc_def_arglist_tested", "proc_def_arglist",
  "proc_def_param_list", "proc_def_param", "procedure_body", "set",
  "arclist_maybe_no_semicolon", "arclist", "arc_with_parallel_semicolon",
  "proc_invocation", "proc_param_list", "proc_invoc_param_list",
  "proc_invoc_param", "include", "overlap_or_parallel",
  "arc_with_parallel", "arc", "titlecommandtoken", "hspace_location",
  "comp", "condition", "ifthen_condition", "else", "ifthen",
  "full_arcattrlist_with_label_or_number", "dash_or_dashdash", "entityrel",
  "markerrel_no_string", "entity_command_prefixes", "optlist", "opt",
  "entitylist", "entity", "first_entity", "styledeflist", "styledef",
  "stylenamelist", "shapedef", "shapedeflist", "shapeline", "colordeflist",
  "color_def_string", "color_string", "colordef", "designdef",
  "scope_open_empty", "designelementlist", "designelement",
  "designoptlist", "designopt", "parallel", "optional_box_keyword",
  "box_list", "mscgen_box", "mscgen_boxlist", "first_box", "first_pipe",
  "pipe_list_no_content", "pipe_list", "emphrel", "boxrel",
  "mscgen_emphrel", "mscgen_boxrel", "vertxpos", "empharcrel_straight",
  "vertrel_no_xpos", "vertrel", "arcrel",
  "arrow_with_specifier_incomplete", "arrow_with_specifier",
  "special_ending", "arcrel_arrow", "arcrel_to", "arcrel_from",
  "arcrel_bidir", "relation_to_cont_no_loss", "relation_from_cont_no_loss",
  "relation_bidir_cont_no_loss", "relation_to", "relation_from",
  "relation_bidir", "relation_to_cont", "relation_from_cont",
  "relation_bidir_cont", "extvertxpos", "extvertxpos_no_string",
  "symbol_type_string", "symbol_command_no_attr", "symbol_command", "note",
  "comment_command", "comment", "colon_string",
  "full_arcattrlist_with_label", "full_arcattrlist", "arcattrlist",
  "arcattr", "vertical_shape", "entity_string_single",
  "reserved_word_string", "symbol_string", "alpha_string_single",
  "string_single", "tok_param_name_as_multi", "multi_string_continuation",
  "entity_string_single_or_param", "entity_string", "alpha_string",
  "string", "scope_open", "scope_close", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-433)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-509)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    3171,  -433,  -433,    60,  -433,   426,  -433,   918,  -433,   506,
     266,  -433,  -433,    27,    68,    88,  -433,  -433,  -433,  -433,
     107,   107,   107,    60,  2516,  1496,   137,  2261,  2516,   124,
     143,  3560,  3581,   158,  2346,  -433,  -433,  3478,  -433,  -433,
    -433,  -433,   573,  3552,  3615,   133,  -433,  -433,  -433,  -433,
    -433,   189,  -433,  -433,   830,   175,    41,    30,   172,  -433,
    -433,  -433,  -433,   181,  3252,  -433,  2688,  2772,  3414,   297,
    -433,   270,  1092,  -433,    60,   191,  -433,   198,   316,   187,
    -433,   223,  -433,  -433,   335,  -433,    60,   446,   270,   270,
     435,   757,  -433,   110,   161,    98,    60,    60,    60,   270,
    -433,  -433,   270,  -433,   107,   202,  -433,  -433,   225,  3102,
    2940,  -433,    60,    60,    60,   220,  -433,  -433,  -433,  -433,
    -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,
    -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,
    -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,
    -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,
    -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,
    -433,  -433,  -433,  -433,   549,  -433,  -433,  -433,   225,   225,
    -433,   235,  -433,   241,   255,   281,    41,  1581,  2176,  -433,
    -433,  -433,  -433,  -433,  -433,  -433,   242,   279,  -433,   298,
    -433,   303,  -433,   192,  -433,  -433,   301,  -433,  -433,    52,
    -433,  -433,  -433,  3024,   332,  -433,  -433,   308,   515,  -433,
     346,  -433,  -433,   270,  3135,  -433,  -433,   107,   172,    19,
     211,  -433,   270,  -433,   107,   107,   631,  -433,  -433,  -433,
      60,   333,  -433,    60,   305,   270,  -433,  -433,  -433,  3518,
     603,   270,  -433,  -433,  -433,  -433,   287,  -433,   573,  -433,
      60,  -433,    49,  -433,   487,  -433,  -433,  1241,  -433,   270,
    -433,   272,   285,  1006,  -433,  -433,   363,  -433,    41,  -433,
    -433,  3252,   138,  -433,   527,   365,  -433,   669,  2856,   376,
    -433,  -433,   382,  -433,  -433,   304,    36,   392,   446,   313,
      60,  -433,   172,  -433,  -433,   408,   172,    60,  -433,   270,
    -433,  -433,   172,  -433,  -433,  -433,  -433,   305,    25,   139,
    -433,  -433,    64,  -433,   380,   321,  -433,    72,  -433,   388,
     393,  -433,    70,  -433,   397,   781,  -433,  -433,  -433,  -433,
    -433,  -433,  -433,   107,  2176,  -433,    60,  1666,   413,    60,
      60,  -433,  -433,  -433,  -433,  -433,    60,    60,   796,  3109,
    3142,   172,  -433,  -433,  -433,   254,  -433,  -433,  -433,  -433,
    -433,   404,  1751,  -433,  -433,  -433,  1156,  -433,  -433,  -433,
    -433,   412,  -433,    39,  2516,  1326,  1496,  1836,  -433,  -433,
     615,   411,  -433,   496,  -433,  -433,  -433,  -433,  -433,   433,
    -433,  -433,  -433,  1921,  -433,  -433,  -433,   107,  -433,  -433,
    -433,   410,   379,  -433,  -433,  -433,  -433,   270,  -433,    60,
    -433,  -433,  -433,  -433,    60,   305,    60,   306,  -433,   306,
      49,  -433,   270,  -433,  -433,  -433,  2176,  -433,  -433,  -433,
    -433,  -433,  2176,  -433,  -433,  -433,  2176,  -433,   421,  -433,
    2006,  -433,  2604,  -433,  -433,   431,  -433,  -433,  -433,  -433,
    3333,   304,  3333,    60,  -433,  -433,   445,   463,  -433,   446,
    -433,   446,  -433,  -433,   203,  -433,  -433,  -433,   149,  -433,
     149,  -433,   432,  -433,  -433,  -433,   451,  -433,  -433,  -433,
     454,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,    60,
    -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,
    -433,  -433,    59,  -433,  -433,  -433,  -433,  -433,  -433,  -433,
    1411,  -433,  -433,   395,   473,   407,   475,  -433,  -433,  -433,
    1411,  -433,  -433,  -433,  -433,  -433,   216,  2516,  1496,   476,
    -433,   102,  -433,   488,  -433,   464,   124,  -433,  -433,   164,
    -433,  -433,  -433,  -433,  -433,  -433,  -433,   536,   489,    60,
    -433,  -433,  -433,  -433,  -433,  -433,   306,  -433,  -433,  -433,
      60,  -433,  -433,  -433,  -433,  -433,  -433,   479,  2006,  -433,
    -433,  -433,  3333,  -433,   463,    60,  -433,   172,  -433,   172,
    -433,  -433,   497,  -433,  -433,  -433,  -433,  -433,  -433,  -433,
    -433,   493,   500,   494,  -433,  -433,   498,  -433,  -433,  2176,
    2176,   279,   303,   218,  -433,   572,  2091,  -433,  -433,  -433,
    -433,  -433,   508,  -433,   509,   306,  -433,  -433,  -433,  -433,
    -433,  -433,  -433,  -433,  -433,  2431,  -433,  -433,  -433,  -433,
     502,  -433,  -433,  -433,  -433,  -433,  -433,  -433,  -433,   514,
     519,  -433,   533,  -433,   538,  -433
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       6,   525,   524,     0,    69,     0,   599,     0,   586,     0,
       0,   496,   495,   443,   447,   451,   526,   379,   342,   341,
     142,   144,   158,   133,   135,   137,   139,    26,    80,    62,
      95,   105,   313,     0,   152,    97,    98,   108,   215,   214,
     216,   217,   168,   165,   483,     0,   490,   491,   492,   170,
     171,   148,   527,    99,     0,     0,     0,     0,     0,   305,
     140,    25,   141,    11,    63,    66,     0,     0,   102,     0,
     100,   147,     0,   169,   121,   127,   218,   120,   131,   310,
     326,   128,   312,   336,   339,   130,   346,   328,   324,   103,
       0,     0,   392,   399,   400,   401,   411,   420,   430,   484,
     160,   161,   494,   162,   116,   115,   589,   590,   591,   237,
       0,   408,     0,     0,     0,   509,   502,   528,   585,   577,
     578,   579,   580,   581,   582,   520,   529,   530,   531,   532,
     533,   534,   535,   536,   537,   538,   539,   540,   541,   542,
     543,   544,   545,   546,   547,   548,   549,   550,   551,   552,
     553,   554,   555,   556,   569,   570,   571,   572,   557,   558,
     559,   561,   562,   563,   564,   565,   566,   560,   573,   574,
     575,   576,   567,   568,     0,   510,   583,   584,   594,   597,
     593,   596,   519,   444,   448,   452,     0,   226,   228,   445,
     449,   453,   143,   145,   159,   132,   250,   134,   272,   283,
     246,   136,   240,   244,   596,   245,     0,   138,    37,     0,
      27,    30,    33,     0,    31,    39,    36,    34,    28,    81,
      49,    61,    96,   106,     0,   314,   317,   497,   316,   501,
       0,   333,   332,   155,   156,   153,     0,   523,   522,   521,
     363,   364,   378,   374,   377,   109,   369,   368,   370,   111,
     375,   201,   167,   200,   203,   204,   174,   175,   164,   166,
     207,   172,   209,   473,   482,   471,   472,   488,   487,   151,
     149,   188,   186,     0,     1,     3,     4,     2,     0,     9,
       7,    12,     0,    67,     0,     0,    71,     0,     0,     0,
      77,   101,     0,    68,   146,   193,     0,   122,   237,     0,
     123,   307,   309,   311,   322,     0,     0,     0,   340,   337,
     344,   330,   329,   325,   104,   383,   382,   385,   386,   389,
     384,   438,     0,   437,   455,   417,   440,     0,   439,   459,
     426,   442,     0,   441,   463,   435,   412,   421,   429,   485,
     493,   117,   306,   119,   587,   592,   409,   224,     0,   405,
       0,   349,   350,   347,   348,   239,   345,     0,   414,   423,
     432,   236,    22,   600,    24,     0,    18,   413,   422,   431,
     505,   507,   512,   503,   595,   598,   518,   446,   450,   454,
       8,   225,   227,     0,   274,   282,   242,   247,   243,   286,
       0,     0,    40,     0,    46,    58,    38,    60,    54,     0,
      32,    35,    29,    50,   107,   498,   315,   500,   335,   157,
     154,   365,   352,   366,   372,   376,   112,   110,   114,   373,
     202,   173,   163,   206,   205,   470,   212,   480,   479,   477,
     481,   489,     0,   150,   189,   185,     0,   178,   179,   180,
     177,   176,   182,     5,    10,    13,   226,    72,     0,    94,
      89,    82,     0,    88,    93,     0,    74,    79,    70,   190,
     196,     0,   194,   125,   221,   219,     0,   124,   229,   233,
     308,   318,   323,   327,     0,   338,   331,   393,   388,   387,
     391,   390,   456,   457,   418,   416,   460,   461,   427,   425,
     464,   465,   436,   434,   118,   588,   407,   222,   223,   406,
     403,   402,   343,   351,   415,   410,   424,   419,   433,   428,
     238,    21,     0,    23,    17,   504,   511,   517,   275,   276,
     516,   514,   513,     0,   263,     0,   256,   273,   277,   278,
       0,   280,   279,   241,   249,   248,     0,   290,   292,     0,
     287,     0,   294,     0,    41,     0,    47,    45,    57,     0,
      59,    53,    52,    51,   499,   367,   353,   354,   356,   342,
     358,   113,   371,   208,   469,   211,   474,   467,   468,   478,
     210,   486,   184,   183,   225,    83,    90,     0,    91,    87,
      75,   199,   195,   197,   126,   231,   235,   232,   320,   319,
     398,   394,   396,   395,   458,   462,   466,   404,    20,    19,
     515,   258,   264,   261,   262,   253,   260,   255,   281,   303,
     304,   289,   291,     0,   297,   296,   300,    43,    48,    56,
      55,   355,   359,   357,   360,   475,   213,    85,    92,   198,
     230,   234,   321,   397,   257,   265,   254,   259,   301,   302,
       0,   284,   288,   295,   298,   299,   361,   362,   476,   268,
     266,   285,   269,   267,   270,   271
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -433,  -433,   -51,  -433,   263,  -433,    76,  -433,  -433,  -433,
     336,   337,  -433,  -383,  -433,  -433,  -433,  -201,   338,  -433,
     -91,  -433,   513,  -433,  -433,  -433,  -432,  -433,  -433,   -57,
     511,  -433,  -433,  -433,  -433,  -433,  -284,  -433,   -29,  -239,
    -433,  -433,  -433,  -433,   262,   119,   -15,   516,    50,   201,
    -433,  -433,  -433,  -196,    58,  -433,  -394,   215,  -433,  -433,
    -433,   -13,  -433,    -8,  -433,  -433,  -433,   295,  -433,   576,
    -433,  -433,  -433,   -32,   -24,  -433,  -433,   -49,   359,  -433,
     362,   582,  -433,  -433,   141,  -433,  -433,  -433,  -433,   300,
     293,   294,     8,    21,    42,  -433,  -433,  -433,  -418,   361,
    -433,  -433,  -433,  -433,  -433,  -433,    22,   378,    61,  -433,
     264,  -433,   -34,   606,   597,  -433,  -433,  -433,   140,  -433,
       1,    -7,    -4,  -433,  -338
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,    55,   277,    56,    57,    58,    59,    60,    61,   210,
     211,   212,   213,   398,   214,   215,   393,   221,   216,    62,
      63,    64,    65,    66,   288,   452,   453,    67,    68,    69,
      70,    71,   258,   442,   272,    72,   460,    73,   252,   260,
     261,   427,    74,    75,    76,   467,   468,    77,   201,   202,
     203,   195,   525,   526,   197,   521,   531,   198,   207,   390,
     539,   540,   541,   542,    78,   305,    79,    80,    81,    82,
      83,    84,    85,    86,    87,   357,    88,   242,   243,   244,
     245,    89,    90,    91,   591,    92,    93,    94,    95,   324,
     329,   334,    96,    97,    98,   325,   330,   335,   566,   567,
     264,    99,   100,   101,   102,   103,   227,   253,   229,   174,
     175,   249,   106,   176,   177,   178,   179,   107,   345,   108,
     180,   204,   205,   110,   366
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     181,   109,   269,   182,   111,   241,   279,   284,   394,   232,
     265,   569,   462,   112,   259,   296,   551,   199,   576,   365,
     218,   219,   104,   424,   196,   426,   113,   514,     1,     2,
     275,   278,   224,   230,   230,   235,  -191,   461,   250,  -251,
     523,   275,    11,    12,   262,   246,  -191,   114,   189,  -251,
     273,  -191,   -42,   391,  -251,   254,    16,   320,   247,   598,
     309,   105,   -42,     1,     2,   109,   -42,   -42,   -42,   109,
     321,   392,   220,   109,   363,   298,   331,   356,   326,   248,
       8,   192,   193,   194,   255,   230,   104,   310,   217,   190,
     104,    16,   323,   478,   104,   233,   276,   336,   337,   338,
     333,   328,  -191,   614,   331,  -251,    52,   276,   225,   191,
     615,   109,  -293,   367,   368,   369,   321,   358,   -42,   332,
     524,   459,   399,     7,  -251,   105,   600,   343,   333,   105,
     359,   322,   104,   105,   280,   380,   608,   -42,   323,   302,
     206,    52,     1,     2,   220,   446,   628,   222,   625,     7,
     188,   360,     1,     2,   301,   304,    11,    12,  -334,   231,
     308,     1,     2,   311,   619,   341,   620,   326,  -334,     8,
      16,   105,  -334,  -334,   599,   274,   590,   582,     8,   396,
      16,   342,   327,   381,   382,   355,     6,  -129,  -129,    16,
     328,   570,   267,    18,    19,   415,   281,  -129,   356,   299,
     387,     6,  -129,     7,   411,     7,   300,   648,     7,   480,
      11,    12,    11,    12,   109,   428,     6,   241,   241,   640,
      52,     1,     2,   609,  -334,    11,    12,   444,   610,   422,
      52,   307,   358,   641,   344,   104,   303,   370,     8,    52,
     536,   412,   376,  -334,   414,   359,    18,    19,   240,    16,
     250,   407,   351,  -129,   511,   512,   383,   246,   246,   537,
     538,   423,   377,   432,   388,   430,   360,   186,   477,   363,
     247,   247,  -129,   187,   105,   217,   378,   352,   188,   217,
     -14,   471,   109,   454,   479,   481,     7,   384,   405,   353,
     354,   248,   248,    11,    12,   409,   410,   -65,   292,    52,
     466,   469,   379,   104,   406,   385,   230,   293,   474,     1,
       2,   386,   -65,  -220,   464,   389,     1,     2,   374,   375,
     513,  -220,   208,  -220,     1,     2,   485,   601,  -220,   606,
       6,   489,     7,     8,   484,   282,   493,    16,     1,     2,
     495,     8,   105,   498,    16,   618,   208,   496,     7,     6,
     500,   501,    16,   403,   413,     8,   434,   502,   503,   505,
     507,   509,   421,   -65,   240,   181,    16,   425,   182,   435,
      18,    19,   522,   443,   355,   447,   564,   199,   470,  -220,
     560,   532,   472,   535,   556,   557,   457,    52,   476,   459,
     558,   543,   458,   568,    52,   568,     1,     2,  -220,   553,
     463,   483,    52,   581,   494,   583,   488,  -252,   603,   487,
     228,     1,     2,     8,   559,    19,    52,   604,   491,   499,
     562,   515,   605,   268,    16,   563,   -16,   565,     8,   270,
     544,   555,   572,   548,   549,  -381,   315,   510,   573,    16,
     575,   580,   574,    18,    19,  -381,   454,     9,   396,   294,
    -381,  -381,   347,   594,    13,    14,    15,   306,  -381,  -381,
       6,   109,     7,   109,   469,   312,   313,   314,   554,    11,
      12,   585,   595,  -252,    52,   596,   524,   339,   602,   592,
     340,   592,   104,   617,   104,   607,   613,   361,   524,    52,
       1,     2,  -252,   254,   623,   616,   -44,   545,   627,   550,
     597,  -381,   633,   634,   546,   635,   -44,     8,   637,   636,
     -44,   -44,   -44,   646,   647,   547,   532,   651,    16,   652,
    -381,   105,   255,   105,   653,   629,   532,   -64,   292,   208,
     199,     7,   568,   209,   183,   184,   185,   293,   654,     1,
       2,   621,   -64,   655,   445,   586,   240,   588,   425,  -506,
     371,   400,  -506,  -506,   402,   401,     8,   372,   622,  -506,
     624,   465,   -44,  -506,  -506,  -506,   373,    16,    52,  -506,
     630,   626,  -506,  -506,   454,     1,     2,   283,   251,   291,
    -506,   -44,   584,   109,  -506,  -506,   469,   533,   612,     7,
     297,   568,     8,   -64,   536,   611,    11,    12,  -506,   527,
     642,   404,   473,    16,   104,   638,   639,   643,   226,   419,
     408,   417,   645,   223,   543,  -506,   543,    52,     1,     2,
     486,   593,   482,   416,   236,   429,   490,   418,   650,   420,
    -506,    13,    14,    15,  -506,     8,   516,   536,    18,    19,
     234,   266,     0,   105,     0,   431,    16,   433,     0,     0,
       0,     0,     0,    52,     0,     0,   537,   538,     0,   183,
     184,   185,     0,   631,     0,   632,    18,    19,     0,   -84,
     448,     0,     1,     2,   449,   -84,   361,   450,     0,   -84,
       0,     0,   -84,   -84,   -84,   -84,     0,   475,   451,     8,
     -84,   117,   -84,   -84,   118,     0,    52,   119,   120,   121,
      16,     0,   122,   123,   124,   -84,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   -84,   136,   137,   138,
     139,   140,   141,   142,   143,     0,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
      52,   168,   169,   170,   171,   172,   173,  -380,   316,     0,
       1,     2,     0,     0,     0,     0,     0,  -380,     0,     0,
       0,     0,  -380,  -380,     0,     0,     0,     0,     0,     0,
    -380,  -380,     0,     0,     1,     2,     0,     0,    16,     0,
       0,     0,     0,     0,   492,   561,     0,     0,     0,     1,
       2,     8,     0,     0,     0,     0,     0,     0,     0,   504,
     571,     0,    16,     0,     0,     0,     8,   317,     0,     0,
       0,     0,     0,  -380,   318,     0,   319,    16,     0,     0,
    -187,   271,     0,     1,     2,     0,  -187,     0,    52,     0,
    -187,     0,  -380,  -187,  -187,  -187,  -187,   587,     0,   589,
       8,  -187,   117,  -187,  -187,   118,     0,     0,   119,   120,
     121,    16,    52,   122,   123,   124,  -187,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,    52,   136,   137,
     138,   139,   140,   141,   142,   143,     0,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,    52,   168,   169,   170,   171,   172,   173,  -508,   115,
       0,     1,     2,     0,     0,     0,  -508,     0,  -508,     0,
       0,     0,  -508,  -508,  -508,   116,     0,     0,     8,     0,
     117,  -508,  -508,   118,     0,     0,   119,   120,   121,    16,
       0,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,     0,   136,   137,   138,   139,
     140,   141,   142,   143,     0,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,    52,
     168,   169,   170,   171,   172,   173,  -181,   436,     0,  -181,
    -181,     0,  -181,     0,     0,     0,  -181,     0,     0,  -181,
    -181,  -181,  -181,     0,     0,     0,  -181,  -181,  -181,  -181,
    -181,     0,     0,     0,   437,   438,   439,  -181,     0,   440,
    -181,   441,  -181,  -181,  -181,  -181,  -181,  -181,  -181,  -181,
    -181,     0,  -181,     0,  -181,  -181,  -181,  -181,     0,     0,
    -181,  -181,     0,  -181,     0,     0,     0,     0,  -181,  -181,
    -181,  -181,  -181,     0,     0,     0,     0,  -181,  -181,  -181,
    -181,  -181,  -181,  -181,  -181,  -181,  -181,  -181,  -181,  -181,
    -181,  -181,  -192,   295,     0,     1,     2,     0,     3,     0,
       0,     0,  -192,     0,     0,     5,     6,  -192,     7,     0,
       0,     0,     8,     9,   282,    11,    12,     0,     0,     0,
      13,    14,    15,    16,     0,     0,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,     0,    29,     0,
      31,    32,    33,    34,     0,     0,    35,    36,     0,    37,
       0,     0,     0,     0,    38,    39,    40,    41,  -192,     1,
       2,   517,     0,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,     8,  -192,   117,     0,
       0,   118,   518,   519,   119,   120,   121,    16,     0,   122,
     123,   124,   520,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,     1,     2,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     7,     0,     0,
       0,     8,     0,   117,    11,    12,   118,     0,     0,   119,
     120,   121,    16,     0,   122,   123,   124,     0,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,     0,   136,
     137,   138,   139,   140,   141,   142,   143,     0,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,    52,   168,   169,   170,   171,   172,   173,     1,
       2,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,     0,   117,     0,
       0,   118,   528,   529,   119,   120,   121,    16,     0,   122,
     123,   124,   530,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,     1,     2,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     0,   117,     0,     0,   118,   528,   529,   119,
     120,   121,    16,     0,   122,   123,   124,     0,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,     0,   136,
     137,   138,   139,   140,   141,   142,   143,     0,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,    52,   168,   169,   170,   171,   172,   173,     1,
       2,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,     0,   117,     0,
       0,   118,     0,     0,   119,   120,   121,    16,     0,   122,
     123,   124,   200,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,     1,     2,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   -15,     0,     0,     0,     0,
       0,     8,     0,   117,     0,     0,   118,     0,     0,   119,
     120,   121,    16,     0,   122,   123,   124,     0,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,     0,   136,
     137,   138,   139,   140,   141,   142,   143,     0,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,    52,   168,   169,   170,   171,   172,   173,     1,
       2,   497,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,     0,   117,     0,
       0,   118,     0,     0,   119,   120,   121,    16,     0,   122,
     123,   124,     0,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,     1,     2,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     0,   117,     0,     0,   118,     0,     0,   119,
     120,   121,    16,     0,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,     0,   136,
     137,   138,   139,   140,   141,   142,   143,     0,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,    52,   168,   169,   170,   171,   172,   173,     1,
       2,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,     0,   117,     0,
       0,   118,     0,     0,   119,   120,   121,    16,     0,   122,
     123,   124,   534,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,     1,     2,   552,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     0,   117,     0,     0,   118,     0,     0,   119,
     120,   121,    16,     0,   122,   123,   124,     0,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,     0,   136,
     137,   138,   139,   140,   141,   142,   143,     0,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,    52,   168,   169,   170,   171,   172,   173,     1,
       2,   449,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,     0,   117,     0,
       0,   118,     0,     0,   119,   120,   121,    16,     0,   122,
     123,   124,     0,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,     1,     2,   644,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     0,   117,     0,     0,   118,     0,     0,   119,
     120,   121,    16,     0,   122,   123,   124,     0,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,     0,   136,
     137,   138,   139,   140,   141,   142,   143,     0,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,    52,   168,   169,   170,   171,   172,   173,     1,
       2,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,     0,   117,     0,
       0,   118,     0,     0,   119,   120,   121,    16,     0,   122,
     123,   124,     0,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,     1,     2,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   208,     0,     7,     0,   209,
       0,     8,     0,   117,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,     0,     0,     0,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,     0,   136,
     137,   138,   139,   140,   141,   142,   143,     0,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,    52,   168,   169,   170,   171,   172,   173,     1,
       2,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     7,     0,     0,     0,     8,     0,   117,     0,
       0,     0,     0,     0,     0,     0,     0,    16,     0,     0,
       0,     0,     0,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,     1,     2,   649,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     0,   117,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,     0,     0,     0,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,     0,   136,
     137,   138,   139,   140,   141,   142,   143,     0,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,    52,   168,   169,   170,   171,   172,   173,     1,
       2,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,     0,   117,     0,
       0,     0,     0,     0,     0,     0,     0,    16,     0,     0,
       0,     0,     0,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,     0,   136,   137,   138,   139,   140,   141,
     142,   143,     0,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,    52,   168,   169,
     170,   171,   172,   173,   -86,   577,     0,   -86,   -86,     0,
     -86,     0,   578,     0,   -86,     0,     0,   -86,   -86,   -86,
     -86,     0,     0,   579,   -86,   -86,   -86,   -86,   -86,     0,
       0,     0,   -86,   -86,   -86,   -86,     0,     0,   -86,   -86,
     -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,
     -86,   -86,   -86,   -86,   -86,   -86,     0,     0,   -86,   -86,
       0,   -86,     0,     0,     0,     0,   -86,   -86,   -86,   -86,
     -86,     0,     0,     0,     0,   -86,   -86,   -86,   -86,   -86,
     -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -73,   285,
       0,   -73,   -73,     0,   -73,     0,     0,     0,   286,     0,
       0,   -73,   -73,   -73,   -73,     0,   287,     0,   -73,   -73,
     -73,   -73,   -73,     0,     0,     0,   -73,   -73,   -73,   -73,
       0,     0,   -73,   -73,   -73,   -73,   -73,   -73,   -73,   -73,
     -73,   -73,   -73,   -73,   -73,   -73,   -73,   -73,   -73,   -73,
       0,     0,   -73,   -73,     0,   -73,     0,     0,     0,     0,
     -73,   -73,   -73,   -73,   -73,     0,     0,     0,     0,   -73,
     -73,   -73,   -73,   -73,   -73,   -73,   -73,   -73,   -73,   -73,
     -73,   -73,   -78,   289,     0,   -78,   -78,     0,   -78,     0,
       0,     0,   290,     0,     0,   -78,   -78,   -78,   -78,     0,
       0,     0,   -78,   -78,   -78,   -78,   -78,     0,     0,     0,
     -78,   -78,   -78,   -78,     0,     0,   -78,   -78,   -78,   -78,
     -78,   -78,   -78,   -78,   -78,   -78,   -78,   -78,   -78,   -78,
     -78,   -78,   -78,   -78,     0,     0,   -78,   -78,     0,   -78,
       0,     0,     0,     0,   -78,   -78,   -78,   -78,   -78,     0,
       0,     0,     0,   -78,   -78,   -78,   -78,   -78,   -78,   -78,
     -78,   -78,   -78,   -78,   -78,   -78,   -76,   455,     0,   -76,
     -76,     0,   -76,     0,     0,     0,   456,     0,     0,   -76,
     -76,   -76,   -76,     0,     0,     0,   -76,   -76,   -76,   -76,
     -76,     0,     0,     0,   -76,   -76,   -76,   -76,     0,     0,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,     0,     0,
     -76,   -76,     0,   -76,     0,     0,     0,     0,   -76,   -76,
     -76,   -76,   -76,     0,     0,     0,     0,   -76,   -76,   -76,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,
     362,     0,     0,     1,     2,     0,     3,     0,     0,     0,
       4,     0,     0,     5,     6,   363,     7,     0,     0,     0,
       8,     9,   282,    11,    12,     0,     0,     0,    13,    14,
      15,    16,     0,     0,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,     0,     0,    35,    36,     0,    37,     0,     0,
       0,     0,    38,    39,    40,    41,   364,     0,     0,     0,
       0,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,   395,     0,     0,     1,     2,     0,
       3,     0,     0,     0,     4,     0,     0,     5,     6,   396,
       7,     0,     0,     0,     8,     9,   282,    11,    12,     0,
       0,     0,    13,    14,    15,    16,     0,     0,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,     0,     0,    35,    36,
       0,    37,     0,     0,     0,     0,    38,    39,    40,    41,
     397,     0,     0,     0,     0,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,   346,   347,
       0,     0,     1,     2,     0,     0,     6,     0,     7,     0,
       0,     0,   506,     9,     0,    11,    12,     0,     0,     8,
      13,    14,    15,   348,   349,   350,     0,    18,    19,     0,
      16,   346,     0,     0,     0,     1,     2,     0,     0,     0,
       0,   351,     0,     0,     0,   508,     9,     0,     0,     0,
       0,     0,     8,    13,    14,    15,   348,   349,   350,     0,
       0,     0,     0,    16,     1,     2,   352,     3,     0,     0,
       0,     4,     0,     0,     5,     6,     0,     7,   353,   354,
      52,     8,     9,    10,    11,    12,     0,     0,     0,    13,
      14,    15,    16,     0,     0,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    52,     0,    35,    36,     0,    37,     0,
       0,     0,     0,    38,    39,    40,    41,     0,     0,     0,
       0,     0,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,     1,     2,     0,     3,     0,
       0,     0,     4,     0,     0,     5,     6,     0,     7,     0,
       0,     0,     8,     9,   282,    11,    12,     0,     0,     0,
      13,    14,    15,    16,     0,     0,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,     0,     0,    35,    36,     0,    37,
       0,     0,     0,     0,    38,    39,    40,    41,     0,     0,
       0,     0,     0,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,     1,     2,     0,     3,
       0,     0,     0,     0,     0,     0,     5,     6,     0,     7,
       0,     0,     0,     8,     9,   282,    11,    12,     0,     0,
       0,    13,    14,    15,    16,     0,     0,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,     0,    29,
       0,    31,    32,    33,    34,     0,     0,    35,    36,     0,
      37,     0,     0,     0,     0,    38,    39,    40,    41,     0,
       0,     0,     0,     0,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,     1,     2,     0,
       3,     0,     0,     0,     0,     0,     0,     5,     6,     0,
       7,     0,     0,     0,     8,     9,   282,    11,    12,     0,
       0,     0,    13,    14,    15,    16,     0,     0,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,     0,
      29,     0,    31,    32,    33,    34,     0,     0,     0,     0,
       0,    37,     0,     0,     0,     0,    38,    39,    40,    41,
       0,     1,     2,     0,     0,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,     0,    54,     8,   236,
       0,     0,     0,     0,     0,     0,    13,    14,    15,    16,
       0,     0,     0,    18,    19,     0,     0,     0,     0,     0,
       0,     1,     2,     0,     0,     0,   237,   238,     0,     0,
       0,     0,     0,     0,     7,     0,   239,   240,     8,   236,
       0,    11,    12,     0,     0,     0,    13,    14,    15,    16,
       0,     0,     0,    18,    19,     1,     2,   251,   254,    52,
       0,     0,     0,     1,     2,     0,     3,     0,     7,     0,
       0,     0,     8,     5,     0,    11,    12,   240,     0,     0,
       8,     9,     0,    16,     1,     2,     0,   255,    13,    14,
      15,    16,     0,     0,    17,     6,     0,     7,     0,    52,
       0,     8,     0,     0,    11,    12,     0,     0,     0,     0,
       0,     0,    16,   256,     0,     0,    18,    19,     1,     2,
       0,     0,     0,     0,     0,     0,     0,   257,     0,     0,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
       0,    52,     0,   119,   120,   121,    16,     0,   122,   123,
     124,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    52,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   263,    52
};

static const yytype_int16 yycheck[] =
{
       7,     0,    51,     7,     3,    37,    57,    64,   209,    33,
      44,   429,   296,     5,    43,    72,   399,    24,   450,   110,
      27,    28,     0,   262,    23,   264,     5,   365,     3,     4,
       0,     1,    31,    32,    33,    34,     0,     1,    37,     0,
       1,     0,    23,    24,    43,    37,    10,     5,    21,    10,
      54,    15,     0,     1,    15,     6,    31,    91,    37,     0,
      84,     0,    10,     3,     4,    64,    14,    15,    16,    68,
       6,    19,    20,    72,    15,    74,     6,   109,     6,    37,
      20,    20,    21,    22,    35,    84,    64,    86,    27,    21,
      68,    31,    28,    68,    72,    34,    66,    96,    97,    98,
      30,    29,    66,     1,     6,    66,    81,    66,    32,    21,
       8,   110,    10,   112,   113,   114,     6,   109,    66,    21,
      81,    85,   213,    16,    85,    64,   520,   105,    30,    68,
     109,    21,   110,    72,    58,   186,   530,    85,    28,    78,
       3,    81,     3,     4,    20,     7,   578,     4,   566,    16,
      12,   109,     3,     4,    78,    79,    23,    24,     0,     1,
      84,     3,     4,    87,     0,   104,   549,     6,    10,    20,
      31,   110,    14,    15,   512,     0,    27,   461,    20,    15,
      31,   105,    21,   187,   188,   109,    14,     0,     1,    31,
      29,   430,    59,    35,    36,   244,    15,    10,   230,     8,
       8,    14,    15,    16,   236,    16,     8,   625,    16,    70,
      23,    24,    23,    24,   213,   264,    14,   249,   250,     1,
      81,     3,     4,     7,    66,    23,    24,   278,    12,   258,
      81,     8,   224,    15,     9,   213,    49,    17,    20,    81,
      22,   240,     7,    85,   243,   224,    35,    36,    59,    31,
     249,   229,    49,    66,     0,     1,    14,   249,   250,    41,
      42,   260,    21,   267,   203,   264,   224,     1,   317,    15,
     249,   250,    85,     7,   213,   214,    21,    74,    12,   218,
      14,   305,   281,   287,   318,   319,    16,     8,   227,    86,
      87,   249,   250,    23,    24,   234,   235,     0,     1,    81,
     299,   300,    21,   281,   228,     7,   305,    10,   307,     3,
       4,     8,    15,     0,     1,    14,     3,     4,   178,   179,
      66,     8,    14,    10,     3,     4,   325,   523,    15,   525,
      14,   330,    16,    20,    13,    22,   335,    31,     3,     4,
     344,    20,   281,   347,    31,   546,    14,   346,    16,    14,
     349,   350,    31,     7,    21,    20,    84,   356,   357,   358,
     359,   360,    75,    66,    59,   372,    31,    61,   372,    84,
      35,    36,   376,    10,   298,    10,   425,   384,   302,    66,
     412,   385,   306,   387,     5,     6,    10,    81,   312,    85,
      11,   390,    10,   427,    81,   429,     3,     4,    85,   403,
       8,    21,    81,   460,   343,   462,    13,     0,     1,    21,
      32,     3,     4,    20,    35,    36,    81,    10,    21,     6,
     419,    17,    15,    45,    31,   424,    14,   426,    20,    51,
      19,    21,   436,     0,     1,     0,     1,   361,   442,    31,
      19,    10,   446,    35,    36,    10,   450,    21,    15,    71,
      15,    16,     7,    21,    28,    29,    30,    79,    23,    24,
      14,   460,    16,   462,   463,    87,    88,    89,   407,    23,
      24,     8,    21,    66,    81,    21,    81,    99,     5,   478,
     102,   480,   460,    19,   462,    10,    10,   109,    81,    81,
       3,     4,    85,     6,     5,     7,     0,     1,    19,    66,
     499,    66,     5,    10,     8,     5,    10,    20,    10,    15,
      14,    15,    16,     5,     5,    19,   520,    15,    31,     5,
      85,   460,    35,   462,     5,   582,   530,     0,     1,    14,
     537,    16,   566,    18,    28,    29,    30,    10,     5,     3,
       4,     5,    15,     5,   281,   469,    59,   471,    61,     0,
       1,   214,     3,     4,   218,   217,    20,     8,   557,    10,
     559,   299,    66,    14,    15,    16,    17,    31,    81,    20,
     585,   570,    23,    24,   578,     3,     4,    64,     5,    68,
      31,    85,   463,   582,    35,    36,   585,   386,   538,    16,
      74,   625,    20,    66,    22,   537,    23,    24,    49,   384,
     613,   223,   307,    31,   582,   609,   610,   615,    32,   250,
     232,   249,   616,    31,   613,    66,   615,    81,     3,     4,
     327,   480,   322,   245,    21,   264,   332,   249,   635,   251,
      81,    28,    29,    30,    85,    20,   372,    22,    35,    36,
      34,    44,    -1,   582,    -1,   267,    31,   269,    -1,    -1,
      -1,    -1,    -1,    81,    -1,    -1,    41,    42,    -1,    28,
      29,    30,    -1,   587,    -1,   589,    35,    36,    -1,     0,
       1,    -1,     3,     4,     5,     6,   298,     8,    -1,    10,
      -1,    -1,    13,    14,    15,    16,    -1,   309,    19,    20,
      21,    22,    23,    24,    25,    -1,    81,    28,    29,    30,
      31,    -1,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    -1,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,     0,     1,    -1,
       3,     4,    -1,    -1,    -1,    -1,    -1,    10,    -1,    -1,
      -1,    -1,    15,    16,    -1,    -1,    -1,    -1,    -1,    -1,
      23,    24,    -1,    -1,     3,     4,    -1,    -1,    31,    -1,
      -1,    -1,    -1,    -1,    13,   417,    -1,    -1,    -1,     3,
       4,    20,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    13,
     432,    -1,    31,    -1,    -1,    -1,    20,    60,    -1,    -1,
      -1,    -1,    -1,    66,    67,    -1,    69,    31,    -1,    -1,
       0,     1,    -1,     3,     4,    -1,     6,    -1,    81,    -1,
      10,    -1,    85,    13,    14,    15,    16,   469,    -1,   471,
      20,    21,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    81,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    81,    48,    49,
      50,    51,    52,    53,    54,    55,    -1,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,     0,     1,
      -1,     3,     4,    -1,    -1,    -1,     8,    -1,    10,    -1,
      -1,    -1,    14,    15,    16,    17,    -1,    -1,    20,    -1,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      -1,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    -1,    48,    49,    50,    51,
      52,    53,    54,    55,    -1,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,     0,     1,    -1,     3,
       4,    -1,     6,    -1,    -1,    -1,    10,    -1,    -1,    13,
      14,    15,    16,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    -1,    -1,    -1,    28,    29,    30,    31,    -1,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    -1,    46,    -1,    48,    49,    50,    51,    -1,    -1,
      54,    55,    -1,    57,    -1,    -1,    -1,    -1,    62,    63,
      64,    65,    66,    -1,    -1,    -1,    -1,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,     0,     1,    -1,     3,     4,    -1,     6,    -1,
      -1,    -1,    10,    -1,    -1,    13,    14,    15,    16,    -1,
      -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,    -1,
      28,    29,    30,    31,    -1,    -1,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    -1,    46,    -1,
      48,    49,    50,    51,    -1,    -1,    54,    55,    -1,    57,
      -1,    -1,    -1,    -1,    62,    63,    64,    65,    66,     3,
       4,     5,    -1,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    20,    85,    22,    -1,
      -1,    25,    26,    27,    28,    29,    30,    31,    -1,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     3,     4,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    16,    -1,    -1,
      -1,    20,    -1,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    33,    34,    35,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,     3,
       4,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    25,    26,    27,    28,    29,    30,    31,    -1,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     3,     4,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    25,    26,    27,    28,
      29,    30,    31,    -1,    33,    34,    35,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,     3,
       4,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    25,    -1,    -1,    28,    29,    30,    31,    -1,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     3,     4,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    33,    34,    35,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,     3,
       4,     5,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    25,    -1,    -1,    28,    29,    30,    31,    -1,    33,
      34,    35,    -1,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     3,     4,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,     3,
       4,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    25,    -1,    -1,    28,    29,    30,    31,    -1,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     3,     4,     5,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    33,    34,    35,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,     3,
       4,     5,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    25,    -1,    -1,    28,    29,    30,    31,    -1,    33,
      34,    35,    -1,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     3,     4,     5,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    33,    34,    35,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,     3,
       4,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    25,    -1,    -1,    28,    29,    30,    31,    -1,    33,
      34,    35,    -1,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     3,     4,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    14,    -1,    16,    -1,    18,
      -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    31,    -1,    -1,    -1,    -1,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,     3,
       4,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    16,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    31,    -1,    -1,
      -1,    -1,    -1,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     3,     4,     5,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    31,    -1,    -1,    -1,    -1,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,     3,
       4,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    31,    -1,    -1,
      -1,    -1,    -1,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,     0,     1,    -1,     3,     4,    -1,
       6,    -1,     8,    -1,    10,    -1,    -1,    13,    14,    15,
      16,    -1,    -1,    19,    20,    21,    22,    23,    24,    -1,
      -1,    -1,    28,    29,    30,    31,    -1,    -1,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    -1,    -1,    54,    55,
      -1,    57,    -1,    -1,    -1,    -1,    62,    63,    64,    65,
      66,    -1,    -1,    -1,    -1,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,     0,     1,
      -1,     3,     4,    -1,     6,    -1,    -1,    -1,    10,    -1,
      -1,    13,    14,    15,    16,    -1,    18,    -1,    20,    21,
      22,    23,    24,    -1,    -1,    -1,    28,    29,    30,    31,
      -1,    -1,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      -1,    -1,    54,    55,    -1,    57,    -1,    -1,    -1,    -1,
      62,    63,    64,    65,    66,    -1,    -1,    -1,    -1,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,     0,     1,    -1,     3,     4,    -1,     6,    -1,
      -1,    -1,    10,    -1,    -1,    13,    14,    15,    16,    -1,
      -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,    -1,
      28,    29,    30,    31,    -1,    -1,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    -1,    -1,    54,    55,    -1,    57,
      -1,    -1,    -1,    -1,    62,    63,    64,    65,    66,    -1,
      -1,    -1,    -1,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,     0,     1,    -1,     3,
       4,    -1,     6,    -1,    -1,    -1,    10,    -1,    -1,    13,
      14,    15,    16,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    -1,    -1,    -1,    28,    29,    30,    31,    -1,    -1,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    -1,    -1,
      54,    55,    -1,    57,    -1,    -1,    -1,    -1,    62,    63,
      64,    65,    66,    -1,    -1,    -1,    -1,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
       0,    -1,    -1,     3,     4,    -1,     6,    -1,    -1,    -1,
      10,    -1,    -1,    13,    14,    15,    16,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    -1,    -1,    -1,    28,    29,
      30,    31,    -1,    -1,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    -1,    -1,    54,    55,    -1,    57,    -1,    -1,
      -1,    -1,    62,    63,    64,    65,    66,    -1,    -1,    -1,
      -1,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,     0,    -1,    -1,     3,     4,    -1,
       6,    -1,    -1,    -1,    10,    -1,    -1,    13,    14,    15,
      16,    -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,
      -1,    -1,    28,    29,    30,    31,    -1,    -1,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    -1,    -1,    54,    55,
      -1,    57,    -1,    -1,    -1,    -1,    62,    63,    64,    65,
      66,    -1,    -1,    -1,    -1,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,     6,     7,
      -1,    -1,     3,     4,    -1,    -1,    14,    -1,    16,    -1,
      -1,    -1,    13,    21,    -1,    23,    24,    -1,    -1,    20,
      28,    29,    30,    31,    32,    33,    -1,    35,    36,    -1,
      31,     6,    -1,    -1,    -1,     3,     4,    -1,    -1,    -1,
      -1,    49,    -1,    -1,    -1,    13,    21,    -1,    -1,    -1,
      -1,    -1,    20,    28,    29,    30,    31,    32,    33,    -1,
      -1,    -1,    -1,    31,     3,     4,    74,     6,    -1,    -1,
      -1,    10,    -1,    -1,    13,    14,    -1,    16,    86,    87,
      81,    20,    21,    22,    23,    24,    -1,    -1,    -1,    28,
      29,    30,    31,    -1,    -1,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    81,    -1,    54,    55,    -1,    57,    -1,
      -1,    -1,    -1,    62,    63,    64,    65,    -1,    -1,    -1,
      -1,    -1,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,     3,     4,    -1,     6,    -1,
      -1,    -1,    10,    -1,    -1,    13,    14,    -1,    16,    -1,
      -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,    -1,
      28,    29,    30,    31,    -1,    -1,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    -1,    -1,    54,    55,    -1,    57,
      -1,    -1,    -1,    -1,    62,    63,    64,    65,    -1,    -1,
      -1,    -1,    -1,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,     3,     4,    -1,     6,
      -1,    -1,    -1,    -1,    -1,    -1,    13,    14,    -1,    16,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,
      -1,    28,    29,    30,    31,    -1,    -1,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    -1,    46,
      -1,    48,    49,    50,    51,    -1,    -1,    54,    55,    -1,
      57,    -1,    -1,    -1,    -1,    62,    63,    64,    65,    -1,
      -1,    -1,    -1,    -1,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,     3,     4,    -1,
       6,    -1,    -1,    -1,    -1,    -1,    -1,    13,    14,    -1,
      16,    -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,
      -1,    -1,    28,    29,    30,    31,    -1,    -1,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    -1,
      46,    -1,    48,    49,    50,    51,    -1,    -1,    -1,    -1,
      -1,    57,    -1,    -1,    -1,    -1,    62,    63,    64,    65,
      -1,     3,     4,    -1,    -1,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    -1,    83,    20,    21,
      -1,    -1,    -1,    -1,    -1,    -1,    28,    29,    30,    31,
      -1,    -1,    -1,    35,    36,    -1,    -1,    -1,    -1,    -1,
      -1,     3,     4,    -1,    -1,    -1,    48,    49,    -1,    -1,
      -1,    -1,    -1,    -1,    16,    -1,    58,    59,    20,    21,
      -1,    23,    24,    -1,    -1,    -1,    28,    29,    30,    31,
      -1,    -1,    -1,    35,    36,     3,     4,     5,     6,    81,
      -1,    -1,    -1,     3,     4,    -1,     6,    -1,    16,    -1,
      -1,    -1,    20,    13,    -1,    23,    24,    59,    -1,    -1,
      20,    21,    -1,    31,     3,     4,    -1,    35,    28,    29,
      30,    31,    -1,    -1,    34,    14,    -1,    16,    -1,    81,
      -1,    20,    -1,    -1,    23,    24,    -1,    -1,    -1,    -1,
      -1,    -1,    31,    61,    -1,    -1,    35,    36,     3,     4,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    75,    -1,    -1,
      -1,    -1,    -1,    81,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    81,    -1,    28,    29,    30,    31,    -1,    33,    34,
      35,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    81,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    80,    81
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     6,    10,    13,    14,    16,    20,    21,
      22,    23,    24,    28,    29,    30,    31,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    54,    55,    57,    62,    63,
      64,    65,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    90,    92,    93,    94,    95,
      96,    97,   108,   109,   110,   111,   112,   116,   117,   118,
     119,   120,   124,   126,   131,   132,   133,   136,   153,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   165,   170,
     171,   172,   174,   175,   176,   177,   181,   182,   183,   190,
     191,   192,   193,   194,   195,   197,   201,   206,   208,   209,
     212,   209,   181,   182,   183,     1,    17,    22,    25,    28,
      29,    30,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    48,    49,    50,    51,
      52,    53,    54,    55,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    82,    83,
      84,    85,    86,    87,   198,   199,   202,   203,   204,   205,
     209,   210,   211,    28,    29,    30,     1,     7,    12,    21,
      21,    21,   197,   197,   197,   140,   209,   143,   146,   210,
      36,   137,   138,   139,   210,   211,     3,   147,    14,    18,
      98,    99,   100,   101,   103,   104,   107,   197,   210,   210,
      20,   106,     4,   170,   209,    95,   158,   195,   196,   197,
     209,     1,   163,   197,   202,   209,    21,    48,    49,    58,
      59,   162,   166,   167,   168,   169,   181,   182,   183,   200,
     209,     5,   127,   196,     6,    35,    61,    75,   121,   127,
     128,   129,   209,    80,   189,   201,   203,    59,   196,   166,
     196,     1,   123,   211,     0,     0,    66,    91,     1,    91,
      95,    15,    22,   111,   118,     1,    10,    18,   113,     1,
      10,   119,     1,    10,   196,     1,   118,   136,   209,     8,
       8,    95,   197,    49,    95,   154,   196,     8,    95,   163,
     209,    95,   196,   196,   196,     1,     1,    60,    67,    69,
     201,     6,    21,    28,   178,   184,     6,    21,    29,   179,
     185,     6,    21,    30,   180,   186,   209,   209,   209,   196,
     196,   197,    95,   195,     9,   207,     6,     7,    31,    32,
      33,    49,    74,    86,    87,    95,   162,   164,   181,   182,
     183,   196,     0,    15,    66,   109,   213,   209,   209,   209,
      17,     1,     8,    17,   207,   207,     7,    21,    21,    21,
      91,   211,   211,    14,     8,     7,     8,     8,   197,    14,
     148,     1,    19,   105,   106,     0,    15,    66,   102,   109,
     100,   107,    99,     7,   196,   197,    95,   195,   196,   197,
     197,   162,   209,    21,   209,   166,   196,   169,   196,   167,
     196,    75,   127,   209,   128,    61,   128,   130,   166,   188,
     209,   196,   211,   196,    84,    84,     1,    28,    29,    30,
      33,    35,   122,    10,    91,    93,     7,    10,     1,     5,
       8,    19,   114,   115,   211,     1,    10,    10,    10,    85,
     125,     1,   125,     8,     1,   133,   209,   134,   135,   209,
      95,   163,    95,   156,   209,   196,    95,   166,    68,   201,
      70,   201,   178,    21,    13,   209,   179,    21,    13,   209,
     180,    21,    13,   209,   197,   211,   209,     5,   211,     6,
     209,   209,   209,   209,    13,   209,    13,   209,    13,   209,
      95,     0,     1,    66,   213,    17,   199,     5,    26,    27,
      36,   144,   211,     1,    81,   141,   142,   146,    26,    27,
      36,   145,   211,   138,    36,   211,    22,    41,    42,   149,
     150,   151,   152,   209,    19,     1,     8,    19,     0,     1,
      66,   102,     5,   211,   197,    21,     5,     6,    11,    35,
     162,   196,   209,   209,   166,   209,   187,   188,   201,   187,
     128,   196,   211,   211,   211,    19,   115,     1,     8,    19,
      10,   118,   125,   118,   134,     8,    95,   196,    95,   196,
      27,   173,   209,   173,    21,    21,    21,   209,     0,   213,
     145,   142,     5,     1,    10,    15,   142,    10,   145,     7,
      12,   143,   137,    10,     1,     8,     7,    19,   106,     0,
     102,     5,   209,     5,   209,   187,   209,    19,   115,   118,
     135,    95,    95,     5,    10,     5,    15,    10,   211,   211,
       1,    15,   150,   152,     5,   211,     5,     5,   187,     5,
     210,    15,     5,     5,     5,     5
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_uint8 yyr1[] =
{
       0,    89,    90,    91,    91,    91,    92,    92,    92,    92,
      92,    93,    93,    93,    94,    94,    94,    95,    95,    95,
      95,    95,    95,    95,    95,    96,    97,    97,    98,    98,
      98,    99,    99,    99,   100,   100,   100,   101,   102,   103,
     104,   104,   104,   104,   104,   104,   105,   105,   105,   106,
     106,   106,   106,   107,   107,   107,   107,   107,   107,   107,
     107,   108,   108,   109,   109,   109,   110,   110,   111,   111,
     111,   111,   111,   111,   111,   111,   111,   111,   111,   111,
     112,   112,   113,   113,   113,   113,   113,   113,   114,   114,
     114,   114,   114,   115,   115,   116,   116,   117,   117,   117,
     118,   118,   118,   119,   119,   119,   119,   119,   119,   119,
     119,   119,   119,   119,   119,   119,   119,   119,   119,   119,
     119,   119,   119,   119,   119,   119,   119,   119,   119,   119,
     119,   119,   119,   119,   119,   119,   119,   119,   119,   119,
     119,   119,   119,   119,   119,   119,   119,   119,   119,   119,
     119,   119,   119,   119,   119,   119,   119,   119,   119,   119,
     119,   119,   119,   119,   119,   119,   119,   119,   119,   119,
     120,   120,   121,   121,   121,   121,   122,   122,   122,   122,
     122,   123,   123,   123,   123,   124,   124,   124,   124,   124,
     125,   126,   126,   126,   126,   126,   126,   126,   126,   126,
     127,   127,   127,   128,   128,   129,   129,   129,   129,   129,
     130,   130,   130,   130,   131,   131,   131,   131,   132,   132,
     132,   132,   133,   133,   133,   133,   133,   133,   133,   134,
     134,   134,   135,   135,   135,   135,   136,   136,   136,   136,
     137,   137,   137,   138,   138,   139,   139,   139,   139,   139,
     140,   140,   140,   140,   140,   141,   141,   141,   141,   141,
     141,   141,   141,   142,   142,   142,   142,   142,   142,   142,
     142,   142,   143,   143,   143,   144,   144,   145,   145,   145,
     146,   146,   146,   146,   147,   147,   148,   149,   149,   150,
     150,   150,   150,   150,   151,   151,   151,   151,   152,   152,
     152,   152,   152,   152,   152,   153,   153,   153,   153,   153,
     154,   154,   155,   155,   155,   155,   155,   155,   155,   155,
     155,   155,   155,   155,   156,   156,   157,   157,   158,   158,
     158,   158,   159,   159,   159,   159,   160,   160,   160,   161,
     161,   162,   162,   163,   163,   163,   163,   164,   164,   164,
     164,   165,   166,   166,   166,   166,   166,   166,   166,   166,
     166,   166,   166,   166,   167,   167,   167,   167,   167,   167,
     167,   168,   168,   168,   168,   168,   169,   169,   169,   170,
     170,   170,   170,   170,   171,   171,   171,   171,   171,   171,
     171,   171,   172,   172,   172,   172,   173,   173,   173,   174,
     174,   174,   174,   174,   174,   174,   174,   174,   174,   174,
     175,   175,   175,   175,   175,   175,   175,   175,   175,   176,
     176,   176,   176,   176,   176,   176,   176,   176,   177,   177,
     177,   177,   177,   177,   177,   177,   177,   178,   178,   179,
     179,   180,   180,   181,   181,   181,   181,   182,   182,   182,
     182,   183,   183,   183,   183,   184,   184,   184,   184,   185,
     185,   185,   185,   186,   186,   186,   186,   187,   187,   188,
     188,   189,   189,   189,   190,   190,   190,   190,   190,   190,
     190,   190,   190,   190,   191,   191,   192,   192,   192,   192,
     193,   193,   193,   194,   194,   195,   195,   196,   196,   196,
     196,   196,   197,   197,   197,   197,   197,   197,   197,   197,
     198,   198,   198,   199,   199,   199,   199,   199,   199,   199,
     199,   200,   200,   200,   201,   201,   201,   201,   202,   202,
     202,   202,   202,   202,   202,   202,   202,   202,   202,   202,
     202,   202,   202,   202,   202,   202,   202,   202,   202,   202,
     202,   202,   202,   202,   202,   202,   202,   202,   202,   202,
     202,   202,   202,   202,   202,   202,   202,   202,   202,   202,
     202,   202,   202,   202,   202,   202,   202,   203,   203,   203,
     203,   203,   203,   204,   205,   205,   206,   207,   207,   208,
     208,   209,   209,   210,   210,   210,   211,   211,   211,   212,
     213
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     1,     1,     2,     0,     2,     3,     2,
       3,     1,     2,     3,     1,     2,     3,     3,     2,     4,
       4,     3,     2,     3,     2,     1,     1,     2,     1,     2,
       1,     1,     2,     1,     1,     2,     1,     1,     1,     1,
       2,     3,     1,     4,     2,     3,     1,     2,     3,     1,
       2,     3,     3,     3,     2,     4,     4,     3,     2,     3,
       2,     2,     1,     1,     2,     1,     1,     2,     2,     1,
       3,     2,     3,     1,     3,     4,     2,     2,     1,     3,
       1,     2,     2,     3,     1,     4,     2,     3,     1,     1,
       2,     2,     3,     1,     1,     1,     2,     1,     1,     1,
       1,     2,     1,     1,     2,     1,     2,     3,     1,     2,
       3,     2,     3,     4,     3,     1,     1,     2,     3,     2,
       1,     1,     2,     2,     3,     3,     4,     1,     1,     1,
       1,     1,     2,     1,     2,     1,     2,     1,     2,     1,
       1,     1,     1,     2,     1,     2,     2,     1,     1,     2,
       3,     2,     1,     2,     3,     2,     2,     3,     1,     2,
       1,     1,     1,     3,     2,     1,     2,     2,     1,     1,
       1,     1,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     3,     3,     3,     2,     1,     2,     3,
       1,     2,     1,     2,     3,     4,     3,     4,     5,     4,
       1,     1,     2,     1,     1,     2,     2,     1,     3,     1,
       2,     2,     1,     3,     1,     1,     1,     1,     1,     3,
       2,     3,     3,     3,     2,     3,     2,     3,     2,     1,
       3,     2,     2,     1,     3,     2,     2,     1,     3,     2,
       1,     3,     2,     2,     1,     1,     1,     2,     3,     3,
       1,     2,     3,     4,     5,     2,     1,     3,     2,     3,
       2,     2,     2,     1,     2,     3,     4,     5,     4,     5,
       6,     7,     1,     3,     2,     1,     1,     1,     1,     1,
       3,     4,     2,     1,     5,     6,     1,     1,     3,     2,
       1,     2,     1,     1,     1,     3,     2,     2,     3,     3,
       2,     3,     3,     2,     2,     1,     2,     2,     3,     2,
       0,     1,     1,     1,     2,     3,     2,     2,     3,     4,
       4,     5,     2,     3,     1,     2,     1,     3,     1,     2,
       2,     3,     2,     2,     1,     3,     1,     2,     3,     1,
       2,     1,     1,     3,     2,     2,     1,     1,     1,     1,
       1,     3,     2,     3,     3,     4,     3,     4,     3,     4,
       4,     5,     5,     1,     1,     2,     2,     3,     1,     1,
       1,     3,     2,     2,     1,     1,     2,     1,     1,     1,
       1,     1,     2,     2,     2,     2,     2,     3,     3,     2,
       3,     3,     1,     3,     4,     4,     1,     2,     1,     1,
       1,     1,     3,     3,     4,     2,     3,     3,     2,     2,
       3,     1,     2,     3,     2,     3,     3,     2,     3,     3,
       1,     2,     3,     2,     3,     3,     2,     3,     3,     2,
       1,     3,     2,     3,     3,     2,     3,     1,     1,     1,
       1,     1,     1,     1,     2,     2,     3,     1,     2,     2,
       3,     1,     2,     2,     3,     1,     2,     2,     3,     1,
       2,     2,     3,     1,     2,     2,     3,     1,     1,     2,
       1,     1,     1,     1,     4,     5,     6,     3,     4,     3,
       3,     3,     2,     1,     1,     2,     4,     2,     2,     3,
       1,     1,     1,     2,     1,     1,     1,     1,     2,     3,
       2,     1,     2,     3,     4,     3,     2,     3,     1,     2,
       1,     3,     2,     3,     3,     4,     3,     3,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     1,
       1,     1,     2,     1,     1,     2,     1,     1,     2,     1,
       1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, RESULT, yyscanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, RESULT, yyscanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), RESULT, yyscanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, RESULT, yyscanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_TOK_STRING: /* TOK_STRING  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2398 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_QSTRING: /* TOK_QSTRING  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2404 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_NUMBER: /* TOK_NUMBER  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2410 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_OCBRACKET: /* TOK_OCBRACKET  */
#line 199 "msc_lang.yy"
            {}
#line 2416 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CCBRACKET: /* TOK_CCBRACKET  */
#line 199 "msc_lang.yy"
            {}
#line 2422 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_PARAM_NAME: /* TOK_PARAM_NAME  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2428 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_MSC: /* TOK_MSC  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2434 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_STRING: /* TOK_COLON_STRING  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2440 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_QUOTED_STRING: /* TOK_COLON_QUOTED_STRING  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2446 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_STYLE_NAME: /* TOK_STYLE_NAME  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2452 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLORDEF: /* TOK_COLORDEF  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2458 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLORDEF_NAME_NUMBER: /* TOK_COLORDEF_NAME_NUMBER  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2464 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_REL_X: /* TOK_REL_X  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2470 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_HEADING: /* TOK_COMMAND_HEADING  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2476 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_NUDGE: /* TOK_COMMAND_NUDGE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2482 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_NEWPAGE: /* TOK_COMMAND_NEWPAGE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2488 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFSHAPE: /* TOK_COMMAND_DEFSHAPE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2494 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFCOLOR: /* TOK_COMMAND_DEFCOLOR  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2500 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFSTYLE: /* TOK_COMMAND_DEFSTYLE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2506 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFDESIGN: /* TOK_COMMAND_DEFDESIGN  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2512 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFPROC: /* TOK_COMMAND_DEFPROC  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2518 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_REPLAY: /* TOK_COMMAND_REPLAY  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2524 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_SET: /* TOK_COMMAND_SET  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2530 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_INCLUDE: /* TOK_COMMAND_INCLUDE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2536 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_BIG: /* TOK_COMMAND_BIG  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2542 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_BOX: /* TOK_COMMAND_BOX  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2548 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_PIPE: /* TOK_COMMAND_PIPE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2554 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_MARK: /* TOK_COMMAND_MARK  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2560 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_MARK_SRC: /* TOK_COMMAND_MARK_SRC  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2566 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_MARK_DST: /* TOK_COMMAND_MARK_DST  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2572 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_PARALLEL: /* TOK_COMMAND_PARALLEL  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2578 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_OVERLAP: /* TOK_COMMAND_OVERLAP  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2584 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_INLINE: /* TOK_COMMAND_INLINE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2590 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_VERTICAL: /* TOK_VERTICAL  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2596 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_VERTICAL_SHAPE: /* TOK_VERTICAL_SHAPE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2602 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_AT: /* TOK_AT  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2608 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_LOST: /* TOK_LOST  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2614 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_AT_POS: /* TOK_AT_POS  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2620 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_SHOW: /* TOK_SHOW  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2626 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_HIDE: /* TOK_HIDE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2632 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ACTIVATE: /* TOK_ACTIVATE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2638 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_DEACTIVATE: /* TOK_DEACTIVATE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2644 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_BYE: /* TOK_BYE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2650 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_START: /* TOK_START  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2656 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_BEFORE: /* TOK_BEFORE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2662 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_END: /* TOK_END  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2668 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_AFTER: /* TOK_AFTER  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2674 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_VSPACE: /* TOK_COMMAND_VSPACE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2680 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_HSPACE: /* TOK_COMMAND_HSPACE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2686 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_SYMBOL: /* TOK_COMMAND_SYMBOL  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2692 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_NOTE: /* TOK_COMMAND_NOTE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2698 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_COMMENT: /* TOK_COMMAND_COMMENT  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2704 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_ENDNOTE: /* TOK_COMMAND_ENDNOTE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2710 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_FOOTNOTE: /* TOK_COMMAND_FOOTNOTE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2716 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_TITLE: /* TOK_COMMAND_TITLE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2722 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_SUBTITLE: /* TOK_COMMAND_SUBTITLE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2728 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_TEXT: /* TOK_COMMAND_TEXT  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2734 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_JOIN: /* TOK_JOIN  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2740 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_IF: /* TOK_IF  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2746 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_THEN: /* TOK_THEN  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2752 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ELSE: /* TOK_ELSE  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2758 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_MSCGEN_RBOX: /* TOK_MSCGEN_RBOX  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2764 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_MSCGEN_ABOX: /* TOK_MSCGEN_ABOX  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2770 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_top_level_arclist: /* top_level_arclist  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arclist);}
#line 2776 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_braced_arclist: /* braced_arclist  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arclist);}
#line 2782 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp1: /* defprochelp1  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 2788 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp2: /* defprochelp2  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 2794 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp3: /* defprochelp3  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 2800 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp4: /* defprochelp4  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 2806 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_scope_open_proc_body: /* scope_open_proc_body  */
#line 199 "msc_lang.yy"
            {}
#line 2812 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_scope_close_proc_body: /* scope_close_proc_body  */
#line 199 "msc_lang.yy"
            {}
#line 2818 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_arglist_tested: /* proc_def_arglist_tested  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 2824 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_arglist: /* proc_def_arglist  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 2830 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_param_list: /* proc_def_param_list  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 2836 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_param: /* proc_def_param  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdef);}
#line 2842 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_procedure_body: /* procedure_body  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procedure);}
#line 2848 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arclist_maybe_no_semicolon: /* arclist_maybe_no_semicolon  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arclist);}
#line 2854 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arclist: /* arclist  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arclist);}
#line 2860 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arc_with_parallel_semicolon: /* arc_with_parallel_semicolon  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 2866 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invocation: /* proc_invocation  */
#line 199 "msc_lang.yy"
            {}
#line 2872 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_proc_param_list: /* proc_param_list  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoclist);}
#line 2878 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invoc_param_list: /* proc_invoc_param_list  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoclist);}
#line 2884 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invoc_param: /* proc_invoc_param  */
#line 195 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoc);}
#line 2890 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_include: /* include  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2896 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_overlap_or_parallel: /* overlap_or_parallel  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2902 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arc_with_parallel: /* arc_with_parallel  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 2908 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arc: /* arc  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 2914 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_titlecommandtoken: /* titlecommandtoken  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2920 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_hspace_location: /* hspace_location  */
#line 194 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).namerel);}
#line 2926 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_comp: /* comp  */
#line 199 "msc_lang.yy"
            {}
#line 2932 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_condition: /* condition  */
#line 199 "msc_lang.yy"
            {}
#line 2938 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_ifthen_condition: /* ifthen_condition  */
#line 200 "msc_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
  #else
    chart.PopContext();
  #endif
}
#line 2950 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_else: /* else  */
#line 200 "msc_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
  #else
    chart.PopContext();
  #endif
}
#line 2962 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_ifthen: /* ifthen  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 2968 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_full_arcattrlist_with_label_or_number: /* full_arcattrlist_with_label_or_number  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attriblist);}
#line 2974 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_entityrel: /* entityrel  */
#line 194 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).namerel);}
#line 2980 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_markerrel_no_string: /* markerrel_no_string  */
#line 194 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).namerel);}
#line 2986 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_entity_command_prefixes: /* entity_command_prefixes  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 2992 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_optlist: /* optlist  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arclist);}
#line 2998 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_opt: /* opt  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 3004 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_entitylist: /* entitylist  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).entitylist);}
#line 3010 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_entity: /* entity  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).entitylist);}
#line 3016 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_first_entity: /* first_entity  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).entitylist);}
#line 3022 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_stylenamelist: /* stylenamelist  */
#line 198 "msc_lang.yy"
            {delete ((*yyvaluep).stringlist);}
#line 3028 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_shapedeflist: /* shapedeflist  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).shape);}
#line 3034 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_shapeline: /* shapeline  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).shapeelement);}
#line 3040 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_color_def_string: /* color_def_string  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3046 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_color_string: /* color_string  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3052 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_parallel: /* parallel  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcparallel);}
#line 3058 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_box_list: /* box_list  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcboxseries);}
#line 3064 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_mscgen_box: /* mscgen_box  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcboxseries);}
#line 3070 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_mscgen_boxlist: /* mscgen_boxlist  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arclist);}
#line 3076 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_first_box: /* first_box  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcboxseries);}
#line 3082 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_first_pipe: /* first_pipe  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcpipe);}
#line 3088 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_pipe_list_no_content: /* pipe_list_no_content  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcpipeseries);}
#line 3094 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_pipe_list: /* pipe_list  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcpipeseries);}
#line 3100 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_boxrel: /* boxrel  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbox);}
#line 3106 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_mscgen_emphrel: /* mscgen_emphrel  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3112 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_mscgen_boxrel: /* mscgen_boxrel  */
#line 192 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbox);}
#line 3118 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_vertxpos: /* vertxpos  */
#line 194 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).vertxpos);}
#line 3124 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_vertrel_no_xpos: /* vertrel_no_xpos  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcvertarrow);}
#line 3130 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_vertrel: /* vertrel  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcvertarrow);}
#line 3136 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arcrel: /* arcrel  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 3142 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_with_specifier_incomplete: /* arrow_with_specifier_incomplete  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcarrow);}
#line 3148 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_with_specifier: /* arrow_with_specifier  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcarrow);}
#line 3154 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_special_ending: /* special_ending  */
#line 194 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arrowending);}
#line 3160 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arcrel_arrow: /* arcrel_arrow  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcarrow);}
#line 3166 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arcrel_to: /* arcrel_to  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcarrow);}
#line 3172 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arcrel_from: /* arcrel_from  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcarrow);}
#line 3178 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arcrel_bidir: /* arcrel_bidir  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcarrow);}
#line 3184 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_extvertxpos: /* extvertxpos  */
#line 194 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).extvertxpos);}
#line 3190 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_extvertxpos_no_string: /* extvertxpos_no_string  */
#line 194 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).extvertxpos);}
#line 3196 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_symbol_type_string: /* symbol_type_string  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3202 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_symbol_command_no_attr: /* symbol_command_no_attr  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 3208 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_symbol_command: /* symbol_command  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 3214 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_note: /* note  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 3220 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_comment: /* comment  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 3226 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_colon_string: /* colon_string  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3232 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_full_arcattrlist_with_label: /* full_arcattrlist_with_label  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attriblist);}
#line 3238 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_full_arcattrlist: /* full_arcattrlist  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attriblist);}
#line 3244 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arcattrlist: /* arcattrlist  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attriblist);}
#line 3250 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_arcattr: /* arcattr  */
#line 193 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attrib);}
#line 3256 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string_single: /* entity_string_single  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3262 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_reserved_word_string: /* reserved_word_string  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3268 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_symbol_string: /* symbol_string  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3274 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_alpha_string_single: /* alpha_string_single  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3280 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_string_single: /* string_single  */
#line 196 "msc_lang.yy"
            {free(((*yyvaluep).str));}
#line 3286 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_tok_param_name_as_multi: /* tok_param_name_as_multi  */
#line 197 "msc_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3292 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_multi_string_continuation: /* multi_string_continuation  */
#line 197 "msc_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3298 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string_single_or_param: /* entity_string_single_or_param  */
#line 197 "msc_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3304 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string: /* entity_string  */
#line 197 "msc_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3310 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_alpha_string: /* alpha_string  */
#line 197 "msc_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3316 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_string: /* string  */
#line 197 "msc_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3322 "msc_csh_lang.cc"
        break;

    case YYSYMBOL_scope_close: /* scope_close  */
#line 191 "msc_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).arcbase);}
#line 3328 "msc_csh_lang.cc"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */


/* User initialization code.  */
#line 8 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    yylloc.first_pos = 0;
    yylloc.last_pos = 0;
  #endif
}

#line 3430 "msc_csh_lang.cc"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, yyscanner);
    }

  if (yychar <= TOK_EOF)
    {
      yychar = TOK_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* msc_with_bye: msc eof  */
#line 260 "msc_lang.yy"
{
	YYACCEPT;
}
#line 3645 "msc_csh_lang.cc"
    break;

  case 4: /* eof: TOK_BYE  */
#line 266 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
	csh.AddCSH_AllCommentBeyond((yylsp[0]));
  #else
  #endif
    free((yyvsp[0].str));
}
#line 3658 "msc_csh_lang.cc"
    break;

  case 5: /* eof: TOK_BYE TOK_SEMICOLON  */
#line 275 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
	csh.AddCSH_AllCommentBeyond((yylsp[0]));
  #else
  #endif
    free((yyvsp[-1].str));
}
#line 3672 "msc_csh_lang.cc"
    break;

  case 6: /* msc: %empty  */
#line 286 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddLineBeginToHints();
    csh.hintStatus = HINT_READY;
    csh.hintSource = EHintSourceType::LINE_START;
    csh.hintsForcedOnly = true;
  #else
    //no action for empty file
  #endif
}
#line 3687 "msc_csh_lang.cc"
    break;

  case 7: /* msc: msckey braced_arclist  */
#line 297 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBefore((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        csh.AddDesignsToHints(true);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1]))) {
        csh.AddOptionsToHints();
        csh.AllowAnything();
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.AddArcs((yyvsp[0].arclist));
  #endif
}
#line 3710 "msc_csh_lang.cc"
    break;

  case 8: /* msc: TOK_MSC error eof  */
#line 316 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBefore((yylsp[-2])) || csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[-1]), "Missing an equal sign or a list of elements between braces ('{' and '}').");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing an equal sign or a list of elements between braces ('{' and '}').");
  #endif
    free((yyvsp[-2].str));
	YYACCEPT; //We should noty parse further in msc_with_bye as we may have something beyond BYE (in eof)
}
#line 3729 "msc_csh_lang.cc"
    break;

  case 9: /* msc: top_level_arclist eof  */
#line 331 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBefore((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    chart.AddArcs((yyvsp[-1].arclist));
  #endif
	YYACCEPT; //We should not parse further in msc_with_bye as we may have something beyond BYE (in eof)
}
#line 3745 "msc_csh_lang.cc"
    break;

  case 10: /* msc: top_level_arclist error eof  */
#line 343 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBefore((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    CshPos pos = (yylsp[-1]);
    if (((yylsp[-2])).last_pos >= ((yylsp[-1])).first_pos)
        pos.first_pos = ((yylsp[-2])).last_pos;
    csh.AddCSH_Error(pos, "Could not recognize this as a valid line.");
  #else
    chart.AddArcs((yyvsp[-2].arclist));
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Could not recognize this as a valid line.");
  #endif
	YYACCEPT; //We should noty parse further in msc_with_bye as we may have something beyond BYE (in eof)
}
#line 3766 "msc_csh_lang.cc"
    break;

  case 12: /* top_level_arclist: arclist_maybe_no_semicolon TOK_CCBRACKET  */
#line 362 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Closing brace missing its opening pair.");
  #else
    (yyval.arclist) = (yyvsp[-1].arclist);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Unexpected '}'.");
  #endif
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3780 "msc_csh_lang.cc"
    break;

  case 13: /* top_level_arclist: arclist_maybe_no_semicolon TOK_CCBRACKET top_level_arclist  */
#line 372 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Closing brace missing its opening pair.");
  #else
    //Merge $3 into $1
    ((yyvsp[-2].arclist))->splice(((yyvsp[-2].arclist))->end(), *((yyvsp[0].arclist)));
    delete ((yyvsp[0].arclist));
    (yyval.arclist) = (yyvsp[-2].arclist);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Unexpected '}'.");
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3797 "msc_csh_lang.cc"
    break;

  case 14: /* msckey: TOK_MSC  */
#line 387 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.SwitchToMscgenCompatMode();
  #else
    chart.SwitchToMscgenCompatMode();
  #endif
    free((yyvsp[0].str));
}
#line 3811 "msc_csh_lang.cc"
    break;

  case 15: /* msckey: TOK_MSC TOK_EQUAL  */
#line 397 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a design name.");
    csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, "msc");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing design name.");
  #endif
    free((yyvsp[-1].str));
}
#line 3828 "msc_csh_lang.cc"
    break;

  case 16: /* msckey: TOK_MSC TOK_EQUAL string  */
#line 410 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_DESIGNNAME);
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, "msc");
    csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_VALUE, "msc");
    if (!csh.SkipContent()) {
        std::string msg = csh.SetDesignTo((yyvsp[0].multi_str).str, true);
        if (msg.length())
            csh.AddCSH_Error((yylsp[0]), std::move(msg));
    }
  #else
    if (!(yyvsp[0].multi_str).had_error && !chart.SkipContent()) {
        ArcBase *dummy = chart.AddAttribute(Attribute("msc", (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0]))));
        if (dummy) delete dummy;
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 3855 "msc_csh_lang.cc"
    break;

  case 17: /* braced_arclist: scope_open arclist_maybe_no_semicolon scope_close  */
#line 434 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    if ((yyvsp[0].arcbase)) ((yyvsp[-1].arclist))->Append((yyvsp[0].arcbase)); //Append any potential SetNumbering
    (yyval.arclist) = (yyvsp[-1].arclist);
  #endif
}
#line 3868 "msc_csh_lang.cc"
    break;

  case 18: /* braced_arclist: scope_open scope_close  */
#line 443 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    (yyval.arclist) = new ArcList;
    //scope_close should not return here with a SetNumbering
    //but just in case
    if ((yyvsp[0].arcbase))
        delete((yyvsp[0].arcbase));
  #endif
}
#line 3884 "msc_csh_lang.cc"
    break;

  case 19: /* braced_arclist: scope_open arclist_maybe_no_semicolon error scope_close  */
#line 455 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    if ((yyvsp[0].arcbase)) ((yyvsp[-2].arclist))->Append((yyvsp[0].arcbase)); //Append any potential SetNumbering
    (yyval.arclist) = (yyvsp[-2].arclist);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "syntax error.");
  #endif
    yyerrok;
}
#line 3900 "msc_csh_lang.cc"
    break;

  case 20: /* braced_arclist: scope_open arclist_maybe_no_semicolon error TOK_EOF  */
#line 467 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    (yyval.arclist) = (yyvsp[-2].arclist);
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_START((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
}
#line 3916 "msc_csh_lang.cc"
    break;

  case 21: /* braced_arclist: scope_open arclist_maybe_no_semicolon TOK_EOF  */
#line 479 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
  #else
    (yyval.arclist) = (yyvsp[-1].arclist);
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
}
#line 3932 "msc_csh_lang.cc"
    break;

  case 22: /* braced_arclist: scope_open TOK_EOF  */
#line 491 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
  #else
    (yyval.arclist) = nullptr;
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
}
#line 3947 "msc_csh_lang.cc"
    break;

  case 23: /* braced_arclist: scope_open arclist_maybe_no_semicolon TOK_BYE  */
#line 502 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
  #else
    (yyval.arclist) = (yyvsp[-1].arclist);
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
  #endif
  free((yyvsp[0].str));
}
#line 3964 "msc_csh_lang.cc"
    break;

  case 24: /* braced_arclist: scope_open TOK_BYE  */
#line 515 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
  #else
    (yyval.arclist) = nullptr;
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
  free((yyvsp[0].str));
}
#line 3980 "msc_csh_lang.cc"
    break;

  case 25: /* defproc: defprochelp1  */
#line 529 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procdefhelper)) {
        if (chart.SkipContent()) {
            chart.Error.Error(CHART_POS_START((yyloc)), "Cannot define procedures inside a procedure.");
        } else if ((yyvsp[0].procdefhelper)->name.had_error) {
            //do nothing, error already reported
        } else if ((yyvsp[0].procdefhelper)->name.str==nullptr || (yyvsp[0].procdefhelper)->name.str[0]==0) {
            chart.Error.Error((yyvsp[0].procdefhelper)->linenum_name, "Missing a procedure name to (re-)define. Ignoring this.");
        } else if (!(yyvsp[0].procdefhelper)->had_error && (yyvsp[0].procdefhelper)->body) {
            if (chart.MyCurrentContext().num_error != chart.Error.GetErrorNum(true, false)) {
                chart.Error.Error(CHART_POS_START((yyloc)), "There are warnings or errors inside the procedure definition. Ignoring it.");
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].name = (yyvsp[0].procdefhelper)->name.str;
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].status = EDefProcResult::PROBLEM;
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].file_pos = (yyvsp[0].procdefhelper)->linenum_body;
            } else if ((yyvsp[0].procdefhelper)->body->status==EDefProcResult::OK || (yyvsp[0].procdefhelper)->body->status==EDefProcResult::EMPTY) {
                if ((yyvsp[0].procdefhelper)->parameters) {
                    auto &p = chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str] = *(yyvsp[0].procdefhelper)->body;
                    p.name = (yyvsp[0].procdefhelper)->name.str;
                    p.parameters = std::move(*(yyvsp[0].procdefhelper)->parameters);
                    if ((yyvsp[0].procdefhelper)->attrs) for (auto &a : *(yyvsp[0].procdefhelper)->attrs)
                        p.AddAttribute(*a, chart);
                    if ((yyvsp[0].procdefhelper)->body->status==EDefProcResult::EMPTY)
                        chart.Error.Warning((yyvsp[0].procdefhelper)->linenum_body, "Empty procedure. Is this what you want?");
                } else {
                     chart.Error.Error(CHART_POS_START((yyloc)), "Ill-formed procedure parameter list. Ignoring this procedure definition.");
                }
            } else {
                 chart.Error.Error(CHART_POS_START((yyloc)), "Ill-formed procedure body. Ignoring this procedure definition.");
            }
        }
        delete (yyvsp[0].procdefhelper);
    }
  #endif
}
#line 4021 "msc_csh_lang.cc"
    break;

  case 26: /* defprochelp1: TOK_COMMAND_DEFPROC  */
#line 568 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.SkipContent()) {
        csh.AddCSH_Error((yylsp[0]), "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing procedure name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->linenum_name = CHART_POS_AFTER((yyloc));
  #endif
    free((yyvsp[0].str));
}
#line 4044 "msc_csh_lang.cc"
    break;

  case 27: /* defprochelp1: TOK_COMMAND_DEFPROC defprochelp2  */
#line 587 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.SkipContent()) {
        csh.AddCSH_Error((yylsp[-1]), "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    }
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
  #endif
    free((yyvsp[-1].str));
}
#line 4065 "msc_csh_lang.cc"
    break;

  case 28: /* defprochelp2: alpha_string  */
#line 605 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PROCNAME);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
    free((yyvsp[0].multi_str).str);
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->name = (yyvsp[0].multi_str);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[0]));
  #endif
}
#line 4081 "msc_csh_lang.cc"
    break;

  case 29: /* defprochelp2: alpha_string defprochelp3  */
#line 617 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PROCNAME);
    free((yyvsp[-1].multi_str).str);
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->name = (yyvsp[-1].multi_str);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[-1]));
  #endif
}
#line 4096 "msc_csh_lang.cc"
    break;

  case 30: /* defprochelp2: defprochelp3  */
#line 628 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(CshPos((yylsp[0]).first_pos, (yylsp[0]).first_pos), "Missing procedure name.");
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[0]));
  #endif
}
#line 4109 "msc_csh_lang.cc"
    break;

  case 31: /* defprochelp3: proc_def_arglist_tested  */
#line 638 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->parameters = (yyvsp[0].procparamdeflist);
  #endif
}
#line 4122 "msc_csh_lang.cc"
    break;

  case 32: /* defprochelp3: proc_def_arglist_tested defprochelp4  */
#line 647 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->parameters = (yyvsp[-1].procparamdeflist);
  #endif
}
#line 4134 "msc_csh_lang.cc"
    break;

  case 33: /* defprochelp3: defprochelp4  */
#line 655 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->parameters = new ProcParamDefList;
  #endif
}
#line 4146 "msc_csh_lang.cc"
    break;

  case 34: /* defprochelp4: full_arcattrlist  */
#line 664 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        csh.AddYesNoToHints();
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->attrs = (yyvsp[0].attriblist);
  #endif
}
#line 4165 "msc_csh_lang.cc"
    break;

  case 35: /* defprochelp4: full_arcattrlist procedure_body  */
#line 679 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        csh.AddYesNoToHints();
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->body = (yyvsp[0].procedure);
    (yyval.procdefhelper)->linenum_body = CHART_POS_START((yylsp[0]));
    (yyval.procdefhelper)->attrs = (yyvsp[-1].attriblist);
  #endif
}
#line 4185 "msc_csh_lang.cc"
    break;

  case 36: /* defprochelp4: procedure_body  */
#line 695 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->body = (yyvsp[0].procedure);
    (yyval.procdefhelper)->linenum_body = CHART_POS_START((yylsp[0]));
  #endif
}
#line 4198 "msc_csh_lang.cc"
    break;

  case 37: /* scope_open_proc_body: TOK_OCBRACKET  */
#line 706 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    chart.MyCurrentContext().num_error = chart.Error.GetErrorNum(true, false);
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT, EContextCreate::EMPTY);  //We have all the styles, but empty
    chart.MyCurrentContext().parameters = std::move(YYGET_EXTRA(yyscanner)->last_procedure_params);
    chart.MyCurrentContext().starts_procedure = true;
    _ASSERT(YYGET_EXTRA(yyscanner)->open_context_mode == base_parse_parm::EScopeOpenMode::NORMAL);
    YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
  #endif
    (yyval.input_text_ptr) = (yyvsp[0].input_text_ptr);
}
#line 4217 "msc_csh_lang.cc"
    break;

  case 38: /* scope_close_proc_body: TOK_CCBRACKET  */
#line 722 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
  #else
    chart.PopContext();
  #endif
    (yyval.input_text_ptr) = (yyvsp[0].input_text_ptr);
}
#line 4231 "msc_csh_lang.cc"
    break;

  case 39: /* proc_def_arglist_tested: proc_def_arglist  */
#line 733 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparamdeflist)) {
        auto pair = Procedure::AreAllParameterNamesUnique(*(yyvsp[0].procparamdeflist));
        if (pair.first) {
            chart.Error.Error(pair.second->linenum_name, "This parameter name is already used.");
            chart.Error.Error(pair.first->linenum_name, pair.second->linenum_name, "This parameter name is already used.");
            delete (yyvsp[0].procparamdeflist);
            (yyval.procparamdeflist) = nullptr;
        } else {
            //Also copy to YYGET_EXTRA(yyscanner)->last_procedure_params and set open_context_mode
            auto &store = YYGET_EXTRA(yyscanner)->last_procedure_params;
            store.clear();
            for (const auto &p : *(yyvsp[0].procparamdeflist))
                store.emplace(p->name, ProcParamResolved(std::string(), FileLineCol(), true));
            (yyval.procparamdeflist) = (yyvsp[0].procparamdeflist);
        }
    } else
        (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4258 "msc_csh_lang.cc"
    break;

  case 40: /* proc_def_arglist: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 757 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparamdeflist) = new ProcParamDefList;
  #endif
}
#line 4271 "msc_csh_lang.cc"
    break;

  case 41: /* proc_def_arglist: TOK_OPARENTHESIS error TOK_CPARENTHESIS  */
#line 766 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter definitions.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter definitions.", "Say something like '($first, $second=default)'.");
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4286 "msc_csh_lang.cc"
    break;

  case 42: /* proc_def_arglist: TOK_OPARENTHESIS  */
#line 777 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter list closed by a parenthesis ')'.");
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4300 "msc_csh_lang.cc"
    break;

  case 43: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list error TOK_CPARENTHESIS  */
#line 787 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter definitions.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter definitions.");
    delete (yyvsp[-2].procparamdeflist);
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4316 "msc_csh_lang.cc"
    break;

  case 44: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list  */
#line 799 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing closing parenthesis ')'.");
    delete (yyvsp[0].procparamdeflist);
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4331 "msc_csh_lang.cc"
    break;

  case 45: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list TOK_CPARENTHESIS  */
#line 810 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparamdeflist) = (yyvsp[-1].procparamdeflist);
  #endif
}
#line 4344 "msc_csh_lang.cc"
    break;

  case 46: /* proc_def_param_list: proc_def_param  */
#line 820 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparamdef)) {
        (yyval.procparamdeflist) = new ProcParamDefList;
        ((yyval.procparamdeflist))->Append((yyvsp[0].procparamdef));
    } else
        (yyval.procparamdeflist)= nullptr;
  #endif
}
#line 4359 "msc_csh_lang.cc"
    break;

  case 47: /* proc_def_param_list: proc_def_param_list TOK_COMMA  */
#line 831 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter after the comma.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter after the comma.");
    delete (yyvsp[-1].procparamdeflist);
    (yyval.procparamdeflist)= nullptr;
  #endif
}
#line 4374 "msc_csh_lang.cc"
    break;

  case 48: /* proc_def_param_list: proc_def_param_list TOK_COMMA proc_def_param  */
#line 842 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[-2].procparamdeflist) && (yyvsp[0].procparamdef)) {
        ((yyvsp[-2].procparamdeflist))->Append((yyvsp[0].procparamdef));
        (yyval.procparamdeflist) = (yyvsp[-2].procparamdeflist);
    } else {
        delete (yyvsp[-2].procparamdeflist);
        delete (yyvsp[0].procparamdef);
        (yyval.procparamdeflist)= nullptr;
    }
  #endif
}
#line 4393 "msc_csh_lang.cc"
    break;

  case 49: /* proc_def_param: TOK_PARAM_NAME  */
#line 858 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].str) && (yyvsp[0].str)[0]=='$' && (yyvsp[0].str)[1])
        csh.AddCSH((yylsp[0]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[0]), "Need name after the '$' sign.");
    (yyval.procparamdef) = nullptr; //no value
  #else
    if ((yyvsp[0].str) && (yyvsp[0].str)[0]=='$' && (yyvsp[0].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[0].str), CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[0].str));
}
#line 4415 "msc_csh_lang.cc"
    break;

  case 50: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL  */
#line 876 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if ((yyvsp[-1].str) && (yyvsp[-1].str)[0]=='$' && (yyvsp[-1].str)[1])
        csh.AddCSH((yylsp[-1]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-1]), "Need name after the '$' sign.");
    (yyval.procparamdef) = nullptr; //no value
  #else
    if ((yyvsp[-1].str) && (yyvsp[-1].str)[0]=='$' && (yyvsp[-1].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-1].str));
}
#line 4438 "msc_csh_lang.cc"
    break;

  case 51: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL string  */
#line 895 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1])
        csh.AddCSH((yylsp[-2]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-2]), "Need name after the '$' sign.");
    (yyval.procparamdef) = (char*)1; //has value
  #else
    if ((yyvsp[0].multi_str).had_error) {
        (yyval.procparamdef) = nullptr;
    } else if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-2].str), CHART_POS_START((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-2])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 4464 "msc_csh_lang.cc"
    break;

  case 52: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL TOK_NUMBER  */
#line 917 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1])
        csh.AddCSH((yylsp[-2]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-2]), "Need name after the '$' sign.");
    (yyval.procparamdef) = (char*)1; //has value
  #else
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-2].str), CHART_POS_START((yylsp[-2])), (yyvsp[0].str), CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-2])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 4488 "msc_csh_lang.cc"
    break;

  case 53: /* procedure_body: scope_open_proc_body arclist_maybe_no_semicolon scope_close_proc_body  */
#line 941 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::OK;
    tmp->text = std::string(((yyvsp[-2].input_text_ptr)), ((yyvsp[0].input_text_ptr))+1)+";";
    tmp->file_pos = CHART_POS_START((yyloc));
    if ((yyvsp[-1].arclist))
        delete (yyvsp[-1].arclist);
    (yyval.procedure) = tmp;
  #endif
}
#line 4506 "msc_csh_lang.cc"
    break;

  case 54: /* procedure_body: scope_open_proc_body scope_close_proc_body  */
#line 955 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::EMPTY;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 4523 "msc_csh_lang.cc"
    break;

  case 55: /* procedure_body: scope_open_proc_body arclist_maybe_no_semicolon error scope_close_proc_body  */
#line 968 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "syntax error.");
    if ((yyvsp[-2].arclist))
        delete (yyvsp[-2].arclist);
  #endif
    yyerrok;
  (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 4545 "msc_csh_lang.cc"
    break;

  case 56: /* procedure_body: scope_open_proc_body arclist_maybe_no_semicolon error TOK_EOF  */
#line 986 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_START((yylsp[-1])), "Here is the corresponding '{'.");
    if ((yyvsp[-2].arclist))
        delete (yyvsp[-2].arclist);
  #endif
  (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 4568 "msc_csh_lang.cc"
    break;

  case 57: /* procedure_body: scope_open_proc_body arclist_maybe_no_semicolon TOK_EOF  */
#line 1005 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
    if ((yyvsp[-1].arclist))
        delete (yyvsp[-1].arclist);
  #endif
  (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 4591 "msc_csh_lang.cc"
    break;

  case 58: /* procedure_body: scope_open_proc_body TOK_EOF  */
#line 1024 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 4611 "msc_csh_lang.cc"
    break;

  case 59: /* procedure_body: scope_open_proc_body arclist_maybe_no_semicolon TOK_BYE  */
#line 1040 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
    if ((yyvsp[-1].arclist))
        delete (yyvsp[-1].arclist);
  #endif
  (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
  free((yyvsp[0].str));
}
#line 4635 "msc_csh_lang.cc"
    break;

  case 60: /* procedure_body: scope_open_proc_body TOK_BYE  */
#line 1060 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
  free((yyvsp[0].str));
}
#line 4656 "msc_csh_lang.cc"
    break;

  case 61: /* set: TOK_COMMAND_SET proc_def_param  */
#line 1078 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].procparamdef))
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing value.");
  #else
    if (!chart.SkipContent())
        chart.SetVariable((yyvsp[0].procparamdef), CHART_POS((yyloc)));
    else
        delete (yyvsp[0].procparamdef);
  #endif
    free((yyvsp[-1].str));
}
#line 4674 "msc_csh_lang.cc"
    break;

  case 62: /* set: TOK_COMMAND_SET  */
#line 1092 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing variable or parameter name to set.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing variable or parameter name to set.");
  #endif
    free((yyvsp[0].str));
}
#line 4688 "msc_csh_lang.cc"
    break;

  case 64: /* arclist_maybe_no_semicolon: arclist arc_with_parallel  */
#line 1105 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yylsp[0]));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a semicolon (';').");
  #else
    if ((yyvsp[0].arcbase)) {
		((yyvsp[-1].arclist))->Append((yyvsp[0].arcbase));
		((yyvsp[0].arcbase))->SetLineEnd(CHART_POS((yylsp[0])));
	}
    (yyval.arclist) = (yyvsp[-1].arclist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
  #endif
}
#line 4707 "msc_csh_lang.cc"
    break;

  case 65: /* arclist_maybe_no_semicolon: arc_with_parallel  */
#line 1120 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yylsp[0]));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a semicolon (';').");
  #else
    if ((yyvsp[0].arcbase)) {
		((yyvsp[0].arcbase))->SetLineEnd(CHART_POS((yylsp[0])));
        (yyval.arclist) = (new ArcList)->Append((yyvsp[0].arcbase)); /* New list */
    } else
        (yyval.arclist) = new ArcList;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
  #endif
}
#line 4726 "msc_csh_lang.cc"
    break;

  case 66: /* arclist: arc_with_parallel_semicolon  */
#line 1137 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].arcbase))
        (yyval.arclist) = (new ArcList)->Append((yyvsp[0].arcbase)); /* New list */
    else
        (yyval.arclist) = new ArcList;
  #endif
}
#line 4739 "msc_csh_lang.cc"
    break;

  case 67: /* arclist: arclist arc_with_parallel_semicolon  */
#line 1146 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].arcbase)) ((yyvsp[-1].arclist))->Append((yyvsp[0].arcbase));     /* Add to existing list */
    (yyval.arclist) = ((yyvsp[-1].arclist));
  #endif
}
#line 4750 "msc_csh_lang.cc"
    break;

  case 68: /* arc_with_parallel_semicolon: arc_with_parallel TOK_SEMICOLON  */
#line 1154 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].arcbase))
		((yyvsp[-1].arcbase))->SetLineEnd(CHART_POS((yyloc)));
    (yyval.arcbase)=(yyvsp[-1].arcbase);
  #endif
}
#line 4769 "msc_csh_lang.cc"
    break;

  case 69: /* arc_with_parallel_semicolon: TOK_SEMICOLON  */
#line 1169 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcbase)=nullptr;
  #endif
}
#line 4786 "msc_csh_lang.cc"
    break;

  case 70: /* arc_with_parallel_semicolon: arc_with_parallel error TOK_SEMICOLON  */
#line 1182 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-2].arcbase))
		((yyvsp[-2].arcbase))->SetLineEnd(CHART_POS((yylsp[-2])));
    (yyval.arcbase)=(yyvsp[-2].arcbase);
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
  #endif
}
#line 4808 "msc_csh_lang.cc"
    break;

  case 71: /* arc_with_parallel_semicolon: proc_invocation TOK_SEMICOLON  */
#line 1200 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-1].cprocedure)) {
        auto ctx = ((yyvsp[-1].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[-1])), &chart);
        if (!ctx.first) {
            MscPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-1].cprocedure))->text.c_str(), ((yyvsp[-1].cprocedure))->text.length(), &((yylsp[-1])), ((yyvsp[-1].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-1].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.arcbase) = nullptr;
  #endif
}
#line 4829 "msc_csh_lang.cc"
    break;

  case 72: /* arc_with_parallel_semicolon: proc_invocation error TOK_SEMICOLON  */
#line 1217 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-2].cprocedure)) {
        auto ctx = ((yyvsp[-2].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[-2])), &chart);
        if (!ctx.first) {
            MscPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-2].cprocedure))->text.c_str(), ((yyvsp[-2].cprocedure))->text.length(), &((yylsp[-2])), ((yyvsp[-2].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-2].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.arcbase) = nullptr;
  #endif
}
#line 4857 "msc_csh_lang.cc"
    break;

  case 73: /* arc_with_parallel_semicolon: proc_invocation  */
#line 1241 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[0].cprocedure)) {
        auto ctx = ((yyvsp[0].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[0])), &chart);
        if (!ctx.first) {
            MscPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[0].cprocedure))->text.c_str(), ((yyvsp[0].cprocedure))->text.length(), &((yylsp[0])), ((yyvsp[0].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[0].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.arcbase) = nullptr;
  #endif
}
#line 4880 "msc_csh_lang.cc"
    break;

  case 74: /* arc_with_parallel_semicolon: proc_invocation proc_param_list TOK_SEMICOLON  */
#line 1260 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-2].cprocedure) && (yyvsp[-1].procparaminvoclist)) {
        auto ctx = ((yyvsp[-2].cprocedure))->MatchParameters((yyvsp[-1].procparaminvoclist), CHART_POS((yylsp[-1])).end, &chart);
        if (!ctx.first) {
            MscPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-2].cprocedure))->text.c_str(), ((yyvsp[-2].cprocedure))->text.length(), &((yylsp[-2])), ((yyvsp[-2].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-2].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else {
        delete (yyvsp[-1].procparaminvoclist);
    }
    (yyval.arcbase) = nullptr;
  #endif
}
#line 4907 "msc_csh_lang.cc"
    break;

  case 75: /* arc_with_parallel_semicolon: proc_invocation proc_param_list error TOK_SEMICOLON  */
#line 1283 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-3].cprocedure) && (yyvsp[-2].procparaminvoclist)) {
        auto ctx = ((yyvsp[-3].cprocedure))->MatchParameters((yyvsp[-2].procparaminvoclist), CHART_POS((yylsp[-2])).end, &chart);
        if (!ctx.first) {
            MscPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-3].cprocedure))->text.c_str(), ((yyvsp[-3].cprocedure))->text.length(), &((yylsp[-3])), ((yyvsp[-3].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-3].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else {
        delete (yyvsp[-2].procparaminvoclist);
    }
    (yyval.arcbase) = nullptr;
  #endif
}
#line 4937 "msc_csh_lang.cc"
    break;

  case 76: /* arc_with_parallel_semicolon: proc_invocation proc_param_list  */
#line 1309 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-1].cprocedure) && (yyvsp[0].procparaminvoclist)) {
        auto ctx = ((yyvsp[-1].cprocedure))->MatchParameters((yyvsp[0].procparaminvoclist), CHART_POS((yylsp[0])).end, &chart);
        if (!ctx.first) {
            MscPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-1].cprocedure))->text.c_str(), ((yyvsp[-1].cprocedure))->text.length(), &((yylsp[-1])), ((yyvsp[-1].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-1].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else
        delete (yyvsp[0].procparaminvoclist);
    (yyval.arcbase) = nullptr;
  #endif
}
#line 4961 "msc_csh_lang.cc"
    break;

  case 77: /* arc_with_parallel_semicolon: include TOK_SEMICOLON  */
#line 1329 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].str)) {
        auto text = chart.Include((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
        if (text.first && text.first->length() && text.second.IsValid())
            MscPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-1])), text.second, EInclusionReason::INCLUDE);
    }
    (yyval.arcbase) = nullptr;
  #endif
    if ((yyvsp[-1].str)) free((yyvsp[-1].str));
}
#line 4983 "msc_csh_lang.cc"
    break;

  case 78: /* arc_with_parallel_semicolon: include  */
#line 1347 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    if ((yyvsp[0].str)) {
        auto text = chart.Include((yyvsp[0].str), CHART_POS_START((yylsp[0])));
        if (text.first && text.first->length() && text.second.IsValid())
            MscPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[0])), text.second, EInclusionReason::INCLUDE);
    }
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    (yyval.arcbase) = nullptr;
  #endif
    if ((yyvsp[0].str)) free((yyvsp[0].str));
}
#line 5003 "msc_csh_lang.cc"
    break;

  case 79: /* arc_with_parallel_semicolon: include error TOK_SEMICOLON  */
#line 1363 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-2].str)) {
        auto text = chart.Include((yyvsp[-2].str), CHART_POS_START((yylsp[-2])));
        if (text.first && text.first->length() && text.second.IsValid())
            MscPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-2])), text.second, EInclusionReason::INCLUDE);
    }
    (yyval.arcbase) = nullptr;
  #endif
    if ((yyvsp[-2].str)) free((yyvsp[-2].str));
}
#line 5028 "msc_csh_lang.cc"
    break;

  case 80: /* proc_invocation: TOK_COMMAND_REPLAY  */
#line 1385 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing procedure name.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing procedure name.");
    (yyval.cprocedure) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 5043 "msc_csh_lang.cc"
    break;

  case 81: /* proc_invocation: TOK_COMMAND_REPLAY alpha_string  */
#line 1396 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_PROCNAME);
  #else
    (yyval.cprocedure) = nullptr;
    if (!(yyvsp[0].multi_str).had_error) {
        auto proc = chart.GetProcedure((yyvsp[0].multi_str).str);
        if (proc==nullptr)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Undefined procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::PROBLEM)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Ill-formed procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::OK) {
            //Only return the procedure if we are not inside a procedure definition
            if (!chart.SkipContent())
                (yyval.cprocedure) = proc;
            //else just move on parsing - we do not reparse procedure replays during
            //the definition of an outer procedure.
        }
        //else return null, emit no error for EMPTY
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 5073 "msc_csh_lang.cc"
    break;

  case 82: /* proc_param_list: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 1423 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparaminvoclist) = new ProcParamInvocationList;
  #endif
}
#line 5086 "msc_csh_lang.cc"
    break;

  case 83: /* proc_param_list: TOK_OPARENTHESIS error TOK_CPARENTHESIS  */
#line 1432 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter syntax.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter syntax. Ignoring procedure call.");
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 5101 "msc_csh_lang.cc"
    break;

  case 84: /* proc_param_list: TOK_OPARENTHESIS  */
#line 1443 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter list closed by a parenthesis ')'. Ignoring procedure call.");
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 5115 "msc_csh_lang.cc"
    break;

  case 85: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list error TOK_CPARENTHESIS  */
#line 1453 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter syntax.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter syntax. Ignoring procedure call.");
    delete (yyvsp[-2].procparaminvoclist);
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 5131 "msc_csh_lang.cc"
    break;

  case 86: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list  */
#line 1465 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing closing parenthesis ')'. Ignoring procedure call.");
    delete (yyvsp[0].procparaminvoclist);
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 5146 "msc_csh_lang.cc"
    break;

  case 87: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list TOK_CPARENTHESIS  */
#line 1476 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparaminvoclist) = (yyvsp[-1].procparaminvoclist);
  #endif
}
#line 5159 "msc_csh_lang.cc"
    break;

  case 88: /* proc_invoc_param_list: proc_invoc_param  */
#line 1486 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparaminvoc)) {
        (yyval.procparaminvoclist) = new ProcParamInvocationList;
        ((yyval.procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
    } else
        (yyval.procparaminvoclist)= nullptr;
  #endif
}
#line 5174 "msc_csh_lang.cc"
    break;

  case 89: /* proc_invoc_param_list: TOK_COMMA  */
#line 1497 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
  #else
    (yyval.procparaminvoclist) = new ProcParamInvocationList;
    ((yyval.procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_START((yylsp[0]))));
  #endif
}
#line 5187 "msc_csh_lang.cc"
    break;

  case 90: /* proc_invoc_param_list: TOK_COMMA proc_invoc_param  */
#line 1506 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[0].procparaminvoc)) {
        (yyval.procparaminvoclist) = new ProcParamInvocationList;
        ((yyval.procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_START((yylsp[-1]))));
        ((yyval.procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
    } else
        (yyval.procparaminvoclist)= nullptr;
  #endif
}
#line 5204 "msc_csh_lang.cc"
    break;

  case 91: /* proc_invoc_param_list: proc_invoc_param_list TOK_COMMA  */
#line 1519 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
  #else
    if ((yyvsp[-1].procparaminvoclist))
        ((yyvsp[-1].procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_AFTER((yylsp[0]))));
    (yyval.procparaminvoclist) = (yyvsp[-1].procparaminvoclist);
  #endif
}
#line 5218 "msc_csh_lang.cc"
    break;

  case 92: /* proc_invoc_param_list: proc_invoc_param_list TOK_COMMA proc_invoc_param  */
#line 1529 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[-2].procparaminvoclist) && (yyvsp[0].procparaminvoc)) {
        ((yyvsp[-2].procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
        (yyval.procparaminvoclist) = (yyvsp[-2].procparaminvoclist);
    } else {
        delete (yyvsp[-2].procparaminvoclist);
        delete (yyvsp[0].procparaminvoc);
        (yyval.procparaminvoclist)= nullptr;
    }
  #endif
}
#line 5237 "msc_csh_lang.cc"
    break;

  case 93: /* proc_invoc_param: string  */
#line 1545 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error) {
        //If this is a quoted string, color as a label, else as an attribute value
        if ((yylsp[0]).first_pos>0 && YYGET_EXTRA(yyscanner)->buff.buf[(yylsp[0]).first_pos-1]=='\"')
            csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, {});
        else
            csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.procparaminvoc) = nullptr;
    else
        (yyval.procparaminvoc) = new ProcParamInvocation((yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 5259 "msc_csh_lang.cc"
    break;

  case 94: /* proc_invoc_param: TOK_NUMBER  */
#line 1563 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    //value of number is actually a string (containing digits)
    (yyval.procparaminvoc) = new ProcParamInvocation((yyvsp[0].str), CHART_POS_START((yylsp[0])));
  #endif
    free((yyvsp[0].str));
}
#line 5273 "msc_csh_lang.cc"
    break;

  case 95: /* include: TOK_COMMAND_INCLUDE  */
#line 1574 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a file name to include. You must use quotation marks ('\"').");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints();
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a file name to include. You must use quotation marks ('\"').");
  #endif
    (yyval.str) = nullptr;
    free((yyvsp[0].str));
}
#line 5290 "msc_csh_lang.cc"
    break;

  case 96: /* include: TOK_COMMAND_INCLUDE TOK_QSTRING  */
#line 1587 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_INCLUDEFILE);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints();
    else if(csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints((yyvsp[0].str), (yylsp[0]));
  #endif
    (yyval.str) = (yyvsp[0].str);
    free((yyvsp[-1].str));
}
#line 5307 "msc_csh_lang.cc"
    break;

  case 101: /* arc_with_parallel: overlap_or_parallel arc  */
#line 1605 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckLineStartHintBetween((yylsp[-1]), (yylsp[0]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[0].arcbase)) {
        if (CaseInsensitiveEqual((yyvsp[-1].str), "parallel"))
            ((yyvsp[0].arcbase))->SetParallel();
        else if (CaseInsensitiveEqual((yyvsp[-1].str), "overlap"))
            ((yyvsp[0].arcbase))->SetOverlap();
        else if (CaseInsensitiveEqual((yyvsp[-1].str), "join"))
            ((yyvsp[0].arcbase))->SetJoin(CHART_POS((yylsp[-1])));
        else {
            _ASSERT(0);
        }
    }
    (yyval.arcbase) = (yyvsp[0].arcbase);
  #endif
    free((yyvsp[-1].str));
}
#line 5338 "msc_csh_lang.cc"
    break;

  case 102: /* arc_with_parallel: overlap_or_parallel  */
#line 1632 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 5358 "msc_csh_lang.cc"
    break;

  case 103: /* arc: arcrel  */
#line 1649 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].arcbase))
        ((yyvsp[0].arcbase))->AddAttributeList(nullptr);
    (yyval.arcbase)=((yyvsp[0].arcbase));
  #endif
}
#line 5371 "msc_csh_lang.cc"
    break;

  case 104: /* arc: arcrel full_arcattrlist_with_label  */
#line 1658 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
  #else
    if ((yyvsp[-1].arcbase))
        ((yyvsp[-1].arcbase))->AddAttributeList((yyvsp[0].attriblist));
    (yyval.arcbase) = ((yyvsp[-1].arcbase));
  #endif
}
#line 5388 "msc_csh_lang.cc"
    break;

  case 105: /* arc: TOK_COMMAND_BIG  */
#line 1671 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintAfter((yylsp[0])))
        csh.AllowAnything();
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an arrow specification.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing an arrow specification.");
  #endif
    free((yyvsp[0].str));
    (yyval.arcbase) = nullptr;
}
#line 5408 "msc_csh_lang.cc"
    break;

  case 106: /* arc: TOK_COMMAND_BIG arcrel  */
#line 1687 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintAt((yylsp[0])) || csh.CheckEntityHintBetween((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
  #else
    //Returns nullptr, if BIG is before a self-pointing arrow
    ArcBase *arc = chart.CreateBlockArrow((yyvsp[0].arcbase));
    if (arc)
        arc->AddAttributeList(nullptr);
    delete (yyvsp[0].arcbase);
    (yyval.arcbase) = arc;
  #endif
    free((yyvsp[-1].str));
}
#line 5431 "msc_csh_lang.cc"
    break;

  case 107: /* arc: TOK_COMMAND_BIG arcrel full_arcattrlist_with_label  */
#line 1706 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockArrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockArrow::AttributeValues(csh.hintAttrName, csh);
    else if (csh.CheckEntityHintAt((yylsp[-1])) || csh.CheckEntityHintBetween((yylsp[-2]), (yylsp[-1])))
        csh.AllowAnything();
  #else
    //Returns nullptr, if BIG is before a self-pointing arrow
    BlockArrow *arrow = chart.CreateBlockArrow((yyvsp[-1].arcbase));
    if (arrow) arrow->AddAttributeList((yyvsp[0].attriblist));
    (yyval.arcbase) = arrow;
    delete (yyvsp[-1].arcbase);
  #endif
    free((yyvsp[-2].str));
}
#line 5457 "msc_csh_lang.cc"
    break;

  case 108: /* arc: TOK_VERTICAL  */
#line 1728 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVerticalTypesToHints();
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.addMarkersAtEnd = true;
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a marker or one of 'brace', 'bracket', 'range', 'box' or an arrow or box symbol, such as '->' or '--'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a marker or one of 'brace', 'bracket', 'range', 'box' or an arrow or box symbol, such as '->' or '--'.");
  #endif
    free((yyvsp[0].str));
    (yyval.arcbase) = nullptr;
}
#line 5481 "msc_csh_lang.cc"
    break;

  case 109: /* arc: TOK_VERTICAL vertrel  */
#line 1748 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVerticalTypesToHints();
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.addMarkersAtEnd = true;
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
  #else
    if ((yyvsp[0].arcvertarrow)) {
      ((yyvsp[0].arcvertarrow))->SetVerticalShape(Vertical::ARROW);
      ((yyvsp[0].arcvertarrow))->AddAttributeList(nullptr);
      (yyval.arcbase) = ((yyvsp[0].arcvertarrow));
    } else (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 5507 "msc_csh_lang.cc"
    break;

  case 110: /* arc: TOK_VERTICAL vertical_shape vertrel  */
#line 1770 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddVerticalTypesToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[0].arcvertarrow)) {
      ((yyvsp[0].arcvertarrow))->SetVerticalShape((yyvsp[-1].vshape));
      ((yyvsp[0].arcvertarrow))->AddAttributeList(nullptr);
      (yyval.arcbase) = ((yyvsp[0].arcvertarrow));
    } else (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-2].str));
}
#line 5532 "msc_csh_lang.cc"
    break;

  case 111: /* arc: TOK_VERTICAL vertical_shape  */
#line 1791 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVerticalTypesToHints();
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.addMarkersAtEnd = true;
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::MARKER)) {
        csh.hintStatus = HINT_FILLING;
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
    }
#else
	ArcTypePlusDir typeplusdir;
	typeplusdir.arc.type = (yyvsp[0].vshape)==Vertical::BOX ? EArcSymbol::BOX_SOLID : EArcSymbol::ARC_SOLID;
	typeplusdir.arc.lost = EArrowLost::NOT;
	typeplusdir.dir = EDirType::RIGHT;
	Vertical *ava = new Vertical(&typeplusdir, MARKER_HERE_STR, MARKER_HERE_STR, &chart);
	VertXPos vxp;
	ava->AddXpos(&vxp);
    ava->SetVerticalShape((yyvsp[0].vshape));
    ava->AddAttributeList(nullptr);
	(yyval.arcbase) = ava;
  #endif
    free((yyvsp[-1].str));
}
#line 5567 "msc_csh_lang.cc"
    break;

  case 112: /* arc: TOK_VERTICAL vertrel full_arcattrlist_with_label  */
#line 1822 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddVerticalTypesToHints();
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.addMarkersAtEnd = true;
        csh.hintStatus = HINT_READY;
    } if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Vertical::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Vertical::AttributeValues(csh.hintAttrName, csh, Vertical::ARROW);
  #else
    if ((yyvsp[-1].arcvertarrow)) {
      ((yyvsp[-1].arcvertarrow))->SetVerticalShape(Vertical::ARROW);
      ((yyvsp[-1].arcvertarrow))->AddAttributeList((yyvsp[0].attriblist));
      (yyval.arcbase) = ((yyvsp[-1].arcvertarrow));
    } else (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-2].str));
}
#line 5596 "msc_csh_lang.cc"
    break;

  case 113: /* arc: TOK_VERTICAL vertical_shape vertrel full_arcattrlist_with_label  */
#line 1847 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-3]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddVerticalTypesToHints();
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.addMarkersAtEnd = true;
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Vertical::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Vertical::AttributeValues(csh.hintAttrName, csh, (yyvsp[-2].vshape));
  #else
    if ((yyvsp[-1].arcvertarrow)) {
      ((yyvsp[-1].arcvertarrow))->SetVerticalShape((yyvsp[-2].vshape));
      ((yyvsp[-1].arcvertarrow))->AddAttributeList((yyvsp[0].attriblist));
      (yyval.arcbase) = ((yyvsp[-1].arcvertarrow));
    } else (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-3].str));
}
#line 5626 "msc_csh_lang.cc"
    break;

  case 114: /* arc: TOK_VERTICAL vertical_shape full_arcattrlist_with_label  */
#line 1873 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginAndParallelToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddVerticalTypesToHints();
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "at", nullptr, EHintType::KEYWORD, true));
        csh.addMarkersAtEnd = true;
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::MARKER)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.addMarkersAtEnd = true;
        csh.hintStatus = HINT_FILLING;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Vertical::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Vertical::AttributeValues(csh.hintAttrName, csh, (yyvsp[-1].vshape));
  #else
	ArcTypePlusDir typeplusdir;
    typeplusdir.arc.type = EArcSymbol::ARC_SOLID;
	typeplusdir.arc.lost = EArrowLost::NOT;
	typeplusdir.dir = EDirType::RIGHT;
	Vertical *ava = new Vertical(&typeplusdir, MARKER_HERE_STR, MARKER_HERE_STR, &chart);
	VertXPos vxp;
	ava->AddXpos(&vxp);
    ava->SetVerticalShape((yyvsp[-1].vshape));
    ava->AddAttributeList((yyvsp[0].attriblist));
	(yyval.arcbase) = ava;
  #endif
    free((yyvsp[-2].str));
}
#line 5665 "msc_csh_lang.cc"
    break;

  case 115: /* arc: full_arcattrlist  */
#line 1908 "msc_lang.yy"
{
    //Here we have no label and may continue as a parallel block
    //->offer parallel attributes, as well...
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0]))) {
        Divider::AttributeNames(csh, false, false);
        ParallelBlocks::AttributeNames(csh, true);
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0]))) {
        Divider::AttributeValues(csh.hintAttrName, csh, false, false);
        ParallelBlocks::AttributeValues(csh.hintAttrName, csh, true);
    }
  #else
    //... but due to the lack of curly brace we are a divider
    (yyval.arcbase) = new Divider(EArcSymbol::DIV_VSPACE, &chart);
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
}
#line 5687 "msc_csh_lang.cc"
    break;

  case 116: /* arc: colon_string  */
#line 1926 "msc_lang.yy"
{
    //Here we have a colon label added: this is a divider
  #ifdef C_S_H_IS_COMPILED
  #else
    AttributeList *al = new AttributeList;
    al->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yyloc)).IncStartCol()));
    (yyval.arcbase) = new Divider(EArcSymbol::DIV_VSPACE, &chart);
    ((yyval.arcbase))->AddAttributeList(al);
  #endif
    free((yyvsp[0].str));
}
#line 5703 "msc_csh_lang.cc"
    break;

  case 117: /* arc: colon_string full_arcattrlist  */
#line 1938 "msc_lang.yy"
{
    //Here we have a colon label added: this is a divider
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Divider::AttributeNames(csh, false, false);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Divider::AttributeValues(csh.hintAttrName, csh, false, false);
  #else
    ((yyvsp[0].attriblist))->Prepend(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()));
    (yyval.arcbase) = new Divider(EArcSymbol::DIV_VSPACE, &chart);
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
#endif
    free((yyvsp[-1].str));
}
#line 5722 "msc_csh_lang.cc"
    break;

  case 118: /* arc: full_arcattrlist colon_string full_arcattrlist  */
#line 1953 "msc_lang.yy"
{
    //Here we have a colon label added: this is a divider
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-2])) ||
        csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Divider::AttributeNames(csh, false, false);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-2])) ||
             csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Divider::AttributeValues(csh.hintAttrName, csh, false, false);
  #else
    ((yyvsp[-2].attriblist))->Append(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()));
    //Merge $3 at the end of $1 (after the colon label, so ordering is kept)
    ((yyvsp[-2].attriblist))->splice(((yyvsp[-2].attriblist))->end(), *((yyvsp[0].attriblist)));
    delete ((yyvsp[0].attriblist)); //empty list now
    (yyval.arcbase) = new Divider(EArcSymbol::DIV_VSPACE, &chart);
    ((yyval.arcbase))->AddAttributeList((yyvsp[-2].attriblist));
#endif
    free((yyvsp[-1].str));
}
#line 5746 "msc_csh_lang.cc"
    break;

  case 119: /* arc: full_arcattrlist colon_string  */
#line 1973 "msc_lang.yy"
{
    //Here we have a colon label added: this is a divider
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        Divider::AttributeNames(csh, false, false);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        Divider::AttributeValues(csh.hintAttrName, csh, false, false);
  #else
    ((yyvsp[-1].attriblist))->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yylsp[0])), CHART_POS((yylsp[0])).IncStartCol()));
    (yyval.arcbase) = new Divider(EArcSymbol::DIV_VSPACE, &chart);
    ((yyval.arcbase))->AddAttributeList((yyvsp[-1].attriblist));
  #endif
    free((yyvsp[0].str));
}
#line 5765 "msc_csh_lang.cc"
    break;

  case 120: /* arc: first_entity  */
#line 1988 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.arcbase) = (new EntityCommand((yyvsp[0].entitylist), &chart, false));
    ((yyval.arcbase))->AddAttributeList(nullptr);
  #endif
}
#line 5777 "msc_csh_lang.cc"
    break;

  case 121: /* arc: entity_command_prefixes  */
#line 1996 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintAfter((yylsp[0])))
        csh.AllowAnything();
  #else
    //Show/Hide/Activate/Deactivate all entities so far.
    //Call constructor specifically created for this case.
    (yyval.arcbase) = new EntityCommand(&chart, (yyvsp[0].str), CHART_POS((yyloc)));
	((yyval.arcbase))->AddAttributeList(nullptr);
  #endif
    free((yyvsp[0].str));
}
#line 5798 "msc_csh_lang.cc"
    break;

  case 122: /* arc: entity_command_prefixes first_entity  */
#line 2013 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetween((yylsp[-1]), (yylsp[0])) || csh.CheckEntityHintAt((yylsp[-1])))
        csh.AllowAnything();
  #else
    EntityCommand *ce = new EntityCommand((yyvsp[0].entitylist), &chart, false);
    ce->AddAttributeList(nullptr);
    (yyval.arcbase) = ce->ApplyPrefix((yyvsp[-1].str));
  #endif
    free((yyvsp[-1].str));
}
#line 5818 "msc_csh_lang.cc"
    break;

  case 123: /* arc: first_entity TOK_COMMA  */
#line 2029 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckEntityHintAfter((yylsp[0])))
        csh.AllowAnything();
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an entity.");
  #else
    EntityCommand *ce = new EntityCommand((yyvsp[-1].entitylist), &chart, false);
    ce->AddAttributeList(nullptr);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing an entity.");
    (yyval.arcbase) = ce;
  #endif
}
#line 5836 "msc_csh_lang.cc"
    break;

  case 124: /* arc: first_entity TOK_COMMA entitylist  */
#line 2043 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckEntityHintBetween((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
  #else
    ((yyvsp[0].entitylist))->Prepend((yyvsp[-2].entitylist));
    EntityCommand *ce = new EntityCommand((yyvsp[0].entitylist), &chart, false);
    delete ((yyvsp[-2].entitylist));
    ce->AddAttributeList(nullptr);
    (yyval.arcbase) = ce;
  #endif
}
#line 5854 "msc_csh_lang.cc"
    break;

  case 125: /* arc: entity_command_prefixes first_entity TOK_COMMA  */
#line 2057 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetween((yylsp[-2]), (yylsp[-1])) || csh.CheckEntityHintAt((yylsp[-2])))
        csh.AllowAnything();
    else if (csh.CheckEntityHintAfter((yylsp[0])))
        csh.AllowAnything();
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an entity.");
  #else
    EntityCommand *ce = new EntityCommand((yyvsp[-1].entitylist), &chart, false);
    ce->AddAttributeList(nullptr);
    (yyval.arcbase) = ce->ApplyPrefix((yyvsp[-2].str));
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing an entity.");
  #endif
    free((yyvsp[-2].str));
}
#line 5879 "msc_csh_lang.cc"
    break;

  case 126: /* arc: entity_command_prefixes first_entity TOK_COMMA entitylist  */
#line 2078 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckLineStartHintAt((yylsp[-3]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetween((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
#else
    ((yyvsp[0].entitylist))->Prepend((yyvsp[-2].entitylist));
    EntityCommand *ce = new EntityCommand((yyvsp[0].entitylist), &chart, false);
    delete ((yyvsp[-2].entitylist));
    ce->AddAttributeList(nullptr);
    (yyval.arcbase) = ce->ApplyPrefix((yyvsp[-3].str));
  #endif
    free((yyvsp[-3].str));
}
#line 5902 "msc_csh_lang.cc"
    break;

  case 127: /* arc: optlist  */
#line 2097 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    /* If there were arcs defined by the options (e.g., background)
     * enclose them in an "ParallelBlocks" element used only for this.
     * This will be an internally defined ParallelBlocks that will
     * get unrolled in MscChart::PostParseArcList()*/
    (yyval.arcbase) = ((yyvsp[0].arclist)) ? new ParallelBlocks(&chart, (yyvsp[0].arclist), nullptr, true, true) : nullptr;
  #endif
}
#line 5917 "msc_csh_lang.cc"
    break;

  case 128: /* arc: mscgen_boxlist  */
#line 2108 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    /* This may be a list of BoxSeries (each with one emptybox), we
     * enclose them in an "ParallelBlocks" element used only for this.
     * This will be an internally defined ParallelBlocks that will
     * get unrolled in MscChart::PostParseArcList()*/
    (yyval.arcbase) = ((yyvsp[0].arclist)) ? new ParallelBlocks(&chart, (yyvsp[0].arclist), nullptr, false, true) : nullptr;
  #endif
}
#line 5932 "msc_csh_lang.cc"
    break;

  case 129: /* arc: box_list  */
#line 2119 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.arcbase) = (yyvsp[0].arcboxseries); //to remove warning for downcast
  #endif
}
#line 5943 "msc_csh_lang.cc"
    break;

  case 130: /* arc: pipe_list  */
#line 2126 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.arcbase) = (yyvsp[0].arcpipeseries); //to remove warning for downcast
  #endif
}
#line 5954 "msc_csh_lang.cc"
    break;

  case 131: /* arc: parallel  */
#line 2133 "msc_lang.yy"
{
    (yyval.arcbase) = (yyvsp[0].arcparallel);
}
#line 5962 "msc_csh_lang.cc"
    break;

  case 132: /* arc: TOK_COMMAND_DEFSHAPE shapedef  */
#line 2137 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (csh.SkipContent()) {
        csh.AddCSH_Error((yylsp[-1]), "Cannot define shapes inside a procedure.");
    }
  #else
    if (chart.SkipContent()) {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define shapes inside a procedure.");
    }
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 5985 "msc_csh_lang.cc"
    break;

  case 133: /* arc: TOK_COMMAND_DEFSHAPE  */
#line 2156 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (csh.SkipContent()) {
        csh.AddCSH_Error((yylsp[0]), "Cannot define shapes inside a procedure.");
    } else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing shape name and definition.");
    }
#else
    if (chart.SkipContent()) {
        chart.Error.Error(CHART_POS_START((yyloc)), "Cannot define shapes inside a procedure.");
    } else {
        chart.Error.Error(CHART_POS((yyloc)).end, "Missing shape name and definition.");
    }
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6013 "msc_csh_lang.cc"
    break;

  case 134: /* arc: TOK_COMMAND_DEFCOLOR colordeflist  */
#line 2180 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 6033 "msc_csh_lang.cc"
    break;

  case 135: /* arc: TOK_COMMAND_DEFCOLOR  */
#line 2196 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6055 "msc_csh_lang.cc"
    break;

  case 136: /* arc: TOK_COMMAND_DEFSTYLE styledeflist  */
#line 2214 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 6075 "msc_csh_lang.cc"
    break;

  case 137: /* arc: TOK_COMMAND_DEFSTYLE  */
#line 2230 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing style name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6097 "msc_csh_lang.cc"
    break;

  case 138: /* arc: TOK_COMMAND_DEFDESIGN designdef  */
#line 2248 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (csh.SkipContent())
        csh.AddCSH_Error((yylsp[-1]), "Cannot define designs inside a procedure.");
  #else
    if (chart.SkipContent()) {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define designs inside a procedure.");
    }
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 6118 "msc_csh_lang.cc"
    break;

  case 139: /* arc: TOK_COMMAND_DEFDESIGN  */
#line 2265 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (csh.SkipContent()) {
        csh.AddCSH_Error((yylsp[0]), "Cannot define designs inside a procedure.");
    } else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing design name to (re-)define.");
    }
  #else
    if (chart.SkipContent()) {
        chart.Error.Error(CHART_POS_START((yyloc)), "Cannot define designs inside a procedure.");
    } else {
        chart.Error.Error(CHART_POS((yyloc)).end, "Missing a design name to (re-)define.");
    }
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6145 "msc_csh_lang.cc"
    break;

  case 140: /* arc: defproc  */
#line 2288 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.arcbase) = nullptr;
  #endif
}
#line 6156 "msc_csh_lang.cc"
    break;

  case 141: /* arc: set  */
#line 2295 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.arcbase) = nullptr;
  #endif
}
#line 6167 "msc_csh_lang.cc"
    break;

  case 142: /* arc: TOK_COMMAND_HEADING  */
#line 2302 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcbase) = new EntityCommand(&chart, (yyvsp[0].str), CHART_POS((yyloc)));
    ((yyval.arcbase))->AddAttributeList(nullptr);
  #endif
    free((yyvsp[0].str));
}
#line 6185 "msc_csh_lang.cc"
    break;

  case 143: /* arc: TOK_COMMAND_HEADING full_arcattrlist  */
#line 2316 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        EntityCommand::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        EntityCommand::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.arcbase) = (new EntityCommand(nullptr, &chart, false));
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-1].str));
}
#line 6206 "msc_csh_lang.cc"
    break;

  case 144: /* arc: TOK_COMMAND_NUDGE  */
#line 2333 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcbase) = (new Divider(EArcSymbol::DIV_VSPACE, &chart));
    ((yyval.arcbase))->AddAttributeList(nullptr);
  #endif
    free((yyvsp[0].str));
}
#line 6224 "msc_csh_lang.cc"
    break;

  case 145: /* arc: TOK_COMMAND_NUDGE full_arcattrlist  */
#line 2347 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Divider::AttributeNames(csh, true, false);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Divider::AttributeValues(csh.hintAttrName, csh, true, false);
  #else
    (yyval.arcbase) = (new Divider(EArcSymbol::DIV_VSPACE, &chart));
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-1].str));
}
#line 6245 "msc_csh_lang.cc"
    break;

  case 146: /* arc: titlecommandtoken full_arcattrlist_with_label  */
#line 2364 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Divider::AttributeNames(csh, false, true);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Divider::AttributeValues(csh.hintAttrName, csh, false, true);
  #else
    const EArcSymbol t = CaseInsensitiveEqual("title", (yyvsp[-1].str)) ? EArcSymbol::DIV_TITLE :
                         CaseInsensitiveEqual("subtitle", (yyvsp[-1].str)) ? EArcSymbol::DIV_SUBTITLE :
                         EArcSymbol::INVALID;
    (yyval.arcbase) = (new Divider(t, &chart));
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-1].str));
}
#line 6269 "msc_csh_lang.cc"
    break;

  case 147: /* arc: titlecommandtoken  */
#line 2384 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing label.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing label. Ignoring (sub)title.", "Titles and subtitles must have a label.");
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6288 "msc_csh_lang.cc"
    break;

  case 148: /* arc: TOK_COMMAND_TEXT  */
#line 2399 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'at' keyword.");
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing 'at' clause.");
#endif
    free((yyvsp[0].str));
}
#line 6310 "msc_csh_lang.cc"
    break;

  case 149: /* arc: TOK_COMMAND_TEXT full_arcattrlist_with_label  */
#line 2417 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Symbol::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Symbol::AttributeValues(csh.hintAttrName, csh);
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing 'at' keyword.");
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS((yylsp[-1])).end, "Missing 'at' clause. Ignoring this.");
    if ((yyvsp[0].attriblist))
        delete (yyvsp[0].attriblist);
  #endif
    free((yyvsp[-1].str));
}
#line 6337 "msc_csh_lang.cc"
    break;

  case 150: /* arc: TOK_COMMAND_TEXT vertxpos full_arcattrlist_with_label  */
#line 2440 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Symbol::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Symbol::AttributeValues(csh.hintAttrName, csh);
#else
    if ((yyvsp[-1].vertxpos)) {
        Symbol *s = new Symbol(&chart, "text", (yyvsp[-1].vertxpos), CHART_POS((yylsp[-1])));
        s->AddAttributeList((yyvsp[0].attriblist));
        (yyval.arcbase) = s;
        delete (yyvsp[-1].vertxpos);
    } else {
        (yyval.arcbase) = nullptr;
    }
  #endif
    free((yyvsp[-2].str));
}
#line 6364 "msc_csh_lang.cc"
    break;

  case 151: /* arc: TOK_COMMAND_TEXT vertxpos  */
#line 2463 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing label.");
  #else
    if ((yyvsp[0].vertxpos))
        delete (yyvsp[0].vertxpos);
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing actual text - specify a label. Ignoring this.");
    (yyval.arcbase) = nullptr;
#endif
    free((yyvsp[-1].str));
}
#line 6385 "msc_csh_lang.cc"
    break;

  case 152: /* arc: TOK_COMMAND_MARK  */
#line 2480 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefixNonSelectable() + "<marker name>",
            "Specify the name of the marker.",
            EHintType::KEYWORD, false));
        csh.hintStatus=HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing marker name.");
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing marker name. Ignoring this.", "You need to supply a name which then can be used to refer to this vertical position you are marking here.");
  #endif
  free((yyvsp[0].str));
}
#line 6409 "msc_csh_lang.cc"
    break;

  case 153: /* arc: TOK_COMMAND_MARK entity_string  */
#line 2500 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH((yylsp[0]), COLOR_MARKERNAME);
    if (!csh.SkipContent() && !(yyvsp[0].multi_str).had_error)
        csh.MarkerNames.insert((yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[0].multi_str).had_error) {
        (yyval.arcbase) = nullptr;
    } else {
        (yyval.arcbase) = new Marker((yyvsp[0].multi_str).str, CHART_POS((yyloc)), &chart);
        ((yyval.arcbase))->AddAttributeList(nullptr);
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 6435 "msc_csh_lang.cc"
    break;

  case 154: /* arc: TOK_COMMAND_MARK entity_string full_arcattrlist  */
#line 2522 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_MARKERNAME);
    if (!csh.SkipContent() && !(yyvsp[-1].multi_str).had_error)
        csh.MarkerNames.insert((yyvsp[-1].multi_str).str);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Marker::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Marker::AttributeValues(csh.hintAttrName, csh);
  #else
    if ((yyvsp[-1].multi_str).had_error) {
        (yyval.arcbase) = nullptr;
        delete (yyvsp[0].attriblist);
    } else {
        (yyval.arcbase) = new Marker((yyvsp[-1].multi_str).str, CHART_POS((yyloc)), &chart);
        ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].multi_str).str);
}
#line 6465 "msc_csh_lang.cc"
    break;

  case 155: /* arc: TOK_COMMAND_MARK full_arcattrlist  */
#line 2548 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefixNonSelectable() + "<marker name>",
            "Specify the name of the marker.",
            EHintType::KEYWORD, false));
        csh.hintStatus=HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Marker::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Marker::AttributeValues(csh.hintAttrName, csh);
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing marker name.");
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS((yylsp[-1])).end, "Missing marker name. Ignoring this.", "You need to supply a name which then can be used to refer to this vertical position you are marking here.");
    if ((yyvsp[0].attriblist))
        delete (yyvsp[0].attriblist);
  #endif
    free((yyvsp[-1].str));
}
#line 6494 "msc_csh_lang.cc"
    break;

  case 156: /* arc: TOK_COMMAND_MARK reserved_word_string  */
#line 2573 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "This is a reserved word and cannot be used as a marker name.");
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[0])), "This is a reserved word and cannot be used as a marker name. Ignoring this.");
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 6514 "msc_csh_lang.cc"
    break;

  case 157: /* arc: TOK_COMMAND_MARK reserved_word_string full_arcattrlist  */
#line 2589 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Marker::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Marker::AttributeValues(csh.hintAttrName, csh);
    csh.AddCSH_Error((yylsp[-1]), "This is a reserved word and cannot be used as a marker name.");
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "This is a reserved word and cannot be used as a marker name. Ignoring this.");
    if ((yyvsp[0].attriblist))
        delete (yyvsp[0].attriblist);
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
}
#line 6539 "msc_csh_lang.cc"
    break;

  case 158: /* arc: TOK_COMMAND_NEWPAGE  */
#line 2610 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    (yyval.arcbase) = new Newpage(&chart, true);
    ((yyval.arcbase))->AddAttributeList(nullptr);
  #endif
    free((yyvsp[0].str));
}
#line 6557 "msc_csh_lang.cc"
    break;

  case 159: /* arc: TOK_COMMAND_NEWPAGE full_arcattrlist  */
#line 2624 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Newpage::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Newpage::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.arcbase) = new Newpage(&chart, true);
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-1].str));
}
#line 6578 "msc_csh_lang.cc"
    break;

  case 163: /* arc: TOK_COMMAND_HSPACE hspace_location full_arcattrlist_with_label_or_number  */
#line 2644 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        AddHSpace::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        AddHSpace::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.arcbase) = new AddHSpace(&chart, (yyvsp[-1].namerel));
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-2].str));
}
#line 6599 "msc_csh_lang.cc"
    break;

  case 164: /* arc: TOK_COMMAND_HSPACE hspace_location  */
#line 2661 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefixNonSelectable() + "<number>",
            "Enter a number in pixels to set horizontal spacing.",
            EHintType::KEYWORD, false));
        csh.AddToHints(CshHint(csh.HintPrefixNonSelectable() + "<label>",
            "Enter some text the width of which will be used as horizontal spacing.",
            EHintType::KEYWORD, false));
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing either a number or a label.");
  #else
    (yyval.arcbase) = new AddHSpace(&chart, (yyvsp[0].namerel)); //Will trigger an error: either label or space attr is needed
  #endif
    free((yyvsp[-1].str));
}
#line 6625 "msc_csh_lang.cc"
    break;

  case 165: /* arc: TOK_COMMAND_HSPACE  */
#line 2683 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.AddLeftRightHSpaceToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an entity, 'left comment' or 'right comment'.");
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing an entity, 'left comment' or 'right comment'. Ignoring this command.");
  #endif
    free((yyvsp[0].str));
}
#line 6648 "msc_csh_lang.cc"
    break;

  case 166: /* arc: TOK_COMMAND_HSPACE full_arcattrlist_with_label_or_number  */
#line 2702 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.AddLeftRightHSpaceToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        AddHSpace::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        AddHSpace::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS((yylsp[-1])).end, "Missing an entity, 'left comment' or 'right comment'. Ignoring this command.");
    delete (yyvsp[0].attriblist);
  #endif
    free((yyvsp[-1].str));
}
#line 6674 "msc_csh_lang.cc"
    break;

  case 167: /* arc: TOK_COMMAND_VSPACE full_arcattrlist_with_label_or_number  */
#line 2724 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        AddVSpace::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        AddVSpace::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.arcbase) = new AddVSpace(&chart);
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-1].str));
}
#line 6695 "msc_csh_lang.cc"
    break;

  case 168: /* arc: TOK_COMMAND_VSPACE  */
#line 2741 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefixNonSelectable() + "<number>",
            "Enter a number in pixels to add that much empty vertical spacing.",
            EHintType::KEYWORD, false));
        csh.AddToHints(CshHint(csh.HintPrefixNonSelectable() + "<label>",
            "Enter some text the height of which will be added as vertical empty space.",
            EHintType::KEYWORD, false));
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing either a number or a label.");
  #else
    (yyval.arcbase) = new AddVSpace(&chart); //will result in an error, since label or space attribute is needed
  #endif
    free((yyvsp[0].str));
}
#line 6721 "msc_csh_lang.cc"
    break;

  case 169: /* arc: ifthen  */
#line 2763 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.IfThenElses.push_back((yyloc));
  #endif
    (yyval.arcbase) = (yyvsp[0].arcbase);
}
#line 6732 "msc_csh_lang.cc"
    break;

  case 173: /* hspace_location: TOK_AT_POS TOK_COMMAND_COMMENT  */
#line 2775 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (CaseInsensitiveEqual((yyvsp[-1].str), "left") || CaseInsensitiveEqual((yyvsp[-1].str), "right"))
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    else
        csh.AddCSH_Error((yylsp[-1]), "Use either `left comment` or `right comment` to specify which comment column to size.");
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.AddLeftRightHSpaceToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (CaseInsensitiveEqual((yyvsp[-1].str), "left"))
        (yyval.namerel) = new NamePair(LNOTE_ENT_STR, CHART_POS((yylsp[-1])), nullptr, CHART_POS((yylsp[-1])));
    else if (CaseInsensitiveEqual((yyvsp[-1].str), "right"))
        (yyval.namerel) = new NamePair(RNOTE_ENT_STR, CHART_POS((yylsp[-1])), nullptr, CHART_POS((yylsp[-1])));
    else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Use either `left` or `right` to specify which comment column to size. Ignoring command.");
        (yyval.namerel) = nullptr;
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 6762 "msc_csh_lang.cc"
    break;

  case 174: /* hspace_location: TOK_AT_POS  */
#line 2801 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (CaseInsensitiveEqual((yyvsp[0].str), "left") || CaseInsensitiveEqual((yyvsp[0].str), "right")) {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
            csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "comment", nullptr, EHintType::KEYWORD, true));
            csh.hintStatus = HINT_READY;
        }
    }  else
        csh.AddCSH_Error((yylsp[0]), "Use either `left comment` or `right comment` to specify which comment column to size.");
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.AddLeftRightHSpaceToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Use <entity>-<entity>, `left comment`, `right comment` to specify horizontal spacing. Ignoring command.");
    (yyval.namerel) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6788 "msc_csh_lang.cc"
    break;

  case 175: /* hspace_location: TOK_COMMAND_COMMENT  */
#line 2823 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.AddLeftRightHSpaceToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Use `left comment` or `right comment` to specify horizontal spacing for comments. Ignoring command.");
    (yyval.namerel) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6807 "msc_csh_lang.cc"
    break;

  case 176: /* comp: TOK_EMPH  */
#line 2840 "msc_lang.yy"
{
    (yyval.compare_op) = (yyvsp[0].arcsymbol)==EArcSymbol::BOX_DOUBLE ? ECompareOperator::EQUAL : ECompareOperator::INVALID;
}
#line 6815 "msc_csh_lang.cc"
    break;

  case 177: /* comp: TOK_REL_MSCGEN  */
#line 2844 "msc_lang.yy"
{
    (yyval.compare_op) = (yyvsp[0].arcsymbol)==EArcSymbol::ARC_DOUBLE_BIDIR ? ECompareOperator::EQUAL : ECompareOperator::INVALID;
}
#line 6823 "msc_csh_lang.cc"
    break;

  case 178: /* comp: TOK_REL_TO  */
#line 2848 "msc_lang.yy"
{
    (yyval.compare_op) = (yyvsp[0].arcsymbol)==EArcSymbol::ARC_DOUBLE ? ECompareOperator::GREATER_OR_EQUAL : (yyvsp[0].arcsymbol)==EArcSymbol::ARC_DOTTED ? ECompareOperator::GREATER : ECompareOperator::INVALID;
}
#line 6831 "msc_csh_lang.cc"
    break;

  case 179: /* comp: TOK_REL_FROM  */
#line 2852 "msc_lang.yy"
{
    (yyval.compare_op) = (yyvsp[0].arcsymbol)==EArcSymbol::ARC_DOUBLE ? ECompareOperator::SMALLER_OR_EQUAL : (yyvsp[0].arcsymbol)==EArcSymbol::ARC_DOTTED ? ECompareOperator::SMALLER : ECompareOperator::INVALID;
}
#line 6839 "msc_csh_lang.cc"
    break;

  case 180: /* comp: TOK_REL_BIDIR  */
#line 2856 "msc_lang.yy"
{
    (yyval.compare_op) = (yyvsp[0].arcsymbol)==EArcSymbol::ARC_DOTTED_BIDIR ? ECompareOperator::NOT_EQUAL : ECompareOperator::INVALID;
}
#line 6847 "msc_csh_lang.cc"
    break;

  case 181: /* condition: string  */
#line 2861 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #endif
    (yyval.condition) = (yyvsp[0].multi_str).had_error ? 2 : (yyvsp[0].multi_str).str && (yyvsp[0].multi_str).str[0];
    free((yyvsp[0].multi_str).str);
}
#line 6859 "msc_csh_lang.cc"
    break;

  case 182: /* condition: string comp  */
#line 2869 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to compare to.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to compare to.");
  #endif
    (yyval.condition) = 2;
    free((yyvsp[-1].multi_str).str);
    (yyvsp[0].compare_op); //to suppress
}
#line 6876 "msc_csh_lang.cc"
    break;

  case 183: /* condition: string comp string  */
#line 2882 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    if ((yyvsp[-1].compare_op)!=ECompareOperator::INVALID) {
        csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
        (yyval.condition) = (yyvsp[-2].multi_str).Compare((yyvsp[-1].compare_op), (yyvsp[0].multi_str));
    } else {
        csh.AddCSH_Error((yylsp[-1]), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        (yyval.condition) = 2;
    }
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if ((yyvsp[-1].compare_op)!=ECompareOperator::INVALID)
        (yyval.condition) = (yyvsp[-2].multi_str).Compare((yyvsp[-1].compare_op), (yyvsp[0].multi_str));
    else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        (yyval.condition) = 2;
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 6903 "msc_csh_lang.cc"
    break;

  case 184: /* condition: string error string  */
#line 2905 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    csh.AddCSH_Error((yylsp[-1]), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
     chart.Error.Error(CHART_POS_START((yylsp[-1])), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
    (yyval.condition) = 2;
}
#line 6920 "msc_csh_lang.cc"
    break;

  case 185: /* ifthen_condition: TOK_IF condition TOK_THEN  */
#line 2921 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = (yyvsp[-1].condition);
    const bool cond_true = (yyvsp[-1].condition)==1;
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (cond_true)
        csh.PushContext();
    else
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = (yyvsp[-1].condition);
    const bool cond_true = (yyvsp[-1].condition)==1;
    if (cond_true)
        chart.PushContext(CHART_POS_START((yylsp[-2])));
    else
        chart.PushContext(CHART_POS_START((yylsp[-2])), EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_true;
    chart.MyCurrentContext().export_styles = cond_true;
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 6952 "msc_csh_lang.cc"
    break;

  case 186: /* ifthen_condition: TOK_IF condition  */
#line 2949 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'then' keyword.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "then",
            "Continue the 'if' statement with 'then'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-1])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing 'then' keyword.");
  #endif
    free((yyvsp[-1].str));
    (yyvsp[0].condition); //to supress warnings
}
#line 6977 "msc_csh_lang.cc"
    break;

  case 187: /* ifthen_condition: TOK_IF  */
#line 2970 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing condition.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[0].str));
}
#line 6995 "msc_csh_lang.cc"
    break;

  case 188: /* ifthen_condition: TOK_IF error  */
#line 2984 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[0]), "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-1])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[-1].str));
}
#line 7013 "msc_csh_lang.cc"
    break;

  case 189: /* ifthen_condition: TOK_IF error TOK_THEN  */
#line 2998 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[-1]), "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-2])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 7036 "msc_csh_lang.cc"
    break;

  case 190: /* else: TOK_ELSE  */
#line 3019 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
    const bool cond_false = csh.Contexts.back().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    (yyval.condition) = csh.Contexts.back().if_condition;
    if (cond_false)
        csh.PushContext();
    else
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    //kill previous context and open new one - set laterreparse if error or if condition was true
    //this will ignore everything in the else clause
    chart.PopContext();
    const bool cond_false = chart.MyCurrentContext().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    (yyval.condition) = chart.MyCurrentContext().if_condition;
    if (cond_false)
        chart.PushContext(CHART_POS_START((yylsp[0])));
    else
        chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT);
  #endif
  free((yyvsp[0].str));
}
#line 7070 "msc_csh_lang.cc"
    break;

  case 191: /* ifthen: ifthen_condition arc_with_parallel  */
#line 3050 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.PopContext();
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "else",
            "Continue the 'if/then' statement with 'else'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].condition)==1) {
        (yyval.arcbase) = (yyvsp[0].arcbase);
    } else {
        (yyval.arcbase) = nullptr;
        delete (yyvsp[0].arcbase);
    }
    chart.PopContext();
  #endif
}
#line 7095 "msc_csh_lang.cc"
    break;

  case 192: /* ifthen: ifthen_condition  */
#line 3071 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].condition)!=2)
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'then'.");
    csh.PopContext();
  #else
    if ((yyvsp[0].condition)!=2)
        chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a well-formed command after 'then'. Ignoring 'if' clause.");
    chart.PopContext();
    (yyval.arcbase) = nullptr;
  #endif
    (yyvsp[0].condition); //suppress
}
#line 7113 "msc_csh_lang.cc"
    break;

  case 193: /* ifthen: ifthen_condition error  */
#line 3085 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Missing command after 'then'.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing a well-formed command. Ignoring 'if' clause.");
    chart.PopContext();
    (yyval.arcbase) = nullptr;
  #endif
    (yyvsp[-1].condition); //suppress
}
#line 7129 "msc_csh_lang.cc"
    break;

  case 194: /* ifthen: ifthen_condition arc_with_parallel else  */
#line 3097 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-1]));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    delete (yyvsp[-1].arcbase);
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-2].condition); (yyvsp[0].condition); //suppress
}
#line 7147 "msc_csh_lang.cc"
    break;

  case 195: /* ifthen: ifthen_condition arc_with_parallel error else  */
#line 3111 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-2]));
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    delete (yyvsp[-2].arcbase);
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "I am not sure what is coming here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-3].condition); (yyvsp[0].condition); //suppress
}
#line 7167 "msc_csh_lang.cc"
    break;

  case 196: /* ifthen: ifthen_condition error else  */
#line 3127 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "I am not sure what is coming here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-2].condition); (yyvsp[0].condition); //suppress
}
#line 7185 "msc_csh_lang.cc"
    break;

  case 197: /* ifthen: ifthen_condition arc_with_parallel else arc_with_parallel  */
#line 3141 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-2]));
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.PopContext();
  #else
    switch ((yyvsp[-3].condition)) {
    case 1: //original condition was true
        (yyval.arcbase) = (yyvsp[-2].arcbase);   //take 'then' branch
        delete (yyvsp[0].arcbase); //delete 'else' branch
        break;
    case 0: //original condition was false
        (yyval.arcbase) = (yyvsp[0].arcbase); //take 'else' branch
        delete (yyvsp[-2].arcbase); //delete 'then' branch
        break;
    default:
        _ASSERT(0);
        FALLTHROUGH;
    case 2: //we had an error, but has reported the error - delete both branches
        (yyval.arcbase) = nullptr;
        delete (yyvsp[-2].arcbase);
        delete (yyvsp[0].arcbase);
    }
    chart.PopContext();
  #endif
    (yyvsp[-1].condition); //suppress
}
#line 7217 "msc_csh_lang.cc"
    break;

  case 198: /* ifthen: ifthen_condition arc_with_parallel error else arc_with_parallel  */
#line 3169 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-3]));
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.AddCSH_Error((yylsp[-2]), "I am not sure what is coming here.");
    csh.PopContext();
  #else
    (yyval.arcbase) = nullptr;
    delete (yyvsp[-3].arcbase);
    delete (yyvsp[0].arcbase);
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    (yyvsp[-4].condition); (yyvsp[-1].condition); //suppress
}
#line 7237 "msc_csh_lang.cc"
    break;

  case 199: /* ifthen: ifthen_condition error else arc_with_parallel  */
#line 3185 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.AddCSH_Error((yylsp[-2]), "I am not sure what is coming here.");
    csh.PopContext();
  #else
    (yyval.arcbase) = nullptr;
    delete (yyvsp[0].arcbase);
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    (yyvsp[-3].condition); (yyvsp[-1].condition); //suppress
}
#line 7255 "msc_csh_lang.cc"
    break;

  case 201: /* full_arcattrlist_with_label_or_number: TOK_NUMBER  */
#line 3203 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    AttributeList *al = new AttributeList;
    al->Append(std::make_unique<Attribute>("space", (yyvsp[0].str), CHART_POS((yylsp[0])), CHART_POS((yylsp[0]))));
    (yyval.attriblist) = al;
  #endif
    free((yyvsp[0].str));
}
#line 7270 "msc_csh_lang.cc"
    break;

  case 202: /* full_arcattrlist_with_label_or_number: TOK_NUMBER full_arcattrlist_with_label  */
#line 3214 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
  #else
    ((yyvsp[0].attriblist))->Append(std::make_unique<Attribute>("space", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1]))));
    (yyval.attriblist) = (yyvsp[0].attriblist);
  #endif
    free((yyvsp[-1].str));
}
#line 7284 "msc_csh_lang.cc"
    break;

  case 205: /* entityrel: entity_string dash_or_dashdash  */
#line 3227 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.CheckEntityHintAt((yylsp[-1]));
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.CheckEntityHintAfter((yylsp[0]));
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.namerel) = nullptr;
    else
        (yyval.namerel) = new NamePair((yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), nullptr, FileLineColRange());
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 7304 "msc_csh_lang.cc"
    break;

  case 206: /* entityrel: dash_or_dashdash entity_string  */
#line 3243 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
    csh.CheckEntityHintAt((yylsp[0]));
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.namerel) = nullptr;
    else
        (yyval.namerel) = new NamePair(nullptr, FileLineColRange(), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 7323 "msc_csh_lang.cc"
    break;

  case 207: /* entityrel: dash_or_dashdash  */
#line 3258 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.CheckEntityHintAfter((yylsp[0]));
  #else
    (yyval.namerel) = nullptr;
  #endif
}
#line 7336 "msc_csh_lang.cc"
    break;

  case 208: /* entityrel: entity_string dash_or_dashdash entity_string  */
#line 3267 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.CheckEntityHintAt((yylsp[-2]));
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
    csh.CheckEntityHintAt((yylsp[0]));
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error)
        (yyval.namerel) = nullptr;
    else
        (yyval.namerel) = new NamePair((yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 7359 "msc_csh_lang.cc"
    break;

  case 209: /* entityrel: entity_string  */
#line 3286 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
    csh.CheckEntityHintAt((yylsp[0]));
  #else
    (yyval.namerel) = new NamePair((yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), nullptr, FileLineColRange());
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 7373 "msc_csh_lang.cc"
    break;

  case 210: /* markerrel_no_string: entity_string dash_or_dashdash  */
#line 3298 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_MARKERNAME);
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.CheckHintAt((yylsp[-1]), EHintSourceType::MARKER);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::MARKER);
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.namerel) = nullptr;
    else
        (yyval.namerel) = new NamePair((yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), nullptr, FileLineColRange());
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 7392 "msc_csh_lang.cc"
    break;

  case 211: /* markerrel_no_string: dash_or_dashdash entity_string  */
#line 3313 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    csh.AddCSH((yylsp[0]), COLOR_MARKERNAME);
    csh.CheckHintAt((yylsp[0]), EHintSourceType::MARKER);
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.namerel) = nullptr;
    else
        (yyval.namerel) = new NamePair(nullptr, FileLineColRange(), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 7410 "msc_csh_lang.cc"
    break;

  case 212: /* markerrel_no_string: dash_or_dashdash  */
#line 3327 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.CheckHintAt((yylsp[0]), EHintSourceType::MARKER);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::MARKER);
  #else
    (yyval.namerel) = nullptr;
  #endif
}
#line 7424 "msc_csh_lang.cc"
    break;

  case 213: /* markerrel_no_string: entity_string dash_or_dashdash entity_string  */
#line 3337 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_MARKERNAME);
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::MARKER);
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    csh.AddCSH((yylsp[0]), COLOR_MARKERNAME);
    csh.CheckHintAt((yylsp[0]), EHintSourceType::MARKER);
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error)
        (yyval.namerel) = nullptr;
    else
        (yyval.namerel) = new NamePair((yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 7445 "msc_csh_lang.cc"
    break;

  case 218: /* optlist: opt  */
#line 3357 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].arcbase)) {
        (yyval.arclist) = (new ArcList)->Append((yyvsp[0].arcbase)); /* New list */
        //($1)->MakeMeLastNotable(); Do not make chart options notable
    } else
        (yyval.arclist) = nullptr;
  #endif
}
#line 7459 "msc_csh_lang.cc"
    break;

  case 219: /* optlist: optlist TOK_COMMA opt  */
#line 3367 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[0].arcbase)) {
        if ((yyvsp[-2].arclist))
            (yyval.arclist) = ((yyvsp[-2].arclist))->Append((yyvsp[0].arcbase));     /* Add to existing list */
        else
            (yyval.arclist) = (new ArcList)->Append((yyvsp[0].arcbase)); /* New list */
        //($3)->MakeMeLastNotable(); Do not make chart options notable
    } else
        (yyval.arclist) = (yyvsp[-2].arclist);
  #endif
}
#line 7482 "msc_csh_lang.cc"
    break;

  case 220: /* optlist: optlist TOK_COMMA  */
#line 3386 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arclist) = (yyvsp[-1].arclist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an option here.");
  #endif
}
#line 7499 "msc_csh_lang.cc"
    break;

  case 221: /* optlist: optlist TOK_COMMA error  */
#line 3399 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "An option expected here.");
  #else
    (yyval.arclist) = (yyvsp[-2].arclist);
  #endif
}
#line 7516 "msc_csh_lang.cc"
    break;

  case 222: /* opt: entity_string TOK_EQUAL TOK_NUMBER  */
#line 3414 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        MscChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-2].multi_str).had_error || ((yyvsp[-2].multi_str).had_param && chart.SkipContent()))
        (yyval.arcbase) = nullptr;
    else
        (yyval.arcbase) = chart.AddAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yylsp[-2])), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 7543 "msc_csh_lang.cc"
    break;

  case 223: /* opt: entity_string TOK_EQUAL string  */
#line 3437 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error) {
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, (yyvsp[-2].multi_str).str);
    }
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        MscChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error ||
        (((yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param) && chart.SkipContent()))
        (yyval.arcbase) = nullptr;
    else
        (yyval.arcbase) = chart.AddAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 7572 "msc_csh_lang.cc"
    break;

  case 224: /* opt: entity_string TOK_EQUAL  */
#line 3462 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing option value.");
    if (!(yyvsp[-1].multi_str).had_error && csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str)) {
        MscChart::AttributeValues((yyvsp[-1].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value.");
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 7596 "msc_csh_lang.cc"
    break;

  case 225: /* opt: TOK_MSC TOK_EQUAL string  */
#line 3482 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_DESIGNNAME);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, "msc")) {
        csh.AddDesignsToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[0].multi_str).had_error && !csh.SkipContent()) {
        std::string msg = csh.SetDesignTo((yyvsp[0].multi_str).str, true);
        if (msg.length())
            csh.AddCSH_Error((yylsp[0]), std::move(msg));
    }
  #else
    if (chart.SkipContent())
        (yyval.arcbase) = nullptr;
    else
        (yyval.arcbase) = chart.AddAttribute(Attribute("msc", (yyvsp[0].multi_str).str, CHART_POS((yyloc)), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 7628 "msc_csh_lang.cc"
    break;

  case 226: /* opt: TOK_MSC TOK_EQUAL  */
#line 3510 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing option value.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, "msc")) {
        csh.AddDesignsToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value.");
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 7651 "msc_csh_lang.cc"
    break;

  case 227: /* opt: TOK_MSC TOK_PLUS_EQUAL string  */
#line 3529 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_DESIGNNAME);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, "msc+")) {
        csh.AddDesignsToHints(false);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[0].multi_str).had_error) {
        std::string msg = csh.SetDesignTo((yyvsp[0].multi_str).str, false);
        if (msg.length())
            csh.AddCSH_Error((yylsp[0]), std::move(msg));
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.arcbase) = nullptr;
    else
        (yyval.arcbase) = chart.AddAttribute(Attribute("msc+", (yyvsp[0].multi_str).str, CHART_POS((yyloc)), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 7683 "msc_csh_lang.cc"
    break;

  case 228: /* opt: TOK_MSC TOK_PLUS_EQUAL  */
#line 3557 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing option value.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, "msc+")) {
        csh.AddDesignsToHints(false);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value.");
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 7706 "msc_csh_lang.cc"
    break;

  case 229: /* entitylist: entity  */
#line 3577 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.entitylist) = ((yyvsp[0].entitylist));
  #endif
}
#line 7716 "msc_csh_lang.cc"
    break;

  case 230: /* entitylist: entitylist TOK_COMMA entity  */
#line 3583 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckEntityHintBetween((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
#else
    ((yyvsp[0].entitylist))->Prepend((yyvsp[-2].entitylist));
    (yyval.entitylist) = (yyvsp[0].entitylist);
    delete ((yyvsp[-2].entitylist));
  #endif
}
#line 7732 "msc_csh_lang.cc"
    break;

  case 231: /* entitylist: entitylist TOK_COMMA  */
#line 3595 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckEntityHintAfter((yylsp[0])))
        csh.AllowAnything();
  #else
    (yyval.entitylist) = ((yyvsp[-1].entitylist));
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an entity here.");
  #endif
}
#line 7747 "msc_csh_lang.cc"
    break;

  case 232: /* entity: entity_string full_arcattrlist_with_label  */
#line 3608 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-1])))
        csh.AllowAnything();
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        EntityApp::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        EntityApp::AttributeValues(csh.hintAttrName, csh);
  #else
    if ((yyvsp[-1].multi_str).had_error) {
        (yyval.entitylist) = nullptr;
        delete (yyvsp[0].attriblist);
    } else {
        EntityApp *ed = new EntityApp((yyvsp[-1].multi_str).str, &chart);
        ed->SetLineEnd(CHART_POS((yyloc)));
        (yyval.entitylist) = ed->AddAttributeList((yyvsp[0].attriblist), nullptr, FileLineCol()).release();
    }
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 7774 "msc_csh_lang.cc"
    break;

  case 233: /* entity: entity_string  */
#line 3631 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.entitylist) = nullptr;
    else {
        EntityApp *ed = new EntityApp((yyvsp[0].multi_str).str, &chart);
        ed->SetLineEnd(CHART_POS((yyloc)));
        (yyval.entitylist) = ed->AddAttributeList(nullptr, nullptr, FileLineCol()).release();
    }
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 7796 "msc_csh_lang.cc"
    break;

  case 234: /* entity: entity_string full_arcattrlist_with_label braced_arclist  */
#line 3649 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        EntityApp::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        EntityApp::AttributeValues(csh.hintAttrName, csh);
  #else
    if ((yyvsp[-2].multi_str).had_error) {
        (yyval.entitylist) = nullptr;
        delete (yyvsp[-1].attriblist);
        delete (yyvsp[0].arclist);
    } else {
        EntityApp *ed = new EntityApp((yyvsp[-2].multi_str).str, &chart);
        ed->SetLineEnd(CHART_POS2((yylsp[-2]), (yylsp[-1])));
        (yyval.entitylist) = ed->AddAttributeList((yyvsp[-1].attriblist), (yyvsp[0].arclist), CHART_POS_START((yylsp[0]))).release();
    }
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 7824 "msc_csh_lang.cc"
    break;

  case 235: /* entity: entity_string braced_arclist  */
#line 3673 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-1])))
        csh.AllowAnything();
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
  #else
    if ((yyvsp[-1].multi_str).had_error) {
        (yyval.entitylist) = nullptr;
        delete (yyvsp[0].arclist);
    } else {
       EntityApp *ed = new EntityApp((yyvsp[-1].multi_str).str, &chart);
       ed->SetLineEnd(CHART_POS((yylsp[-1])));
       (yyval.entitylist) = ed->AddAttributeList(nullptr, (yyvsp[0].arclist), CHART_POS_START((yylsp[0]))).release();
    }
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 7847 "msc_csh_lang.cc"
    break;

  case 236: /* first_entity: entity_string full_arcattrlist_with_label  */
#line 3693 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        EntityApp::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        EntityApp::AttributeValues(csh.hintAttrName, csh);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_KeywordOrEntity((yylsp[-1]), (yyvsp[-1].multi_str).str);  //Do it after AddLineBeginToHints so if this is a newly defined entity it one is not included among the hints
  #else
    if ((yyvsp[-1].multi_str).had_error) {
        (yyval.entitylist) = nullptr;
        delete (yyvsp[0].attriblist);
    } else {
        EntityApp *ed = new EntityApp((yyvsp[-1].multi_str).str, &chart);
        ed->SetLineEnd(CHART_POS((yyloc)));
        (yyval.entitylist) = ed->AddAttributeList((yyvsp[0].attriblist), nullptr, FileLineCol()).release();
    }
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 7875 "msc_csh_lang.cc"
    break;

  case 237: /* first_entity: entity_string  */
#line 3717 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_KeywordOrEntity((yylsp[0]), (yyvsp[0].multi_str).str);   //Do it after AddLineBeginToHints so if this is a newly defined entity it one is not included among the hints
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.entitylist) = nullptr;
    else {
        EntityApp *ed = new EntityApp((yyvsp[0].multi_str).str, &chart);
        ed->SetLineEnd(CHART_POS((yyloc)));
        (yyval.entitylist) = ed->AddAttributeList(nullptr, nullptr, FileLineCol()).release();
    }
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 7899 "msc_csh_lang.cc"
    break;

  case 238: /* first_entity: entity_string full_arcattrlist_with_label braced_arclist  */
#line 3737 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        EntityApp::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        EntityApp::AttributeValues(csh.hintAttrName, csh);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_KeywordOrEntity((yylsp[-2]), (yyvsp[-2].multi_str).str);  //Do it after AddLineBeginToHints so this one is not included
  #else
    if ((yyvsp[-2].multi_str).had_error) {
        (yyval.entitylist) = nullptr;
        delete (yyvsp[-1].attriblist);
        delete (yyvsp[0].arclist);
    } else {
        EntityApp *ed = new EntityApp((yyvsp[-2].multi_str).str, &chart);
        ed->SetLineEnd(CHART_POS2((yylsp[-2]), (yylsp[-1])));
        (yyval.entitylist) = ed->AddAttributeList((yyvsp[-1].attriblist), (yyvsp[0].arclist), CHART_POS_START((yylsp[0]))).release();
    }
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 7928 "msc_csh_lang.cc"
    break;

  case 239: /* first_entity: entity_string braced_arclist  */
#line 3762 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_KeywordOrEntity((yylsp[-1]), (yyvsp[-1].multi_str).str);   //Do it after AddLineBeginToHints so this one is not included
  #else
    if ((yyvsp[-1].multi_str).had_error) {
        (yyval.entitylist) = nullptr;
        delete (yyvsp[0].arclist);
    } else {
        EntityApp *ed = new EntityApp((yyvsp[-1].multi_str).str, &chart);
        ed->SetLineEnd(CHART_POS((yylsp[-1])));
        (yyval.entitylist) = ed->AddAttributeList(nullptr, (yyvsp[0].arclist), CHART_POS_START((yylsp[0]))).release();
    }
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 7953 "msc_csh_lang.cc"
    break;

  case 241: /* styledeflist: styledeflist TOK_COMMA styledef  */
#line 3785 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 7967 "msc_csh_lang.cc"
    break;

  case 242: /* styledeflist: styledeflist TOK_COMMA  */
#line 3795 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing style definition here.", "Try just removing the comma.");
#endif
}
#line 7983 "msc_csh_lang.cc"
    break;

  case 243: /* styledef: stylenamelist full_arcattrlist  */
#line 3808 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    for (auto &str : *((yyvsp[-1].stringlist)))
        if (csh.ForbiddenStyles.find(str) == csh.ForbiddenStyles.end())
            csh.CurrentContext().StyleNames.insert(str);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        MscStyle().AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        MscStyle().AttributeValues(csh.hintAttrName, csh);
  #else
    if (!chart.SkipContent())
	    chart.AddAttributeListToStyleList((yyvsp[0].attriblist), (yyvsp[-1].stringlist)); //deletes $2, as well
    else
        delete (yyvsp[0].attriblist);
  #endif
    delete((yyvsp[-1].stringlist));
}
#line 8005 "msc_csh_lang.cc"
    break;

  case 244: /* styledef: stylenamelist  */
#line 3826 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH_ErrorAfter((yyloc), "Missing attribute definitons in square brackets ('[' and ']').");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing attribute definitons in square brackets ('[' and ']').");
  #endif
    delete((yyvsp[0].stringlist));
}
#line 8018 "msc_csh_lang.cc"
    break;

  case 245: /* stylenamelist: string  */
#line 3837 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.stringlist) = new std::list<string>;
    if (!(yyvsp[0].multi_str).had_error) {
        csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
        if (strcmp((yyvsp[0].multi_str).str, "emphasis")==0)
            ((yyval.stringlist))->push_back("box");
        else if (strcmp((yyvsp[0].multi_str).str, "emptyemphasis")==0)
            ((yyval.stringlist))->push_back("emptybox");
        else ((yyval.stringlist))->push_back((yyvsp[0].multi_str).str);
    }
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
	    csh.AddStylesToHints(true, true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.stringlist) = new std::list<string>;
    if (!(yyvsp[0].multi_str).had_error)
        ((yyval.stringlist))->push_back(ConvertEmphasisToBox((yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])), chart));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 8046 "msc_csh_lang.cc"
    break;

  case 246: /* stylenamelist: TOK_EMPH_PLUS_PLUS  */
#line 3861 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
    (yyval.stringlist) = new std::list<string>;
	((yyval.stringlist))->push_back("++");
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
		csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.stringlist) = new std::list<string>;
    ((yyval.stringlist))->push_back(ConvertEmphasisToBox("++", CHART_POS_START((yylsp[0])), chart));
  #endif
}
#line 8065 "msc_csh_lang.cc"
    break;

  case 247: /* stylenamelist: stylenamelist TOK_COMMA  */
#line 3876 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
	csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a style name to (re-)define.");
    if (csh.CheckHintAfter((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
		csh.AddStylesToHints(true, true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
	(yyval.stringlist) = (yyvsp[-1].stringlist);
  #else
    (yyval.stringlist) = (yyvsp[-1].stringlist);
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
  #endif
}
#line 8085 "msc_csh_lang.cc"
    break;

  case 248: /* stylenamelist: stylenamelist TOK_COMMA string  */
#line 3892 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    (yyval.stringlist) = (yyvsp[-2].stringlist);
    if (!(yyvsp[0].multi_str).had_error) {
        csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
        if (strcmp((yyvsp[0].multi_str).str, "emphasis")==0)
            ((yyval.stringlist))->push_back("box");
        else if (strcmp((yyvsp[0].multi_str).str, "emptyemphasis")==0)
            ((yyval.stringlist))->push_back("emptybox");
        else ((yyval.stringlist))->push_back((yyvsp[0].multi_str).str);
    }
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[0].multi_str).had_error)
        ((yyvsp[-2].stringlist))->push_back(ConvertEmphasisToBox((yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])), chart));
    (yyval.stringlist) = (yyvsp[-2].stringlist);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 8114 "msc_csh_lang.cc"
    break;

  case 249: /* stylenamelist: stylenamelist TOK_COMMA TOK_EMPH_PLUS_PLUS  */
#line 3917 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
    (yyval.stringlist) = (yyvsp[-2].stringlist);
	((yyval.stringlist))->push_back("++");
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
	    csh.AddStylesToHints(true, true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
  #else
    ((yyvsp[-2].stringlist))->push_back(ConvertEmphasisToBox("++", CHART_POS_START((yylsp[0])), chart));
    (yyval.stringlist) = (yyvsp[-2].stringlist);
  #endif
}
#line 8135 "msc_csh_lang.cc"
    break;

  case 250: /* shapedef: entity_string  */
#line 3935 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.AddCSH_ErrorAfter((yyloc), ("Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '"+std::string((yyvsp[0].multi_str).str) +"'.").c_str());
  #else
    if (!(yyvsp[0].multi_str).had_error)
       chart.Error.Error(CHART_POS((yyloc)).end, "Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '"+std::string((yyvsp[0].multi_str).str) +"'.");
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 8151 "msc_csh_lang.cc"
    break;

  case 251: /* shapedef: entity_string TOK_OCBRACKET  */
#line 3947 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yylsp[0]));
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.AddCSH_ErrorAfter((yyloc), ("Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '"+std::string((yyvsp[-1].multi_str).str) +"'.").c_str());
  #else
    if (!(yyvsp[-1].multi_str).had_error)
        chart.Error.Error(CHART_POS((yyloc)).end, "Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '"+std::string((yyvsp[-1].multi_str).str) +"'.");
  #endif
    free((yyvsp[-1].multi_str).str);
    (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 8170 "msc_csh_lang.cc"
    break;

  case 252: /* shapedef: entity_string TOK_OCBRACKET shapedeflist  */
#line 3962 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yylsp[-1])+(yylsp[0]));
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-1]), COLOR_BRACE);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a closing brace ('}').");
    if (!csh.SkipContent())
    	csh.AddShapeName((yyvsp[-2].multi_str).str);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), CHART_POS_AFTER((yylsp[0])), "Here is the corresponding '{'.");
    if ((yyvsp[0].shape)) {
        if (!chart.SkipContent() && !(yyvsp[-2].multi_str).had_error)
	        chart.Shapes.Add(std::string((yyvsp[-2].multi_str).str), CHART_POS_START((yylsp[-2])), chart.file_url, chart.file_info, std::move(*(yyvsp[0].shape)), chart.Error);
	    delete (yyvsp[0].shape);
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 8196 "msc_csh_lang.cc"
    break;

  case 253: /* shapedef: entity_string TOK_OCBRACKET shapedeflist TOK_CCBRACKET  */
#line 3984 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yylsp[-2])+(yylsp[0]));
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH((yylsp[-3]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-2]), COLOR_BRACE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    if (!csh.SkipContent() && !(yyvsp[-3].multi_str).had_error)
    	csh.AddShapeName((yyvsp[-3].multi_str).str);
  #else
    if ((yyvsp[-1].shape)) {
        if (!chart.SkipContent() && !(yyvsp[-3].multi_str).had_error)
            chart.Shapes.Add((yyvsp[-3].multi_str).str, CHART_POS_START((yylsp[-3])), chart.file_url, chart.file_info, std::move(*(yyvsp[-1].shape)), chart.Error);
        delete (yyvsp[-1].shape);
    }
  #endif
    free((yyvsp[-3].multi_str).str);
    (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
    (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 8221 "msc_csh_lang.cc"
    break;

  case 254: /* shapedef: entity_string TOK_OCBRACKET shapedeflist error TOK_CCBRACKET  */
#line 4005 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yylsp[-3])+(yylsp[0]));
    if (!(yyvsp[-4].multi_str).had_error)
        csh.AddCSH((yylsp[-4]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-3]), COLOR_BRACE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    if (!csh.SkipContent() && !(yyvsp[-4].multi_str).had_error)
    	csh.AddShapeName((yyvsp[-4].multi_str).str);
    csh.AddCSH_Error((yylsp[-1]), "Only numbers can come after shape commands.");
  #else
    if ((yyvsp[-2].shape)) {
        if (!chart.SkipContent() && !(yyvsp[-4].multi_str).had_error)
            chart.Shapes.Add((yyvsp[-4].multi_str).str, CHART_POS_START((yylsp[-4])), chart.file_url, chart.file_info, std::move(*(yyvsp[-2].shape)), chart.Error);
        delete (yyvsp[-2].shape);
    }
  #endif
    free((yyvsp[-4].multi_str).str);
    (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
    (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 8247 "msc_csh_lang.cc"
    break;

  case 255: /* shapedeflist: shapeline TOK_SEMICOLON  */
#line 4028 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    (yyval.shape) = new Shape;
	if ((yyvsp[-1].shapeelement)) {
		((yyval.shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
  #endif
}
#line 8263 "msc_csh_lang.cc"
    break;

  case 256: /* shapedeflist: shapeline  */
#line 4040 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    (yyval.shape) = new Shape;
	if ((yyvsp[0].shapeelement)) {
		((yyval.shape))->Add(std::move(*((yyvsp[0].shapeelement))));
		delete (yyvsp[0].shapeelement);
	}
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing semicolon (';').");
  #endif
}
#line 8280 "msc_csh_lang.cc"
    break;

  case 257: /* shapedeflist: error shapeline TOK_SEMICOLON  */
#line 4053 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-2]), "I do not understand this.");
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
#else
    (yyval.shape) = new Shape;
	if ((yyvsp[-1].shapeelement)) {
		((yyval.shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "syntax error.");
  #endif
}
#line 8298 "msc_csh_lang.cc"
    break;

  case 258: /* shapedeflist: error shapeline  */
#line 4067 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "I do not understand this.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
#else
    (yyval.shape) = new Shape;
	if ((yyvsp[0].shapeelement)) {
		((yyval.shape))->Add(std::move(*((yyvsp[0].shapeelement))));
		delete (yyvsp[0].shapeelement);
	}
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "syntax error.");
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing semicolon (';').");
  #endif
}
#line 8317 "msc_csh_lang.cc"
    break;

  case 259: /* shapedeflist: shapedeflist shapeline TOK_SEMICOLON  */
#line 4082 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
	if ((yyvsp[-1].shapeelement)) {
		((yyvsp[-2].shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
    (yyval.shape) = (yyvsp[-2].shape);
  #endif
}
#line 8333 "msc_csh_lang.cc"
    break;

  case 260: /* shapedeflist: shapedeflist shapeline  */
#line 4094 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
	if ((yyvsp[0].shapeelement)) {
		((yyvsp[-1].shape))->Add(std::move(*((yyvsp[0].shapeelement))));
		delete (yyvsp[0].shapeelement);
	}
    (yyval.shape) = (yyvsp[-1].shape);
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing semicolon (';').");
  #endif
}
#line 8350 "msc_csh_lang.cc"
    break;

  case 261: /* shapedeflist: shapedeflist error  */
#line 4107 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Only numbers can come after shape commands.");
  #else
    (yyval.shape) = (yyvsp[-1].shape);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "syntax error.");
  #endif
}
#line 8363 "msc_csh_lang.cc"
    break;

  case 262: /* shapedeflist: shapedeflist TOK_SEMICOLON  */
#line 4116 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    (yyval.shape) = (yyvsp[-1].shape);
  #endif
}
#line 8375 "msc_csh_lang.cc"
    break;

  case 263: /* shapeline: TOK_SHAPE_COMMAND  */
#line 4126 "msc_lang.yy"
{
    const int num_args = 0;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[0].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
	if (should_args != num_args)
		csh.AddCSH_ErrorAfter((yyloc), ShapeElement::ErrorMsg((yyvsp[0].shapecommand), num_args));
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args != num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[0].shapecommand), num_args).append(" Ignoring line."));
	else
	    (yyval.shapeelement) = new ShapeElement((yyvsp[0].shapecommand));
  #endif
}
#line 8395 "msc_csh_lang.cc"
    break;

  case 264: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER  */
#line 4142 "msc_lang.yy"
{
    const int num_args = 1;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-1].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-1].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-1].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	} else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG && (((yyvsp[0].str))[0]<'0' || ((yyvsp[0].str))[0]>'2' || ((yyvsp[0].str))[1]!=0))
		csh.AddCSH_Error((yylsp[0]), "S (section) commands require an integer between 0 and 2.");
  #else
	(yyval.shapeelement) = nullptr;
	const double a = atof((yyvsp[0].str));
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yylsp[0])).end, ShapeElement::ErrorMsg((yyvsp[-1].shapecommand), num_args).append(" Ignoring line."));
	else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG && (a!=0 && a!=1 && a!=2))
		chart.Error.Error(CHART_POS_START((yylsp[0])), "S (section) commands require an integer between 0 and 2. Ignoring line.");
	else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG)
	    (yyval.shapeelement) = new ShapeElement(ShapeElement::Type((yyvsp[-1].shapecommand) + unsigned(a)));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-1].shapecommand), a);
  #endif
    free((yyvsp[0].str));
}
#line 8427 "msc_csh_lang.cc"
    break;

  case 265: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER  */
#line 4170 "msc_lang.yy"
{
    const int num_args = 2;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-2].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-2].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-2].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-2].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-2].shapecommand), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 8455 "msc_csh_lang.cc"
    break;

  case 266: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER alpha_string  */
#line 4194 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if ((yyvsp[-3].shapecommand)!=ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[0]), "You need to specify a number here.");
  #else
    (yyval.shapeelement) = nullptr;
    if ((yyvsp[-3].shapecommand)!=ShapeElement::PORT)
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a number here. Ignoring line.");
    else
        (yyval.shapeelement) = new ShapeElement(atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), (yyvsp[0].multi_str).str);
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 8476 "msc_csh_lang.cc"
    break;

  case 267: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER alpha_string TOK_NUMBER  */
#line 4211 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
    if ((yyvsp[-4].shapecommand)!=ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[-1]), "You need to specify a number here.");
  #else
    (yyval.shapeelement) = nullptr;
    if ((yyvsp[-4].shapecommand)!=ShapeElement::PORT)
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting a number here. Ignoring line.");
    else
        (yyval.shapeelement) = new ShapeElement(atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), (yyvsp[-1].multi_str).str, atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].str));
    free((yyvsp[-1].multi_str).str);
    free((yyvsp[0].str));
}
#line 8498 "msc_csh_lang.cc"
    break;

  case 268: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 4229 "msc_lang.yy"
{
    const int num_args = 3;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-3].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-3].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	} else if ((yyvsp[-3].shapecommand)==ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[0]), "You need to specify a port name here starting with a letter.");
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args).append(" Ignoring line."));
	else if ((yyvsp[-3].shapecommand)==ShapeElement::PORT)
		chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a port name here. Ignoring line.");
    else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-3].shapecommand), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 8531 "msc_csh_lang.cc"
    break;

  case 269: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 4258 "msc_lang.yy"
{
    const int num_args = 4;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-4].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-4].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-4].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-4].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-4].shapecommand), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 8563 "msc_csh_lang.cc"
    break;

  case 270: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 4286 "msc_lang.yy"
{
    const int num_args = 5;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-5].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-5].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-5].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-4]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 4:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-5].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-5].shapecommand), atof((yyvsp[-4].str)), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-4].str));
    free((yyvsp[-3].str));
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 8597 "msc_csh_lang.cc"
    break;

  case 271: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 4316 "msc_lang.yy"
{
    const int num_args = 6;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-6].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-6]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-6].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-6].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-5]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-4]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 4:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 5:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-6].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-6].shapecommand), atof((yyvsp[-5].str)), atof((yyvsp[-4].str)), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-5].str));
    free((yyvsp[-4].str));
    free((yyvsp[-3].str));
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 8633 "msc_csh_lang.cc"
    break;

  case 273: /* colordeflist: colordeflist TOK_COMMA colordef  */
#line 4350 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
	}
  #endif
}
#line 8648 "msc_csh_lang.cc"
    break;

  case 274: /* colordeflist: colordeflist TOK_COMMA  */
#line 4361 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
  #endif
}
#line 8666 "msc_csh_lang.cc"
    break;

  case 279: /* color_string: string  */
#line 4379 "msc_lang.yy"
{
    if ((yyvsp[0].multi_str).had_error) {
        (yyval.str) = strdup("");
        free((yyvsp[0].multi_str).str);
    } else
        (yyval.str) = (yyvsp[0].multi_str).str;
}
#line 8678 "msc_csh_lang.cc"
    break;

  case 280: /* colordef: alpha_string TOK_EQUAL color_string  */
#line 4389 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH((yylsp[-2]), COLOR_COLORNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    if (!(yyvsp[-2].multi_str).had_error) {
        ColorType color = csh.CurrentContext().Colors.GetColor((yyvsp[0].str));
        if (color.type!=ColorType::INVALID)
            csh.CurrentContext().Colors[(yyvsp[-2].multi_str).str] = color;
    }
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent() && !(yyvsp[-2].multi_str).had_error)
        chart.MyCurrentContext().colors.AddColor((yyvsp[-2].multi_str).str, (yyvsp[0].str), chart.Error, CHART_POS((yyloc)));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 8710 "msc_csh_lang.cc"
    break;

  case 281: /* colordef: alpha_string TOK_EQUAL TOK_EMPH_PLUS_PLUS color_string  */
#line 4417 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH((yylsp[-3]), COLOR_COLORNAME);
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    csh.AddCSH((yylsp[-1]), COLOR_COLORDEF);
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    if (!(yyvsp[-3].multi_str).had_error) {
        ColorType color = csh.CurrentContext().Colors.GetColor("++"+string((yyvsp[0].str)));
        if (color.type!=ColorType::INVALID)
            csh.CurrentContext().Colors[(yyvsp[-3].multi_str).str] = color;
    }
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent())
       chart.MyCurrentContext().colors.AddColor((yyvsp[-3].multi_str).str, "++"+string((yyvsp[0].str)), chart.Error, CHART_POS((yyloc)));
  #endif
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[0].str));
}
#line 8743 "msc_csh_lang.cc"
    break;

  case 282: /* colordef: alpha_string TOK_EQUAL  */
#line 4446 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH((yylsp[-1]), COLOR_COLORNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing color definition.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing color definition.");
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 8768 "msc_csh_lang.cc"
    break;

  case 283: /* colordef: alpha_string  */
#line 4467 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_COLORNAME);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing equal sign ('=') and a color definition.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing equal sign ('=') and a color definition.");
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 8788 "msc_csh_lang.cc"
    break;

  case 284: /* designdef: TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON TOK_CCBRACKET  */
#line 4486 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_DESIGNNAME);
    csh.AddCSH((yylsp[-1]), COLOR_SEMICOLON);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-3])+(yylsp[0]));
    if (!csh.SkipContent()) {
        auto &d = csh.CurrentContext().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-4].str));
        if (i == d.end())
            d.emplace((yyvsp[-4].str), csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::LINE_START) ||
         csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) ) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent()) {
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-4].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-3]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
    free((yyvsp[-4].str));
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 8832 "msc_csh_lang.cc"
    break;

  case 285: /* designdef: TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON error TOK_CCBRACKET  */
#line 4526 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_DESIGNNAME);
    csh.AddCSH((yylsp[-2]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as part of a design definition.");
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-4])+(yylsp[0]));
    if (!csh.SkipContent()) {
        auto &d = csh.CurrentContext().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-5].str));
        if (i == d.end())
            d.emplace((yyvsp[-5].str), csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween((yylsp[-4]), (yylsp[-3]), EHintSourceType::LINE_START) ||
         csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::LINE_START))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    if (!chart.SkipContent()) {
        //if closing brace missing, still do the design definition
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-5].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-4]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 8877 "msc_csh_lang.cc"
    break;

  case 286: /* scope_open_empty: TOK_OCBRACKET  */
#line 4569 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(false, EContextParse::NORMAL);
  #else
    //push empty color & style sets for design definition
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::NORMAL, EContextCreate::CLEAR);
  #endif
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 8892 "msc_csh_lang.cc"
    break;

  case 288: /* designelementlist: designelementlist TOK_SEMICOLON designelement  */
#line 4582 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SEMICOLON);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#endif
}
#line 8906 "msc_csh_lang.cc"
    break;

  case 289: /* designelement: TOK_COMMAND_DEFCOLOR colordeflist  */
#line 4593 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
    }
  #endif
    free((yyvsp[-1].str));
}
#line 8925 "msc_csh_lang.cc"
    break;

  case 290: /* designelement: TOK_COMMAND_DEFCOLOR  */
#line 4608 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
  #endif
    free((yyvsp[0].str));
}
#line 8947 "msc_csh_lang.cc"
    break;

  case 291: /* designelement: TOK_COMMAND_DEFSTYLE styledeflist  */
#line 4626 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
	}
  #endif
    free((yyvsp[-1].str));
}
#line 8966 "msc_csh_lang.cc"
    break;

  case 292: /* designelement: TOK_COMMAND_DEFSTYLE  */
#line 4641 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.AllowAnything();
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing style name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
  #endif
    free((yyvsp[0].str));
}
#line 8988 "msc_csh_lang.cc"
    break;

  case 295: /* designoptlist: designoptlist TOK_COMMA designopt  */
#line 4662 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 9002 "msc_csh_lang.cc"
    break;

  case 296: /* designoptlist: designoptlist TOK_COMMA  */
#line 4672 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 9016 "msc_csh_lang.cc"
    break;

  case 297: /* designoptlist: designoptlist error  */
#line 4682 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Extra stuff after design options. Maybe missing a comma?");
  #endif
}
#line 9026 "msc_csh_lang.cc"
    break;

  case 298: /* designopt: entity_string TOK_EQUAL TOK_NUMBER  */
#line 4689 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        MscChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[-2].multi_str).had_error)
        chart.AddDesignAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 9051 "msc_csh_lang.cc"
    break;

  case 299: /* designopt: entity_string TOK_EQUAL string  */
#line 4710 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        MscChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        chart.AddDesignAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 9077 "msc_csh_lang.cc"
    break;

  case 300: /* designopt: entity_string TOK_EQUAL  */
#line 4732 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-1].multi_str).had_error && csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str)) {
        MscChart::AttributeValues((yyvsp[-1].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value. Ignoring this.");
#endif
    free((yyvsp[-1].multi_str).str);
}
#line 9099 "msc_csh_lang.cc"
    break;

  case 301: /* designopt: TOK_MSC TOK_EQUAL string  */
#line 4750 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_DESIGNNAME);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str))) {
        MscChart::AttributeValues("msc", csh);
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[0].multi_str).had_error) {
        std::string msg = csh.SetDesignTo((yyvsp[0].multi_str).str, true);
        if (msg.length())
           csh.AddCSH_Error((yylsp[0]), std::move(msg));
    }
  #else
    if (!(yyvsp[0].multi_str).had_error)
        chart.AddDesignAttribute(Attribute("msc", (yyvsp[0].multi_str).str, CHART_POS((yyloc)), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 9129 "msc_csh_lang.cc"
    break;

  case 302: /* designopt: TOK_MSC TOK_PLUS_EQUAL string  */
#line 4776 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_DESIGNNAME);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str))) {
        MscChart::AttributeValues("msc+", csh);
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[0].multi_str).had_error) {
        std::string msg = csh.SetDesignTo((yyvsp[0].multi_str).str, false);
        if (msg.length())
            csh.AddCSH_Error((yylsp[0]), std::move(msg));
    }
  #else
    if (!(yyvsp[0].multi_str).had_error)
        chart.AddDesignAttribute(Attribute("msc+", (yyvsp[0].multi_str).str, CHART_POS((yyloc)), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 9159 "msc_csh_lang.cc"
    break;

  case 303: /* designopt: TOK_MSC TOK_EQUAL  */
#line 4802 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].str), COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, "msc")) {
        MscChart::AttributeValues((yyvsp[-1].str), csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing design name. Ignoring this.");
#endif
    free((yyvsp[-1].str));
}
#line 9180 "msc_csh_lang.cc"
    break;

  case 304: /* designopt: TOK_MSC TOK_PLUS_EQUAL  */
#line 4819 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].str), COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, "msc+")) {
        MscChart::AttributeValues((yyvsp[-1].str), csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing design name. Ignoring this.");
#endif
    free((yyvsp[-1].str));
}
#line 9201 "msc_csh_lang.cc"
    break;

  case 305: /* parallel: braced_arclist  */
#line 4838 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].arclist))
        (yyval.arcparallel) = new ParallelBlocks(&chart, (yyvsp[0].arclist), nullptr, false, false);
    else
        (yyval.arcparallel) = nullptr;
  #endif
}
#line 9214 "msc_csh_lang.cc"
    break;

  case 306: /* parallel: full_arcattrlist braced_arclist  */
#line 4847 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        ParallelBlocks::AttributeNames(csh, true);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        ParallelBlocks::AttributeValues(csh.hintAttrName, csh, true);
  #else
    if ((yyvsp[0].arclist)) {
        (yyval.arcparallel) = new ParallelBlocks(&chart, (yyvsp[0].arclist), (yyvsp[-1].attriblist), false, false);
    } else {
        (yyval.arcparallel) = nullptr;
        if ((yyvsp[-1].attriblist)) delete (yyvsp[-1].attriblist);
    }
  #endif
}
#line 9234 "msc_csh_lang.cc"
    break;

  case 307: /* parallel: parallel braced_arclist  */
#line 4863 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].arclist)==nullptr)
        (yyval.arcparallel) = (yyvsp[-1].arcparallel);
    else if ((yyvsp[-1].arcparallel))
        (yyval.arcparallel) = ((yyvsp[-1].arcparallel))->AddArcList((yyvsp[0].arclist), nullptr);
    else
        (yyval.arcparallel) = new ParallelBlocks(&chart, (yyvsp[0].arclist), nullptr, false, false);
  #endif
}
#line 9249 "msc_csh_lang.cc"
    break;

  case 308: /* parallel: parallel full_arcattrlist braced_arclist  */
#line 4874 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        ParallelBlocks::AttributeNames(csh, false);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        ParallelBlocks::AttributeValues(csh.hintAttrName, csh, false);
  #else
    if ((yyvsp[0].arclist)==nullptr) {
        (yyval.arcparallel) = (yyvsp[-2].arcparallel);
        if ((yyvsp[-1].attriblist)) delete (yyvsp[-1].attriblist);
    } else if ((yyvsp[-2].arcparallel))
        (yyval.arcparallel) = ((yyvsp[-2].arcparallel))->AddArcList((yyvsp[0].arclist), (yyvsp[-1].attriblist));
    else
        (yyval.arcparallel) = new ParallelBlocks(&chart, (yyvsp[0].arclist), (yyvsp[-1].attriblist), false, false);
  #endif
}
#line 9270 "msc_csh_lang.cc"
    break;

  case 309: /* parallel: parallel full_arcattrlist  */
#line 4891 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        ParallelBlocks::AttributeNames(csh, false);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        ParallelBlocks::AttributeValues(csh.hintAttrName, csh, false);
    csh.AddCSH_ErrorAfter((yylsp[0]),
        "Need an additional parallel block enclosed between '{' and '}'.");
  #else
    (yyval.arcparallel) = (yyvsp[-1].arcparallel);
    if ((yyvsp[0].attriblist)) delete (yyvsp[0].attriblist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])),
        "Missing an additional parallel block enclosed between '{' and '}' after the attributes.");
  #endif
}
#line 9290 "msc_csh_lang.cc"
    break;

  case 311: /* optional_box_keyword: TOK_COMMAND_BOX  */
#line 4908 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
  #endif
    free((yyvsp[0].str));
}
#line 9302 "msc_csh_lang.cc"
    break;

  case 312: /* box_list: first_box  */
#line 4919 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintAfter((yylsp[0]));
  #else
    (yyval.arcboxseries) = (yyvsp[0].arcboxseries);
  #endif
}
#line 9314 "msc_csh_lang.cc"
    break;

  case 313: /* box_list: TOK_COMMAND_BOX  */
#line 4927 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintAfter((yylsp[0])))
        csh.AllowAnything();
  #else
    //Allocate a solid box with no entities and no attribute list (the second param to BoxSeries())
    (yyval.arcboxseries) = new BoxSeries(
	          new Box(EArcSymbol::BOX_SOLID, CHART_POS((yylsp[0])), nullptr, CHART_POS((yylsp[0])), nullptr, CHART_POS((yylsp[0])), &chart),
			      nullptr);
    ((yyval.arcboxseries))->ExpandFirstLineEnd(CHART_POS((yyloc)));
  #endif
    free((yyvsp[0].str));
}
#line 9336 "msc_csh_lang.cc"
    break;

  case 314: /* box_list: TOK_COMMAND_BOX braced_arclist  */
#line 4945 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.CheckEntityHintAfter((yylsp[0]));
  #else
    FileLineColRange box_file_pos(FileLineCol(chart.current_file, ((yylsp[0])).first_line, ((yylsp[0])).first_column - 1),
						            FileLineCol(chart.current_file, ((yylsp[0])).first_line, ((yylsp[0])).first_column - 1));
    Box *temp = new Box(EArcSymbol::BOX_SOLID, box_file_pos,
                        nullptr, CHART_POS((yylsp[-1])), nullptr, CHART_POS((yylsp[-1])), &chart);
    temp->AddArcList((yyvsp[0].arclist));
	temp->SetLineEnd(box_file_pos);
    (yyval.arcboxseries) = new BoxSeries(temp, nullptr);
  #endif
    free((yyvsp[-1].str));
}
#line 9356 "msc_csh_lang.cc"
    break;

  case 315: /* box_list: TOK_COMMAND_BOX full_arcattrlist_with_label braced_arclist  */
#line 4961 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        Box::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        Box::AttributeValues(csh.hintAttrName, csh);
    else
        csh.CheckEntityHintAfter((yylsp[0]));
  #else
    FileLineColRange box_file_pos(FileLineCol(chart.current_file, ((yylsp[-1])).first_line, ((yylsp[-1])).first_column - 1),
						            FileLineCol(chart.current_file, ((yylsp[-1])).first_line, ((yylsp[-1])).first_column - 1));
    Box *temp = new Box(EArcSymbol::BOX_SOLID, box_file_pos,
                        nullptr, CHART_POS((yylsp[-2])), nullptr, CHART_POS((yylsp[-2])), &chart);
    temp->AddArcList((yyvsp[0].arclist))->SetLineEnd(CHART_POS2((yylsp[-1]), (yylsp[-1])));
    (yyval.arcboxseries) = new BoxSeries(temp, (yyvsp[-1].attriblist));
  #endif
    free((yyvsp[-2].str));
}
#line 9380 "msc_csh_lang.cc"
    break;

  case 316: /* box_list: TOK_COMMAND_BOX full_arcattrlist_with_label  */
#line 4981 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Box::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Box::AttributeValues(csh.hintAttrName, csh);
    else
        csh.CheckEntityHintAfter((yylsp[0]));
  #else
    FileLineColRange box_file_pos(FileLineCol(chart.current_file, ((yylsp[0])).first_line, ((yylsp[0])).first_column - 1),
						            FileLineCol(chart.current_file, ((yylsp[0])).first_line, ((yylsp[0])).first_column - 1));
    Box *temp = new Box(EArcSymbol::BOX_SOLID, box_file_pos,
                        nullptr, CHART_POS((yylsp[-1])), nullptr, CHART_POS((yylsp[-1])), &chart);
    temp->SetLineEnd(CHART_POS2((yylsp[0]), (yylsp[0])));
    (yyval.arcboxseries) = new BoxSeries(temp, (yyvsp[0].attriblist));
  #endif
    free((yyvsp[-1].str));
}
#line 9404 "msc_csh_lang.cc"
    break;

  case 317: /* box_list: TOK_COMMAND_BOX first_box  */
#line 5001 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else
        csh.CheckEntityHintAfter((yylsp[0]));
  #else
    ((yyvsp[0].arcboxseries))->ExpandFirstLineEnd(CHART_POS((yyloc)));
    (yyval.arcboxseries) = (yyvsp[0].arcboxseries);
  #endif
  free((yyvsp[-1].str));
}
#line 9423 "msc_csh_lang.cc"
    break;

  case 318: /* box_list: box_list optional_box_keyword boxrel  */
#line 5017 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    ((yyvsp[0].arcbox))->SetLineEnd(CHART_POS2((yylsp[-1]), (yylsp[0])));
    (yyval.arcboxseries) = ((yyvsp[-2].arcboxseries))->AddBox((yyvsp[0].arcbox), nullptr);
  #endif
}
#line 9434 "msc_csh_lang.cc"
    break;

  case 319: /* box_list: box_list optional_box_keyword boxrel full_arcattrlist_with_label  */
#line 5024 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Box::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Box::AttributeValues(csh.hintAttrName, csh);
    else
        csh.CheckEntityHintAfter((yylsp[0]));
  #else
    ((yyvsp[-1].arcbox))->SetLineEnd(CHART_POS2((yylsp[-2]), (yylsp[0])));
    (yyval.arcboxseries) = ((yyvsp[-3].arcboxseries))->AddBox((yyvsp[-1].arcbox), (yyvsp[0].attriblist));
  #endif
}
#line 9452 "msc_csh_lang.cc"
    break;

  case 320: /* box_list: box_list optional_box_keyword boxrel braced_arclist  */
#line 5038 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintAfter((yylsp[0]));
  #else
    ((yyvsp[-1].arcbox))->AddArcList((yyvsp[0].arclist))->SetLineEnd(CHART_POS2((yylsp[-2]), (yylsp[-1])));
    (yyval.arcboxseries) = ((yyvsp[-3].arcboxseries))->AddBox((yyvsp[-1].arcbox), nullptr);
  #endif
}
#line 9465 "msc_csh_lang.cc"
    break;

  case 321: /* box_list: box_list optional_box_keyword boxrel full_arcattrlist_with_label braced_arclist  */
#line 5047 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        Box::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        Box::AttributeValues(csh.hintAttrName, csh);
    else
        csh.CheckEntityHintAfter((yylsp[0]));
  #else
    ((yyvsp[-2].arcbox))->AddArcList((yyvsp[0].arclist))->SetLineEnd(CHART_POS2((yylsp[-3]), (yylsp[-1])));
    (yyval.arcboxseries) = ((yyvsp[-4].arcboxseries))->AddBox((yyvsp[-2].arcbox), (yyvsp[-1].attriblist));
  #endif
}
#line 9483 "msc_csh_lang.cc"
    break;

  case 322: /* box_list: box_list braced_arclist  */
#line 5061 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintAfter((yylsp[0]));
  #else
    FileLineColRange box_file_pos(FileLineCol(chart.current_file, ((yylsp[0])).first_line, ((yylsp[0])).first_column - 1),
						            FileLineCol(chart.current_file, ((yylsp[0])).first_line, ((yylsp[0])).first_column - 1));
    Box *temp = new Box(EArcSymbol::BOX_UNDETERMINED_FOLLOW, box_file_pos,
                        nullptr, CHART_POS((yylsp[-1])), nullptr, CHART_POS((yylsp[-1])), &chart);
    temp->AddArcList((yyvsp[0].arclist));
	temp->SetLineEnd(box_file_pos);
    (yyval.arcboxseries) = ((yyvsp[-1].arcboxseries))->AddBox(temp, nullptr);
  #endif
}
#line 9501 "msc_csh_lang.cc"
    break;

  case 323: /* box_list: box_list full_arcattrlist_with_label braced_arclist  */
#line 5075 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        Box::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        Box::AttributeValues(csh.hintAttrName, csh);
    else
        csh.CheckEntityHintAfter((yylsp[0]));
  #else
    FileLineColRange box_file_pos(FileLineCol(chart.current_file, ((yylsp[-1])).first_line, ((yylsp[-1])).first_column - 1),
						            FileLineCol(chart.current_file, ((yylsp[-1])).first_line, ((yylsp[-1])).first_column - 1));
    Box *temp = new Box(EArcSymbol::BOX_UNDETERMINED_FOLLOW, box_file_pos,
                        nullptr, CHART_POS((yylsp[-2])), nullptr, CHART_POS((yylsp[-2])), &chart);
    temp->AddArcList((yyvsp[0].arclist))->SetLineEnd(CHART_POS2((yylsp[-1]), (yylsp[-1])));
    (yyval.arcboxseries) = ((yyvsp[-2].arcboxseries))->AddBox(temp, (yyvsp[-1].attriblist));
  #endif
}
#line 9523 "msc_csh_lang.cc"
    break;

  case 324: /* mscgen_box: mscgen_boxrel  */
#line 5094 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].arcbox)) {
        ((yyvsp[0].arcbox))->SetLineEnd(CHART_POS((yyloc)));
        (yyval.arcboxseries) = new BoxSeries((yyvsp[0].arcbox), nullptr);
    } else
        (yyval.arcboxseries) = nullptr;
  #endif
}
#line 9537 "msc_csh_lang.cc"
    break;

  case 325: /* mscgen_box: mscgen_boxrel full_arcattrlist_with_label  */
#line 5104 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Box::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Box::AttributeValues(csh.hintAttrName, csh);
  #else
    if ((yyvsp[-1].arcbox)) {
        ((yyvsp[-1].arcbox))->SetLineEnd(CHART_POS((yyloc)));
        (yyval.arcboxseries) = new BoxSeries((yyvsp[-1].arcbox), (yyvsp[0].attriblist));
    } else
        (yyval.arcboxseries) = nullptr;
  #endif
}
#line 9556 "msc_csh_lang.cc"
    break;

  case 326: /* mscgen_boxlist: mscgen_box  */
#line 5120 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].arcboxseries)) {
        (yyval.arclist) = new ArcList;
        ((yyval.arclist))->Append((yyvsp[0].arcboxseries));
    } else
        (yyval.arclist) = nullptr;
  #endif
}
#line 9570 "msc_csh_lang.cc"
    break;

  case 327: /* mscgen_boxlist: mscgen_boxlist TOK_COMMA mscgen_box  */
#line 5130 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[0].arcboxseries)) {
        if ((yyvsp[-2].arclist)) {
            ((yyvsp[-2].arclist))->back()->SetParallel();
            (yyval.arclist) = (yyvsp[-2].arclist);
        } else
            (yyval.arclist) = new ArcList;
        ((yyval.arclist))->Append((yyvsp[0].arcboxseries));
    } else
        (yyval.arclist) = (yyvsp[-2].arclist);
  #endif
}
#line 9590 "msc_csh_lang.cc"
    break;

  case 328: /* first_box: boxrel  */
#line 5149 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    ((yyvsp[0].arcbox))->SetLineEnd(CHART_POS((yyloc)));
    (yyval.arcboxseries) = new BoxSeries((yyvsp[0].arcbox), nullptr);
  #endif
}
#line 9601 "msc_csh_lang.cc"
    break;

  case 329: /* first_box: boxrel full_arcattrlist_with_label  */
#line 5156 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Box::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Box::AttributeValues(csh.hintAttrName, csh);
  #else
    ((yyvsp[-1].arcbox))->SetLineEnd(CHART_POS((yyloc)));
    (yyval.arcboxseries) = new BoxSeries((yyvsp[-1].arcbox), (yyvsp[0].attriblist));
  #endif
}
#line 9617 "msc_csh_lang.cc"
    break;

  case 330: /* first_box: boxrel braced_arclist  */
#line 5168 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    ((yyvsp[-1].arcbox))->SetLineEnd(CHART_POS((yylsp[-1])));
    ((yyvsp[-1].arcbox))->AddArcList((yyvsp[0].arclist));
    (yyval.arcboxseries) = new BoxSeries((yyvsp[-1].arcbox), nullptr);
  #endif
}
#line 9629 "msc_csh_lang.cc"
    break;

  case 331: /* first_box: boxrel full_arcattrlist_with_label braced_arclist  */
#line 5176 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        Box::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        Box::AttributeValues(csh.hintAttrName, csh);
  #else
    ((yyvsp[-2].arcbox))->SetLineEnd(CHART_POS2((yylsp[-2]), (yylsp[-1])));
    ((yyvsp[-2].arcbox))->AddArcList((yyvsp[0].arclist));
    (yyval.arcboxseries) = new BoxSeries((yyvsp[-2].arcbox), (yyvsp[-1].attriblist));
  #endif
}
#line 9646 "msc_csh_lang.cc"
    break;

  case 332: /* first_pipe: TOK_COMMAND_PIPE boxrel  */
#line 5191 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcpipe) = new Pipe((yyvsp[0].arcbox));
    ((yyval.arcpipe))->SetLineEnd(CHART_POS((yyloc)));
    ((yyval.arcpipe))->AddAttributeList(nullptr);
  #endif
    free((yyvsp[-1].str));
}
#line 9668 "msc_csh_lang.cc"
    break;

  case 333: /* first_pipe: TOK_COMMAND_PIPE error  */
#line 5209 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a box symbol.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a box symbol. Ignoring pipe.");
    (yyval.arcpipe) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 9690 "msc_csh_lang.cc"
    break;

  case 334: /* first_pipe: TOK_COMMAND_PIPE  */
#line 5227 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else
        csh.CheckEntityHintAfter((yylsp[0]));
  #else
    (yyval.arcpipe) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "The keyword '" + string((yyvsp[0].str)) +"' should be followed by an entity, or '--', '..', '++' or '=='.");
  #endif
    free((yyvsp[0].str));
}
#line 9709 "msc_csh_lang.cc"
    break;

  case 335: /* first_pipe: TOK_COMMAND_PIPE boxrel full_arcattrlist_with_label  */
#line 5242 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Pipe::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Pipe::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.arcpipe) = new Pipe((yyvsp[-1].arcbox));
    ((yyval.arcpipe))->SetLineEnd(CHART_POS((yyloc)));
    ((yyval.arcpipe))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-2].str));
}
#line 9734 "msc_csh_lang.cc"
    break;

  case 336: /* pipe_list_no_content: first_pipe  */
#line 5264 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintAfter((yyloc));
  #else
    if ((yyvsp[0].arcpipe))
      (yyval.arcpipeseries) = new PipeSeries((yyvsp[0].arcpipe));
    else
      (yyval.arcpipeseries) = nullptr;
  #endif
}
#line 9749 "msc_csh_lang.cc"
    break;

  case 337: /* pipe_list_no_content: pipe_list_no_content boxrel  */
#line 5275 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintAfter((yyloc));
#else
    //($2) is never nullptr: "boxrel" always return a value (except oo memory)
    Pipe *ap = new Pipe((yyvsp[0].arcbox));
    ap->SetLineEnd(CHART_POS((yylsp[0])));
    if ((yyvsp[-1].arcpipeseries))
      (yyval.arcpipeseries) = ((yyvsp[-1].arcpipeseries))->AddFollowWithAttributes(ap, nullptr);
    else {
      ap->AddAttributeList(nullptr);
      (yyval.arcpipeseries) = new PipeSeries(ap);
    }
  #endif
}
#line 9769 "msc_csh_lang.cc"
    break;

  case 338: /* pipe_list_no_content: pipe_list_no_content boxrel full_arcattrlist_with_label  */
#line 5291 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Pipe::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Pipe::AttributeValues(csh.hintAttrName, csh);
    else
        csh.CheckEntityHintAfter((yyloc));
  #else
    //($2) is never nullptr: "boxrel" always return a value (except oo memory)
    Pipe *ap = new Pipe((yyvsp[-1].arcbox));
    ap->SetLineEnd(CHART_POS2((yylsp[-1]), (yylsp[0])));
    if ((yyvsp[-2].arcpipeseries))
      (yyval.arcpipeseries) = ((yyvsp[-2].arcpipeseries))->AddFollowWithAttributes(ap, (yyvsp[0].attriblist));
    else {
      ap->AddAttributeList((yyvsp[0].attriblist));
      (yyval.arcpipeseries) = new PipeSeries(ap);
    }
  #endif
}
#line 9794 "msc_csh_lang.cc"
    break;

  case 340: /* pipe_list: pipe_list_no_content braced_arclist  */
#line 5314 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintBetween((yylsp[-1]), (yylsp[0]));
  #else
    (yyval.arcpipeseries) = ((yyvsp[-1].arcpipeseries))->AddArcList((yyvsp[0].arclist));
  #endif
}
#line 9806 "msc_csh_lang.cc"
    break;

  case 343: /* boxrel: entity_string emphrel entity_string  */
#line 5325 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintAt((yylsp[-2]));
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0]));
    if (!(yyvsp[0].multi_str).had_error)
         csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error)
        (yyval.arcbox) = nullptr;
    else
        (yyval.arcbox) = new Box((yyvsp[-1].arcsymbol), CHART_POS((yylsp[-1])), (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), &chart);
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 9829 "msc_csh_lang.cc"
    break;

  case 344: /* boxrel: emphrel entity_string  */
#line 5344 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0]));
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.arcbox) = nullptr;
    else
        (yyval.arcbox) = new Box((yyvsp[-1].arcsymbol), CHART_POS((yylsp[-1])), nullptr, CHART_POS((yylsp[-1])), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), &chart);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 9848 "msc_csh_lang.cc"
    break;

  case 345: /* boxrel: entity_string emphrel  */
#line 5359 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintAt((yylsp[-1]));
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.CheckEntityHintAfter((yylsp[0]));
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.arcbox) = nullptr;
    else
        (yyval.arcbox) = new Box((yyvsp[0].arcsymbol), CHART_POS((yylsp[0])), (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), nullptr, CHART_POS((yylsp[0])), &chart);
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 9868 "msc_csh_lang.cc"
    break;

  case 346: /* boxrel: emphrel  */
#line 5375 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.CheckEntityHintAfter((yylsp[0]));
  #else
    (yyval.arcbox) = new Box((yyvsp[0].arcsymbol), CHART_POS((yylsp[0])), nullptr, CHART_POS((yylsp[0])), nullptr, CHART_POS((yylsp[0])), &chart);
  #endif
}
#line 9881 "msc_csh_lang.cc"
    break;

  case 351: /* mscgen_boxrel: entity_string mscgen_emphrel entity_string  */
#line 5387 "msc_lang.yy"
{
    //mscgen compatibility: a box with no content or
#ifdef C_S_H_IS_COMPILED
    csh.CheckEntityHintAt((yylsp[-2]));
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (csh.mscgen_compat == EMscgenCompat::FORCE_MSCGEN)
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    else
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD_MSCGEN);
    csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0]));
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
#else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error) {
        (yyval.arcbox) = nullptr;
    } else {
        Box::Emscgen_compat c = Box::MSCGEN_COMPAT_NONE;
        if (CaseInsensitiveEqual((yyvsp[-1].str), "rbox")) c = Box::MSCGEN_COMPAT_RBOX;
        else if (CaseInsensitiveEqual((yyvsp[-1].str), "abox")) c = Box::MSCGEN_COMPAT_ABOX;
        else if (CaseInsensitiveEqual((yyvsp[-1].str), "box")) c = Box::MSCGEN_COMPAT_BOX;
        else if (CaseInsensitiveEqual((yyvsp[-1].str), "note")) c = Box::MSCGEN_COMPAT_NOTE;
        (yyval.arcbox) = new Box(c, CHART_POS((yylsp[-1])), (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), &chart);
    }
    if (chart.mscgen_compat != EMscgenCompat::FORCE_MSCGEN)
        chart.Error.WarnMscgen(CHART_POS_START((yylsp[-1])), "Deprecated box symbol.", "Use '--' and attribute 'line.corner' instead.");
#endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 9917 "msc_csh_lang.cc"
    break;

  case 352: /* vertxpos: TOK_AT entity_string  */
#line 5420 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 9943 "msc_csh_lang.cc"
    break;

  case 353: /* vertxpos: TOK_AT entity_string TOK_NUMBER  */
#line 5442 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-2]), (yylsp[-1]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), VertXPos::POS_AT, atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].multi_str).str);
    free((yyvsp[0].str));
}
#line 9971 "msc_csh_lang.cc"
    break;

  case 354: /* vertxpos: TOK_AT entity_string TOK_DASH  */
#line 5466 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-2]), (yylsp[-1]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    } else
        csh.CheckEntityHintAfter((yylsp[0]));
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), VertXPos::POS_LEFT_SIDE);
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].multi_str).str);
}
#line 9999 "msc_csh_lang.cc"
    break;

  case 355: /* vertxpos: TOK_AT entity_string TOK_DASH TOK_NUMBER  */
#line 5490 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-3]), (yylsp[-2]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-2].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), VertXPos::POS_LEFT_SIDE, atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 10028 "msc_csh_lang.cc"
    break;

  case 356: /* vertxpos: TOK_AT entity_string TOK_PLUS  */
#line 5515 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-2]), (yylsp[-1]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), VertXPos::POS_RIGHT_SIDE);
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].multi_str).str);
}
#line 10055 "msc_csh_lang.cc"
    break;

  case 357: /* vertxpos: TOK_AT entity_string TOK_PLUS TOK_NUMBER  */
#line 5538 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-3]), (yylsp[-2]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    }
#else
    if ((yyvsp[-2].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), VertXPos::POS_RIGHT_SIDE, atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 10084 "msc_csh_lang.cc"
    break;

  case 358: /* vertxpos: TOK_AT entity_string emphrel  */
#line 5563 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-2]), (yylsp[-1]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else switch ((yyvsp[0].arcsymbol)) {
    default:
        _ASSERT(0);
        FALLTHROUGH;
    case EArcSymbol::BOX_SOLID:
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), VertXPos::POS_LEFT_BY);
        break;
    case EArcSymbol::BOX_DASHED:
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), VertXPos::POS_RIGHT_BY);
        break;
    case EArcSymbol::BOX_DOTTED:
        chart.Error.Error(CHART_POS_START((yylsp[0])),
                        "unexpected '..', expected '-', '--', '+' or '++'."
                        " Ignoring vertical.");
        (yyval.vertxpos) = nullptr;
        break;
    case EArcSymbol::BOX_DOUBLE:
        chart.Error.Error(CHART_POS_START((yylsp[0])),
                        "unexpected '==', expected '-', '--', '+' or '++'."
                        " Ignoring vertical.");
        (yyval.vertxpos) = nullptr;
        break;
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].multi_str).str);
}
#line 10132 "msc_csh_lang.cc"
    break;

  case 359: /* vertxpos: TOK_AT entity_string TOK_DASH entity_string  */
#line 5607 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-3]), (yylsp[-2]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    } else
        csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0]));
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 10163 "msc_csh_lang.cc"
    break;

  case 360: /* vertxpos: TOK_AT entity_string TOK_EMPH entity_string  */
#line 5634 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-3]), (yylsp[-2]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    } else
        csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0]));
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 10194 "msc_csh_lang.cc"
    break;

  case 361: /* vertxpos: TOK_AT entity_string TOK_DASH entity_string TOK_NUMBER  */
#line 5661 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-3]), (yyvsp[-3].multi_str).str);
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-4]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-4]), (yylsp[-3]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    } else
        csh.CheckEntityHintBetweenAndAt((yylsp[-2]), (yylsp[-1]));
  #else
    if ((yyvsp[-3].multi_str).had_error || (yyvsp[-1].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-3].multi_str).str, CHART_POS((yylsp[-3])), (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-4].str));
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[-1].multi_str).str);
    free((yyvsp[0].str));
}
#line 10227 "msc_csh_lang.cc"
    break;

  case 362: /* vertxpos: TOK_AT entity_string TOK_EMPH entity_string TOK_NUMBER  */
#line 5690 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-3]), (yyvsp[-3].multi_str).str);
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-4]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-4]), (yylsp[-3]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    } else
        csh.CheckEntityHintBetweenAndAt((yylsp[-2]), (yylsp[-1]));
  #else
    if ((yyvsp[-3].multi_str).had_error || (yyvsp[-1].multi_str).had_error)
        (yyval.vertxpos) = nullptr;
    else
        (yyval.vertxpos) = new VertXPos(chart, (yyvsp[-3].multi_str).str, CHART_POS((yylsp[-3])), (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), atof((yyvsp[0].str)));
  #endif
    free((yyvsp[-4].str));
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[-1].multi_str).str);
    free((yyvsp[0].str));
}
#line 10260 "msc_csh_lang.cc"
    break;

  case 363: /* vertxpos: TOK_AT  */
#line 5719 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintAfter((yylsp[0]))) {
        csh.AddVertXPosSyntaxNonSelectableToHints(false);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an entity name.");
  #else
    (yyval.vertxpos) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing an entity name.");
  #endif
    free((yyvsp[0].str));
}
#line 10282 "msc_csh_lang.cc"
    break;

  case 364: /* empharcrel_straight: emphrel  */
#line 5739 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
	(yyval.arctypeplusdir) = 0; //dummy to supress warning
  #else
    (yyval.arctypeplusdir) = new ArcTypePlusDir((yyvsp[0].arcsymbol), EDirType::INDETERMINATE);
  #endif
}
#line 10295 "msc_csh_lang.cc"
    break;

  case 365: /* empharcrel_straight: TOK_ASTERISK emphrel  */
#line 5748 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    csh.CheckEntityHintBetween((yylsp[-1]), (yylsp[0]));
	(yyval.arctypeplusdir) = 0; //dummy to supress warning
  #else
    (yyval.arctypeplusdir) = new ArcTypePlusDir((yyvsp[0].arcsymbol), EArrowLost::AT_SRC, EDirType::INDETERMINATE, CHART_POS((yylsp[-1])));
  #endif
}
#line 10309 "msc_csh_lang.cc"
    break;

  case 366: /* empharcrel_straight: emphrel TOK_ASTERISK  */
#line 5758 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
	(yyval.arctypeplusdir) = 0; //dummy to supress warning
  #else
    (yyval.arctypeplusdir) = new ArcTypePlusDir((yyvsp[-1].arcsymbol), EArrowLost::AT_SRC, EDirType::INDETERMINATE, CHART_POS((yylsp[0])));
  #endif
}
#line 10322 "msc_csh_lang.cc"
    break;

  case 367: /* empharcrel_straight: TOK_ASTERISK emphrel TOK_ASTERISK  */
#line 5767 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
    csh.CheckEntityHintBetween((yylsp[-2]), (yylsp[-1]));
    (yyval.arctypeplusdir) = 0; //dummy to supress warning
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Only one loss can be specified. Ignoring second asterisk ('*').");
    (yyval.arctypeplusdir) = new ArcTypePlusDir((yyvsp[-1].arcsymbol), EArrowLost::AT_SRC, EDirType::INDETERMINATE, CHART_POS((yylsp[-2])));
  #endif
}
#line 10338 "msc_csh_lang.cc"
    break;

  case 368: /* empharcrel_straight: relation_from  */
#line 5779 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	(yyval.arctypeplusdir) = 0; //dummy to supress warning
  #else
    (yyval.arctypeplusdir) = new ArcTypePlusDir((yyvsp[0].arcsegdata), EDirType::LEFT);
  #endif
}
#line 10350 "msc_csh_lang.cc"
    break;

  case 369: /* empharcrel_straight: relation_to  */
#line 5787 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	(yyval.arctypeplusdir) = 0; //dummy to supress warning
  #else
    (yyval.arctypeplusdir) = new ArcTypePlusDir((yyvsp[0].arcsegdata), EDirType::RIGHT);
  #endif
}
#line 10362 "msc_csh_lang.cc"
    break;

  case 370: /* empharcrel_straight: relation_bidir  */
#line 5795 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	(yyval.arctypeplusdir) = 0; //dummy to supress warning
  #else
    (yyval.arctypeplusdir) = new ArcTypePlusDir((yyvsp[0].arcsegdata), EDirType::BIDIR);
  #endif
}
#line 10374 "msc_csh_lang.cc"
    break;

  case 371: /* vertrel_no_xpos: entity_string empharcrel_straight entity_string  */
#line 5804 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
       csh.AddCSH((yylsp[-2]), COLOR_MARKERNAME);
    if (!(yyvsp[0].multi_str).had_error)
       csh.AddCSH((yylsp[0]), COLOR_MARKERNAME);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::MARKER))
        if (csh.hint_vertical_shapes)
            csh.AddVerticalTypesToHints();
    csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::MARKER);
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error)
        (yyval.arcvertarrow) = nullptr;
    else
        (yyval.arcvertarrow) = new Vertical((yyvsp[-1].arctypeplusdir), (yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, &chart);
    delete (yyvsp[-1].arctypeplusdir);
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 10399 "msc_csh_lang.cc"
    break;

  case 372: /* vertrel_no_xpos: empharcrel_straight entity_string  */
#line 5825 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_MARKERNAME);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::MARKER))
        if (csh.hint_vertical_shapes)
            csh.AddVerticalTypesToHints();
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.arcvertarrow) = nullptr;
    else
        (yyval.arcvertarrow) = new Vertical((yyvsp[-1].arctypeplusdir), MARKER_HERE_STR, (yyvsp[0].multi_str).str, &chart);
    delete (yyvsp[-1].arctypeplusdir);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 10420 "msc_csh_lang.cc"
    break;

  case 373: /* vertrel_no_xpos: entity_string empharcrel_straight  */
#line 5842 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH((yylsp[-1]), COLOR_MARKERNAME);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::MARKER))
        if (csh.hint_vertical_shapes)
            csh.AddVerticalTypesToHints();
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::MARKER);
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.arcvertarrow) = nullptr;
    else
        (yyval.arcvertarrow) = new Vertical((yyvsp[0].arctypeplusdir), (yyvsp[-1].multi_str).str, MARKER_HERE_STR, &chart);
    delete (yyvsp[0].arctypeplusdir);
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 10442 "msc_csh_lang.cc"
    break;

  case 374: /* vertrel_no_xpos: empharcrel_straight  */
#line 5860 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::MARKER);
  #else
    (yyval.arcvertarrow) = new Vertical((yyvsp[0].arctypeplusdir), MARKER_HERE_STR, MARKER_HERE_STR, &chart);
    delete (yyvsp[0].arctypeplusdir);
  #endif
}
#line 10455 "msc_csh_lang.cc"
    break;

  case 375: /* vertrel_no_xpos: entity_string  */
#line 5869 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_MARKERNAME);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::MARKER))
        if (csh.hint_vertical_shapes)
            csh.AddVerticalTypesToHints();
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a box or arrow symbol.");
  #else
    (yyval.arcvertarrow) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a box or arrow symbol.");
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 10474 "msc_csh_lang.cc"
    break;

  case 376: /* vertrel: vertrel_no_xpos vertxpos  */
#line 5886 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[-1].arcvertarrow))
        (yyval.arcvertarrow) = ((yyvsp[-1].arcvertarrow))->AddXpos((yyvsp[0].vertxpos));
    else
        (yyval.arcvertarrow) = nullptr;
    delete (yyvsp[0].vertxpos);
  #endif
}
#line 10489 "msc_csh_lang.cc"
    break;

  case 377: /* vertrel: vertrel_no_xpos  */
#line 5897 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    }
#else
    if ((yyvsp[0].arcvertarrow)) {
        VertXPos vxp;
        (yyval.arcvertarrow) = ((yyvsp[0].arcvertarrow))->AddXpos(&vxp);
    } else
        (yyval.arcvertarrow) = nullptr;
  #endif
}
#line 10508 "msc_csh_lang.cc"
    break;

  case 378: /* vertrel: vertxpos  */
#line 5912 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].vertxpos)) {
		ArcTypePlusDir typeplusdir;
		typeplusdir.arc.type = EArcSymbol::ARC_SOLID;
		typeplusdir.arc.lost = EArrowLost::NOT;
		typeplusdir.dir = EDirType::RIGHT;
		Vertical *ava = new Vertical(&typeplusdir, MARKER_HERE_STR, MARKER_HERE_STR, &chart);
		ava->AddXpos((yyvsp[0].vertxpos));
		(yyval.arcvertarrow) = ava;
		delete (yyvsp[0].vertxpos);
	} else
	    (yyval.arcvertarrow) = nullptr;
  #endif
}
#line 10529 "msc_csh_lang.cc"
    break;

  case 379: /* arcrel: TOK_SPECIAL_ARC  */
#line 5930 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
  #else
    (yyval.arcbase) = new Divider((yyvsp[0].arcsymbol), &chart);
  #endif
}
#line 10541 "msc_csh_lang.cc"
    break;

  case 380: /* arcrel: arrow_with_specifier  */
#line 5938 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddArrowSpecifiersToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
    //explicit copy here to suppress bison warning (since the two types are different)
    (yyval.arcbase) = (yyvsp[0].arcarrow);
}
#line 10556 "msc_csh_lang.cc"
    break;

  case 381: /* arcrel: arrow_with_specifier_incomplete  */
#line 5949 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #endif
    //explicit copy here to suppress bison warning (since the two types are different)
    (yyval.arcbase) = (yyvsp[0].arcarrow);
}
#line 10567 "msc_csh_lang.cc"
    break;

  case 382: /* arcrel: arrow_with_specifier error  */
#line 5956 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Could not figure out what comes here.");
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddArrowSpecifiersToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Could not figure out what comes here.");
  #endif
    //explicit copy here to suppress bison warning (since the two types are different)
    (yyval.arcbase) = (yyvsp[-1].arcarrow);
}
#line 10585 "msc_csh_lang.cc"
    break;

  case 383: /* arcrel: arrow_with_specifier_incomplete error  */
#line 5970 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Could not figure out what comes here.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Could not figure out what comes here.");
  #endif
    //explicit copy here to suppress bison warning (since the two types are different)
    (yyval.arcbase) = (yyvsp[-1].arcarrow);
}
#line 10599 "msc_csh_lang.cc"
    break;

  case 384: /* arrow_with_specifier_incomplete: arrow_with_specifier entity_string_single  */
#line 5981 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (CaseInsensitiveBeginsWith("lost", (yyvsp[0].str)) ||
        CaseInsensitiveBeginsWith("start", (yyvsp[0].str)) ||
        CaseInsensitiveBeginsWith("end", (yyvsp[0].str)))
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD_PARTIAL);
    else
        csh.AddCSH_Error((yylsp[0]), "Expecting 'lost at', 'start before' or 'end after' clauses, a semicolon ';' or attributes '['.");
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddArrowSpecifiersToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Expecting 'lost at', 'start before' or 'end after' clauses, a semicolon ';' or attributes '['.");
    (yyval.arcarrow) = ((yyvsp[-1].arcarrow));
  #endif
   free((yyvsp[0].str));
}
#line 10622 "msc_csh_lang.cc"
    break;

  case 385: /* arrow_with_specifier_incomplete: arrow_with_specifier TOK_LOST  */
#line 6000 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'at' clause.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing 'at' clause.");
    (yyval.arcarrow) = ((yyvsp[-1].arcarrow));
  #endif
   free((yyvsp[0].str));
}
#line 10641 "msc_csh_lang.cc"
    break;

  case 386: /* arrow_with_specifier_incomplete: arrow_with_specifier TOK_START  */
#line 6015 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'before' keyword.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "before",
            "Use the 'start before' keyword to indicate where the message originates from.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing 'before' keyword.");
    (yyval.arcarrow) = ((yyvsp[-1].arcarrow));
  #endif
   free((yyvsp[0].str));
}
#line 10662 "msc_csh_lang.cc"
    break;

  case 387: /* arrow_with_specifier_incomplete: arrow_with_specifier TOK_START entity_string_single  */
#line 6032 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (CaseInsensitiveBeginsWith("before", (yyvsp[0].str)))
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD_PARTIAL);
    else
        csh.AddCSH_Error((yylsp[0]), "Missing 'before' keyword.");
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "before",
            "Use the 'start before' keyword to indicate where the message originates from.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing 'before' keyword.");
    (yyval.arcarrow) = ((yyvsp[-2].arcarrow));
  #endif
   free((yyvsp[-1].str));
   free((yyvsp[0].str));
}
#line 10687 "msc_csh_lang.cc"
    break;

  case 388: /* arrow_with_specifier_incomplete: arrow_with_specifier TOK_START TOK_BEFORE  */
#line 6053 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an entity after 'start before'.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddStartBeforeEndAfterContentToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing entity after the 'start before' clause.");
    (yyval.arcarrow) = ((yyvsp[-2].arcarrow));
  #endif
   free((yyvsp[-1].str));
   free((yyvsp[0].str));
}
#line 10708 "msc_csh_lang.cc"
    break;

  case 389: /* arrow_with_specifier_incomplete: arrow_with_specifier TOK_END  */
#line 6070 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'after' keyword.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "after",
            "Use the 'end after' keyword to indicate where the message is sent to.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing 'after' keyword.");
    (yyval.arcarrow) = ((yyvsp[-1].arcarrow));
  #endif
   free((yyvsp[0].str));
}
#line 10729 "msc_csh_lang.cc"
    break;

  case 390: /* arrow_with_specifier_incomplete: arrow_with_specifier TOK_END entity_string_single  */
#line 6087 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (CaseInsensitiveBeginsWith("after", (yyvsp[0].str)))
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD_PARTIAL);
    else
        csh.AddCSH_Error((yylsp[0]), "Missing 'after' keyword.");
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "after",
            "Use the 'end after' keyword to indicate where the message is sent to.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing 'after' keyword.");
    (yyval.arcarrow) = ((yyvsp[-2].arcarrow));
  #endif
   free((yyvsp[-1].str));
   free((yyvsp[0].str));
}
#line 10754 "msc_csh_lang.cc"
    break;

  case 391: /* arrow_with_specifier_incomplete: arrow_with_specifier TOK_END TOK_AFTER  */
#line 6108 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an entity after 'end after'.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddStartBeforeEndAfterContentToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing entity after the 'end after' clause.");
    (yyval.arcarrow) = ((yyvsp[-2].arcarrow));
  #endif
   free((yyvsp[-1].str));
   free((yyvsp[0].str));
}
#line 10775 "msc_csh_lang.cc"
    break;

  case 392: /* arrow_with_specifier: arcrel_arrow  */
#line 6127 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddArrowSpecifiersToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.arcarrow) = (yyvsp[0].arcarrow);
}
#line 10789 "msc_csh_lang.cc"
    break;

  case 393: /* arrow_with_specifier: arrow_with_specifier TOK_LOST vertxpos  */
#line 6137 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
  #else
    if ((yyvsp[-2].arcarrow))
        (yyval.arcarrow) = ((yyvsp[-2].arcarrow))->AddLostPos((yyvsp[0].vertxpos), CHART_POS2((yylsp[-1]), (yylsp[0])));
    else {
        (yyval.arcarrow) = nullptr;
        delete (yyvsp[0].vertxpos);
    }
  #endif
   free((yyvsp[-1].str));
}
#line 10807 "msc_csh_lang.cc"
    break;

  case 394: /* arrow_with_specifier: arrow_with_specifier TOK_START TOK_BEFORE special_ending  */
#line 6151 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
  #else
    (yyval.arcarrow) = ((yyvsp[-3].arcarrow))->AddStartPos((yyvsp[0].arrowending));
  #endif
   free((yyvsp[-2].str));
   free((yyvsp[-1].str));
}
#line 10822 "msc_csh_lang.cc"
    break;

  case 395: /* arrow_with_specifier: arrow_with_specifier TOK_END TOK_AFTER special_ending  */
#line 6162 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
  #else
    (yyval.arcarrow) = ((yyvsp[-3].arcarrow))->AddEndPos((yyvsp[0].arrowending));
  #endif
   free((yyvsp[-2].str));
   free((yyvsp[-1].str));
}
#line 10837 "msc_csh_lang.cc"
    break;

  case 396: /* special_ending: entity_string  */
#line 6174 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ENTITYNAME);
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.arrowending) = nullptr;
    else
        (yyval.arrowending) = new ArrowEnding(chart, (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])));
  #endif
   free((yyvsp[0].multi_str).str);
}
#line 10854 "msc_csh_lang.cc"
    break;

  case 397: /* special_ending: entity_string TOK_NUMBER  */
#line 6187 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH((yylsp[-1]), COLOR_ENTITYNAME);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if ((yyvsp[-1].multi_str).had_error)
        (yyval.arrowending) = nullptr;
    else
        (yyval.arrowending) = new ArrowEnding(chart, (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), atof((yyvsp[0].str)));
  #endif
   free((yyvsp[-1].multi_str).str);
   free((yyvsp[0].str));
}
#line 10873 "msc_csh_lang.cc"
    break;

  case 398: /* special_ending: TOK_COLORDEF_NAME_NUMBER  */
#line 6202 "msc_lang.yy"
{
  //We allow this to capture <entity>+-number without space
  //Find the +-sign's offset
  unsigned off = 0;
  while (((yyvsp[0].str))[off] && ((yyvsp[0].str))[off]!='+' && ((yyvsp[0].str))[off]!='-') off++;
  #ifdef C_S_H_IS_COMPILED
    CshPos pos = (yylsp[0]);
    pos.last_pos = (pos.first_pos + off)-1;
    csh.AddCSH(pos, COLOR_ENTITYNAME);
    pos = (yylsp[0]);
    pos.first_pos = (pos.first_pos+off);
    csh.AddCSH(pos, COLOR_ATTRVALUE);
  #else
    double offset = atof(((yyvsp[0].str))+off);
    auto pos = CHART_POS((yylsp[0]));
    pos.end.col -= strlen((yyvsp[0].str))-off;
    ((yyvsp[0].str))[off] = 0; //truncate string to actual entity name
    (yyval.arrowending) = new ArrowEnding(chart, (yyvsp[0].str), pos, offset);
  #endif
   free((yyvsp[0].str));
}
#line 10899 "msc_csh_lang.cc"
    break;

  case 402: /* arcrel_arrow: entity_string TOK_REL_MSCGEN entity_string  */
#line 6227 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    //This rule can happen only in mscgen compat mode
    //(or else the lexer does not return TOK_REL_MSCGEN
    _ASSERT(csh.mscgen_compat == EMscgenCompat::FORCE_MSCGEN);
    if (csh.CheckEntityHintAt((yylsp[-2])) || csh.CheckEntityHintAt((yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
#else
    //This rule can happen only in mscgen compat mode
    //(or else the lexer does not return TOK_REL_MSCGEN
    _ASSERT(chart.mscgen_compat == EMscgenCompat::FORCE_MSCGEN);
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = nullptr;
    else {
        ArrowSegmentData data((yyvsp[-1].arcsymbol));
        (yyval.arcarrow) = chart.CreateArrow(data, (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, true, CHART_POS((yylsp[0])), (yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param);
        ((yyval.arcarrow))->Indicate_Mscgen_Headless();
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 10931 "msc_csh_lang.cc"
    break;

  case 403: /* arcrel_arrow: entity_string TOK_REL_DASH_X entity_string  */
#line 6255 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])) || csh.CheckEntityHintAt((yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = nullptr;
    else {
        ArrowSegmentData data(EArcSymbol::ARC_SOLID, EArrowLost::AT_DST, CHART_POS((yylsp[-1])));
        (yyval.arcarrow) = chart.CreateArrow(data, (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, true, CHART_POS((yylsp[0])), (yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param);
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 10956 "msc_csh_lang.cc"
    break;

  case 404: /* arcrel_arrow: entity_string TOK_REL_X TOK_DASH entity_string  */
#line 6276 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-3])) || csh.CheckEntityHintAt((yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[-3].multi_str).had_error)
    csh.AddCSH_EntityName((yylsp[-3]), (yyvsp[-3].multi_str).str);
    if (csh.mscgen_compat == EMscgenCompat::FORCE_MSCGEN) {
        if (((yylsp[-2])).last_pos+1 == ((yylsp[-1])).first_pos) {
            csh.AddCSH((yylsp[-2]) + (yylsp[-1]), COLOR_SYMBOL);
            if (!(yyvsp[0].multi_str).had_error)
                csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
        } else {
            csh.AddCSH_Error((yylsp[-2]), "Missing arrow symbol before 'x'. (Perhaps remove space after?)");
        }
    } else {
        if (((yylsp[-2])).last_pos+1 == ((yylsp[-1])).first_pos) {
            csh.AddCSH_Error((yylsp[-2]) + (yylsp[-1]), "Unsupported arrow symbol ('x-'), use '* <-' for indicating loss instead.");
        } else {
            csh.AddCSH_Error((yylsp[-2]), "Missing arrow symbol before 'x'.");
        }
    }
  #else
    if (chart.mscgen_compat == EMscgenCompat::FORCE_MSCGEN) {
        if (CHART_POS_AFTER((yylsp[-2])) == CHART_POS_START((yylsp[-1]))) {
            if ((yyvsp[-3].multi_str).had_error || (yyvsp[0].multi_str).had_error)
                (yyval.arcarrow) = nullptr;
            else {
                ArrowSegmentData data(EArcSymbol::ARC_SOLID, EArrowLost::AT_DST, CHART_POS((yylsp[-2])));
                (yyval.arcarrow) = chart.CreateArrow(data, (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), (yyvsp[-3].multi_str).str, false, CHART_POS((yylsp[-3])), (yyvsp[-3].multi_str).had_param || (yyvsp[0].multi_str).had_param);
            }
        } else {
            chart.Error.Error(CHART_POS_START((yylsp[-2])), "Missing arrow symbol before 'x'.",
                "Perhaps remove the whitespace after?");
            (yyval.arcarrow) = nullptr;
        }
    } else {
        if (CHART_POS_AFTER((yylsp[-2])) == CHART_POS_START((yylsp[-1]))) {
            chart.Error.Error(CHART_POS_START((yylsp[-2])), "This arrow symbol ('x-') is supported only in mscgen compatibility mode.",
                "Use an asterisk '*' instead to express a lost message (like '-> *').");
            (yyval.arcarrow) = nullptr;
        } else {
            chart.Error.Error(CHART_POS_START((yylsp[-2])), "Missing arrow symbol before 'x'.");
            (yyval.arcarrow) = nullptr;
        }
    }
  #endif
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 11011 "msc_csh_lang.cc"
    break;

  case 405: /* arcrel_arrow: entity_string TOK_REL_DASH_X  */
#line 6327 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-1])))
        csh.AllowAnything();
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an entity name.");
  #else
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing an entity name after the loss arrow symbol.");
    (yyval.arcarrow) = nullptr;
#endif
    free((yyvsp[-1].multi_str).str);
}
#line 11030 "msc_csh_lang.cc"
    break;

  case 406: /* arcrel_arrow: entity_string TOK_REL_X TOK_DASH  */
#line 6342 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (csh.mscgen_compat == EMscgenCompat::FORCE_MSCGEN) {
        if (((yylsp[-1])).last_pos+1 == ((yylsp[0])).first_pos) {
            csh.AddCSH((yylsp[-1]) + (yylsp[0]), COLOR_SYMBOL);
            csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing an entity name.");
        } else {
            csh.AddCSH_Error((yylsp[-1]), "Missing arrow symbol before 'x'. (Perhaps remove space after?)");
        }
    } else {
        if (((yylsp[-1])).last_pos+1 == ((yylsp[0])).first_pos) {
            csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), "Unsupported arrow symbol ('x-'), use '* <-' for indicating loss instead.");
        } else {
            csh.AddCSH_Error((yylsp[-1]), "Missing arrow symbol before 'x'.");
        }
    }
  #else
    if (chart.mscgen_compat == EMscgenCompat::FORCE_MSCGEN && CHART_POS_AFTER((yylsp[-1])) == CHART_POS_START((yylsp[0])))
         chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing an entity name after the loss arrow symbol.");
    else
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing arrow symbol before 'x'.");
    (yyval.arcarrow) = nullptr;
#endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[-1].str));
}
#line 11065 "msc_csh_lang.cc"
    break;

  case 407: /* arcrel_arrow: entity_string TOK_DASH entity_string  */
#line 6373 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])) || csh.CheckEntityHintAt((yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
    csh.AddCSH_Error((yylsp[-1]), "Missing an arrow or box symbol.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing an arrow or box symbol. Ignoring this line.");
    (yyval.arcarrow) = nullptr;
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 11086 "msc_csh_lang.cc"
    break;

  case 408: /* arcrel_arrow: TOK_DASH entity_string  */
#line 6390 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1])+(yylsp[0]), "Missing an arrow or box symbol.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing an arrow or box symbol. Ignoring this line.");
    (yyval.arcarrow) = nullptr;
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11100 "msc_csh_lang.cc"
    break;

  case 409: /* arcrel_arrow: entity_string TOK_DASH  */
#line 6400 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-1])))
        csh.AllowAnything();
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH_Error((yylsp[0]), "Missing an arrow or box symbol.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing an arrow or box symbol. Ignoring this line.");
    (yyval.arcarrow) = nullptr;
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 11118 "msc_csh_lang.cc"
    break;

  case 410: /* arcrel_to: entity_string relation_to entity_string  */
#line 6416 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])) || csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, true, CHART_POS((yylsp[0])), (yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 11141 "msc_csh_lang.cc"
    break;

  case 411: /* arcrel_to: relation_to  */
#line 6435 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[0]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
  #else
    if ((yyvsp[0].arcsegdata))
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[0].arcsegdata), LSIDE_ENT_STR, CHART_POS((yylsp[0])), RSIDE_ENT_STR, true, CHART_POS((yylsp[0])), false);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[0].arcsegdata);
  #endif
}
#line 11160 "msc_csh_lang.cc"
    break;

  case 412: /* arcrel_to: relation_to entity_string  */
#line 6450 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), LSIDE_ENT_STR, CHART_POS((yylsp[-1])), (yyvsp[0].multi_str).str, true, CHART_POS((yylsp[0])), (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11180 "msc_csh_lang.cc"
    break;

  case 413: /* arcrel_to: TOK_PIPE_SYMBOL relation_to entity_string  */
#line 6466 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
       csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), LSIDE_PIPE_ENT_STR, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, true, CHART_POS((yylsp[0])), (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11201 "msc_csh_lang.cc"
    break;

  case 414: /* arcrel_to: entity_string relation_to  */
#line 6483 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-1])))
        csh.AllowAnything();
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    if (csh.CheckEntityHintAfter((yylsp[0]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
  #else
    if ((yyvsp[0].arcsegdata) && !(yyvsp[-1].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[0].arcsegdata), (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), RSIDE_ENT_STR, true, CHART_POS((yylsp[0])), (yyvsp[-1].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[0].arcsegdata);
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 11225 "msc_csh_lang.cc"
    break;

  case 415: /* arcrel_to: entity_string relation_to TOK_PIPE_SYMBOL  */
#line 6503 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (csh.CheckEntityHintAfter((yylsp[-1]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[-2].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), RSIDE_PIPE_ENT_STR, true, CHART_POS((yylsp[0])), (yyvsp[-2].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 11250 "msc_csh_lang.cc"
    break;

  case 416: /* arcrel_to: arcrel_to relation_to_cont entity_string  */
#line 6524 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = ((yyvsp[-2].arcarrow))->AddSegment(*(yyvsp[-1].arcsegdata), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), CHART_POS2((yylsp[-1]), (yylsp[0])));
    else
        (yyval.arcarrow) = (yyvsp[-2].arcarrow);
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11270 "msc_csh_lang.cc"
    break;

  case 417: /* arcrel_to: arcrel_to relation_to_cont  */
#line 6540 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[-1]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
  #else
    if ((yyvsp[0].arcsegdata))
        (yyval.arcarrow) = ((yyvsp[-1].arcarrow))->AddSegment(*(yyvsp[0].arcsegdata), nullptr, CHART_POS((yylsp[0])), CHART_POS((yylsp[0])));
    else
        (yyval.arcarrow) = (yyvsp[-1].arcarrow);
    delete (yyvsp[0].arcsegdata);
  #endif
}
#line 11289 "msc_csh_lang.cc"
    break;

  case 418: /* arcrel_to: arcrel_to relation_to_cont TOK_PIPE_SYMBOL  */
#line 6555 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[-2]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
  #else
    if ((yyvsp[-1].arcsegdata))
        (yyval.arcarrow) = ((yyvsp[-2].arcarrow))->AddSegment(*(yyvsp[-1].arcsegdata), RSIDE_PIPE_ENT_STR, CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])));
    else
        (yyval.arcarrow) = (yyvsp[-2].arcarrow);
    delete (yyvsp[-1].arcsegdata);
  #endif
}
#line 11309 "msc_csh_lang.cc"
    break;

  case 419: /* arcrel_from: entity_string relation_from entity_string  */
#line 6573 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])) || csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), (yyvsp[-2].multi_str).str, false, CHART_POS((yylsp[-2])), (yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 11332 "msc_csh_lang.cc"
    break;

  case 420: /* arcrel_from: relation_from  */
#line 6592 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[0]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
  #else
    if ((yyvsp[0].arcsegdata))
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[0].arcsegdata), RSIDE_ENT_STR, CHART_POS((yylsp[0])), LSIDE_ENT_STR, false, CHART_POS((yylsp[0])), false);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[0].arcsegdata);
  #endif
}
#line 11351 "msc_csh_lang.cc"
    break;

  case 421: /* arcrel_from: relation_from entity_string  */
#line 6607 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), LSIDE_ENT_STR, false, CHART_POS((yylsp[-1])), (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11371 "msc_csh_lang.cc"
    break;

  case 422: /* arcrel_from: TOK_PIPE_SYMBOL relation_from entity_string  */
#line 6623 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), LSIDE_PIPE_ENT_STR, false, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11392 "msc_csh_lang.cc"
    break;

  case 423: /* arcrel_from: entity_string relation_from  */
#line 6640 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-1])))
        csh.AllowAnything();
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    if (csh.CheckEntityHintAfter((yylsp[0]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
  #else
    if ((yyvsp[0].arcsegdata) && !(yyvsp[-1].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[0].arcsegdata), RSIDE_ENT_STR, CHART_POS((yylsp[0])), (yyvsp[-1].multi_str).str, false, CHART_POS((yylsp[-1])), (yyvsp[-1].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[0].arcsegdata);
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 11416 "msc_csh_lang.cc"
    break;

  case 424: /* arcrel_from: entity_string relation_from TOK_PIPE_SYMBOL  */
#line 6660 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (csh.CheckEntityHintAfter((yylsp[-1]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[-2].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), RSIDE_PIPE_ENT_STR, CHART_POS((yylsp[0])), (yyvsp[-2].multi_str).str, false, CHART_POS((yylsp[-2])), (yyvsp[-2].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 11441 "msc_csh_lang.cc"
    break;

  case 425: /* arcrel_from: arcrel_from relation_from_cont entity_string  */
#line 6681 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = ((yyvsp[-2].arcarrow))->AddSegment(*(yyvsp[-1].arcsegdata), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), CHART_POS2((yylsp[-1]), (yylsp[0])));
    else
        (yyval.arcarrow) = (yyvsp[-2].arcarrow);
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11461 "msc_csh_lang.cc"
    break;

  case 426: /* arcrel_from: arcrel_from relation_from_cont  */
#line 6697 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[0]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
  #else
    if ((yyvsp[0].arcsegdata))
        (yyval.arcarrow) = ((yyvsp[-1].arcarrow))->AddSegment(*(yyvsp[0].arcsegdata), nullptr, CHART_POS((yylsp[0])), CHART_POS((yylsp[0])));
    else
        (yyval.arcarrow) = (yyvsp[-1].arcarrow);
    delete (yyvsp[0].arcsegdata);
  #endif
}
#line 11480 "msc_csh_lang.cc"
    break;

  case 427: /* arcrel_from: arcrel_from relation_from_cont TOK_PIPE_SYMBOL  */
#line 6712 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[-1]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
  #else
    if ((yyvsp[-1].arcsegdata))
        (yyval.arcarrow) = ((yyvsp[-2].arcarrow))->AddSegment(*(yyvsp[-1].arcsegdata), RSIDE_PIPE_ENT_STR, CHART_POS((yylsp[0])), CHART_POS((yylsp[-1])));
    else
        (yyval.arcarrow) = (yyvsp[-2].arcarrow);
    delete (yyvsp[-1].arcsegdata);
  #endif
}
#line 11500 "msc_csh_lang.cc"
    break;

  case 428: /* arcrel_bidir: entity_string relation_bidir entity_string  */
#line 6730 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])) ||csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, true, CHART_POS((yylsp[0])), (yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 11523 "msc_csh_lang.cc"
    break;

  case 429: /* arcrel_bidir: relation_bidir entity_string  */
#line 6749 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), LSIDE_ENT_STR, CHART_POS((yylsp[-1])), (yyvsp[0].multi_str).str, true, CHART_POS((yylsp[0])), (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11543 "msc_csh_lang.cc"
    break;

  case 430: /* arcrel_bidir: relation_bidir  */
#line 6765 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[0]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
  #else
    if ((yyvsp[0].arcsegdata))
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[0].arcsegdata), LSIDE_ENT_STR, CHART_POS((yylsp[0])), RSIDE_ENT_STR, true, CHART_POS((yylsp[0])), false);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[0].arcsegdata);
  #endif
}
#line 11562 "msc_csh_lang.cc"
    break;

  case 431: /* arcrel_bidir: TOK_PIPE_SYMBOL relation_bidir entity_string  */
#line 6780 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), LSIDE_PIPE_ENT_STR, CHART_POS((yylsp[-2])), (yyvsp[0].multi_str).str, true, CHART_POS((yylsp[0])), (yyvsp[0].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11583 "msc_csh_lang.cc"
    break;

  case 432: /* arcrel_bidir: entity_string relation_bidir  */
#line 6797 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-1])))
        csh.AllowAnything();
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    if (csh.CheckEntityHintAfter((yylsp[0])))
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
  #else
    if ((yyvsp[0].arcsegdata) && !(yyvsp[-1].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[0].arcsegdata), (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), RSIDE_ENT_STR, true, CHART_POS((yylsp[0])), (yyvsp[-1].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[0].arcsegdata);
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 11605 "msc_csh_lang.cc"
    break;

  case 433: /* arcrel_bidir: entity_string relation_bidir TOK_PIPE_SYMBOL  */
#line 6815 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAt((yylsp[-2])))
        csh.AllowAnything();
    csh.AddCSH_EntityName((yylsp[-2]), (yyvsp[-2].multi_str).str);
    if (csh.CheckEntityHintAfter((yylsp[-1]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[-2].multi_str).had_error)
        (yyval.arcarrow) = chart.CreateArrow(*(yyvsp[-1].arcsegdata), (yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2])), RSIDE_PIPE_ENT_STR, true, CHART_POS((yylsp[0])), (yyvsp[-2].multi_str).had_param);
    else
        (yyval.arcarrow) = nullptr;
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 11629 "msc_csh_lang.cc"
    break;

  case 434: /* arcrel_bidir: arcrel_bidir relation_bidir_cont entity_string  */
#line 6835 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.AllowAnything();
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_EntityName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
    if ((yyvsp[-1].arcsegdata) && !(yyvsp[0].multi_str).had_error)
        (yyval.arcarrow) = ((yyvsp[-2].arcarrow))->AddSegment(*(yyvsp[-1].arcsegdata), (yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), CHART_POS2((yylsp[-1]), (yylsp[0])));
    else
        (yyval.arcarrow) = (yyvsp[-2].arcarrow);
    delete (yyvsp[-1].arcsegdata);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11649 "msc_csh_lang.cc"
    break;

  case 435: /* arcrel_bidir: arcrel_bidir relation_bidir_cont  */
#line 6851 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[0]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
  #else
    if ((yyvsp[0].arcsegdata))
        (yyval.arcarrow) = ((yyvsp[-1].arcarrow))->AddSegment(*(yyvsp[0].arcsegdata), nullptr, CHART_POS((yylsp[0])), CHART_POS((yylsp[0])));
    else
        (yyval.arcarrow) = (yyvsp[-1].arcarrow);
    delete (yyvsp[0].arcsegdata);
  #endif
}
#line 11668 "msc_csh_lang.cc"
    break;

  case 436: /* arcrel_bidir: arcrel_bidir relation_bidir_cont TOK_PIPE_SYMBOL  */
#line 6866 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckEntityHintAfter((yylsp[-1]))) {
        csh.hintStatus = HINT_FILLING; //to allow adding "lost_at"
        csh.AllowAnything();
    }
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
  #else
    if ((yyvsp[-1].arcsegdata))
        (yyval.arcarrow) = ((yyvsp[-2].arcarrow))->AddSegment(*(yyvsp[-1].arcsegdata), RSIDE_PIPE_ENT_STR, CHART_POS((yylsp[0])), CHART_POS((yylsp[-1])));
    else
        (yyval.arcarrow) = (yyvsp[-2].arcarrow);
    delete (yyvsp[-1].arcsegdata);
  #endif
}
#line 11688 "msc_csh_lang.cc"
    break;

  case 438: /* relation_to_cont_no_loss: TOK_DASH  */
#line 6882 "msc_lang.yy"
                                                {(yyval.arcsymbol)=EArcSymbol::ARC_UNDETERMINED_SEGMENT;}
#line 11694 "msc_csh_lang.cc"
    break;

  case 440: /* relation_from_cont_no_loss: TOK_DASH  */
#line 6883 "msc_lang.yy"
                                                    {(yyval.arcsymbol)=EArcSymbol::ARC_UNDETERMINED_SEGMENT;}
#line 11700 "msc_csh_lang.cc"
    break;

  case 442: /* relation_bidir_cont_no_loss: TOK_DASH  */
#line 6884 "msc_lang.yy"
                                                      {(yyval.arcsymbol)=EArcSymbol::ARC_UNDETERMINED_SEGMENT;}
#line 11706 "msc_csh_lang.cc"
    break;

  case 443: /* relation_to: TOK_REL_TO  */
#line 6887 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 0;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol));
  #endif
}
#line 11719 "msc_csh_lang.cc"
    break;

  case 444: /* relation_to: TOK_ASTERISK TOK_REL_TO  */
#line 6896 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 1;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[-1])));
  #endif
}
#line 11732 "msc_csh_lang.cc"
    break;

  case 445: /* relation_to: TOK_REL_TO TOK_ASTERISK  */
#line 6905 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 1;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_DST, CHART_POS((yylsp[0])));
  #endif
}
#line 11745 "msc_csh_lang.cc"
    break;

  case 446: /* relation_to: TOK_ASTERISK TOK_REL_TO TOK_ASTERISK  */
#line 6914 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]) + (yylsp[-1]), COLOR_SYMBOL);
    csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
    csh.asteriskNo = 2;
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "One arrow may be lost only once. Ignoring subsequent asterisks ('*').");
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[-2])));
  #endif
}
#line 11760 "msc_csh_lang.cc"
    break;

  case 447: /* relation_from: TOK_REL_FROM  */
#line 6926 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 1;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol));
  #endif
}
#line 11773 "msc_csh_lang.cc"
    break;

  case 448: /* relation_from: TOK_ASTERISK TOK_REL_FROM  */
#line 6935 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 1;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol), EArrowLost::AT_DST, CHART_POS((yylsp[-1])));
  #endif
}
#line 11786 "msc_csh_lang.cc"
    break;

  case 449: /* relation_from: TOK_REL_FROM TOK_ASTERISK  */
#line 6944 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 1;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[0])));
  #endif
}
#line 11799 "msc_csh_lang.cc"
    break;

  case 450: /* relation_from: TOK_ASTERISK TOK_REL_FROM TOK_ASTERISK  */
#line 6953 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]) + (yylsp[-1]), COLOR_SYMBOL);
    csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
    csh.asteriskNo = 2;
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "One arrow may be lost only once. Ignoring subsequent asterisks ('*').");
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_DST, CHART_POS((yylsp[-2])));
  #endif
}
#line 11814 "msc_csh_lang.cc"
    break;

  case 451: /* relation_bidir: TOK_REL_BIDIR  */
#line 6965 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 0;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol));
  #endif
}
#line 11827 "msc_csh_lang.cc"
    break;

  case 452: /* relation_bidir: TOK_ASTERISK TOK_REL_BIDIR  */
#line 6974 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 1;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[-1])));
  #endif
}
#line 11840 "msc_csh_lang.cc"
    break;

  case 453: /* relation_bidir: TOK_REL_BIDIR TOK_ASTERISK  */
#line 6983 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
    csh.asteriskNo = 1;
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_DST, CHART_POS((yylsp[0])));
  #endif
}
#line 11853 "msc_csh_lang.cc"
    break;

  case 454: /* relation_bidir: TOK_ASTERISK TOK_REL_BIDIR TOK_ASTERISK  */
#line 6992 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]) + (yylsp[-1]), COLOR_SYMBOL);
    csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
    csh.asteriskNo = 2;
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "One arrow may be lost only once. Ignoring subsequent asterisks ('*').");
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[-2])));
  #endif
}
#line 11868 "msc_csh_lang.cc"
    break;

  case 455: /* relation_to_cont: relation_to_cont_no_loss  */
#line 7004 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol));
  #endif
}
#line 11880 "msc_csh_lang.cc"
    break;

  case 456: /* relation_to_cont: TOK_ASTERISK relation_to_cont_no_loss  */
#line 7012 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[-1]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[-1])));
  #endif
}
#line 11896 "msc_csh_lang.cc"
    break;

  case 457: /* relation_to_cont: relation_to_cont_no_loss TOK_ASTERISK  */
#line 7024 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_DST, CHART_POS((yylsp[0])));
  #endif
}
#line 11912 "msc_csh_lang.cc"
    break;

  case 458: /* relation_to_cont: TOK_ASTERISK relation_to_cont_no_loss TOK_ASTERISK  */
#line 7036 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[-2]), MULTIPLE_ASTERISK_ERROR_MSG);
    csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "One arrow may be lost only once. Ignoring this asterisk ('*').");
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[-2])));
  #endif
}
#line 11930 "msc_csh_lang.cc"
    break;

  case 459: /* relation_from_cont: relation_from_cont_no_loss  */
#line 7051 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol));
  #endif
}
#line 11942 "msc_csh_lang.cc"
    break;

  case 460: /* relation_from_cont: TOK_ASTERISK relation_from_cont_no_loss  */
#line 7059 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[-1]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol), EArrowLost::AT_DST, CHART_POS((yylsp[-1])));
  #endif
}
#line 11958 "msc_csh_lang.cc"
    break;

  case 461: /* relation_from_cont: relation_from_cont_no_loss TOK_ASTERISK  */
#line 7071 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[0])));
  #endif
}
#line 11974 "msc_csh_lang.cc"
    break;

  case 462: /* relation_from_cont: TOK_ASTERISK relation_from_cont_no_loss TOK_ASTERISK  */
#line 7083 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[-2]), MULTIPLE_ASTERISK_ERROR_MSG);
    csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "One arrow may be lost only once. Ignoring this asterisk ('*').");
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_DST, CHART_POS((yylsp[-2])));
  #endif
}
#line 11992 "msc_csh_lang.cc"
    break;

  case 463: /* relation_bidir_cont: relation_bidir_cont_no_loss  */
#line 7098 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SYMBOL);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol));
  #endif
}
#line 12004 "msc_csh_lang.cc"
    break;

  case 464: /* relation_bidir_cont: TOK_ASTERISK relation_bidir_cont_no_loss  */
#line 7106 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[-1]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[0].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[-1])));
  #endif
}
#line 12020 "msc_csh_lang.cc"
    break;

  case 465: /* relation_bidir_cont: relation_bidir_cont_no_loss TOK_ASTERISK  */
#line 7118 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_DST, CHART_POS((yylsp[0])));
  #endif
}
#line 12036 "msc_csh_lang.cc"
    break;

  case 466: /* relation_bidir_cont: TOK_ASTERISK relation_bidir_cont_no_loss TOK_ASTERISK  */
#line 7130 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (++csh.asteriskNo == 1)
        csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    else
        csh.AddCSH_Error((yylsp[-2]), MULTIPLE_ASTERISK_ERROR_MSG);
    csh.AddCSH_Error((yylsp[0]), MULTIPLE_ASTERISK_ERROR_MSG);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "One arrow may be lost only once. Ignoring this asterisk ('*').");
    (yyval.arcsegdata) = new ArrowSegmentData((yyvsp[-1].arcsymbol), EArrowLost::AT_SRC, CHART_POS((yylsp[-2])));
  #endif
}
#line 12054 "msc_csh_lang.cc"
    break;

  case 468: /* extvertxpos: entity_string_single  */
#line 7148 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ExtvxposDesignatorName((yylsp[0]), (yyvsp[0].str));
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    }
  #else
    (yyval.extvertxpos) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 12071 "msc_csh_lang.cc"
    break;

  case 469: /* extvertxpos_no_string: TOK_AT_POS vertxpos  */
#line 7162 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ExtvxposDesignatorName((yylsp[-1]), (yyvsp[-1].str));
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.extvertxpos) = new ExtVertXPos((yyvsp[-1].str), CHART_POS((yylsp[-1])), (yyvsp[0].vertxpos));
    delete (yyvsp[0].vertxpos);
  #endif
    free((yyvsp[-1].str));
}
#line 12092 "msc_csh_lang.cc"
    break;

  case 470: /* extvertxpos_no_string: TOK_AT_POS  */
#line 7179 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ExtvxposDesignatorName((yylsp[0]), (yyvsp[0].str));
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddVertXPosSyntaxNonSelectableToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.extvertxpos) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 12112 "msc_csh_lang.cc"
    break;

  case 474: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string markerrel_no_string extvertxpos  */
#line 7200 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[-2]), (yyvsp[-2].str));
    if (csh.CheckLineStartHintAt((yylsp[-3]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-3]), (yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::MARKER)) {
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    }
  #else
    (yyval.arcbase) = new Symbol(&chart, (yyvsp[-2].str), (yyvsp[-1].namerel), (yyvsp[0].extvertxpos), nullptr);
    delete (yyvsp[-1].namerel);
    delete (yyvsp[0].extvertxpos);
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].str));
}
#line 12144 "msc_csh_lang.cc"
    break;

  case 475: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string markerrel_no_string extvertxpos extvertxpos  */
#line 7228 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[-3]), (yyvsp[-3].str));
    if (csh.CheckLineStartHintAt((yylsp[-4]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-4]), (yylsp[-3]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-3]), (yylsp[-2]), EHintSourceType::MARKER)) {
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    }
#else
    (yyval.arcbase) = new Symbol(&chart, (yyvsp[-3].str), (yyvsp[-2].namerel), (yyvsp[-1].extvertxpos), (yyvsp[0].extvertxpos));
    delete (yyvsp[-2].namerel);
    delete (yyvsp[-1].extvertxpos);
    delete (yyvsp[0].extvertxpos);
  #endif
    free((yyvsp[-4].str));
    free((yyvsp[-3].str));
}
#line 12180 "msc_csh_lang.cc"
    break;

  case 476: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string markerrel_no_string extvertxpos extvertxpos extvertxpos  */
#line 7261 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[-4]), (yyvsp[-4].str));
    if (csh.CheckLineStartHintAt((yylsp[-5]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-5]), (yylsp[-4]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-4]), (yylsp[-3]), EHintSourceType::MARKER)) {
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-3]), (yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    }
    csh.AddCSH_Error((yylsp[0]), "Too many left/center/right specifiers here, at most two can be given.");
#else
    (yyval.arcbase) = new Symbol(&chart, (yyvsp[-4].str), (yyvsp[-3].namerel), (yyvsp[-2].extvertxpos), (yyvsp[-1].extvertxpos));
    delete (yyvsp[-3].namerel);
    delete (yyvsp[-2].extvertxpos);
    delete (yyvsp[-1].extvertxpos);
    delete (yyvsp[0].extvertxpos);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Too many specifiers here, ignoring last one.", "At most two can be given.");
  #endif
    free((yyvsp[-5].str));
    free((yyvsp[-4].str));
}
#line 12222 "msc_csh_lang.cc"
    break;

  case 477: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string extvertxpos_no_string  */
#line 7299 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[-1]), (yyvsp[-1].str));
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::MARKER)) {
        csh.AddLeftRightCenterToHints(); //markers, plus left/center/right
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    }
  #else
    (yyval.arcbase) = new Symbol(&chart, (yyvsp[-1].str), nullptr, (yyvsp[0].extvertxpos), nullptr);
    delete (yyvsp[0].extvertxpos);
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
}
#line 12251 "msc_csh_lang.cc"
    break;

  case 478: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string extvertxpos_no_string extvertxpos  */
#line 7324 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[-2]), (yyvsp[-2].str));
    if (csh.CheckLineStartHintAt((yylsp[-3]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-3]), (yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::MARKER)) {
        csh.AddLeftRightCenterToHints(); //markers, plus left/center/right
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    }
#else
    (yyval.arcbase) = new Symbol(&chart, (yyvsp[-2].str), nullptr, (yyvsp[-1].extvertxpos), (yyvsp[0].extvertxpos));
    delete (yyvsp[-1].extvertxpos);
    delete (yyvsp[0].extvertxpos);
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].str));
}
#line 12284 "msc_csh_lang.cc"
    break;

  case 479: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string vertxpos  */
#line 7353 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[-1]), (yyvsp[-1].str));
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::MARKER)) {
        csh.AddLeftRightCenterToHints(); //markers, plus left/center/right
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcbase) = new Symbol(&chart, (yyvsp[-1].str), (yyvsp[0].vertxpos), CHART_POS((yylsp[0])));
    delete (yyvsp[0].vertxpos);
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
}
#line 12310 "msc_csh_lang.cc"
    break;

  case 480: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string markerrel_no_string  */
#line 7375 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[-1]), (yyvsp[-1].str));
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::MARKER)) {
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_FILLING;
    }
#else
    (yyval.arcbase) = new Symbol(&chart, (yyvsp[-1].str), (yyvsp[0].namerel), nullptr, nullptr);
    delete (yyvsp[0].namerel);
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
}
#line 12338 "msc_csh_lang.cc"
    break;

  case 481: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string entity_string  */
#line 7399 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[-1]), (yyvsp[-1].str));
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_LeftRightCenterMarker((yylsp[0]), (yyvsp[0].multi_str).str);
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::MARKER)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.arcbase) = nullptr;
    else
        (yyval.arcbase) = new Symbol(&chart, (yyvsp[-1].str), nullptr, nullptr, nullptr);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting 'left', 'right', 'center' or a marker name followed by a dash. Ignoring this piece.");
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 12370 "msc_csh_lang.cc"
    break;

  case 482: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL symbol_type_string  */
#line 7427 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_SymbolName((yylsp[0]), (yyvsp[0].str));
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::MARKER)) {
        csh.AddLeftRightCenterToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.arcbase) = new Symbol(&chart, (yyvsp[0].str), nullptr, nullptr, nullptr);
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 12395 "msc_csh_lang.cc"
    break;

  case 483: /* symbol_command_no_attr: TOK_COMMAND_SYMBOL  */
#line 7448 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddSymbolTypesToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yylsp[0])).end, "Missing a symbol type.", "Use 'arc', '...', 'text', 'rectangle', 'cross' or 'shape'.");
    (yyval.arcbase) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 12416 "msc_csh_lang.cc"
    break;

  case 484: /* symbol_command: symbol_command_no_attr  */
#line 7466 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].arcbase))
        ((yyvsp[0].arcbase))->AddAttributeList(nullptr);
    (yyval.arcbase) = ((yyvsp[0].arcbase));
  #endif
}
#line 12428 "msc_csh_lang.cc"
    break;

  case 485: /* symbol_command: symbol_command_no_attr full_arcattrlist_with_label  */
#line 7474 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Symbol::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Symbol::AttributeValues(csh.hintAttrName, csh);
  #else
    if ((yyvsp[-1].arcbase))
        ((yyvsp[-1].arcbase))->AddAttributeList((yyvsp[0].attriblist));
    (yyval.arcbase) = ((yyvsp[-1].arcbase));
  #endif
}
#line 12445 "msc_csh_lang.cc"
    break;

  case 486: /* note: TOK_COMMAND_NOTE TOK_AT string full_arcattrlist_with_label  */
#line 7488 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_EntityOrMarkerName((yylsp[-1]), (yyvsp[-1].multi_str).str);
    if (csh.CheckLineStartHintAt((yylsp[-3]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Note::AttributeNames(csh, true);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Note::AttributeValues(csh.hintAttrName, csh, true);
    else if (csh.CheckEntityHintBetweenAndAt((yylsp[-2]), (yylsp[-1])))
        csh.addMarkersAtEnd = true; //after 'at' a marker or an entity may come
  #else
    if ((yyvsp[-1].multi_str).had_error) {
        (yyval.arcbase) = nullptr;
        delete (yyvsp[0].attriblist);
    } else {
        (yyval.arcbase) = new Note(&chart, (yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])));
        ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
    }
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].str));
    free((yyvsp[-1].multi_str).str);
}
#line 12478 "msc_csh_lang.cc"
    break;

  case 487: /* note: TOK_COMMAND_NOTE full_arcattrlist_with_label  */
#line 7517 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "at", nullptr, EHintType::KEYWORD, true)); //XXX Other can come
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Note::AttributeNames(csh, true);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Note::AttributeValues(csh.hintAttrName, csh, true);
  #else
    (yyval.arcbase) = new Note(&chart);
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-1].str));
}
#line 12502 "msc_csh_lang.cc"
    break;

  case 488: /* note: TOK_COMMAND_NOTE TOK_AT  */
#line 7537 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing an entity or marker name.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Notes need a label.");
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintAfter((yylsp[0])))
        csh.addMarkersAtEnd = true; //after 'at' a marker or an entity may come
  #else
    (yyval.arcbase) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing an entity or marker name after 'at'.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a label, ignoring note.", "Notes and comments must have text, specify a label.");
#endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 12526 "msc_csh_lang.cc"
    break;

  case 489: /* note: TOK_COMMAND_NOTE TOK_AT full_arcattrlist_with_label  */
#line 7557 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing an entity or marker name.");
    if (csh.CheckLineStartHintAt((yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckEntityHintBetweenAndAt((yylsp[-1]), (yylsp[0])))
        csh.addMarkersAtEnd = true;
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Note::AttributeNames(csh, true);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Note::AttributeValues(csh.hintAttrName, csh, true);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing an entity or marker name after 'at'. Ignoring 'at' clause.");
    (yyval.arcbase) = new Note(&chart);
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
}
#line 12553 "msc_csh_lang.cc"
    break;

  case 490: /* comment_command: TOK_COMMAND_COMMENT  */
#line 7581 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.eside)= ESide::LEFT;
  #endif
    free((yyvsp[0].str));
}
#line 12564 "msc_csh_lang.cc"
    break;

  case 491: /* comment_command: TOK_COMMAND_ENDNOTE  */
#line 7588 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.eside)= ESide::END;
  #endif
    free((yyvsp[0].str));
}
#line 12575 "msc_csh_lang.cc"
    break;

  case 492: /* comment_command: TOK_COMMAND_FOOTNOTE  */
#line 7595 "msc_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.eside)= ESide::END;
  #endif
    free((yyvsp[0].str));
}
#line 12586 "msc_csh_lang.cc"
    break;

  case 493: /* comment: comment_command full_arcattrlist_with_label  */
#line 7603 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Note::AttributeNames(csh, false);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Note::AttributeValues(csh.hintAttrName, csh, false);
  #else
    (yyval.arcbase) = new Note(&chart, (yyvsp[-1].eside));
    ((yyval.arcbase))->AddAttributeList((yyvsp[0].attriblist));
  #endif
}
#line 12606 "msc_csh_lang.cc"
    break;

  case 494: /* comment: comment_command  */
#line 7619 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Comments and notes need a label.");
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a label, ignoring comment/note.", "Notes and comments must have text, specify a label.");
    (yyval.arcbase) = nullptr;
  #endif
}
#line 12624 "msc_csh_lang.cc"
    break;

  case 495: /* colon_string: TOK_COLON_QUOTED_STRING  */
#line 7634 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), false);
    csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 12636 "msc_csh_lang.cc"
    break;

  case 496: /* colon_string: TOK_COLON_STRING  */
#line 7642 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), true);
	csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 12648 "msc_csh_lang.cc"
    break;

  case 497: /* full_arcattrlist_with_label: colon_string  */
#line 7651 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attriblist) = (new AttributeList)->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yyloc)).IncStartCol()));
  #endif
    free((yyvsp[0].str));
}
#line 12660 "msc_csh_lang.cc"
    break;

  case 498: /* full_arcattrlist_with_label: colon_string full_arcattrlist  */
#line 7659 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attriblist) = ((yyvsp[0].attriblist))->Prepend(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()));
  #endif
    free((yyvsp[-1].str));
}
#line 12672 "msc_csh_lang.cc"
    break;

  case 499: /* full_arcattrlist_with_label: full_arcattrlist colon_string full_arcattrlist  */
#line 7667 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attriblist) = ((yyvsp[-2].attriblist))->Append(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()));
        //Merge $3 at the end of $1
        ((yyvsp[-2].attriblist))->splice(((yyvsp[-2].attriblist))->end(), *((yyvsp[0].attriblist)));
        delete ((yyvsp[0].attriblist)); //empty list now
        (yyval.attriblist) = (yyvsp[-2].attriblist);
  #endif
    free((yyvsp[-1].str));
}
#line 12688 "msc_csh_lang.cc"
    break;

  case 500: /* full_arcattrlist_with_label: full_arcattrlist colon_string  */
#line 7679 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attriblist) = ((yyvsp[-1].attriblist))->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yylsp[0])), CHART_POS((yylsp[0])).IncStartCol()));
  #endif
    free((yyvsp[0].str));
}
#line 12700 "msc_csh_lang.cc"
    break;

  case 502: /* full_arcattrlist: TOK_OSBRACKET TOK_CSBRACKET  */
#line 7690 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attriblist) = new AttributeList;
  #endif
}
#line 12715 "msc_csh_lang.cc"
    break;

  case 503: /* full_arcattrlist: TOK_OSBRACKET arcattrlist TOK_CSBRACKET  */
#line 7701 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
	csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attriblist) = (yyvsp[-1].attriblist);
  #endif
}
#line 12730 "msc_csh_lang.cc"
    break;

  case 504: /* full_arcattrlist: TOK_OSBRACKET arcattrlist error TOK_CSBRACKET  */
#line 7712 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[-1]), "Extra stuff after an attribute list. Maybe missing a comma?");
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attriblist) = (yyvsp[-2].attriblist);
  #endif
}
#line 12746 "msc_csh_lang.cc"
    break;

  case 505: /* full_arcattrlist: TOK_OSBRACKET error TOK_CSBRACKET  */
#line 7724 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as an attribute or style name.");
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attriblist) = new AttributeList;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting an attribute or style name. Ignoring all until the closing square bracket (']').");
#endif
}
#line 12763 "msc_csh_lang.cc"
    break;

  case 506: /* full_arcattrlist: TOK_OSBRACKET arcattrlist  */
#line 7737 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a square bracket (']').");
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
	csh.SqBracketPairs.push_back((yyloc));
  #else
    (yyval.attriblist) = (yyvsp[0].attriblist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 12779 "msc_csh_lang.cc"
    break;

  case 507: /* full_arcattrlist: TOK_OSBRACKET arcattrlist error  */
#line 7749 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[0]), "Missing a ']'.");
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attriblist) = (yyvsp[-1].attriblist);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing ']'.");
  #endif
}
#line 12795 "msc_csh_lang.cc"
    break;

  case 508: /* full_arcattrlist: TOK_OSBRACKET  */
#line 7761 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a square bracket (']').");
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attriblist) = new AttributeList;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 12811 "msc_csh_lang.cc"
    break;

  case 509: /* full_arcattrlist: TOK_OSBRACKET error  */
#line 7773 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[0]), "Missing a ']'.");
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attriblist) = new AttributeList;
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing ']'.");
  #endif
}
#line 12827 "msc_csh_lang.cc"
    break;

  case 510: /* arcattrlist: arcattr  */
#line 7786 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.attriblist) = (new AttributeList)->Append((yyvsp[0].attrib));
  #endif
}
#line 12838 "msc_csh_lang.cc"
    break;

  case 511: /* arcattrlist: arcattrlist TOK_COMMA arcattr  */
#line 7793 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attriblist) = ((yyvsp[-2].attriblist))->Append((yyvsp[0].attrib));
  #endif
}
#line 12851 "msc_csh_lang.cc"
    break;

  case 512: /* arcattrlist: arcattrlist TOK_COMMA  */
#line 7802 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing attribute or style name.");
  #else
    (yyval.attriblist) = (yyvsp[-1].attriblist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an attribute or style name here.");
  #endif
}
#line 12866 "msc_csh_lang.cc"
    break;

  case 513: /* arcattr: alpha_string TOK_EQUAL string  */
#line 7814 "msc_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error) {
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
        if (!(yyvsp[0].multi_str).had_error) {
            csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, (yyvsp[-2].multi_str).str);
            csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
        }
    }
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error ||
        (((yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param) && chart.SkipContent()))
        (yyval.attrib) = nullptr;
    else
        (yyval.attrib) = new Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 12893 "msc_csh_lang.cc"
    break;

  case 514: /* arcattr: alpha_string TOK_EQUAL color_def_string  */
#line 7837 "msc_lang.yy"
{
  //string=r,g,b,a or string=name,a
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error) {
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), (yyvsp[-2].multi_str).str);
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
    }
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
  #else
    if ((yyvsp[-2].multi_str).had_error || ((yyvsp[-2].multi_str).had_param && chart.SkipContent()))
        (yyval.attrib) = nullptr;
    else
        (yyval.attrib) = new Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 12917 "msc_csh_lang.cc"
    break;

  case 515: /* arcattr: alpha_string TOK_EQUAL TOK_EMPH_PLUS_PLUS color_string  */
#line 7856 "msc_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    if (!(yyvsp[-3].multi_str).had_error) {
        csh.AddCSH_AttrName((yylsp[-3]), (yyvsp[-3].multi_str).str, COLOR_ATTRNAME);
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[-1])+(yylsp[0]), (string("++")+(yyvsp[0].str)).c_str(), (yyvsp[-3].multi_str).str);
        csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1])+(yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-3].multi_str).str);
    }
    csh.CheckHintAt((yylsp[-3]), EHintSourceType::ATTR_NAME);
  #else
    if ((yyvsp[-3].multi_str).had_error || ((yyvsp[-3].multi_str).had_param && chart.SkipContent()))
        (yyval.attrib) = nullptr;
    else
        (yyval.attrib) = new Attribute((yyvsp[-3].multi_str).str, string("++")+(yyvsp[0].str), CHART_POS((yylsp[-3])), CHART_POS2((yylsp[-1]),(yylsp[0])));
  #endif
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[0].str));
}
#line 12941 "msc_csh_lang.cc"
    break;

  case 516: /* arcattr: alpha_string TOK_EQUAL TOK_EMPH_PLUS_PLUS  */
#line 7876 "msc_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error) {
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), "++", (yyvsp[-2].multi_str).str);
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Continue with a color name or definition.");
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
  #else
    if ((yyvsp[-2].multi_str).had_error || ((yyvsp[-2].multi_str).had_param && chart.SkipContent()))
        (yyval.attrib) = nullptr;
    else
        (yyval.attrib) = new Attribute((yyvsp[-2].multi_str).str, "++", CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 12965 "msc_csh_lang.cc"
    break;

  case 517: /* arcattr: alpha_string TOK_EQUAL TOK_NUMBER  */
#line 7896 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error) {
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
    }
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
  #else
    if ((yyvsp[-2].multi_str).had_error || ((yyvsp[-2].multi_str).had_param && chart.SkipContent()))
        (yyval.attrib) = nullptr;
    else
        (yyval.attrib) = new Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 12988 "msc_csh_lang.cc"
    break;

  case 518: /* arcattr: alpha_string TOK_EQUAL  */
#line 7915 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error) {
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_ATTRNAME);
        csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str);
    }
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    if ((yyvsp[-1].multi_str).had_error || ((yyvsp[-1].multi_str).had_param && chart.SkipContent()))
        (yyval.attrib) = nullptr;
    else
        (yyval.attrib) = new Attribute((yyvsp[-1].multi_str).str, {}, CHART_POS((yyloc)), CHART_POS((yyloc)));
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 13009 "msc_csh_lang.cc"
    break;

  case 519: /* arcattr: string  */
#line 7932 "msc_lang.yy"
{
  //here we accept non alpha strings for "->" and similar style names
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_StyleOrAttrName((yylsp[0]), (yyvsp[0].multi_str).str);
    csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.attrib) = nullptr;
    else
        (yyval.attrib) = new Attribute((yyvsp[0].multi_str).str, CHART_POS((yyloc)));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 13028 "msc_csh_lang.cc"
    break;

  case 520: /* arcattr: TOK_EMPH_PLUS_PLUS  */
#line 7948 "msc_lang.yy"
{
  //here we accept non alpha strings for "->" and similar style names
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_StyleOrAttrName((yylsp[0]), "++");
    csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attrib) = new Attribute("++", CHART_POS((yyloc)));
  #endif
}
#line 13042 "msc_csh_lang.cc"
    break;

  case 521: /* vertical_shape: TOK_VERTICAL_SHAPE  */
#line 7959 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.hint_vertical_shapes = false;
  #endif
  if (CaseInsensitiveEqual((yyvsp[0].str), "brace")) (yyval.vshape) = Vertical::BRACE;
  else if (CaseInsensitiveEqual((yyvsp[0].str), "bracket")) (yyval.vshape) = Vertical::BRACKET;
  else if (CaseInsensitiveEqual((yyvsp[0].str), "range")) (yyval.vshape) = Vertical::RANGE;
  else if (CaseInsensitiveEqual((yyvsp[0].str), "pointer")) (yyval.vshape) = Vertical::POINTER;
  else {
      (yyval.vshape) = Vertical::ARROW;
      _ASSERT(0);
  }
  free((yyvsp[0].str));
}
#line 13061 "msc_csh_lang.cc"
    break;

  case 522: /* vertical_shape: TOK_COMMAND_BOX  */
#line 7974 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.hint_vertical_shapes = false;
  #endif
  (yyval.vshape) = Vertical::BOX;
  free((yyvsp[0].str));
}
#line 13073 "msc_csh_lang.cc"
    break;

  case 523: /* vertical_shape: TOK_COMMAND_BIG  */
#line 7982 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.hint_vertical_shapes = false;
  #endif
  (yyval.vshape) = Vertical::ARROW;
  free((yyvsp[0].str));
}
#line 13085 "msc_csh_lang.cc"
    break;

  case 524: /* entity_string_single: TOK_QSTRING  */
#line 7994 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddQuotedString((yylsp[0]));
  #endif
  (yyval.str) = (yyvsp[0].str);
}
#line 13096 "msc_csh_lang.cc"
    break;

  case 527: /* entity_string_single: TOK_SHAPE_COMMAND  */
#line 8003 "msc_lang.yy"
{
	(yyval.str) = (char*)malloc(2);
	((yyval.str))[0] = ShapeElement::act_code[(yyvsp[0].shapecommand)];
	((yyval.str))[1] = 0;
}
#line 13106 "msc_csh_lang.cc"
    break;

  case 577: /* symbol_string: TOK_REL_TO  */
#line 8026 "msc_lang.yy"
{
    switch ((yyvsp[0].arcsymbol)) {
    case EArcSymbol::ARC_DOTTED: (yyval.str) = strdup(">"); break;
    case EArcSymbol::ARC_DASHED: (yyval.str) = strdup(">>"); break;
    case EArcSymbol::ARC_SOLID:  (yyval.str) = strdup("->"); break;
    case EArcSymbol::ARC_DOUBLE: (yyval.str) = strdup("=>"); break;
    case EArcSymbol::ARC_DBLDBL: (yyval.str) = strdup("=>>"); break;
    case EArcSymbol::ARC_DOUBLE2:
  #ifdef C_S_H_IS_COMPILED
        if (csh.mscgen_compat == EMscgenCompat::FORCE_MSCGEN)
  #else
        if (chart.mscgen_compat == EMscgenCompat::FORCE_MSCGEN)
  #endif
            (yyval.str) = strdup(":>");
        else
            (yyval.str) = strdup("==>");
        break;
    default: _ASSERT(0);
    }
}
#line 13131 "msc_csh_lang.cc"
    break;

  case 578: /* symbol_string: TOK_REL_FROM  */
#line 8047 "msc_lang.yy"
{
    switch ((yyvsp[0].arcsymbol)) {
    case EArcSymbol::ARC_DOTTED: (yyval.str) = strdup("<"); break;
    case EArcSymbol::ARC_DASHED: (yyval.str) = strdup("<<"); break;
    case EArcSymbol::ARC_SOLID:  (yyval.str) = strdup("<-"); break;
    case EArcSymbol::ARC_DOUBLE: (yyval.str) = strdup("<="); break;
    case EArcSymbol::ARC_DBLDBL: (yyval.str) = strdup("=>>"); break;
    case EArcSymbol::ARC_DOUBLE2:
  #ifdef C_S_H_IS_COMPILED
        if (csh.mscgen_compat == EMscgenCompat::FORCE_MSCGEN)
  #else
        if (chart.mscgen_compat == EMscgenCompat::FORCE_MSCGEN)
  #endif
            (yyval.str) = strdup("<:");
        else
            (yyval.str) = strdup("<==");
        break;
    default: _ASSERT(0);
    }
}
#line 13156 "msc_csh_lang.cc"
    break;

  case 579: /* symbol_string: TOK_REL_BIDIR  */
#line 8069 "msc_lang.yy"
{
    switch ((yyvsp[0].arcsymbol)) {
    case EArcSymbol::ARC_DOTTED: (yyval.str) = strdup("<>"); break;
    case EArcSymbol::ARC_DASHED: (yyval.str) = strdup("<<>>"); break;
    case EArcSymbol::ARC_SOLID:  (yyval.str) = strdup("<->"); break;
    case EArcSymbol::ARC_DOUBLE: (yyval.str) = strdup("<=>"); break;
    case EArcSymbol::ARC_DBLDBL: (yyval.str) = strdup("=>>"); break;
    case EArcSymbol::ARC_DOUBLE2:
  #ifdef C_S_H_IS_COMPILED
        if (csh.mscgen_compat == EMscgenCompat::FORCE_MSCGEN)
  #else
        if (chart.mscgen_compat == EMscgenCompat::FORCE_MSCGEN)
  #endif
            (yyval.str) = strdup("<:>");
        else
            (yyval.str) = strdup("<==>");
        break;
    default: _ASSERT(0);
    }
}
#line 13181 "msc_csh_lang.cc"
    break;

  case 580: /* symbol_string: TOK_REL_MSCGEN  */
#line 8090 "msc_lang.yy"
{
    //This can only come in mscgen mode
    switch ((yyvsp[0].arcsymbol)) {
    case EArcSymbol::ARC_DOTTED: (yyval.str) = strdup(".."); break;
    case EArcSymbol::ARC_SOLID:  (yyval.str) = strdup("--"); break;
    case EArcSymbol::ARC_DOUBLE: (yyval.str) = strdup("=="); break;
    case EArcSymbol::ARC_DOUBLE2: (yyval.str) = strdup("::"); break;
    default: _ASSERT(0);
    }
}
#line 13196 "msc_csh_lang.cc"
    break;

  case 581: /* symbol_string: TOK_SPECIAL_ARC  */
#line 8101 "msc_lang.yy"
{
    switch ((yyvsp[0].arcsymbol)) {
    case EArcSymbol::DIV_DIVIDER:  (yyval.str) = strdup("---"); break;
    case EArcSymbol::DIV_DISCO:    (yyval.str) = strdup("..."); break;
    case EArcSymbol::DIV_VSPACE:   (yyval.str) = strdup("|||"); break;
    default: _ASSERT(0);
    }
}
#line 13209 "msc_csh_lang.cc"
    break;

  case 582: /* symbol_string: TOK_EMPH  */
#line 8110 "msc_lang.yy"
{
    switch ((yyvsp[0].arcsymbol)) {
    case EArcSymbol::BOX_SOLID:  (yyval.str) = strdup("--"); break;
    case EArcSymbol::BOX_DASHED: (yyval.str) = strdup("++"); break; //will likely not happen due to special handling in TOK_COLORDEF
    case EArcSymbol::BOX_DOTTED: (yyval.str) = strdup(".."); break;
    case EArcSymbol::BOX_DOUBLE: (yyval.str) = strdup("=="); break;
    default: _ASSERT(0);
    }
}
#line 13223 "msc_csh_lang.cc"
    break;

  case 586: /* tok_param_name_as_multi: TOK_PARAM_NAME  */
#line 8126 "msc_lang.yy"
{
    (yyval.multi_str).str = nullptr;
    (yyval.multi_str).multi = true;
    (yyval.multi_str).had_param = true;
    (yyval.multi_str).had_error = false;
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].str)==nullptr || (yyvsp[0].str)[0]!='$' || (yyvsp[0].str)[1]==0)
        csh.AddCSH_Error((yylsp[0]), "Need name after the '$' sign.");
  #else
    if ((yyvsp[0].str)==nullptr || (yyvsp[0].str)[0]!='$' || (yyvsp[0].str)[1]==0) {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Need name after the '$' sign.");
        (yyval.multi_str).had_error = true;
    } else if (!chart.SkipContent()) {
        //When parsing a procedure we we accept all params
        //as they may be variables defined later
        auto p = chart.GetParameter((yyvsp[0].str));
        if (p==nullptr) {
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Undefined parameter or variable name.");
            (yyval.multi_str).had_error = true;
        } else {
            (yyval.multi_str).str = strdup(StringFormat::PushPosEscapes(p->value.c_str(), CHART_POS_START((yylsp[0]))).c_str());
        }
    }
  #endif
  //avoid returning null
  if ((yyval.multi_str).str==nullptr)
        (yyval.multi_str).str = strdup("");
  free((yyvsp[0].str));
}
#line 13257 "msc_csh_lang.cc"
    break;

  case 587: /* multi_string_continuation: TOK_TILDE  */
#line 8159 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to concatenate after '~'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to concatenate after '~'.");
  #endif
    (yyval.multi_str).str = strdup("");
    (yyval.multi_str).multi = true;
    (yyval.multi_str).had_param = false;
    (yyval.multi_str).had_error = true;
}
#line 13273 "msc_csh_lang.cc"
    break;

  case 588: /* multi_string_continuation: TOK_TILDE string  */
#line 8171 "msc_lang.yy"
{
    (yyval.multi_str) = (yyvsp[0].multi_str);
}
#line 13281 "msc_csh_lang.cc"
    break;

  case 589: /* entity_string_single_or_param: entity_string_single  */
#line 8176 "msc_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 13289 "msc_csh_lang.cc"
    break;

  case 591: /* entity_string: entity_string_single_or_param  */
#line 8182 "msc_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].multi_str));
}
#line 13297 "msc_csh_lang.cc"
    break;

  case 592: /* entity_string: entity_string_single_or_param multi_string_continuation  */
#line 8186 "msc_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].multi_str));
}
#line 13305 "msc_csh_lang.cc"
    break;

  case 594: /* alpha_string: alpha_string_single  */
#line 8192 "msc_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 13313 "msc_csh_lang.cc"
    break;

  case 595: /* alpha_string: alpha_string_single multi_string_continuation  */
#line 8196 "msc_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), (yyvsp[0].multi_str));
}
#line 13321 "msc_csh_lang.cc"
    break;

  case 597: /* string: string_single  */
#line 8202 "msc_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 13329 "msc_csh_lang.cc"
    break;

  case 598: /* string: string_single multi_string_continuation  */
#line 8206 "msc_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), (yyvsp[0].multi_str));
}
#line 13337 "msc_csh_lang.cc"
    break;

  case 599: /* scope_open: TOK_OCBRACKET  */
#line 8211 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext();
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (YYGET_EXTRA(yyscanner)->open_context_mode == base_parse_parm::EScopeOpenMode::PROC_REPLAY) {
        //Open a scope to replay a procedure. The text of the procedure has
        //already been placed to the lex buffer stack (in fact the '{' already comes
        //from there), and the parameters are in YYEXTRA.
        YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
        chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::REPARSING);
        chart.MyCurrentContext().parameters = std::move(YYGET_EXTRA(yyscanner)->last_procedure_params);
        chart.MyCurrentContext().starts_procedure = true;
        chart.MyCurrentContext().export_colors = YYGET_EXTRA(yyscanner)->last_procedure->export_colors;
        chart.MyCurrentContext().export_styles = YYGET_EXTRA(yyscanner)->last_procedure->export_styles;
        YYGET_EXTRA(yyscanner)->last_procedure = nullptr;
    } else {
        //Just open a regular scope
        chart.PushContext(CHART_POS_START((yylsp[0])));
    }
  #endif
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 13369 "msc_csh_lang.cc"
    break;

  case 600: /* scope_close: TOK_CCBRACKET  */
#line 8240 "msc_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.arcbase) = nullptr;
    csh.PopContext();
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
  #else
    OptAttr<double> hscale = chart.MyCurrentContext().hscale;
    (yyval.arcbase) = chart.PopContext().release();
    if (hscale)
        chart.MyCurrentContext().hscale = hscale;
  #endif
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 13387 "msc_csh_lang.cc"
    break;


#line 13391 "msc_csh_lang.cc"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= TOK_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == TOK_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, RESULT, yyscanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, RESULT, yyscanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, RESULT, yyscanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 8255 "msc_lang.yy"



/* END OF FILE */
