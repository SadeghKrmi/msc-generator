%option reentrant noyywrap nounput
%option bison-bridge bison-locations

%{
    /*
        This file is part of Msc-generator.
        Copyright (C) 2008-2022 Zoltan Turanyi
        Distributed under GNU Affero General Public License.

        Msc-generator is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        Msc-generator is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
        */

#include <stdio.h>
#include <string.h>
#include <iostream>

#ifndef HAVE_UNISTD_H
#define YY_NO_UNISTD_H
extern int isatty(int);
#endif

#include "cgen_shapes.h"
#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #include "blockcsh.h"
    #define YYMSC_RESULT_TYPE block::BlockCsh
    #define RESULT csh
    #define YYLTYPE_IS_DECLARED  //If we scan for color syntax highlight use this location type (YYLTYPE)
    #define YYLTYPE CshPos
    #define YYGET_EXTRA blockcsh_get_extra
    #define CHAR_IF_CSH(A) char
    #define FIRST_IF_CSH(A, B) A
    using namespace block;
    #include "block_csh_lang.h"   //Must be after language_misc.h
    #include "block_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
#else
    #define C_S_H (0)
    #include "blockchart.h"
    #define YYMSC_RESULT_TYPE block::BlockChart
    #define RESULT chart
    #define YYGET_EXTRA block_get_extra
    #define CHAR_IF_CSH(A) A
    #define FIRST_IF_CSH(A, B) B
    #include "parse_tools.h"
    using namespace block;
    #include "block_lang.h"
    #include "block_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined

    //this function must be in the lex file, so that lex replaces the "yy" in yy_create_buffer and
    //yypush_buffer_state to the appropriate chart-specific prefix.
    void BlockPushFlex(base_parse_parm &pp, const char *text, size_t len, YYLTYPE *old_pos, const FileLineCol &new_pos, EInclusionReason reason)
    {
        if (text==nullptr || len==0) return;
        pp.buffs.emplace_back(text, len);
        pp.pos_stack.line = old_pos->first_line;
        pp.pos_stack.col = old_pos->first_column-1; //not sure why -1 is needed
        pp.pos_stack.Push(new_pos);
        pp.pos_stack.reason = reason;
        yypush_buffer_state(yy_create_buffer( nullptr, YY_BUF_SIZE, pp.yyscanner ), pp.yyscanner);
        pp.chart->Progress.RegisterParse(len);
    }
#endif

%}

%%

 /* Newline characters in all forms accepted */
\x0d\x0a     %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

\x0a         %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

\x0d         %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

 /* # starts a comment last until end of line */
#[^\x0d\x0a]* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #endif
%}

 /* // starts a comment last until end of line */
\/\/[^\x0d\x0a]* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #endif
%}

 /* / * .. * / block comments*/
\/\*([^\*]*(\*[^\/])?)*\*\/ %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #else
    language_process_block_comments(yytext, yylloc);
  #endif
%}

 /* / * ... unclosed block comments */
\/\* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
    yyget_extra(yyscanner)->csh->AddCSH_Error(*yylloc, "Unpaired beginning of block comment '/" "*'.");
  #else
    yyget_extra(yyscanner)->chart->Error.Error(CHART_POS_START(*yylloc),
         "Unpaired beginning of block comment '/" "*'.");
  #endif
%}

[ \t]+    /* ignore whitespace */;

 /* These shape definition keywords are case sensitive */
M	yylval_param->shapecommand = ShapeElement::MOVE_TO; return TOK_SHAPE_COMMAND;
L	yylval_param->shapecommand = ShapeElement::LINE_TO; return TOK_SHAPE_COMMAND;
C	yylval_param->shapecommand = ShapeElement::CURVE_TO; return TOK_SHAPE_COMMAND;
E	yylval_param->shapecommand = ShapeElement::CLOSE_PATH; return TOK_SHAPE_COMMAND;
S	yylval_param->shapecommand = ShapeElement::SECTION_BG; return TOK_SHAPE_COMMAND;
T	yylval_param->shapecommand = ShapeElement::TEXT_AREA; return TOK_SHAPE_COMMAND;
H	yylval_param->shapecommand = ShapeElement::HINT_AREA; return TOK_SHAPE_COMMAND;
P	yylval_param->shapecommand = ShapeElement::PORT; return TOK_SHAPE_COMMAND;


 /* These keywords are case insensitive */
(?i:bye)       yylval_param->str = strdup(yytext); return TOK_BYE;
(?i:defshape)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFSHAPE;
(?i:defcolor)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFCOLOR;
(?i:defstyle)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFSTYLE;
(?i:defdesign) yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFDESIGN;
(?i:usedesign) yylval_param->str = strdup(yytext); return TOK_COMMAND_USEDESIGN;
(?i:use)       yylval_param->str = strdup(yytext); return TOK_COMMAND_USESTYLE;
(?:arrows)     yylval_param->str = strdup(yytext); return TOK_COMMAND_ARROWS;
(?:blocks)     yylval_param->str = strdup(yytext); return TOK_COMMAND_BLOCKS;
(?i:defproc)   yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFPROC;
(?i:replay)    yylval_param->str = strdup(yytext); return TOK_COMMAND_REPLAY;
(?i:set)       yylval_param->str = strdup(yytext); return TOK_COMMAND_SET; 
(?i:include)   yylval_param->str = strdup(yytext); return TOK_COMMAND_INCLUDE;
(?i:if)        yylval_param->str = strdup(yytext); return TOK_IF;
(?i:then)      yylval_param->str = strdup(yytext); return TOK_THEN;
(?i:else)      yylval_param->str = strdup(yytext); return TOK_ELSE;

(?i:box)      yylval_param->str = strdup(yytext); return TOK_BOX;
(?i:boxcol)   yylval_param->str = strdup(yytext); return TOK_BOXCOL;
(?i:col)      yylval_param->str = strdup(yytext); return TOK_COLUMN;
(?i:row)      yylval_param->str = strdup(yytext); return TOK_ROW;
(?i:cell)     yylval_param->str = strdup(yytext); return TOK_CELL;
(?i:text)     yylval_param->str = strdup(yytext); return TOK_TEXT;
(?i:shape)    yylval_param->str = strdup(yytext); return TOK_SHAPE;
(?i:space)    yylval_param->str = strdup(yytext); return TOK_SPACE_COMMAND;
(?i:break)    yylval_param->str = strdup(yytext); return TOK_BREAK_COMMAND;
(?i:multi)    yylval_param->str = strdup(yytext); return TOK_MULTI_COMMAND;
(?i:around)   yylval_param->str = strdup(yytext); return TOK_AROUND_COMMAND;
(?i:join)     yylval_param->str = strdup(yytext); return TOK_JOIN_COMMAND;
(?i:copy)     yylval_param->str = strdup(yytext); return TOK_COPY_COMMAND;
(?i:as)       yylval_param->str = strdup(yytext); return TOK_AS;
(?i:mark)     yylval_param->str = strdup(yytext); return TOK_MARK_COMMAND;
(?i:extend)   yylval_param->str = strdup(yytext); return TOK_EXTEND_COMMAND;
(?i:template) yylval_param->str = strdup(yytext); return TOK_TEMPLATE_COMMAND;

(?i:below)    yylval_param->alignmodifier = {true,  EDirection::Below}; return TOK_ALIGN_MODIFIER;
(?i:above)    yylval_param->alignmodifier = {true,  EDirection::Above}; return TOK_ALIGN_MODIFIER;
(?i:leftof)   yylval_param->alignmodifier = {true,  EDirection::Left};  return TOK_ALIGN_MODIFIER;
(?i:rightof)  yylval_param->alignmodifier = {true,  EDirection::Right}; return TOK_ALIGN_MODIFIER;
(?i:bottom)   yylval_param->alignmodifier = {false, EDirection::Below}; return TOK_ALIGN_MODIFIER;
(?i:top)      yylval_param->alignmodifier = {false, EDirection::Above}; return TOK_ALIGN_MODIFIER;
(?i:left)     yylval_param->alignmodifier = {false, EDirection::Left};  return TOK_ALIGN_MODIFIER;
(?i:right)    yylval_param->alignmodifier = {false, EDirection::Right}; return TOK_ALIGN_MODIFIER;

(?:add)       yylval_param->str = strdup(yytext); return TOK_CLONE_MODIFIER_ADD;
(?:move)      yylval_param->str = strdup(yytext); return TOK_CLONE_MODIFIER_MOVE;
(?:drop)      yylval_param->str = strdup(yytext); return TOK_CLONE_MODIFIER_DROP;
(?:update)    yylval_param->str = strdup(yytext); return TOK_CLONE_MODIFIER_UPDATE;
(?:replace)   yylval_param->str = strdup(yytext); return TOK_CLONE_MODIFIER_REPLACE;
(?:before)    yylval_param->str = strdup(yytext); return TOK_CLONE_MODIFIER_BEFORE;
(?:recursive) yylval_param->str = strdup(yytext); return TOK_CLONE_MODIFIER_RECURSIVE;

\%       return TOK_PERCENT;
\@       return TOK_ATSYMBOL;
\*       return TOK_ASTERISK;
=        return TOK_EQUAL;
,        return TOK_COMMA;
\;       return TOK_SEMICOLON;
\[       return TOK_OSBRACKET;
\]       return TOK_CSBRACKET;
\+\+     return TOK_PLUS_PLUS;
\+       return TOK_PLUS;
\-       return TOK_MINUS;
\~       return TOK_TILDE;
\(       return TOK_OPARENTHESIS;
\)       return TOK_CPARENTHESIS;
\{       %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->input_text_ptr = nullptr;
  #else
    yylval_param->input_text_ptr = yyget_extra(yyscanner)->buffs.back().GetInputPosFrom_yytext(yytext);
  #endif
    return TOK_OCBRACKET;
%}

\}       %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->input_text_ptr = nullptr;
  #else
    yylval_param->input_text_ptr = yyget_extra(yyscanner)->buffs.back().GetInputPosFrom_yytext(yytext);
  #endif
    return TOK_CCBRACKET;
%}

\<     yylval_param->compare_op = ECompareOperator::SMALLER;          return TOK_COMP_OP;
\<=    yylval_param->compare_op = ECompareOperator::SMALLER_OR_EQUAL; return TOK_COMP_OP;
==     yylval_param->compare_op = ECompareOperator::EQUAL;            return TOK_COMP_OP;
\<\>   yylval_param->compare_op = ECompareOperator::NOT_EQUAL;        return TOK_COMP_OP;
=\>    yylval_param->compare_op = ECompareOperator::GREATER_OR_EQUAL; return TOK_COMP_OP;
\>     yylval_param->compare_op = ECompareOperator::GREATER;          return TOK_COMP_OP;


-\>      yylval_param->arrowstyle = EArrowStyle::SOLID;  return TOK_ARROW_FW;     // ->
\<-      yylval_param->arrowstyle = EArrowStyle::SOLID;  return TOK_ARROW_BW;   // <-
\<-\>    yylval_param->arrowstyle = EArrowStyle::SOLID;  return TOK_ARROW_BIDIR;  // <->
--       yylval_param->arrowstyle = EArrowStyle::SOLID;  return TOK_ARROW_NO;  // --
\<=\>    yylval_param->arrowstyle = EArrowStyle::DOUBLE; return TOK_ARROW_BIDIR;  // <=>
\>\>     yylval_param->arrowstyle = EArrowStyle::DASHED; return TOK_ARROW_FW;     // >>
\<\<     yylval_param->arrowstyle = EArrowStyle::DASHED; return TOK_ARROW_BW;   // <<
\<\<\>\> yylval_param->arrowstyle = EArrowStyle::DASHED; return TOK_ARROW_BIDIR;  // <<>>
\.\.     yylval_param->arrowstyle = EArrowStyle::DOTTED; return TOK_ARROW_NO;  // ..



 /* This is a colon-quoted string, finished by a quotation mark (UTF-8 allowed)
 ** : "<string>"
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*\" %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    {
    /* after whitespaces we are guaranteed to have a tailing and heading quot */
    char *s = remove_head_tail_whitespace(yytext+1);
    /* s now points to the heading quotation marks.
    ** Now get rid of both quotation marks */
    std::string str(s+1);
    str.erase(str.length()-1);
    /* Calculate the position of the string and prepend a location escape */
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s+1 - yytext));
    yylval_param->str = strdup((pos.Print() + str).c_str());
    }
  #endif
    return TOK_COLON_QUOTED_STRING;
%}

 /* This is a colon-quoted string, finished by a newline (trailing context) (UTF-8 allowed)
 ** : "<string>$
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*  %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
  #else
    {
    /* after whitespaces we are guaranteed to have a heading quot */
    const char *s = remove_head_tail_whitespace(yytext+1);
    // s now points to heading quotation mark
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s - yytext));
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    /* Advance pos beyond the leading quotation mark */
    pos.col++;
    yylval_param->str = strdup((pos.Print() + (s+1)).c_str());
    }
  #endif
    return TOK_COLON_QUOTED_STRING;
%}

 /* This is a non quoted colon-string
 ** : <string>
 ** terminated by any of: [ { or ;
 ** Honors escaping of the above via a backslash. (UTF-8 allowed)
 ** Can contain quotation marks (escaped or unescaped), but can not start with it
 ** If it contains a hashmark, unescaped [ { or ; is allowed till the end of the line
 ** (representing a commented section inside a label)
 ** Not available in mscgen compatibility mode. There we use the one below
 *  \:[\t]*(((#[^\x0d\x0a]*)|[^\"\;\[\{\\]|(\\.))((#[^\x0d\x0a]*)|[^\;\[\{\\]|(\\.))*(\\)?|\\)
 * \:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*
 */
<INITIAL>\:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc);
  #endif
    return TOK_COLON_STRING;
%}

 /* This is a degenerate non quoted colon-string
 ** a colon followed by a solo escape or just a colon
 */
\:[ \t]*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
   #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc);
  #endif
    return TOK_COLON_STRING;
%}


 /* A simple quoted string, that can have escaped quotation marks inside. (UTF-8 allowed)*/
\"([^\"\x0d\x0a]|(\\\"))*\" %{
    yylval_param->str = strdup(yytext+1);
    yylval_param->str[strlen(yylval_param->str) - 1] = '\0';
    return TOK_QSTRING;
%}

 /* A simple quoted string, missing a closing quotation mark (UTF-8 allowed)*/
\"([^\"\x0d\x0a]|(\\\"))* %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext+1);
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
#else
    {
    yylval_param->str = strdup(yytext+1);
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column);
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    }
  #endif
    return TOK_QSTRING;
%}

 /* Numbers */
[+\-]?[0-9]+\.?[0-9]*  %{
    yylval_param->str = strdup(yytext);
    return TOK_NUMBER;
%}

 /* Strings not ending with a dot. We allow any non ASCII UTF-8 character through. */
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])* %{
    yylval_param->str = strdup(yytext);
    return TOK_STRING;
%}

 /* Strings ending with a dot, not followed by a second dot .
  * We allow any non ASCII UTF-8 character through.
  * Two dots one after another shall be parsed a '..' into TOK_EMPH*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*\./[^\.] %{
    yylval_param->str = strdup(yytext);
    return TOK_STRING;
%}

 /* Parameter names: $ followed by a string not ending with a dot. We allow any non ASCII UTF-8 character through.
  * A single '$" also matches.*/
\$[A-Za-z_\x80-\xff]?([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])* %{
    yylval_param->str = strdup(yytext);
    return TOK_PARAM_NAME;
%}

 /* Parameter names: $$ representing a value unique to the actual invocation.*/
($$) %{
    yylval_param->str = strdup(yytext);
    return TOK_PARAM_NAME;
%}

<<EOF>> %{
  #ifdef C_S_H_IS_COMPILED
    return TOK_EOF;
  #else
    {
    auto &pp = *YYGET_EXTRA(yyscanner);
    if (pp.buffs.size()==0) {
        yyterminate();
        return TOK_EOF;
    }
    pp.buffs.pop_back();
    if (pp.buffs.size()==0) {
        yyterminate();
        return TOK_EOF;
    }
    //else just continue scanning the buffer above
    yypop_buffer_state(yyscanner);
    pp.pos_stack.Pop();
    yylloc->last_line = unsigned(pp.pos_stack.line);
    yylloc->last_column = unsigned(pp.pos_stack.col-1);
    }
  #endif
%}

 /* Ignore (terminating) zero */
\0

. return TOK_UNRECOGNIZED_CHAR;

%%

/* END OF FILE */
