#define COLOR_SYNTAX_HIGHLIGHT 1 /* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         blockcsh_parse
#define yylex           blockcsh_lex
#define yyerror         blockcsh_error
#define yydebug         blockcsh_debug
#define yynerrs         blockcsh_nerrs

/* First part of user prologue.  */
#line 15 "block_lang.yy"

/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT //so that subsequent definition in block_csh_lang2.h causes no warning
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #define YYMSC_RESULT_TYPE BlockCsh
    #define RESULT csh
    #include "cgen_shapes.h"
    #include "blockcsh.h"
    #include "blockchart.h"
    #define YYGET_EXTRA blockcsh_get_extra
    #define YYLTYPE_IS_DECLARED
    #define YYLTYPE CshPos
    #define CHAR_IF_CSH(A) char
    #define FIRST_IF_CSH(A, B) A
#else
    #define C_S_H (0)
    #define YYMSC_RESULT_TYPE BlockChart
    #define RESULT chart
    #define YYGET_EXTRA block_get_extra
    #define CHAR_IF_CSH(A) A
    #define FIRST_IF_CSH(A, B) B
    #include "cgen_shapes.h"
    #include "blockchart.h"
#endif

using namespace block;


#line 125 "block_csh_lang.cc"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "block_csh_lang.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* TOK_EOF  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_TOK_STRING = 3,                 /* TOK_STRING  */
  YYSYMBOL_TOK_QSTRING = 4,                /* TOK_QSTRING  */
  YYSYMBOL_TOK_NUMBER = 5,                 /* TOK_NUMBER  */
  YYSYMBOL_TOK_DASH = 6,                   /* TOK_DASH  */
  YYSYMBOL_TOK_EQUAL = 7,                  /* TOK_EQUAL  */
  YYSYMBOL_TOK_COMMA = 8,                  /* TOK_COMMA  */
  YYSYMBOL_TOK_SEMICOLON = 9,              /* TOK_SEMICOLON  */
  YYSYMBOL_TOK_PLUS_PLUS = 10,             /* TOK_PLUS_PLUS  */
  YYSYMBOL_TOK_PLUS = 11,                  /* TOK_PLUS  */
  YYSYMBOL_TOK_MINUS = 12,                 /* TOK_MINUS  */
  YYSYMBOL_TOK_ATSYMBOL = 13,              /* TOK_ATSYMBOL  */
  YYSYMBOL_TOK_ASTERISK = 14,              /* TOK_ASTERISK  */
  YYSYMBOL_TOK_PERCENT = 15,               /* TOK_PERCENT  */
  YYSYMBOL_TOK_OCBRACKET = 16,             /* TOK_OCBRACKET  */
  YYSYMBOL_TOK_CCBRACKET = 17,             /* TOK_CCBRACKET  */
  YYSYMBOL_TOK_OSBRACKET = 18,             /* TOK_OSBRACKET  */
  YYSYMBOL_TOK_CSBRACKET = 19,             /* TOK_CSBRACKET  */
  YYSYMBOL_TOK_BOX = 20,                   /* TOK_BOX  */
  YYSYMBOL_TOK_BOXCOL = 21,                /* TOK_BOXCOL  */
  YYSYMBOL_TOK_ROW = 22,                   /* TOK_ROW  */
  YYSYMBOL_TOK_COLUMN = 23,                /* TOK_COLUMN  */
  YYSYMBOL_TOK_TEXT = 24,                  /* TOK_TEXT  */
  YYSYMBOL_TOK_SHAPE = 25,                 /* TOK_SHAPE  */
  YYSYMBOL_TOK_CELL = 26,                  /* TOK_CELL  */
  YYSYMBOL_TOK_SHAPE_COMMAND = 27,         /* TOK_SHAPE_COMMAND  */
  YYSYMBOL_TOK_ALIGN_MODIFIER = 28,        /* TOK_ALIGN_MODIFIER  */
  YYSYMBOL_TOK_BREAK_COMMAND = 29,         /* TOK_BREAK_COMMAND  */
  YYSYMBOL_TOK_SPACE_COMMAND = 30,         /* TOK_SPACE_COMMAND  */
  YYSYMBOL_TOK_MULTI_COMMAND = 31,         /* TOK_MULTI_COMMAND  */
  YYSYMBOL_TOK_AROUND_COMMAND = 32,        /* TOK_AROUND_COMMAND  */
  YYSYMBOL_TOK_JOIN_COMMAND = 33,          /* TOK_JOIN_COMMAND  */
  YYSYMBOL_TOK_COPY_COMMAND = 34,          /* TOK_COPY_COMMAND  */
  YYSYMBOL_TOK_AS = 35,                    /* TOK_AS  */
  YYSYMBOL_TOK_MARK_COMMAND = 36,          /* TOK_MARK_COMMAND  */
  YYSYMBOL_TOK_EXTEND_COMMAND = 37,        /* TOK_EXTEND_COMMAND  */
  YYSYMBOL_TOK_TEMPLATE_COMMAND = 38,      /* TOK_TEMPLATE_COMMAND  */
  YYSYMBOL_TOK_COMP_OP = 39,               /* TOK_COMP_OP  */
  YYSYMBOL_TOK_COLON_STRING = 40,          /* TOK_COLON_STRING  */
  YYSYMBOL_TOK_COLON_QUOTED_STRING = 41,   /* TOK_COLON_QUOTED_STRING  */
  YYSYMBOL_TOK_COMMAND_DEFSHAPE = 42,      /* TOK_COMMAND_DEFSHAPE  */
  YYSYMBOL_TOK_COMMAND_DEFCOLOR = 43,      /* TOK_COMMAND_DEFCOLOR  */
  YYSYMBOL_TOK_COMMAND_DEFSTYLE = 44,      /* TOK_COMMAND_DEFSTYLE  */
  YYSYMBOL_TOK_COMMAND_DEFDESIGN = 45,     /* TOK_COMMAND_DEFDESIGN  */
  YYSYMBOL_TOK_COMMAND_USEDESIGN = 46,     /* TOK_COMMAND_USEDESIGN  */
  YYSYMBOL_TOK_COMMAND_USESTYLE = 47,      /* TOK_COMMAND_USESTYLE  */
  YYSYMBOL_TOK_COMMAND_ARROWS = 48,        /* TOK_COMMAND_ARROWS  */
  YYSYMBOL_TOK_COMMAND_BLOCKS = 49,        /* TOK_COMMAND_BLOCKS  */
  YYSYMBOL_TOK_CLONE_MODIFIER_RECURSIVE = 50, /* TOK_CLONE_MODIFIER_RECURSIVE  */
  YYSYMBOL_TOK_CLONE_MODIFIER_ADD = 51,    /* TOK_CLONE_MODIFIER_ADD  */
  YYSYMBOL_TOK_CLONE_MODIFIER_MOVE = 52,   /* TOK_CLONE_MODIFIER_MOVE  */
  YYSYMBOL_TOK_CLONE_MODIFIER_DROP = 53,   /* TOK_CLONE_MODIFIER_DROP  */
  YYSYMBOL_TOK_CLONE_MODIFIER_UPDATE = 54, /* TOK_CLONE_MODIFIER_UPDATE  */
  YYSYMBOL_TOK_CLONE_MODIFIER_REPLACE = 55, /* TOK_CLONE_MODIFIER_REPLACE  */
  YYSYMBOL_TOK_CLONE_MODIFIER_BEFORE = 56, /* TOK_CLONE_MODIFIER_BEFORE  */
  YYSYMBOL_TOK_TILDE = 57,                 /* TOK_TILDE  */
  YYSYMBOL_TOK_PARAM_NAME = 58,            /* TOK_PARAM_NAME  */
  YYSYMBOL_TOK_OPARENTHESIS = 59,          /* TOK_OPARENTHESIS  */
  YYSYMBOL_TOK_CPARENTHESIS = 60,          /* TOK_CPARENTHESIS  */
  YYSYMBOL_TOK_COMMAND_DEFPROC = 61,       /* TOK_COMMAND_DEFPROC  */
  YYSYMBOL_TOK_COMMAND_REPLAY = 62,        /* TOK_COMMAND_REPLAY  */
  YYSYMBOL_TOK_COMMAND_SET = 63,           /* TOK_COMMAND_SET  */
  YYSYMBOL_TOK_BYE = 64,                   /* TOK_BYE  */
  YYSYMBOL_TOK_IF = 65,                    /* TOK_IF  */
  YYSYMBOL_TOK_THEN = 66,                  /* TOK_THEN  */
  YYSYMBOL_TOK_ELSE = 67,                  /* TOK_ELSE  */
  YYSYMBOL_TOK_COMMAND_INCLUDE = 68,       /* TOK_COMMAND_INCLUDE  */
  YYSYMBOL_TOK_UNRECOGNIZED_CHAR = 69,     /* TOK_UNRECOGNIZED_CHAR  */
  YYSYMBOL_TOK__NEVER__HAPPENS = 70,       /* TOK__NEVER__HAPPENS  */
  YYSYMBOL_TOK_ARROW_FW = 71,              /* TOK_ARROW_FW  */
  YYSYMBOL_TOK_ARROW_BW = 72,              /* TOK_ARROW_BW  */
  YYSYMBOL_TOK_ARROW_BIDIR = 73,           /* TOK_ARROW_BIDIR  */
  YYSYMBOL_TOK_ARROW_NO = 74,              /* TOK_ARROW_NO  */
  YYSYMBOL_YYACCEPT = 75,                  /* $accept  */
  YYSYMBOL_chart_with_bye = 76,            /* chart_with_bye  */
  YYSYMBOL_eof = 77,                       /* eof  */
  YYSYMBOL_chart = 78,                     /* chart  */
  YYSYMBOL_top_level_instrlist = 79,       /* top_level_instrlist  */
  YYSYMBOL_braced_instrlist = 80,          /* braced_instrlist  */
  YYSYMBOL_instrlist = 81,                 /* instrlist  */
  YYSYMBOL_multi_instr = 82,               /* multi_instr  */
  YYSYMBOL_multi_instr_req_semicolon = 83, /* multi_instr_req_semicolon  */
  YYSYMBOL_complete_instr = 84,            /* complete_instr  */
  YYSYMBOL_include = 85,                   /* include  */
  YYSYMBOL_instr_req_semicolon = 86,       /* instr_req_semicolon  */
  YYSYMBOL_instr_ending_brace = 87,        /* instr_ending_brace  */
  YYSYMBOL_braced_clone_action_list = 88,  /* braced_clone_action_list  */
  YYSYMBOL_clone_action_list = 89,         /* clone_action_list  */
  YYSYMBOL_clone_action = 90,              /* clone_action  */
  YYSYMBOL_thing_to_update = 91,           /* thing_to_update  */
  YYSYMBOL_things_to_update = 92,          /* things_to_update  */
  YYSYMBOL_opt_things_to_update = 93,      /* opt_things_to_update  */
  YYSYMBOL_clone_action_req_semicolon = 94, /* clone_action_req_semicolon  */
  YYSYMBOL_clone_modifier_replace_with_name = 95, /* clone_modifier_replace_with_name  */
  YYSYMBOL_clone_action_no_semicolon = 96, /* clone_action_no_semicolon  */
  YYSYMBOL_opt_before = 97,                /* opt_before  */
  YYSYMBOL_total_full_block_def = 98,      /* total_full_block_def  */
  YYSYMBOL_full_block_def_enclose = 99,    /* full_block_def_enclose  */
  YYSYMBOL_alignment_modifiers = 100,      /* alignment_modifiers  */
  YYSYMBOL_full_block_def_with_pre = 101,  /* full_block_def_with_pre  */
  YYSYMBOL_multi_command = 102,            /* multi_command  */
  YYSYMBOL_full_block_def = 103,           /* full_block_def  */
  YYSYMBOL_block_def = 104,                /* block_def  */
  YYSYMBOL_block_keyword = 105,            /* block_keyword  */
  YYSYMBOL_shape_block_header = 106,       /* shape_block_header  */
  YYSYMBOL_blocknameposlist = 107,         /* blocknameposlist  */
  YYSYMBOL_command = 108,                  /* command  */
  YYSYMBOL_copy_header = 109,              /* copy_header  */
  YYSYMBOL_copy_header_resolved = 110,     /* copy_header_resolved  */
  YYSYMBOL_copy_header_with_pre = 111,     /* copy_header_with_pre  */
  YYSYMBOL_copy_attr = 112,                /* copy_attr  */
  YYSYMBOL_arrowsymbol = 113,              /* arrowsymbol  */
  YYSYMBOL_coord_hinted_with_dir = 114,    /* coord_hinted_with_dir  */
  YYSYMBOL_coord_hinted_with_dir_and_dist = 115, /* coord_hinted_with_dir_and_dist  */
  YYSYMBOL_arrow_end = 116,                /* arrow_end  */
  YYSYMBOL_tok_plus_or_minus = 117,        /* tok_plus_or_minus  */
  YYSYMBOL_arrow_end_block_port_compass_distance = 118, /* arrow_end_block_port_compass_distance  */
  YYSYMBOL_distance_for_arrow_end = 119,   /* distance_for_arrow_end  */
  YYSYMBOL_arrow_end_block_port_compass = 120, /* arrow_end_block_port_compass  */
  YYSYMBOL_arrowend_list = 121,            /* arrowend_list  */
  YYSYMBOL_arrowend_list_solo = 122,       /* arrowend_list_solo  */
  YYSYMBOL_arrow_needs_semi = 123,         /* arrow_needs_semi  */
  YYSYMBOL_arrow_no_semi = 124,            /* arrow_no_semi  */
  YYSYMBOL_arrow_cont = 125,               /* arrow_cont  */
  YYSYMBOL_full_attrlist_with_arrow_labels = 126, /* full_attrlist_with_arrow_labels  */
  YYSYMBOL_opt_percent = 127,              /* opt_percent  */
  YYSYMBOL_arrow_label_number = 128,       /* arrow_label_number  */
  YYSYMBOL_arrow_label_number_with_extend = 129, /* arrow_label_number_with_extend  */
  YYSYMBOL_arrow_label = 130,              /* arrow_label  */
  YYSYMBOL_optlist = 131,                  /* optlist  */
  YYSYMBOL_opt = 132,                      /* opt  */
  YYSYMBOL_usestylemodifier = 133,         /* usestylemodifier  */
  YYSYMBOL_usestylemode = 134,             /* usestylemode  */
  YYSYMBOL_usestyle = 135,                 /* usestyle  */
  YYSYMBOL_styledeflist = 136,             /* styledeflist  */
  YYSYMBOL_styledef = 137,                 /* styledef  */
  YYSYMBOL_stylenameposlist = 138,         /* stylenameposlist  */
  YYSYMBOL_shapedef = 139,                 /* shapedef  */
  YYSYMBOL_shapedeflist = 140,             /* shapedeflist  */
  YYSYMBOL_shapeline = 141,                /* shapeline  */
  YYSYMBOL_colordeflist_actioned = 142,    /* colordeflist_actioned  */
  YYSYMBOL_colordeflist = 143,             /* colordeflist  */
  YYSYMBOL_string_or_num = 144,            /* string_or_num  */
  YYSYMBOL_colordef = 145,                 /* colordef  */
  YYSYMBOL_usedesign = 146,                /* usedesign  */
  YYSYMBOL_designdef = 147,                /* designdef  */
  YYSYMBOL_scope_open_empty = 148,         /* scope_open_empty  */
  YYSYMBOL_designelementlist = 149,        /* designelementlist  */
  YYSYMBOL_designelement = 150,            /* designelement  */
  YYSYMBOL_designoptlist = 151,            /* designoptlist  */
  YYSYMBOL_designopt = 152,                /* designopt  */
  YYSYMBOL_defproc = 153,                  /* defproc  */
  YYSYMBOL_defprochelp1 = 154,             /* defprochelp1  */
  YYSYMBOL_defprochelp2 = 155,             /* defprochelp2  */
  YYSYMBOL_defprochelp3 = 156,             /* defprochelp3  */
  YYSYMBOL_defprochelp4 = 157,             /* defprochelp4  */
  YYSYMBOL_scope_open_proc_body = 158,     /* scope_open_proc_body  */
  YYSYMBOL_scope_close_proc_body = 159,    /* scope_close_proc_body  */
  YYSYMBOL_proc_def_arglist_tested = 160,  /* proc_def_arglist_tested  */
  YYSYMBOL_proc_def_arglist = 161,         /* proc_def_arglist  */
  YYSYMBOL_proc_def_param_list = 162,      /* proc_def_param_list  */
  YYSYMBOL_proc_def_param = 163,           /* proc_def_param  */
  YYSYMBOL_procedure_body = 164,           /* procedure_body  */
  YYSYMBOL_set = 165,                      /* set  */
  YYSYMBOL_proc_invocation = 166,          /* proc_invocation  */
  YYSYMBOL_proc_param_list = 167,          /* proc_param_list  */
  YYSYMBOL_proc_invoc_param_list = 168,    /* proc_invoc_param_list  */
  YYSYMBOL_proc_invoc_param = 169,         /* proc_invoc_param  */
  YYSYMBOL_ifthenbranch = 170,             /* ifthenbranch  */
  YYSYMBOL_comp = 171,                     /* comp  */
  YYSYMBOL_condition = 172,                /* condition  */
  YYSYMBOL_ifthen_condition = 173,         /* ifthen_condition  */
  YYSYMBOL_else = 174,                     /* else  */
  YYSYMBOL_ifthen = 175,                   /* ifthen  */
  YYSYMBOL_colon_string = 176,             /* colon_string  */
  YYSYMBOL_full_attrlist_with_label = 177, /* full_attrlist_with_label  */
  YYSYMBOL_full_attrlist = 178,            /* full_attrlist  */
  YYSYMBOL_attrlist = 179,                 /* attrlist  */
  YYSYMBOL_attr = 180,                     /* attr  */
  YYSYMBOL_attrvalue = 181,                /* attrvalue  */
  YYSYMBOL_attrvalue_simple = 182,         /* attrvalue_simple  */
  YYSYMBOL_alignment_attrvalue = 183,      /* alignment_attrvalue  */
  YYSYMBOL_edgepos = 184,                  /* edgepos  */
  YYSYMBOL_alpha_string_or_percent = 185,  /* alpha_string_or_percent  */
  YYSYMBOL_blocknames_plus_number_multi = 186, /* blocknames_plus_number_multi  */
  YYSYMBOL_blocknames_plus_number_or_number = 187, /* blocknames_plus_number_or_number  */
  YYSYMBOL_blocknames_plus_number = 188,   /* blocknames_plus_number  */
  YYSYMBOL_blocknames_plus = 189,          /* blocknames_plus  */
  YYSYMBOL_coord_hinted = 190,             /* coord_hinted  */
  YYSYMBOL_coord = 191,                    /* coord  */
  YYSYMBOL_reserved_word_string = 192,     /* reserved_word_string  */
  YYSYMBOL_symbol_string = 193,            /* symbol_string  */
  YYSYMBOL_entity_string_single = 194,     /* entity_string_single  */
  YYSYMBOL_alpha_string_single = 195,      /* alpha_string_single  */
  YYSYMBOL_string_single = 196,            /* string_single  */
  YYSYMBOL_tok_param_name_as_multi = 197,  /* tok_param_name_as_multi  */
  YYSYMBOL_multi_string_continuation = 198, /* multi_string_continuation  */
  YYSYMBOL_entity_string_single_or_param = 199, /* entity_string_single_or_param  */
  YYSYMBOL_entity_string = 200,            /* entity_string  */
  YYSYMBOL_alpha_string = 201,             /* alpha_string  */
  YYSYMBOL_string = 202,                   /* string  */
  YYSYMBOL_scope_open = 203,               /* scope_open  */
  YYSYMBOL_scope_close = 204               /* scope_close  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;


/* Second part of user prologue.  */
#line 232 "block_lang.yy"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <iostream>

#ifdef C_S_H_IS_COMPILED
    #include "block_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "block_csh_lang2.h"  //Needs parse_param from block_lang_misc.h
    /* yyerror
     *  Error handling function.  Do nothing for CSH */
    void yyerror(YYLTYPE* /*loc*/, Csh & /*csh*/, void * /*yyscanner*/, const char * /*str*/) {}
#else
    #include "block_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "block_lang2.h"      //Needs parse_param from block_lang_misc.h
    /* Use verbose error reporting such that the expected token names are dumped */
    //#define YYERROR_VERBOSE
    void yyerror(YYLTYPE *loc, YYMSC_RESULT_TYPE &chart, void *yyscanner, const char *str)
    {
        chart.Error.Error(CHART_POS_START(*loc), str);
    }
#endif

#ifdef C_S_H_IS_COMPILED
void BlockCshParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#else
void BlockParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, size_t len)
#endif
{
    std::remove_pointer<YY_EXTRA_TYPE>::type pp;
    pp.RESULT = &RESULT;
#ifdef C_S_H_IS_COMPILED
    pp.buff.buf = buff;
    pp.buff.length = len;
    pp.buff.pos = 0;
    blockcsh_lex_init(&pp.yyscanner);
    blockcsh_set_extra(&pp, pp.yyscanner);
    blockcsh_parse(RESULT, pp.yyscanner);
    blockcsh_lex_destroy(pp.yyscanner);
#else
    pp.buffs.emplace_back(buff, len);
    pp.pos_stack.file = RESULT.current_file;
    pp.pos_stack.col = 1; //start from column 1.
    block_lex_init(&pp.yyscanner);
    block_set_extra(&pp, pp.yyscanner);
    block_parse(RESULT, pp.yyscanner);
    block_lex_destroy(pp.yyscanner);
#endif
}


#line 416 "block_csh_lang.cc"


#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  216
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3479

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  75
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  130
/* YYNRULES -- Number of rules.  */
#define YYNRULES  533
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  584

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   329


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   287,   287,   291,   301,   302,   311,   323,   334,   349,
     350,   360,   375,   387,   399,   414,   426,   439,   450,   464,
     478,   484,   490,   491,   504,   514,   527,   543,   551,   559,
     568,   604,   635,   652,   672,   673,   689,   707,   724,   737,
     758,   771,   782,   796,   809,   826,   843,   867,   886,   905,
     931,   951,   965,   981,  1003,  1016,  1031,  1047,  1065,  1071,
    1081,  1082,  1089,  1112,  1123,  1140,  1154,  1158,  1179,  1197,
    1216,  1234,  1252,  1273,  1291,  1313,  1319,  1334,  1353,  1368,
    1381,  1399,  1417,  1436,  1452,  1472,  1490,  1502,  1515,  1523,
    1532,  1543,  1554,  1571,  1578,  1586,  1601,  1615,  1634,  1634,
    1637,  1652,  1660,  1677,  1695,  1719,  1741,  1765,  1786,  1806,
    1831,  1849,  1863,  1882,  1901,  1918,  1935,  1946,  1960,  1963,
    1974,  1989,  2006,  2006,  2008,  2024,  2045,  2082,  2101,  2113,
    2132,  2152,  2165,  2189,  2206,  2230,  2248,  2249,  2259,  2278,
    2291,  2315,  2343,  2344,  2357,  2368,  2383,  2403,  2431,  2458,
    2496,  2511,  2534,  2557,  2589,  2603,  2619,  2647,  2648,  2649,
    2650,  2651,  2652,  2655,  2670,  2699,  2713,  2742,  2755,  2764,
    2782,  2796,  2811,  2834,  2853,  2881,  2902,  2936,  2973,  3016,
    3025,  3026,  3035,  3036,  3056,  3057,  3058,  3059,  3060,  3061,
    3074,  3075,  3089,  3100,  3112,  3113,  3123,  3142,  3143,  3147,
    3159,  3159,  3161,  3162,  3166,  3175,  3186,  3198,  3210,  3234,
    3261,  3289,  3308,  3327,  3351,  3373,  3397,  3417,  3443,  3463,
    3493,  3515,  3540,  3554,  3577,  3578,  3604,  3619,  3639,  3658,
    3681,  3708,  3738,  3771,  3791,  3818,  3840,  3868,  3891,  3891,
    3894,  3908,  3920,  3923,  3928,  3940,  3954,  3968,  3983,  3998,
    4012,  4013,  4024,  4036,  4051,  4070,  4081,  4104,  4113,  4128,
    4141,  4157,  4198,  4224,  4232,  4240,  4250,  4262,  4270,  4282,
    4296,  4310,  4327,  4346,  4347,  4364,  4377,  4398,  4410,  4427,
    4440,  4462,  4473,  4487,  4510,  4532,  4556,  4574,  4593,  4612,
    4621,  4638,  4667,  4692,  4714,  4737,  4767,  4796,  4827,  4865,
    4883,  4884,  4918,  4933,  4934,  4939,  4965,  4992,  5018,  5034,
    5044,  5067,  5090,  5107,  5147,  5190,  5202,  5203,  5214,  5228,
    5245,  5261,  5278,  5279,  5281,  5282,  5292,  5302,  5309,  5330,
    5353,  5378,  5417,  5436,  5454,  5466,  5477,  5487,  5496,  5504,
    5513,  5528,  5544,  5555,  5571,  5582,  5606,  5615,  5626,  5636,
    5648,  5659,  5669,  5680,  5691,  5707,  5725,  5744,  5766,  5788,
    5802,  5815,  5833,  5852,  5871,  5887,  5907,  5925,  5939,  5951,
    5962,  5990,  5999,  6010,  6020,  6032,  6043,  6053,  6064,  6073,
    6086,  6096,  6112,  6130,  6144,  6145,  6157,  6159,  6167,  6180,
    6203,  6217,  6245,  6266,  6280,  6294,  6315,  6348,  6369,  6383,
    6395,  6409,  6425,  6439,  6468,  6484,  6505,  6513,  6522,  6531,
    6540,  6554,  6563,  6566,  6578,  6590,  6603,  6617,  6630,  6643,
    6656,  6670,  6683,  6706,  6720,  6759,  6781,  6803,  6821,  6837,
    6853,  6854,  6870,  6871,  6873,  6877,  6881,  6886,  6887,  6897,
    6906,  6913,  6920,  6930,  6937,  6949,  6950,  6960,  6961,  6962,
    6966,  6972,  6999,  7000,  7014,  7037,  7064,  7094,  7121,  7144,
    7171,  7195,  7207,  7222,  7237,  7252,  7264,  7278,  7291,  7306,
    7324,  7339,  7356,  7381,  7381,  7381,  7381,  7381,  7381,  7381,
    7382,  7382,  7382,  7382,  7383,  7383,  7383,  7384,  7384,  7385,
    7385,  7385,  7386,  7386,  7386,  7387,  7387,  7387,  7388,  7388,
    7388,  7388,  7389,  7389,  7389,  7390,  7390,  7390,  7390,  7391,
    7391,  7391,  7392,  7401,  7441,  7442,  7449,  7456,  7458,  7461,
    7494,  7506,  7511,  7515,  7517,  7521,  7526,  7527,  7531,  7536,
    7537,  7541,  7547,  7582
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "TOK_EOF", "error", "\"invalid token\"", "TOK_STRING", "TOK_QSTRING",
  "TOK_NUMBER", "TOK_DASH", "TOK_EQUAL", "TOK_COMMA", "TOK_SEMICOLON",
  "TOK_PLUS_PLUS", "TOK_PLUS", "TOK_MINUS", "TOK_ATSYMBOL", "TOK_ASTERISK",
  "TOK_PERCENT", "TOK_OCBRACKET", "TOK_CCBRACKET", "TOK_OSBRACKET",
  "TOK_CSBRACKET", "TOK_BOX", "TOK_BOXCOL", "TOK_ROW", "TOK_COLUMN",
  "TOK_TEXT", "TOK_SHAPE", "TOK_CELL", "TOK_SHAPE_COMMAND",
  "TOK_ALIGN_MODIFIER", "TOK_BREAK_COMMAND", "TOK_SPACE_COMMAND",
  "TOK_MULTI_COMMAND", "TOK_AROUND_COMMAND", "TOK_JOIN_COMMAND",
  "TOK_COPY_COMMAND", "TOK_AS", "TOK_MARK_COMMAND", "TOK_EXTEND_COMMAND",
  "TOK_TEMPLATE_COMMAND", "TOK_COMP_OP", "TOK_COLON_STRING",
  "TOK_COLON_QUOTED_STRING", "TOK_COMMAND_DEFSHAPE",
  "TOK_COMMAND_DEFCOLOR", "TOK_COMMAND_DEFSTYLE", "TOK_COMMAND_DEFDESIGN",
  "TOK_COMMAND_USEDESIGN", "TOK_COMMAND_USESTYLE", "TOK_COMMAND_ARROWS",
  "TOK_COMMAND_BLOCKS", "TOK_CLONE_MODIFIER_RECURSIVE",
  "TOK_CLONE_MODIFIER_ADD", "TOK_CLONE_MODIFIER_MOVE",
  "TOK_CLONE_MODIFIER_DROP", "TOK_CLONE_MODIFIER_UPDATE",
  "TOK_CLONE_MODIFIER_REPLACE", "TOK_CLONE_MODIFIER_BEFORE", "TOK_TILDE",
  "TOK_PARAM_NAME", "TOK_OPARENTHESIS", "TOK_CPARENTHESIS",
  "TOK_COMMAND_DEFPROC", "TOK_COMMAND_REPLAY", "TOK_COMMAND_SET",
  "TOK_BYE", "TOK_IF", "TOK_THEN", "TOK_ELSE", "TOK_COMMAND_INCLUDE",
  "TOK_UNRECOGNIZED_CHAR", "TOK__NEVER__HAPPENS", "TOK_ARROW_FW",
  "TOK_ARROW_BW", "TOK_ARROW_BIDIR", "TOK_ARROW_NO", "$accept",
  "chart_with_bye", "eof", "chart", "top_level_instrlist",
  "braced_instrlist", "instrlist", "multi_instr",
  "multi_instr_req_semicolon", "complete_instr", "include",
  "instr_req_semicolon", "instr_ending_brace", "braced_clone_action_list",
  "clone_action_list", "clone_action", "thing_to_update",
  "things_to_update", "opt_things_to_update", "clone_action_req_semicolon",
  "clone_modifier_replace_with_name", "clone_action_no_semicolon",
  "opt_before", "total_full_block_def", "full_block_def_enclose",
  "alignment_modifiers", "full_block_def_with_pre", "multi_command",
  "full_block_def", "block_def", "block_keyword", "shape_block_header",
  "blocknameposlist", "command", "copy_header", "copy_header_resolved",
  "copy_header_with_pre", "copy_attr", "arrowsymbol",
  "coord_hinted_with_dir", "coord_hinted_with_dir_and_dist", "arrow_end",
  "tok_plus_or_minus", "arrow_end_block_port_compass_distance",
  "distance_for_arrow_end", "arrow_end_block_port_compass",
  "arrowend_list", "arrowend_list_solo", "arrow_needs_semi",
  "arrow_no_semi", "arrow_cont", "full_attrlist_with_arrow_labels",
  "opt_percent", "arrow_label_number", "arrow_label_number_with_extend",
  "arrow_label", "optlist", "opt", "usestylemodifier", "usestylemode",
  "usestyle", "styledeflist", "styledef", "stylenameposlist", "shapedef",
  "shapedeflist", "shapeline", "colordeflist_actioned", "colordeflist",
  "string_or_num", "colordef", "usedesign", "designdef",
  "scope_open_empty", "designelementlist", "designelement",
  "designoptlist", "designopt", "defproc", "defprochelp1", "defprochelp2",
  "defprochelp3", "defprochelp4", "scope_open_proc_body",
  "scope_close_proc_body", "proc_def_arglist_tested", "proc_def_arglist",
  "proc_def_param_list", "proc_def_param", "procedure_body", "set",
  "proc_invocation", "proc_param_list", "proc_invoc_param_list",
  "proc_invoc_param", "ifthenbranch", "comp", "condition",
  "ifthen_condition", "else", "ifthen", "colon_string",
  "full_attrlist_with_label", "full_attrlist", "attrlist", "attr",
  "attrvalue", "attrvalue_simple", "alignment_attrvalue", "edgepos",
  "alpha_string_or_percent", "blocknames_plus_number_multi",
  "blocknames_plus_number_or_number", "blocknames_plus_number",
  "blocknames_plus", "coord_hinted", "coord", "reserved_word_string",
  "symbol_string", "entity_string_single", "alpha_string_single",
  "string_single", "tok_param_name_as_multi", "multi_string_continuation",
  "entity_string_single_or_param", "entity_string", "alpha_string",
  "string", "scope_open", "scope_close", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-366)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-527)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    2856,  -366,  -366,    83,  -366,   328,  -366,  -366,  -366,  -366,
    -366,  -366,   328,  -366,  -366,   212,  -366,    58,    61,  3372,
     241,   328,    49,    98,  3419,   328,  3037,  1485,    86,  1485,
    -366,    59,   154,  -366,   224,  2907,  3288,    76,   762,   108,
     214,    56,  -366,  -366,  2790,  -366,  1905,  -366,  1974,  2043,
    -366,   208,  -366,   463,  -366,  3372,  -366,  -366,   828,   828,
    -366,  -366,  -366,    53,   208,   220,   269,    99,   269,  -366,
     117,    89,    94,   111,  -366,    53,  -366,   233,  -366,   407,
    1088,  -366,  -366,  -366,  -366,  -366,  2112,  2181,  -366,   229,
    -366,  -366,  -366,   190,   304,  2658,  -366,   308,  -366,  -366,
    -366,   285,   232,  -366,   250,   119,  -366,  -366,  -366,  -366,
    -366,  3449,   268,   537,  -366,   265,   251,    53,  -366,   208,
    3434,   208,  -366,   271,  -366,  -366,  -366,  -366,  -366,  -366,
    -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,
    -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,
    -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,  -366,
    -366,  -366,  -366,  -366,  -366,  -366,   286,   292,  -366,   190,
    -366,   299,  -366,  -366,  -366,  -366,  -366,  -366,  -366,   302,
    -366,    57,  -366,   190,  -366,  -366,   305,  -366,  -366,  -366,
    -366,   303,   324,  -366,   315,   325,  -366,    69,  -366,  1629,
    -366,  -366,  -366,  2724,   160,  -366,  -366,   318,    54,  -366,
     339,  -366,   287,   294,  1698,  -366,  -366,  -366,    62,   343,
    -366,  2856,  -366,  -366,   364,  -366,   365,  -366,   368,  -366,
    -366,  -366,  -366,    65,   156,   182,  -366,  -366,  -366,  -366,
      53,   372,   265,  -366,    37,   377,    53,  -366,  -366,  -366,
    3374,   382,  -366,  -366,  -366,   217,  -366,   148,   148,  -366,
    -366,   148,   148,  -366,   148,  -366,  1836,  -366,  -366,  -366,
     380,  -366,   384,  -366,   385,  -366,   612,  2250,   322,  -366,
    -366,  2319,  3102,  1485,  -366,   958,  2972,  -366,  -366,  -366,
    -366,  2526,  -366,    83,   395,   402,  -366,  -366,  -366,  -366,
    -366,   328,  -366,   328,   397,  -366,   687,  -366,   328,  -366,
    -366,  -366,  2388,  3037,  -366,  -366,  1023,  1485,  1485,  -366,
    -366,  -366,  2732,   408,  -366,   357,   354,  3167,   413,   360,
    -366,  1560,  -366,  -366,  -366,  -366,  2592,  -366,  -366,  -366,
    -366,  1153,  -366,  -366,  1485,  -366,  1485,  -366,  -366,  -366,
    -366,  -366,  -366,  -366,  -366,  -366,  1225,  -366,   265,  -366,
    -366,   394,  2575,   321,   321,   321,   328,  -366,  3356,  -366,
     392,  2856,  -366,  -366,  -366,  -366,  -366,   409,  -366,  -366,
     391,   414,    74,  -366,    74,    74,  -366,  -366,   419,  1088,
     893,  -366,   367,  -366,  1290,  -366,  1767,  -366,  -366,   420,
    -366,  -366,  2856,   322,  2856,  -366,  -366,  -366,  -366,   425,
    -366,  -366,   190,  -366,  -366,   497,   418,  -366,    64,  -366,
    -366,  -366,    83,    83,  -366,  -366,  -366,   416,  -366,  -366,
     405,   428,  2457,   429,   292,  -366,  1355,  -366,  -366,  -366,
    -366,  3037,  1485,  -366,   442,  -366,   200,  -366,   430,  -366,
    -366,  -366,   393,   443,  -366,   455,   446,  -366,  -366,   403,
      76,  -366,  -366,    68,  -366,  -366,  -366,  -366,  -366,  -366,
     377,  -366,   321,  -366,   328,  2856,  -366,  -366,  -366,  -366,
     454,  2575,  -366,  -366,    74,  -366,  -366,   169,  -366,  -366,
    -366,   459,  -366,  -366,  -366,  -366,  -366,  -366,   208,  -366,
     208,  -366,   208,  -366,   328,  -366,  -366,  -366,   404,  1290,
    -366,  -366,  -366,  2856,  -366,  -366,  -366,  -366,  -366,   695,
    -366,  -366,  -366,  -366,  -366,   460,   465,   456,  -366,   462,
    -366,  -366,  -366,   302,   209,  -366,   328,  1420,  -366,  -366,
    -366,  -366,  -366,  -366,  -366,  -366,    53,  -366,  -366,  -366,
     321,  -366,  -366,   208,  -366,  -366,  -366,  -366,  -366,  -366,
    -366,  -366,  -366,  -366,  -366,  3232,  -366,  -366,   461,  -366,
    -366,  -366,  -366,  -366,  -366,  -366,  -366,   475,   477,  -366,
     486,  -366,   490,  -366
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       7,   514,   515,   242,    43,   165,   532,   157,   158,   159,
     160,   161,   163,   162,   516,   128,   170,   154,   139,   124,
     171,   175,   256,   252,    63,    72,    57,    36,    74,   312,
     267,   264,   263,   519,   467,   332,   369,   368,     0,    54,
       0,     0,     8,    28,     9,    22,     0,    20,     0,     0,
      40,    33,   122,   137,   123,   144,   136,   142,   146,   150,
      60,   179,   180,   182,    65,   194,   197,    68,   199,   202,
     226,    32,    29,     0,   250,     0,    66,    34,   257,   270,
     272,    59,    58,    75,   331,    61,     0,     0,    27,   190,
     461,   522,   523,   524,   204,     0,   243,   249,   166,   164,
     453,   129,   130,   452,   131,   454,   155,   156,   140,   141,
     127,   125,   458,     0,   173,   172,   176,   255,   251,    62,
     137,    64,    71,   281,   309,   473,   474,   475,   476,   477,
     478,   479,   512,   502,   503,   504,   505,   506,   507,   508,
     509,   510,   511,   480,   481,   482,   483,   487,   488,   489,
     490,   491,   492,   493,   494,   495,   496,   497,   484,   485,
     486,   501,   498,   499,   500,    56,   299,   300,   517,   527,
     526,   308,   188,   189,   184,   185,   186,   187,   513,    35,
     273,   277,   518,   530,   529,   278,     0,    73,   311,   265,
     266,   441,   470,   466,   468,   437,   451,   454,   343,     0,
     333,   336,   339,     0,   337,   345,   342,   340,   334,   370,
     355,   367,   394,   392,     0,    55,     1,     4,     0,     5,
       2,    10,    23,    21,     0,    25,     0,    51,     0,    42,
      30,   138,   181,    70,    38,   204,   143,   145,   407,   406,
     147,     0,   408,   148,   412,   167,   151,   152,   183,    76,
       0,   195,   207,   200,   201,   206,   198,   220,   228,    67,
     203,   222,   227,    37,     0,   253,     0,   268,   269,   429,
     271,   421,   529,   428,     0,    45,     0,     0,   399,   384,
     385,     0,   191,   520,   525,   262,   211,   205,    17,   533,
      19,     0,    13,   242,   247,   248,   132,   133,   134,   135,
     456,   455,   126,   459,   420,   413,     0,   174,   177,   254,
      31,    77,     0,   302,   310,   528,   307,   275,   279,   276,
     531,   315,     0,   440,   465,   472,   469,   438,   443,     0,
     346,     0,   352,   364,   344,   366,     0,   360,   338,   341,
     335,   356,   395,   391,     0,   386,   388,     3,     6,    11,
      26,    53,    44,    69,    39,   149,   168,   409,   411,   153,
      83,   102,   118,    99,    99,    99,   111,    85,     0,    86,
       0,     0,    88,   101,   100,    79,   196,   209,   208,   221,
     225,   224,   230,   223,   229,   233,   260,   258,     0,   423,
     427,    46,     0,   383,   378,   371,     0,   377,   382,     0,
      48,   396,   402,     0,   400,   193,   192,   521,   261,   430,
     432,   433,   435,   434,   213,   215,   212,    16,     0,    18,
      12,   246,   242,   242,   457,   460,   416,   418,   414,   178,
       0,   290,     0,     0,   301,   304,     0,   305,   303,   274,
     280,   319,   321,   323,     0,   316,     0,   324,     0,   442,
     464,   463,   471,   449,   439,   445,   447,   444,   347,     0,
     353,   351,   363,     0,   365,   359,   358,   357,   390,   389,
     169,   410,    99,   103,   120,     0,   119,    94,    93,    95,
      98,   118,    92,   108,   105,   110,    82,     0,    84,    87,
      78,     0,    89,   117,   116,   210,   241,   236,   232,   235,
     231,   237,   234,   422,   426,   424,   372,   379,     0,   380,
     376,    49,   405,   401,   403,   431,   436,   217,   216,   214,
      15,    14,   244,   245,   415,     0,   291,   289,   284,     0,
     286,   306,   318,   320,     0,   327,   326,   330,   462,   450,
     446,   448,   349,   354,   362,   361,   104,   121,   115,   114,
      96,   109,   113,   107,    81,    80,    90,   240,   425,   374,
     381,   404,   219,   218,   287,   292,   285,   288,     0,   313,
     317,   325,   328,   329,   106,    97,   112,   295,   293,   314,
     296,   294,   297,   298
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -366,  -366,   278,  -366,   282,   -43,   -57,     1,  -366,     4,
    -366,  -366,  -366,  -117,  -366,   131,   -45,  -366,  -323,  -366,
    -366,  -366,    25,   483,  -366,   484,  -366,  -366,   -10,    42,
    -366,  -366,   450,  -366,  -366,    -2,  -366,   488,   176,  -366,
    -366,   -27,  -366,  -366,   188,  -366,   -39,    13,  -366,  -366,
    -366,  -262,  -286,   487,   493,  -366,  -366,   252,  -366,  -366,
    -221,    78,   199,    84,  -366,  -366,  -128,    82,  -366,    91,
     215,  -316,  -366,  -366,  -366,    -9,  -366,    -7,  -366,  -366,
    -366,   323,   326,  -366,  -319,  -366,  -366,  -366,  -197,   332,
    -366,  -366,  -366,  -366,  -364,  -365,  -366,  -366,  -366,  -272,
    -366,   288,   -35,    -4,   422,   144,   153,  -366,   -33,  -366,
    -366,  -366,   447,    -5,   524,    31,  -249,  -236,  -235,  -366,
    -366,  -366,   163,  -136,  -366,     0,   -13,   -11,   -61,  -239
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,    40,   220,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,   249,   368,   369,   479,   480,   481,   370,
     371,   372,   475,    51,    52,    53,    54,    55,    56,    57,
      58,    59,   240,    60,    61,    62,    63,    64,   178,    65,
      66,    67,   255,    68,   287,    69,    70,   382,    71,    72,
      73,   497,    97,    74,    75,    76,    77,    78,    79,    80,
      81,   179,   180,   181,   122,   432,   433,   165,   166,   437,
     167,    82,   187,   322,   444,   445,   446,   447,    83,    84,
     200,   201,   202,   203,   337,   204,   205,   331,   211,   206,
      85,    86,   277,   396,   397,   281,   346,   213,    87,   402,
      88,   242,   243,   244,   270,   271,   408,   409,   410,   454,
     455,   195,   102,   196,   111,    89,    90,   168,   182,    91,
     169,   183,    92,   284,    93,   170,   184,   185,    95,   292
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      94,   194,   332,   250,   311,    98,   443,   421,   230,   404,
     103,   375,    99,   171,   234,   105,   114,   465,   188,   112,
     112,   116,   208,   209,   247,   123,   233,   214,   248,   373,
     507,   207,   259,   315,   197,   263,   411,   512,   291,   514,
     265,   483,   484,   231,    94,   222,   104,   320,   223,   412,
     413,   232,   420,   235,     3,   237,   217,   218,   245,   245,
     250,   110,   217,   106,   520,   318,   108,   272,   544,   273,
     198,   113,   113,   257,   300,   113,   310,   238,   239,   268,
     301,   289,   309,   113,   328,   334,    23,    94,   279,   186,
       6,   280,   113,   238,   239,    94,   103,   236,    96,  -238,
     272,   105,   273,     3,  -239,   238,   239,   257,   189,   172,
     231,   307,   215,   199,   238,   239,    33,   113,   232,    33,
     219,   172,   499,   501,   300,   261,   219,   172,  -238,   490,
     301,   513,   297,  -239,   210,   113,   522,   523,   173,   238,
     239,   411,   241,   241,   545,   560,   336,   373,   561,   546,
     173,     1,     2,   302,   412,   413,   173,   238,   239,   325,
    -238,  -238,  -238,  -238,   261,  -239,  -239,  -239,  -239,   554,
     174,   175,   176,   177,   113,    14,   198,   319,   113,   521,
     107,   109,   174,   175,   176,   177,   289,   252,   174,   175,
     176,   177,   197,   253,   254,   286,   238,   239,   353,   354,
     207,   535,   190,    94,   207,   355,    33,    34,   536,  -322,
     568,   359,     1,     2,   216,     1,     2,   100,   443,   381,
     377,    94,   378,   381,     6,   381,   569,     1,     2,   191,
     379,   380,   192,   251,   383,   380,    14,   380,   357,    14,
     101,   266,   282,   258,     1,     2,   262,   283,   555,   264,
     374,    14,   441,   442,   256,    29,   260,   235,   235,   113,
     298,   235,   235,   543,   235,   398,   388,    33,    14,   406,
      33,    34,   407,   416,   252,   384,   516,   385,   299,   303,
     253,   254,    33,   113,   193,   197,   308,   312,     1,     2,
     100,    94,   222,   452,   313,   223,   424,   314,   425,    33,
     171,   105,   525,   105,   529,   438,   316,   440,   429,   252,
     317,   285,    14,   293,   456,   253,   254,   286,   323,   294,
     295,   321,   448,   326,     1,     2,   197,     1,     2,   191,
     467,     1,     2,   468,   198,   469,    94,   222,   327,   496,
     223,   496,   496,    33,    34,   440,   341,   498,    14,   500,
     502,    14,   348,   342,   471,    14,   470,     1,     2,   191,
     343,   473,   476,   482,   482,   482,   485,   552,   374,   477,
     478,    94,   493,   350,   351,   494,   272,   352,   273,    33,
     356,    14,    33,   398,   324,  -526,    33,   376,   389,   401,
     197,   390,   -91,   491,   391,   -91,   -91,     1,     2,   257,
     422,   492,    94,   279,    94,   279,   280,   423,   280,   -91,
       1,     2,    33,   449,   451,   518,   426,   450,   457,   -91,
     458,    14,   261,   250,   495,   438,   285,   506,   171,   511,
     515,   519,   431,   526,    14,   524,   576,   537,   530,   -91,
     -91,   -91,   -91,   -91,   -91,   -91,   -91,   -91,   472,   553,
     -91,   534,    33,   538,   267,   557,   -91,   557,   539,   557,
     540,   541,   550,   542,   559,    33,     1,     2,   556,   564,
     565,   567,   482,   566,   547,    94,   548,     5,   579,   549,
     580,   476,   581,     7,     8,     9,    10,    11,    12,    13,
      14,   582,   250,    17,    18,   583,   347,    21,   398,   489,
       1,     2,   517,   349,   558,   575,   551,   119,   120,   246,
     118,   574,   121,    94,   279,   117,   439,   280,   387,   563,
     533,    33,    34,   532,    14,   570,   573,   531,   434,   571,
     338,   340,   358,   503,   448,   306,   448,  -419,   304,   339,
       1,     2,   269,   505,   115,  -419,  -419,   172,   296,     0,
     482,  -419,   578,  -419,  -419,    33,   305,   125,   126,   127,
     128,   129,   130,   131,    14,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   173,  -419,  -419,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,     0,    33,  -419,     0,   158,   159,
     160,   161,   162,   163,   164,  -419,     0,     0,   174,   175,
     176,   177,  -373,   392,     0,     1,     2,   393,     0,     0,
     394,  -373,   172,     0,     0,     0,  -373,     0,  -373,  -373,
       0,     0,   125,   126,   127,   128,   129,   130,   131,    14,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   173,     0,     0,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,     0,
      33,  -373,   395,   158,   159,   160,   161,   162,   163,   164,
    -373,     0,     0,   174,   175,   176,   177,  -417,   427,     0,
    -417,  -417,  -417,     0,     0,   389,  -417,  -417,     1,     2,
     562,  -417,     0,  -417,  -417,     0,   428,  -417,  -417,  -417,
    -417,  -417,  -417,  -417,  -417,  -417,  -417,  -417,  -417,  -417,
    -417,  -417,    14,  -417,  -417,  -417,  -417,  -417,  -417,  -417,
    -417,  -417,  -417,  -417,  -417,  -417,  -417,  -417,  -417,  -417,
    -417,  -417,  -417,     0,     0,  -417,  -417,     0,  -417,  -417,
    -417,  -417,  -417,    33,  -417,  -417,     0,     0,  -417,  -417,
    -417,  -417,  -393,   212,     0,     1,     2,  -393,     0,     0,
       0,  -393,   172,     0,     0,     0,  -393,     0,  -393,  -393,
       0,     0,   125,   126,   127,   128,   129,   130,   131,    14,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   173,     0,     0,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,     0,
      33,  -393,     0,   158,   159,   160,   161,   162,   163,   164,
    -393,     1,     2,   174,   175,   176,   177,     0,   172,     0,
       0,     0,     0,     0,     0,     0,   113,     0,   125,   126,
     127,   128,   129,   130,   131,    14,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   173,   238,   239,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,    33,     0,     0,   158,
     159,   160,   161,   162,   163,   164,     1,     2,   191,   174,
     175,   176,   177,   172,   504,     0,     0,     0,     0,     0,
       0,     0,     0,   125,   126,   127,   128,   129,   130,   131,
      14,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   173,     0,     0,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
       0,    33,    34,     0,   158,   159,   160,   161,   162,   163,
     164,     1,     2,   191,   174,   175,   176,   177,   172,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   125,   126,
     127,   128,   129,   130,   131,    14,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   173,     0,     0,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,    33,    34,     0,   158,
     159,   160,   161,   162,   163,   164,     1,     2,   435,   174,
     175,   176,   177,   172,   436,     0,     0,     0,     0,     0,
       0,     0,     0,   125,   126,   127,   128,   129,   130,   131,
      14,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   173,     0,     0,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
       0,    33,     0,     0,   158,   159,   160,   161,   162,   163,
     164,     1,     2,   269,   174,   175,   176,   177,   172,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   125,   126,
     127,   128,   129,   130,   131,    14,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   173,     0,     0,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,    33,     0,     0,   158,
     159,   160,   161,   162,   163,   164,     1,     2,   466,   174,
     175,   176,   177,   172,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   125,   126,   127,   128,   129,   130,   131,
      14,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   173,     0,     0,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
       0,    33,     0,     0,   158,   159,   160,   161,   162,   163,
     164,     0,     0,     0,   174,   175,   176,   177,     1,     2,
       0,     0,     0,  -279,     0,   172,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   125,   126,   127,   128,   129,
     130,   131,    14,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   173,     0,     0,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,     0,    33,     0,     0,   158,   159,   160,   161,
     162,   163,   164,     1,     2,   393,   174,   175,   176,   177,
     172,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     125,   126,   127,   128,   129,   130,   131,    14,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   173,
       0,     0,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,     0,    33,     0,
       0,   158,   159,   160,   161,   162,   163,   164,     1,     2,
     435,   174,   175,   176,   177,   172,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   125,   126,   127,   128,   129,
     130,   131,    14,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   173,     0,     0,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,     0,    33,     0,     0,   158,   159,   160,   161,
     162,   163,   164,     1,     2,   572,   174,   175,   176,   177,
     172,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     125,   126,   127,   128,   129,   130,   131,    14,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   173,
       0,     0,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,     0,    33,     0,
       0,   158,   159,   160,   161,   162,   163,   164,     1,     2,
       0,   174,   175,   176,   177,   172,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   125,   126,   127,   128,   129,
     130,   131,    14,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   173,     0,     0,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,     0,    33,     0,     0,   158,   159,   160,   161,
     162,   163,   164,     0,     0,     0,   174,   175,   176,   177,
    -350,   459,     0,  -350,  -350,  -350,     0,     0,   460,  -350,
       0,     0,     0,     0,  -350,     0,  -350,  -350,  -350,     0,
    -350,  -350,  -350,  -350,  -350,  -350,  -350,  -350,  -350,  -350,
    -350,  -350,  -350,  -350,  -350,     0,  -350,  -350,  -350,     0,
       0,     0,  -350,  -350,  -350,  -350,  -350,  -350,  -350,  -350,
    -350,  -350,  -350,  -350,  -350,  -350,     0,     0,  -350,  -350,
     461,  -350,  -350,  -350,  -350,  -350,     0,  -350,  -350,  -348,
     329,     0,  -348,  -348,  -348,     0,     0,     0,  -348,     0,
       0,     0,     0,  -348,     0,  -348,  -348,  -348,     0,  -348,
    -348,  -348,  -348,  -348,  -348,  -348,  -348,  -348,  -348,  -348,
    -348,  -348,  -348,  -348,     0,  -348,  -348,  -348,     0,     0,
       0,  -348,  -348,  -348,  -348,  -348,  -348,  -348,  -348,  -348,
    -348,  -348,  -348,  -348,  -348,     0,     0,   210,  -348,   330,
    -348,  -348,  -348,  -348,  -348,     0,  -348,  -348,  -387,   344,
       0,  -387,  -387,  -387,     0,     0,     0,  -387,     0,     0,
       0,     0,  -387,     0,  -387,  -387,     0,     0,  -387,  -387,
    -387,  -387,  -387,  -387,  -387,  -387,  -387,  -387,  -387,  -387,
    -387,  -387,  -387,     0,  -387,  -387,  -387,   345,     0,     0,
    -387,  -387,  -387,  -387,  -387,  -387,  -387,  -387,  -387,  -387,
    -387,  -387,  -387,  -387,     0,     0,  -387,  -387,     0,  -387,
    -387,  -387,  -387,  -387,  -387,  -387,  -387,  -375,   508,     0,
    -375,  -375,  -375,     0,     0,   509,  -375,     0,     0,     0,
       0,  -375,     0,  -375,  -375,     0,     0,  -375,  -375,  -375,
    -375,  -375,  -375,  -375,  -375,  -375,  -375,  -375,  -375,  -375,
    -375,  -375,     0,  -375,  -375,  -375,     0,     0,     0,  -375,
    -375,  -375,  -375,  -375,  -375,  -375,  -375,  -375,  -375,  -375,
    -375,  -375,  -375,     0,     0,  -375,  -375,   510,  -375,  -375,
    -375,  -375,  -375,     0,  -375,  -375,  -259,   386,     0,     1,
       2,  -259,     0,     0,  -259,  -259,     0,     0,     0,     0,
    -259,     0,  -259,  -259,     0,     0,  -259,  -259,  -259,  -259,
    -259,  -259,  -259,    14,  -259,  -259,  -259,  -259,  -259,  -259,
    -259,     0,  -259,  -259,  -259,     0,     0,     0,  -259,  -259,
    -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,
    -259,  -259,     0,     0,    33,  -259,     0,  -259,  -259,  -259,
    -259,  -259,     0,  -259,  -259,   -24,   224,     0,   -24,   -24,
     -24,     0,     0,     0,   225,     0,     0,     0,     0,   -24,
       0,   -24,   -24,     0,     0,   -24,   -24,   -24,   -24,   -24,
     -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,
       0,   -24,   -24,   -24,     0,     0,     0,   -24,   -24,   -24,
     -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,
     -24,     0,     0,   -24,   -24,     0,   -24,   -24,   -24,   -24,
     -24,     0,   -24,   -24,   -52,   226,     0,   -52,   -52,   -52,
       0,     0,     0,   227,     0,     0,     0,     0,   -52,     0,
     -52,   -52,     0,     0,   -52,   -52,   -52,   -52,   -52,   -52,
     -52,   -52,   -52,   -52,   -52,   -52,   -52,   -52,   -52,     0,
     -52,   -52,   -52,     0,     0,     0,   -52,   -52,   -52,   -52,
     -52,   -52,   -52,   -52,   -52,   -52,   -52,   -52,   -52,   -52,
       0,     0,   -52,   -52,     0,   -52,   -52,   -52,   -52,   -52,
       0,   -52,   -52,   -41,   228,     0,   -41,   -41,   -41,     0,
       0,     0,   229,     0,     0,     0,     0,   -41,     0,   -41,
     -41,     0,     0,   -41,   -41,   -41,   -41,   -41,   -41,   -41,
     -41,   -41,   -41,   -41,   -41,   -41,   -41,   -41,     0,   -41,
     -41,   -41,     0,     0,     0,   -41,   -41,   -41,   -41,   -41,
     -41,   -41,   -41,   -41,   -41,   -41,   -41,   -41,   -41,     0,
       0,   -41,   -41,     0,   -41,   -41,   -41,   -41,   -41,     0,
     -41,   -41,   -47,   274,     0,   -47,   -47,   -47,     0,     0,
       0,   275,     0,     0,     0,     0,   -47,     0,   -47,   -47,
       0,     0,   -47,   -47,   -47,   -47,   -47,   -47,   -47,   -47,
     -47,   -47,   -47,   -47,   -47,   -47,   -47,     0,   -47,   -47,
     -47,     0,     0,     0,   -47,   -47,   -47,   -47,   -47,   -47,
     -47,   -47,   -47,   -47,   -47,   -47,   -47,   -47,     0,     0,
     -47,   276,     0,   -47,   -47,   -47,   -47,   -47,     0,   -47,
     -47,  -398,   278,     0,     1,     2,     3,     0,     0,     0,
       4,     0,     0,     0,     0,     5,     0,     6,  -398,     0,
       0,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,     0,    22,    23,    24,
       0,     0,     0,    25,    26,    27,    28,    29,    30,    31,
      32,  -398,  -398,  -398,  -398,  -398,  -398,     0,     0,    33,
      34,     0,    35,    36,    37,  -398,    38,     0,  -398,    39,
     -50,   399,     0,   -50,   -50,   -50,     0,     0,     0,   400,
       0,     0,     0,     0,   -50,     0,   -50,   -50,     0,     0,
     -50,   -50,   -50,   -50,   -50,   -50,   -50,   -50,   -50,   -50,
     -50,   -50,   -50,   -50,   -50,     0,   -50,   -50,   -50,     0,
       0,     0,   -50,   -50,   -50,   -50,   -50,   -50,   -50,   -50,
     -50,   -50,   -50,   -50,   -50,   -50,     0,     0,   -50,   -50,
       0,   -50,   -50,   -50,   -50,   -50,     0,   -50,   -50,  -397,
     403,     0,  -397,  -397,  -397,     0,     0,     0,  -397,     0,
       0,     0,     0,  -397,     0,  -397,  -397,     0,     0,  -397,
    -397,  -397,  -397,  -397,  -397,  -397,  -397,  -397,  -397,  -397,
    -397,  -397,  -397,  -397,     0,  -397,  -397,  -397,     0,     0,
       0,  -397,  -397,  -397,  -397,  -397,  -397,  -397,  -397,  -397,
    -397,  -397,  -397,  -397,  -397,     0,     0,  -397,  -397,     0,
    -397,  -397,  -397,  -397,  -397,     0,   401,  -397,  -282,   430,
       0,  -282,  -282,  -282,     0,     0,     0,  -282,     0,     0,
       0,     0,  -282,     0,  -282,  -282,     0,     0,  -282,  -282,
    -282,  -282,  -282,  -282,  -282,   431,  -282,  -282,  -282,  -282,
    -282,  -282,  -282,     0,  -282,  -282,  -282,     0,     0,     0,
    -282,  -282,  -282,  -282,  -282,  -282,  -282,  -282,  -282,  -282,
    -282,  -282,  -282,  -282,     0,     0,  -282,  -282,     0,  -282,
    -282,  -282,  -282,  -282,     0,  -282,  -282,  -283,   527,     0,
    -283,  -283,  -283,     0,     0,     0,  -283,     0,     0,     0,
       0,  -283,     0,  -283,   528,     0,     0,  -283,  -283,  -283,
    -283,  -283,  -283,  -283,   431,  -283,  -283,  -283,  -283,  -283,
    -283,  -283,     0,  -283,  -283,  -283,     0,     0,     0,  -283,
    -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,
    -283,  -283,  -283,     0,     0,  -283,  -283,     0,  -283,  -283,
    -283,  -283,  -283,     0,  -283,  -283,   417,   418,     0,     1,
       2,     3,     0,     0,     0,     4,     0,     0,     0,     0,
       5,     0,     6,   289,     0,     0,     7,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,     0,    22,    23,    24,     0,     0,     0,    25,    26,
      27,    28,    29,    30,    31,    32,     0,     0,     1,     2,
       0,     0,     0,     0,    33,    34,     0,    35,    36,    37,
     419,    38,   462,   463,    39,     1,     2,     3,     0,     0,
       0,     4,    14,     0,     0,     0,     5,     0,     6,   334,
       0,     0,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,     0,    22,    23,
      24,   474,     0,    33,    25,    26,    27,    28,    29,    30,
      31,    32,     0,     0,     0,     0,     0,     0,     0,     0,
      33,    34,     0,    35,    36,    37,   464,    38,   288,     0,
      39,     1,     2,     3,     0,     0,     0,     4,     0,     0,
       0,     0,     5,     0,     6,   289,     0,     0,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,     0,    22,    23,    24,     0,     0,     0,
      25,    26,    27,    28,    29,    30,    31,    32,     0,     0,
       0,     0,     0,     0,     0,     0,    33,    34,     0,    35,
      36,    37,   290,    38,   333,     0,    39,     1,     2,     3,
       0,     0,     0,     4,     0,     1,     2,     0,     5,     0,
       6,   334,     0,     0,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    14,
      22,    23,    24,     0,     0,     0,    25,    26,    27,    28,
      29,    30,    31,    32,     0,   441,   442,     0,    29,     0,
       0,     0,    33,    34,     0,    35,    36,    37,   335,    38,
      33,     0,    39,     1,     2,     3,     0,     0,     0,     4,
       0,     0,     0,     0,     5,     0,     6,   221,     0,     0,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,     0,    22,    23,    24,     0,
       0,     0,    25,    26,    27,    28,    29,    30,    31,    32,
       0,     0,     0,     0,     0,     0,     0,     0,    33,    34,
       0,    35,    36,    37,     0,    38,     0,     0,    39,     1,
       2,     3,     0,     0,     0,     4,     0,     0,     0,     0,
       5,     0,     6,     0,     0,     0,     7,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,     0,    22,    23,    24,     0,     0,     0,    25,    26,
      27,    28,    29,    30,    31,    32,     0,     0,     0,     0,
       1,     2,     0,     0,    33,    34,     0,    35,    36,    37,
       0,    38,     0,   198,    39,   113,     0,   125,   126,   127,
     128,   129,   130,   131,    14,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,     0,     0,     0,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,     0,    33,   199,     0,   158,   159,
     160,   161,   162,   163,   164,     1,     2,   414,     0,     0,
       0,     0,     0,     0,     0,   415,     0,     0,     0,     0,
       0,     0,   125,   126,   127,   128,   129,   130,   131,    14,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,     0,     0,     0,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,     0,
      33,     0,     0,   158,   159,   160,   161,   162,   163,   164,
       1,     2,   124,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   125,   126,   127,
     128,   129,   130,   131,    14,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,     0,     0,     0,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,     0,    33,     0,     0,   158,   159,
     160,   161,   162,   163,   164,     1,     2,   405,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   125,   126,   127,   128,   129,   130,   131,    14,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,     0,     0,     0,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,     0,
      33,     0,     0,   158,   159,   160,   161,   162,   163,   164,
       1,     2,   453,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   125,   126,   127,
     128,   129,   130,   131,    14,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,     0,     0,     0,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,     0,    33,     0,     0,   158,   159,
     160,   161,   162,   163,   164,     1,     2,   577,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   125,   126,   127,   128,   129,   130,   131,    14,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,     0,     0,     0,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,     0,
      33,     1,     2,   158,   159,   160,   161,   162,   163,   164,
       0,     0,     0,     0,     0,     0,     0,     0,   125,   126,
     127,   128,   129,   130,   131,    14,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,     0,     0,     0,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,    33,     0,     0,   158,
     159,   160,   161,   162,   163,   164,   486,   487,     0,     1,
       2,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   289,   360,     1,     2,     1,     2,     0,
       0,     0,     0,    14,     0,     0,     5,     0,     0,     0,
       0,   289,     7,     8,     9,    10,    11,    12,    13,    14,
       0,    14,    17,    30,    31,    32,   361,   362,   363,   364,
     365,   366,     0,     0,    33,     0,     0,     0,     0,     0,
     488,    30,    31,    32,   361,   362,   363,   364,   365,   366,
      33,     0,    33,     5,     0,     0,     0,     0,   367,     7,
       8,     9,    10,    11,    12,    13,     0,    15,     5,    17,
      18,    19,     0,    21,     7,     8,     9,    10,    11,    12,
      13,     0,     0,     5,    17,    18,     0,     0,    21,     7,
       8,     9,    10,    11,    12,    13,     0,     0,     0,    17
};

static const yytype_int16 yycheck[] =
{
       0,    34,   199,    64,   121,     5,   322,   293,    51,   281,
      15,   250,    12,    26,    53,    15,    20,   336,    29,    19,
      20,    21,    35,    36,    59,    25,    53,    38,    63,   250,
     394,    35,    67,   169,    34,    70,   285,   402,    95,   404,
      75,   364,   365,    53,    44,    44,    15,   183,    44,   285,
     285,    53,   291,    53,     5,    55,     0,     1,    58,    59,
     121,    19,     0,     5,     0,     8,     5,    80,     0,    80,
      16,    18,    18,     8,     5,    18,   119,    40,    41,    79,
      11,    17,   117,    18,    15,    17,    37,    87,    87,     3,
      16,    87,    18,    40,    41,    95,   101,    55,    15,    10,
     113,   101,   113,     5,    10,    40,    41,     8,    49,    10,
     120,   115,     4,    59,    40,    41,    58,    18,   120,    58,
      64,    10,   384,   385,     5,     8,    64,    10,    39,   368,
      11,   403,   101,    39,    58,    18,   422,   423,    39,    40,
      41,   390,    58,    59,   463,   509,   203,   368,   513,   472,
      39,     3,     4,   111,   390,   390,    39,    40,    41,   192,
      71,    72,    73,    74,     8,    71,    72,    73,    74,     0,
      71,    72,    73,    74,    18,    27,    16,   181,    18,   418,
      17,    18,    71,    72,    73,    74,    17,     5,    71,    72,
      73,    74,   192,    11,    12,    13,    40,    41,   233,   234,
     204,     1,    48,   203,   208,   240,    58,    59,     8,     9,
       1,   246,     3,     4,     0,     3,     4,     5,   534,   258,
       3,   221,     5,   262,    16,   264,    17,     3,     4,     5,
     257,   258,     8,    13,   261,   262,    27,   264,   242,    27,
      28,     8,    13,    67,     3,     4,    70,    57,   487,    73,
     250,    27,    43,    44,    66,    46,    68,   257,   258,    18,
      28,   261,   262,   460,   264,   276,   266,    58,    27,   282,
      58,    59,   283,   286,     5,   262,   412,   264,    28,    11,
      11,    12,    58,    18,    60,   285,    35,    16,     3,     4,
       5,   291,   291,   326,     8,   291,   301,     5,   303,    58,
     313,   301,   430,   303,   432,   316,     7,   318,   308,     5,
       8,     7,    27,     5,   327,    11,    12,    13,    15,    11,
      12,    16,   322,     8,     3,     4,   326,     3,     4,     5,
     341,     3,     4,   344,    16,   346,   336,   336,    13,   382,
     336,   384,   385,    58,    59,   356,     7,   382,    27,   384,
     385,    27,     9,    66,   358,    27,   356,     3,     4,     5,
      66,   361,   362,   363,   364,   365,   366,   484,   368,    48,
      49,   371,   371,     9,     9,   371,   389,     9,   389,    58,
       8,    27,    58,   394,    60,     8,    58,     5,     8,    67,
     390,     7,     0,     1,     9,     3,     4,     3,     4,     8,
       5,     9,   402,   402,   404,   404,   402,     5,   404,    17,
       3,     4,    58,     5,    60,   415,    19,    60,     5,    27,
      60,    27,     8,   484,    15,   436,     7,    60,   441,     9,
       5,    13,    27,     5,    27,    19,   553,     7,     9,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    54,   484,
      58,     9,    58,    60,    47,   498,    64,   500,    15,   502,
       5,    15,     8,    60,    60,    58,     3,     4,     9,     9,
       5,     9,   472,    17,   474,   475,   475,    14,    17,   475,
       5,   481,     5,    20,    21,    22,    23,    24,    25,    26,
      27,     5,   553,    30,    31,     5,   218,    34,   509,   368,
       3,     4,     5,   221,   504,   550,   481,    24,    24,    59,
      23,   546,    24,   513,   513,    22,   317,   513,   266,   519,
     442,    58,    59,   441,    27,   534,   537,   436,   313,   536,
     204,   208,   244,   389,   534,   113,   536,     0,     1,   207,
       3,     4,     5,   390,    20,     8,     9,    10,   101,    -1,
     550,    14,   565,    16,    17,    58,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    -1,    58,    59,    -1,    61,    62,
      63,    64,    65,    66,    67,    68,    -1,    -1,    71,    72,
      73,    74,     0,     1,    -1,     3,     4,     5,    -1,    -1,
       8,     9,    10,    -1,    -1,    -1,    14,    -1,    16,    17,
      -1,    -1,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    -1,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    -1,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    -1,    -1,    71,    72,    73,    74,     0,     1,    -1,
       3,     4,     5,    -1,    -1,     8,     9,    10,     3,     4,
       5,    14,    -1,    16,    17,    -1,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    27,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    -1,    -1,    58,    59,    -1,    61,    62,
      63,    64,    65,    58,    67,    68,    -1,    -1,    71,    72,
      73,    74,     0,     1,    -1,     3,     4,     5,    -1,    -1,
      -1,     9,    10,    -1,    -1,    -1,    14,    -1,    16,    17,
      -1,    -1,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    -1,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    -1,
      58,    59,    -1,    61,    62,    63,    64,    65,    66,    67,
      68,     3,     4,    71,    72,    73,    74,    -1,    10,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    18,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    -1,    58,    -1,    -1,    61,
      62,    63,    64,    65,    66,    67,     3,     4,     5,    71,
      72,    73,    74,    10,    11,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    -1,    -1,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      -1,    58,    59,    -1,    61,    62,    63,    64,    65,    66,
      67,     3,     4,     5,    71,    72,    73,    74,    10,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    -1,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    -1,    58,    59,    -1,    61,
      62,    63,    64,    65,    66,    67,     3,     4,     5,    71,
      72,    73,    74,    10,    11,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    -1,    -1,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      -1,    58,    -1,    -1,    61,    62,    63,    64,    65,    66,
      67,     3,     4,     5,    71,    72,    73,    74,    10,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    -1,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    -1,    58,    -1,    -1,    61,
      62,    63,    64,    65,    66,    67,     3,     4,     5,    71,
      72,    73,    74,    10,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    -1,    -1,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      -1,    58,    -1,    -1,    61,    62,    63,    64,    65,    66,
      67,    -1,    -1,    -1,    71,    72,    73,    74,     3,     4,
      -1,    -1,    -1,     8,    -1,    10,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    -1,    -1,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    -1,    58,    -1,    -1,    61,    62,    63,    64,
      65,    66,    67,     3,     4,     5,    71,    72,    73,    74,
      10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      -1,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    -1,    58,    -1,
      -1,    61,    62,    63,    64,    65,    66,    67,     3,     4,
       5,    71,    72,    73,    74,    10,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    -1,    -1,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    -1,    58,    -1,    -1,    61,    62,    63,    64,
      65,    66,    67,     3,     4,     5,    71,    72,    73,    74,
      10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      -1,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    -1,    58,    -1,
      -1,    61,    62,    63,    64,    65,    66,    67,     3,     4,
      -1,    71,    72,    73,    74,    10,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    -1,    -1,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    -1,    58,    -1,    -1,    61,    62,    63,    64,
      65,    66,    67,    -1,    -1,    -1,    71,    72,    73,    74,
       0,     1,    -1,     3,     4,     5,    -1,    -1,     8,     9,
      -1,    -1,    -1,    -1,    14,    -1,    16,    17,    18,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    -1,    36,    37,    38,    -1,
      -1,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    -1,    -1,    58,    59,
      60,    61,    62,    63,    64,    65,    -1,    67,    68,     0,
       1,    -1,     3,     4,     5,    -1,    -1,    -1,     9,    -1,
      -1,    -1,    -1,    14,    -1,    16,    17,    18,    -1,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    -1,    36,    37,    38,    -1,    -1,
      -1,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    -1,    -1,    58,    59,    60,
      61,    62,    63,    64,    65,    -1,    67,    68,     0,     1,
      -1,     3,     4,     5,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    14,    -1,    16,    17,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    -1,    36,    37,    38,    39,    -1,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    -1,    -1,    58,    59,    -1,    61,
      62,    63,    64,    65,    66,    67,    68,     0,     1,    -1,
       3,     4,     5,    -1,    -1,     8,     9,    -1,    -1,    -1,
      -1,    14,    -1,    16,    17,    -1,    -1,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    -1,    36,    37,    38,    -1,    -1,    -1,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    -1,    -1,    58,    59,    60,    61,    62,
      63,    64,    65,    -1,    67,    68,     0,     1,    -1,     3,
       4,     5,    -1,    -1,     8,     9,    -1,    -1,    -1,    -1,
      14,    -1,    16,    17,    -1,    -1,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    -1,    36,    37,    38,    -1,    -1,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    -1,    58,    59,    -1,    61,    62,    63,
      64,    65,    -1,    67,    68,     0,     1,    -1,     3,     4,
       5,    -1,    -1,    -1,     9,    -1,    -1,    -1,    -1,    14,
      -1,    16,    17,    -1,    -1,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      -1,    36,    37,    38,    -1,    -1,    -1,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    -1,    -1,    58,    59,    -1,    61,    62,    63,    64,
      65,    -1,    67,    68,     0,     1,    -1,     3,     4,     5,
      -1,    -1,    -1,     9,    -1,    -1,    -1,    -1,    14,    -1,
      16,    17,    -1,    -1,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    -1,
      36,    37,    38,    -1,    -1,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      -1,    -1,    58,    59,    -1,    61,    62,    63,    64,    65,
      -1,    67,    68,     0,     1,    -1,     3,     4,     5,    -1,
      -1,    -1,     9,    -1,    -1,    -1,    -1,    14,    -1,    16,
      17,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    -1,    36,
      37,    38,    -1,    -1,    -1,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    -1,
      -1,    58,    59,    -1,    61,    62,    63,    64,    65,    -1,
      67,    68,     0,     1,    -1,     3,     4,     5,    -1,    -1,
      -1,     9,    -1,    -1,    -1,    -1,    14,    -1,    16,    17,
      -1,    -1,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    -1,    36,    37,
      38,    -1,    -1,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    -1,    -1,
      58,    59,    -1,    61,    62,    63,    64,    65,    -1,    67,
      68,     0,     1,    -1,     3,     4,     5,    -1,    -1,    -1,
       9,    -1,    -1,    -1,    -1,    14,    -1,    16,    17,    -1,
      -1,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    -1,    36,    37,    38,
      -1,    -1,    -1,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    -1,    58,
      59,    -1,    61,    62,    63,    64,    65,    -1,    67,    68,
       0,     1,    -1,     3,     4,     5,    -1,    -1,    -1,     9,
      -1,    -1,    -1,    -1,    14,    -1,    16,    17,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    -1,    36,    37,    38,    -1,
      -1,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    -1,    -1,    58,    59,
      -1,    61,    62,    63,    64,    65,    -1,    67,    68,     0,
       1,    -1,     3,     4,     5,    -1,    -1,    -1,     9,    -1,
      -1,    -1,    -1,    14,    -1,    16,    17,    -1,    -1,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    -1,    36,    37,    38,    -1,    -1,
      -1,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    -1,    -1,    58,    59,    -1,
      61,    62,    63,    64,    65,    -1,    67,    68,     0,     1,
      -1,     3,     4,     5,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    14,    -1,    16,    17,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    -1,    36,    37,    38,    -1,    -1,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    -1,    -1,    58,    59,    -1,    61,
      62,    63,    64,    65,    -1,    67,    68,     0,     1,    -1,
       3,     4,     5,    -1,    -1,    -1,     9,    -1,    -1,    -1,
      -1,    14,    -1,    16,    17,    -1,    -1,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    -1,    36,    37,    38,    -1,    -1,    -1,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    -1,    -1,    58,    59,    -1,    61,    62,
      63,    64,    65,    -1,    67,    68,     0,     1,    -1,     3,
       4,     5,    -1,    -1,    -1,     9,    -1,    -1,    -1,    -1,
      14,    -1,    16,    17,    -1,    -1,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    -1,    36,    37,    38,    -1,    -1,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    -1,    -1,     3,     4,
      -1,    -1,    -1,    -1,    58,    59,    -1,    61,    62,    63,
      64,    65,     0,     1,    68,     3,     4,     5,    -1,    -1,
      -1,     9,    27,    -1,    -1,    -1,    14,    -1,    16,    17,
      -1,    -1,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    -1,    36,    37,
      38,    56,    -1,    58,    42,    43,    44,    45,    46,    47,
      48,    49,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      58,    59,    -1,    61,    62,    63,    64,    65,     0,    -1,
      68,     3,     4,     5,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    14,    -1,    16,    17,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    -1,    36,    37,    38,    -1,    -1,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    58,    59,    -1,    61,
      62,    63,    64,    65,     0,    -1,    68,     3,     4,     5,
      -1,    -1,    -1,     9,    -1,     3,     4,    -1,    14,    -1,
      16,    17,    -1,    -1,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    27,
      36,    37,    38,    -1,    -1,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    -1,    43,    44,    -1,    46,    -1,
      -1,    -1,    58,    59,    -1,    61,    62,    63,    64,    65,
      58,    -1,    68,     3,     4,     5,    -1,    -1,    -1,     9,
      -1,    -1,    -1,    -1,    14,    -1,    16,    17,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    -1,    36,    37,    38,    -1,
      -1,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    58,    59,
      -1,    61,    62,    63,    -1,    65,    -1,    -1,    68,     3,
       4,     5,    -1,    -1,    -1,     9,    -1,    -1,    -1,    -1,
      14,    -1,    16,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    -1,    36,    37,    38,    -1,    -1,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    -1,    -1,    -1,    -1,
       3,     4,    -1,    -1,    58,    59,    -1,    61,    62,    63,
      -1,    65,    -1,    16,    68,    18,    -1,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    -1,    -1,    -1,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    -1,    58,    59,    -1,    61,    62,
      63,    64,    65,    66,    67,     3,     4,     5,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    13,    -1,    -1,    -1,    -1,
      -1,    -1,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    -1,    -1,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    -1,
      58,    -1,    -1,    61,    62,    63,    64,    65,    66,    67,
       3,     4,     5,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    -1,    -1,    -1,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    -1,    58,    -1,    -1,    61,    62,
      63,    64,    65,    66,    67,     3,     4,     5,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    -1,    -1,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    -1,
      58,    -1,    -1,    61,    62,    63,    64,    65,    66,    67,
       3,     4,     5,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    -1,    -1,    -1,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    -1,    58,    -1,    -1,    61,    62,
      63,    64,    65,    66,    67,     3,     4,     5,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    -1,    -1,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    -1,
      58,     3,     4,    61,    62,    63,    64,    65,    66,    67,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    -1,    -1,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    -1,    58,    -1,    -1,    61,
      62,    63,    64,    65,    66,    67,     0,     1,    -1,     3,
       4,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    17,     0,     3,     4,     3,     4,    -1,
      -1,    -1,    -1,    27,    -1,    -1,    14,    -1,    -1,    -1,
      -1,    17,    20,    21,    22,    23,    24,    25,    26,    27,
      -1,    27,    30,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    -1,    -1,    58,    -1,    -1,    -1,    -1,    -1,
      64,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      58,    -1,    58,    14,    -1,    -1,    -1,    -1,    64,    20,
      21,    22,    23,    24,    25,    26,    -1,    28,    14,    30,
      31,    32,    -1,    34,    20,    21,    22,    23,    24,    25,
      26,    -1,    -1,    14,    30,    31,    -1,    -1,    34,    20,
      21,    22,    23,    24,    25,    26,    -1,    -1,    -1,    30
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     9,    14,    16,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    36,    37,    38,    42,    43,    44,    45,    46,
      47,    48,    49,    58,    59,    61,    62,    63,    65,    68,
      76,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     108,   109,   110,   111,   112,   114,   115,   116,   118,   120,
     121,   123,   124,   125,   128,   129,   130,   131,   132,   133,
     134,   135,   146,   153,   154,   165,   166,   173,   175,   190,
     191,   194,   197,   199,   200,   203,    15,   127,   200,   200,
       5,    28,   187,   188,   190,   200,     5,   197,     5,   197,
     104,   189,   200,    18,   178,   189,   200,   129,   128,    98,
     100,   112,   139,   200,     5,    20,    21,    22,    23,    24,
      25,    26,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    61,    62,
      63,    64,    65,    66,    67,   142,   143,   145,   192,   195,
     200,   201,    10,    39,    71,    72,    73,    74,   113,   136,
     137,   138,   193,   196,   201,   202,     3,   147,   202,    49,
      48,     5,     8,    60,   183,   186,   188,   200,    16,    59,
     155,   156,   157,   158,   160,   161,   164,   178,   201,   201,
      58,   163,     1,   172,   202,     4,     0,     0,     1,    64,
      77,    17,    82,    84,     1,     9,     1,     9,     1,     9,
      80,   103,   110,   116,   121,   200,   104,   200,    40,    41,
     107,   138,   176,   177,   178,   200,   107,   177,   177,    88,
     203,    13,     5,    11,    12,   117,   119,     8,   113,   177,
     119,     8,   113,   177,   113,   177,     8,    47,   200,     5,
     179,   180,   201,   202,     1,     9,    59,   167,     1,    82,
      84,   170,    13,    57,   198,     7,    13,   119,     0,    17,
      64,    81,   204,     5,    11,    12,   187,   190,    28,    28,
       5,    11,   104,    11,     1,    19,   179,   178,    35,   177,
      80,    88,    16,     8,     5,   198,     7,     8,     8,   178,
     198,    16,   148,    15,    60,   183,     8,    13,    15,     1,
      60,   162,   163,     0,    17,    64,    81,   159,   157,   164,
     156,     7,    66,    66,     1,    39,   171,    77,     9,    79,
       9,     9,     9,   177,   177,   177,     8,   178,   176,   177,
       0,    50,    51,    52,    53,    54,    55,    64,    89,    90,
      94,    95,    96,   135,   200,   204,     5,     3,     5,   116,
     116,   121,   122,   116,   122,   122,     1,   132,   200,     8,
       7,     9,     1,     5,     8,    60,   168,   169,   202,     1,
       9,    67,   174,     1,   174,     5,   201,   202,   181,   182,
     183,   191,   192,   193,     5,    13,   201,     0,     1,    64,
     204,   127,     5,     5,   188,   188,    19,     1,    19,   200,
       1,    27,   140,   141,   145,     5,    11,   144,   202,   137,
     202,    43,    44,   146,   149,   150,   151,   152,   200,     5,
      60,    60,   183,     5,   184,   185,   201,     5,    60,     1,
       8,    60,     0,     1,    64,   159,     5,   202,   202,   202,
     200,   178,    54,   200,    56,    97,   200,    48,    49,    91,
      92,    93,   200,    93,    93,   200,     0,     1,    64,    90,
     204,     1,     9,    82,    84,    15,    80,   126,   177,   126,
     177,   126,   177,   180,    11,   181,    60,   169,     1,     8,
      60,     9,   170,   174,   170,     5,   198,     5,   200,    13,
       0,   204,   127,   127,    19,   141,     5,     1,    17,   141,
       9,   144,   142,   136,     9,     1,     8,     7,    60,    15,
       5,    15,    60,   163,     0,   159,    93,   200,    82,    84,
       8,    97,    88,   177,     0,   204,     9,    80,   200,    60,
     169,   170,     5,   200,     9,     5,    17,     9,     1,    17,
     150,   152,     5,   202,   177,    91,    88,     5,   201,    17,
       5,     5,     5,     5
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_uint8 yyr1[] =
{
       0,    75,    76,    76,    77,    77,    77,    78,    78,    79,
      79,    79,    80,    80,    80,    80,    80,    80,    80,    80,
      81,    81,    81,    81,    82,    82,    82,    82,    82,    82,
      82,    82,    83,    83,    83,    83,    83,    83,    83,    83,
      84,    84,    84,    84,    84,    84,    84,    84,    84,    84,
      84,    84,    84,    84,    85,    85,    86,    86,    86,    86,
      86,    86,    86,    86,    86,    86,    86,    86,    86,    86,
      86,    87,    87,    87,    87,    87,    87,    87,    88,    88,
      88,    88,    88,    88,    88,    88,    89,    89,    90,    90,
      90,    90,    91,    91,    91,    92,    92,    92,    93,    93,
      94,    94,    94,    94,    94,    94,    94,    94,    94,    94,
      95,    95,    96,    96,    96,    96,    96,    96,    97,    97,
      97,    97,    98,    98,    99,    99,    99,    99,   100,   100,
     100,   100,   100,   100,   100,   100,   101,   101,   101,   102,
     102,   102,   103,   103,   103,   103,   104,   104,   104,   104,
     104,   104,   104,   104,   104,   104,   104,   105,   105,   105,
     105,   105,   105,   106,   106,   106,   106,   107,   107,   107,
     108,   108,   108,   108,   108,   109,   109,   109,   109,   110,
     111,   111,   112,   112,   113,   113,   113,   113,   113,   113,
     114,   114,   114,   114,   115,   115,   115,   116,   116,   116,
     117,   117,   118,   118,   118,   118,   119,   119,   119,   119,
     119,   120,   120,   120,   120,   120,   120,   120,   120,   120,
     121,   121,   121,   121,   122,   122,   123,   123,   123,   123,
     123,   123,   123,   123,   123,   124,   124,   124,   125,   125,
     126,   126,   127,   127,   128,   128,   128,   128,   128,   128,
     129,   129,   129,   130,   130,   130,   130,   131,   131,   131,
     131,   132,   132,   133,   133,   133,   133,   134,   134,   134,
     134,   135,   135,   136,   136,   136,   137,   137,   138,   138,
     138,   139,   139,   139,   139,   139,   140,   140,   140,   140,
     141,   141,   141,   141,   141,   141,   141,   141,   141,   142,
     143,   143,   143,   144,   144,   145,   145,   145,   145,   145,
     145,   146,   146,   147,   147,   148,   149,   149,   150,   150,
     150,   150,   150,   150,   151,   151,   151,   151,   152,   152,
     152,   153,   154,   154,   155,   155,   155,   156,   156,   156,
     157,   157,   157,   158,   159,   160,   161,   161,   161,   161,
     161,   161,   162,   162,   162,   163,   163,   163,   163,   164,
     164,   164,   164,   164,   164,   164,   164,   165,   165,   166,
     166,   167,   167,   167,   167,   167,   167,   168,   168,   168,
     168,   168,   169,   169,   170,   170,   171,   172,   172,   172,
     172,   173,   173,   173,   173,   173,   174,   175,   175,   175,
     175,   175,   175,   175,   175,   175,   176,   176,   177,   177,
     177,   177,   177,   178,   178,   178,   178,   178,   178,   178,
     178,   179,   179,   179,   180,   180,   180,   180,   180,   180,
     181,   181,   181,   181,   182,   182,   182,   183,   183,   183,
     183,   183,   183,   183,   183,   184,   184,   185,   185,   185,
     185,   186,   187,   187,   188,   188,   188,   188,   189,   189,
     189,   190,   191,   191,   191,   191,   191,   191,   191,   191,
     191,   191,   191,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   192,   193,   194,   194,   194,   195,   196,   197,
     198,   198,   199,   199,   200,   200,   201,   201,   201,   202,
     202,   202,   203,   204
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     3,     1,     1,     2,     0,     1,     1,
       2,     3,     3,     2,     4,     4,     3,     2,     3,     2,
       1,     2,     1,     2,     1,     2,     3,     1,     1,     1,
       2,     3,     1,     1,     1,     2,     1,     2,     2,     3,
       1,     1,     2,     1,     3,     2,     3,     1,     3,     4,
       2,     2,     1,     3,     1,     2,     2,     1,     1,     1,
       1,     1,     2,     1,     2,     1,     1,     2,     1,     3,
       2,     2,     1,     2,     1,     1,     2,     3,     3,     2,
       4,     4,     3,     2,     3,     2,     1,     2,     1,     2,
       3,     1,     1,     1,     1,     1,     2,     3,     1,     0,
       1,     1,     1,     2,     3,     2,     4,     3,     2,     3,
       2,     1,     4,     3,     3,     3,     2,     2,     0,     1,
       1,     2,     1,     1,     1,     2,     3,     2,     1,     2,
       2,     2,     3,     3,     3,     3,     1,     1,     2,     1,
       2,     2,     1,     2,     1,     2,     1,     2,     2,     3,
       1,     2,     2,     3,     1,     2,     2,     1,     1,     1,
       1,     1,     1,     1,     2,     1,     2,     1,     2,     3,
       1,     1,     2,     2,     3,     1,     2,     3,     4,     1,
       1,     2,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     2,     3,     3,     1,     2,     3,     1,     2,     1,
       1,     1,     1,     2,     1,     2,     1,     1,     2,     2,
       3,     2,     3,     3,     4,     3,     4,     4,     5,     5,
       2,     3,     2,     3,     1,     1,     1,     2,     2,     3,
       3,     4,     4,     3,     4,     4,     4,     4,     1,     1,
       2,     1,     0,     1,     5,     5,     4,     3,     3,     2,
       1,     2,     1,     2,     3,     2,     1,     1,     3,     2,
       3,     3,     2,     1,     1,     2,     2,     1,     2,     2,
       1,     2,     1,     1,     3,     2,     2,     1,     1,     2,
       3,     1,     2,     3,     4,     5,     2,     3,     3,     2,
       1,     2,     3,     4,     5,     4,     5,     6,     7,     1,
       1,     3,     2,     1,     1,     3,     4,     2,     1,     1,
       2,     2,     1,     5,     6,     1,     1,     3,     2,     1,
       2,     1,     1,     1,     1,     3,     2,     2,     3,     3,
       2,     1,     1,     2,     1,     2,     1,     1,     2,     1,
       1,     2,     1,     1,     1,     1,     2,     3,     1,     4,
       2,     3,     1,     2,     3,     1,     2,     3,     3,     3,
       2,     4,     4,     3,     2,     3,     2,     2,     1,     1,
       2,     2,     3,     1,     4,     2,     3,     1,     1,     2,
       2,     3,     1,     1,     1,     1,     1,     1,     2,     3,
       3,     3,     2,     1,     2,     3,     1,     2,     1,     2,
       3,     4,     3,     4,     5,     4,     1,     1,     1,     2,
       3,     2,     1,     2,     3,     4,     3,     2,     3,     1,
       2,     1,     3,     2,     3,     4,     3,     2,     1,     1,
       1,     2,     1,     1,     1,     1,     2,     1,     2,     3,
       2,     1,     3,     2,     3,     1,     2,     1,     2,     1,
       2,     1,     1,     1,     1,     2,     2,     3,     1,     2,
       3,     1,     5,     4,     4,     3,     2,     1,     2,     3,
       2,     4,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     1,     1,     1,     2,     1,     1,     2,     1,
       1,     2,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, RESULT, yyscanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, RESULT, yyscanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), RESULT, yyscanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, RESULT, yyscanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_TOK_STRING: /* TOK_STRING  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2309 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_QSTRING: /* TOK_QSTRING  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2315 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_NUMBER: /* TOK_NUMBER  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2321 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_OCBRACKET: /* TOK_OCBRACKET  */
#line 183 "block_lang.yy"
            { }
#line 2327 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CCBRACKET: /* TOK_CCBRACKET  */
#line 183 "block_lang.yy"
            { }
#line 2333 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_BOX: /* TOK_BOX  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2339 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_BOXCOL: /* TOK_BOXCOL  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2345 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ROW: /* TOK_ROW  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2351 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLUMN: /* TOK_COLUMN  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2357 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_TEXT: /* TOK_TEXT  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2363 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_SHAPE: /* TOK_SHAPE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2369 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CELL: /* TOK_CELL  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2375 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_SHAPE_COMMAND: /* TOK_SHAPE_COMMAND  */
#line 183 "block_lang.yy"
            { }
#line 2381 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ALIGN_MODIFIER: /* TOK_ALIGN_MODIFIER  */
#line 183 "block_lang.yy"
            { }
#line 2387 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_BREAK_COMMAND: /* TOK_BREAK_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2393 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_SPACE_COMMAND: /* TOK_SPACE_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2399 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_MULTI_COMMAND: /* TOK_MULTI_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2405 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_AROUND_COMMAND: /* TOK_AROUND_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2411 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_JOIN_COMMAND: /* TOK_JOIN_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2417 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COPY_COMMAND: /* TOK_COPY_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2423 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_AS: /* TOK_AS  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2429 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_MARK_COMMAND: /* TOK_MARK_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2435 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_EXTEND_COMMAND: /* TOK_EXTEND_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2441 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_TEMPLATE_COMMAND: /* TOK_TEMPLATE_COMMAND  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2447 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMP_OP: /* TOK_COMP_OP  */
#line 183 "block_lang.yy"
            { }
#line 2453 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_STRING: /* TOK_COLON_STRING  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2459 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_QUOTED_STRING: /* TOK_COLON_QUOTED_STRING  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2465 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFSHAPE: /* TOK_COMMAND_DEFSHAPE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2471 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFCOLOR: /* TOK_COMMAND_DEFCOLOR  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2477 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFSTYLE: /* TOK_COMMAND_DEFSTYLE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2483 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFDESIGN: /* TOK_COMMAND_DEFDESIGN  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2489 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_USEDESIGN: /* TOK_COMMAND_USEDESIGN  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2495 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_USESTYLE: /* TOK_COMMAND_USESTYLE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2501 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_ARROWS: /* TOK_COMMAND_ARROWS  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2507 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_BLOCKS: /* TOK_COMMAND_BLOCKS  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2513 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CLONE_MODIFIER_RECURSIVE: /* TOK_CLONE_MODIFIER_RECURSIVE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2519 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CLONE_MODIFIER_ADD: /* TOK_CLONE_MODIFIER_ADD  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2525 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CLONE_MODIFIER_MOVE: /* TOK_CLONE_MODIFIER_MOVE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2531 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CLONE_MODIFIER_DROP: /* TOK_CLONE_MODIFIER_DROP  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2537 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CLONE_MODIFIER_UPDATE: /* TOK_CLONE_MODIFIER_UPDATE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2543 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CLONE_MODIFIER_REPLACE: /* TOK_CLONE_MODIFIER_REPLACE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2549 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CLONE_MODIFIER_BEFORE: /* TOK_CLONE_MODIFIER_BEFORE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2555 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_PARAM_NAME: /* TOK_PARAM_NAME  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2561 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFPROC: /* TOK_COMMAND_DEFPROC  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2567 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_REPLAY: /* TOK_COMMAND_REPLAY  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2573 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_SET: /* TOK_COMMAND_SET  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2579 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_BYE: /* TOK_BYE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2585 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_IF: /* TOK_IF  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2591 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_THEN: /* TOK_THEN  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2597 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ELSE: /* TOK_ELSE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2603 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_INCLUDE: /* TOK_COMMAND_INCLUDE  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2609 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ARROW_FW: /* TOK_ARROW_FW  */
#line 183 "block_lang.yy"
            { }
#line 2615 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ARROW_BW: /* TOK_ARROW_BW  */
#line 183 "block_lang.yy"
            { }
#line 2621 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ARROW_BIDIR: /* TOK_ARROW_BIDIR  */
#line 183 "block_lang.yy"
            { }
#line 2627 "block_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ARROW_NO: /* TOK_ARROW_NO  */
#line 183 "block_lang.yy"
            { }
#line 2633 "block_csh_lang.cc"
        break;

    case YYSYMBOL_top_level_instrlist: /* top_level_instrlist  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 2643 "block_csh_lang.cc"
        break;

    case YYSYMBOL_braced_instrlist: /* braced_instrlist  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 2653 "block_csh_lang.cc"
        break;

    case YYSYMBOL_instrlist: /* instrlist  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 2663 "block_csh_lang.cc"
        break;

    case YYSYMBOL_multi_instr: /* multi_instr  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 2673 "block_csh_lang.cc"
        break;

    case YYSYMBOL_multi_instr_req_semicolon: /* multi_instr_req_semicolon  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 2683 "block_csh_lang.cc"
        break;

    case YYSYMBOL_complete_instr: /* complete_instr  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction);
  #endif
}
#line 2693 "block_csh_lang.cc"
        break;

    case YYSYMBOL_include: /* include  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2699 "block_csh_lang.cc"
        break;

    case YYSYMBOL_instr_req_semicolon: /* instr_req_semicolon  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction);
  #endif
}
#line 2709 "block_csh_lang.cc"
        break;

    case YYSYMBOL_instr_ending_brace: /* instr_ending_brace  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction);
  #endif
}
#line 2719 "block_csh_lang.cc"
        break;

    case YYSYMBOL_braced_clone_action_list: /* braced_clone_action_list  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).cloneactionlist);
  #endif
}
#line 2729 "block_csh_lang.cc"
        break;

    case YYSYMBOL_clone_action_list: /* clone_action_list  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).cloneactionlist);
  #endif
}
#line 2739 "block_csh_lang.cc"
        break;

    case YYSYMBOL_clone_action: /* clone_action  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).cloneaction);
  #endif
}
#line 2749 "block_csh_lang.cc"
        break;

    case YYSYMBOL_thing_to_update: /* thing_to_update  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2755 "block_csh_lang.cc"
        break;

    case YYSYMBOL_things_to_update: /* things_to_update  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 2761 "block_csh_lang.cc"
        break;

    case YYSYMBOL_opt_things_to_update: /* opt_things_to_update  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 2767 "block_csh_lang.cc"
        break;

    case YYSYMBOL_clone_action_req_semicolon: /* clone_action_req_semicolon  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).cloneaction);
  #endif
}
#line 2777 "block_csh_lang.cc"
        break;

    case YYSYMBOL_clone_modifier_replace_with_name: /* clone_modifier_replace_with_name  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 2783 "block_csh_lang.cc"
        break;

    case YYSYMBOL_clone_action_no_semicolon: /* clone_action_no_semicolon  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).cloneaction);
  #endif
}
#line 2793 "block_csh_lang.cc"
        break;

    case YYSYMBOL_opt_before: /* opt_before  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 2799 "block_csh_lang.cc"
        break;

    case YYSYMBOL_total_full_block_def: /* total_full_block_def  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).blockblocklist);}
#line 2805 "block_csh_lang.cc"
        break;

    case YYSYMBOL_full_block_def_enclose: /* full_block_def_enclose  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).blockblocklist);}
#line 2811 "block_csh_lang.cc"
        break;

    case YYSYMBOL_alignment_modifiers: /* alignment_modifiers  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).alignment_attr);}
#line 2817 "block_csh_lang.cc"
        break;

    case YYSYMBOL_full_block_def_with_pre: /* full_block_def_with_pre  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).blockblocklist);}
#line 2823 "block_csh_lang.cc"
        break;

    case YYSYMBOL_multi_command: /* multi_command  */
#line 183 "block_lang.yy"
            { }
#line 2829 "block_csh_lang.cc"
        break;

    case YYSYMBOL_full_block_def: /* full_block_def  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).blockblocklist);}
#line 2835 "block_csh_lang.cc"
        break;

    case YYSYMBOL_block_def: /* block_def  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).blockblocklist);}
#line 2841 "block_csh_lang.cc"
        break;

    case YYSYMBOL_block_keyword: /* block_keyword  */
#line 183 "block_lang.yy"
            { }
#line 2847 "block_csh_lang.cc"
        break;

    case YYSYMBOL_shape_block_header: /* shape_block_header  */
#line 183 "block_lang.yy"
            { }
#line 2853 "block_csh_lang.cc"
        break;

    case YYSYMBOL_blocknameposlist: /* blocknameposlist  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 2859 "block_csh_lang.cc"
        break;

    case YYSYMBOL_command: /* command  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction);
  #endif
}
#line 2869 "block_csh_lang.cc"
        break;

    case YYSYMBOL_copy_header: /* copy_header  */
#line 205 "block_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    free(((*yyvaluep).copystruct));
  #else
    ((*yyvaluep).copystruct)->Free();
  #endif
}
#line 2881 "block_csh_lang.cc"
        break;

    case YYSYMBOL_copy_header_resolved: /* copy_header_resolved  */
#line 205 "block_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    free(((*yyvaluep).copystruct));
  #else
    ((*yyvaluep).copystruct)->Free();
  #endif
}
#line 2893 "block_csh_lang.cc"
        break;

    case YYSYMBOL_copy_header_with_pre: /* copy_header_with_pre  */
#line 205 "block_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    free(((*yyvaluep).copystruct));
  #else
    ((*yyvaluep).copystruct)->Free();
  #endif
}
#line 2905 "block_csh_lang.cc"
        break;

    case YYSYMBOL_copy_attr: /* copy_attr  */
#line 205 "block_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    free(((*yyvaluep).copystruct));
  #else
    ((*yyvaluep).copystruct)->Free();
  #endif
}
#line 2917 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrowsymbol: /* arrowsymbol  */
#line 183 "block_lang.yy"
            { }
#line 2923 "block_csh_lang.cc"
        break;

    case YYSYMBOL_coord_hinted_with_dir: /* coord_hinted_with_dir  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 2929 "block_csh_lang.cc"
        break;

    case YYSYMBOL_coord_hinted_with_dir_and_dist: /* coord_hinted_with_dir_and_dist  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 2935 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_end: /* arrow_end  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 2941 "block_csh_lang.cc"
        break;

    case YYSYMBOL_tok_plus_or_minus: /* tok_plus_or_minus  */
#line 183 "block_lang.yy"
            { }
#line 2947 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_end_block_port_compass_distance: /* arrow_end_block_port_compass_distance  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 2953 "block_csh_lang.cc"
        break;

    case YYSYMBOL_distance_for_arrow_end: /* distance_for_arrow_end  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 2959 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_end_block_port_compass: /* arrow_end_block_port_compass  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 2965 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrowend_list: /* arrowend_list  */
#line 192 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete chart.ExtractStashed(); //to clear any arrows we may have collected in BlockChart::StashedElement
  #endif
    delete ((*yyvaluep).stringposlist);
}
#line 2976 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrowend_list_solo: /* arrowend_list_solo  */
#line 192 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete chart.ExtractStashed(); //to clear any arrows we may have collected in BlockChart::StashedElement
  #endif
    delete ((*yyvaluep).stringposlist);
}
#line 2987 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_needs_semi: /* arrow_needs_semi  */
#line 186 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete chart.ExtractStashed(); //to clear any arrows we may have collected in BlockChart::StashedElement
    delete ((*yyvaluep).stringposlist_nocsh);
  #endif
}
#line 2998 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_no_semi: /* arrow_no_semi  */
#line 186 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete chart.ExtractStashed(); //to clear any arrows we may have collected in BlockChart::StashedElement
    delete ((*yyvaluep).stringposlist_nocsh);
  #endif
}
#line 3009 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_cont: /* arrow_cont  */
#line 186 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete chart.ExtractStashed(); //to clear any arrows we may have collected in BlockChart::StashedElement
    delete ((*yyvaluep).stringposlist_nocsh);
  #endif
}
#line 3020 "block_csh_lang.cc"
        break;

    case YYSYMBOL_full_attrlist_with_arrow_labels: /* full_attrlist_with_arrow_labels  */
#line 219 "block_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
  #else
    ((*yyvaluep).arrowattrs).Free();
  #endif
}
#line 3031 "block_csh_lang.cc"
        break;

    case YYSYMBOL_opt_percent: /* opt_percent  */
#line 183 "block_lang.yy"
            { }
#line 3037 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_label_number: /* arrow_label_number  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).arrowlabel);
  #endif
}
#line 3047 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_label_number_with_extend: /* arrow_label_number_with_extend  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).arrowlabel);
  #endif
}
#line 3057 "block_csh_lang.cc"
        break;

    case YYSYMBOL_arrow_label: /* arrow_label  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).arrowlabel);
  #endif
}
#line 3067 "block_csh_lang.cc"
        break;

    case YYSYMBOL_optlist: /* optlist  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 3077 "block_csh_lang.cc"
        break;

    case YYSYMBOL_opt: /* opt  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction);
  #endif
}
#line 3087 "block_csh_lang.cc"
        break;

    case YYSYMBOL_usestylemodifier: /* usestylemodifier  */
#line 183 "block_lang.yy"
            { }
#line 3093 "block_csh_lang.cc"
        break;

    case YYSYMBOL_usestylemode: /* usestylemode  */
#line 183 "block_lang.yy"
            { }
#line 3099 "block_csh_lang.cc"
        break;

    case YYSYMBOL_usestyle: /* usestyle  */
#line 225 "block_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
  #else
    delete ((*yyvaluep).usecommandhelper).attrs;
  #endif
}
#line 3110 "block_csh_lang.cc"
        break;

    case YYSYMBOL_styledeflist: /* styledeflist  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 3120 "block_csh_lang.cc"
        break;

    case YYSYMBOL_styledef: /* styledef  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 3130 "block_csh_lang.cc"
        break;

    case YYSYMBOL_stylenameposlist: /* stylenameposlist  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 3136 "block_csh_lang.cc"
        break;

    case YYSYMBOL_shapedeflist: /* shapedeflist  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).shape);
  #endif
}
#line 3146 "block_csh_lang.cc"
        break;

    case YYSYMBOL_shapeline: /* shapeline  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).shapeelement);
  #endif
}
#line 3156 "block_csh_lang.cc"
        break;

    case YYSYMBOL_colordeflist: /* colordeflist  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 3162 "block_csh_lang.cc"
        break;

    case YYSYMBOL_string_or_num: /* string_or_num  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3168 "block_csh_lang.cc"
        break;

    case YYSYMBOL_colordef: /* colordef  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 3174 "block_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp1: /* defprochelp1  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 3180 "block_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp2: /* defprochelp2  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 3186 "block_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp3: /* defprochelp3  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 3192 "block_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp4: /* defprochelp4  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 3198 "block_csh_lang.cc"
        break;

    case YYSYMBOL_scope_open_proc_body: /* scope_open_proc_body  */
#line 183 "block_lang.yy"
            { }
#line 3204 "block_csh_lang.cc"
        break;

    case YYSYMBOL_scope_close_proc_body: /* scope_close_proc_body  */
#line 183 "block_lang.yy"
            { }
#line 3210 "block_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_arglist_tested: /* proc_def_arglist_tested  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 3216 "block_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_arglist: /* proc_def_arglist  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 3222 "block_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_param_list: /* proc_def_param_list  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 3228 "block_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_param: /* proc_def_param  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdef);}
#line 3234 "block_csh_lang.cc"
        break;

    case YYSYMBOL_procedure_body: /* procedure_body  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procedure);}
#line 3240 "block_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invocation: /* proc_invocation  */
#line 183 "block_lang.yy"
            { }
#line 3246 "block_csh_lang.cc"
        break;

    case YYSYMBOL_proc_param_list: /* proc_param_list  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoclist);}
#line 3252 "block_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invoc_param_list: /* proc_invoc_param_list  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoclist);}
#line 3258 "block_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invoc_param: /* proc_invoc_param  */
#line 203 "block_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoc);}
#line 3264 "block_csh_lang.cc"
        break;

    case YYSYMBOL_ifthenbranch: /* ifthenbranch  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 3274 "block_csh_lang.cc"
        break;

    case YYSYMBOL_comp: /* comp  */
#line 183 "block_lang.yy"
            { }
#line 3280 "block_csh_lang.cc"
        break;

    case YYSYMBOL_condition: /* condition  */
#line 183 "block_lang.yy"
            { }
#line 3286 "block_csh_lang.cc"
        break;

    case YYSYMBOL_ifthen_condition: /* ifthen_condition  */
#line 212 "block_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
  #else
    chart.PopContext();
  #endif
}
#line 3298 "block_csh_lang.cc"
        break;

    case YYSYMBOL_else: /* else  */
#line 212 "block_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
  #else
    chart.PopContext();
  #endif
}
#line 3310 "block_csh_lang.cc"
        break;

    case YYSYMBOL_ifthen: /* ifthen  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 3320 "block_csh_lang.cc"
        break;

    case YYSYMBOL_colon_string: /* colon_string  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 3326 "block_csh_lang.cc"
        break;

    case YYSYMBOL_full_attrlist_with_label: /* full_attrlist_with_label  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).attributelist);
  #endif
}
#line 3336 "block_csh_lang.cc"
        break;

    case YYSYMBOL_full_attrlist: /* full_attrlist  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).attributelist);
  #endif
}
#line 3346 "block_csh_lang.cc"
        break;

    case YYSYMBOL_attrlist: /* attrlist  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).attributelist);
  #endif
}
#line 3356 "block_csh_lang.cc"
        break;

    case YYSYMBOL_attr: /* attr  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).attribute);
  #endif
}
#line 3366 "block_csh_lang.cc"
        break;

    case YYSYMBOL_attrvalue: /* attrvalue  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3372 "block_csh_lang.cc"
        break;

    case YYSYMBOL_attrvalue_simple: /* attrvalue_simple  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3378 "block_csh_lang.cc"
        break;

    case YYSYMBOL_alignment_attrvalue: /* alignment_attrvalue  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3384 "block_csh_lang.cc"
        break;

    case YYSYMBOL_edgepos: /* edgepos  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3390 "block_csh_lang.cc"
        break;

    case YYSYMBOL_alpha_string_or_percent: /* alpha_string_or_percent  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3396 "block_csh_lang.cc"
        break;

    case YYSYMBOL_blocknames_plus_number_multi: /* blocknames_plus_number_multi  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3402 "block_csh_lang.cc"
        break;

    case YYSYMBOL_blocknames_plus_number_or_number: /* blocknames_plus_number_or_number  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 3408 "block_csh_lang.cc"
        break;

    case YYSYMBOL_blocknames_plus_number: /* blocknames_plus_number  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 3414 "block_csh_lang.cc"
        break;

    case YYSYMBOL_blocknames_plus: /* blocknames_plus  */
#line 185 "block_lang.yy"
            {delete ((*yyvaluep).stringposlist);}
#line 3420 "block_csh_lang.cc"
        break;

    case YYSYMBOL_coord_hinted: /* coord_hinted  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3426 "block_csh_lang.cc"
        break;

    case YYSYMBOL_coord: /* coord  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3432 "block_csh_lang.cc"
        break;

    case YYSYMBOL_reserved_word_string: /* reserved_word_string  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 3438 "block_csh_lang.cc"
        break;

    case YYSYMBOL_symbol_string: /* symbol_string  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 3444 "block_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string_single: /* entity_string_single  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 3450 "block_csh_lang.cc"
        break;

    case YYSYMBOL_alpha_string_single: /* alpha_string_single  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 3456 "block_csh_lang.cc"
        break;

    case YYSYMBOL_string_single: /* string_single  */
#line 182 "block_lang.yy"
            {free(((*yyvaluep).str));}
#line 3462 "block_csh_lang.cc"
        break;

    case YYSYMBOL_tok_param_name_as_multi: /* tok_param_name_as_multi  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3468 "block_csh_lang.cc"
        break;

    case YYSYMBOL_multi_string_continuation: /* multi_string_continuation  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3474 "block_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string_single_or_param: /* entity_string_single_or_param  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3480 "block_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string: /* entity_string  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3486 "block_csh_lang.cc"
        break;

    case YYSYMBOL_alpha_string: /* alpha_string  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3492 "block_csh_lang.cc"
        break;

    case YYSYMBOL_string: /* string  */
#line 204 "block_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 3498 "block_csh_lang.cc"
        break;

    case YYSYMBOL_scope_close: /* scope_close  */
#line 198 "block_lang.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).instruction_list);
  #endif
}
#line 3508 "block_csh_lang.cc"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */


/* User initialization code.  */
#line 8 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    yylloc.first_pos = 0;
    yylloc.last_pos = 0;
  #endif
}

#line 3610 "block_csh_lang.cc"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, yyscanner);
    }

  if (yychar <= TOK_EOF)
    {
      yychar = TOK_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* chart_with_bye: chart eof  */
#line 288 "block_lang.yy"
{
    YYACCEPT;
}
#line 3825 "block_csh_lang.cc"
    break;

  case 3: /* chart_with_bye: chart error eof  */
#line 292 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
    YYACCEPT;
}
#line 3838 "block_csh_lang.cc"
    break;

  case 5: /* eof: TOK_BYE  */
#line 303 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_AllCommentBeyond((yylsp[0]));
  #else
  #endif
    free((yyvsp[0].str));
}
#line 3851 "block_csh_lang.cc"
    break;

  case 6: /* eof: TOK_BYE TOK_SEMICOLON  */
#line 312 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_AllCommentBeyond((yylsp[0]));
  #else
  #endif
    free((yyvsp[-1].str));
}
#line 3865 "block_csh_lang.cc"
    break;

  case 7: /* chart: %empty  */
#line 323 "block_lang.yy"
{
  //Add here what to do for an empty chart
  #ifdef C_S_H_IS_COMPILED
    csh.AddLineBeginToHints();
    csh.hintStatus = HINT_READY;
    csh.hintSource = EHintSourceType::LINE_START;
    csh.hintsForcedOnly = true;
  #else
    //no action for empty file
  #endif
}
#line 3881 "block_csh_lang.cc"
    break;

  case 8: /* chart: top_level_instrlist  */
#line 335 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBefore((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    //Add the instructionlist to the chart
    chart.Blocks.SetContent((yyvsp[0].instruction_list));
    delete (yyvsp[0].instruction_list);
  #endif
}
#line 3898 "block_csh_lang.cc"
    break;

  case 10: /* top_level_instrlist: instrlist TOK_CCBRACKET  */
#line 351 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Closing brace missing its opening pair.");
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Unexpected '}'.");
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 3912 "block_csh_lang.cc"
    break;

  case 11: /* top_level_instrlist: instrlist TOK_CCBRACKET top_level_instrlist  */
#line 361 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Closing brace missing its opening pair.");
  #else
    //Merge $3 into $1
    ((yyvsp[-2].instruction_list))->splice(((yyvsp[-2].instruction_list))->end(), *((yyvsp[0].instruction_list)));
    delete ((yyvsp[0].instruction_list));
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Unexpected '}'.");
  #endif
    (yyvsp[-1].input_text_ptr); //suppress
}
#line 3929 "block_csh_lang.cc"
    break;

  case 12: /* braced_instrlist: scope_open instrlist scope_close  */
#line 376 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    if ((yyvsp[0].instruction_list)) {
        ((yyvsp[-1].instruction_list))->Append((yyvsp[0].instruction_list)); //Append any potential CommandNumbering
        delete (yyvsp[0].instruction_list);
    }
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
  #endif
}
#line 3945 "block_csh_lang.cc"
    break;

  case 13: /* braced_instrlist: scope_open scope_close  */
#line 388 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    (yyval.instruction_list) = new BlockInstrList;
    //scope_close should not return here with a CommandNumbering
    //but just in case
    if ((yyvsp[0].instruction_list))
        delete((yyvsp[0].instruction_list));
  #endif
}
#line 3961 "block_csh_lang.cc"
    break;

  case 14: /* braced_instrlist: scope_open instrlist error scope_close  */
#line 400 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    if ((yyvsp[0].instruction_list)) {
        ((yyvsp[-2].instruction_list))->Append((yyvsp[0].instruction_list));
        delete (yyvsp[0].instruction_list);
    }
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
    yyerrok;
}
#line 3980 "block_csh_lang.cc"
    break;

  case 15: /* braced_instrlist: scope_open instrlist error TOK_EOF  */
#line 415 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    (yyval.instruction_list)->Append(chart.PopContext().release()); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
}
#line 3996 "block_csh_lang.cc"
    break;

  case 16: /* braced_instrlist: scope_open instrlist TOK_EOF  */
#line 427 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    (yyval.instruction_list)->Append(chart.PopContext().release()); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
}
#line 4013 "block_csh_lang.cc"
    break;

  case 17: /* braced_instrlist: scope_open TOK_EOF  */
#line 440 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    (yyval.instruction_list) = chart.PopContext().release(); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
}
#line 4028 "block_csh_lang.cc"
    break;

  case 18: /* braced_instrlist: scope_open instrlist TOK_BYE  */
#line 451 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    (yyval.instruction_list)->Append(chart.PopContext().release()); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
  #endif
  free((yyvsp[0].str));
}
#line 4046 "block_csh_lang.cc"
    break;

  case 19: /* braced_instrlist: scope_open TOK_BYE  */
#line 465 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = chart.PopContext().release(); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
  free((yyvsp[0].str));
}
#line 4062 "block_csh_lang.cc"
    break;

  case 20: /* instrlist: complete_instr  */
#line 479 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.instruction_list) = chart.AppendInstructionToList(nullptr, (yyvsp[0].instruction));
  #endif
}
#line 4072 "block_csh_lang.cc"
    break;

  case 21: /* instrlist: instrlist complete_instr  */
#line 485 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.instruction_list) = chart.AppendInstructionToList((yyvsp[-1].instruction_list), (yyvsp[0].instruction));
  #endif
}
#line 4082 "block_csh_lang.cc"
    break;

  case 23: /* instrlist: instrlist multi_instr  */
#line 492 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[-1].instruction_list)==nullptr)
        (yyvsp[-1].instruction_list) = new BlockInstrList;
    if ((yyvsp[0].instruction_list)) {
        ((yyvsp[-1].instruction_list))->Append((yyvsp[0].instruction_list));
        delete (yyvsp[0].instruction_list);
    }
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
  #endif
}
#line 4098 "block_csh_lang.cc"
    break;

  case 24: /* multi_instr: multi_instr_req_semicolon  */
#line 505 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    (yyval.instruction_list)=(yyvsp[0].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 4112 "block_csh_lang.cc"
    break;

  case 25: /* multi_instr: multi_instr_req_semicolon TOK_SEMICOLON  */
#line 515 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list)=(yyvsp[-1].instruction_list);
  #endif
}
#line 4129 "block_csh_lang.cc"
    break;

  case 26: /* multi_instr: multi_instr_req_semicolon error TOK_SEMICOLON  */
#line 528 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list)=(yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the instruction as I understood it.");
  #endif
}
#line 4149 "block_csh_lang.cc"
    break;

  case 27: /* multi_instr: ifthen  */
#line 544 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.IfThenElses.push_back((yyloc));
  #endif
    (yyval.instruction_list) = (yyvsp[0].instruction_list);
}
#line 4161 "block_csh_lang.cc"
    break;

  case 28: /* multi_instr: braced_instrlist  */
#line 552 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    //Standalone braced instruction lists are instructions - for the purpose of indentation
    csh.AddInstruction((yylsp[0]));
  #endif
  (yyval.instruction_list) = (yyvsp[0].instruction_list);
}
#line 4173 "block_csh_lang.cc"
    break;

  case 29: /* multi_instr: arrow_no_semi  */
#line 560 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddInstruction((yylsp[0]));
  #else
    (yyval.instruction_list) = chart.ExtractStashed();
    if ((yyvsp[0].stringposlist_nocsh)) delete (yyvsp[0].stringposlist_nocsh);
  #endif
}
#line 4186 "block_csh_lang.cc"
    break;

  case 30: /* multi_instr: total_full_block_def braced_instrlist  */
#line 569 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddInstruction((yyloc));
    csh.parent_name = csh.GetCurrentPrefix(); //we dont have content: restore prefix to that of our surrounding context
    csh.CreateBlocksFromLast((yyvsp[-1].blockblocklist)); //Copy the content of the last block to all in the $1 list
  #else
    chart.parent_style.Empty();
    chart.current_parent = chart.MyCurrentContext().parent; //we are done with content: restore prefix to that of our surrounding context
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent() && (yyvsp[-1].blockblocklist) && (yyvsp[-1].blockblocklist)->size())  {
        if ((yyvsp[0].instruction_list)) {
			(yyvsp[-1].blockblocklist)->front()->SetContent((yyvsp[0].instruction_list));
            delete (yyvsp[0].instruction_list);
			for (auto i = std::next((yyvsp[-1].blockblocklist)->begin()); i!=(yyvsp[-1].blockblocklist)->end(); i++) {
                //Create a cloned list of the content of the first block
                FileLineColRange inclusion((*i)->file_pos.TopOfStack());
                auto c = std::make_unique<BlockInstrList>();
                for (auto &pBlock: (yyvsp[-1].blockblocklist)->front()->content)
                    c->Append(pBlock->Clone(inclusion, (*i)->name_full, nullptr, nullptr, nullptr));
				(*i)->SetContent(c.get());
			}
		} else {
			for (auto &pBlock : *(yyvsp[-1].blockblocklist))
				pBlock->SetContent(nullptr);
		}
        (yyval.instruction_list) = new BlockInstrList;
		for (auto &pBlock : *(yyvsp[-1].blockblocklist))
			(yyval.instruction_list)->Append(std::move(pBlock));
    } else  {
        (yyval.instruction_list) = nullptr;
        delete (yyvsp[0].instruction_list);
    }
  #endif
    delete (yyvsp[-1].blockblocklist);
}
#line 4226 "block_csh_lang.cc"
    break;

  case 31: /* multi_instr: TOK_TEMPLATE_COMMAND total_full_block_def braced_instrlist  */
#line 605 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.parent_name = csh.GetCurrentPrefix(); //we dont have content: restore prefix to that of our surrounding context
    csh.CreateBlocksFromLast((yyvsp[-1].blockblocklist));
    csh.Templatize((yyvsp[-1].blockblocklist));
  #else
    chart.parent_style.Empty();
    chart.current_parent = chart.MyCurrentContext().parent; //we are done with content: restore prefix to that of our surrounding context
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent() && (yyvsp[-1].blockblocklist) && (yyvsp[-1].blockblocklist)->size())  {
		if ((yyvsp[-1].blockblocklist)->size()>1)
			chart.Error.Error((*std::next((yyvsp[-1].blockblocklist)->begin()))->file_pos.start,
			                  "Template definitions commands can only define one template at a time. Ignoring the rest.");
		(yyvsp[-1].blockblocklist)->front()->SetContent((yyvsp[0].instruction_list));
        delete (yyvsp[0].instruction_list);
        chart.CreateTemplate((yyvsp[-1].blockblocklist)->front().release());
		delete (yyvsp[-1].blockblocklist);
    } else  {
        delete (yyvsp[-1].blockblocklist);
        delete (yyvsp[0].instruction_list);
    }
    (yyval.instruction_list) = nullptr;
  #endif
    free((yyvsp[-2].str));
}
#line 4258 "block_csh_lang.cc"
    break;

  case 32: /* multi_instr_req_semicolon: arrow_needs_semi  */
#line 636 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddInstruction((yylsp[0]));
  #else
    (yyval.instruction_list) = chart.ExtractStashed();
	//If arrow_needs semi has a non-empty value (the list of the
	//last comma separaed arrow end strings) and NO stashed arrows,
	//it means we just had a list of comma separated arrow_end values
	//(unless we skip content), of course.
	if ((yyval.instruction_list)->size()==0 && (yyvsp[0].stringposlist_nocsh) && (yyvsp[0].stringposlist_nocsh)->size()>0 && !chart.SkipContent()) {
		delete (yyval.instruction_list);
		(yyval.instruction_list) = chart.UpdateBlock((yyvsp[0].stringposlist_nocsh), nullptr).release();
	}
    if ((yyvsp[0].stringposlist_nocsh)) delete (yyvsp[0].stringposlist_nocsh);
  #endif
}
#line 4279 "block_csh_lang.cc"
    break;

  case 33: /* multi_instr_req_semicolon: total_full_block_def  */
#line 653 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.parent_name = csh.GetCurrentPrefix(); //we dont have content: restore prefix to that of our surrounding context
    csh.CreateBlocksFromLast((yyvsp[0].blockblocklist)); //Here we probably have no content to copy, but need to add multi block names
  #else
    chart.parent_style.Empty();
    chart.current_parent = chart.MyCurrentContext().parent; //we are done with content: restore prefix to that of our surrounding context
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent() && (yyvsp[0].blockblocklist) && (yyvsp[0].blockblocklist)->size()) {
		for (auto &pBlock : *(yyvsp[0].blockblocklist))
			pBlock->SetContent(nullptr);
        (yyval.instruction_list) = new BlockInstrList;
        for (auto &pBlock : *(yyvsp[0].blockblocklist))
            (yyval.instruction_list)->Append(std::move(pBlock));
    } else 
        (yyval.instruction_list) = nullptr;
  #endif
    delete (yyvsp[0].blockblocklist);
}
#line 4303 "block_csh_lang.cc"
    break;

  case 35: /* multi_instr_req_semicolon: TOK_COMMAND_DEFSTYLE styledeflist  */
#line 674 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list) = (yyvsp[0].instruction_list);
  #endif
    free((yyvsp[-1].str));
}
#line 4323 "block_csh_lang.cc"
    break;

  case 36: /* multi_instr_req_semicolon: TOK_COMMAND_DEFSTYLE  */
#line 690 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing style name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
    (yyval.instruction_list) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 4345 "block_csh_lang.cc"
    break;

  case 37: /* multi_instr_req_semicolon: arrowend_list full_attrlist_with_label  */
#line 708 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.UpdateBlocks((yyvsp[-1].stringposlist));
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockBlock::AttributeNames(EBlockType::Box, csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues(EBlockType::Box, csh.hintAttrName, csh);
  #else
	if ((yyvsp[-1].stringposlist) && (yyvsp[0].attributelist) && !chart.SkipContent())
		(yyval.instruction_list) = chart.UpdateBlock((yyvsp[-1].stringposlist), (yyvsp[0].attributelist)).release();
	else
		(yyval.instruction_list) = nullptr;
	delete (yyvsp[0].attributelist);
  #endif
	delete (yyvsp[-1].stringposlist);
}
#line 4366 "block_csh_lang.cc"
    break;

  case 38: /* multi_instr_req_semicolon: alignment_modifiers arrowend_list  */
#line 725 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.UpdateBlocks((yyvsp[0].stringposlist));
  #else
    if ((yyvsp[0].stringposlist) && !chart.SkipContent())
        (yyval.instruction_list) = chart.UpdateBlock((yyvsp[0].stringposlist), nullptr, (yyvsp[-1].alignment_attr)).release();
    else
        (yyval.instruction_list) = nullptr;
    delete (yyvsp[-1].alignment_attr);
  #endif
    delete (yyvsp[0].stringposlist);
}
#line 4383 "block_csh_lang.cc"
    break;

  case 39: /* multi_instr_req_semicolon: alignment_modifiers arrowend_list full_attrlist_with_label  */
#line 738 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.UpdateBlocks((yyvsp[-1].stringposlist));
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockBlock::AttributeNames(EBlockType::Box, csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues(EBlockType::Box, csh.hintAttrName, csh);
  #else
  	if ((yyvsp[-1].stringposlist) && (yyvsp[0].attributelist) && !chart.SkipContent())
	    (yyval.instruction_list) = chart.UpdateBlock((yyvsp[-1].stringposlist), (yyvsp[0].attributelist), (yyvsp[-2].alignment_attr)).release();
	  else
		  (yyval.instruction_list) = nullptr;
    delete (yyvsp[-2].alignment_attr);
    delete (yyvsp[0].attributelist);
  #endif
    delete (yyvsp[-1].stringposlist);
}
#line 4405 "block_csh_lang.cc"
    break;

  case 40: /* complete_instr: instr_ending_brace  */
#line 759 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[0].instruction);
  #endif
}
#line 4422 "block_csh_lang.cc"
    break;

  case 41: /* complete_instr: instr_req_semicolon  */
#line 772 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[0].instruction);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 4437 "block_csh_lang.cc"
    break;

  case 42: /* complete_instr: instr_req_semicolon TOK_SEMICOLON  */
#line 783 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[-1].instruction);
  #endif
}
#line 4455 "block_csh_lang.cc"
    break;

  case 43: /* complete_instr: TOK_SEMICOLON  */
#line 797 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction)=nullptr;
  #endif
}
#line 4472 "block_csh_lang.cc"
    break;

  case 44: /* complete_instr: instr_req_semicolon error TOK_SEMICOLON  */
#line 810 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[-2].instruction);
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
  #endif
}
#line 4493 "block_csh_lang.cc"
    break;

  case 45: /* complete_instr: proc_invocation TOK_SEMICOLON  */
#line 827 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-1].cprocedure)) {
        auto ctx = ((yyvsp[-1].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[-1])), &chart);
        if (!ctx.first) {
            BlockPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-1].cprocedure))->text.c_str(), ((yyvsp[-1].cprocedure))->text.length(), &((yylsp[-1])), ((yyvsp[-1].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-1].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 4514 "block_csh_lang.cc"
    break;

  case 46: /* complete_instr: proc_invocation error TOK_SEMICOLON  */
#line 844 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-2].cprocedure)) {
        auto ctx = ((yyvsp[-2].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[-2])), &chart);
        if (!ctx.first) {
            BlockPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-2].cprocedure))->text.c_str(), ((yyvsp[-2].cprocedure))->text.length(), &((yylsp[-2])), ((yyvsp[-2].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-2].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 4542 "block_csh_lang.cc"
    break;

  case 47: /* complete_instr: proc_invocation  */
#line 868 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[0].cprocedure)) {
        auto ctx = ((yyvsp[0].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[0])), &chart);
        if (!ctx.first) {
            BlockPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[0].cprocedure))->text.c_str(), ((yyvsp[0].cprocedure))->text.length(), &((yylsp[0])), ((yyvsp[0].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[0].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 4565 "block_csh_lang.cc"
    break;

  case 48: /* complete_instr: proc_invocation proc_param_list TOK_SEMICOLON  */
#line 887 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-2].cprocedure) && (yyvsp[-1].procparaminvoclist)) {
        auto ctx = ((yyvsp[-2].cprocedure))->MatchParameters((yyvsp[-1].procparaminvoclist), CHART_POS((yylsp[-1])).end, &chart);
        if (!ctx.first) {
            BlockPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-2].cprocedure))->text.c_str(), ((yyvsp[-2].cprocedure))->text.length(), &((yylsp[-2])), ((yyvsp[-2].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-2].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else {
        delete (yyvsp[-1].procparaminvoclist);
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 4588 "block_csh_lang.cc"
    break;

  case 49: /* complete_instr: proc_invocation proc_param_list error TOK_SEMICOLON  */
#line 906 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-3].cprocedure) && (yyvsp[-2].procparaminvoclist)) {
        auto ctx = ((yyvsp[-3].cprocedure))->MatchParameters((yyvsp[-2].procparaminvoclist), CHART_POS((yylsp[-2])).end, &chart);
        if (!ctx.first) {
            BlockPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-3].cprocedure))->text.c_str(), ((yyvsp[-3].cprocedure))->text.length(), &((yylsp[-3])), ((yyvsp[-3].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-3].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else {
        delete (yyvsp[-2].procparaminvoclist);
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 4618 "block_csh_lang.cc"
    break;

  case 50: /* complete_instr: proc_invocation proc_param_list  */
#line 932 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-1].cprocedure) && (yyvsp[0].procparaminvoclist)) {
        auto ctx = ((yyvsp[-1].cprocedure))->MatchParameters((yyvsp[0].procparaminvoclist), CHART_POS((yylsp[0])).end, &chart);
        if (!ctx.first) {
            BlockPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-1].cprocedure))->text.c_str(), ((yyvsp[-1].cprocedure))->text.length(), &((yylsp[-1])), ((yyvsp[-1].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-1].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else
        delete (yyvsp[0].procparaminvoclist);
    (yyval.instruction) = nullptr;
  #endif
}
#line 4642 "block_csh_lang.cc"
    break;

  case 51: /* complete_instr: include TOK_SEMICOLON  */
#line 952 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-1].str)) {
        auto text = chart.Include((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
        if (text.first && text.first->length() && text.second.IsValid())
            BlockPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-1])), text.second, EInclusionReason::INCLUDE);
    }
    (yyval.instruction) = nullptr;
  #endif
    if ((yyvsp[-1].str)) free((yyvsp[-1].str));
}
#line 4660 "block_csh_lang.cc"
    break;

  case 52: /* complete_instr: include  */
#line 966 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    if ((yyvsp[0].str)) {
        auto text = chart.Include((yyvsp[0].str), CHART_POS_START((yylsp[0])));
        if (text.first && text.first->length() && text.second.IsValid())
            BlockPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[0])), text.second, EInclusionReason::INCLUDE);
    }
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    (yyval.instruction) = nullptr;
  #endif
    if ((yyvsp[0].str)) free((yyvsp[0].str));
}
#line 4680 "block_csh_lang.cc"
    break;

  case 53: /* complete_instr: include error TOK_SEMICOLON  */
#line 982 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-2].str)) {
        auto text = chart.Include((yyvsp[-2].str), CHART_POS_START((yylsp[-2])));
        if (text.first && text.first->length() && text.second.IsValid())
            BlockPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-2])), text.second, EInclusionReason::INCLUDE);
    }
    (yyval.instruction) = nullptr;
  #endif
    if ((yyvsp[-2].str)) free((yyvsp[-2].str));
}
#line 4705 "block_csh_lang.cc"
    break;

  case 54: /* include: TOK_COMMAND_INCLUDE  */
#line 1004 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a file name to include. You must use quotation marks ('\"').");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints();
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a file name to include. You must use quotation marks ('\"').");
  #endif
    (yyval.str) = nullptr;
    free((yyvsp[0].str));
}
#line 4722 "block_csh_lang.cc"
    break;

  case 55: /* include: TOK_COMMAND_INCLUDE TOK_QSTRING  */
#line 1017 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_INCLUDEFILE);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints();
    else if(csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints((yyvsp[0].str), (yylsp[0]));
  #endif
    (yyval.str) = (yyvsp[0].str);
    free((yyvsp[-1].str));
}
#line 4739 "block_csh_lang.cc"
    break;

  case 56: /* instr_req_semicolon: TOK_COMMAND_DEFCOLOR colordeflist_actioned  */
#line 1032 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 4759 "block_csh_lang.cc"
    break;

  case 57: /* instr_req_semicolon: TOK_COMMAND_DEFCOLOR  */
#line 1048 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 4781 "block_csh_lang.cc"
    break;

  case 58: /* instr_req_semicolon: usedesign  */
#line 1066 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.instruction) = nullptr;
  #endif
}
#line 4791 "block_csh_lang.cc"
    break;

  case 59: /* instr_req_semicolon: usestyle  */
#line 1072 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if (!chart.SkipContent())
        chart.AddAttributeListToRunningStyle((yyvsp[0].usecommandhelper).attrs, (yyvsp[0].usecommandhelper).keywords);
    else
        delete (yyvsp[0].usecommandhelper).attrs;
    (yyval.instruction) = nullptr;
  #endif
}
#line 4805 "block_csh_lang.cc"
    break;

  case 61: /* instr_req_semicolon: set  */
#line 1083 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.instruction) = nullptr;
  #endif
}
#line 4816 "block_csh_lang.cc"
    break;

  case 62: /* instr_req_semicolon: TOK_TEMPLATE_COMMAND total_full_block_def  */
#line 1090 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.parent_name = csh.GetCurrentPrefix(); //we dont have content: restore prefix to that of our surrounding context
    csh.CreateBlocksFromLast((yyvsp[0].blockblocklist));
    csh.Templatize((yyvsp[0].blockblocklist));
  #else
    chart.parent_style.Empty();
    chart.current_parent = chart.MyCurrentContext().parent; //we are done with content: restore prefix to that of our surrounding context
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent() && (yyvsp[0].blockblocklist) && (yyvsp[0].blockblocklist)->size())  {
		if ((yyvsp[0].blockblocklist)->size()>1)
			chart.Error.Error((*std::next((yyvsp[0].blockblocklist)->begin()))->file_pos.start,
			                  "Template definitions commands can only define one template at a time. Ignoring the rest.");
        (yyvsp[0].blockblocklist)->front()->SetContent(nullptr);
        chart.CreateTemplate((yyvsp[0].blockblocklist)->front().release());
    }
    delete (yyvsp[0].blockblocklist);
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 4843 "block_csh_lang.cc"
    break;

  case 63: /* instr_req_semicolon: TOK_TEMPLATE_COMMAND  */
#line 1113 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD))
        csh.AddKeywordsToHints(true, false, true);
  #else
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 4858 "block_csh_lang.cc"
    break;

  case 64: /* instr_req_semicolon: TOK_TEMPLATE_COMMAND copy_attr  */
#line 1124 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.Templatize((yyvsp[0].copystruct), (yylsp[0]));
    free((yyvsp[0].copystruct));
  #else
    _ASSERT((yyvsp[0].copystruct)); //copy_header should never return null
    //Take care not to make changes to the chart if we are storing a procedure
    if (chart.SkipContent())
        (yyvsp[0].copystruct)->Free();
    else
        chart.CreateTemplate(chart.CreateCopy((yyvsp[0].copystruct)));
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 4879 "block_csh_lang.cc"
    break;

  case 65: /* instr_req_semicolon: copy_attr  */
#line 1141 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    free((yyvsp[0].copystruct));
  #else
    _ASSERT((yyvsp[0].copystruct)); //copy_header should never return null
    //Take care not to make changes to the chart if we are storing a procedure
    if (chart.SkipContent()) {
        (yyvsp[0].copystruct)->Free();
        (yyval.instruction) = nullptr;
    } else
        (yyval.instruction) = chart.CreateCopy((yyvsp[0].copystruct));
  #endif
}
#line 4897 "block_csh_lang.cc"
    break;

  case 66: /* instr_req_semicolon: arrow_label  */
#line 1155 "block_lang.yy"
{
  (yyval.instruction) = (yyvsp[0].arrowlabel);
}
#line 4905 "block_csh_lang.cc"
    break;

  case 67: /* instr_req_semicolon: arrow_end full_attrlist_with_label  */
#line 1159 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
	    csh.UpdateCSH_ArrowEndAtLineBegin((yylsp[-1]), (yyvsp[-1].multi_str).str);
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockBlock::AttributeNames(EBlockType::Box, csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues(EBlockType::Box, csh.hintAttrName, csh);
  #else
	if (!(yyvsp[-1].multi_str).had_error && (yyvsp[0].attributelist) && !chart.SkipContent())
		(yyval.instruction) = chart.UpdateBlock((yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), (yyvsp[0].attributelist)).release();
	else
		(yyval.instruction) = nullptr;
	delete (yyvsp[0].attributelist);
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 4930 "block_csh_lang.cc"
    break;

  case 68: /* instr_req_semicolon: arrow_end  */
#line 1180 "block_lang.yy"
{
    /* This is where we handle an unrecognized/partial keyword alone at the beginning of a line.*/
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
	    csh.UpdateCSH_ArrowEndAtLineBegin((yylsp[0]), (yyvsp[0].multi_str).str);
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
	if (!(yyvsp[0].multi_str).had_error && !chart.SkipContent())
		(yyval.instruction) = chart.UpdateBlock((yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), nullptr).release();
	else
		(yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 4952 "block_csh_lang.cc"
    break;

  case 69: /* instr_req_semicolon: alignment_modifiers arrow_end full_attrlist_with_label  */
#line 1198 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-2])))
        csh.hintStatus = HINT_READY;
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockBlock::AttributeNames(EBlockType::Box, csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues(EBlockType::Box, csh.hintAttrName, csh);
  #else
    if (!(yyvsp[-1].multi_str).had_error && (yyvsp[0].attributelist) && !chart.SkipContent())
        (yyval.instruction) = chart.UpdateBlock((yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1])), (yyvsp[0].attributelist), (yyvsp[-2].alignment_attr)).release();
    else
        (yyval.instruction) = nullptr;
    delete (yyvsp[-2].alignment_attr);
    delete (yyvsp[0].attributelist);
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 4975 "block_csh_lang.cc"
    break;

  case 70: /* instr_req_semicolon: alignment_modifiers arrow_end  */
#line 1217 "block_lang.yy"
{
    /* This is where we handle an unrecognized/partial keyword alone at the beginning of a line.*/
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.hintStatus = HINT_READY;
    }
  #else
	if (!(yyvsp[0].multi_str).had_error && !chart.SkipContent())
		(yyval.instruction) = chart.UpdateBlock((yyvsp[0].multi_str).str, CHART_POS((yylsp[0])), nullptr, (yyvsp[-1].alignment_attr)).release();
	else
		(yyval.instruction) = nullptr;
    delete (yyvsp[-1].alignment_attr);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 4995 "block_csh_lang.cc"
    break;

  case 71: /* instr_ending_brace: TOK_COMMAND_DEFSHAPE shapedef  */
#line 1235 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error((yylsp[-1]), "Cannot define shapes as part of a procedure.");
    else
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define shapes as part of a procedure.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 5017 "block_csh_lang.cc"
    break;

  case 72: /* instr_ending_brace: TOK_COMMAND_DEFSHAPE  */
#line 1253 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error((yylsp[0]), "Cannot define shapes as part of a procedure.");
    else
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing shape name and definition.");
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Cannot define shapes as part of a procedure.");
    else
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing shape name and definition.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 5042 "block_csh_lang.cc"
    break;

  case 73: /* instr_ending_brace: TOK_COMMAND_DEFDESIGN designdef  */
#line 1274 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error((yylsp[-1]), "Cannot define designs inside a procedure.");
    else
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define designs inside a procedure.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 5064 "block_csh_lang.cc"
    break;

  case 74: /* instr_ending_brace: TOK_COMMAND_DEFDESIGN  */
#line 1292 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error((yylsp[0]), "Cannot define designs inside a procedure.");
    else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing design name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Cannot define designs inside a procedure.");
    else
        chart.Error.Error(CHART_POS((yyloc)).end, "Missing a design name to (re-)define.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 5090 "block_csh_lang.cc"
    break;

  case 75: /* instr_ending_brace: defproc  */
#line 1314 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.instruction) = nullptr;
  #endif
}
#line 5100 "block_csh_lang.cc"
    break;

  case 76: /* instr_ending_brace: copy_attr braced_clone_action_list  */
#line 1320 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    free((yyvsp[-1].copystruct));
  #else
    _ASSERT((yyvsp[-1].copystruct)); //copy_header should never return null
    (yyvsp[-1].copystruct)->modifiers = (yyvsp[0].cloneactionlist);
    //Take care not to make changes to the chart if we are storing a procedure
    if (chart.SkipContent()) {
        (yyvsp[-1].copystruct)->Free();
        (yyval.instruction) = nullptr;
    } else
        (yyval.instruction) = chart.CreateCopy((yyvsp[-1].copystruct));
  #endif
}
#line 5119 "block_csh_lang.cc"
    break;

  case 77: /* instr_ending_brace: TOK_TEMPLATE_COMMAND copy_attr braced_clone_action_list  */
#line 1335 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.Templatize((yyvsp[-1].copystruct), (yylsp[-1]));
    free((yyvsp[-1].copystruct));
  #else
    _ASSERT((yyvsp[-1].copystruct)); //copy_header should never return null
    (yyvsp[-1].copystruct)->modifiers = (yyvsp[0].cloneactionlist);
    //Take care not to make changes to the chart if we are storing a procedure
    if (chart.SkipContent())
        (yyvsp[-1].copystruct)->Free();
    else
        chart.CreateTemplate(chart.CreateCopy((yyvsp[-1].copystruct)));
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-2].str));
}
#line 5141 "block_csh_lang.cc"
    break;

  case 78: /* braced_clone_action_list: scope_open clone_action_list scope_close  */
#line 1354 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    if (csh.IsCursorAtLineBegin() && (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD) ||
        csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD))) {
        csh.ClearHints(); //clone action list may end in an instruction after which we get the regular line begin hints.
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = (yyvsp[-1].cloneactionlist);
    delete (yyvsp[0].instruction_list);
  #endif
}
#line 5160 "block_csh_lang.cc"
    break;

  case 79: /* braced_clone_action_list: scope_open scope_close  */
#line 1369 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    if (csh.IsCursorAtLineBegin() && csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = nullptr;
    delete (yyvsp[0].instruction_list);
  #endif
}
#line 5177 "block_csh_lang.cc"
    break;

  case 80: /* braced_clone_action_list: scope_open clone_action_list error scope_close  */
#line 1382 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
    if (csh.IsCursorAtLineBegin() && (csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::KEYWORD) ||
        csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD))) {
        csh.ClearHints(); //clone action list may end in an instruction after which we get the regular line begin hints.
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = (yyvsp[-2].cloneactionlist);
    delete (yyvsp[0].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
    yyerrok;
}
#line 5199 "block_csh_lang.cc"
    break;

  case 81: /* braced_clone_action_list: scope_open clone_action_list error TOK_EOF  */
#line 1400 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
    csh.PopContext();
    if (csh.IsCursorAtLineBegin() && (csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::KEYWORD) ||
        csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD))) {
        csh.ClearHints(); //clone action list may end in an instruction after which we get the regular line begin hints.
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = (yyvsp[-2].cloneactionlist);
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
}
#line 5221 "block_csh_lang.cc"
    break;

  case 82: /* braced_clone_action_list: scope_open clone_action_list TOK_EOF  */
#line 1418 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
    if (csh.IsCursorAtLineBegin() && (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD) ||
        csh.CheckHintAfter((yylsp[-1]), EHintSourceType::KEYWORD))) {
        csh.ClearHints(); //clone action list may end in an instruction after which we get the regular line begin hints.
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = (yyvsp[-1].cloneactionlist);
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
}
#line 5244 "block_csh_lang.cc"
    break;

  case 83: /* braced_clone_action_list: scope_open TOK_EOF  */
#line 1437 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
    if (csh.IsCursorAtLineBegin() && csh.CheckHintAfter((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = nullptr;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
}
#line 5264 "block_csh_lang.cc"
    break;

  case 84: /* braced_clone_action_list: scope_open clone_action_list TOK_BYE  */
#line 1453 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
    csh.PopContext();
    if (csh.IsCursorAtLineBegin() && (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD) ||
        csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD))) {
        csh.ClearHints(); //clone action list may end in an instruction after which we get the regular line begin hints.
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = (yyvsp[-1].cloneactionlist);
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
  #endif
  free((yyvsp[0].str));
}
#line 5288 "block_csh_lang.cc"
    break;

  case 85: /* braced_clone_action_list: scope_open TOK_BYE  */
#line 1473 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
    if (csh.IsCursorAtLineBegin() && csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = nullptr;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
  free((yyvsp[0].str));
}
#line 5309 "block_csh_lang.cc"
    break;

  case 86: /* clone_action_list: clone_action  */
#line 1491 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddUseKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneactionlist) = new CloneActionList;
    (yyval.cloneactionlist)->Append((yyvsp[0].cloneaction));
  #endif
}
#line 5325 "block_csh_lang.cc"
    break;

  case 87: /* clone_action_list: clone_action_list clone_action  */
#line 1503 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddUseKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyvsp[-1].cloneactionlist)->Append((yyvsp[0].cloneaction));
    (yyval.cloneactionlist) = (yyvsp[-1].cloneactionlist);
  #endif
}
#line 5341 "block_csh_lang.cc"
    break;

  case 88: /* clone_action: clone_action_no_semicolon  */
#line 1516 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
  #else
    (yyval.cloneaction) = (yyvsp[0].cloneaction);
  #endif
}
#line 5353 "block_csh_lang.cc"
    break;

  case 89: /* clone_action: clone_action_req_semicolon TOK_SEMICOLON  */
#line 1524 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddInstruction((yyloc));
  #else
    (yyval.cloneaction) = (yyvsp[-1].cloneaction);
  #endif
}
#line 5366 "block_csh_lang.cc"
    break;

  case 90: /* clone_action: clone_action_req_semicolon error TOK_SEMICOLON  */
#line 1533 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[0]), "Syntax error.");
    csh.AddInstruction((yyloc));
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Syntax error.");
    (yyval.cloneaction) = (yyvsp[-2].cloneaction);
  #endif
}
#line 5381 "block_csh_lang.cc"
    break;

  case 91: /* clone_action: clone_action_req_semicolon  */
#line 1544 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
    csh.AddInstruction((yyloc));
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing semicolon (';').");
    (yyval.cloneaction) = (yyvsp[0].cloneaction);
  #endif
}
#line 5395 "block_csh_lang.cc"
    break;

  case 92: /* thing_to_update: entity_string  */
#line 1555 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error) {
        if (CaseInsensitiveEqual("all", (yyvsp[0].multi_str).str))
            csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        else
            csh.AddCSH_LocalBlockName((yylsp[0]), (yyvsp[0].multi_str).str);
    }
  #endif
    if ((yyvsp[0].multi_str).had_error) {
        free((yyvsp[0].multi_str).str);
        (yyval.str) = nullptr;
    } else {
        (yyval.str) = (yyvsp[0].multi_str).str;
    }
}
#line 5416 "block_csh_lang.cc"
    break;

  case 93: /* thing_to_update: TOK_COMMAND_BLOCKS  */
#line 1572 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 5427 "block_csh_lang.cc"
    break;

  case 94: /* thing_to_update: TOK_COMMAND_ARROWS  */
#line 1579 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 5438 "block_csh_lang.cc"
    break;

  case 95: /* things_to_update: thing_to_update  */
#line 1587 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD))
        csh.AddWhatToUpdateToHints(true);
    (yyval.stringposlist) = new CshStringWithPosList;
    if ((yyvsp[0].str))
        (yyval.stringposlist)->push_back({(yyvsp[0].str), (yylsp[0])});
  #else
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[0].str))
        (yyval.stringposlist)->push_back({(yyvsp[0].str), CHART_POS((yylsp[0]))});
  #endif
    free((yyvsp[0].str));
}
#line 5457 "block_csh_lang.cc"
    break;

  case 96: /* things_to_update: things_to_update TOK_COMMA  */
#line 1602 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing, a block name, 'blocks', 'arrows' or 'all'.");
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD))
        csh.AddWhatToUpdateToHints(true);
    else if(csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD))
        csh.AddWhatToUpdateToHints(false);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing, a block name, 'blocks', 'arrows' or 'all'.");
  #endif
    (yyval.stringposlist) = (yyvsp[-1].stringposlist);
}
#line 5475 "block_csh_lang.cc"
    break;

  case 97: /* things_to_update: things_to_update TOK_COMMA thing_to_update  */
#line 1616 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD))
        csh.AddWhatToUpdateToHints(true);
    else if(csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD))
        csh.AddWhatToUpdateToHints(false);
    if ((yyvsp[0].str))
        (yyvsp[-2].stringposlist)->push_back({(yyvsp[0].str), (yylsp[0])});
  #else
    if ((yyvsp[0].str))
        (yyvsp[-2].stringposlist)->push_back({(yyvsp[0].str), CHART_POS((yylsp[0]))});
  #endif
    (yyval.stringposlist) = (yyvsp[-2].stringposlist);
    free((yyvsp[0].str));
}
#line 5496 "block_csh_lang.cc"
    break;

  case 99: /* opt_things_to_update: %empty  */
#line 1634 "block_lang.yy"
                                                   {(yyval.stringposlist) = nullptr;}
#line 5502 "block_csh_lang.cc"
    break;

  case 100: /* clone_action_req_semicolon: entity_string  */
#line 1638 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAt((yylsp[0]),  EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else {
        csh.AddCSH_Error((yylsp[0]), "Expecting 'add', 'drop', 'move', 'update', 'replace' or 'use', 'blocks use', 'arrows use'.");
    }
  #else
    (yyval.cloneaction) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting 'add', 'drop', 'move', 'update', 'replace' or 'use', 'blocks use', 'arrows use'. Ignoring this line.");
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 5521 "block_csh_lang.cc"
    break;

  case 101: /* clone_action_req_semicolon: usestyle  */
#line 1653 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    //update running style as normal
    chart.AddAttributeListToRunningStyle((yyvsp[0].usecommandhelper).attrs, (yyvsp[0].usecommandhelper).keywords);
    (yyval.cloneaction) = nullptr;
  #endif
}
#line 5533 "block_csh_lang.cc"
    break;

  case 102: /* clone_action_req_semicolon: TOK_CLONE_MODIFIER_RECURSIVE  */
#line 1661 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "update",
                               "Update attributes of all blocks, all arrows or all elements recursively.",
                               EHintType::KEYWORD));
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "Expecting 'update'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting 'update'.");
    (yyval.cloneaction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 5554 "block_csh_lang.cc"
    break;

  case 103: /* clone_action_req_semicolon: TOK_CLONE_MODIFIER_RECURSIVE entity_string  */
#line 1678 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "update",
                               "Update attributes of all blocks, all arrows or all elements recursively.",
                               EHintType::KEYWORD));
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "Expecting 'update'.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting 'update'.");
    (yyval.cloneaction) = nullptr;
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 5576 "block_csh_lang.cc"
    break;

  case 104: /* clone_action_req_semicolon: TOK_CLONE_MODIFIER_RECURSIVE TOK_CLONE_MODIFIER_UPDATE opt_things_to_update  */
#line 1696 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if ((yyvsp[0].stringposlist)==nullptr)
        csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing what to update.");
    else
        csh.AddCSH_ErrorAfter((yyloc), "Expecting an attribute list.");
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY) || csh.CheckHintAfter((yyloc), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].str))
        chart.Error.Error(CHART_POS_AFTER((yyloc)), "Expecting an attribute list.");
    else
        chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing what to update ('all', 'blocks', 'arrows' or a block name). Ignoring update attempt.");
    (yyval.cloneaction) = nullptr;
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
    delete (yyvsp[0].stringposlist);
}
#line 5604 "block_csh_lang.cc"
    break;

  case 105: /* clone_action_req_semicolon: TOK_CLONE_MODIFIER_UPDATE opt_things_to_update  */
#line 1720 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if ((yyvsp[0].stringposlist)==nullptr)
        csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing what to update.");
    else
        csh.AddCSH_ErrorAfter((yyloc), "Expecting an attribute list.");
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY) || csh.CheckHintAfter((yyloc), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[0].stringposlist))
        chart.Error.Error(CHART_POS_AFTER((yyloc)), "Expecting an attribute list.");
    else
        chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing what to update ('all', 'blocks', 'arrows' or a block name). Ignoring update attempt.");
    (yyval.cloneaction) = nullptr;
  #endif
    free((yyvsp[-1].str));
    delete (yyvsp[0].stringposlist);
}
#line 5630 "block_csh_lang.cc"
    break;

  case 106: /* clone_action_req_semicolon: TOK_CLONE_MODIFIER_RECURSIVE TOK_CLONE_MODIFIER_UPDATE opt_things_to_update full_attrlist_with_label  */
#line 1742 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if ((yyvsp[-1].stringposlist)==nullptr)
        csh.AddCSH_ErrorAfter((yylsp[-2]), "Missing what to update.");
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(true);
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    if ((yyvsp[-1].stringposlist)) {
        (yyval.cloneaction) = new CloneAction(CHART_POS2((yylsp[-3]), (yylsp[-2])), (yyvsp[-1].stringposlist), CHART_POS((yylsp[-1])), (yyvsp[0].attributelist), nullptr);
        (yyval.cloneaction)->recursive = true;
    } else {
        chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing what to update ('all', 'blocks', 'arrows' or a block name). Ignoring update attempt.");
        (yyval.cloneaction) = nullptr;
    }
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-2].str));
}
#line 5658 "block_csh_lang.cc"
    break;

  case 107: /* clone_action_req_semicolon: TOK_CLONE_MODIFIER_UPDATE opt_things_to_update full_attrlist_with_label  */
#line 1766 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if ((yyvsp[-1].stringposlist)==nullptr)
        csh.AddCSH_ErrorAfter((yylsp[-2]), "Missing what to update.");
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(true);
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    if ((yyvsp[-1].stringposlist))
        (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-2])), (yyvsp[-1].stringposlist), CHART_POS((yylsp[-1])), (yyvsp[0].attributelist), nullptr);
    else {
        chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing what to update ('all', 'blocks', 'arrows' or a block name). Ignoring update attempt.");
        (yyval.cloneaction) = nullptr;
    }
  #endif
    free((yyvsp[-2].str));
}
#line 5683 "block_csh_lang.cc"
    break;

  case 108: /* clone_action_req_semicolon: TOK_CLONE_MODIFIER_DROP opt_things_to_update  */
#line 1787 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY) || csh.CheckHintAfter((yyloc), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(true);
        csh.hintStatus = HINT_READY;
    }
    if ((yyvsp[0].stringposlist)) 
        for (auto &[name, file_pos] : *(yyvsp[0].stringposlist))
            csh.DeleteBlock(name);
    delete (yyvsp[0].stringposlist);
  #else
    (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-1])), (yyvsp[0].stringposlist));
  #endif
    free((yyvsp[-1].str));
}
#line 5707 "block_csh_lang.cc"
    break;

  case 109: /* clone_action_req_semicolon: TOK_CLONE_MODIFIER_MOVE opt_things_to_update opt_before  */
#line 1807 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-2]),  EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddBeforeToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[0]), EHintSourceType::ENTITY) || csh.CheckHintAfter((yylsp[-2]), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(true);
        csh.hintStatus = HINT_READY;
    }   
    if ((yyvsp[-1].stringposlist)) 
        for (auto &[name, file_pos] : *(yyvsp[-1].stringposlist))
            csh.DeleteBlock(name);
    delete (yyvsp[-1].stringposlist);
  #else
    (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-2])), (yyvsp[-1].stringposlist), (yyvsp[0].str) ? (yyvsp[0].str) : "", CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].str));
    if ((yyvsp[0].str)) free((yyvsp[0].str));
}
#line 5735 "block_csh_lang.cc"
    break;

  case 110: /* clone_modifier_replace_with_name: TOK_CLONE_MODIFIER_REPLACE entity_string  */
#line 1831 "block_lang.yy"
                                                                           {
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_LocalBlockName((yylsp[0]), (yyvsp[0].multi_str).str);
    if (csh.CheckHintAt((yylsp[-1]),  EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(false);
        csh.hintStatus = HINT_READY;
    }
    csh.DeleteBlock((yyvsp[0].multi_str).str);
    (yyval.stringposlist) = new CshStringWithPosList{ {(yyvsp[0].multi_str).str, (yylsp[0])} };
  #else
    (yyval.stringposlist) = new StringWithPosList{ {(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))} };
  #endif
    free((yyvsp[-1].str));
}
#line 5758 "block_csh_lang.cc"
    break;

  case 111: /* clone_modifier_replace_with_name: TOK_CLONE_MODIFIER_REPLACE  */
#line 1849 "block_lang.yy"
                                          {
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(false);
        csh.hintStatus = HINT_READY;
    }
    (yyval.stringposlist) = new CshStringWithPosList;
  #else
    (yyval.stringposlist) = new StringWithPosList;
  #endif
    free((yyvsp[0].str));
}
#line 5776 "block_csh_lang.cc"
    break;

  case 112: /* clone_action_no_semicolon: TOK_CLONE_MODIFIER_UPDATE opt_things_to_update full_attrlist_with_label braced_clone_action_list  */
#line 1864 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-3]),  EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-3]), (yylsp[-1]), EHintSourceType::KEYWORD))
        csh.AddWhatToUpdateToHints(true);
    delete (yyvsp[-2].stringposlist);
  #else
    if ((yyvsp[-2].stringposlist)==nullptr) {
        chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing what to update (a block name). Ignoring update attempt.");
        (yyval.cloneaction) = nullptr;
    } else
        (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-3])), (yyvsp[-2].stringposlist), CHART_POS((yylsp[-2])), (yyvsp[-1].attributelist), (yyvsp[0].cloneactionlist));
  #endif
    free((yyvsp[-3].str));
}
#line 5799 "block_csh_lang.cc"
    break;

  case 113: /* clone_action_no_semicolon: TOK_CLONE_MODIFIER_UPDATE opt_things_to_update braced_clone_action_list  */
#line 1883 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-2]),  EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[0]), EHintSourceType::KEYWORD))
        csh.AddWhatToUpdateToHints(true);
    delete (yyvsp[-1].stringposlist);
  #else
    if ((yyvsp[-1].stringposlist)==nullptr) {
        chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing what to update (a block name). Ignoring update attempt.");
        (yyval.cloneaction) = nullptr;
    } else
        (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-2])), (yyvsp[-1].stringposlist), CHART_POS((yylsp[-1])), nullptr, (yyvsp[0].cloneactionlist));
  #endif
    free((yyvsp[-2].str));
}
#line 5822 "block_csh_lang.cc"
    break;

  case 114: /* clone_action_no_semicolon: TOK_CLONE_MODIFIER_ADD opt_before complete_instr  */
#line 1902 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-2]),  EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddBeforeToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-2])), (yyvsp[0].instruction), (yyvsp[-1].str) ? (yyvsp[-1].str) : "", CHART_POS((yylsp[-1])));
  #endif
    free((yyvsp[-2].str));
    if ((yyvsp[-1].str)) free((yyvsp[-1].str));
}
#line 5843 "block_csh_lang.cc"
    break;

  case 115: /* clone_action_no_semicolon: TOK_CLONE_MODIFIER_ADD opt_before multi_instr  */
#line 1919 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-2]),  EHintSourceType::KEYWORD)) {
        csh.AddCloneActionKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddBeforeToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-2])), (yyvsp[0].instruction_list), (yyvsp[-1].str) ? (yyvsp[-1].str) : "", CHART_POS((yylsp[-1])));
  #endif
    free((yyvsp[-2].str));
    if ((yyvsp[-1].str)) free((yyvsp[-1].str));
}
#line 5864 "block_csh_lang.cc"
    break;

  case 116: /* clone_action_no_semicolon: clone_modifier_replace_with_name complete_instr  */
#line 1936 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[-1].stringposlist) && (yyvsp[-1].stringposlist)->size())
        (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-1])), (yyvsp[-1].stringposlist)->front().name, (yyvsp[-1].stringposlist)->front().file_pos, (yyvsp[0].instruction));
    else
        (yyval.cloneaction) = nullptr;
  #endif
    delete (yyvsp[-1].stringposlist);
}
#line 5879 "block_csh_lang.cc"
    break;

  case 117: /* clone_action_no_semicolon: clone_modifier_replace_with_name multi_instr  */
#line 1947 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[-1].stringposlist) && (yyvsp[-1].stringposlist)->size())
        (yyval.cloneaction) = new CloneAction(CHART_POS((yylsp[-1])), (yyvsp[-1].stringposlist)->front().name, (yyvsp[-1].stringposlist)->front().file_pos, (yyvsp[0].instruction_list));
    else
        (yyval.cloneaction) = nullptr;
  #endif
    delete (yyvsp[-1].stringposlist);
}
#line 5894 "block_csh_lang.cc"
    break;

  case 118: /* opt_before: %empty  */
#line 1960 "block_lang.yy"
{
    (yyval.str) = nullptr;
}
#line 5902 "block_csh_lang.cc"
    break;

  case 119: /* opt_before: entity_string  */
#line 1964 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAt((yylsp[0]),  EHintSourceType::KEYWORD)) {
        csh.AddBeforeToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.str) = nullptr;
    free((yyvsp[0].multi_str).str);
}
#line 5917 "block_csh_lang.cc"
    break;

  case 120: /* opt_before: TOK_CLONE_MODIFIER_BEFORE  */
#line 1975 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing block name.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddWhatToUpdateToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing block name. Ignoring 'before' clause.");
  #endif
    (yyval.str) = nullptr;
    free((yyvsp[0].str));
}
#line 5936 "block_csh_lang.cc"
    break;

  case 121: /* opt_before: TOK_CLONE_MODIFIER_BEFORE entity_string  */
#line 1990 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_LocalBlockName((yylsp[0]), (yyvsp[0].multi_str).str);
  #else
  #endif
    (yyval.str) = (yyvsp[0].multi_str).str;
    free((yyvsp[-1].str));
}
#line 5950 "block_csh_lang.cc"
    break;

  case 124: /* full_block_def_enclose: TOK_AROUND_COMMAND  */
#line 2009 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing block definition.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, true, true);
        csh.AddEntityNamesAtTheEnd("Enclose block %s in another block.");
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing block name. Ignoring this.");
  #endif
    (yyval.blockblocklist) = nullptr;
    free((yyvsp[0].str));
}
#line 5970 "block_csh_lang.cc"
    break;

  case 125: /* full_block_def_enclose: TOK_AROUND_COMMAND blocknames_plus  */
#line 2025 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_NameList((yyvsp[0].stringposlist), (yylsp[0]), COLOR_ENTITYNAME, COLOR_COMMA);
    csh.AddCSH_Error((yylsp[0]), "Missing block definition.");
    if (csh.CheckHintLocated((yylsp[0]))) {
        csh.AddEntityNamesAtTheEnd("Enclose block %s in another block.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, true, true);
        csh.AddEntityNamesAtTheEnd("Enclose block %s in another block.");
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing block definition. Ignoring this.");
  #endif
    (yyval.blockblocklist) = nullptr;
    free((yyvsp[-1].str));
    delete (yyvsp[0].stringposlist);
}
#line 5995 "block_csh_lang.cc"
    break;

  case 126: /* full_block_def_enclose: TOK_AROUND_COMMAND blocknames_plus block_def  */
#line 2046 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_NameList((yyvsp[-1].stringposlist), (yylsp[-1]), COLOR_ENTITYNAME, COLOR_COMMA);
    csh.parent_name = csh.GetCurrentPrefix(); //we dont have content: restore prefix to that of our surrounding context
    if (csh.CheckHintLocated((yylsp[-1]))) {
        csh.AddEntityNamesAtTheEnd("Enclose block %s in another block.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, true, true);
        csh.AddEntityNamesAtTheEnd("Enclose block %s in another block.");
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[-1].stringposlist);
	if ((yyvsp[0].blockblocklist) && (yyvsp[0].blockblocklist)->size()>1)
		csh.AddCSH_Error(std::next((yyvsp[0].blockblocklist)->begin())->file_pos,
			             "Around commands can only define one block.");
    (yyval.blockblocklist) = (yyvsp[0].blockblocklist);
  #else
    if ((yyvsp[0].blockblocklist) && !chart.SkipContent()) {
		if ((yyvsp[0].blockblocklist)->size()>1) {
			chart.Error.Error((*std::next((yyvsp[0].blockblocklist)->begin()))->file_pos.start,
			                  "Around commands can only define one block. Ignoring the rest.");
			(yyvsp[0].blockblocklist)->erase(std::next((yyvsp[0].blockblocklist)->begin()), (yyvsp[0].blockblocklist)->end());
		}
		if ((yyvsp[0].blockblocklist)->size())
			(yyvsp[0].blockblocklist)->front()->AddAround((yyvsp[-1].stringposlist), CHART_POS_START((yylsp[-1])));
        (yyval.blockblocklist) = (yyvsp[0].blockblocklist);
    } else {
        (yyval.blockblocklist) = nullptr;
        delete (yyvsp[-1].stringposlist);
        delete (yyvsp[0].blockblocklist);
    }
  #endif
    free((yyvsp[-2].str));
}
#line 6036 "block_csh_lang.cc"
    break;

  case 127: /* full_block_def_enclose: TOK_AROUND_COMMAND block_def  */
#line 2083 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.parent_name = csh.GetCurrentPrefix(); //we dont have content: restore prefix to that of our surrounding context
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing block name to be around.");
    if (csh.CheckHintAfter((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, true, true);
        csh.AddEntityNamesAtTheEnd("Enclose block %s in another block.");
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing block name to be around. Ignoring 'around' clause.");
  #endif
    (yyval.blockblocklist) = (yyvsp[0].blockblocklist);
    free((yyvsp[-1].str));
}
#line 6057 "block_csh_lang.cc"
    break;

  case 128: /* alignment_modifiers: TOK_ALIGN_MODIFIER  */
#line 2102 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.alignment_attr) = BlockBlock::TranslatePre(chart, (yyvsp[0].alignmodifier), CHART_POS_START((yylsp[0])));
  #endif
}
#line 6073 "block_csh_lang.cc"
    break;

  case 129: /* alignment_modifiers: TOK_ALIGN_MODIFIER TOK_ALIGN_MODIFIER  */
#line 2114 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Position the block under definition in relation to block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, false, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.alignment_attr) = BlockBlock::TranslatePre(chart, (yyvsp[-1].alignmodifier), (yyvsp[0].alignmodifier), CHART_POS_START((yylsp[-1])), CHART_POS_START((yylsp[0])));
  #endif
}
#line 6096 "block_csh_lang.cc"
    break;

  case 130: /* alignment_modifiers: TOK_ALIGN_MODIFIER blocknames_plus_number_or_number  */
#line 2133 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_NameList((yyvsp[0].stringposlist), (yylsp[0]), COLOR_ENTITYNAME, COLOR_COMMA, COLOR_ATTRVALUE, COLOR_ENTITYNAME);
    if (csh.CheckHintLocated((yylsp[0]))) {
        csh.HandleBlockNamePlus("modifier");
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, false, true);
        csh.AddEntityNamesAtTheEnd("Position the block under definition in relation to block %s.");
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[0].stringposlist);
  #else
    (yyval.alignment_attr) = BlockBlock::TranslatePre(chart, (yyvsp[-1].alignmodifier), CHART_POS_START((yylsp[-1])), (yyvsp[0].stringposlist));
  #endif
}
#line 6120 "block_csh_lang.cc"
    break;

  case 131: /* alignment_modifiers: TOK_ALIGN_MODIFIER coord_hinted  */
#line 2153 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.alignment_attr) = BlockBlock::TranslatePre(chart, (yyvsp[-1].alignmodifier), CHART_POS_START((yylsp[-1])), (yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
  #endif
  free((yyvsp[0].multi_str).str);
}
#line 6137 "block_csh_lang.cc"
    break;

  case 132: /* alignment_modifiers: TOK_ALIGN_MODIFIER TOK_ALIGN_MODIFIER blocknames_plus_number_or_number  */
#line 2166 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_NameList((yyvsp[0].stringposlist), (yylsp[0]), COLOR_ENTITYNAME, COLOR_COMMA, COLOR_ATTRVALUE, COLOR_ENTITYNAME);
    if (csh.CheckHintLocated((yylsp[0]))) {
        csh.HandleBlockNamePlus("modifier");
    } else if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, false, true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(false, false, true);
        csh.AddEntityNamesAtTheEnd("Position the block under definition in relation to block %s.");
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[0].stringposlist);
  #else
    (yyval.alignment_attr) = BlockBlock::TranslatePre(chart, (yyvsp[-2].alignmodifier), (yyvsp[-1].alignmodifier), CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[-1])), (yyvsp[0].stringposlist));
  #endif
}
#line 6165 "block_csh_lang.cc"
    break;

  case 133: /* alignment_modifiers: TOK_ALIGN_MODIFIER TOK_ALIGN_MODIFIER coord_hinted  */
#line 2190 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, false, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.alignment_attr) = BlockBlock::TranslatePre(chart, (yyvsp[-2].alignmodifier), (yyvsp[-1].alignmodifier), CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[-1])), (yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 6186 "block_csh_lang.cc"
    break;

  case 134: /* alignment_modifiers: TOK_ALIGN_MODIFIER blocknames_plus_number_or_number TOK_ALIGN_MODIFIER  */
#line 2207 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_NameList((yyvsp[-1].stringposlist), (yylsp[-1]), COLOR_ENTITYNAME, COLOR_COMMA, COLOR_ATTRVALUE, COLOR_ENTITYNAME);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintLocated((yylsp[-1]))) {
        csh.HandleBlockNamePlus("modifier");
    } else if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddKeywordsToHints(true, false, true);
        csh.AddEntityNamesAtTheEnd("Position the block under definition in relation to block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(false, false, true);
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    (yyval.alignment_attr) = BlockBlock::TranslatePre(chart, (yyvsp[-2].alignmodifier), (yyvsp[0].alignmodifier), CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), (yyvsp[-1].stringposlist));
  #endif
}
#line 6214 "block_csh_lang.cc"
    break;

  case 135: /* alignment_modifiers: TOK_ALIGN_MODIFIER coord_hinted TOK_ALIGN_MODIFIER  */
#line 2231 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(false, false, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.alignment_attr) = BlockBlock::TranslatePre(chart, (yyvsp[-2].alignmodifier), (yyvsp[0].alignmodifier), CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), (yyvsp[-1].multi_str).str, CHART_POS_START((yylsp[-1])));
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 6235 "block_csh_lang.cc"
    break;

  case 137: /* full_block_def_with_pre: alignment_modifiers  */
#line 2250 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing block definition.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing block definition. Ignoring this.");
    delete (yyvsp[0].alignment_attr);
  #endif
    (yyval.blockblocklist) = nullptr;
}
#line 6249 "block_csh_lang.cc"
    break;

  case 138: /* full_block_def_with_pre: alignment_modifiers full_block_def  */
#line 2260 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(true, false, true);
        csh.AddEntityNamesAtTheEnd("Position the block under definition in relation to block %s.");
        csh.hintStatus = HINT_READY;
    }
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if ((yyvsp[0].blockblocklist) && (yyvsp[0].blockblocklist)->size())
        (yyvsp[0].blockblocklist)->front()->ApplyPre((yyvsp[-1].alignment_attr));
    else
        delete (yyvsp[-1].alignment_attr);
  #endif
    (yyval.blockblocklist) = (yyvsp[0].blockblocklist);
}
#line 6270 "block_csh_lang.cc"
    break;

  case 139: /* multi_command: TOK_MULTI_COMMAND  */
#line 2279 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddKeywordsToHints(false, false, false);
        csh.hintStatus = HINT_READY;
    }
  #else
  #endif
    free((yyvsp[0].str));
    (yyval.condition) = 3;
}
#line 6287 "block_csh_lang.cc"
    break;

  case 140: /* multi_command: TOK_MULTI_COMMAND TOK_NUMBER  */
#line 2292 "block_lang.yy"
{
    const double d = atof((yyvsp[0].str));
    int num = d==floor(d) && d>0 && d<=10 ? int(d) : 0;
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[-1]))) {
        csh.AddKeywordsToHints(false, false, false);
        csh.hintStatus = HINT_READY;
    }
    if (num)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    else
        csh.AddCSH_Error((yylsp[0]), "Expecting number between [1..10].");
  #else
    if (num==0) {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting number between [1..10]. Assuming 3.");
        num=3;
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
    (yyval.condition) = num;
}
#line 6315 "block_csh_lang.cc"
    break;

  case 141: /* multi_command: TOK_MULTI_COMMAND tok_param_name_as_multi  */
#line 2316 "block_lang.yy"
{
    int num = 3;
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[-1]))) {
        csh.AddKeywordsToHints(false, false, false);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if (!chart.SkipContent()) {
        if (!(yyvsp[0].multi_str).had_error) {
            if (from_chars((yyvsp[0].multi_str).str, num) || num<1 || num>10) {
                chart.Error.Error(CHART_POS_START((yylsp[0])),
                    StrCat("Expecting number between [1..10], instead of '", (yyvsp[0].multi_str).str,
                           "'. Assuming 3."));
                num = 3;
            }
        }
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
    (yyval.condition) = num;
}
#line 6345 "block_csh_lang.cc"
    break;

  case 143: /* full_block_def: multi_command block_def  */
#line 2345 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAfter((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(false, false, false);
        csh.hintStatus = HINT_READY;
    }
    (yyval.blockblocklist) = csh.MakeMulti((yyvsp[-1].condition), (yyvsp[0].blockblocklist));
  #else
    //Create multi silently does nothing if $2 is  null - SkipContent() is handled that way
    (yyval.blockblocklist) = chart.CreateMulti((yyvsp[-1].condition), (yyvsp[0].blockblocklist), CHART_POS((yyloc)));
  #endif
}
#line 6362 "block_csh_lang.cc"
    break;

  case 144: /* full_block_def: multi_command  */
#line 2358 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(false, false, false);
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.blockblocklist) = nullptr;
    (void)(yyvsp[0].condition); //to make bison stop complaining
}
#line 6377 "block_csh_lang.cc"
    break;

  case 145: /* full_block_def: multi_command entity_string  */
#line 2369 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AfterMultiPartial((yylsp[0]), (yyvsp[0].multi_str).str);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddKeywordsToHints(false, false, false);
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.blockblocklist) = nullptr;
    (void)(yyvsp[-1].condition); //to make bison stop complaining
    free((yyvsp[0].multi_str).str);
}
#line 6394 "block_csh_lang.cc"
    break;

  case 146: /* block_def: block_keyword  */
#line 2384 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_NewBlock(-1);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddBlockKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
    (yyval.blockblocklist) = new CshStringWithPosList{{"", (yyloc)}};
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent()) {
	    (yyval.blockblocklist) = new BlockBlockList;
        (yyval.blockblocklist)->Append(std::make_unique<BlockBlock>(chart, (yyvsp[0].eblocktype), CHART_POS((yyloc))));
        (yyval.blockblocklist)->back()->AddAttributeList(nullptr);
    } else
        (yyval.blockblocklist) = nullptr;
  #endif
}
#line 6418 "block_csh_lang.cc"
    break;

  case 147: /* block_def: block_keyword blocknameposlist  */
#line 2404 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    for (auto &sp: *(yyvsp[0].stringposlist))
        csh.AddCSH_NewBlock(sp.file_pos, sp.name, -1);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddBlockKeywordsToHints();
        csh.hintStatus = HINT_READY;
    }
    (yyval.blockblocklist) = (yyvsp[0].stringposlist);
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent()) {
	    (yyval.blockblocklist) = new BlockBlockList;
        for (auto &sp: *(yyvsp[0].stringposlist)) {
            (yyval.blockblocklist)->Append(std::make_unique<BlockBlock>(chart, (yyvsp[-1].eblocktype),
                                                    (yyvsp[0].stringposlist)->size()== 1 ? CHART_POS((yyloc)) : sp.file_pos,
                                                    sp.name));
            (yyval.blockblocklist)->back()->AddAttributeList(nullptr);
        }
        if ((yyval.blockblocklist)->size())
            chart.current_parent = (yyval.blockblocklist)->front().get();
    } else
        (yyval.blockblocklist) = nullptr;
    delete (yyvsp[0].stringposlist);
  #endif
}
#line 6450 "block_csh_lang.cc"
    break;

  case 148: /* block_def: block_keyword full_attrlist_with_label  */
#line 2432 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_NewBlock(-1);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddBlockKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        //block_keyword returns a character specific to the type of block we have
        //we use it to filter attributes
        BlockBlock::AttributeNames((yyvsp[-1].eblocktype), csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues((yyvsp[-1].eblocktype), csh.hintAttrName, csh);
    (yyval.blockblocklist) = new CshStringWithPosList{{"", (yyloc)}};
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent())  {
	    (yyval.blockblocklist) = new BlockBlockList;
        (yyval.blockblocklist)->Append(std::make_unique<BlockBlock>(chart, (yyvsp[-1].eblocktype), CHART_POS((yyloc))));
        (yyval.blockblocklist)->back()->AddAttributeList((yyvsp[0].attributelist));
    } else {
        (yyval.blockblocklist) = nullptr;
        delete (yyvsp[0].attributelist);
    }
  #endif
}
#line 6481 "block_csh_lang.cc"
    break;

  case 149: /* block_def: block_keyword blocknameposlist full_attrlist_with_label  */
#line 2459 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    for (auto &sp: *(yyvsp[-1].stringposlist))
        csh.AddCSH_NewBlock(sp.file_pos, sp.name, -1);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddBlockKeywordsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        //block_keyword returns a character specific to the type of block we have
        //we use it to filter attributes
        BlockBlock::AttributeNames((yyvsp[-2].eblocktype), csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues((yyvsp[-2].eblocktype), csh.hintAttrName, csh);
    (yyval.blockblocklist) = (yyvsp[-1].stringposlist);
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent())  {
	    (yyval.blockblocklist) = new BlockBlockList;
        for (auto &sp: *(yyvsp[-1].stringposlist)) {
            (yyval.blockblocklist)->Append(std::make_unique<BlockBlock>(chart, (yyvsp[-2].eblocktype),
                                                    (yyvsp[-1].stringposlist)->size()== 1 ? CHART_POS((yyloc)) : sp.file_pos,
                                                    sp.name));
            if (&sp == &(yyvsp[-1].stringposlist)->back())
                (yyval.blockblocklist)->back()->AddAttributeList((yyvsp[0].attributelist));
            else
                (yyval.blockblocklist)->back()->AddAttributeList(Duplicate(*(yyvsp[0].attributelist)).release());
        }
        if ((yyval.blockblocklist)->size())
            chart.current_parent = (yyval.blockblocklist)->front().get();
    } else {
        (yyval.blockblocklist) = nullptr;
        delete (yyvsp[0].attributelist);
    }
    delete (yyvsp[-1].stringposlist);
  #endif
}
#line 6523 "block_csh_lang.cc"
    break;

  case 150: /* block_def: shape_block_header  */
#line 2497 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_NewBlock((yyvsp[0].condition));
    (yyval.blockblocklist) = new CshStringWithPosList{{"", (yyloc)}};
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent() && (yyvsp[0].condition)>=0) {
	    (yyval.blockblocklist) = new BlockBlockList;
        (yyval.blockblocklist)->Append(std::make_unique<BlockBlock>(chart, (yyvsp[0].condition), CHART_POS((yyloc))));
        (yyval.blockblocklist)->back()->AddAttributeList(nullptr);
    } else
        (yyval.blockblocklist) = nullptr;
  #endif
}
#line 6542 "block_csh_lang.cc"
    break;

  case 151: /* block_def: shape_block_header blocknameposlist  */
#line 2512 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    for (auto &sp: *(yyvsp[0].stringposlist))
        csh.AddCSH_NewBlock(sp.file_pos, sp.name, (yyvsp[-1].condition));
    (yyval.blockblocklist) = (yyvsp[0].stringposlist);
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent() && (yyvsp[-1].condition)>=0) {
	    (yyval.blockblocklist) = new BlockBlockList;
        for (auto &sp: *(yyvsp[0].stringposlist)) {
            (yyval.blockblocklist)->Append(std::make_unique<BlockBlock>(chart, (yyvsp[-1].condition),
                                                    (yyvsp[0].stringposlist)->size()== 1 ? CHART_POS((yyloc)) : sp.file_pos,
                                                    sp.name));
            (yyval.blockblocklist)->back()->AddAttributeList(nullptr);
        }
        if ((yyval.blockblocklist)->size())
            chart.current_parent = (yyval.blockblocklist)->front().get();
    } else
        (yyval.blockblocklist) = nullptr;
    delete (yyvsp[0].stringposlist);
  #endif
}
#line 6569 "block_csh_lang.cc"
    break;

  case 152: /* block_def: shape_block_header full_attrlist_with_label  */
#line 2535 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_NewBlock((yyvsp[-1].condition));
    //block_keyword returns a character specific to the type of block we have
    //we use it to filter attributes
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockBlock::AttributeNames(EBlockType::Shape, csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues(EBlockType::Shape, csh.hintAttrName, csh);
    (yyval.blockblocklist) = new CshStringWithPosList{{"", (yyloc)}};
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent() && (yyvsp[-1].condition)>=0)  {
	    (yyval.blockblocklist) = new BlockBlockList;
        (yyval.blockblocklist)->Append(std::make_unique<BlockBlock>(chart, (yyvsp[-1].condition), CHART_POS((yyloc))));
        (yyval.blockblocklist)->back()->AddAttributeList((yyvsp[0].attributelist));
    } else {
        (yyval.blockblocklist) = nullptr;
        delete (yyvsp[0].attributelist);
    }
  #endif
}
#line 6596 "block_csh_lang.cc"
    break;

  case 153: /* block_def: shape_block_header blocknameposlist full_attrlist_with_label  */
#line 2558 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    for (auto &sp: *(yyvsp[-1].stringposlist))
        csh.AddCSH_NewBlock(sp.file_pos, sp.name, (yyvsp[-2].condition));
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockBlock::AttributeNames(EBlockType::Shape, csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues(EBlockType::Shape, csh.hintAttrName, csh);
    (yyval.blockblocklist) = (yyvsp[-1].stringposlist);
  #else
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent() && (yyvsp[-2].condition)>=0)  {
	    (yyval.blockblocklist) = new BlockBlockList;
        for (auto &sp: *(yyvsp[-1].stringposlist)) {
            (yyval.blockblocklist)->Append(std::make_unique<BlockBlock>(chart, (yyvsp[-2].condition),
                                                    (yyvsp[-1].stringposlist)->size()== 1 ? CHART_POS((yyloc)) : sp.file_pos,
                                                    sp.name));
            if (&sp == &(yyvsp[-1].stringposlist)->back())
                (yyval.blockblocklist)->back()->AddAttributeList((yyvsp[0].attributelist));
            else
                (yyval.blockblocklist)->back()->AddAttributeList(Duplicate(*(yyvsp[0].attributelist)).release());
        }
        if ((yyval.blockblocklist)->size())
            chart.current_parent = (yyval.blockblocklist)->front().get();
    } else {
        (yyval.blockblocklist) = nullptr;
        delete (yyvsp[0].attributelist);
    }
    delete (yyvsp[-1].stringposlist);
  #endif
}
#line 6632 "block_csh_lang.cc"
    break;

  case 154: /* block_def: TOK_SPACE_COMMAND  */
#line 2590 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_KEYWORD);
    (yyval.blockblocklist) = new CshStringWithPosList{{"", (yyloc)}};
  #else
    if (!chart.SkipContent()) {
  		(yyval.blockblocklist) = new BlockBlockList;
      (yyval.blockblocklist)->Append(std::make_unique<BlockSpace>(chart, CHART_POS((yylsp[0]))));
    } else
        (yyval.blockblocklist) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6650 "block_csh_lang.cc"
    break;

  case 155: /* block_def: TOK_SPACE_COMMAND TOK_NUMBER  */
#line 2604 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    (yyval.blockblocklist) = new CshStringWithPosList{{"", (yyloc)}};
 #else
    if (!chart.SkipContent()) {
	    (yyval.blockblocklist) = new BlockBlockList;
      (yyval.blockblocklist)->Append(std::make_unique<BlockSpace>(chart, CHART_POS((yylsp[-1])), atoi((yyvsp[0].str))));
    } else
        (yyval.blockblocklist) = nullptr;
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 6670 "block_csh_lang.cc"
    break;

  case 156: /* block_def: TOK_SPACE_COMMAND tok_param_name_as_multi  */
#line 2620 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    (yyval.blockblocklist) = new CshStringWithPosList{{"", (yyloc)}};
  #else
    if (!chart.SkipContent()) {
        double s = -1;
        if (!(yyvsp[0].multi_str).had_error) {
            if (from_chars((yyvsp[0].multi_str).str, s) || s<0) {
                s = -1;
                chart.Error.Error(CHART_POS_START((yylsp[0])),
                    StrCat("A positive number is needed here, instead of '", (yyvsp[0].multi_str).str,
                           "'. Ignoring value."));
            }
        }
	    (yyval.blockblocklist) = new BlockBlockList;
        (yyval.blockblocklist)->Append(std::make_unique<BlockSpace>(chart, CHART_POS((yylsp[-1])), s));
    } else
        (yyval.blockblocklist) = nullptr;
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 6699 "block_csh_lang.cc"
    break;

  case 157: /* block_keyword: TOK_BOX  */
#line 2647 "block_lang.yy"
                          { free((yyvsp[0].str)); (yyval.eblocktype)=EBlockType::Box; }
#line 6705 "block_csh_lang.cc"
    break;

  case 158: /* block_keyword: TOK_BOXCOL  */
#line 2648 "block_lang.yy"
                          { free((yyvsp[0].str)); (yyval.eblocktype)=EBlockType::Boxcol; }
#line 6711 "block_csh_lang.cc"
    break;

  case 159: /* block_keyword: TOK_ROW  */
#line 2649 "block_lang.yy"
                          { free((yyvsp[0].str)); (yyval.eblocktype)=EBlockType::Row; }
#line 6717 "block_csh_lang.cc"
    break;

  case 160: /* block_keyword: TOK_COLUMN  */
#line 2650 "block_lang.yy"
                          { free((yyvsp[0].str)); (yyval.eblocktype)=EBlockType::Column; }
#line 6723 "block_csh_lang.cc"
    break;

  case 161: /* block_keyword: TOK_TEXT  */
#line 2651 "block_lang.yy"
                          { free((yyvsp[0].str)); (yyval.eblocktype)=EBlockType::Text;}
#line 6729 "block_csh_lang.cc"
    break;

  case 162: /* block_keyword: TOK_CELL  */
#line 2652 "block_lang.yy"
                          { free((yyvsp[0].str)); (yyval.eblocktype)=EBlockType::Cell;}
#line 6735 "block_csh_lang.cc"
    break;

  case 163: /* shape_block_header: TOK_SHAPE  */
#line 2656 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing shape name to use.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddShapesToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing shape name to use. Ignoring this shape.");
  #endif
    (yyval.condition) = -1;
    free((yyvsp[0].str));
}
#line 6754 "block_csh_lang.cc"
    break;

  case 164: /* shape_block_header: TOK_SHAPE entity_string  */
#line 2671 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddShapesToHints();
        csh.hintStatus = HINT_READY;
    }
    if ((yyvsp[0].multi_str).had_error)  {
        (yyval.condition) = -1;
    } else {
        (yyval.condition) = csh.GetShapeNum((yyvsp[0].multi_str).str);
        if ((yyval.condition)>=0)
            csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE_EMPH);
        else
            csh.AddCSH_Error((yylsp[0]), "Unrecognized shape name.");
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.condition) = -1;
    else {
        (yyval.condition) = chart.Shapes.GetShapeNo((yyvsp[0].multi_str).str);
        if ((yyval.condition)==-1)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Unrecognized shape name. Ignoring this shape.");
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 6787 "block_csh_lang.cc"
    break;

  case 165: /* shape_block_header: TOK_ASTERISK  */
#line 2700 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing shape name to use.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddShapesToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing shape name to use. Ignoring this shape.");
  #endif
    (yyval.condition) = -1;
}
#line 6805 "block_csh_lang.cc"
    break;

  case 166: /* shape_block_header: TOK_ASTERISK entity_string  */
#line 2714 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddShapesToHints();
        csh.hintStatus = HINT_READY;
    }
    if ((yyvsp[0].multi_str).had_error)  {
        (yyval.condition) = -1;
    } else {
        (yyval.condition) = csh.GetShapeNum((yyvsp[0].multi_str).str);
        if ((yyval.condition)>=0)
            csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE_EMPH);
        else
            csh.AddCSH_Error((yylsp[0]), "Unrecognized shape name.");
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.condition) = -1;
    else {
        (yyval.condition) = chart.Shapes.GetShapeNo((yyvsp[0].multi_str).str);
        if ((yyval.condition)==-1)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Unrecognized shape name. Ignoring this shape.");
    }
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 6837 "block_csh_lang.cc"
    break;

  case 167: /* blocknameposlist: entity_string  */
#line 2743 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.stringposlist) = new CshStringWithPosList;
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
  #else
    (yyval.stringposlist) = new StringWithPosList;
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 6854 "block_csh_lang.cc"
    break;

  case 168: /* blocknameposlist: stylenameposlist TOK_COMMA  */
#line 2756 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a block name to define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re)define.");
  #endif
    (yyval.stringposlist) = (yyvsp[-1].stringposlist);
}
#line 6867 "block_csh_lang.cc"
    break;

  case 169: /* blocknameposlist: stylenameposlist TOK_COMMA entity_string  */
#line 2765 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyvsp[-2].stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
  #else
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyvsp[-2].stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
  #endif
    (yyval.stringposlist) = (yyvsp[-2].stringposlist);
    free((yyvsp[0].multi_str).str);
}
#line 6883 "block_csh_lang.cc"
    break;

  case 170: /* command: TOK_BREAK_COMMAND  */
#line 2783 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_KEYWORD);
  #else
    if (!chart.SkipContent()) {
        auto p = new BlockBreak(chart, CHART_POS((yylsp[0])));
        p->SetContent(nullptr);
        (yyval.instruction) = p;
    } else
        (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 6901 "block_csh_lang.cc"
    break;

  case 171: /* command: TOK_JOIN_COMMAND  */
#line 2797 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing block definition.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddEntityNamesAtTheEnd("Join block %s with other blocks.");
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing block names to join. Ignoring 'join' keyword here.");
  #endif
    (yyval.instruction) = nullptr;
    free((yyvsp[0].str));
}
#line 6920 "block_csh_lang.cc"
    break;

  case 172: /* command: TOK_JOIN_COMMAND blocknames_plus  */
#line 2812 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_NameList((yyvsp[0].stringposlist), (yylsp[0]), COLOR_ENTITYNAME, COLOR_COMMA);
    if (csh.CheckHintLocated((yylsp[0]))) {
        csh.AddEntityNamesAtTheEnd("Join block %s with other blocks.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddEntityNamesAtTheEnd("Join block %s with other blocks.");
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[0].stringposlist);
  #else
    if (!chart.SkipContent())
        (yyval.instruction) = chart.CreateJoin(CHART_POS((yyloc)), (yyvsp[0].stringposlist));
    else {
        (yyval.instruction) = nullptr;
        delete (yyvsp[0].stringposlist);
    }
  #endif
    free((yyvsp[-1].str));
}
#line 6947 "block_csh_lang.cc"
    break;

  case 173: /* command: TOK_JOIN_COMMAND full_attrlist  */
#line 2835 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintAfter((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddEntityNamesAtTheEnd("Join block %s with other blocks.");
        csh.hintStatus = HINT_READY;
    }
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        JoinCommand::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        JoinCommand::AttributeValues(csh.hintAttrName, csh);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing block names to join. Ignoring command.");
    delete (yyvsp[0].attributelist);
  #endif
    (yyval.instruction) = nullptr;
    free((yyvsp[-1].str));
}
#line 6970 "block_csh_lang.cc"
    break;

  case 174: /* command: TOK_JOIN_COMMAND blocknames_plus full_attrlist  */
#line 2854 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_NameList((yyvsp[-1].stringposlist), (yylsp[-1]), COLOR_ENTITYNAME, COLOR_COMMA);
    if (csh.CheckHintLocated((yylsp[-1]))) {
        csh.AddEntityNamesAtTheEnd("Join block %s with other blocks.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        JoinCommand::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        JoinCommand::AttributeValues(csh.hintAttrName, csh);
    else if (csh.CheckHintAfter((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddEntityNamesAtTheEnd("Join block %s with other blocks.");
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    if (!chart.SkipContent())
        (yyval.instruction) = chart.CreateJoin(CHART_POS((yyloc)), (yyvsp[-1].stringposlist), (yyvsp[0].attributelist));
    else {
        (yyval.instruction) = nullptr;
        delete (yyvsp[-1].stringposlist);
    }
  #endif
    free((yyvsp[-2].str));
}
#line 7001 "block_csh_lang.cc"
    break;

  case 175: /* copy_header: TOK_COPY_COMMAND  */
#line 2882 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing block name to copy.");
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Create a copy of block %s.");
        csh.hintStatus = HINT_READY;
    }
    (yyval.copystruct) = nullptr;
  #else
    (yyval.copystruct) = new CopyParseHelper;
    (yyval.copystruct)->Init();
    (yyval.copystruct)->file_pos = CHART_POS((yyloc));
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing block name to copy.");
  #endif
    free((yyvsp[0].str));
}
#line 7026 "block_csh_lang.cc"
    break;

  case 176: /* copy_header: TOK_COPY_COMMAND entity_string  */
#line 2903 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.hadProcReplay = true;
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Create a copy of block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "as",
                               "Optionally continue with 'as' to specify the name of the block or with attributes or content.",
                               EHintType::KEYWORD));
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[0].multi_str).had_error) {
        csh.Copy((yyvsp[0].multi_str).str, (yyvsp[0].multi_str).str, (yylsp[0]), (yylsp[0]));
        (yyval.copystruct) = (yyvsp[0].multi_str).str;
    } else 
        free((yyvsp[0].multi_str).str);
  #else
    (yyval.copystruct) = new CopyParseHelper;
    (yyval.copystruct)->Init();
    if (!(yyvsp[0].multi_str).had_error) {
        (yyval.copystruct)->file_pos = CHART_POS((yyloc));
        (yyval.copystruct)->block_name = (yyvsp[0].multi_str).str;
        (yyval.copystruct)->block_pos = CHART_POS_START((yylsp[0]));
    } else 
        free((yyvsp[0].multi_str).str);
  #endif
    free((yyvsp[-1].str));
}
#line 7064 "block_csh_lang.cc"
    break;

  case 177: /* copy_header: TOK_COPY_COMMAND entity_string TOK_AS  */
#line 2937 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.hadProcReplay = true;
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a name for the copy.");
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Create a copy of block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "as",
                               "Optionally continue with 'as' to specify the name of the block or with attributes or content.",
                               EHintType::KEYWORD));
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[-1].multi_str).had_error) {
        csh.Copy((yyvsp[-1].multi_str).str, (yyvsp[-1].multi_str).str, (yylsp[-1]), (yylsp[-1]));
        (yyval.copystruct) = (yyvsp[-1].multi_str).str;
    } else 
        free((yyvsp[-1].multi_str).str);
  #else
    (yyval.copystruct) = new CopyParseHelper;
    (yyval.copystruct)->Init();
    if (!(yyvsp[-1].multi_str).had_error) {
        (yyval.copystruct)->file_pos = CHART_POS((yyloc));
        (yyval.copystruct)->block_name = (yyvsp[-1].multi_str).str;
        (yyval.copystruct)->block_pos = CHART_POS_START((yylsp[-1]));
    }
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a name for the copy.");
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 7105 "block_csh_lang.cc"
    break;

  case 178: /* copy_header: TOK_COPY_COMMAND entity_string TOK_AS entity_string  */
#line 2974 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.hadProcReplay = true;
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Create a copy of block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "as",
                               "Optionally continue with 'as' to specify the name of the block or with attributes or content.",
                               EHintType::KEYWORD));
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        csh.Copy((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, (yylsp[-2]), (yylsp[0]));
    free((yyvsp[-2].multi_str).str);
    if (!(yyvsp[0].multi_str).had_error)
        (yyval.copystruct) = (yyvsp[0].multi_str).str;
    else
        free((yyvsp[0].multi_str).str);
  #else
    (yyval.copystruct) = new CopyParseHelper;
    (yyval.copystruct)->Init();
    if (!(yyvsp[-2].multi_str).had_error) {
        (yyval.copystruct)->file_pos = CHART_POS((yyloc));
        (yyval.copystruct)->block_name = (yyvsp[-2].multi_str).str;
        (yyval.copystruct)->block_pos = CHART_POS_START((yylsp[-2]));
    } else
        free((yyvsp[-2].multi_str).str);
    if (!(yyvsp[0].multi_str).had_error)
        (yyval.copystruct)->copy_name = (yyvsp[0].multi_str).str;
    else
        free((yyvsp[0].multi_str).str);
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[-1].str));
}
#line 7151 "block_csh_lang.cc"
    break;

  case 179: /* copy_header_resolved: copy_header  */
#line 3017 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.copystruct) = (yyvsp[0].copystruct);
  #else
    (yyval.copystruct) = chart.ResolveCopyHelper((yyvsp[0].copystruct));
  #endif
}
#line 7163 "block_csh_lang.cc"
    break;

  case 181: /* copy_header_with_pre: alignment_modifiers copy_header_resolved  */
#line 3027 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    _ASSERT((yyvsp[-1].alignment_attr)); //copy_header should never return null
    (yyvsp[0].copystruct)->align = (yyvsp[-1].alignment_attr);
  #endif
    (yyval.copystruct) = (yyvsp[0].copystruct);
}
#line 7175 "block_csh_lang.cc"
    break;

  case 183: /* copy_attr: copy_header_with_pre full_attrlist_with_label  */
#line 3037 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockBlock::AttributeNames(EBlockType::Box, csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockBlock::AttributeValues(EBlockType::Box, csh.hintAttrName, csh);
    (yyval.copystruct) = (yyvsp[-1].copystruct);
  #else
    _ASSERT((yyvsp[-1].copystruct)); //copy_header should never return null
    (yyval.copystruct) = (yyvsp[-1].copystruct);
    (yyval.copystruct)->attributes = (yyvsp[0].attributelist);
  #endif
}
#line 7193 "block_csh_lang.cc"
    break;

  case 184: /* arrowsymbol: TOK_ARROW_FW  */
#line 3056 "block_lang.yy"
                                {(yyval.arrowtype) = {(yyvsp[0].arrowstyle), true,  false};}
#line 7199 "block_csh_lang.cc"
    break;

  case 185: /* arrowsymbol: TOK_ARROW_BW  */
#line 3057 "block_lang.yy"
                                {(yyval.arrowtype) = {(yyvsp[0].arrowstyle), false, true};}
#line 7205 "block_csh_lang.cc"
    break;

  case 186: /* arrowsymbol: TOK_ARROW_BIDIR  */
#line 3058 "block_lang.yy"
                                {(yyval.arrowtype) = {(yyvsp[0].arrowstyle), true,  true};}
#line 7211 "block_csh_lang.cc"
    break;

  case 187: /* arrowsymbol: TOK_ARROW_NO  */
#line 3059 "block_lang.yy"
                                {(yyval.arrowtype) = {(yyvsp[0].arrowstyle), false, false};}
#line 7217 "block_csh_lang.cc"
    break;

  case 188: /* arrowsymbol: TOK_PLUS_PLUS  */
#line 3060 "block_lang.yy"
                                {(yyval.arrowtype) = {EArrowStyle::DASHED, false, false};}
#line 7223 "block_csh_lang.cc"
    break;

  case 189: /* arrowsymbol: TOK_COMP_OP  */
#line 3062 "block_lang.yy"
{
    switch ((yyvsp[0].compare_op)) {
    case ECompareOperator::SMALLER:          (yyval.arrowtype) =  {EArrowStyle::DOTTED, false, true};  break;
    case ECompareOperator::SMALLER_OR_EQUAL: (yyval.arrowtype) =  {EArrowStyle::DOUBLE, false, true};  break;
    case ECompareOperator::EQUAL:            (yyval.arrowtype) =  {EArrowStyle::DOTTED, false, false}; break;
    case ECompareOperator::NOT_EQUAL:        (yyval.arrowtype) =  {EArrowStyle::DOTTED, true,  true};  break;
    default: _ASSERT(0); FALLTHROUGH;
    case ECompareOperator::GREATER_OR_EQUAL: (yyval.arrowtype) =  {EArrowStyle::DOUBLE, true,  false}; break;
    case ECompareOperator::GREATER:          (yyval.arrowtype) =  {EArrowStyle::DOTTED, true,  false}; break;
    }
}
#line 7239 "block_csh_lang.cc"
    break;

  case 191: /* coord_hinted_with_dir: coord_hinted TOK_ATSYMBOL  */
#line 3076 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing direction or compass point.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing direction or compass point.");
  #endif
    (yyval.multi_str) = (yyvsp[-1].multi_str);
}
#line 7257 "block_csh_lang.cc"
    break;

  case 192: /* coord_hinted_with_dir: coord_hinted TOK_ATSYMBOL alpha_string  */
#line 3090 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", (yyvsp[0].multi_str));
}
#line 7272 "block_csh_lang.cc"
    break;

  case 193: /* coord_hinted_with_dir: coord_hinted TOK_ATSYMBOL TOK_NUMBER  */
#line 3101 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", (yyvsp[0].str));
}
#line 7287 "block_csh_lang.cc"
    break;

  case 195: /* coord_hinted_with_dir_and_dist: coord_hinted_with_dir TOK_ATSYMBOL  */
#line 3114 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.AddCSH_ErrorAfter((yyloc), "Missing distance value for the direction.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing distance value for the direction.");
  #endif
    (yyval.multi_str) = (yyvsp[-1].multi_str);
}
#line 7301 "block_csh_lang.cc"
    break;

  case 196: /* coord_hinted_with_dir_and_dist: coord_hinted_with_dir TOK_ATSYMBOL TOK_NUMBER  */
#line 3124 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    if ((yyvsp[0].str)[0]=='-'||(yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@+", (yyvsp[0].str));
  #else
    if ((yyvsp[0].str)[0]=='-'||(yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), "+", (yyvsp[0].str));
  #endif
}
#line 7320 "block_csh_lang.cc"
    break;

  case 198: /* arrow_end: coord_hinted_with_dir_and_dist distance_for_arrow_end  */
#line 3144 "block_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].multi_str));
}
#line 7328 "block_csh_lang.cc"
    break;

  case 199: /* arrow_end: arrow_end_block_port_compass_distance  */
#line 3148 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ENTITY, (yylsp[0]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_LOCATED;
        csh.hintSource = EHintSourceType::LINE_START;
    }
  #endif
    (yyval.multi_str) = (yyvsp[0].multi_str);
}
#line 7343 "block_csh_lang.cc"
    break;

  case 200: /* tok_plus_or_minus: TOK_PLUS  */
#line 3159 "block_lang.yy"
                            {(yyval.condition)=+1;}
#line 7349 "block_csh_lang.cc"
    break;

  case 201: /* tok_plus_or_minus: TOK_MINUS  */
#line 3159 "block_lang.yy"
                                                 {(yyval.condition)=-1;}
#line 7355 "block_csh_lang.cc"
    break;

  case 203: /* arrow_end_block_port_compass_distance: arrow_end_block_port_compass_distance distance_for_arrow_end  */
#line 3163 "block_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].multi_str));
}
#line 7363 "block_csh_lang.cc"
    break;

  case 204: /* arrow_end_block_port_compass_distance: entity_string  */
#line 3167 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_BlockNameOrNew((yylsp[0]), (yyvsp[0].multi_str).str);
    csh.CheckHintAt((yylsp[0]), EHintSourceType::ENTITY); //We need to hint later, when we know the context
    //Hinting is left in HINT_LOCATED
  #endif
    (yyval.multi_str) = (yyvsp[0].multi_str);
}
#line 7376 "block_csh_lang.cc"
    break;

  case 205: /* arrow_end_block_port_compass_distance: entity_string distance_for_arrow_end  */
#line 3176 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_BlockNameOrNew((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.CheckHintAt((yylsp[-1]), EHintSourceType::ENTITY); //We need to hint later, when we know the context
    //Hinting is left in HINT_LOCATED
  #endif
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].multi_str));
}
#line 7389 "block_csh_lang.cc"
    break;

  case 206: /* distance_for_arrow_end: tok_plus_or_minus  */
#line 3187 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.AddCSH_Error((yylsp[0]), "Missing <num>, <num>%, x<num>, y<num>, x<num>% or y<num>%.");
    (yyval.multi_str).CombineThemToMe((yyvsp[0].condition)==+1 ? "+" : "-");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing <num>, <num>%, x<num>, y<num>, x<num>% or y<num>%.");
    (yyval.multi_str) = multi_segment_string(nullptr);
    (void)(yyvsp[0].condition); //stop complaining about unused value
  #endif
}
#line 7405 "block_csh_lang.cc"
    break;

  case 207: /* distance_for_arrow_end: TOK_NUMBER  */
#line 3199 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
  #else
    if ((yyvsp[0].str)[0]=='-' || (yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe(CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe("+", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
  #endif
}
#line 7421 "block_csh_lang.cc"
    break;

  case 208: /* distance_for_arrow_end: tok_plus_or_minus TOK_NUMBER  */
#line 3211 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].condition)==+1 ? "+" : "-", (yyvsp[0].str));
  #else
    if ((yyvsp[-1].condition) == +1) {
        if ((yyvsp[0].str)[0]=='-' || (yyvsp[0].str)[0]=='+')
            (yyval.multi_str).CombineThemToMe(CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
        else
            (yyval.multi_str).CombineThemToMe("+", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
    } else {
        //An extra minus sign
        if ((yyvsp[0].str)[0]=='-') {
            (yyval.multi_str).CombineThemToMe(CHART_POS_START((yylsp[0])).Print(), "+", const_cast<const char*>((yyvsp[0].str)+1)); //dont free $2+1
            free((yyvsp[0].str));
        } else if ((yyvsp[0].str)[0]=='+') {
            (yyval.multi_str).CombineThemToMe(CHART_POS_START((yylsp[0])).Print(), "-", const_cast<const char*>((yyvsp[0].str)+1)); //dont free $2+1
            free((yyvsp[0].str));
        } else
            (yyval.multi_str).CombineThemToMe("-", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
    }
  #endif
}
#line 7449 "block_csh_lang.cc"
    break;

  case 209: /* distance_for_arrow_end: tok_plus_or_minus TOK_STRING  */
#line 3235 "block_lang.yy"
{
    _ASSERT((yyvsp[0].str) && *(yyvsp[0].str)); //a non-empty string
	double dummy;
    if ((yyvsp[0].str) && ((yyvsp[0].str)[0]=='y' || (yyvsp[0].str)[0]=='Y' || (yyvsp[0].str)[0]=='x' || (yyvsp[0].str)[0]=='X') &&
		!from_chars((yyvsp[0].str)+1, dummy)) {
#ifdef C_S_H_IS_COMPILED
		csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
		csh.AddCSH(CshPos((yylsp[0]).first_pos, (yylsp[0]).first_pos), COLOR_ATTRVALUE_EMPH);
		CshPos pos = (yylsp[0]);
		pos.first_pos++;
		csh.AddCSH(pos, COLOR_ATTRVALUE);

		(yyval.multi_str).CombineThemToMe((yyvsp[-1].condition) == +1 ? "+" : "-", (yyvsp[0].str));
#else
	    (yyval.multi_str).CombineThemToMe((yyvsp[-1].condition) == +1 ? "+" : "-", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
#endif
	} else {
#ifdef C_S_H_IS_COMPILED
	    csh.AddCSH_Error((yylsp[0]), "Expecting <num>, <num>%, x<num>, y<num>, x<num>% or y<num>%.");
#else
	    chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting <num>, <num>%, x<num>, y<num>, x<num>% or y<num>%. Ignoring this.");
#endif
	    (yyval.multi_str) = multi_segment_string(nullptr);
		free((yyvsp[0].str));
	}
}
#line 7480 "block_csh_lang.cc"
    break;

  case 210: /* distance_for_arrow_end: tok_plus_or_minus TOK_STRING TOK_PERCENT  */
#line 3262 "block_lang.yy"
{
    _ASSERT((yyvsp[-1].str) && *(yyvsp[-1].str)); //a non-empty string
	double dummy;
    if ((yyvsp[-1].str) && ((yyvsp[-1].str)[0]=='y' || (yyvsp[-1].str)[0]=='Y' || (yyvsp[-1].str)[0]=='x' || (yyvsp[-1].str)[0]=='X') &&
		!from_chars((yyvsp[-1].str)+1, dummy)) {
#ifdef C_S_H_IS_COMPILED
		csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
		csh.AddCSH(CshPos((yylsp[-1]).first_pos, (yylsp[-1]).first_pos), COLOR_ATTRVALUE_EMPH);
		CshPos pos = (yylsp[-1]);
		pos.first_pos++;
		csh.AddCSH(pos+(yylsp[0]), COLOR_ATTRVALUE);

		(yyval.multi_str).CombineThemToMe((yyvsp[-2].condition) == +1 ? "+" : "-", (yyvsp[-1].str), "%");
#else
	    (yyval.multi_str).CombineThemToMe((yyvsp[-2].condition) == +1 ? "+" : "-", CHART_POS_START((yylsp[-1])).Print(), (yyvsp[-1].str), "%");
#endif
	} else {
#ifdef C_S_H_IS_COMPILED
	    csh.AddCSH_Error((yylsp[-1]), "Expecting <num>, <num>%, x<num>, y<num>, x<num>% or y<num>%.");
#else
	    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting <num>, <num>%, x<num>, y<num>, x<num>% or y<num>%. Ignoring this.");
#endif
	    (yyval.multi_str) = multi_segment_string(nullptr);
		free((yyvsp[-1].str));
	}
}
#line 7511 "block_csh_lang.cc"
    break;

  case 211: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL  */
#line 3290 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-1]), (yyvsp[-1].multi_str).str);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing portname or compass point.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing portname or compass point.");
  #endif
    (yyval.multi_str) = (yyvsp[-1].multi_str);
}
#line 7534 "block_csh_lang.cc"
    break;

  case 212: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL alpha_string  */
#line 3309 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }

    (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", (yyvsp[0].multi_str));
  #else
    (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].multi_str));
  #endif
}
#line 7557 "block_csh_lang.cc"
    break;

  case 213: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL TOK_NUMBER  */
#line 3328 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    if ((yyvsp[0].str)[0]=='-'||(yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@+", (yyvsp[0].str));
  #else
    if ((yyvsp[0].str)[0]=='-'||(yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), "+", (yyvsp[0].str));
  #endif
}
#line 7585 "block_csh_lang.cc"
    break;

  case 214: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL alpha_string TOK_ATSYMBOL  */
#line 3352 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-3]), (yyvsp[-3].multi_str).str);
    csh.AddCSH((yylsp[-2])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else  if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing compass point.");
    (yyval.multi_str).CombineThemToMe((yyvsp[-3].multi_str), "@", (yyvsp[-1].multi_str));
  #else
    (yyval.multi_str).CombineThemToMe((yyvsp[-3].multi_str), "@", CHART_POS_START((yylsp[-1])).Print(), (yyvsp[-1].multi_str));
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing compass point.");
  #endif
}
#line 7611 "block_csh_lang.cc"
    break;

  case 215: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL TOK_ATSYMBOL  */
#line 3374 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-2]), (yyvsp[-2].multi_str).str);
    csh.AddCSH((yylsp[-1])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing portname.");
    csh.AddCSH_ErrorAfter((yyloc), "Missing compass point.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing portname.");
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing compass point.");
  #endif
    (yyval.multi_str) = (yyvsp[-2].multi_str);
}
#line 7639 "block_csh_lang.cc"
    break;

  case 216: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL TOK_ATSYMBOL entity_string  */
#line 3398 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-3]), (yyvsp[-3].multi_str).str);
    csh.AddCSH((yylsp[-2])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[-2]), "Missing portname.");
    (yyval.multi_str).CombineThemToMe((yyvsp[-3].multi_str), "@", (yyvsp[0].multi_str));
  #else
    (yyval.multi_str).CombineThemToMe((yyvsp[-3].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].multi_str));
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing portname.");
  #endif
}
#line 7663 "block_csh_lang.cc"
    break;

  case 217: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL TOK_ATSYMBOL TOK_NUMBER  */
#line 3418 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-3]), (yyvsp[-3].multi_str).str);
    csh.AddCSH((yylsp[-2])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yylsp[-2]), "Missing portname.");
    if ((yyvsp[0].str)[0]=='-'||(yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe((yyvsp[-3].multi_str), "@", (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-3].multi_str), "@+", (yyvsp[0].str));
  #else
    if ((yyvsp[0].str)[0]=='-'||(yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe((yyvsp[-3].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-3].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), "+", (yyvsp[0].str));
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing portname.");
  #endif
}
#line 7693 "block_csh_lang.cc"
    break;

  case 218: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL alpha_string TOK_ATSYMBOL entity_string  */
#line 3444 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-4]), (yyvsp[-4].multi_str).str);
    csh.AddCSH((yylsp[-3])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-4]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    }  if (csh.CheckHintBetweenAndAt((yylsp[-3]), (yylsp[-2]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    (yyval.multi_str).CombineThemToMe((yyvsp[-4].multi_str), "@", (yyvsp[-2].multi_str), "@", (yyvsp[0].multi_str));
  #else
    (yyval.multi_str).CombineThemToMe((yyvsp[-4].multi_str), "@", CHART_POS_START((yylsp[-2])).Print(), (yyvsp[-2].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].multi_str));
  #endif
}
#line 7717 "block_csh_lang.cc"
    break;

  case 219: /* arrow_end_block_port_compass: entity_string TOK_ATSYMBOL alpha_string TOK_ATSYMBOL TOK_NUMBER  */
#line 3464 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    auto shape = csh.AddCSH_BlockNameOrNew((yylsp[-4]), (yyvsp[-4].multi_str).str);
    csh.AddCSH((yylsp[-3])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-4]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    }  if (csh.CheckHintBetweenAndAt((yylsp[-3]), (yylsp[-2]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddPortsToHints(shape);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) { //hint source for ports and compass points is entity
        csh.AddCompassPointsToHints();
        csh.hintStatus = HINT_READY;
    }
    if ((yyvsp[0].str)[0]=='-'||(yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe((yyvsp[-4].multi_str), "@", (yyvsp[-2].multi_str), "@", (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-4].multi_str), "@", (yyvsp[-2].multi_str), "@+", (yyvsp[0].str));
  #else
    if ((yyvsp[0].str)[0]=='-'||(yyvsp[0].str)[0]=='+')
        (yyval.multi_str).CombineThemToMe((yyvsp[-4].multi_str), "@", CHART_POS_START((yylsp[-2])).Print(), (yyvsp[-2].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-4].multi_str), "@", CHART_POS_START((yylsp[-2])).Print(), (yyvsp[-2].multi_str), "@", CHART_POS_START((yylsp[0])).Print(), "+", (yyvsp[0].str));
  #endif
}
#line 7747 "block_csh_lang.cc"
    break;

  case 220: /* arrowend_list: arrow_end TOK_COMMA  */
#line 3494 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a block name or coordinate here.");
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        //leave in HINT_LOCATED
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    }
    (yyval.stringposlist) = new CshStringWithPosList;
    if (!(yyvsp[-1].multi_str).had_error && (yyvsp[-1].multi_str).str)
        (yyval.stringposlist)->push_back({(yyvsp[-1].multi_str).str, (yylsp[-1])});
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a block name or coordinate here.");
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[-1].multi_str).str && !(yyvsp[-1].multi_str).had_error)
        (yyval.stringposlist)->push_back({(yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1]))});
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 7773 "block_csh_lang.cc"
    break;

  case 221: /* arrowend_list: arrow_end TOK_COMMA arrow_end  */
#line 3516 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-2]))) {
        //leave in HINT_LOCATED
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    (yyval.stringposlist) = new CshStringWithPosList;
    if (!(yyvsp[-2].multi_str).had_error && (yyvsp[-2].multi_str).str)
        (yyval.stringposlist)->push_back({(yyvsp[-2].multi_str).str, (yylsp[-2])});
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
  #else
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[-2].multi_str).str && !(yyvsp[-2].multi_str).had_error)
        (yyval.stringposlist)->push_back({(yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2]))});
    if ((yyvsp[0].multi_str).str && !(yyvsp[0].multi_str).had_error)
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 7802 "block_csh_lang.cc"
    break;

  case 222: /* arrowend_list: arrowend_list TOK_COMMA  */
#line 3541 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
      csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a block name or coordinate here.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a block name or coordinate here.");
  #endif
    (yyval.stringposlist) = (yyvsp[-1].stringposlist);
}
#line 7820 "block_csh_lang.cc"
    break;

  case 223: /* arrowend_list: arrowend_list TOK_COMMA arrow_end  */
#line 3555 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyvsp[-2].stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
  #else
    if ((yyvsp[-2].stringposlist) && !(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyvsp[-2].stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
  #endif
    (yyval.stringposlist) = (yyvsp[-2].stringposlist);
    free((yyvsp[0].multi_str).str);
}
#line 7844 "block_csh_lang.cc"
    break;

  case 225: /* arrowend_list_solo: arrow_end  */
#line 3579 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    //possinbly leave in HINT_LOCATED
    (yyval.stringposlist) = new CshStringWithPosList;
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
  #else
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[0].multi_str).str && !(yyvsp[0].multi_str).had_error)
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 7862 "block_csh_lang.cc"
    break;

  case 226: /* arrow_needs_semi: arrowend_list  */
#line 3605 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[0].stringposlist);
   //csh.AddCSH_ErrorAfter(@1, "Missing an arrow symbol or attributes here.");
  #else
    //chart.Error.Error(CHART_POS(@$).end, "Missing an arrow or attributes symbol here.");
	//Keep list of arrow_end strings
    (yyval.stringposlist_nocsh) = (yyvsp[0].stringposlist);
  #endif
}
#line 7881 "block_csh_lang.cc"
    break;

  case 227: /* arrow_needs_semi: arrowend_list arrowsymbol  */
#line 3620 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a block name or coordinate here.");
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a block name or coordinate here.");
    (yyval.stringposlist_nocsh) = new StringWithPosList;
    delete (yyvsp[-1].stringposlist);
  #endif
    (void)(yyvsp[0].arrowtype); //to suppress bison warning of unused value.
}
#line 7905 "block_csh_lang.cc"
    break;

  case 228: /* arrow_needs_semi: arrow_end arrowsymbol  */
#line 3640 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a block name or coordinate here.");
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a block name or coordinate here.");
    (yyval.stringposlist_nocsh) = new StringWithPosList;
    free((yyvsp[-1].multi_str).str);
  #endif
    (void)(yyvsp[0].arrowtype); //to suppress bison warning of unused value.
}
#line 7928 "block_csh_lang.cc"
    break;

  case 229: /* arrow_needs_semi: arrowend_list arrowsymbol arrowend_list_solo  */
#line 3659 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[0].stringposlist);
  #else
    if (!chart.SkipContent())
        chart.StashArrows((yyvsp[-2].stringposlist), (yyvsp[-1].arrowtype), (yyvsp[0].stringposlist), {nullptr, nullptr}, CHART_POS((yylsp[-1])));
    (yyval.stringposlist_nocsh) = (yyvsp[0].stringposlist);
  #endif
    delete (yyvsp[-2].stringposlist);
}
#line 7955 "block_csh_lang.cc"
    break;

  case 230: /* arrow_needs_semi: arrow_end arrowsymbol arrowend_list_solo  */
#line 3682 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-2]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[0].stringposlist);
  #else
    if (!chart.SkipContent()) {
        auto one = std::make_unique<StringWithPosList>();
        if ((yyvsp[-2].multi_str).str && !(yyvsp[-2].multi_str).had_error)
            one->push_back({(yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2]))});
        chart.StashArrows(one.get(), (yyvsp[-1].arrowtype), (yyvsp[0].stringposlist), {nullptr, nullptr}, CHART_POS((yylsp[-1])));
    }
    (yyval.stringposlist_nocsh) = (yyvsp[0].stringposlist);
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 7986 "block_csh_lang.cc"
    break;

  case 231: /* arrow_needs_semi: arrowend_list arrowsymbol arrowend_list_solo full_attrlist_with_label  */
#line 3709 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-3]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
    else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[-3].stringposlist);
    delete (yyvsp[-1].stringposlist);
  #else
    if (!chart.SkipContent())
        chart.StashArrows((yyvsp[-3].stringposlist), (yyvsp[-2].arrowtype), (yyvsp[-1].stringposlist), {(yyvsp[0].attributelist), nullptr}, CHART_POS((yylsp[-2])));
    else
       delete (yyvsp[0].attributelist);
    delete (yyvsp[-3].stringposlist);
    (yyval.stringposlist_nocsh) = (yyvsp[-1].stringposlist);
  #endif
}
#line 8020 "block_csh_lang.cc"
    break;

  case 232: /* arrow_needs_semi: arrow_end arrowsymbol arrowend_list_solo full_attrlist_with_label  */
#line 3739 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-3]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
    else if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::ENTITY) ||
        csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    if (!chart.SkipContent()) {
        auto one = std::make_unique<StringWithPosList>();
        if ((yyvsp[-3].multi_str).str && !(yyvsp[-3].multi_str).had_error)
            one->push_back({(yyvsp[-3].multi_str).str, CHART_POS((yylsp[-3]))});
        chart.StashArrows(one.get(), (yyvsp[-2].arrowtype), (yyvsp[-1].stringposlist), {(yyvsp[0].attributelist), nullptr}, CHART_POS((yylsp[-2])));
    } else
       delete (yyvsp[0].attributelist);
    (yyval.stringposlist_nocsh) = (yyvsp[-1].stringposlist);
  #endif
    free((yyvsp[-3].multi_str).str);
}
#line 8057 "block_csh_lang.cc"
    break;

  case 233: /* arrow_needs_semi: arrow_cont arrowsymbol arrowend_list_solo  */
#line 3772 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[0].stringposlist);
  #else
    if (!chart.SkipContent())
        chart.StashArrows((yyvsp[-2].stringposlist_nocsh), (yyvsp[-1].arrowtype), (yyvsp[0].stringposlist), {nullptr, nullptr}, CHART_POS((yylsp[-1])));
    delete (yyvsp[-2].stringposlist_nocsh);
    (yyval.stringposlist_nocsh) = (yyvsp[0].stringposlist);
  #endif
}
#line 8081 "block_csh_lang.cc"
    break;

  case 234: /* arrow_needs_semi: arrow_cont arrowsymbol arrowend_list_solo full_attrlist_with_label  */
#line 3792 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
    else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    if (!chart.SkipContent())
        chart.StashArrows((yyvsp[-3].stringposlist_nocsh), (yyvsp[-2].arrowtype), (yyvsp[-1].stringposlist), {(yyvsp[0].attributelist), nullptr}, CHART_POS((yylsp[-2])));
    else
       delete (yyvsp[0].attributelist);
    delete (yyvsp[-3].stringposlist_nocsh);
    (yyval.stringposlist_nocsh) = (yyvsp[-1].stringposlist);
  #endif
}
#line 8111 "block_csh_lang.cc"
    break;

  case 235: /* arrow_no_semi: arrowend_list arrowsymbol arrowend_list_solo full_attrlist_with_arrow_labels  */
#line 3819 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    if (!chart.SkipContent())
        chart.StashArrows((yyvsp[-3].stringposlist), (yyvsp[-2].arrowtype), (yyvsp[-1].stringposlist), std::move((yyvsp[0].arrowattrs)), CHART_POS((yylsp[-2])));
    else
       (yyvsp[0].arrowattrs).Free();
    (yyval.stringposlist_nocsh) = (yyvsp[-1].stringposlist);
  #endif
    delete (yyvsp[-3].stringposlist);
}
#line 8137 "block_csh_lang.cc"
    break;

  case 236: /* arrow_no_semi: arrow_end arrowsymbol arrowend_list_solo full_attrlist_with_arrow_labels  */
#line 3841 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
        if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-3]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    if (!chart.SkipContent()) {
        auto one = std::make_unique<StringWithPosList>();
        if ((yyvsp[-3].multi_str).str && !(yyvsp[-3].multi_str).had_error)
            one->push_back({(yyvsp[-3].multi_str).str, CHART_POS((yylsp[-3]))});
        chart.StashArrows(one.get(), (yyvsp[-2].arrowtype), (yyvsp[-1].stringposlist), std::move((yyvsp[0].arrowattrs)), CHART_POS((yylsp[-2])));
    } else
       (yyvsp[0].arrowattrs).Free();
    (yyval.stringposlist_nocsh) = (yyvsp[-1].stringposlist);
  #endif
    free((yyvsp[-3].multi_str).str);
}
#line 8169 "block_csh_lang.cc"
    break;

  case 237: /* arrow_no_semi: arrow_cont arrowsymbol arrowend_list_solo full_attrlist_with_arrow_labels  */
#line 3869 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_SYMBOL);
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ENTITY)) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[-1]))) {
        csh.AddEntityNamesAtTheEnd("Make the arrow or line start or end at block %s.");
        csh.hintStatus = HINT_READY;
        csh.hintSource = EHintSourceType::ENTITY;
    }
    delete (yyvsp[-1].stringposlist);
  #else
    if (!chart.SkipContent())
        chart.StashArrows((yyvsp[-3].stringposlist_nocsh), (yyvsp[-2].arrowtype), (yyvsp[-1].stringposlist), std::move((yyvsp[0].arrowattrs)), CHART_POS((yylsp[-2])));
    else
       (yyvsp[0].arrowattrs).Free();
    delete (yyvsp[-3].stringposlist_nocsh);
    (yyval.stringposlist_nocsh) = (yyvsp[-1].stringposlist);
  #endif
}
#line 8195 "block_csh_lang.cc"
    break;

  case 240: /* full_attrlist_with_arrow_labels: full_attrlist_with_label braced_instrlist  */
#line 3895 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
    //return 1 if we had a braced arclist
    (yyval.arrowattrs) = 1;
  #else
    (yyval.arrowattrs).al = (yyvsp[-1].attributelist);
    (yyval.arrowattrs).bl = (yyvsp[0].instruction_list);
  #endif
}
#line 8213 "block_csh_lang.cc"
    break;

  case 241: /* full_attrlist_with_arrow_labels: braced_instrlist  */
#line 3909 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    //return 1 if we had a braced arclist
    (yyval.arrowattrs) = 1;
  #else
    (yyval.arrowattrs).al = nullptr;
    (yyval.arrowattrs).bl = (yyvsp[0].instruction_list);
  #endif
}
#line 8227 "block_csh_lang.cc"
    break;

  case 242: /* opt_percent: %empty  */
#line 3920 "block_lang.yy"
{
    (yyval.condition) = 0;
}
#line 8235 "block_csh_lang.cc"
    break;

  case 243: /* opt_percent: TOK_PERCENT  */
#line 3924 "block_lang.yy"
{
    (yyval.condition) = 1;
}
#line 8243 "block_csh_lang.cc"
    break;

  case 244: /* arrow_label_number: TOK_NUMBER opt_percent TOK_PLUS TOK_NUMBER opt_percent  */
#line 3929 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
  #else
    auto num = ArrowLabel::ConvertPosition(chart, (yyvsp[-4].str), (yyvsp[-3].condition), CHART_POS2((yylsp[-4]), (yylsp[-3])), (yyvsp[-1].str), (yyvsp[0].condition), CHART_POS2((yylsp[-1]), (yylsp[0])));
    if (num)
        (yyval.arrowlabel) = new ArrowLabel(chart, num.value().first, num.value().second, CHART_POS((yyloc)));
    else
        (yyval.arrowlabel) = nullptr;
  #endif
}
#line 8259 "block_csh_lang.cc"
    break;

  case 245: /* arrow_label_number: TOK_NUMBER opt_percent TOK_MINUS TOK_NUMBER opt_percent  */
#line 3941 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
    free((yyvsp[-4].str));
    free((yyvsp[-1].str));
  #else
    auto num = ArrowLabel::ConvertPosition(chart, (yyvsp[-4].str), (yyvsp[-3].condition), CHART_POS2((yylsp[-4]), (yylsp[-3])), (yyvsp[-1].str), (yyvsp[0].condition), CHART_POS2((yylsp[-1]), (yylsp[0])));
    if (num)
        (yyval.arrowlabel) = new ArrowLabel(chart, num.value().first, -num.value().second, CHART_POS((yyloc)));
    else
        (yyval.arrowlabel) = nullptr;
  #endif
}
#line 8277 "block_csh_lang.cc"
    break;

  case 246: /* arrow_label_number: TOK_NUMBER opt_percent TOK_NUMBER opt_percent  */
#line 3955 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
    free((yyvsp[-3].str));
    free((yyvsp[-1].str));
  #else
    auto num = ArrowLabel::ConvertPosition(chart, (yyvsp[-3].str), (yyvsp[-2].condition), CHART_POS2((yylsp[-3]), (yylsp[-2])), (yyvsp[-1].str), (yyvsp[0].condition), CHART_POS2((yylsp[-1]), (yylsp[0])));
    if (num)
        (yyval.arrowlabel) = new ArrowLabel(chart, num.value().first, num.value().second, CHART_POS((yyloc)));
    else
        (yyval.arrowlabel) = nullptr;
  #endif
}
#line 8295 "block_csh_lang.cc"
    break;

  case 247: /* arrow_label_number: TOK_NUMBER opt_percent TOK_PLUS  */
#line 3969 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
    csh.AddCSH_Error((yylsp[0]), "Need digits after sign.");
    free((yyvsp[-2].str));
  #else
    auto num = ArrowLabel::ConvertPosition(chart, (yyvsp[-2].str), (yyvsp[-1].condition), CHART_POS2((yylsp[-2]), (yylsp[-1])));
    if (num)
        (yyval.arrowlabel) = new ArrowLabel(chart, num.value().first, num.value().second, CHART_POS((yyloc)));
    else
        (yyval.arrowlabel) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Need digits after sign.");
  #endif
}
#line 8314 "block_csh_lang.cc"
    break;

  case 248: /* arrow_label_number: TOK_NUMBER opt_percent TOK_MINUS  */
#line 3984 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
    csh.AddCSH_Error((yylsp[0]), "Need digits after sign.");
    free((yyvsp[-2].str));
  #else
    auto num = ArrowLabel::ConvertPosition(chart, (yyvsp[-2].str), (yyvsp[-1].condition), CHART_POS2((yylsp[-2]), (yylsp[-1])));
    if (num)
        (yyval.arrowlabel) = new ArrowLabel(chart, num.value().first, num.value().second, CHART_POS((yyloc)));
    else
        (yyval.arrowlabel) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Need digits after sign.");
  #endif
}
#line 8333 "block_csh_lang.cc"
    break;

  case 249: /* arrow_label_number: TOK_NUMBER opt_percent  */
#line 3999 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
    free((yyvsp[-1].str));
  #else
    auto num = ArrowLabel::ConvertPosition(chart, (yyvsp[-1].str), (yyvsp[0].condition), CHART_POS2((yylsp[-1]), (yylsp[0])));
    if (num)
        (yyval.arrowlabel) = new ArrowLabel(chart, num.value().first, num.value().second, CHART_POS((yyloc)));
    else
        (yyval.arrowlabel) = nullptr;
  #endif
}
#line 8350 "block_csh_lang.cc"
    break;

  case 251: /* arrow_label_number_with_extend: TOK_EXTEND_COMMAND arrow_label_number  */
#line 4014 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
  #else
    if ((yyvsp[0].arrowlabel))
        (yyvsp[0].arrowlabel)->is_extension = true;
  #endif
    free((yyvsp[-1].str));
    (yyval.arrowlabel) = (yyvsp[0].arrowlabel);
}
#line 8365 "block_csh_lang.cc"
    break;

  case 252: /* arrow_label_number_with_extend: TOK_EXTEND_COMMAND  */
#line 4025 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing number for arrow label.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing number for arrow label. Ignoring arrow label.");
  #endif
    free((yyvsp[0].str));
    (yyval.arrowlabel) = nullptr;
}
#line 8380 "block_csh_lang.cc"
    break;

  case 253: /* arrow_label: arrow_label_number_with_extend full_attrlist_with_label  */
#line 4037 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        ArrowLabel::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        ArrowLabel::AttributeValues(csh.hintAttrName, csh);
  #else
    if ((yyvsp[-1].arrowlabel) && (yyvsp[0].attributelist))
        (yyvsp[-1].arrowlabel)->AddAttributeList((yyvsp[0].attributelist));
    else
        delete (yyvsp[0].attributelist);
    (yyval.arrowlabel) = (yyvsp[-1].arrowlabel);
  #endif
}
#line 8399 "block_csh_lang.cc"
    break;

  case 254: /* arrow_label: TOK_MARK_COMMAND arrow_label_number_with_extend full_attrlist_with_label  */
#line 4052 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        ArrowLabel::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        ArrowLabel::AttributeValues(csh.hintAttrName, csh);
  #else
    if ((yyvsp[-1].arrowlabel) && (yyvsp[0].attributelist))
        (yyvsp[-1].arrowlabel)->AddAttributeList((yyvsp[0].attributelist));
    else
        delete (yyvsp[0].attributelist);
    if ((yyvsp[-1].arrowlabel))
        (yyvsp[-1].arrowlabel)->has_marker = true;
    (yyval.arrowlabel) = (yyvsp[-1].arrowlabel);
  #endif
    free((yyvsp[-2].str));
}
#line 8422 "block_csh_lang.cc"
    break;

  case 255: /* arrow_label: TOK_MARK_COMMAND arrow_label_number_with_extend  */
#line 4071 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
  #else
    if ((yyvsp[0].arrowlabel))
        (yyvsp[0].arrowlabel)->has_marker = true;
    (yyval.arrowlabel) = (yyvsp[0].arrowlabel);
  #endif
    free((yyvsp[-1].str));
}
#line 8437 "block_csh_lang.cc"
    break;

  case 256: /* arrow_label: TOK_MARK_COMMAND  */
#line 4082 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddMarkExtendToHints(false, true);
		csh.hintStatus = HINT_READY;
	}
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing label position.");
  #else
    (yyval.arrowlabel) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing label position.");
  #endif
    free((yyvsp[0].str));
}
#line 8456 "block_csh_lang.cc"
    break;

  case 257: /* optlist: opt  */
#line 4105 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction))
        (yyval.instruction_list) = (new BlockInstrList)->Append((yyvsp[0].instruction)); /* New list */
    else
        (yyval.instruction_list) = new BlockInstrList;
  #endif
}
#line 8469 "block_csh_lang.cc"
    break;

  case 258: /* optlist: optlist TOK_COMMA opt  */
#line 4114 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction)) ((yyvsp[-2].instruction_list))->Append((yyvsp[0].instruction));     /* Add to existing list */
    (yyval.instruction_list) = ((yyvsp[-2].instruction_list));
  #endif
  #endif
}
#line 8488 "block_csh_lang.cc"
    break;

  case 259: /* optlist: optlist TOK_COMMA  */
#line 4129 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an option here.");
  #endif
}
#line 8505 "block_csh_lang.cc"
    break;

  case 260: /* optlist: optlist TOK_COMMA error  */
#line 4142 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "An option expected here.");
  #else
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "I am not sure what is coming here.");
  #endif
}
#line 8523 "block_csh_lang.cc"
    break;

  case 261: /* opt: entity_string TOK_EQUAL attrvalue  */
#line 4158 "block_lang.yy"
{
  //$3 may contain position escapes and need handling of csh.addEntityNamesAtEnd
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, (yyvsp[-2].multi_str).str);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated((yylsp[0]))) {
        if (!csh.HandleBlockNamePlus((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, EHintSourceType::ATTR_VALUE)) {
            csh.hintStatus = HINT_NONE;
            if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
                BlockChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
                csh.hintStatus = HINT_READY;
            }
        }
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        BlockChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
	if (CaseInsensitiveEqual((yyvsp[-2].multi_str).str, "pedantic")) {
		if (CaseInsensitiveEqual((yyvsp[0].multi_str).str, "yes"))
			csh.Context2.back().pedantic = true;
		else if (CaseInsensitiveEqual((yyvsp[0].multi_str).str, "no"))
			csh.Context2.back().pedantic = false;
	}
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error || (chart.SkipContent() && ((yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param))) {
    } else if (Attribute *p = chart.CreateAttribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])))) {
        chart.AddAttribute(*p);
        delete p;
    } 
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 8568 "block_csh_lang.cc"
    break;

  case 262: /* opt: entity_string TOK_EQUAL  */
#line 4199 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing option value.");
    if (!(yyvsp[-1].multi_str).had_error && csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str)) {
        BlockChart::AttributeValues((yyvsp[-1].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 8592 "block_csh_lang.cc"
    break;

  case 263: /* usestylemodifier: TOK_COMMAND_BLOCKS  */
#line 4225 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #endif
    (yyval.use_keywords) = USE_KEYWORD_BLOCKS;
    free((yyvsp[0].str));
}
#line 8604 "block_csh_lang.cc"
    break;

  case 264: /* usestylemodifier: TOK_COMMAND_ARROWS  */
#line 4233 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #endif
    (yyval.use_keywords) = USE_KEYWORD_ARROWS;
    free((yyvsp[0].str));
}
#line 8616 "block_csh_lang.cc"
    break;

  case 265: /* usestylemodifier: TOK_COMMAND_ARROWS TOK_COMMAND_BLOCKS  */
#line 4241 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #endif
    (yyval.use_keywords) = EUseKeywords(USE_KEYWORD_BLOCKS | USE_KEYWORD_ARROWS);
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 8630 "block_csh_lang.cc"
    break;

  case 266: /* usestylemodifier: TOK_COMMAND_BLOCKS TOK_COMMAND_ARROWS  */
#line 4251 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #endif
    (yyval.use_keywords) = EUseKeywords(USE_KEYWORD_BLOCKS | USE_KEYWORD_ARROWS);
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 8644 "block_csh_lang.cc"
    break;

  case 267: /* usestylemode: TOK_COMMAND_USESTYLE  */
#line 4263 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #endif
    (yyval.use_keywords) = USE_KEYWORD_ALL;
    free((yyvsp[0].str));
}
#line 8656 "block_csh_lang.cc"
    break;

  case 268: /* usestylemode: usestylemodifier TOK_COMMAND_USESTYLE  */
#line 4271 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddUseKeywordsToHints((yyvsp[-1].use_keywords));
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.use_keywords) = (yyvsp[-1].use_keywords);
    free((yyvsp[0].str));
}
#line 8672 "block_csh_lang.cc"
    break;

  case 269: /* usestylemode: usestylemodifier entity_string  */
#line 4283 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddUseKeywordsToHints((yyvsp[-1].use_keywords));
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "Missing 'use' keyword.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing 'use' keyword. Ignoring this line.");
  #endif
    (yyval.use_keywords) = USE_KEYWORD_NONE;
    free((yyvsp[0].multi_str).str);
}
#line 8690 "block_csh_lang.cc"
    break;

  case 270: /* usestylemode: usestylemodifier  */
#line 4297 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'use' keyword.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddUseKeywordsToHints((yyvsp[0].use_keywords));
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing 'use' keyword. Ignoring this line.");
  #endif
    (yyval.use_keywords) = USE_KEYWORD_NONE;
}
#line 8707 "block_csh_lang.cc"
    break;

  case 271: /* usestyle: usestylemode attrlist  */
#line 4311 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockStyle(EBlockStyleType::Running, EColorMeaning::FILL).AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockStyle(EBlockStyleType::Running, EColorMeaning::FILL).AttributeValues(csh.hintAttrName, csh);
	//If use includes blocks and attrlist contained a shape==xxx
	//we set the default shape for this context
	if ((yyvsp[0].attributelist)>=-1 && ((yyvsp[-1].use_keywords) & USE_KEYWORD_BLOCKS))
		csh.Context2.back().def_shape = (yyvsp[0].attributelist);
    csh.DropBlockMentions((yylsp[0])); //No chart option takes a block name as value in any form
  #else
    (yyval.usecommandhelper).keywords = (yyvsp[-1].use_keywords);
    (yyval.usecommandhelper).attrs = (yyvsp[0].attributelist);
  #endif
}
#line 8728 "block_csh_lang.cc"
    break;

  case 272: /* usestyle: usestylemode  */
#line 4328 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].use_keywords))
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing style or attribute name to apply.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        BlockStyle(EBlockStyleType::Running, EColorMeaning::FILL).AttributeNames(csh);
        csh.hintStatus = HINT_READY; //dont add linebegin hints later
    }
  #else
    if ((yyvsp[0].use_keywords))
        chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing style or attribute name to apply. Ignoring statement.");
    (yyval.usecommandhelper).keywords = (yyvsp[0].use_keywords);
    (yyval.usecommandhelper).attrs = nullptr;
  #endif
}
#line 8748 "block_csh_lang.cc"
    break;

  case 274: /* styledeflist: styledeflist TOK_COMMA styledef  */
#line 4348 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-2].instruction_list) && (yyvsp[0].instruction_list))
        (yyvsp[-2].instruction_list)->Append((yyvsp[0].instruction_list));
    if ((yyvsp[-2].instruction_list)) {
        (yyval.instruction_list) = (yyvsp[-2].instruction_list);
        delete (yyvsp[0].instruction_list);
    } else
        (yyval.instruction_list) = (yyvsp[0].instruction_list);
  #endif
}
#line 8769 "block_csh_lang.cc"
    break;

  case 275: /* styledeflist: styledeflist TOK_COMMA  */
#line 4365 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing style definition here.", "Try just removing the comma.");
#endif
}
#line 8785 "block_csh_lang.cc"
    break;

  case 276: /* styledef: stylenameposlist full_attrlist  */
#line 4378 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_NameList((yyvsp[-1].stringposlist), (yylsp[-1]), COLOR_STYLENAME, COLOR_COMMA);
    if (!csh.Contexts.back().SkipContent())
        for (auto &str : *((yyvsp[-1].stringposlist)))
            if (str.name.length() && csh.ForbiddenStyles.find(str.name) == csh.ForbiddenStyles.end())
                csh.Contexts.back().StyleNames.insert(str.name);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        BlockStyle(EBlockStyleType::Unspecified, EColorMeaning::FILL).AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        BlockStyle(EBlockStyleType::Unspecified, EColorMeaning::FILL).AttributeValues(csh.hintAttrName, csh);
  #else
    if (chart.SkipContent())
        delete (yyvsp[0].attributelist);
    else
        chart.AddAttributeListToStyleListWithFilePos((yyvsp[0].attributelist), (yyvsp[-1].stringposlist)); //deletes $2
    (yyval.instruction_list) = nullptr;
  #endif
    delete((yyvsp[-1].stringposlist));
}
#line 8810 "block_csh_lang.cc"
    break;

  case 277: /* styledef: stylenameposlist  */
#line 4399 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_NameList((yyvsp[0].stringposlist), (yylsp[0]), COLOR_STYLENAME, COLOR_COMMA);
    csh.AddCSH_ErrorAfter((yyloc), "Missing attribute definitons in square brackets ('[' and ']').");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing attribute definitons in square brackets ('[' and ']').");
    (yyval.instruction_list) = nullptr;
  #endif
    delete((yyvsp[0].stringposlist));
}
#line 8825 "block_csh_lang.cc"
    break;

  case 278: /* stylenameposlist: string  */
#line 4411 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
    (yyval.stringposlist) = new CshStringWithPosList;
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
  #else
    (yyval.stringposlist) = new StringWithPosList;
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 8846 "block_csh_lang.cc"
    break;

  case 279: /* stylenameposlist: stylenameposlist TOK_COMMA  */
#line 4428 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a style name to (re)define.");
    if (csh.CheckHintAfter((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
    csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re)define.");
  #endif
    (yyval.stringposlist) = (yyvsp[-1].stringposlist);
}
#line 8863 "block_csh_lang.cc"
    break;

  case 280: /* stylenameposlist: stylenameposlist TOK_COMMA string  */
#line 4441 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyvsp[-2].stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
  #else
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyvsp[-2].stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
  #endif
    (yyval.stringposlist) = (yyvsp[-2].stringposlist);
    free((yyvsp[0].multi_str).str);
}
#line 8883 "block_csh_lang.cc"
    break;

  case 281: /* shapedef: entity_string  */
#line 4463 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.AddCSH_ErrorAfter((yyloc), ("Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '"+string((yyvsp[0].multi_str).str ? (yyvsp[0].multi_str).str : "") +"'.").c_str());
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '"+string((yyvsp[0].multi_str).str ? (yyvsp[0].multi_str).str : "") +"'.");
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 8898 "block_csh_lang.cc"
    break;

  case 282: /* shapedef: entity_string TOK_OCBRACKET  */
#line 4474 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yylsp[0]));
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.AddCSH_ErrorAfter((yyloc), ("Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '"+string((yyvsp[-1].multi_str).str ? (yyvsp[-1].multi_str).str: "") +"'.").c_str());
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '"+string((yyvsp[-1].multi_str).str ? (yyvsp[-1].multi_str).str: "") +"'.");
  #endif
    free((yyvsp[-1].multi_str).str);
    (yyvsp[0].input_text_ptr); //suppress
}
#line 8916 "block_csh_lang.cc"
    break;

  case 283: /* shapedef: entity_string TOK_OCBRACKET shapedeflist  */
#line 4488 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yylsp[-1])+(yylsp[0]));
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-1]), COLOR_BRACE);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a closing brace ('}').");
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddShapeName((yyvsp[-2].multi_str).str, (yyvsp[0].shape));
    delete (yyvsp[0].shape);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), CHART_POS_AFTER((yylsp[0])), "Here is the corresponding '{'.");
    if (!(yyvsp[-2].multi_str).had_error && (yyvsp[-2].multi_str).str && (yyvsp[0].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-2].multi_str).str), CHART_POS_START((yylsp[-2])), chart.file_url, chart.file_info, std::move(*(yyvsp[0].shape)), chart.Error);
        delete (yyvsp[0].shape);
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    (yyvsp[-1].input_text_ptr); //suppress
}
#line 8943 "block_csh_lang.cc"
    break;

  case 284: /* shapedef: entity_string TOK_OCBRACKET shapedeflist TOK_CCBRACKET  */
#line 4511 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yylsp[-2])+(yylsp[0]));
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH((yylsp[-3]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-2]), COLOR_BRACE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddShapeName((yyvsp[-3].multi_str).str, (yyvsp[-1].shape));
    delete (yyvsp[-1].shape);
  #else
    if (!(yyvsp[-3].multi_str).had_error && (yyvsp[-3].multi_str).str && (yyvsp[-1].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-3].multi_str).str), CHART_POS_START((yylsp[-3])), chart.file_url, chart.file_info, std::move(*(yyvsp[-1].shape)), chart.Error);
        delete (yyvsp[-1].shape);
    }
  #endif
    free((yyvsp[-3].multi_str).str);
    (yyvsp[-2].input_text_ptr); //suppress
    (yyvsp[0].input_text_ptr); //suppress
}
#line 8969 "block_csh_lang.cc"
    break;

  case 285: /* shapedef: entity_string TOK_OCBRACKET shapedeflist error TOK_CCBRACKET  */
#line 4533 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yylsp[-3])+(yylsp[0]));
    if (!(yyvsp[-4].multi_str).had_error)
        csh.AddCSH((yylsp[-4]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-3]), COLOR_BRACE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    if (!(yyvsp[-4].multi_str).had_error)
        csh.AddShapeName((yyvsp[-4].multi_str).str, (yyvsp[-2].shape));
    delete (yyvsp[-2].shape);
    csh.AddCSH_Error((yylsp[-1]), "Only numbers can come after shape commands.");
  #else
    if (!(yyvsp[-4].multi_str).had_error && (yyvsp[-4].multi_str).str && (yyvsp[-2].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-4].multi_str).str), CHART_POS_START((yylsp[-4])), chart.file_url, chart.file_info, std::move(*(yyvsp[-2].shape)), chart.Error);
        delete (yyvsp[-2].shape);
    }
  #endif
    free((yyvsp[-4].multi_str).str);
    (yyvsp[-3].input_text_ptr); //suppress
    (yyvsp[0].input_text_ptr); //suppress
}
#line 8996 "block_csh_lang.cc"
    break;

  case 286: /* shapedeflist: shapeline TOK_SEMICOLON  */
#line 4557 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if ((yyvsp[-1].shapeelement) && (yyvsp[-1].shapeelement)[0]) {
        (yyval.shape) = new std::vector<std::string>;
        (yyval.shape)->emplace_back((yyvsp[-1].shapeelement));
        free((yyvsp[-1].shapeelement));
    } else
        (yyval.shape) = nullptr;
  #else
    (yyval.shape) = new Shape;
    if ((yyvsp[-1].shapeelement)) {
        ((yyval.shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
        delete (yyvsp[-1].shapeelement);
    }
  #endif
}
#line 9018 "block_csh_lang.cc"
    break;

  case 287: /* shapedeflist: error shapeline TOK_SEMICOLON  */
#line 4575 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-2]), "I do not understand this.");
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if ((yyvsp[-1].shapeelement) && (yyvsp[-1].shapeelement)[0]) {
        (yyval.shape) = new std::vector<std::string>;
        (yyval.shape)->emplace_back((yyvsp[-1].shapeelement));
        free((yyvsp[-1].shapeelement));
    } else
        (yyval.shape) = nullptr;
#else
    (yyval.shape) = new Shape;
    if ((yyvsp[-1].shapeelement)) {
        ((yyval.shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
        delete (yyvsp[-1].shapeelement);
    }
  #endif
}
#line 9041 "block_csh_lang.cc"
    break;

  case 288: /* shapedeflist: shapedeflist shapeline TOK_SEMICOLON  */
#line 4594 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if ((yyvsp[-1].shapeelement) && (yyvsp[-1].shapeelement)[0]) {
        if ((yyvsp[-2].shape)==nullptr)
            (yyval.shape) = new std::vector<std::string>;
        (yyval.shape)->emplace_back((yyvsp[-1].shapeelement));
        free((yyvsp[-1].shapeelement));
    } else
        (yyval.shape) = (yyvsp[-2].shape);
  #else
    if ((yyvsp[-1].shapeelement)) {
        ((yyvsp[-2].shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
        delete (yyvsp[-1].shapeelement);
    }
    (yyval.shape) = (yyvsp[-2].shape);
  #endif
}
#line 9064 "block_csh_lang.cc"
    break;

  case 289: /* shapedeflist: shapedeflist error  */
#line 4613 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Only numbers can come after shape commands.");
  #else
  #endif
    (yyval.shape) = (yyvsp[-1].shape);
}
#line 9076 "block_csh_lang.cc"
    break;

  case 290: /* shapeline: TOK_SHAPE_COMMAND  */
#line 4622 "block_lang.yy"
{
    const int num_args = 0;
    const int should_args = ShapeElement::GetNumArgs((yyvsp[0].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (should_args != num_args)
        csh.AddCSH_ErrorAfter((yyloc), ShapeElement::ErrorMsg((yyvsp[0].shapecommand), num_args));
    (yyval.shapeelement) = nullptr; //no port name
  #else
    (yyval.shapeelement) = nullptr;
    if (should_args != num_args)
        chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[0].shapecommand), num_args).append(" Ignoring line."));
    else
        (yyval.shapeelement) = new ShapeElement((yyvsp[0].shapecommand));
  #endif
}
#line 9097 "block_csh_lang.cc"
    break;

  case 291: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER  */
#line 4639 "block_lang.yy"
{
    const int num_args = 1;
    const int should_args = ShapeElement::GetNumArgs((yyvsp[-1].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (should_args != num_args) {
        std::string msg = ShapeElement::ErrorMsg((yyvsp[-1].shapecommand), num_args);
        switch (ShapeElement::GetNumArgs((yyvsp[-1].shapecommand))) {
        case 0:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
        }
    } else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG && (((yyvsp[0].str))[0]<'0' || ((yyvsp[0].str))[0]>'2' || ((yyvsp[0].str))[1]!=0))
        csh.AddCSH_Error((yylsp[0]), "S (section) commands require an integer between 0 and 2.");
    (yyval.shapeelement) = nullptr; //no port name
  #else
    (yyval.shapeelement) = nullptr;
    const double a = atof((yyvsp[0].str));
    if (should_args > num_args)
        chart.Error.Error(CHART_POS((yylsp[0])).end, ShapeElement::ErrorMsg((yyvsp[-1].shapecommand), num_args).append(" Ignoring line."));
    else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG && (a!=0 && a!=1 && a!=2))
        chart.Error.Error(CHART_POS_START((yylsp[0])), "S (section) commands require an integer between 0 and 2. Ignoring line.");
    else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG)
        (yyval.shapeelement) = new ShapeElement(ShapeElement::Type((yyvsp[-1].shapecommand) + unsigned(a)));
    else
        (yyval.shapeelement) = new ShapeElement((yyvsp[-1].shapecommand), a);
  #endif
  free((yyvsp[0].str));
}
#line 9130 "block_csh_lang.cc"
    break;

  case 292: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER  */
#line 4668 "block_lang.yy"
{
    const int num_args = 2;
    const int should_args = ShapeElement::GetNumArgs((yyvsp[-2].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (should_args != num_args) {
        std::string msg = ShapeElement::ErrorMsg((yyvsp[-2].shapecommand), num_args);
        switch (ShapeElement::GetNumArgs((yyvsp[-2].shapecommand))) {
        case 0:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
        case 1:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
        }
    }
    (yyval.shapeelement) = nullptr; //no port name
  #else
    (yyval.shapeelement) = nullptr;
    if (should_args > num_args)
        chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-2].shapecommand), num_args).append(" Ignoring line."));
    else
        (yyval.shapeelement) = new ShapeElement((yyvsp[-2].shapecommand), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 9159 "block_csh_lang.cc"
    break;

  case 293: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER alpha_string  */
#line 4693 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if ((yyvsp[-3].shapecommand)==ShapeElement::PORT) {
        (yyval.shapeelement) = (yyvsp[0].multi_str).str;
    } else {
        csh.AddCSH_Error((yylsp[0]), "You need to specify a number here.");
        (yyval.shapeelement) = nullptr; //no port name
        free((yyvsp[0].multi_str).str);
    }
  #else
    (yyval.shapeelement) = nullptr;
    if ((yyvsp[-3].shapecommand)!=ShapeElement::PORT)
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a number here. Ignoring line.");
    else
        (yyval.shapeelement) = new ShapeElement(atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), (yyvsp[0].multi_str).str);
    free((yyvsp[0].multi_str).str);
  #endif
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
}
#line 9185 "block_csh_lang.cc"
    break;

  case 294: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER alpha_string TOK_NUMBER  */
#line 4715 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
    if ((yyvsp[-4].shapecommand)==ShapeElement::PORT) {
        (yyval.shapeelement) = (yyvsp[-1].multi_str).str;
    } else {
        csh.AddCSH_Error((yylsp[-1]), "You need to specify a number here.");
        (yyval.shapeelement) = nullptr; //no port name
        free((yyvsp[-1].multi_str).str);
    }
  #else
    (yyval.shapeelement) = nullptr;
    if ((yyvsp[-4].shapecommand)!=ShapeElement::PORT)
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting a number here. Ignoring line.");
    else
        (yyval.shapeelement) = new ShapeElement(atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), (yyvsp[-1].multi_str).str, atof((yyvsp[0].str)));
    free((yyvsp[-1].multi_str).str);
  #endif
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[0].str));
}
#line 9212 "block_csh_lang.cc"
    break;

  case 295: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 4738 "block_lang.yy"
{
    const int num_args = 3;
    const int should_args = ShapeElement::GetNumArgs((yyvsp[-3].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if (should_args != num_args) {
        std::string msg = ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args);
        switch (ShapeElement::GetNumArgs((yyvsp[-3].shapecommand))) {
        case 0:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
        case 1:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
        case 2:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
        }
    } else if ((yyvsp[-3].shapecommand)==ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[0]), "You need to specify a port name here starting with a letter.");
    (yyval.shapeelement) = nullptr; //no port name
  #else
    (yyval.shapeelement) = nullptr;
    if (should_args > num_args)
        chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args).append(" Ignoring line."));
    else if ((yyvsp[-3].shapecommand)==ShapeElement::PORT)
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a port name here. Ignoring line.");
    else
        (yyval.shapeelement) = new ShapeElement((yyvsp[-3].shapecommand), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 9246 "block_csh_lang.cc"
    break;

  case 296: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 4768 "block_lang.yy"
{
    const int num_args = 4;
    const int should_args = ShapeElement::GetNumArgs((yyvsp[-4].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
    if (should_args != num_args) {
        std::string msg = ShapeElement::ErrorMsg((yyvsp[-4].shapecommand), num_args);
        switch (ShapeElement::GetNumArgs((yyvsp[-4].shapecommand))) {
        case 0:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
        case 1:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
        case 2:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
        case 3:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
        }
    }
    (yyval.shapeelement) = nullptr; //no port name
  #else
    (yyval.shapeelement) = nullptr;
    if (should_args > num_args)
        chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-4].shapecommand), num_args).append(" Ignoring line."));
    else
        (yyval.shapeelement) = new ShapeElement((yyvsp[-4].shapecommand), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 9279 "block_csh_lang.cc"
    break;

  case 297: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 4797 "block_lang.yy"
{
    const int num_args = 5;
    const int should_args = ShapeElement::GetNumArgs((yyvsp[-5].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_KEYWORD);
    if (should_args != num_args) {
        std::string msg = ShapeElement::ErrorMsg((yyvsp[-5].shapecommand), num_args);
        switch (ShapeElement::GetNumArgs((yyvsp[-5].shapecommand))) {
        case 0:  csh.AddCSH_Error((yylsp[-4]) + (yylsp[0]), std::move(msg)); break;
        case 1:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
        case 2:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
        case 3:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
        case 4:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
        }
    }
    (yyval.shapeelement) = nullptr; //no port name
  #else
    (yyval.shapeelement) = nullptr;
    if (should_args > num_args)
        chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-5].shapecommand), num_args).append(" Ignoring line."));
    else
        (yyval.shapeelement) = new ShapeElement((yyvsp[-5].shapecommand), atof((yyvsp[-4].str)), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-4].str));
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 9314 "block_csh_lang.cc"
    break;

  case 298: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 4828 "block_lang.yy"
{
    const int num_args = 6;
    const int should_args = ShapeElement::GetNumArgs((yyvsp[-6].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-6]), COLOR_KEYWORD);
    if (should_args != num_args) {
        std::string msg = ShapeElement::ErrorMsg((yyvsp[-6].shapecommand), num_args);
        switch (ShapeElement::GetNumArgs((yyvsp[-6].shapecommand))) {
        case 0:  csh.AddCSH_Error((yylsp[-5]) + (yylsp[0]), std::move(msg)); break;
        case 1:  csh.AddCSH_Error((yylsp[-4]) + (yylsp[0]), std::move(msg)); break;
        case 2:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
        case 3:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
        case 4:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
        case 5:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
        }
    }
    (yyval.shapeelement) = nullptr; //no port name
  #else
    (yyval.shapeelement) = nullptr;
    if (should_args > num_args)
        chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-6].shapecommand), num_args).append(" Ignoring line."));
    else
        (yyval.shapeelement) = new ShapeElement((yyvsp[-6].shapecommand), atof((yyvsp[-5].str)), atof((yyvsp[-4].str)), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-5].str));
  free((yyvsp[-4].str));
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 9351 "block_csh_lang.cc"
    break;

  case 299: /* colordeflist_actioned: colordeflist  */
#line 4866 "block_lang.yy"
{
    if ((yyvsp[0].stringposlist)) for (auto i = (yyvsp[0].stringposlist)->begin(); i!=(yyvsp[0].stringposlist)->end(); i++) {
        auto j = i++; //j equals name, i equals value
        if (i==(yyvsp[0].stringposlist)->end()) break;
  #ifdef C_S_H_IS_COMPILED
        ColorType color = csh.Contexts.back().Colors.GetColor(i->name);
        if (color.type!=ColorType::INVALID)
            csh.Contexts.back().Colors[j->name] = color;
  #else
        if (!chart.SkipContent())
            chart.MyCurrentContext().colors.AddColor(j->name, i->name, chart.Error, FileLineColRange(j->file_pos.start, i->file_pos.end));
  #endif
    }
    delete (yyvsp[0].stringposlist);
}
#line 9371 "block_csh_lang.cc"
    break;

  case 301: /* colordeflist: colordeflist TOK_COMMA colordef  */
#line 4885 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    if ((yyvsp[0].stringposlist)) {
        _ASSERT((yyvsp[0].stringposlist)->size()==2);
        if ((yyvsp[0].stringposlist)->front().name.length()==0) {
            if ((yyvsp[-2].stringposlist)) {
                (yyvsp[-2].stringposlist)->back().name.append(",").append((yyvsp[0].stringposlist)->back().name);
                (yyvsp[-2].stringposlist)->back().file_pos += (yyvsp[0].stringposlist)->back().file_pos;
            } else {
  #ifdef C_S_H_IS_COMPILED
                csh.AddCSH_Error((yyvsp[0].stringposlist)->back().file_pos, "Missing color name to (re)define.");
  #else
                chart.Error.Error((yyvsp[0].stringposlist)->back().file_pos.start, "Missing color name to (re)define. Ignoring this number here.");
  #endif
                delete (yyvsp[0].stringposlist);
                (yyvsp[0].stringposlist) = nullptr;
            }
        } else if ((yyvsp[-2].stringposlist)) {
            (yyvsp[-2].stringposlist)->insert((yyvsp[-2].stringposlist)->end(), std::make_move_iterator((yyvsp[0].stringposlist)->begin()), std::make_move_iterator((yyvsp[0].stringposlist)->end()));
        }
    }
    if ((yyvsp[-2].stringposlist)) {
        delete (yyvsp[0].stringposlist);
        (yyval.stringposlist) = (yyvsp[-2].stringposlist);
    } else
        (yyval.stringposlist) = (yyvsp[0].stringposlist);
}
#line 9409 "block_csh_lang.cc"
    break;

  case 302: /* colordeflist: colordeflist TOK_COMMA  */
#line 4919 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
  #endif
    (yyval.stringposlist) = (yyvsp[-1].stringposlist);
}
#line 9427 "block_csh_lang.cc"
    break;

  case 304: /* string_or_num: TOK_NUMBER  */
#line 4935 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 9435 "block_csh_lang.cc"
    break;

  case 305: /* colordef: alpha_string TOK_EQUAL string_or_num  */
#line 4940 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error) {
        csh.AddCSH((yylsp[-2]), COLOR_COLORNAME);
        (yyval.stringposlist) = new CshStringWithPosList({CshStringWithPos((yyvsp[-2].multi_str).str, (yylsp[-2])), CshStringWithPos((yyvsp[0].multi_str).str, (yylsp[0]))});
    } else
        (yyval.stringposlist) = nullptr;
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[-2].multi_str).had_error)
        (yyval.stringposlist) = new StringWithPosList({StringWithPos((yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2]))), StringWithPos((yyvsp[0].multi_str).str, CHART_POS((yylsp[0])))});
    else
        (yyval.stringposlist) = nullptr;
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 9465 "block_csh_lang.cc"
    break;

  case 306: /* colordef: alpha_string TOK_EQUAL TOK_PLUS string_or_num  */
#line 4966 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-3].multi_str).had_error) {
        csh.AddCSH((yylsp[-3]), COLOR_COLORNAME);
        (yyval.stringposlist) = new CshStringWithPosList({{(yyvsp[-3].multi_str).str, (yylsp[-3])}, {"++"+string((yyvsp[0].multi_str).str), (yylsp[0])}});
    } else
        (yyval.stringposlist) = nullptr;
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    csh.AddCSH((yylsp[-1]), COLOR_COLORDEF);
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[-3].multi_str).had_error)
        (yyval.stringposlist) = new StringWithPosList({{(yyvsp[-3].multi_str).str, CHART_POS((yylsp[-3]))}, {"++"+string((yyvsp[0].multi_str).str), CHART_POS((yylsp[0]))}});
    else
        (yyval.stringposlist) = nullptr;
  #endif
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 9496 "block_csh_lang.cc"
    break;

  case 307: /* colordef: alpha_string TOK_EQUAL  */
#line 4993 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error) {
        csh.AddCSH((yylsp[-1]), COLOR_COLORNAME);
        (yyval.stringposlist) = new CshStringWithPosList({{(yyvsp[-1].multi_str).str, (yylsp[-1])}, {"", (yylsp[0])}});
    } else
        (yyval.stringposlist) = nullptr;
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing color definition.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing color definition.");
    if (!(yyvsp[-1].multi_str).had_error)
        (yyval.stringposlist) = new StringWithPosList({{(yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1]))}, {"", CHART_POS((yylsp[0]))}});
    else
        (yyval.stringposlist) = nullptr;
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 9526 "block_csh_lang.cc"
    break;

  case 308: /* colordef: alpha_string  */
#line 5019 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_COLORNAME);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing equal sign ('=') and a color definition.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing equal sign ('=') and a color definition.");
  #endif
    free((yyvsp[0].multi_str).str);
    (yyval.stringposlist) = nullptr;
}
#line 9546 "block_csh_lang.cc"
    break;

  case 309: /* colordef: TOK_NUMBER  */
#line 5035 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    (yyval.stringposlist) = new CshStringWithPosList({{"", (yylsp[0])}, {(yyvsp[0].str), (yylsp[0])}});
  #else
    (yyval.stringposlist) = new StringWithPosList({{"", CHART_POS((yylsp[0]))}, {(yyvsp[0].str), CHART_POS((yylsp[0]))}});
  #endif
    free((yyvsp[0].str));
}
#line 9560 "block_csh_lang.cc"
    break;

  case 310: /* colordef: colordef TOK_NUMBER  */
#line 5045 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #endif
    if ((yyvsp[-1].stringposlist) && (yyvsp[-1].stringposlist)->size()) {
        if ((yyvsp[0].str)[0]=='+' || (yyvsp[0].str)[0]=='-')
            (yyval.stringposlist)->back().name.append((yyvsp[0].str));
        else
            (yyval.stringposlist)->back().name.append("+").append((yyvsp[0].str));
  #ifdef C_S_H_IS_COMPILED
        (yyval.stringposlist)->back().file_pos.last_pos = (yylsp[0]).last_pos;
  #else
        (yyval.stringposlist)->back().file_pos.end = CHART_POS((yylsp[0])).end;
  #endif
    }
    free((yyvsp[0].str));
    (yyval.stringposlist) = (yyvsp[-1].stringposlist);
}
#line 9582 "block_csh_lang.cc"
    break;

  case 311: /* usedesign: TOK_COMMAND_USEDESIGN string  */
#line 5068 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddDesignsToHints(true);
        csh.AddDesignsToHints(false);
        csh.hintStatus = HINT_READY; //dont add linebegin hints later
    }
  #else
    if (!(yyvsp[0].multi_str).had_error) {
        auto i = chart.Designs.find((yyvsp[0].multi_str).str);
        if (i==chart.Designs.end())
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Unknown design. Ignoring it.");
        else
            chart.MyCurrentContext().ApplyContextContent(i->second);
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 9609 "block_csh_lang.cc"
    break;

  case 312: /* usedesign: TOK_COMMAND_USEDESIGN  */
#line 5091 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing design name to apply.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddDesignsToHints(true);
        csh.AddDesignsToHints(false);
        csh.hintStatus = HINT_READY; //dont add linebegin hints later
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing design name to apply. Ignoring statement.");
  #endif
    free((yyvsp[0].str));
}
#line 9628 "block_csh_lang.cc"
    break;

  case 313: /* designdef: TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON TOK_CCBRACKET  */
#line 5108 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_DESIGNNAME);
    csh.AddCSH((yylsp[-1]), COLOR_SEMICOLON);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-3])+(yylsp[0]));
    if (!csh.Contexts.back().SkipContent()) {
        auto &d = csh.Contexts.back().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-4].str));
        if (i == d.end())
            d.emplace((yyvsp[-4].str), csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::LINE_START) ||
         csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) ) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent()) {
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-4].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-3]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
    free((yyvsp[-4].str));
    (yyvsp[0].input_text_ptr); //supress
}
#line 9672 "block_csh_lang.cc"
    break;

  case 314: /* designdef: TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON error TOK_CCBRACKET  */
#line 5148 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_DESIGNNAME);
    csh.AddCSH((yylsp[-2]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as part of a design definition.");
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-4])+(yylsp[0]));
    if (!csh.Contexts.back().SkipContent()) {
        auto &d = csh.Contexts.back().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-5].str));
        if (i == d.end())
            d.emplace((yyvsp[-5].str), csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween((yylsp[-4]), (yylsp[-3]), EHintSourceType::LINE_START) ||
         csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::LINE_START))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    if (!chart.SkipContent()) {
        //if closing brace missing, still do the design definition
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-5].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-4]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
    (yyvsp[0].input_text_ptr); //supress
}
#line 9717 "block_csh_lang.cc"
    break;

  case 315: /* scope_open_empty: TOK_OCBRACKET  */
#line 5191 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(true, EContextParse::NORMAL);
  #else
    //push empty color & style sets for design definition
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::NORMAL, EContextCreate::CLEAR);
  #endif
    (yyvsp[0].input_text_ptr); //supress
}
#line 9732 "block_csh_lang.cc"
    break;

  case 317: /* designelementlist: designelementlist TOK_SEMICOLON designelement  */
#line 5204 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SEMICOLON);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#endif
}
#line 9746 "block_csh_lang.cc"
    break;

  case 318: /* designelement: TOK_COMMAND_DEFCOLOR colordeflist_actioned  */
#line 5215 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    free((yyvsp[-1].str));
}
#line 9764 "block_csh_lang.cc"
    break;

  case 319: /* designelement: TOK_COMMAND_DEFCOLOR  */
#line 5229 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
  #endif
    free((yyvsp[0].str));
}
#line 9785 "block_csh_lang.cc"
    break;

  case 320: /* designelement: TOK_COMMAND_DEFSTYLE styledeflist  */
#line 5246 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    delete (yyvsp[0].instruction_list);
  #endif
    free((yyvsp[-1].str));
}
#line 9805 "block_csh_lang.cc"
    break;

  case 321: /* designelement: TOK_COMMAND_DEFSTYLE  */
#line 5262 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing style name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
  #endif
    free((yyvsp[0].str));
}
#line 9826 "block_csh_lang.cc"
    break;

  case 325: /* designoptlist: designoptlist TOK_COMMA designopt  */
#line 5283 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 9840 "block_csh_lang.cc"
    break;

  case 326: /* designoptlist: designoptlist TOK_COMMA  */
#line 5293 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 9854 "block_csh_lang.cc"
    break;

  case 327: /* designoptlist: designoptlist error  */
#line 5303 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Extra stuff after design options. Maybe missing a comma?");
  #endif
}
#line 9864 "block_csh_lang.cc"
    break;

  case 328: /* designopt: entity_string TOK_EQUAL TOK_NUMBER  */
#line 5310 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        BlockChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[-2].multi_str).had_error)
        chart.AddDesignAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 9889 "block_csh_lang.cc"
    break;

  case 329: /* designopt: entity_string TOK_EQUAL string  */
#line 5331 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        BlockChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[-2].multi_str).had_error)
        if (!chart.AddDesignAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])))))
            chart.Error.Error(CHART_POS_START((yylsp[-2])), "Unrecognized design option. Ignoring it.");
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 9916 "block_csh_lang.cc"
    break;

  case 330: /* designopt: entity_string TOK_EQUAL  */
#line 5354 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-1].multi_str).had_error && csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str)) {
        BlockChart::AttributeValues((yyvsp[-1].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value. Ignoring this.");
#endif
    free((yyvsp[-1].multi_str).str);
}
#line 9938 "block_csh_lang.cc"
    break;

  case 331: /* defproc: defprochelp1  */
#line 5379 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procdefhelper)) {
        if (chart.SkipContent()) {
            chart.Error.Error(CHART_POS_START((yyloc)), "Cannot define procedures inside a procedure.");
        } else if ((yyvsp[0].procdefhelper)->name.had_error) {
            //do nothing, error already reported
        } else if ((yyvsp[0].procdefhelper)->name.str==nullptr || (yyvsp[0].procdefhelper)->name.str[0]==0) {
            chart.Error.Error((yyvsp[0].procdefhelper)->linenum_name, "Missing a procedure name to (re-)define. Ignoring this.");
        } else if (!(yyvsp[0].procdefhelper)->had_error && (yyvsp[0].procdefhelper)->body) {
            if (chart.MyCurrentContext().num_error != chart.Error.GetErrorNum(true, false)) {
                chart.Error.Error(CHART_POS_START((yyloc)), "There are warnings or errors inside the procedure definition. Ignoring it.");
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].name = (yyvsp[0].procdefhelper)->name.str;
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].status = EDefProcResult::PROBLEM;
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].file_pos = (yyvsp[0].procdefhelper)->linenum_body;
            } else if ((yyvsp[0].procdefhelper)->body->status==EDefProcResult::OK || (yyvsp[0].procdefhelper)->body->status==EDefProcResult::EMPTY) {
                if ((yyvsp[0].procdefhelper)->parameters) {
                    auto &p = chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str] = *(yyvsp[0].procdefhelper)->body;
                    p.name = (yyvsp[0].procdefhelper)->name.str;
                    p.parameters = std::move(*(yyvsp[0].procdefhelper)->parameters);
                    if ((yyvsp[0].procdefhelper)->attrs) for (auto &a : *(yyvsp[0].procdefhelper)->attrs)
                        p.AddAttribute(*a, chart);
                    if ((yyvsp[0].procdefhelper)->body->status==EDefProcResult::EMPTY)
                        chart.Error.Warning((yyvsp[0].procdefhelper)->linenum_body, "Empty procedure. Is this what you want?");
                } else {
                     chart.Error.Error(CHART_POS_START((yyloc)), "Ill-formed procedure parameter list. Ignoring this procedure definition.");
                }
            } else {
                 chart.Error.Error(CHART_POS_START((yyloc)), "Ill-formed procedure body. Ignoring this procedure definition.");
            }
        }
        delete (yyvsp[0].procdefhelper);
    }
  #endif
}
#line 9979 "block_csh_lang.cc"
    break;

  case 332: /* defprochelp1: TOK_COMMAND_DEFPROC  */
#line 5418 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent()) {
        csh.AddCSH_Error((yylsp[0]), "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing procedure name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->linenum_name = CHART_POS_AFTER((yyloc));
  #endif
    free((yyvsp[0].str));
}
#line 10002 "block_csh_lang.cc"
    break;

  case 333: /* defprochelp1: TOK_COMMAND_DEFPROC defprochelp2  */
#line 5437 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent()) {
        csh.AddCSH_Error((yylsp[-1]), "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    }
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
  #endif
    free((yyvsp[-1].str));
}
#line 10023 "block_csh_lang.cc"
    break;

  case 334: /* defprochelp2: alpha_string  */
#line 5455 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PROCNAME);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
    free((yyvsp[0].multi_str).str);
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->name = (yyvsp[0].multi_str);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[0]));
  #endif
}
#line 10039 "block_csh_lang.cc"
    break;

  case 335: /* defprochelp2: alpha_string defprochelp3  */
#line 5467 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PROCNAME);
    free((yyvsp[-1].multi_str).str);
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->name = (yyvsp[-1].multi_str);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[-1]));
  #endif
}
#line 10054 "block_csh_lang.cc"
    break;

  case 336: /* defprochelp2: defprochelp3  */
#line 5478 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(CshPos((yylsp[0]).first_pos, (yylsp[0]).first_pos), "Missing procedure name.");
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[0]));
  #endif
}
#line 10067 "block_csh_lang.cc"
    break;

  case 337: /* defprochelp3: proc_def_arglist_tested  */
#line 5488 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->parameters = (yyvsp[0].procparamdeflist);
  #endif
}
#line 10080 "block_csh_lang.cc"
    break;

  case 338: /* defprochelp3: proc_def_arglist_tested defprochelp4  */
#line 5497 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->parameters = (yyvsp[-1].procparamdeflist);
  #endif
}
#line 10092 "block_csh_lang.cc"
    break;

  case 339: /* defprochelp3: defprochelp4  */
#line 5505 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->parameters = new ProcParamDefList;
  #endif
}
#line 10104 "block_csh_lang.cc"
    break;

  case 340: /* defprochelp4: full_attrlist  */
#line 5514 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        csh.AddYesNoToHints();
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->attrs = (yyvsp[0].attributelist);
  #endif
}
#line 10123 "block_csh_lang.cc"
    break;

  case 341: /* defprochelp4: full_attrlist procedure_body  */
#line 5529 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        csh.AddYesNoToHints();
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->body = (yyvsp[0].procedure);
    (yyval.procdefhelper)->linenum_body = CHART_POS_START((yylsp[0]));
    (yyval.procdefhelper)->attrs = (yyvsp[-1].attributelist);
  #endif
}
#line 10143 "block_csh_lang.cc"
    break;

  case 342: /* defprochelp4: procedure_body  */
#line 5545 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->body = (yyvsp[0].procedure);
    (yyval.procdefhelper)->linenum_body = CHART_POS_START((yylsp[0]));
  #endif
}
#line 10156 "block_csh_lang.cc"
    break;

  case 343: /* scope_open_proc_body: TOK_OCBRACKET  */
#line 5556 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    chart.MyCurrentContext().num_error = chart.Error.GetErrorNum(true, false);
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT, EContextCreate::EMPTY);  //We have all the styles, but empty
    chart.MyCurrentContext().parameters = std::move(YYGET_EXTRA(yyscanner)->last_procedure_params);
    chart.MyCurrentContext().starts_procedure = true;
    _ASSERT(YYGET_EXTRA(yyscanner)->open_context_mode == base_parse_parm::EScopeOpenMode::NORMAL);
    YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
  #endif
    (yyval.input_text_ptr) = (yyvsp[0].input_text_ptr);
}
#line 10175 "block_csh_lang.cc"
    break;

  case 344: /* scope_close_proc_body: TOK_CCBRACKET  */
#line 5572 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
  #else
    chart.PopContext();
  #endif
    (yyval.input_text_ptr) = (yyvsp[0].input_text_ptr);
}
#line 10189 "block_csh_lang.cc"
    break;

  case 345: /* proc_def_arglist_tested: proc_def_arglist  */
#line 5583 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparamdeflist)) {
        auto pair = Procedure::AreAllParameterNamesUnique(*(yyvsp[0].procparamdeflist));
        if (pair.first) {
            chart.Error.Error(pair.second->linenum_name, "This parameter name is already used.");
            chart.Error.Error(pair.first->linenum_name, pair.second->linenum_name, "This parameter name is already used.");
            delete (yyvsp[0].procparamdeflist);
            (yyval.procparamdeflist) = nullptr;
        } else {
            //Also copy to YYGET_EXTRA(yyscanner)->last_procedure_params and set open_context_mode
            auto &store = YYGET_EXTRA(yyscanner)->last_procedure_params;
            store.clear();
            for (const auto &p : *(yyvsp[0].procparamdeflist))
                store.emplace(p->name, ProcParamResolved(std::string(), FileLineCol(), true));
            (yyval.procparamdeflist) = (yyvsp[0].procparamdeflist);
        }
    } else
        (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 10216 "block_csh_lang.cc"
    break;

  case 346: /* proc_def_arglist: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 5607 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparamdeflist) = new ProcParamDefList;
  #endif
}
#line 10229 "block_csh_lang.cc"
    break;

  case 347: /* proc_def_arglist: TOK_OPARENTHESIS error TOK_CPARENTHESIS  */
#line 5616 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter definitions.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter definitions.", "Say something like '($first, $second=default)'.");
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 10244 "block_csh_lang.cc"
    break;

  case 348: /* proc_def_arglist: TOK_OPARENTHESIS  */
#line 5627 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter list closed by a parenthesis ')'.");
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 10258 "block_csh_lang.cc"
    break;

  case 349: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list error TOK_CPARENTHESIS  */
#line 5637 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter definitions.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter definitions.");
    delete (yyvsp[-2].procparamdeflist);
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 10274 "block_csh_lang.cc"
    break;

  case 350: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list  */
#line 5649 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing closing parenthesis ')'.");
    delete (yyvsp[0].procparamdeflist);
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 10289 "block_csh_lang.cc"
    break;

  case 351: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list TOK_CPARENTHESIS  */
#line 5660 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparamdeflist) = (yyvsp[-1].procparamdeflist);
  #endif
}
#line 10302 "block_csh_lang.cc"
    break;

  case 352: /* proc_def_param_list: proc_def_param  */
#line 5670 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparamdef)) {
        (yyval.procparamdeflist) = new ProcParamDefList;
        ((yyval.procparamdeflist))->Append((yyvsp[0].procparamdef));
    } else
        (yyval.procparamdeflist)= nullptr;
  #endif
}
#line 10317 "block_csh_lang.cc"
    break;

  case 353: /* proc_def_param_list: proc_def_param_list TOK_COMMA  */
#line 5681 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter after the comma.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter after the comma.");
    delete (yyvsp[-1].procparamdeflist);
    (yyval.procparamdeflist)= nullptr;
  #endif
}
#line 10332 "block_csh_lang.cc"
    break;

  case 354: /* proc_def_param_list: proc_def_param_list TOK_COMMA proc_def_param  */
#line 5692 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[-2].procparamdeflist) && (yyvsp[0].procparamdef)) {
        ((yyvsp[-2].procparamdeflist))->Append((yyvsp[0].procparamdef));
        (yyval.procparamdeflist) = (yyvsp[-2].procparamdeflist);
    } else {
        delete (yyvsp[-2].procparamdeflist);
        delete (yyvsp[0].procparamdef);
        (yyval.procparamdeflist)= nullptr;
    }
  #endif
}
#line 10351 "block_csh_lang.cc"
    break;

  case 355: /* proc_def_param: TOK_PARAM_NAME  */
#line 5708 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].str) && (yyvsp[0].str)[0]=='$' && (yyvsp[0].str)[1])
        csh.AddCSH((yylsp[0]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[0]), "Need name after the '$' sign.");
    (yyval.procparamdef) = nullptr; //no value
  #else
    if ((yyvsp[0].str) && (yyvsp[0].str)[0]=='$' && (yyvsp[0].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[0].str), CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[0].str));
}
#line 10373 "block_csh_lang.cc"
    break;

  case 356: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL  */
#line 5726 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if ((yyvsp[-1].str) && (yyvsp[-1].str)[0]=='$' && (yyvsp[-1].str)[1])
        csh.AddCSH((yylsp[-1]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-1]), "Need name after the '$' sign.");
    (yyval.procparamdef) = nullptr; //no value
  #else
    if ((yyvsp[-1].str) && (yyvsp[-1].str)[0]=='$' && (yyvsp[-1].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-1].str));
}
#line 10396 "block_csh_lang.cc"
    break;

  case 357: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL string  */
#line 5745 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1])
        csh.AddCSH((yylsp[-2]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-2]), "Need name after the '$' sign.");
    (yyval.procparamdef) = (char*)1; //has value
  #else
    if ((yyvsp[0].multi_str).had_error) {
        (yyval.procparamdef) = nullptr;
    } else if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-2].str), CHART_POS_START((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-2])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 10422 "block_csh_lang.cc"
    break;

  case 358: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL TOK_NUMBER  */
#line 5767 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1])
        csh.AddCSH((yylsp[-2]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-2]), "Need name after the '$' sign.");
    (yyval.procparamdef) = (char*)1; //has value
  #else
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-2].str), CHART_POS_START((yylsp[-2])), (yyvsp[0].str), CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-2])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 10446 "block_csh_lang.cc"
    break;

  case 359: /* procedure_body: scope_open_proc_body instrlist scope_close_proc_body  */
#line 5789 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::OK;
    tmp->text = std::string(((yyvsp[-2].input_text_ptr)), ((yyvsp[0].input_text_ptr))+1)+";";
    tmp->file_pos = CHART_POS_START((yyloc));
    if ((yyvsp[-1].instruction_list))
        delete (yyvsp[-1].instruction_list);
    (yyval.procedure) = tmp;
  #endif
}
#line 10464 "block_csh_lang.cc"
    break;

  case 360: /* procedure_body: scope_open_proc_body scope_close_proc_body  */
#line 5803 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::EMPTY;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 10481 "block_csh_lang.cc"
    break;

  case 361: /* procedure_body: scope_open_proc_body instrlist error scope_close_proc_body  */
#line 5816 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "syntax error.");
    if ((yyvsp[-2].instruction_list))
        delete (yyvsp[-2].instruction_list);
  #endif
    yyerrok;
  (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 10503 "block_csh_lang.cc"
    break;

  case 362: /* procedure_body: scope_open_proc_body instrlist error TOK_EOF  */
#line 5834 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_START((yylsp[-1])), "Here is the corresponding '{'.");
    if ((yyvsp[-2].instruction_list))
        delete (yyvsp[-2].instruction_list);
  #endif
  (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 10526 "block_csh_lang.cc"
    break;

  case 363: /* procedure_body: scope_open_proc_body instrlist TOK_EOF  */
#line 5853 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
    if ((yyvsp[-1].instruction_list))
        delete (yyvsp[-1].instruction_list);
  #endif
  (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 10549 "block_csh_lang.cc"
    break;

  case 364: /* procedure_body: scope_open_proc_body TOK_EOF  */
#line 5872 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 10569 "block_csh_lang.cc"
    break;

  case 365: /* procedure_body: scope_open_proc_body instrlist TOK_BYE  */
#line 5888 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
    if ((yyvsp[-1].instruction_list))
        delete (yyvsp[-1].instruction_list);
  #endif
    (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
    free((yyvsp[0].str));
}
#line 10593 "block_csh_lang.cc"
    break;

  case 366: /* procedure_body: scope_open_proc_body TOK_BYE  */
#line 5908 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
    (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
    free((yyvsp[0].str));
}
#line 10614 "block_csh_lang.cc"
    break;

  case 367: /* set: TOK_COMMAND_SET proc_def_param  */
#line 5926 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].procparamdef))
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing value.");
  #else
    if (!chart.SkipContent())
        chart.SetVariable((yyvsp[0].procparamdef), CHART_POS((yyloc)));
    else
        delete (yyvsp[0].procparamdef);
  #endif
    free((yyvsp[-1].str));
}
#line 10632 "block_csh_lang.cc"
    break;

  case 368: /* set: TOK_COMMAND_SET  */
#line 5940 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing variable or parameter name to set.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing variable or parameter name to set.");
  #endif
    free((yyvsp[0].str));
}
#line 10646 "block_csh_lang.cc"
    break;

  case 369: /* proc_invocation: TOK_COMMAND_REPLAY  */
#line 5952 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing procedure name.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing procedure name.");
    (yyval.cprocedure) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 10661 "block_csh_lang.cc"
    break;

  case 370: /* proc_invocation: TOK_COMMAND_REPLAY alpha_string  */
#line 5963 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_PROCNAME);
    csh.hadProcReplay = true;
  #else
    (yyval.cprocedure) = nullptr;
    if (!(yyvsp[0].multi_str).had_error) {
        auto proc = chart.GetProcedure((yyvsp[0].multi_str).str);
        if (proc==nullptr)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Undefined procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::PROBLEM)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Ill-formed procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::OK) {
            //Only return the procedure if we are not inside a procedure definition
            if (!chart.SkipContent())
                (yyval.cprocedure) = proc;
            //else just move on parsing - we do not reparse procedure replays during
            //the definition of an outer procedure.
        }
        //else return null, emit no error for EMPTY
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 10692 "block_csh_lang.cc"
    break;

  case 371: /* proc_param_list: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 5991 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparaminvoclist) = new ProcParamInvocationList;
  #endif
}
#line 10705 "block_csh_lang.cc"
    break;

  case 372: /* proc_param_list: TOK_OPARENTHESIS error TOK_CPARENTHESIS  */
#line 6000 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter syntax.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter syntax. Ignoring procedure call.");
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 10720 "block_csh_lang.cc"
    break;

  case 373: /* proc_param_list: TOK_OPARENTHESIS  */
#line 6011 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter list closed by a parenthesis ')'. Ignoring procedure call.");
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 10734 "block_csh_lang.cc"
    break;

  case 374: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list error TOK_CPARENTHESIS  */
#line 6021 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter syntax.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter syntax. Ignoring procedure call.");
    delete (yyvsp[-2].procparaminvoclist);
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 10750 "block_csh_lang.cc"
    break;

  case 375: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list  */
#line 6033 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing closing parenthesis ')'. Ignoring procedure call.");
    delete (yyvsp[0].procparaminvoclist);
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 10765 "block_csh_lang.cc"
    break;

  case 376: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list TOK_CPARENTHESIS  */
#line 6044 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparaminvoclist) = (yyvsp[-1].procparaminvoclist);
  #endif
}
#line 10778 "block_csh_lang.cc"
    break;

  case 377: /* proc_invoc_param_list: proc_invoc_param  */
#line 6054 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparaminvoc)) {
        (yyval.procparaminvoclist) = new ProcParamInvocationList;
        ((yyval.procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
    } else
        (yyval.procparaminvoclist)= nullptr;
  #endif
}
#line 10793 "block_csh_lang.cc"
    break;

  case 378: /* proc_invoc_param_list: TOK_COMMA  */
#line 6065 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
  #else
    (yyval.procparaminvoclist) = new ProcParamInvocationList;
    ((yyval.procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_START((yylsp[0]))));
  #endif
}
#line 10806 "block_csh_lang.cc"
    break;

  case 379: /* proc_invoc_param_list: TOK_COMMA proc_invoc_param  */
#line 6074 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[0].procparaminvoc)) {
        (yyval.procparaminvoclist) = new ProcParamInvocationList;
        ((yyval.procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_START((yylsp[-1]))));
        ((yyval.procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
    } else
        (yyval.procparaminvoclist)= nullptr;
  #endif
}
#line 10823 "block_csh_lang.cc"
    break;

  case 380: /* proc_invoc_param_list: proc_invoc_param_list TOK_COMMA  */
#line 6087 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
  #else
    if ((yyvsp[-1].procparaminvoclist))
        ((yyvsp[-1].procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_AFTER((yylsp[0]))));
    (yyval.procparaminvoclist) = (yyvsp[-1].procparaminvoclist);
  #endif
}
#line 10837 "block_csh_lang.cc"
    break;

  case 381: /* proc_invoc_param_list: proc_invoc_param_list TOK_COMMA proc_invoc_param  */
#line 6097 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[-2].procparaminvoclist) && (yyvsp[0].procparaminvoc)) {
        ((yyvsp[-2].procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
        (yyval.procparaminvoclist) = (yyvsp[-2].procparaminvoclist);
    } else {
        delete (yyvsp[-2].procparaminvoclist);
        delete (yyvsp[0].procparaminvoc);
        (yyval.procparaminvoclist)= nullptr;
    }
  #endif
}
#line 10856 "block_csh_lang.cc"
    break;

  case 382: /* proc_invoc_param: string  */
#line 6113 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error) {
        //If this is a quoted string, color as a label, else as an attribute value
        if ((yylsp[0]).first_pos>0 && YYGET_EXTRA(yyscanner)->buff.buf[(yylsp[0]).first_pos-1]=='\"')
            csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, {});
        else
            csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.procparaminvoc) = nullptr;
    else
        (yyval.procparaminvoc) = new ProcParamInvocation((yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 10878 "block_csh_lang.cc"
    break;

  case 383: /* proc_invoc_param: TOK_NUMBER  */
#line 6131 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    (yyval.procparaminvoc) = new ProcParamInvocation((yyvsp[0].str), CHART_POS_START((yylsp[0])));
  #endif
    free((yyvsp[0].str));
}
#line 10891 "block_csh_lang.cc"
    break;

  case 385: /* ifthenbranch: complete_instr  */
#line 6146 "block_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction))
        (yyval.instruction_list) = (new BlockInstrList)->Append((yyvsp[0].instruction)); /* New list */
    else
        (yyval.instruction_list) = new BlockInstrList;
  #endif
}
#line 10904 "block_csh_lang.cc"
    break;

  case 387: /* condition: string  */
#line 6160 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #endif
    (yyval.condition) = (yyvsp[0].multi_str).had_error ? 2 : (yyvsp[0].multi_str).str && (yyvsp[0].multi_str).str[0];
    free((yyvsp[0].multi_str).str);
}
#line 10916 "block_csh_lang.cc"
    break;

  case 388: /* condition: string comp  */
#line 6168 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to compare to.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to compare to.");
  #endif
    (yyval.condition) = 2;
    free((yyvsp[-1].multi_str).str);
    (yyvsp[0].compare_op); //to suppress
}
#line 10933 "block_csh_lang.cc"
    break;

  case 389: /* condition: string comp string  */
#line 6181 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    if ((yyvsp[-1].compare_op)!=ECompareOperator::INVALID) {
        csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
        (yyval.condition) = (yyvsp[-2].multi_str).Compare((yyvsp[-1].compare_op), (yyvsp[0].multi_str));
    } else {
        csh.AddCSH_Error((yylsp[-1]), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        (yyval.condition) = 2;
    }
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if ((yyvsp[-1].compare_op)!=ECompareOperator::INVALID)
        (yyval.condition) = (yyvsp[-2].multi_str).Compare((yyvsp[-1].compare_op), (yyvsp[0].multi_str));
    else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        (yyval.condition) = 2;
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 10960 "block_csh_lang.cc"
    break;

  case 390: /* condition: string error string  */
#line 6204 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    csh.AddCSH_Error((yylsp[-1]), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
     chart.Error.Error(CHART_POS_START((yylsp[-1])), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
    (yyval.condition) = 2;
}
#line 10977 "block_csh_lang.cc"
    break;

  case 391: /* ifthen_condition: TOK_IF condition TOK_THEN  */
#line 6218 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    (yyval.condition) = csh.Contexts.back().if_condition = (yyvsp[-1].condition);
    const bool cond_true = (yyvsp[-1].condition)==1;
    if (cond_true)
        csh.PushContext();
    else
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = (yyvsp[-1].condition);
    const bool cond_true = (yyvsp[-1].condition)==1;
    if (cond_true)
        chart.PushContext(CHART_POS_START((yylsp[-2])));
    else
        chart.PushContext(CHART_POS_START((yylsp[-2])), EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_true;
    chart.MyCurrentContext().export_styles = cond_true;
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 11009 "block_csh_lang.cc"
    break;

  case 392: /* ifthen_condition: TOK_IF condition  */
#line 6246 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = (yyvsp[0].condition);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'then' keyword.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "then",
            "Continue the 'if' statement with 'then'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = (yyvsp[0].condition);
    chart.PushContext(CHART_POS_START((yylsp[-1])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing 'then' keyword.");
  #endif
    free((yyvsp[-1].str));
    (yyvsp[0].condition); //to supress warnings
}
#line 11034 "block_csh_lang.cc"
    break;

  case 393: /* ifthen_condition: TOK_IF  */
#line 6267 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing condition.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[0].str));
}
#line 11052 "block_csh_lang.cc"
    break;

  case 394: /* ifthen_condition: TOK_IF error  */
#line 6281 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[0]), "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-1])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[-1].str));
}
#line 11070 "block_csh_lang.cc"
    break;

  case 395: /* ifthen_condition: TOK_IF error TOK_THEN  */
#line 6295 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[-1]), "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-2])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 11093 "block_csh_lang.cc"
    break;

  case 396: /* else: TOK_ELSE  */
#line 6316 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
    const bool cond_false = csh.Contexts.back().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    (yyval.condition) = csh.Contexts.back().if_condition;
    if (cond_false)
        csh.PushContext();
    else
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    //kill previous context and open new one - set laterreparse if error or if condition was true
    //this will ignore everything in the else clause
    chart.PopContext();
    const bool cond_false = chart.MyCurrentContext().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    (yyval.condition) = chart.MyCurrentContext().if_condition;
    if (cond_false)
        chart.PushContext(CHART_POS_START((yylsp[0])));
    else
        chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_false;
    chart.MyCurrentContext().export_styles = cond_false;
  #endif
    free((yyvsp[0].str));
}
#line 11129 "block_csh_lang.cc"
    break;

  case 397: /* ifthen: ifthen_condition ifthenbranch  */
#line 6349 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.PopContext();
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "else",
            "Continue the 'if/then' statement with 'else'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].condition)==1) {
        (yyval.instruction_list) = (yyvsp[0].instruction_list);
    } else {
        (yyval.instruction_list) = nullptr;
        delete (yyvsp[0].instruction_list);
    }
    chart.PopContext();
  #endif
}
#line 11154 "block_csh_lang.cc"
    break;

  case 398: /* ifthen: ifthen_condition  */
#line 6370 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].condition)!=2)
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'then'.");
    csh.PopContext();
  #else
    if ((yyvsp[0].condition)!=2)
        chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a well-formed command after 'then'. Ignoring 'if' clause.");
    chart.PopContext();
    (yyval.instruction_list) = nullptr;
  #endif
    (yyvsp[0].condition); //suppress
}
#line 11172 "block_csh_lang.cc"
    break;

  case 399: /* ifthen: ifthen_condition error  */
#line 6384 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Missing command after 'then'.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing a well-formed command. Ignoring 'if' clause.");
    chart.PopContext();
    (yyval.instruction_list) = nullptr;
  #endif
    (yyvsp[-1].condition); //suppress
}
#line 11188 "block_csh_lang.cc"
    break;

  case 400: /* ifthen: ifthen_condition ifthenbranch else  */
#line 6396 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-1]));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    delete (yyvsp[-1].instruction_list);
    (yyval.instruction_list) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-2].condition); (yyvsp[0].condition); //suppress
}
#line 11206 "block_csh_lang.cc"
    break;

  case 401: /* ifthen: ifthen_condition ifthenbranch error else  */
#line 6410 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-2]));
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    delete (yyvsp[-2].instruction_list);
    (yyval.instruction_list) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "I am not sure what is coming here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-3].condition); (yyvsp[0].condition); //suppress
}
#line 11226 "block_csh_lang.cc"
    break;

  case 402: /* ifthen: ifthen_condition error else  */
#line 6426 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "I am not sure what is coming here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-2].condition); (yyvsp[0].condition); //suppress
}
#line 11244 "block_csh_lang.cc"
    break;

  case 403: /* ifthen: ifthen_condition ifthenbranch else ifthenbranch  */
#line 6440 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-2]));
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.PopContext();
  #else
    switch ((yyvsp[-3].condition)) {
    case 1: //original condition was true
        (yyval.instruction_list) = (yyvsp[-2].instruction_list);   //take 'then' branch
        delete (yyvsp[0].instruction_list); //delete 'else' branch
        break;
    case 0: //original condition was false
        (yyval.instruction_list) = (yyvsp[0].instruction_list); //take 'else' branch
        delete (yyvsp[-2].instruction_list); //delete 'then' branch
        break;
    default:
        _ASSERT(0);
        FALLTHROUGH;
    case 2: //we had an error, but has reported the error - delete both branches
        (yyval.instruction_list) = nullptr;
        delete (yyvsp[-2].instruction_list);
        delete (yyvsp[0].instruction_list);
        break;
    }
    chart.PopContext();
  #endif
    (yyvsp[-1].condition); //suppress
}
#line 11277 "block_csh_lang.cc"
    break;

  case 404: /* ifthen: ifthen_condition ifthenbranch error else ifthenbranch  */
#line 6469 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-3]));
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.AddCSH_Error((yylsp[-2]), "I am not sure what is coming here.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = nullptr;
    delete (yyvsp[-3].instruction_list);
    delete (yyvsp[0].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    (yyvsp[-4].condition); (yyvsp[-1].condition); //suppress
}
#line 11297 "block_csh_lang.cc"
    break;

  case 405: /* ifthen: ifthen_condition error else ifthenbranch  */
#line 6485 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.AddCSH_Error((yylsp[-2]), "I am not sure what is coming here.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = nullptr;
    delete (yyvsp[0].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    (yyvsp[-3].condition); (yyvsp[-1].condition); //suppress
}
#line 11315 "block_csh_lang.cc"
    break;

  case 406: /* colon_string: TOK_COLON_QUOTED_STRING  */
#line 6506 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), false);
    csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 11327 "block_csh_lang.cc"
    break;

  case 407: /* colon_string: TOK_COLON_STRING  */
#line 6514 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), true);
    csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 11339 "block_csh_lang.cc"
    break;

  case 408: /* full_attrlist_with_label: colon_string  */
#line 6523 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	(yyval.attributelist) = -2; //we do not contain any shape=xxx attr.
  #else
        (yyval.attributelist) = (new AttributeList)->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yyloc)).IncStartCol()));
  #endif
    free((yyvsp[0].str));
}
#line 11352 "block_csh_lang.cc"
    break;

  case 409: /* full_attrlist_with_label: colon_string full_attrlist  */
#line 6532 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	(yyval.attributelist) = (yyvsp[0].attributelist); //copy any potential shape attr
  #else
        (yyval.attributelist) = ((yyvsp[0].attributelist))->Prepend(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()));
  #endif
    free((yyvsp[-1].str));
}
#line 11365 "block_csh_lang.cc"
    break;

  case 410: /* full_attrlist_with_label: full_attrlist colon_string full_attrlist  */
#line 6541 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	if ((yyvsp[0].attributelist)>=-1) (yyval.attributelist) = (yyvsp[0].attributelist);
	else (yyval.attributelist) = (yyvsp[-2].attributelist); //copy any potential shape attr
  #else
        (yyval.attributelist) = ((yyvsp[-2].attributelist))->Append(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()));
        //Merge $3 at the end of $1
        ((yyvsp[-2].attributelist))->splice(((yyvsp[-2].attributelist))->end(), *((yyvsp[0].attributelist)));
        delete ((yyvsp[0].attributelist)); //empty list now
        (yyval.attributelist) = (yyvsp[-2].attributelist);
  #endif
    free((yyvsp[-1].str));
}
#line 11383 "block_csh_lang.cc"
    break;

  case 411: /* full_attrlist_with_label: full_attrlist colon_string  */
#line 6555 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	(yyval.attributelist) = (yyvsp[-1].attributelist); //copy any potential shape attr
  #else
        (yyval.attributelist) = ((yyvsp[-1].attributelist))->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yylsp[0])), CHART_POS((yylsp[0])).IncStartCol()));
  #endif
    free((yyvsp[0].str));
}
#line 11396 "block_csh_lang.cc"
    break;

  case 413: /* full_attrlist: TOK_OSBRACKET TOK_CSBRACKET  */
#line 6567 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
	(yyval.attributelist) = -2; //we do not contain any shape=xxx attr.
  #else
    (yyval.attributelist) = new AttributeList;
  #endif
}
#line 11412 "block_csh_lang.cc"
    break;

  case 414: /* full_attrlist: TOK_OSBRACKET attrlist TOK_CSBRACKET  */
#line 6579 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
	(yyval.attributelist) = (yyvsp[-1].attributelist); //copy any potential shape attr
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
  #endif
}
#line 11428 "block_csh_lang.cc"
    break;

  case 415: /* full_attrlist: TOK_OSBRACKET attrlist error TOK_CSBRACKET  */
#line 6591 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[-1]), "Extra stuff after an attribute list. Maybe missing a comma?");
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::ATTR_NAME);
	(yyval.attributelist) = (yyvsp[-2].attributelist); //copy any potential shape attr
  #else
    (yyval.attributelist) = (yyvsp[-2].attributelist);
  #endif
}
#line 11445 "block_csh_lang.cc"
    break;

  case 416: /* full_attrlist: TOK_OSBRACKET error TOK_CSBRACKET  */
#line 6604 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as an attribute or style name.");
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
	(yyval.attributelist) = -2; //we do not contain any shape=xxx attr.
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting an attribute or style name. Ignoring all until the closing square bracket (']').");
#endif
}
#line 11463 "block_csh_lang.cc"
    break;

  case 417: /* full_attrlist: TOK_OSBRACKET attrlist  */
#line 6618 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a square bracket (']').");
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
    csh.SqBracketPairs.push_back((yyloc));
	(yyval.attributelist) = (yyvsp[0].attributelist); //copy any potential shape attr
  #else
    (yyval.attributelist) = (yyvsp[0].attributelist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 11480 "block_csh_lang.cc"
    break;

  case 418: /* full_attrlist: TOK_OSBRACKET attrlist error  */
#line 6631 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[0]), "Missing a ']'.");
    csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
	(yyval.attributelist) = (yyvsp[-1].attributelist); //copy any potential shape attr
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing ']'.");
  #endif
}
#line 11497 "block_csh_lang.cc"
    break;

  case 419: /* full_attrlist: TOK_OSBRACKET  */
#line 6644 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a square bracket (']').");
    csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME);
	(yyval.attributelist) = -2; //we do not contain any shape=xxx attr.
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 11514 "block_csh_lang.cc"
    break;

  case 420: /* full_attrlist: TOK_OSBRACKET error  */
#line 6657 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[0]), "Missing a ']'.");
    csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
	(yyval.attributelist) = -2; //we do not contain any shape=xxx attr.
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing ']'.");
  #endif
}
#line 11531 "block_csh_lang.cc"
    break;

  case 421: /* attrlist: attr  */
#line 6671 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	(yyval.attributelist) = (yyvsp[0].attribute); //copy any potential shape number
  #else
    if ((yyvsp[0].attribute) && (yyvsp[0].attribute)->name.length()==0) {
        chart.Error.Error(CHART_POS_START((yyloc)), "Missing attribute name.");
        delete (yyvsp[0].attribute);
        (yyval.attributelist) = new AttributeList;
    } else
        (yyval.attributelist) = (new AttributeList)->Append((yyvsp[0].attribute));
  #endif
}
#line 11548 "block_csh_lang.cc"
    break;

  case 422: /* attrlist: attrlist TOK_COMMA attr  */
#line 6684 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
	if ((yyvsp[0].attribute)>=-1)
		(yyval.attributelist) = (yyvsp[0].attribute);
	else
		(yyval.attributelist) = (yyvsp[-2].attributelist); //copy any potential shape number
  #else
    if ((yyvsp[0].attribute) && (yyvsp[0].attribute)->name.length()==0) {
        if ((yyvsp[-2].attributelist)->size()==0)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing attribute name.");
        else {
            (yyvsp[-2].attributelist)->back()->value += StrCat(',', CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].attribute)->value);
            (yyvsp[-2].attributelist)->back()->type = EAttrType::STRING;
        }
        delete (yyvsp[0].attribute);
        (yyval.attributelist) = (yyvsp[-2].attributelist);
    } else
        (yyval.attributelist) = ((yyvsp[-2].attributelist))->Append((yyvsp[0].attribute));
  #endif
}
#line 11575 "block_csh_lang.cc"
    break;

  case 423: /* attrlist: attrlist TOK_COMMA  */
#line 6707 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing attribute or style name.");
	(yyval.attributelist) = (yyvsp[-1].attributelist); //copy any potential shape number
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an attribute or style name here.");
  #endif
}
#line 11591 "block_csh_lang.cc"
    break;

  case 424: /* attr: alpha_string TOK_EQUAL attrvalue  */
#line 6721 "block_lang.yy"
{
  //string=string, alignment attributes
  //$3 may contain position escapes and need handling of csh.addEntityNamesAtEnd
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, (yyvsp[-2].multi_str).str);
    if (csh.CheckHintLocated((yylsp[0]))) {
        if (!csh.HandleBlockNamePlus((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, EHintSourceType::ATTR_VALUE)) {
            csh.hintStatus = HINT_NONE;
            csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
        }
    } else {
        if (!(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
            csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
        if (!(yyvsp[-2].multi_str).had_error)
            csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
    }
	if (!(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error) {
		if (CaseInsensitiveEqual((yyvsp[-2].multi_str).str, "shape")) {
			(yyval.attribute) = csh.GetShapeNum((yyvsp[0].multi_str).str);
			if ((yyval.attribute)==-1 && !CaseInsensitiveEqual((yyvsp[0].multi_str).str, "box"))
				(yyval.attribute) = -2; //not found
		} else {
			(yyval.attribute) = -2; //we are not a valid shape=xxx attr.
		}
	}
  #else
    if ((yyvsp[-2].multi_str).had_error || (chart.SkipContent() && (yyvsp[-2].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = chart.CreateAttribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 11634 "block_csh_lang.cc"
    break;

  case 425: /* attr: alpha_string TOK_EQUAL TOK_PLUS entity_string  */
#line 6760 "block_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-3]), (yyvsp[-3].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[-1])+(yylsp[0]), (string("++")+(yyvsp[0].multi_str).str).c_str(), (yyvsp[-3].multi_str).str);
    csh.CheckHintAt((yylsp[-3]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-3].multi_str).had_error)
        csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1])+(yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-3].multi_str).str);
	(yyval.attribute) = -2; //we are not a valid shape=xxx attr.
  #else
    if ((yyvsp[-3].multi_str).had_error || (chart.SkipContent() && (yyvsp[-3].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[-3].multi_str).str, string("++")+(yyvsp[0].multi_str).str, CHART_POS((yylsp[-3])), CHART_POS2((yylsp[-1]),(yylsp[0])));
  #endif
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 11660 "block_csh_lang.cc"
    break;

  case 426: /* attr: alpha_string TOK_EQUAL TOK_PLUS  */
#line 6782 "block_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
    csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), "++", (yyvsp[-2].multi_str).str);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Continue with a color name or definition.");
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
	(yyval.attribute) = -2; //we are not a valid shape=xxx attr.
  #else
    if ((yyvsp[-2].multi_str).had_error || (chart.SkipContent() && (yyvsp[-2].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[-2].multi_str).str, "++", CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 11686 "block_csh_lang.cc"
    break;

  case 427: /* attr: alpha_string TOK_EQUAL  */
#line 6804 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str);
	(yyval.attribute) = -2; //we are not a valid shape=xxx attr.
  #else
    if ((yyvsp[-1].multi_str).had_error || (chart.SkipContent() && (yyvsp[-1].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[-1].multi_str).str, {}, CHART_POS((yyloc)), CHART_POS((yyloc)));
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 11708 "block_csh_lang.cc"
    break;

  case 428: /* attr: string  */
#line 6822 "block_lang.yy"
{
  //here we accept non alpha strings for "->" and similar style names
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_StyleOrAttrName((yylsp[0]), (yyvsp[0].multi_str).str);
    csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME);
	(yyval.attribute) = -2; //we are not a valid shape=xxx attr.
  #else
    if ((yyvsp[0].multi_str).had_error || (chart.SkipContent() && (yyvsp[0].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[0].multi_str).str, CHART_POS((yyloc)));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11728 "block_csh_lang.cc"
    break;

  case 429: /* attr: TOK_NUMBER  */
#line 6838 "block_lang.yy"
{
  //This may come as, e.g., size=12,12
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
	(yyval.attribute) = -2; //we are not a valid shape=xxx attr.
  #else
    //An attribute of empty name is to be concatenated to the previous one.
    (yyval.attribute) = new Attribute("", (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yyloc)));
  #endif
    free((yyvsp[0].str));
}
#line 11744 "block_csh_lang.cc"
    break;

  case 431: /* attrvalue: attrvalue_simple TOK_NUMBER  */
#line 6855 "block_lang.yy"
{
  //This may come as, e.g., size=12,12
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if ((yyvsp[0].str)[0]=='+' || (yyvsp[0].str)[0]=='-')
        (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), "+", (yyvsp[0].str));
  #else
    if ((yyvsp[0].str)[0]=='+' || (yyvsp[0].str)[0]=='-')
        (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), "+", CHART_POS_START((yylsp[0])).Print(), (yyvsp[0].str));
  #endif
}
#line 11764 "block_csh_lang.cc"
    break;

  case 434: /* attrvalue_simple: symbol_string  */
#line 6874 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 11772 "block_csh_lang.cc"
    break;

  case 435: /* attrvalue_simple: reserved_word_string  */
#line 6878 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 11780 "block_csh_lang.cc"
    break;

  case 436: /* attrvalue_simple: reserved_word_string multi_string_continuation  */
#line 6882 "block_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), (yyvsp[0].multi_str));
}
#line 11788 "block_csh_lang.cc"
    break;

  case 438: /* alignment_attrvalue: blocknames_plus_number_multi TOK_ATSYMBOL  */
#line 6888 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing token or number here.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddEntityNamesAtTheEnd("AddAfter@"); //to be filled later
  #endif
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), "@");
}
#line 11802 "block_csh_lang.cc"
    break;

  case 439: /* alignment_attrvalue: blocknames_plus_number_multi TOK_ATSYMBOL edgepos  */
#line 6898 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1])+(yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddEntityNamesAtTheEnd("AddAfter@"); //to be filled later
  #endif
    (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "@", (yyvsp[0].multi_str));
}
#line 11815 "block_csh_lang.cc"
    break;

  case 440: /* alignment_attrvalue: TOK_NUMBER TOK_PERCENT  */
#line 6907 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
  #endif
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), "%");
}
#line 11826 "block_csh_lang.cc"
    break;

  case 441: /* alignment_attrvalue: TOK_NUMBER  */
#line 6914 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
  #endif
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 11837 "block_csh_lang.cc"
    break;

  case 442: /* alignment_attrvalue: TOK_NUMBER TOK_PERCENT TOK_NUMBER  */
#line 6921 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
  #endif
    if ((yyvsp[0].str)[0]=='+' || (yyvsp[0].str)[0]=='-')
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].str), "%", (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].str), "%+", (yyvsp[0].str));
}
#line 11851 "block_csh_lang.cc"
    break;

  case 443: /* alignment_attrvalue: entity_string TOK_PERCENT  */
#line 6931 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
  #endif
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), "%");
}
#line 11862 "block_csh_lang.cc"
    break;

  case 444: /* alignment_attrvalue: entity_string TOK_PERCENT TOK_NUMBER  */
#line 6938 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_ATTRVALUE);
  #endif
    if ((yyvsp[0].str)[0]=='+' || (yyvsp[0].str)[0]=='-')
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "%", (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), "%+", (yyvsp[0].str));
}
#line 11876 "block_csh_lang.cc"
    break;

  case 446: /* edgepos: alpha_string_or_percent TOK_NUMBER  */
#line 6951 "block_lang.yy"
{
    if ((yyvsp[0].str)[0]=='+' || (yyvsp[0].str)[0]=='-')
        (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].str));
    else
        (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), "+", (yyvsp[0].str));
}
#line 11887 "block_csh_lang.cc"
    break;

  case 449: /* alpha_string_or_percent: TOK_NUMBER  */
#line 6963 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 11895 "block_csh_lang.cc"
    break;

  case 450: /* alpha_string_or_percent: TOK_NUMBER TOK_PERCENT  */
#line 6967 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[-1].str));
}
#line 11903 "block_csh_lang.cc"
    break;

  case 451: /* blocknames_plus_number_multi: blocknames_plus_number  */
#line 6973 "block_lang.yy"
{
    std::string acc;
    (yyval.multi_str).multi = false;
    (yyval.multi_str).had_param = false;
    (yyval.multi_str).had_error = false;
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_NameList((yyvsp[0].stringposlist), (yylsp[0]), COLOR_ENTITYNAME, COLOR_COMMA, COLOR_ATTRVALUE, COLOR_ENTITYNAME);
  #endif
    for (auto &s: *(yyvsp[0].stringposlist))
        if (s.name.length()==0)
            (yyval.multi_str).had_error = true;
        else {
            //avoid double signs
            if (acc.length() && s.name.length() && (s.name.front()=='-' || s.name.front()=='+'))
                acc.pop_back();
  #ifdef C_S_H_IS_COMPILED
            acc.append(s.name).push_back('+');
  #else
            acc.append(s.file_pos.start.Print()).append(s.name).push_back('+');
  #endif
        }
    if (acc.length()) acc.pop_back();
    (yyval.multi_str).str = strdup(acc.c_str());
    delete (yyvsp[0].stringposlist);
}
#line 11933 "block_csh_lang.cc"
    break;

  case 453: /* blocknames_plus_number_or_number: TOK_NUMBER  */
#line 7001 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.stringposlist) = new CshStringWithPosList;
    (yyval.stringposlist)->push_back({(yyvsp[0].str), (yylsp[0])});
  #else
    (yyval.stringposlist) = new StringWithPosList;
    (yyval.stringposlist)->push_back({(yyvsp[0].str), CHART_POS((yylsp[0]))});
  #endif
    free((yyvsp[0].str));
}
#line 11948 "block_csh_lang.cc"
    break;

  case 454: /* blocknames_plus_number: entity_string  */
#line 7015 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.stringposlist) = new CshStringWithPosList;
    if ((yyvsp[0].multi_str).str && !(yyvsp[0].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
    } else {
        (yyval.stringposlist)->push_back({"", (yylsp[0])});
        csh.AddCSH_Error((yylsp[0]), "Expecting a block name or number here.");
    }
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ENTITY))
        csh.AddEntityNamesAtTheEnd("AddEntities+num"); //to be filled later
  #else
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[0].multi_str).str && !(yyvsp[0].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a block name or number here.");
        (yyval.stringposlist)->push_back({"", CHART_POS((yylsp[0]))});
    }
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 11975 "block_csh_lang.cc"
    break;

  case 455: /* blocknames_plus_number: entity_string TOK_PLUS  */
#line 7038 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.stringposlist) = new CshStringWithPosList;
    if ((yyvsp[-1].multi_str).str && !(yyvsp[-1].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[-1].multi_str).str, (yylsp[-1])});
    } else {
        (yyval.stringposlist)->push_back({"", (yylsp[-1])});
        csh.AddCSH_Error((yylsp[-1]), "Expecting a block name here.");
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a box name here.");
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_VALUE))
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddEntityNamesAtTheEnd("AddEntities+num"); //to be filled later
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a box name here.");
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[-1].multi_str).str && !(yyvsp[-1].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1]))});
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting a block name here.");
        (yyval.stringposlist)->push_back({"", CHART_POS((yylsp[-1]))});
    }
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 12006 "block_csh_lang.cc"
    break;

  case 456: /* blocknames_plus_number: entity_string TOK_NUMBER  */
#line 7065 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.stringposlist) = new CshStringWithPosList;
    if ((yyvsp[-1].multi_str).str && !(yyvsp[-1].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[-1].multi_str).str, (yylsp[-1])});
    } else {
        csh.AddCSH_Error((yylsp[-1]), "Expecting a block name here.");
        (yyval.stringposlist)->push_back({"", (yylsp[-1])});
    }
    (yyval.stringposlist)->push_back({(yyvsp[0].str)+((yyvsp[0].str)[0]=='+'), (yylsp[0])});
    //Do the checking in two separate items, so that hintedStringPos
    //covers only one of them (for correct replacement)
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ENTITY))
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    else if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ENTITY))
        csh.AddEntityNamesAtTheEnd("AddEntities+num"); //to be filled later
  #else
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[-1].multi_str).str && !(yyvsp[-1].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1]))});
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting a block name here.");
        (yyval.stringposlist)->push_back({"", CHART_POS((yylsp[-1]))});
    }
    (yyval.stringposlist)->push_back({(yyvsp[0].str), CHART_POS((yylsp[0]))});
  #endif
    free((yyvsp[-1].multi_str).str);
    free((yyvsp[0].str));
}
#line 12040 "block_csh_lang.cc"
    break;

  case 457: /* blocknames_plus_number: entity_string TOK_PLUS blocknames_plus_number  */
#line 7095 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY) ||
        csh.CheckHintAt((yylsp[-2]), EHintSourceType::ENTITY))
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    if ((yyvsp[0].stringposlist)) {
       if (!(yyvsp[-2].multi_str).had_error && (yyvsp[-2].multi_str).str)
            (yyvsp[0].stringposlist)->insert((yyvsp[0].stringposlist)->begin(), {(yyvsp[-2].multi_str).str, (yylsp[-2])});
       else
            (yyvsp[0].stringposlist)->insert((yyvsp[0].stringposlist)->begin(), {"", (yylsp[-2])});
    }
  #else
    if ((yyvsp[0].stringposlist)) {
        if (!(yyvsp[-2].multi_str).had_error && (yyvsp[-2].multi_str).str)
            (yyvsp[0].stringposlist)->insert((yyvsp[0].stringposlist)->begin(), {(yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2]))});
        else
            (yyvsp[0].stringposlist)->insert((yyvsp[0].stringposlist)->begin(), {"", CHART_POS((yylsp[-2]))});
    }
  #endif
    (yyval.stringposlist) = (yyvsp[0].stringposlist);
    free((yyvsp[-2].multi_str).str);
}
#line 12067 "block_csh_lang.cc"
    break;

  case 458: /* blocknames_plus: entity_string  */
#line 7122 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.stringposlist) = new CshStringWithPosList;
    if ((yyvsp[0].multi_str).str && !(yyvsp[0].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, (yylsp[0])});
    } else {
        csh.AddCSH_Error((yylsp[0]), "Expecting a block name or number here.");
        (yyval.stringposlist)->push_back({"", (yylsp[0])});
    }
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ENTITY))
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
  #else
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[0].multi_str).str && !(yyvsp[0].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[0].multi_str).str, CHART_POS((yylsp[0]))});
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a block name or number here.");
        (yyval.stringposlist)->push_back({"", CHART_POS((yylsp[0]))});
    }
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 12094 "block_csh_lang.cc"
    break;

  case 459: /* blocknames_plus: entity_string TOK_PLUS  */
#line 7145 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.stringposlist) = new CshStringWithPosList;
    if ((yyvsp[-1].multi_str).str && !(yyvsp[-1].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[-1].multi_str).str, (yylsp[-1])});
    } else {
        csh.AddCSH_Error((yylsp[-1]), "Expecting a block name here.");
        (yyval.stringposlist)->push_back({"", (yylsp[-1])});
    }
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a box name here.");
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_VALUE))
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a box name here.");
    (yyval.stringposlist) = new StringWithPosList;
    if ((yyvsp[-1].multi_str).str && !(yyvsp[-1].multi_str).had_error) {
        (yyval.stringposlist)->push_back({(yyvsp[-1].multi_str).str, CHART_POS((yylsp[-1]))});
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting a block name here.");
        (yyval.stringposlist)->push_back({"", CHART_POS((yylsp[-1]))});
    }
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 12125 "block_csh_lang.cc"
    break;

  case 460: /* blocknames_plus: entity_string TOK_PLUS blocknames_plus_number  */
#line 7172 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY))
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    if ((yyvsp[0].stringposlist)) {
       if (!(yyvsp[-2].multi_str).had_error && (yyvsp[-2].multi_str).str)
            (yyvsp[0].stringposlist)->insert((yyvsp[0].stringposlist)->begin(), {(yyvsp[-2].multi_str).str, (yylsp[-2])});
       else
            (yyvsp[0].stringposlist)->insert((yyvsp[0].stringposlist)->begin(), {"", (yylsp[-2])});
    }
  #else
    if ((yyvsp[0].stringposlist)) {
        if (!(yyvsp[-2].multi_str).had_error && (yyvsp[-2].multi_str).str)
            (yyvsp[0].stringposlist)->insert((yyvsp[0].stringposlist)->begin(), {(yyvsp[-2].multi_str).str, CHART_POS((yylsp[-2]))});
        else
            (yyvsp[0].stringposlist)->insert((yyvsp[0].stringposlist)->begin(), {"", CHART_POS((yylsp[-2]))});
    }
  #endif
    (yyval.stringposlist) = (yyvsp[0].stringposlist);
    free((yyvsp[-2].multi_str).str);
}
#line 12151 "block_csh_lang.cc"
    break;

  case 461: /* coord_hinted: coord  */
#line 7196 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    //We dont just check if the hint was located in @$ as the cursor may be
	//*after* @$ in case of a missing parenthesis, for example.
    if (csh.CheckHintLocated((yylsp[0])) || csh.CheckHintLocated(EHintSourceType::ATTR_VALUE))
        csh.HandleBlockNamePlus("generic_coord");
  #endif
    (yyval.multi_str) = (yyvsp[0].multi_str);
}
#line 12165 "block_csh_lang.cc"
    break;

  case 462: /* coord: TOK_OPARENTHESIS alignment_attrvalue TOK_COMMA alignment_attrvalue TOK_CPARENTHESIS  */
#line 7208 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[-2]), COLOR_COMMA);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    if (csh.addEntityNamesAtEnd=="AddAfter@") {
        if (csh.CursorIn((yylsp[-3]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@X");
        else if (csh.CursorIn((yylsp[-1]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@Y");
    }
  #endif
    (yyval.multi_str).CombineThemToMe("(", (yyvsp[-3].multi_str), "," , (yyvsp[-1].multi_str), ")");
}
#line 12184 "block_csh_lang.cc"
    break;

  case 463: /* coord: TOK_OPARENTHESIS alignment_attrvalue TOK_COMMA TOK_CPARENTHESIS  */
#line 7223 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    if (csh.addEntityNamesAtEnd=="AddAfter@") {
        if (csh.CursorIn((yylsp[-2]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@X");
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    }
  #endif
    (yyval.multi_str).CombineThemToMe("(", (yyvsp[-2].multi_str), ",)");
}
#line 12203 "block_csh_lang.cc"
    break;

  case 464: /* coord: TOK_OPARENTHESIS TOK_COMMA alignment_attrvalue TOK_CPARENTHESIS  */
#line 7238 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[-2]), COLOR_COMMA);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    if (csh.addEntityNamesAtEnd=="AddAfter@") {
        if (csh.CursorIn((yylsp[-1]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@Y");
    } else if (csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::ATTR_VALUE)) {
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    }
  #endif
    (yyval.multi_str).CombineThemToMe("(," , (yyvsp[-1].multi_str), ")");
}
#line 12222 "block_csh_lang.cc"
    break;

  case 465: /* coord: TOK_OPARENTHESIS TOK_COMMA TOK_CPARENTHESIS  */
#line 7253 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    }
  #endif
    (yyval.multi_str) = multi_segment_string("(,)");
}
#line 12238 "block_csh_lang.cc"
    break;

  case 466: /* coord: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 7265 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    }
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing an X coordinate staring with a blockname.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing an X coordinate staring with a blockname. Ignoring this.");
  #endif
    (yyval.multi_str) = multi_segment_string("(");
}
#line 12256 "block_csh_lang.cc"
    break;

  case 467: /* coord: TOK_OPARENTHESIS  */
#line 7279 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing an X coordinate staring with a blockname.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing an X coordinate staring with a blockname. Ignoring this.");
  #endif
    (yyval.multi_str) = multi_segment_string("(");
}
#line 12273 "block_csh_lang.cc"
    break;

  case 468: /* coord: TOK_OPARENTHESIS alignment_attrvalue  */
#line 7292 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    if (csh.addEntityNamesAtEnd=="AddAfter@") {
        if (csh.CursorIn((yylsp[0]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@X");
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing comma and an Y coordinate after.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing comma and an Y coordinate after. Ignoring this.");
  #endif
    delete (yyvsp[0].multi_str).str;
    (yyval.multi_str) = multi_segment_string("(");
}
#line 12292 "block_csh_lang.cc"
    break;

  case 469: /* coord: TOK_OPARENTHESIS alignment_attrvalue TOK_COMMA  */
#line 7307 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.addEntityNamesAtEnd=="AddAfter@") {
        if (csh.CursorIn((yylsp[-1]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@X");
    } else if (csh.CheckHintAfter((yyloc), EHintSourceType::ATTR_VALUE)) {
        csh.AddEntityNamesAtTheEnd("AddEntities"); //to be filled later
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing an Y coordinate starting with a block name.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing an Y coordinate starting with a block name. Ignoring this.");
  #endif
    delete (yyvsp[-1].multi_str).str;
    (yyval.multi_str) = multi_segment_string("(");
}
#line 12314 "block_csh_lang.cc"
    break;

  case 470: /* coord: TOK_OPARENTHESIS TOK_COMMA  */
#line 7325 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.addEntityNamesAtEnd=="AddAfter@") {
        if (csh.CursorIn((yylsp[0]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@X");
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing an Y coordinate starting with a block name.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing an Y coordinate starting with a block name. Ignoring this.");
  #endif
    (yyval.multi_str) = multi_segment_string("(");
}
#line 12333 "block_csh_lang.cc"
    break;

  case 471: /* coord: TOK_OPARENTHESIS alignment_attrvalue TOK_COMMA alignment_attrvalue  */
#line 7340 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.addEntityNamesAtEnd=="AddAfter@") {
        if (csh.CursorIn((yylsp[-2]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@X");
        else if (csh.CursorIn((yylsp[0]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@Y");
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing a closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing a closing parenthesis ')'.");
  #endif
    (yyval.multi_str).CombineThemToMe("(", (yyvsp[-2].multi_str), "," , (yyvsp[0].multi_str), ")");
}
#line 12354 "block_csh_lang.cc"
    break;

  case 472: /* coord: TOK_OPARENTHESIS TOK_COMMA alignment_attrvalue  */
#line 7357 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.addEntityNamesAtEnd=="AddAfter@") {
        if (csh.CursorIn((yylsp[0]))>CURSOR_AFTER)
            csh.AddEntityNamesAtTheEnd("AddAfter@Y");
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing a closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing a closing parenthesis ')'.");
  #endif
    (yyval.multi_str).CombineThemToMe("(," , (yyvsp[0].multi_str), ")");
}
#line 12373 "block_csh_lang.cc"
    break;

  case 512: /* reserved_word_string: TOK_ALIGN_MODIFIER  */
#line 7393 "block_lang.yy"
{
    (yyval.str) = strdup((yyvsp[0].alignmodifier).dir==EDirection::Below ? ((yyvsp[0].alignmodifier).major ? "below" : "bottom") :
                (yyvsp[0].alignmodifier).dir==EDirection::Above ? ((yyvsp[0].alignmodifier).major ? "above" : "top") :
                (yyvsp[0].alignmodifier).dir==EDirection::Right ? ((yyvsp[0].alignmodifier).major ? "rightof" : "right") :
                                            ((yyvsp[0].alignmodifier).major ? "leftof" : "left"));
}
#line 12384 "block_csh_lang.cc"
    break;

  case 513: /* symbol_string: arrowsymbol  */
#line 7402 "block_lang.yy"
{
    if ((yyvsp[0].arrowtype).start) {
        if ((yyvsp[0].arrowtype).end)
            switch((yyvsp[0].arrowtype).style) {
            default: _ASSERT(0); FALLTHROUGH;
            case EArrowStyle::SOLID: (yyval.str) = strdup("<->"); break;
            case EArrowStyle::DOTTED: (yyval.str) = strdup("<>"); break;
            case EArrowStyle::DASHED: (yyval.str) = strdup("<<>>"); break;
            case EArrowStyle::DOUBLE: (yyval.str) = strdup("<=>"); break;
            }
        else
            switch((yyvsp[0].arrowtype).style) {
            default: _ASSERT(0); FALLTHROUGH;
            case EArrowStyle::SOLID: (yyval.str) = strdup("<-"); break;
            case EArrowStyle::DOTTED: (yyval.str) = strdup("<"); break;
            case EArrowStyle::DASHED: (yyval.str) = strdup("<<"); break;
            case EArrowStyle::DOUBLE: (yyval.str) = strdup("<="); break;
            }
    } else {
        if ((yyvsp[0].arrowtype).end)
            switch((yyvsp[0].arrowtype).style) {
            default: _ASSERT(0); FALLTHROUGH;
            case EArrowStyle::SOLID: (yyval.str) = strdup("->"); break;
            case EArrowStyle::DOTTED: (yyval.str) = strdup(">"); break;
            case EArrowStyle::DASHED: (yyval.str) = strdup(">>"); break;
            case EArrowStyle::DOUBLE: (yyval.str) = strdup("=>"); break;
            }
        else
            switch((yyvsp[0].arrowtype).style) {
            default: _ASSERT(0); FALLTHROUGH;
            case EArrowStyle::SOLID: (yyval.str) = strdup("--"); break;
            case EArrowStyle::DOTTED: (yyval.str) = strdup(".."); break;
            case EArrowStyle::DASHED: (yyval.str) = strdup("++"); break;
            case EArrowStyle::DOUBLE: (yyval.str) = strdup("=="); break;
            }
    }
}
#line 12426 "block_csh_lang.cc"
    break;

  case 515: /* entity_string_single: TOK_QSTRING  */
#line 7443 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddQuotedString((yylsp[0]));
  #endif
  (yyval.str) = (yyvsp[0].str);
}
#line 12437 "block_csh_lang.cc"
    break;

  case 516: /* entity_string_single: TOK_SHAPE_COMMAND  */
#line 7450 "block_lang.yy"
{
    (yyval.str) = (char*)malloc(2);
    ((yyval.str))[0] = ShapeElement::act_code[(yyvsp[0].shapecommand)];
    ((yyval.str))[1] = 0;
}
#line 12447 "block_csh_lang.cc"
    break;

  case 519: /* tok_param_name_as_multi: TOK_PARAM_NAME  */
#line 7462 "block_lang.yy"
{
    (yyval.multi_str).str = nullptr;
    (yyval.multi_str).multi = true;
    (yyval.multi_str).had_param = true;
    (yyval.multi_str).had_error = false;
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].str)==nullptr || (yyvsp[0].str)[0]!='$' || (yyvsp[0].str)[1]==0)
        csh.AddCSH_Error((yylsp[0]), "Need name after the '$' sign.");
  #else
    if ((yyvsp[0].str)==nullptr || (yyvsp[0].str)[0]!='$' || (yyvsp[0].str)[1]==0) {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Need name after the '$' sign.");
        (yyval.multi_str).had_error = true;
    } else if (!chart.SkipContent()) {
        //When parsing a procedure we we accept all params
        //as they may be variables defined later
        auto p = chart.GetParameter((yyvsp[0].str));
        if (p==nullptr) {
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Undefined parameter or variable name.");
            (yyval.multi_str).had_error = true;
        } else {
            (yyval.multi_str).str = strdup(StringFormat::PushPosEscapes(p->value.c_str(), CHART_POS_START((yylsp[0]))).c_str());
        }
    }
  #endif
    //avoid returning null
    if ((yyval.multi_str).str==nullptr)
        (yyval.multi_str).str = strdup("");
    free((yyvsp[0].str));
}
#line 12481 "block_csh_lang.cc"
    break;

  case 520: /* multi_string_continuation: TOK_TILDE  */
#line 7495 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to concatenate after '~'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to concatenate after '~'.");
  #endif
    (yyval.multi_str).str = strdup("");
    (yyval.multi_str).multi = true;
    (yyval.multi_str).had_param = false;
    (yyval.multi_str).had_error = true;
}
#line 12497 "block_csh_lang.cc"
    break;

  case 521: /* multi_string_continuation: TOK_TILDE string  */
#line 7507 "block_lang.yy"
{
    (yyval.multi_str) = (yyvsp[0].multi_str);
}
#line 12505 "block_csh_lang.cc"
    break;

  case 522: /* entity_string_single_or_param: entity_string_single  */
#line 7512 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 12513 "block_csh_lang.cc"
    break;

  case 524: /* entity_string: entity_string_single_or_param  */
#line 7518 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].multi_str));
}
#line 12521 "block_csh_lang.cc"
    break;

  case 525: /* entity_string: entity_string_single_or_param multi_string_continuation  */
#line 7522 "block_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].multi_str));
}
#line 12529 "block_csh_lang.cc"
    break;

  case 527: /* alpha_string: alpha_string_single  */
#line 7528 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 12537 "block_csh_lang.cc"
    break;

  case 528: /* alpha_string: alpha_string_single multi_string_continuation  */
#line 7532 "block_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), (yyvsp[0].multi_str));
}
#line 12545 "block_csh_lang.cc"
    break;

  case 530: /* string: string_single  */
#line 7538 "block_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 12553 "block_csh_lang.cc"
    break;

  case 531: /* string: string_single multi_string_continuation  */
#line 7542 "block_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), (yyvsp[0].multi_str));
}
#line 12561 "block_csh_lang.cc"
    break;

  case 532: /* scope_open: TOK_OCBRACKET  */
#line 7548 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext();
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (YYGET_EXTRA(yyscanner)->open_context_mode == base_parse_parm::EScopeOpenMode::PROC_REPLAY) {
        //Open a scope to replay a procedure. The text of the procedure has
        //already been placed to the lex buffer stack (in fact the '{' already comes
        //from there), and the parameters are in YYEXTRA.
        //Copy current parent to the newly opened scope - it shall be the same during procedure replay as outside the proc
        Parent parent = chart.MyCurrentContext().parent;
        YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
        chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::REPARSING);
        chart.MyCurrentContext().starts_procedure = true;
        chart.MyCurrentContext().parameters = std::move(YYGET_EXTRA(yyscanner)->last_procedure_params);
        chart.MyCurrentContext().export_colors = YYGET_EXTRA(yyscanner)->last_procedure->export_colors;
        chart.MyCurrentContext().export_styles = YYGET_EXTRA(yyscanner)->last_procedure->export_styles;
        YYGET_EXTRA(yyscanner)->last_procedure = nullptr;
        chart.MyCurrentContext().parent = std::move(parent);
    } else {
        //Just open a regular scope
        chart.PushContext(CHART_POS_START((yylsp[0])));
        //Apply the saved alignment attributes to running style
        chart.MyCurrentContext().running_style_blocks += chart.parent_style;
        chart.MyCurrentContext().parent = chart.current_parent;
    }
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 12599 "block_csh_lang.cc"
    break;

  case 533: /* scope_close: TOK_CCBRACKET  */
#line 7583 "block_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.instruction_list) = nullptr;
    csh.PopContext();
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list) = chart.PopContext().release();
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 12618 "block_csh_lang.cc"
    break;


#line 12622 "block_csh_lang.cc"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= TOK_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == TOK_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, RESULT, yyscanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, RESULT, yyscanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, RESULT, yyscanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 7599 "block_lang.yy"



/* END OF FILE */
