/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/* This file is included by parsers both for color syntax and language compilation */

#ifndef BLOCK_PARSERHELPER_H
#define BLOCK_PARSERHELPER_H

#include <string>
#include "language_misc.h"

#ifdef C_S_H_IS_COMPILED
    //Use stock parse_parm - good enough for us.
    typedef base_parse_parm_csh block_parse_parm_csh;

    /** Actual 'parse_param' structure (descendant of base_parse_param), shall be defined by chart.*/
    #define YY_EXTRA_TYPE block_parse_parm_csh *

#else

    //Use stock parse_parm - good enough for us.
    typedef base_parse_parm block_parse_parm;

    /** Actual 'parse_param' structure (descendant of base_parse_param), shall be defined by chart.*/
    #define YY_EXTRA_TYPE block_parse_parm *

    void BlockPushFlex(base_parse_parm &pp, const char *text, size_t len, YYLTYPE *old_pos, const FileLineCol &new_pos, EInclusionReason reason);

#endif 

#endif