/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_BLOCKCSH_BLOCK_CSH_LANG_H_INCLUDED
# define YY_BLOCKCSH_BLOCK_CSH_LANG_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int blockcsh_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    TOK_EOF = 0,                   /* TOK_EOF  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    TOK_STRING = 258,              /* TOK_STRING  */
    TOK_QSTRING = 259,             /* TOK_QSTRING  */
    TOK_NUMBER = 260,              /* TOK_NUMBER  */
    TOK_DASH = 261,                /* TOK_DASH  */
    TOK_EQUAL = 262,               /* TOK_EQUAL  */
    TOK_COMMA = 263,               /* TOK_COMMA  */
    TOK_SEMICOLON = 264,           /* TOK_SEMICOLON  */
    TOK_PLUS_PLUS = 265,           /* TOK_PLUS_PLUS  */
    TOK_PLUS = 266,                /* TOK_PLUS  */
    TOK_MINUS = 267,               /* TOK_MINUS  */
    TOK_ATSYMBOL = 268,            /* TOK_ATSYMBOL  */
    TOK_ASTERISK = 269,            /* TOK_ASTERISK  */
    TOK_PERCENT = 270,             /* TOK_PERCENT  */
    TOK_OCBRACKET = 271,           /* TOK_OCBRACKET  */
    TOK_CCBRACKET = 272,           /* TOK_CCBRACKET  */
    TOK_OSBRACKET = 273,           /* TOK_OSBRACKET  */
    TOK_CSBRACKET = 274,           /* TOK_CSBRACKET  */
    TOK_BOX = 275,                 /* TOK_BOX  */
    TOK_BOXCOL = 276,              /* TOK_BOXCOL  */
    TOK_ROW = 277,                 /* TOK_ROW  */
    TOK_COLUMN = 278,              /* TOK_COLUMN  */
    TOK_TEXT = 279,                /* TOK_TEXT  */
    TOK_SHAPE = 280,               /* TOK_SHAPE  */
    TOK_CELL = 281,                /* TOK_CELL  */
    TOK_SHAPE_COMMAND = 282,       /* TOK_SHAPE_COMMAND  */
    TOK_ALIGN_MODIFIER = 283,      /* TOK_ALIGN_MODIFIER  */
    TOK_BREAK_COMMAND = 284,       /* TOK_BREAK_COMMAND  */
    TOK_SPACE_COMMAND = 285,       /* TOK_SPACE_COMMAND  */
    TOK_MULTI_COMMAND = 286,       /* TOK_MULTI_COMMAND  */
    TOK_AROUND_COMMAND = 287,      /* TOK_AROUND_COMMAND  */
    TOK_JOIN_COMMAND = 288,        /* TOK_JOIN_COMMAND  */
    TOK_COPY_COMMAND = 289,        /* TOK_COPY_COMMAND  */
    TOK_AS = 290,                  /* TOK_AS  */
    TOK_MARK_COMMAND = 291,        /* TOK_MARK_COMMAND  */
    TOK_EXTEND_COMMAND = 292,      /* TOK_EXTEND_COMMAND  */
    TOK_TEMPLATE_COMMAND = 293,    /* TOK_TEMPLATE_COMMAND  */
    TOK_COMP_OP = 294,             /* TOK_COMP_OP  */
    TOK_COLON_STRING = 295,        /* TOK_COLON_STRING  */
    TOK_COLON_QUOTED_STRING = 296, /* TOK_COLON_QUOTED_STRING  */
    TOK_COMMAND_DEFSHAPE = 297,    /* TOK_COMMAND_DEFSHAPE  */
    TOK_COMMAND_DEFCOLOR = 298,    /* TOK_COMMAND_DEFCOLOR  */
    TOK_COMMAND_DEFSTYLE = 299,    /* TOK_COMMAND_DEFSTYLE  */
    TOK_COMMAND_DEFDESIGN = 300,   /* TOK_COMMAND_DEFDESIGN  */
    TOK_COMMAND_USEDESIGN = 301,   /* TOK_COMMAND_USEDESIGN  */
    TOK_COMMAND_USESTYLE = 302,    /* TOK_COMMAND_USESTYLE  */
    TOK_COMMAND_ARROWS = 303,      /* TOK_COMMAND_ARROWS  */
    TOK_COMMAND_BLOCKS = 304,      /* TOK_COMMAND_BLOCKS  */
    TOK_CLONE_MODIFIER_RECURSIVE = 305, /* TOK_CLONE_MODIFIER_RECURSIVE  */
    TOK_CLONE_MODIFIER_ADD = 306,  /* TOK_CLONE_MODIFIER_ADD  */
    TOK_CLONE_MODIFIER_MOVE = 307, /* TOK_CLONE_MODIFIER_MOVE  */
    TOK_CLONE_MODIFIER_DROP = 308, /* TOK_CLONE_MODIFIER_DROP  */
    TOK_CLONE_MODIFIER_UPDATE = 309, /* TOK_CLONE_MODIFIER_UPDATE  */
    TOK_CLONE_MODIFIER_REPLACE = 310, /* TOK_CLONE_MODIFIER_REPLACE  */
    TOK_CLONE_MODIFIER_BEFORE = 311, /* TOK_CLONE_MODIFIER_BEFORE  */
    TOK_TILDE = 312,               /* TOK_TILDE  */
    TOK_PARAM_NAME = 313,          /* TOK_PARAM_NAME  */
    TOK_OPARENTHESIS = 314,        /* TOK_OPARENTHESIS  */
    TOK_CPARENTHESIS = 315,        /* TOK_CPARENTHESIS  */
    TOK_COMMAND_DEFPROC = 316,     /* TOK_COMMAND_DEFPROC  */
    TOK_COMMAND_REPLAY = 317,      /* TOK_COMMAND_REPLAY  */
    TOK_COMMAND_SET = 318,         /* TOK_COMMAND_SET  */
    TOK_BYE = 319,                 /* TOK_BYE  */
    TOK_IF = 320,                  /* TOK_IF  */
    TOK_THEN = 321,                /* TOK_THEN  */
    TOK_ELSE = 322,                /* TOK_ELSE  */
    TOK_COMMAND_INCLUDE = 323,     /* TOK_COMMAND_INCLUDE  */
    TOK_UNRECOGNIZED_CHAR = 324,   /* TOK_UNRECOGNIZED_CHAR  */
    TOK__NEVER__HAPPENS = 325,     /* TOK__NEVER__HAPPENS  */
    TOK_ARROW_FW = 326,            /* TOK_ARROW_FW  */
    TOK_ARROW_BW = 327,            /* TOK_ARROW_BW  */
    TOK_ARROW_BIDIR = 328,         /* TOK_ARROW_BIDIR  */
    TOK_ARROW_NO = 329             /* TOK_ARROW_NO  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 84 "block_lang.yy"

    gsl::owner<char*>                                          str;
    ShapeElement::Type                                         shapecommand;
    gsl::owner<FIRST_IF_CSH(std::vector<string>,Shape)*>       shape;
    gsl::owner<CHAR_IF_CSH(ShapeElement)*>                     shapeelement;
    FIRST_IF_CSH(int, gsl::owner<Attribute*>)                  attribute;       //-2 if none, -1<= if a shape= attr
    FIRST_IF_CSH(int, gsl::owner<AttributeList*>)              attributelist;   //-2 if none, -1<= if a shape= attr is present
    EBlockType                                                 eblocktype;
    AlignModifier                                              alignmodifier;
    gsl::owner<FIRST_IF_CSH(CshStringWithPosList,BlockBlockList)*> blockblocklist;
    gsl::owner<CHAR_IF_CSH(BlockInstruction)*>                 instruction;
    gsl::owner<CHAR_IF_CSH(BlockInstrList)*>                   instruction_list;
    const char*                                                input_text_ptr;
    int                                                        condition; //0:false, 1:true, 2:had_error
    ECompareOperator                                           compare_op;
    EUseKeywords                                               use_keywords;
    multi_segment_string                                       multi_str;
    gsl::owner<CHAR_IF_CSH(Procedure)*>                        procedure;
    CHAR_IF_CSH(const Procedure)*                              cprocedure;
    gsl::owner<CHAR_IF_CSH(ProcParamDef)*>                     procparamdef;
    gsl::owner<CHAR_IF_CSH(ProcParamDefList)*>                 procparamdeflist;
    gsl::owner<CHAR_IF_CSH(ProcParamInvocation)*>              procparaminvoc;
    gsl::owner<CHAR_IF_CSH(ProcParamInvocationList)*>          procparaminvoclist;
    gsl::owner<CHAR_IF_CSH(ProcDefParseHelper<AttributeList>)*>procdefhelper;
    EArrowStyle                                                arrowstyle;
    ArrowType                                                  arrowtype;
    gsl::owner<FIRST_IF_CSH(CshStringWithPosList, StringWithPosList)*> stringposlist;
    gsl::owner<CHAR_IF_CSH(StringWithPosList)*>                stringposlist_nocsh;
    gsl::owner<CHAR_IF_CSH(CopyParseHelper)*>                  copystruct;
    gsl::owner<CHAR_IF_CSH(AlignmentAttr)*>                    alignment_attr;
    gsl::owner<CHAR_IF_CSH(ArrowLabel)*>                       arrowlabel;
    CHAR_IF_CSH(ArrowAttrHelper)                               arrowattrs;
    gsl::owner<CHAR_IF_CSH(CloneAction)*>                      cloneaction;
    gsl::owner<CHAR_IF_CSH(CloneActionList)*>                  cloneactionlist;
    FIRST_IF_CSH(char*, UseCommandParseHelper)                 usecommandhelper;

#line 175 "block_csh_lang.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif




int blockcsh_parse (YYMSC_RESULT_TYPE &RESULT, void *yyscanner);


#endif /* !YY_BLOCKCSH_BLOCK_CSH_LANG_H_INCLUDED  */
