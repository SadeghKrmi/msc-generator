/*
    This file is part of Msc-generator.
	Copyright (C) 2008-2022 Zoltan Turanyi
	Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file MiniEditor.cpp The internal editor control and its hosting dockable pane.
* @ingroup Msc_generator_files */

#include "stdafx.h"
#include "Msc-generator.h"
#include "MscGenDoc.h"
#include "MiniEditor.h"
#include "MainFrm.h"
#include "utf8utils.h" 


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/** The language selector dialog box. */
class CRenameEntityDlg : public CDialog {
public:
    CRenameEntityDlg(const ColorSyntaxAppearance &app, const CRect *character_pos)
        : CDialog(CRenameEntityDlg::IDD)
        , weight(app.effects & COLOR_FLAG_BOLD ? FW_BOLD : FW_NORMAL)
        , italics(app.effects &COLOR_FLAG_ITALICS)
        , underline(app.effects &COLOR_FLAG_UNDERLINE)
        , has_color(app.effects &COLOR_FLAG_COLOR)
        , color(RGB(app.r, app.g, app.b))
        , position(character_pos)
    {
    }

    // Dialog Data
    enum { IDD = IDD_DIALOG_RENAME_ENTITY };

protected:
    BOOL OnInitDialog() override;
    void DoDataExchange(CDataExchange *pDX) override;
    afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);

    // Implementation
protected:
    DECLARE_MESSAGE_MAP()
public:
    CString m_EntityName; 

private:
    const int weight;
    const bool italics;
    const bool underline;
    const bool has_color;
    const int font_size = 12;
    const COLORREF color;
    const CRect *position;
};

BEGIN_MESSAGE_MAP(CRenameEntityDlg, CDialog)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


BOOL CRenameEntityDlg::OnInitDialog() {
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    ASSERT(pApp != nullptr);
    auto *txt = ((CEdit*)GetDlgItem(IDC_EDIT_RENAME_ENTITY));
    CFont font;
    CClientDC dc(this);
    font.CreateFont(-((dc.GetDeviceCaps(LOGPIXELSY) * font_size) / 72), 0,
                    0, 0, weight, italics, underline, FALSE, 
                    DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
                    PROOF_QUALITY, FIXED_PITCH | FF_MODERN,
                    _T("Consolas"));
    txt->SetFont(&font, TRUE);
    BOOL a = CDialog::OnInitDialog();
    txt->SetSel(0, -1);
    txt->SetFocus();
    //make the window appear on top of all other windows
    //https://stackoverflow.com/questions/593403/always-in-front-dialogs
    if (position) {
        CRect rect;
        GetWindowRect(&rect);
        rect.MoveToXY(position->CenterPoint().x - rect.Width()/2, position->bottom);
        //Move to stay in screen
        //https://docs.microsoft.com/hu-hu/windows/win32/gdi/positioning-objects-on-a-multiple-display-setup
        HMONITOR hMonitor = MonitorFromRect(*position, MONITOR_DEFAULTTONEAREST);
        MONITORINFO mi;
        mi.cbSize = sizeof(mi);
        GetMonitorInfo(hMonitor, &mi);
        RECT &screen = mi.rcWork;
        //If we cant fit below, fit above
        if (screen.bottom < rect.bottom) rect.MoveToY(position->top - rect.Height());
        if (screen.left > rect.left) rect.MoveToX(screen.left);
        if (screen.right < rect.right) rect.MoveToX(screen.right-rect.Width());
        SetWindowPos(&this->wndTop, rect.left, rect.top, 0, 0, SWP_NOSIZE);
    } else
        SetWindowPos(&this->wndTop, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
    return FALSE;
}

void CRenameEntityDlg::DoDataExchange(CDataExchange *pDX) {
    CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_EDIT_RENAME_ENTITY, m_EntityName);
}

HBRUSH CRenameEntityDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor) {
    HBRUSH ret = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    if (CTLCOLOR_EDIT == nCtlColor
            && IDC_EDIT_RENAME_ENTITY == pWnd->GetDlgCtrlID()
            && has_color)
        pDC->SetTextColor(color);
    return ret;
}


BEGIN_MESSAGE_MAP(CScrollingRichEditCtrl, CRichEditCtrl)
    ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

BOOL CScrollingRichEditCtrl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
#ifndef SHARED_HANDLERS
    CFrameWnd *pMainWnd = dynamic_cast<CFrameWnd*>(theApp.GetMainWnd());
    ASSERT(pMainWnd!=nullptr);
    if (pMainWnd && pMainWnd->GetActiveView() != nullptr) {
        CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(pMainWnd->GetActiveView()->GetDocument());
        if (pDoc != nullptr)
            pDoc->DispatchMouseWheel(nFlags, zDelta, pt);
    }
#endif
    return TRUE;
}

BEGIN_MESSAGE_MAP(CCshRichEditCtrl, CScrollingRichEditCtrl)
    ON_MESSAGE(EM_PASTESPECIAL, OnPasteSpecial)
    ON_WM_PAINT()
END_MESSAGE_MAP()


const unsigned linewidth = 40;

long CCshRichEditCtrl::CharHeight() const {
    return long(abs(m_parent->m_FontPixelHeight)*1.2*m_parent->m_zoom); //magic number, too}
}

CCshRichEditCtrl::CCshRichEditCtrl(CEditorBar *parent) : 
    m_hintsPopup(parent, this),
    m_parent(parent)
{
    m_tabsize = 4;
    m_bUserRequested = false;
    m_bTillCursorOnly = false;
    m_bRedrawState = true;
    m_ctxMenuPos.x = -1; //indicate invalid
}

/** How long text we accept in the internal editor.*/
#define LIMIT_TEXT 640000
/** Textual representation of LIMIT_TEXT */
#define LIMIT_TEXT_STR "640Kbytes"

/** Create the object. 
 * initialize the tabsize and color scheme.
 * Create our hint popup window, too. */
BOOL CCshRichEditCtrl::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
    auto pConf = GetConf();
    if (!pConf) return false;
    if (pConf) {
        m_tabsize = GetRegistryInt(REG_KEY_TABSIZE, 4);
        pConf->m_languages.cshParams->m_II = GetRegistryInt(REG_KEY_TABSIZE_II, 4);
        pConf->m_languages.cshParams->m_IB = GetRegistryInt(REG_KEY_TABSIZE_IB, 0);
        pConf->m_languages.cshParams->m_IM = GetRegistryInt(REG_KEY_TABSIZE_IM, 2);
        pConf->m_languages.cshParams->m_IA = GetRegistryInt(REG_KEY_TABSIZE_IA, 2);
        pConf->m_languages.cshParams->m_bSIAttr = GetRegistryInt(REG_KEY_SI_ATTR, TRUE);
        pConf->m_languages.cshParams->m_bSIAttr = GetRegistryInt(REG_KEY_SI_TEXT, TRUE);
    } 

    if (!CRichEditCtrl::Create(dwStyle, rect, pParentWnd, nID))
        return false;
    if (!m_hintsPopup.Create(IDD_POPUPLIST, this))
        return false;
    //Limit the length to 64K - longer text takes a long time to color
    LimitText(LIMIT_TEXT);
    //Disable automatic font change as a result of input language change (but allow 2 fonts for asian...?)
    LRESULT opts = SendMessage(EM_GETLANGOPTIONS);
    SendMessage(EM_SETLANGOPTIONS, 0, (opts & ~IMF_AUTOFONT) | IMF_DUALFONT);
    return true;
}

void CCshRichEditCtrl::OnPaint()
{
    CRichEditCtrl::OnPaint();
    auto pConf = GetConf();
    if (pConf && pConf->m_bCurrentLineHighLight) {
        CHARRANGE cr;
        GetSel(cr);
        if (cr.cpMin==cr.cpMax) {
            CDC *pDC = this->GetDC();
            CRect rect;
            GetClientRect(rect);
            pDC->IntersectClipRect(&rect);
            rect.top = PosFromChar(cr.cpMin).y;
            rect.bottom = rect.top + CharHeight();
            //Check the vertical scrollbar
            //if active we may need to increase the size
            SCROLLINFO si;
            si.cbSize = sizeof(si);
            si.fMask = SIF_POS | SIF_RANGE | SIF_TRACKPOS;
            if (GetScrollInfo(SB_HORZ, &si)) {
                if (si.nMax!=si.nMin) {
                    if (si.nMin!=si.nPos && si.nMin!=si.nTrackPos) 
                        rect.left -= 20;
                    if (si.nMax!=si.nPos && si.nMax!=si.nTrackPos)
                        rect.right += 20;
                }
            }
            const int width = int(round(std::max(1., sqrt(m_parent->m_zoom))));

            if (1) {
                COLORREF crPenColor = RGB(0, 0, 0);
                int nPenStyle = PS_DOT;
                int nEndStyle = PS_ENDCAP_ROUND; // PS_ENDCAP_FLAT;

                // STEP1: Create a LOGBRUSH and create a geometric pen. 
                CBrush brush;
                LOGBRUSH logBrush;

                brush.CreateSolidBrush(crPenColor); //ur line color 
                brush.GetLogBrush(&logBrush);
                CPen pen(nPenStyle | PS_GEOMETRIC | nEndStyle, width, &logBrush, 0, NULL);

                // STEP2: Select the geometric pen and starting drawing line. 
                CPen *pOldPen = (CPen*)pDC->SelectObject(&pen);
                auto old_brush = pDC->SelectStockObject(NULL_BRUSH);

                pDC->RoundRect(rect, CPoint(2,2));

                // STEP3: Clean Up. 
                pDC->SelectObject(pOldPen);
                pDC->SelectObject(old_brush);
                pen.DeleteObject();
            } else {
                pDC->DrawDragRect(rect, CSize(width, width), nullptr, CSize(1, 1));
            }
            this->ReleaseDC(pDC);
        }
    }
}


/** Return the position of the selection in line and column 
* @param [out] lStartLine The line number of the start of the selection
* @param [out] lStartCol The line number of the start of the selection
* @param [out] lEndLine The line number of the end of the selection
* @param [out] lEndCol The line number of the end of the selection
*/
void CCshRichEditCtrl::GetSelLineCol(int &lStartLine, int &lStartCol, int &lEndLine, int &lEndCol) const
{
	CHARRANGE cr;
	GetSel(cr);
	ConvertPosToLineCol(cr.cpMin, lStartLine, lStartCol);
	ConvertPosToLineCol(cr.cpMax, lEndLine, lEndCol);
}

/** Return the content of a line
 * @param [in] line Which line we want
 * @param [out] str The returned content of the line.
 * @returns the length of the line.*/
int CCshRichEditCtrl::GetLineString(int line, CStringA &str)
{
	int nLineLength = LineLength(LineIndex(line)) + sizeof(int) + 1;
    CStringW s;
	nLineLength = GetLine(line, s.GetBufferSetLength(nLineLength), nLineLength);
	s.Truncate(nLineLength); 
	if (s[nLineLength-1] == '\x0d')
		s.Truncate(--nLineLength); //kill terminating lineend
    str = AsUTF8(s);
    return str.GetLength();
}



/** Calculates the tab stop from the current position 
 * @param [in] col The current column of the cursor (must be nonnegative)
 * @param [in] forward true if we look for a tabstop to the right, false if left
 * @param [in] smartIndent A pre-calculated smart ident position if >=0.
 *                        In this we jump to that (if in the right direction), 
 *                        else we align to previndent (so end result differs 
 *                        from prev_indent in 'm_tabsize' (Man, what does this mean?!?)
 * @param [in] prevIndent The previously existing indentation of the line 
 *                       (must be nonnegative)
 * @param [in] strict If true and the actual value of prevIndent lies in the right 
 *                    direction, we jump there
 * @returns the calculated indentation */
int CCshRichEditCtrl::CalcTabStop(int col, bool forward, int smartIndent, int prevIndent, bool strict)
{
	if (smartIndent>=0) {
		if ( forward && col<smartIndent) return smartIndent;
		if (!forward && col>smartIndent) return smartIndent;
	}
	if (strict && prevIndent>=0) {
		if ( forward && col<prevIndent) return prevIndent;
		if (!forward && col>prevIndent) return prevIndent;
	}
    if (forward)
        return ((col+m_tabsize)/m_tabsize)*m_tabsize;
    if (col<=m_tabsize)
        return 0;
    return ((col-1)/m_tabsize)*m_tabsize;
}

/** Sets the indentation of current line by adding or removing leading whitespace
 * Adds or removes spaces so that caret moves to right indent (from lStart) 
 * This assumes there is enough space in front of us if we need to remove 
 * to get to the right indent.
 * We also assume
 * @param target_indent The identation we want 
 * @param current_indent The current identation of the line
 * @param col The current cursor position in the line (lStart)
 * @param lStart The start of the selection or current cursor position
 * @param lEnd The end of the selection or current cursor position
 * @param [in] standalone If true, we disable drawing and adjust selection afterwards.
 *                        If false we assume this is called for consecutive lines
 *                        so we do not disable redraw and leave selection as garbage.
 *                        Also if we are not standalone, we update the CSH so that 
 *                        subsequent lines work well.
 *                        Also, if we are standalone, we update last_change.
 * @returns true if the text has changed */
bool CCshRichEditCtrl::SetCurrentIndentTo(int target_indent, int current_indent, int col,
                                         long lStart, long lEnd, bool standalone)
{
    if (current_indent == -1) 
        current_indent = col; //empty line - use current cursor pos as indent
    if (target_indent == current_indent) return false; //nothing to do
    WindowUpdateStatus state;
    if (standalone)
        state = DisableWindowUpdate();
    int line2, col2;
    ConvertPosToLineCol(lEnd, line2, col2);
    if (target_indent > current_indent) {
        SetSel(lStart-col, lStart-col);
        ReplaceSelUtf8(CStringA(' ', target_indent-current_indent));
    } else {
        SetSel(lStart-col, lStart-col+current_indent-target_indent);
        ReplaceSelUtf8("");
    }
    if (standalone) {
        const long lBegin = lStart - col;
        if (col>=current_indent)
            col += target_indent - current_indent; //move with idented text
        else
            col = target_indent;
        if (col2 >= current_indent)
            col2 += target_indent - current_indent; //move with idented text
        else
            col2 = target_indent;
        SetSel(lBegin+col, lBegin+col2);
        UpdateCSH(CSH); //never pop up hint window after indentation, but update positions
        RestoreWindowUpdate(state);
    } else {
        Csh *csh = GetConf()->m_lang->pCsh.get();
        _ASSERT(csh);
        if (csh==nullptr) return 0;
        CshPos::AdjustList(csh->ColonLabels, lStart-col+1, target_indent-current_indent);
        CshPos::AdjustList(csh->QuotedStrings, lStart-col+1, target_indent-current_indent);
        CshPos::AdjustList(csh->Instructions, lStart-col+1, target_indent-current_indent);
        CshPos::AdjustList(csh->BracePairs, lStart-col+1, target_indent-current_indent);
        CshPos::AdjustList(csh->SqBracketPairs, lStart-col+1, target_indent-current_indent);
    }
    return true;
}

LRESULT CCshRichEditCtrl::OnPasteSpecial(WPARAM wParam, LPARAM /*lParam*/)
{
    //if we do not paste CF_TEXT or CF_UNICODETEXT, we use regular paste
    if (wParam!=CF_TEXT && wParam!=CF_UNICODETEXT) {
        CRichEditCtrl::Paste();
        return 0;
    }

    // Test to see if we can open the clipboard first.
    if (OpenClipboard()) {
        std::string utf8;
        HANDLE hClipboardData;
        if (wParam==CF_TEXT) {
            // Retrieve the Clipboard data (specifying that 
            // we want ANSI text (via the CF_TEXT value).
            hClipboardData = GetClipboardData(CF_TEXT);
            // Call GlobalLock so that to retrieve a pointer
            // to the data associated with the handle returned
            // from GetClipboardData.
            utf8 = (char*)GlobalLock(hClipboardData);
        } else {
            // Retrieve the Clipboard data (specifying that 
            // we want UTF-16 text (via the CF_UNICODETEXT value).
            hClipboardData = GetClipboardData(CF_UNICODETEXT);
            WCHAR *utf16 = (WCHAR *)GlobalLock(hClipboardData);
            utf8 = ConvertFromUTF16_to_UTF8({(const char *)utf16, wcslen(utf16)*2});
        }
        CStringA text(utf8.c_str()); 
        ReplaceTAB(text, m_tabsize);
        const auto state = GetWindowUpdate();
        SetRedraw(false);
        ReplaceSelUtf8(text);
        RestoreWindowUpdate(state);
        // Unlock the global memory.
        GlobalUnlock(hClipboardData);
        // Finally, when finished I simply close the Clipboard
        // which has the effect of unlocking it so that other
        // applications can examine or modify its contents.
        CloseClipboard();
    }
    return 0;
}

/** Automatically re-indents between the two lines (inclusive).
 * Keeps selection. to_line can be -1, which means end-of-file.*/
bool CCshRichEditCtrl::ReIndent(int from_line, int to_line)
{
    if (to_line<0) 
        to_line = GetLineCount()-1;
    if (from_line>to_line)
        return false;
    CWaitCursor c;
    long lStart, lEnd;
    GetSel(lStart, lEnd);
    int sCol, sLine; ConvertPosToLineCol(lStart, sLine, sCol);
    int eCol, eLine; ConvertPosToLineCol(lEnd,   eLine, eCol);
    POINT scroll_pos;
    ::SendMessage(m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&scroll_pos);

    auto state = DisableWindowUpdate();
    bool changed = false;
    CStringA strLine;
    bool local_changed = false;
    for (int i = from_line; i<=to_line; i++) {
        //if the last line and not really selected, skip
        if (i==to_line && eCol==0) {
            lEnd = ConvertLineColToPos(to_line, eCol);
            break;
        }
        if (local_changed)
            UpdateCSH(HINTS_AND_LABELS);
        const int line_begin = ConvertLineColToPos(i, 0);
        int old_indent;
        const int new_indent = GetConf()->m_lang->pCsh->FindProperLineIndent(line_begin, false, &old_indent);
        local_changed = SetCurrentIndentTo(new_indent, old_indent, 0,
                                           ConvertLineColToPos(i, 0), 0L, false);
        changed |= local_changed;
        if (i==sLine && sCol>=old_indent)
            sCol += new_indent-old_indent;
        else if (i==eLine && eCol>=old_indent)
            eCol += new_indent-old_indent;
    }
    SetSel(ConvertLineColToPos(sLine, sCol), ConvertLineColToPos(eLine, eCol));
    ::SendMessage(m_hWnd, EM_SETSCROLLPOS, 0, (LPARAM)&scroll_pos);
    RestoreWindowUpdate(state);
    return changed;
}



/** Implements smart behaviour by capturing relevant messages.
 * Catches if the user presses Ctrl+Space, moves the selection in hint 
 * mode, types a character in hint mode, types {, [ or } (for indenting),
 * presses ENTER, TAB, BACKSPACE (for indenting) or presses ESC
 * (to cancel tracking or hint modes).*/
BOOL CCshRichEditCtrl::PreTranslateMessage(MSG* pMsg)
{
    if (pMsg->message != WM_KEYDOWN && pMsg->message != WM_CHAR)
        return CRichEditCtrl::PreTranslateMessage(pMsg);
    static std::list<std::pair<UINT, WPARAM>> msg;
    msg.emplace_back(pMsg->message, pMsg->wParam);
    GetSel(m_crSel_before);
    ::SendMessage(m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&m_scroll_pos_before);
    long lStart = m_crSel_before.cpMin;
    long lEnd = m_crSel_before.cpMax;
    auto pConf = GetConf();
    if (!pConf) 
        return CRichEditCtrl::PreTranslateMessage(pMsg);
    Csh *csh = pConf->m_lang->pCsh.get();
    _ASSERT(csh);
    if (csh==nullptr) return 0;
    if (InHintMode()) {
        if (pMsg->message == WM_KEYDOWN) { 
            if (pMsg->wParam == VK_ESCAPE) {CancelHintMode(); return TRUE;}
            if (pMsg->wParam == VK_UP)     {m_hintsPopup.m_listBox.UpDownKey(-1); return TRUE;}
            if (pMsg->wParam == VK_DOWN)   {m_hintsPopup.m_listBox.UpDownKey(+1); return TRUE;}
            if (pMsg->wParam == VK_PRIOR)  {m_hintsPopup.m_listBox.UpDownKey(-2); return TRUE;}
            if (pMsg->wParam == VK_NEXT)   {m_hintsPopup.m_listBox.UpDownKey(+2); return TRUE;}
            if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_TAB ||
                (pMsg->wParam == VK_SPACE && GetKeyState(VK_CONTROL) & 0x8000)) {
                const CshHint *item = m_hintsPopup.m_listBox.GetSelectedHint();
                if (item && item->state == HINT_ITEM_SELECTED) {
                    ReplaceHintedString(item);
                    return TRUE;
                }
                //if nothing selected let it run further as if not in hint mode
            }
        } else if (pMsg->message == WM_CHAR) {
            if (isalnum(pMsg->wParam) || pMsg->wParam == '_' || pMsg->wParam<=31 || pMsg->wParam>=127)
                //characters that can make up a hinted keyword -
                //or are control characters
                //we insert them and re-calc hints afterwards in OnCommand
                return CRichEditCtrl::PreTranslateMessage(pMsg);
            //do as in not hint mode if no item is selected
            const CshHint *item = m_hintsPopup.m_listBox.GetSelectedHint();
            if (item) {
                //Do not expand for escape type hints
                if (item->type==EHintType::ESCAPE)
                    return CRichEditCtrl::PreTranslateMessage(pMsg);
                //Expand hint under cursor, if it is full match or prefix
                if (item->state != HINT_ITEM_SELECTED) return CRichEditCtrl::PreTranslateMessage(pMsg);
                if (pMsg->wParam == '.') {
                    //expand only till the next dot in the hint
                    unsigned pos = csh->hintedStringPos.last_pos - csh->hintedStringPos.first_pos;
                    pos = item->plain.find_first_of('.', pos);
                    if (pos != string::npos) {
                        item->replaceto = item->plain.substr(0, pos);
                        ReplaceHintedString(item);
                        return CRichEditCtrl::PreTranslateMessage(pMsg);
                    }
                }
                //Replace only if we do not allow free text to be entered
                if (!m_hintsPopup.m_listBox.m_csh_store.allow_anything)
                    ReplaceHintedString(item);
                return CRichEditCtrl::PreTranslateMessage(pMsg);
            } //else continue with non hint-mode stuff
        }
    }
    //Handle a batch of WM_KEYDOWN events
    if (pMsg->message == WM_KEYDOWN) {
        //show hints for ctrl+space
        if (pMsg->wParam == VK_SPACE && GetKeyState(VK_CONTROL) & 0x8000) {
            m_bUserRequested = true;
            //just update CSH records for hints
            UpdateCSH(HINTS_AND_LABELS);
            //see if we are at the beginning of a word (except escape hints)
            bool till_cursor_only = false;
            if (lStart==0) till_cursor_only = true;
            else if (csh->hadEscapeHint) {
                CStringA str;
                GetTextRangeUtf8(lStart-1, lStart, str);
                //Non-ANSI characters make isalnum to throw an assert
                //Non-ANSI characters are assumed to be alphanumeric
                if (str[0]>=0 && str[0]!='_' && !isalnum(static_cast<unsigned char>(str[0])))
                    till_cursor_only = true;
            }
            //if so, set the hint mode such that we only consider the word under the curson
            //up to the cursor (and cut away the remainder)
            StartHintMode(till_cursor_only);
            return TRUE;
        }

        //Kill tracking mode, start fading and restore home category for Escape
        //(for full screen mode this editor will not be in the focus and will not get
        //the message - that escape will be handled by CMainFrm::PreTranslateMessage())
        if (pMsg->wParam == VK_ESCAPE) {
            CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(AfxGetMainWnd());
            if (pMainWnd) 
                    pMainWnd->DoEscapePressed();
            return TRUE;
        }
        //Highlight if F4 (adding F4 to an accelerator does not help for some reason)
        if (pMsg->wParam == VK_F4) {
            CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(AfxGetMainWnd());
            if (pMainWnd) {
                CMscGenDoc *pDoc = dynamic_cast<CMscGenDoc *>(pMainWnd->GetActiveDocument());
                if (pDoc) {
                    pDoc->OnTrackHere();
                    return TRUE;
                }
            }
        }

        //call parent if not keys relevant for smart indenting
        if (pMsg->wParam != VK_TAB && pMsg->wParam != VK_RETURN && pMsg->wParam != VK_BACK)
            return CRichEditCtrl::PreTranslateMessage(pMsg);
    }

    //Do nothing for characters other than braces and opening square bracket
    if (pMsg->message == WM_CHAR && pMsg->wParam != '}' && 
        pMsg->wParam != '{' && pMsg->wParam != '[' && pMsg->wParam != ']')
        return CRichEditCtrl::PreTranslateMessage(pMsg);

    //At this point we have either WM_CHAR and a brace/bracket
    //or WM_KEYDOWN and TAB, ENTER OR BKSPACE

	int line, col;
	ConvertPosToLineCol(lStart, line, col);
	const int current_indent = csh->FirstCurrentLineIndent(ConvertLineColToPos(line,0));

    //Handle {}[] and ENTER
    char buff[2] = {0, 0}; //we will insert this string
    if (pMsg->wParam == VK_RETURN) {
        buff[0] = '\n';
    } else if (pMsg->wParam == '{' || pMsg->wParam == '[' || pMsg->wParam == '}' || pMsg->wParam == ']') {
        //if we are not at the beginning of a line do nothing
        if (current_indent!=-1 && current_indent<col) 
            return CRichEditCtrl::PreTranslateMessage(pMsg); //not in leading whitespace
        buff[0] = pMsg->wParam;
    }
    if (buff[0]) {
        if (!pConf->m_bSmartIndent) return CRichEditCtrl::PreTranslateMessage(pMsg);
        WindowUpdateStatus state = DisableWindowUpdate();
        ReplaceSelUtf8(buff, FALSE);
        m_parent->UpdateNumbers(false, false); //We only do stuff here if the inserted char is a newline.
        UpdateCSH(HINTS_AND_LABELS);
        GetSel(lStart, lEnd);
        ConvertPosToLineCol(lStart, line, col);
        //if the user pressed an enter at the end of the label,
        //indent as if part of the label
        int indent = csh->FindProperLineIndent(lStart, pMsg->wParam == VK_RETURN);
        const int current_indent2 = csh->FirstCurrentLineIndent(ConvertLineColToPos(line, 0));
        SetCurrentIndentTo(indent, current_indent2, col, lStart, lEnd, true);
        //if we have inserted a char, set cursor after it
        if (pMsg->wParam != VK_RETURN)
            indent++;
        lStart = ConvertLineColToPos(line, indent);
        SetSel(lStart, lStart);
        //always call it: we get here only if we insert {, [, or RETURN, so we 
        //have always changed the text and need to update coloring/
        UpdateCSH(CSH);
        RestoreWindowUpdate(state);
        StartHintMode(false); //re-position hint window or start hinting at line begin (after an enter)
        return TRUE;
    }

    if (pMsg->wParam == VK_BACK) {
        //in case of ctrl+backspace we do not do
        //any smart indent. (Why not??)
        if (bool(GetKeyState(VK_CONTROL) & 0x8000)) 
            return CRichEditCtrl::PreTranslateMessage(pMsg);
		//if not in leading whitespace or at beginning of line
        if ((col==0) || (current_indent!=-1 && current_indent<col)) 
            return CRichEditCtrl::PreTranslateMessage(pMsg);
		//in leading whitespace, consider smart indent and/or previous line indent
        if (!pConf->m_bSmartIndent) return CRichEditCtrl::PreTranslateMessage(pMsg);
        if (lStart!=lEnd) return CRichEditCtrl::PreTranslateMessage(pMsg);
        //OK, here either there is nothing in the line or we are in leading whitespace,
        //but never at the beginning of the line.
        const int smartIndent = csh->FindProperLineIndent(lStart, false);
        if (current_indent==col && smartIndent<col) {
            //if at the head of the text and required pos is before us - indent line
            SetCurrentIndentTo(smartIndent, current_indent, col, lStart, lEnd, true);
            return TRUE;
        }
        const int indent = CalcTabStop(col, false, smartIndent, current_indent, true);
        if (current_indent!=-1) {
            //if at the head of the text and required pos is NOT before us - 
            //move to previous tab stop
            SetCurrentIndentTo(indent, current_indent, col, lStart, lEnd, true);
        } else {
            //backspace from col to indent
            _ASSERT(indent<col);
            auto state2 = DisableWindowUpdate();
            SetSel(ConvertLineColToPos(line, indent), lEnd);
            ReplaceSelUtf8("");
            UpdateCSH(CSH); //never pop up hint window after a bkspace, but update stuff
            RestoreWindowUpdate(state2);
        } 
        return TRUE;
	}
    //Here are the rules for TAB
    //1. if TAB mode is on, we indent all lines in selection correctly.
    //2. if TAB mode is off
    //    - if the selection is a cursor
    //        * if we are in the leading whitespace 
    //             we indent the line correctly and jump to the beginning of the line
    //        * else we insert as many spaces to fall to int multiple of m_tabsize
    //    - if the selection is a range, we delete it and fall to int multiple of m_tabsize
    if (pMsg->wParam == VK_TAB) {
        CancelHintMode();
        const bool shift = bool(GetKeyState(VK_SHIFT) & 0x8000);
        if (pConf->m_bTABIndents) { //TAB mode
            int eLine, eCol;
            ConvertPosToLineCol(lEnd, eLine, eCol);
            bool changed = false;
            auto state2 = DisableWindowUpdate();
            if (line == eLine) {
                const int indent = csh->FindProperLineIndent(lStart, false);
                changed = SetCurrentIndentTo(indent, current_indent, col, lStart, lEnd, true);
            } else { 
                changed = ReIndent(line, eLine);
            }
            if (changed)
                UpdateCSH(CSH); //never pop up hint window after a TAB
            RestoreWindowUpdate(state2);
        } else { //no smart indent or no tab mode
            //if no selection:
            if (lStart == lEnd) {
                int indent;
                //just insert/remove spaces up to the next/prev tab stop
                //if not in leading whitespace
                if (current_indent!=-1 && current_indent<col) {
                    indent = CalcTabStop(col, true);
                } else {
                    //in leading whitespace, consider smart ident and/or previous line indent
                    const int smartIndent = csh->FindProperLineIndent(lStart, false);
                    indent = CalcTabStop(col, !shift, smartIndent, current_indent, true);
                }
                if (indent != col) {
                    const auto state2 = DisableWindowUpdate();
                    if (indent<col) {
                        SetSel(lStart+(indent-col), lStart);
                        ReplaceSelUtf8("");
                    } else {
                        ReplaceSelUtf8(CStringA(' ', indent-col));
                    }
                    UpdateCSH(CSH); //ignore return value and do not pop up hints
                    RestoreWindowUpdate(state2);
                }
            } else {
                //a full selection, do indentation
                //Leave last_change as if we deleted the whole selection
                //and inserted something - we do not track where do we change what

                int startLine = line;
                int endLine, endCol;
                ConvertPosToLineCol(lEnd, endLine, endCol);
                //If last line is not really selected, skip it
                if (endCol==0) {
                    endLine--;
                }
                //Calculate the amount of space to add/remove based on the first non-empty line in the selection
                int headLine;
                int headCol = -1;
                for (headLine = startLine; headLine<endLine; headLine++) {
                    headCol = csh->FirstCurrentLineIndent(LineIndex(headLine));
                    if (headCol >= 0) break;
                }
                //if all lines empty, nothing to do 
                if (headCol == -1) return TRUE;
                const int smartIndent = csh->FindProperLineIndent(LineIndex(headLine), false);
                const int offset = CalcTabStop(headCol, !shift, smartIndent, headCol, true) - headCol;

                //Insert/remove "offset" amount of space from the beginning of each line
                const auto state2 = DisableWindowUpdate();
                POINT scroll_pos;
                ::SendMessage(m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&scroll_pos);
                CStringA spaces(' ', std::max(0, offset)); //empty string if we remove, otherwise as many spaces as we insert
                for (int l = startLine; l<=endLine; l++) {
                    const long lLineBegin = LineIndex(l);
                    const int current_indent2 = csh->FirstCurrentLineIndent(lLineBegin);
                    const int adjusted_offset = -std::min(current_indent2, -offset);  //reduce potential remove if not so many spaces at the beginning
                    //empty line - do nothing
                    if (current_indent2==-1) continue;
                    SetSel(lLineBegin, lLineBegin + std::max(0, -adjusted_offset)); //empty selection if we insert, otherwise the spaces to remove
                    ReplaceSelUtf8(spaces);

                    //Adjust lStart and lEnd
                    if (l == startLine && adjusted_offset<0) { //can happen only once: at the first line
                        lStart += adjusted_offset;
                        if (lStart < lLineBegin) lStart = lLineBegin;
                    }
                    lEnd += adjusted_offset;
                }
                SetSel(lStart, lEnd);
                ::SendMessage(m_hWnd, EM_SETSCROLLPOS, 0, (LPARAM)&scroll_pos);
                UpdateCSH(CSH); //ignore if we need to hint
                RestoreWindowUpdate(state2);
            }
        }
        return TRUE;
    }
    return CRichEditCtrl::PreTranslateMessage(pMsg);
}

/** Changes the text in the control to `text` and sets cursor pos.
* After that it does a full coloring
* We never notify the document object about a change in this function,
* as it is called by the document object only.*/
void CCshRichEditCtrl::UpdateText(const char *text, int lStartLine, int lStartCol, int lEndLine, int lEndCol, const POINT &scr)
{
    CHARRANGE cr;
    cr.cpMin = ConvertLineColToPos(lStartLine, lStartCol);
    cr.cpMax = ConvertLineColToPos(lEndLine, lEndCol);
    UpdateText(text, cr, scr);
}

/** Set the text of the control to a UTF-8 string. */
void CCshRichEditCtrl::SetUtf8Text(const char *utf8)
{
    DWORD hi, lo;
    ::SendMessage(m_hWnd, EM_GETZOOM, (WPARAM)&hi, (LPARAM)&lo);
    auto state = DisableWindowUpdate();
    SETTEXTEX TextInfo = {0};
    TextInfo.flags = ST_DEFAULT;
    TextInfo.codepage = CP_UTF8;
    ::SendMessage(m_hWnd, EM_SETTEXTEX, (WPARAM)&TextInfo, (LPARAM)(const char*)utf8);
    ::SendMessage(m_hWnd, EM_SETZOOM, (WPARAM)hi, (LPARAM)lo);
    RestoreWindowUpdate(state);
}

void CCshRichEditCtrl::ReplaceSelUtf8(const char *utf8, bool bCanUndo)
{
    SETTEXTEX TextInfo = {0};
    TextInfo.flags = ST_SELECTION;
    if (bCanUndo) TextInfo.flags |= ST_KEEPUNDO;
    TextInfo.codepage = CP_UTF8;
    ::SendMessage(m_hWnd, EM_SETTEXTEX, (WPARAM)&TextInfo, (LPARAM)(const char*)utf8);
}

/** Get the text of the control as a UTF-8 string.
 * In some cases even though GETTEXTLENGTH returns nonzero, GETTEXT returns
 * the empty string. (Some unicode chars required.) In these cases
 * we just return the empty string even though the cursor pos will be large
 * and there is in fact some string in the edit control.
 * To me this seems a Windows bug.*/
std::string CCshRichEditCtrl::GetUtf8TextAsStdString() const {
    GETTEXTLENGTHEX TextLenInfo = {0};
    TextLenInfo.codepage = CP_UTF8;
    TextLenInfo.flags = GTL_CLOSE; //get approximation for buffer size
    int buff_len = ::SendMessage(m_hWnd, EM_GETTEXTLENGTHEX, (WPARAM)&TextLenInfo, (LPARAM)0);
    std::string ret;
    if (buff_len) {
        GETTEXTEX TextInfo = {0};
        TextInfo.cb = buff_len; //lenth of teh buffer in bytes
        TextInfo.flags = GT_DEFAULT | //Line endings not converted to CRLF
            GT_NOHIDDENTEXT;
        TextInfo.codepage = CP_UTF8;
        TextInfo.lpDefaultChar = nullptr;  //Use system default if a char cannot be represented in UTF8
        TextInfo.lpUsedDefChar = nullptr;  //We dont want feedback if a char cannot be represented in UTF8
        ret.resize(buff_len);
        ::SendMessage(m_hWnd, EM_GETTEXTEX, (WPARAM)&TextInfo, (LPARAM)&ret.front());
        //now replace \r's with '\n's - and find true string end.
        for (auto i = ret.begin(); i!=ret.end(); i++)
            if (*i=='\r') *i = '\n';
            else if (*i==0) {
                ret.erase(i, ret.end());
                break;
            }
    }
    return ret;
}

/** Get the text of the control as a UTF-8 string.*/
CStringA CCshRichEditCtrl::GetUtf8TextAsCStringA() const
{
    //Same as GetUtf8TextAsStdString, but with a different return
    GETTEXTLENGTHEX TextLenInfo = {0};
    TextLenInfo.codepage = CP_UTF8;
    TextLenInfo.flags = GTL_CLOSE; //get approximation for buffer size
    int buff_len = ::SendMessage(m_hWnd, EM_GETTEXTLENGTHEX, (WPARAM)&TextLenInfo, (LPARAM)0);
    CStringA ret;
    if (buff_len) {
        GETTEXTEX TextInfo = {0};
        TextInfo.cb = buff_len; //lenth of teh buffer in bytes
        TextInfo.flags = GT_DEFAULT | //Line endings not converted to CRLF
            GT_NOHIDDENTEXT;
        TextInfo.codepage = CP_UTF8;
        TextInfo.lpDefaultChar = nullptr;  //Use system default if a char cannot be represented in UTF8
        TextInfo.lpUsedDefChar = nullptr;  //We dont want feedback if a char cannot be represented in UTF8
        auto p = ret.GetBuffer(buff_len+1);
        ::SendMessage(m_hWnd, EM_GETTEXTEX, (WPARAM)&TextInfo, (LPARAM)p);
        ret.ReleaseBuffer(strlen(p)); //we assume null terminated return
        //now replace \r's with '\n's
        ret.Replace('\r', '\n');
    }
    return ret;
}

/** Get the text of a range as a UTF-8 string. */
void CCshRichEditCtrl::GetTextRangeUtf8(long start, long end, CStringA &str)
{
    if (end<=start) {
        str.Empty();
        return;
    }
    GETTEXTEX TextInfo = {0};
    TextInfo.cb = (end-start)*4; //one char is at most 4 bytes in UTF-8
    TextInfo.flags = GT_DEFAULT | //Line endings not converted to CRLF
        GT_NOHIDDENTEXT | GT_SELECTION;
    TextInfo.codepage = CP_UTF8;
    TextInfo.lpDefaultChar = nullptr;  //Use system default if a char cannot be represented in UTF8
    TextInfo.lpUsedDefChar = nullptr;  //We dont want feedback if a char cannot be represented in UTF8
    char * buff = (char*)malloc(TextInfo.cb);

    CHARRANGE cr;
    GetSel(cr);
    auto status = DisableWindowUpdate();
    SetSel(start, end);
    ::SendMessage(m_hWnd, EM_GETTEXTEX, (WPARAM)&TextInfo, (LPARAM)buff);
    SetSel(cr);
    RestoreWindowUpdate(status);

    str = buff;
    free(buff);
}


/** Set the text of the control to a Unicode string. */
void CCshRichEditCtrl::SetUnicodeText(const CStringW &wstring)
{
    SETTEXTEX TextInfo = {0};
    TextInfo.flags = ST_DEFAULT;
    TextInfo.codepage = 1200; //=Unicode
    ::SendMessage(m_hWnd, EM_SETTEXTEX, (WPARAM)&TextInfo, (LPARAM)(const wchar_t*)wstring);
}
/** Get the text of the control as a Unicode string. */
void CCshRichEditCtrl::GetUnicodeText(CStringW &wstring) const
{
    GETTEXTLENGTHEX TextLenInfo = {0};
    TextLenInfo.codepage = 1200; //=Unicode
    TextLenInfo.flags = GTL_CLOSE; //get approximation for buffer size
    int buff_len = ::SendMessage(m_hWnd, EM_GETTEXTLENGTHEX, (WPARAM)&TextLenInfo, (LPARAM)0);
    GETTEXTEX TextInfo = {0};
    TextInfo.cb = buff_len; //lenth of teh buffer in bytes
    TextInfo.flags = GT_DEFAULT | //Line endings not converted to CRLF
        GT_NOHIDDENTEXT;
    TextInfo.codepage = CP_UTF8;
    TextInfo.lpDefaultChar = nullptr;  //Use system default if a char cannot be represented in UTF8
    TextInfo.lpUsedDefChar = nullptr;  //We dont want feedback if a char cannot be represented in UTF8
    wchar_t * buff = (wchar_t*)malloc(buff_len);
    ::SendMessage(m_hWnd, EM_GETTEXTEX, (WPARAM)&TextInfo, (LPARAM)buff);
    wstring = buff;
    free(buff);
}


struct TextDeltaResult
{
    long same_at_front;     ///<Number of characters same in the two strings at the beginning
    long same_at_end;       ///<Number of characters same in the two strings at the end
    std::string_view diff;  ///<The part that has changed in the new string
};

/** Determines how many UTF-8 characters are the same at the beginning and
 * at the end of two strings.*/
TextDeltaResult UTF8Delta(std::string_view told, std::string_view tnew)
{
    long same_at_front = 0, same_at_end = 0; //number of characters unchanged at the beginning and end of the file
    const char* first_pos = tnew.data()+tnew.length(); //first byte of the first character at the beginning of the text, which differs in 't'
    const char* last_pos = tnew.data()+tnew.length();  //first byte of the first char at he end of the text that is the same in both (pointer is in 't')
    const int minlen = std::min(told.length(), tnew.length());
    if (minlen) {
        //An utf8 char is eiter 0x00-0x7f (top bit clear, ascii)
        //or one char 0xc0-0xff (two top bits clear) followed by one or more 0x70-0xbf (top bit set, second top bit clear)
        for (const char* pO = told.data(), *pT = tnew.data(); *pO && *pT; pO++, pT++)
            if (*pO != *pT) {
                if ((*pT & 0xc0)==0x80) { //a subsequent byte of a multi-byte UTF-8 char differs. Undo the advance we did at its first char
                    same_at_front--;
                    while ((*pT & 0xc0) != 0xc0) pT--; //go back to the first char of the UTF-8 symbol.
                } 
                first_pos = pT; //point to the first differing char
                break;
            } else if (!(*pT & 0x80) || ((*pT & 0xc0) == 0xc0)) same_at_front++, first_pos = pT;
        if (first_pos<last_pos)
            for (const char* pO = told.data() + told.length() - 1, *pT = tnew.data() + tnew.length() - 1
                 ; pO >= told.data() && pT >= first_pos && same_at_front+same_at_end<minlen //use first pos to avoid overlapping equal ranges, check told.len() to avoid overlaps between the end of the common prefix and the end of the inserted text
                 ; pO--, pT--) 
                if (*pO != *pT) break;
                else if (!(*pT & 0x80) || ((*pT & 0xc0) == 0xc0)) same_at_end++, last_pos = pT;
    }
    return { same_at_front, same_at_end, {first_pos, size_t(last_pos-first_pos)} };
}


/** Changes the text in the control to `text` and sets cursor & scroll pos.
 * After that it does a full coloring
 * We never notify the document object about a change in this function,
 * as it is called by the document object only.*/
void CCshRichEditCtrl::UpdateText(const char *text, const CHARRANGE &cr, const POINT &scr)
{
    CStringA t = text;
    RemoveCRLF(t);

    CancelHintMode();
    const auto state = DisableWindowUpdate();
    auto csh_mode = FORCE_CSH;
    if (auto d = UTF8Delta(m_prev_text, std::string_view(t, t.GetLength())); d.same_at_end || d.same_at_front) {
        SetSel(d.same_at_front, UTF8len(m_prev_text) - d.same_at_end);
        std::string repl(d.diff);
        ReplaceSelUtf8(repl.c_str(), false);
        csh_mode = CSH;
    } else
        SetUtf8Text(t); //Hard reset of text, clears all formatting with ASCII was: SetWindowText(t);
    
    CHARRANGE loc_cr = cr;
    SetSel(loc_cr);
    ::SendMessage(m_hWnd, EM_SETSCROLLPOS, 0, (LPARAM)&scr);
    if (IsInternalEditorVisible())
        UpdateCSH(csh_mode); //no hinting
    RestoreWindowUpdate(state);
    m_parent->UpdateNumbers(false, true);
    SetFocus();

    if (t.GetLength()>LIMIT_TEXT && m_parent) {
        m_parent->SetReadOnly();
        MessageBox(_T("The internal editor can only handle files up to ") _T(LIMIT_TEXT_STR)
                   _T(". Use an external editor to edit."),
                   _T("Msc-generator"), MB_OK | MB_ICONERROR);
    }
}




/** Updates CSH in the editor window and notifies the document.
@param [in] updateCSH Tells us what to update. 
                      HINTS_AND_COLON_LABELS re-parses, updates hints & colon_labels
                      but does not re-color and leaves m_prev_text, CshList and CshErrors 
                      intact. Used when Ctrl+Space has been 
                      pressed or during indentation. 
                      CSH updates both hints and recolor the text
                      (only if m_bShowCSH are set).
                      FORCE_CSH forces a full recoloring (discarding delta).
@returns In hint node we return true if we keep hinting the same string. Outside
         hint mode we return true if the user typed (so that we may need to enter
         hint mode).

About position values.
CSH entries start with the first character of the file indexed as 1.
Thus, if a csh entry corresponds to the first character (only) of a file,
it has 'first_pos = 1' and 'last_pos = 1'. For the first three letter 
of the file these values are 1 and 3.
In contrast, RichEditCtrl values start being indexed from zero.
Thus if the caret is before the first character, we get lStart=lEnd = 0.
*/

bool CCshRichEditCtrl::UpdateCSH(UpdateCSHType updateCSH)
{
    bool ret = false;
    auto pConf = GetConf();
    if (!pConf) return false;
    switch (updateCSH) {
    default:
        break;
    case CSH:
        if (pConf->m_bShowCsh || pConf->m_bShowCshErrors ||
            pConf->m_bShowCshErrorsInWindow)
            break;
        FALLTHROUGH;
    case HINTS_AND_LABELS:
        //nothing to do if we do not hint or do smart indent
        if (pConf->m_bSmartIndent || pConf->m_bHints)
            break;
        return false;
    }
    string text = GetUtf8TextAsStdString();    //with ASCII was: GetWindowText(text);
    //No need to remove CR ('\r') characters, GetUtf8Text() returns '\n' only linefeeds
    //Ensure non empty file ends in \n. Parsing fails sometimes without this.
    if (text.length() && text.back()!='\n')
        text.push_back('\n');
    //record cursor position 
    CHARRANGE cr;
    GetSel(cr);

    Csh *csh = pConf->m_lang->pCsh.get();
    _ASSERT(csh);
    if (csh==nullptr) return false;
    CMscGenDoc *pDoc = GetMscGenDocument();
    //pDoc can be legally nullptr here before the first view is established.
    if (pDoc)
        csh->FileName = AsUTF8(pDoc->GetPathName());
    //if we only refresh Hints ColonLabels and QuotedStrings
    if (updateCSH==HINTS_AND_LABELS) {
        //Save old CSH entries
        CshListType save_csh = std::move(csh->CshList);
        CshErrorList save_csh_errors = std::move(csh->CshErrors);
        //Parse the text
        CshPos old_uc = csh->hintedStringPos;
        csh->ParseText(text, cr.cpMax == cr.cpMin ? cr.cpMin : -1, pConf->m_Pedantic);
        //restore old csh and error list - the ones matching the (yet unchanged) m_prev_text
        csh->CshList.swap(save_csh);
        csh->CshErrors.swap(save_csh_errors);
        //Here the csh entries will be unchanged (we do not update csh)
        //but the hints and labels will be refreshed.
        //If we consider the hinted string only up to the cursor, trim the returned pos
        if (m_bTillCursorOnly && csh->hintedStringPos.last_pos>cr.cpMin)
            csh->hintedStringPos.last_pos = cr.cpMin;
        //return true if the past and new csh->hintedStringPos overlap
        return csh->hintedStringPos.first_pos <= old_uc.last_pos && old_uc.first_pos<=csh->hintedStringPos.last_pos;
        //Here we do not update the doc. HINTS_AND_LABELS are
        //only used, when 1) user pressed Ctrl+Space (no change to text)
        //or 2) when we do smart indent (will call this function again with CSH,
        //when we are done). We also do not update the mscgen_compat status bar icon
    }

    //now updateCSH is either CSH or FORCE_CSH
    /* Compute diff to last text */
    auto d = UTF8Delta(m_prev_text, text);
    const long old_len = UTF8len(m_prev_text), new_len = UTF8len(text);
    long start = d.same_at_front;
    long ins = std::max(std::max(new_len - d.same_at_front - d.same_at_end, new_len-old_len), 0L);
    long del = std::max(long(0), (old_len+ins)-new_len);
    _ASSERT(ins>=0 && del>=0);
    const bool text_changed = ins || del;

    //Now expand the changed range to include the selection before the action.
    //This is needed for te (unformatted or badly formatted) 
    //text which is equal to the text already there, the above algorithm will not 
    //detect the change and coloring will remain as pasted.
    //Note that start is now in RichEditCtrl space: if we insert at the beginning
    //of the file, start is zero.
    if (m_crSel_before.cpMin < m_crSel_before.cpMax) {
        if (ins==0 && del==0) {
            //if the text has not changed, all the change we register here
            //is the selection
            del = ins = m_crSel_before.cpMax - m_crSel_before.cpMin;
            start = m_crSel_before.cpMin;
        } else {
            if (m_crSel_before.cpMin < start) {
                //if the selection started before our detected start of the change
                ins += start - m_crSel_before.cpMin;
                del += start - m_crSel_before.cpMin;
                start = m_crSel_before.cpMin;
            }
            if (m_crSel_before.cpMax > start + del) {
                //if the change ended after our detected end of change
                ins += m_crSel_before.cpMax - start - del;
                del = m_crSel_before.cpMax - start;
            }
        }
    }

    const auto state = GetWindowUpdate();
    CHARFORMAT *const scheme = pConf->m_csh_cf[csh->params->color_scheme];

    if (updateCSH == FORCE_CSH) {
        //if we force a csh update (e.g., due to switching csh schemes)
        //we force a fill update.
        //(however, we have correctly calculated ins,del,start above, since
        //those may be needed by MscGenDoc to decide on grouping undo)
        csh->CshList.clear();
        csh->CshErrors.clear();
    }
    //if we had csh entries before, adjust them according to ins/del
    if (csh->CshList.size() || csh->CshErrors.error_ranges.size()) {
        //Since start is as if the first char in the file is indexed
        //at zero, we add one (csh entries are indexed from 1).
        CshPos::AdjustList(csh->CshList,                start+1, ins-del);
        CshPos::AdjustList(csh->CshErrors.error_ranges, start+1, ins-del);
        //We do not adjust ColonLabels/QoutedStrings/CshErrors.error_texts 
        //here, since that is not needed
        //for calculating coloring diffs, only to smart indent and error labels.
        //If we inserted, destroy any marking overlapping with the insertion.
        if (ins) {
            for (auto &pos : csh->CshList)
                if (pos.first_pos <= start+1+ins &&
                    start+1 <= pos.last_pos)
                    pos.color = COLOR_NORMAL;
            for (auto &pos : csh->CshErrors.error_ranges)
                if (pos.first_pos <= start+1+ins &&
                    start+1 <= pos.last_pos)
                    pos.color = COLOR_MAX;
        }
    }
    //PARSE the text for color syntax
    //Take the last hinted string from m_csh (before overwriting it by m_designlib_csh)
    CshPos old_uc = csh->hintedStringPos;
    //move the last list to old_xxx
    CshListType old_csh_list = std::move(csh->CshList);
    CshErrorList old_csh_error_list = std::move(csh->CshErrors);
    //Take the design, color and style definitions from the designlib
    csh->ParseText(text, cr.cpMax == cr.cpMin ? cr.cpMin : -1, pConf->m_Pedantic); //we pass ownership of 'text' to csh
    //If we consider the hinted string only up to the cursor, trim the returned pos
    if (m_bTillCursorOnly && csh->hintedStringPos.last_pos>cr.cpMin && !csh->hadFileHint)
        csh->hintedStringPos.last_pos = cr.cpMin;

    //retune true if the past and new csh->hintedStringPos overlap
    ret = csh->hintedStringPos.first_pos <= old_uc.last_pos && old_uc.first_pos<=csh->hintedStringPos.last_pos;
    //if we are not in hint mode, we return true if we should enter one
    if (!InHintMode() && ins-del>=1)
        ret = true;

    //List the parse errors in the error window
#ifndef SHARED_HANDLERS
    if ((pConf->m_bShowCshErrorsInWindow || updateCSH == FORCE_CSH) && theApp.m_pWndOutputView) {
        MscError Error;
        Error.AddFile("Hint");
        std::vector<CString> errors;
        std::vector<FileLineCol> error_pos;
        //populate list only if we need to show the errors and not judt FORCE_CSH
        if (pConf->m_bShowCshErrorsInWindow) {
            errors.reserve(csh->CshErrors.error_ranges.size());
            error_pos.reserve(csh->CshErrors.error_ranges.size());
            for (const auto &e : csh->CshErrors.error_ranges) {
                int line, col;
                ConvertPosToLineCol(e.first_pos, line, col);
                line++; col++; //Needed as errors are indexed from one, positions from zero
                error_pos.emplace_back(CMscGenDoc::FILE_NO_FOR_CSH_ERRORS, line, col);
                errors.push_back(AsUnicode(Error.FormulateElement(FileLineCol(0, line, col), FileLineCol(0, line, col), 
                                                                  ErrorElement::EType::Error, false, false, true, 
                                                                  ErrorElement::ELineType::Main, e.text).text));
            }
        }
        theApp.m_pWndOutputView->ShowCshErrors(errors, std::move(error_pos));
        SetFocus(); //back to editor
    }
#endif
    //See if we may need to touch the screen
    if (((pConf->m_bShowCsh || pConf->m_bShowCshErrors) && updateCSH >= CSH) || updateCSH == FORCE_CSH) {
        //create a diff 
        //csh_list is unfortunately not always sorted by first_pos, but close.
        csh->CshList.SortByPos();
        //Test if both csh->CshList and csh->CshErrors.error_ranges are sorted and non-overlapping
        ASSERT(csh->CshErrors.CheckIfErrorOrMaxColorsOnly());
        ASSERT(CheckOrderedAndNonOverlapping(csh->CshErrors.error_ranges));
        ASSERT(CheckOrderedAndNonOverlapping(csh->CshList));

        CshListType csh_delta, csh_error_delta;
        if (updateCSH == FORCE_CSH) {
            csh_delta = csh->CshList;
            csh_error_delta.reserve(csh->CshErrors.error_ranges.size());
            for (const auto &e : csh->CshErrors.error_ranges)
                csh_error_delta.push_back(e); //copy only the CshEntry part of e (which is of type CshError)
        } else {
            csh_error_delta.DiffInto(old_csh_error_list.error_ranges, csh->CshErrors.error_ranges, COLOR_MAX);
            //kill any entries overlapping with a no_error - so they are forced to be refreshed
            for (auto &err : csh_error_delta)
                if (err.color == COLOR_MAX)
                    for (auto &pos : old_csh_list)
                        if (pos.first_pos <= err.last_pos && err.first_pos <= pos.last_pos)
                            pos.color = COLOR_MAX;
            csh_delta.DiffInto(old_csh_list, csh->CshList, COLOR_NORMAL);
        }


        //See if we actually need to touch the screen
        //Note: if we have inserted, we need to make that text COLOR_NORMAL even if csh_delta is
        //empty.
        if (ins || csh_delta.size() || csh_error_delta.size() || updateCSH==FORCE_CSH) {

            //Ok now copy the delta to the editor window
            //record scroll position 
            POINT scroll_pos;
            ::SendMessage(m_hWnd, EM_GETSCROLLPOS, 0, (LPARAM)&scroll_pos);

            //freeze screen, prevent visual updates
            DisableWindowUpdate();
            //Erase all formatting on a full update (deltas contain all entries in this case)
            if (updateCSH == FORCE_CSH) {
                SetSel(0, -1); //select all
                SetSelectionCharFormat(scheme[COLOR_NORMAL]); //set formatting to neutral
            } else if (ins) {
                SetSel(start, start+ins); //select inserted text
                SetSelectionCharFormat(scheme[COLOR_NORMAL]); //set formatting to neutral
            }

            //First erase formatting from parts, where errors ceased to exist
            for (auto &e : csh_error_delta)
                if (e.color==COLOR_MAX) {
                    SetSel(e.first_pos-1, e.last_pos);
                    SetSelectionCharFormat(scheme[COLOR_NORMAL]);
                }

            //Now add coloring to parts where it changed 
            if (pConf->m_bShowCsh)
                for (auto &e : csh_delta)
                    if (e.color<COLOR_MAX) {
                        SetSel(e.first_pos-1, e.last_pos);
                        SetSelectionCharFormat(scheme[e.color]);
                    }
            //Now add errors, if we show them
            if (pConf->m_bShowCshErrors)
                for (auto &e : csh_error_delta)
                    if (e.color<COLOR_MAX) {
                        SetSel(e.first_pos-1, e.last_pos);
                        SetSelectionCharFormat(scheme[e.color]);
                    }

            //restore cursor and scroll position
            SetSel(cr);
            ::SendMessage(m_hWnd, EM_SETSCROLLPOS, 0, (LPARAM)&scroll_pos);
        }
    }
    m_prev_text = std::move(text);

    //Ok, notify the document if the text has actually changed
    //We do this even if notifications to doc are disabled - those only refer to 
    //changes in the selection.
    if (text_changed && pDoc) 
        pDoc->OnInternalEditorChange(start, ins, del, m_crSel_before, m_scroll_pos_before);

    RestoreWindowUpdate(state);

    //Update status bar (compatibility mode pane)
    msc::MscCsh *p = dynamic_cast<msc::MscCsh*>(csh);
    if (p) {
        CMainFrame *pWnd = dynamic_cast<CMainFrame *>(AfxGetMainWnd());
        if (pWnd)
            pWnd->m_mscgen_mode_ind = p->mscgen_compat == EMscgenCompat::FORCE_MSCGEN;
    } //else current language is not "msc"
    return ret;
}

//Complete re-coloring and font re-set 
void CCshRichEditCtrl::ReCsh()
{
    auto state = DisableWindowUpdate();
    CHARRANGE cr;
    GetSel(cr);
    SetSel(0, -1); //all
    SetFont(m_parent->m_Font.get());
    UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
    SetSel(cr);
    RestoreWindowUpdate(state);
}

/** Cancels coloring of a partially matching keyword.
 * E.g., we start typing 'headi' at the beginning of the line,
 * it will be recognized as a partial match for 'heading' keyword
 * and will be colored so. But if we move the cursor away in 
 * OnSelChange(), we apply coloring to this word as a newly 
 * defined entity instead.*/
void CCshRichEditCtrl::CancelPartialMatch()
{
	CHARRANGE cr;
	GetSel(cr);
    auto pConf = GetConf();
    if (!pConf) return;
    Csh *csh = pConf->m_lang->pCsh.get();
    _ASSERT(csh);
    if (csh==nullptr) return;

    //First check if hint is on and we have left the hinted word
    if (InHintMode() && !csh->hintedStringPos.IsWithin(cr.cpMin))
        m_hintsPopup.Hide();

    if (!csh->was_partial) return;
	csh->was_partial = false;
    if (!pConf->m_bShowCsh) return;

    UpdateCSH(CSH); //ignore result on hinting
}


void CCshRichEditCtrl::JumpToLine(int line, int col) {
	long index = ConvertLineColToPos(line-1, col?col-1:0);
	SetSel(index, index);
	SetFocus();
}


long CCshRichEditCtrl::GetMenuCharPos() const {
    if (m_ctxMenuPos.x>=0)
        return CharFromPos(m_ctxMenuPos);
    long lStart, lEnd;
    GetSel(lStart, lEnd);
    return lStart;
}


std::pair<int, int> CCshRichEditCtrl::GetMenuLineCol() const {
    int line, col;
    ConvertPosToLineCol(GetMenuCharPos(), line, col);
    return {line, col};
}


void CCshRichEditCtrl::StartHintMode(bool setUptoCursor)
{
    auto pConf = GetConf();
    if (!pConf) return;
    Csh *csh = pConf->m_lang->pCsh.get();
    _ASSERT(csh);
    if (csh==nullptr) return;
    long start, end;
    GetSel(start, end);
    //cancel hints if
    //1. multiple characters selected
    //2. no hints collected
    //3. If this StartHintMode is the result of a previous autocompletion (initiated from this function below)
    //4. the cursor stands at the beginning of a real nonzero len hinted word 
    //   and the user did not press Ctrl+Space
    const CshPos &p = csh->hintedStringPos;
    if (start!=end || csh->Hints.size()==0 || 
        (p.first_pos != p.last_pos && !m_bUserRequested && p.first_pos == start)) {
        CancelHintMode();
        return;
    }
    
    CPoint pt;
    pt.x = PosFromChar(csh->hintedStringPos.first_pos).x;
    int line = LineFromChar(csh->hintedStringPos.first_pos);
    pt.y = PosFromChar(LineIndex(line+1)).y;
    if (pt.y==0) { //last line
        if (line==0) //empty file
            pt.y=20;
        else
            pt.y = 2*PosFromChar(LineIndex(line)).y-PosFromChar(LineIndex(line-1)).y;
    }
    ClientToScreen(&pt);
    CStringA text;
    long s,e;
    GetSel(s,e);
    if (setUptoCursor) {
        m_bTillCursorOnly = true;
        if (s<csh->hintedStringPos.last_pos)
            csh->hintedStringPos.last_pos = s;
    }
    if (csh->hintedStringPos.first_pos>=0) 
        GetTextRangeUtf8(csh->hintedStringPos.first_pos, csh->hintedStringPos.last_pos, text);

    DWORD hi, lo;
    ::SendMessage(m_hWnd, EM_GETZOOM, (WPARAM)&hi, (LPARAM)&lo);
    double zoom = lo ? double(hi)/double(lo) : 1.;
    //The call below kills hints if the user did not request them
    const auto res = m_hintsPopup.m_listBox.PreprocessHints(*csh, 
        (const char *)text, m_bUserRequested, InHintMode(), zoom);
    if (res == CHintListBox::HINTS_REPLACE) 
        //we need to replace - there is only one selectable hint
        for (const auto &h : csh->Hints)
            if (h.selectable) {
                ReplaceHintedString(&h);
                return;
            }
    //Show the window (or keep it shown & update the list)
    m_hintsPopup.Show(text, pt.x, pt.y);
    SetFocus();
}

void CCshRichEditCtrl::CancelHintMode()
{
    m_hintsPopup.Hide();
    m_bUserRequested = false;
    m_bTillCursorOnly = false; 
}

/**Replace the part of the text pointed by csh->hintedStringPos to substitute.*/
void CCshRichEditCtrl::ReplaceHintedString(const CshHint *hint)
{
    auto pConf = GetConf();
    if (!pConf) return;
    Csh *csh = pConf->m_lang->pCsh.get();
    _ASSERT(csh);
    if (csh==nullptr) return;
    const auto state = DisableWindowUpdate();
    SetSel(csh->hintedStringPos.first_pos, csh->hintedStringPos.last_pos);
    bool step_back;
    if (!hint) {
        ReplaceSelUtf8("");
        step_back = false;
    } else {
        std::string rep = hint->GetReplacementString();
        ReplaceSelUtf8(rep.c_str());
        step_back = rep.length() && (rep.back()==')' || rep.back()=='\"');
    } 
    //selection ends up at the end of the inserted text
    if (step_back) {
        long s, e;
        GetSel(s, e);
        SetSel(e-1, e-1);
    }
    m_bUserRequested = false; //This is no longer user requested
    UpdateCSH(CSH);  //also generates the new hints
    RestoreWindowUpdate(state);
    if ((hint && hint->keep) || step_back)
        StartHintMode(false); //refresh the hint popup content 
    else
        CancelHintMode();
}


/** Get the current redraw and doc update status. */
CCshRichEditCtrl::WindowUpdateStatus CCshRichEditCtrl::GetWindowUpdate()
{
    WindowUpdateStatus ret;
    ret.redraw_enabled = m_bRedrawState;
    ret.doc_updates_enabled = m_parent->m_bSuspendNotifications;
    return ret;
}

/** Disables both redraw and doc updates. */
CCshRichEditCtrl::WindowUpdateStatus CCshRichEditCtrl::DisableWindowUpdate()
{
    WindowUpdateStatus ret;
    ret.redraw_enabled = m_bRedrawState;
    ret.doc_updates_enabled = m_parent->m_bSuspendNotifications;
    m_bRedrawState = false;
    m_parent->m_bSuspendNotifications = true;
    if (ret.redraw_enabled)
        SetRedraw(false);
    return ret;
}

/** Restores a stored redraw and doc update status.
* If redraw gets re-enabled, we Invalidate() and UpdateWindow().*/
void CCshRichEditCtrl::RestoreWindowUpdate(const WindowUpdateStatus &s)
{
    m_parent->m_bSuspendNotifications = s.doc_updates_enabled;
    if (s.redraw_enabled!= m_bRedrawState) {
        m_bRedrawState = s.redraw_enabled;
        SetRedraw(m_bRedrawState);
        if (m_bRedrawState) {
            Invalidate();
            UpdateWindow();
        }
    }
}


std::pair<std::string_view, size_t>
CCshRichEditCtrl::GetEntityNameAtMenu() const {
    Csh *csh = GetConf()->m_lang->pCsh.get();
    if (!csh) return {};
    return csh->EntityNameUnder(GetMenuCharPos()+1); //+1 is to convert to CshPos units
}

void CCshRichEditCtrl::OnRenameEntity() {
    auto [src_entity_name, pos_within_entity] = GetEntityNameAtMenu();
    Csh *csh = GetConf()->m_lang->pCsh.get();
    if (!csh) return;
    if (src_entity_name.empty() || !csh) {
        MessageBeep(MB_ICONASTERISK);
        return;
    }
    CPoint pos = PosFromChar(GetMenuCharPos());
    ClientToScreen(&pos);
    pos.y -= CharHeight()/2;
    CRect rect(pos, CSize(1, CharHeight()*2));
    CRenameEntityDlg renameDlg(MscCshAppearanceList[csh->params->color_scheme][COLOR_ENTITYNAME], &rect);
    renameDlg.m_EntityName = AsUnicode(csh->AskReplace(src_entity_name, pos_within_entity));
    if (renameDlg.DoModal() != IDOK)
        return;
    const string replace_to = ConvertFromUTF16_to_UTF8(std::wstring_view(renameDlg.m_EntityName.GetString(), renameDlg.m_EntityName.GetLength()));
    long lStart, lEnd;
    GetSel(lStart, lEnd);
    const std::string text = csh->ReplaceEntityName(src_entity_name, pos_within_entity, replace_to, lStart, lEnd);
    const auto state = DisableWindowUpdate();
    SetSel(0, -1); //select all
    ReplaceSelUtf8(text.c_str());
    SetSel(lStart, lEnd); //restore selection
    UpdateCSH(FORCE_CSH);  
    RestoreWindowUpdate(state);
}


/////////////////////////////////////////////////////////////////////////////
//      CPaneSplitter

BEGIN_MESSAGE_MAP(CPaneSplitter, CSplitterWndEx)
    ON_WM_MOUSEWHEEL()
    ON_WM_NCHITTEST()
    ON_NOTIFY_RANGE(EN_SELCHANGE, AFX_IDW_PANE_FIRST, AFX_IDW_PANE_LAST, OnSelChange)
END_MESSAGE_MAP()

CPaneSplitter::CPaneSplitter(CEditorBar * p) : m_parent(p) 
{
    m_width = 40;
    auto pConfig = GetConf();
    if (pConfig)
        m_width = unsigned(m_width * pConfig->GetCurrentDPIScalingFactor());
}

/**Set focus to editor*/
void CPaneSplitter::OnSetFocus(CWnd* pOldWnd)
{
    CSplitterWndEx::OnSetFocus(pOldWnd);
    m_parent->m_ctrlEditor.SetFocus();
}


void CPaneSplitter::OnSelChange(UINT id, NMHDR * pNotifyStruct, LRESULT * result)
{
    m_parent->SelChanged();
}


BOOL CPaneSplitter::DoMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
    CPoint pt2 = pt;
    CRect view;
    GetClientRect(&view);
    ScreenToClient(&pt2);
    //Process message only if within our view
    if (!view.PtInRect(pt2)) return FALSE;
    m_parent->m_ctrlEditor.CancelHintMode();
    if (nFlags & MK_CONTROL) {
        WPARAM w;
        LPARAM l;
        if (m_parent->m_ctrlEditor.SendMessage(EM_GETZOOM, WPARAM(&w), LPARAM(&l))) {
            const double old_zoom = l ? (double)w/l : 1.;
            //zoom 10% per each WHEEL_DELTA
            const double new_zoom = old_zoom * (1 + double(zDelta)/WHEEL_DELTA*0.1); //1-+10%
            w = WPARAM(std::min(std::max(64*new_zoom, 10.), 700.));
            l = 64;
            m_parent->m_ctrlEditor.SendMessage(EM_SETZOOM, w, l);
            m_parent->m_ctrlLinenum.SendMessage(EM_SETZOOM, w, l);
            m_parent->UpdateNumbers(true, false);
            m_parent->m_zoom = double(w)/l;
        }
    } else {
        int lines = -zDelta*4/WHEEL_DELTA;
        m_parent->m_ctrlEditor.LineScroll(lines);
        m_parent->m_ctrlLinenum.LineScroll(lines);
        m_parent->UpdateNumbers(true, false);
    }
    return TRUE;
}

void CPaneSplitter::UpdateLineWindowWidth(unsigned numChar)
{
    //as many chars as needed, plus 1/2 char for headspace, times DPI
    CDC *pDC = m_parent->m_ctrlLinenum.GetDC();
    auto old = pDC->SelectObject(*m_parent->m_Font);
    const double unzoomed_linenum_width = pDC->GetTextExtent(L"0", 1).cx * (numChar+.5) *
                                          GetConf()->GetCurrentDPIScalingFactor();
    pDC->SelectObject(old);
    DWORD w, l;
    if (m_parent->m_ctrlEditor.SendMessage(EM_GETZOOM, WPARAM(&w), LPARAM(&l))) {
        const double p = l ? (double)w/l : 1.;
        auto pConf = GetConf();
        const unsigned width = pConf && pConf->m_bShowLineNums ? unsigned(unzoomed_linenum_width*p) : 0;
        if (width!=m_width) {
            SetColumnInfo(0, width, 10);
            RecalcLayout();
            m_width = width;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////
// CEditorBar

/** The new message we use for the find and replace dialog. */
static UINT nFindReplaceDialogMessage = ::RegisterWindowMessage(FINDMSGSTRING);

BEGIN_MESSAGE_MAP(CEditorBar, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
    ON_WM_TIMER()
	ON_REGISTERED_MESSAGE(nFindReplaceDialogMessage, OnFindReplaceMessage)
END_MESSAGE_MAP()

CEditorBar::CEditorBar() : m_wndSplitter(this), m_ctrlEditor(this)
{
    m_lastline = {1,1};
    m_linesNumbered = unsigned(-1);
    m_lInitialSearchPos = 0;
    m_bFirstSearch = true;
	m_bSuspendNotifications = FALSE;
	m_bLastMatchCase = GetRegistryInt(REG_KEY_FINDMATCHCASE, 1);
	m_bLastMatchWholeWord = false;
	m_ptLastFindPos.x = GetRegistryInt(REG_KEY_FINDWINPOS_X, 0);
	m_ptLastFindPos.y = GetRegistryInt(REG_KEY_FINDWINPOS_Y, 0);
	CDC dc;
	dc.CreateCompatibleDC(nullptr);
	CRect r;
	dc.GetClipBox(&r);
	r.bottom -= 20;
	r.right -=20;
	if (!r.PtInRect(m_ptLastFindPos)) m_ptLastFindPos.x = m_ptLastFindPos.y = 0;
    m_totalLenAtPreviousSelChange = 0;
    m_FontPixelHeight = 10;
    m_zoom = 1;
}

CEditorBar::~CEditorBar()
{
#ifndef SHARED_HANDLERS
    if (m_ptLastFindPos != CPoint(0, 0)) {
        theApp.WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_FINDWINPOS_X, m_ptLastFindPos.x);
        theApp.WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_FINDWINPOS_Y, m_ptLastFindPos.y);
        theApp.WriteProfileInt(REG_SECTION_SETTINGS, REG_KEY_FINDMATCHCASE, m_bLastMatchCase);
    }
#endif
}

/**Creates us, the font and the edit control.*/
int CEditorBar::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

    m_wndSplitter.CreateStatic(this, 1, 2, WS_CHILD | WS_VISIBLE | WS_VSCROLL);

    const CRect rectLines(0, 0, linewidth, lpCreateStruct->cy);
    const CRect rectEditor(linewidth, 0, lpCreateStruct->cx-linewidth, lpCreateStruct->cy);

    // Create line numbers
    const DWORD dwStyleLine = WS_CHILD | WS_VISIBLE | WS_DISABLED |  ES_MULTILINE | ES_READONLY | ES_RIGHT;
    if (!m_ctrlLinenum.Create(dwStyleLine, rectLines, &m_wndSplitter, m_wndSplitter.IdFromRowCol(0,0))) {
        TRACE0("Failed to create lines editbox\n");
        return -1;      // fail to create
    }
    m_ctrlLinenum.SetTextMode(TM_SINGLECODEPAGE | TM_SINGLELEVELUNDO | TM_RICHTEXT);
    
	// Create text editor
    const DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_HSCROLL | ES_AUTOVSCROLL | //WS_VSCROLL | 
						  ES_MULTILINE | ES_WANTRETURN | ES_AUTOHSCROLL | ES_NOHIDESEL |
                          ES_SELECTIONBAR;

    //if (!m_wndSplitter.AddWindow(0, 1, &m_ctrlEditor, _T("RichEdit20A"), dwStyle, 0, CSize(0, 0))) {
	if (!m_ctrlEditor.Create(dwStyle, rectEditor, &m_wndSplitter, m_wndSplitter.IdFromRowCol(0, 1))) {//this, IDC_INTERNAL_EDITOR)) {
		TRACE0("Failed to create text editor control\n");
		return -1;      // fail to create
	}
    m_wndSplitter.SetColumnInfo(0, 40, 25); //Just a default, also set in CPaneSplitter::CPaneSplitter to CPaneSplitter::m_width

	m_ctrlEditor.SetTextMode(TM_MULTICODEPAGE| TM_MULTILEVELUNDO | TM_RICHTEXT);
    SelectNewFont(
        GetRegistryString(REG_KEY_INTERNAL_EDITOR_FONT, _T("Courier New")),
        GetRegistryInt(REG_KEY_INTERNAL_FONT_SIZE10, 160),
        GetRegistryInt(REG_KEY_INTERNAL_FONT_CHARSET, DEFAULT_CHARSET),
        GetRegistryInt(REG_KEY_INTERNAL_FONT_FIXEDONLY, TRUE));
    m_ctrlEditor.SetFocus();
	m_ctrlEditor.SetEventMask(m_ctrlEditor.GetEventMask() | ENM_CHANGE | ENM_SELCHANGE | ENM_SCROLL );

#ifndef SHARED_HANDLERS
    CMscGenDoc *pDoc = GetMscGenDocument();
	if (pDoc != nullptr) {
        if (pDoc->m_ExternalEditor.IsRunning())
            SetReadOnly();
        const auto state = theApp.m_pWndEditor->m_ctrlEditor.DisableWindowUpdate();
        theApp.m_pWndEditor->m_ctrlEditor.SetSel(0,-1);
        theApp.m_pWndEditor->m_ctrlEditor.ReplaceSelUtf8(pDoc->m_itrEditing->GetText());
        theApp.m_pWndEditor->m_ctrlEditor.RestoreWindowUpdate(state);
    }
#endif
	return 0;
}



/** Text to append after the title of the internal editor, when read-only.*/
#define READONLY_STRING " (Read-only)"
/** Sets the editor to read only mode. */
void CEditorBar::SetReadOnly(bool readonly)
{
	if (!m_ctrlEditor.SetReadOnly(readonly)) return;
	CString text;
	GetWindowText(text);
	if (text.Right(12).CompareNoCase(_T(READONLY_STRING))==0) {
		if (!readonly)
			SetWindowText(text.Left(text.GetLength()-12));
	} else if (readonly)
		SetWindowText(text + READONLY_STRING);
}


/**Updates the editor control on size change.*/
void CEditorBar::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
    CRect rect;
    GetClientRect(rect);
    m_wndSplitter.SetWindowPos(NULL, rect.left+1, rect.top+1, rect.Width()-2, rect.Height()-2,
                               SWP_NOZORDER | SWP_NOACTIVATE);
    m_wndSplitter.Invalidate();
    m_wndSplitter.UpdateWindow();
}

/**Draw border around editor.*/
void CEditorBar::OnPaint()
{
	CPaintDC dc(this); // device context for painting

    CRect rect;
    m_wndSplitter.GetWindowRect(rect);
    ScreenToClient(rect);
	rect.InflateRect(1, 1);
	dc.Draw3dRect(rect, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

/**Set focus to editor*/
void CEditorBar::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
	m_ctrlEditor.SetFocus();
}


/** Reacts to EN_CHANGE events from the editor.
 * If notifications are not suspended via m_bSuspendNotifications,
 * we update coloring and enter hint mode if settings require.*/
BOOL CPaneSplitter::OnCommand(WPARAM wParam, LPARAM lParam)
{
	HWND hWndCtrl = (HWND)lParam;
	int nCode = HIWORD(wParam);
    if (hWndCtrl == (HWND)m_parent->m_ctrlEditor) {
        if (nCode != EN_CHANGE) return CSplitterWndEx::OnCommand(wParam, lParam);
        if (m_parent->m_bSuspendNotifications) return TRUE;

        //re-set font
        //m_ctrlEditor.SetFont(&m_Font);

        const bool hint = m_parent->m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::CSH);
        //Update hints if we are in hint mode
        if (m_parent->m_ctrlEditor.InHintMode()) {
            //Kill the user requested nature
            m_parent->m_ctrlEditor.CancelUserSelected();
            if (!hint) //We have switched to a different string to hint
                m_parent->m_ctrlEditor.CancelUserSelected();
            m_parent->m_ctrlEditor.StartHintMode(false); //false == do not change m_bTillCursorOnly
        } else {
            long s, e;
            m_parent->m_ctrlEditor.GetSel(s, e);
            //Enter hint mode only if we typed a character (outside hint mode
            //this is what UpdateCSH returns)
            if (hint && s==e) {
                bool till_cursor_only = false;
                if (s<=1)
                    till_cursor_only = true;
                else {
                    CStringA str;
                    m_parent->m_ctrlEditor.GetTextRangeUtf8(s-2, s-1, str);
                    //Non-ANSI characters make isalnum to throw an assert
                    //Non-ANSI characters are assumed to be alphanumeric
                    if (str[0]>=0 && str[0]!='_' && !isalnum(static_cast<unsigned char>(str[0])))
                        till_cursor_only = true;
                }
                m_parent->m_ctrlEditor.StartHintMode(till_cursor_only);
            }
        }
        m_parent->UpdateNumbers(false, true); //just check if selection has changed
        return TRUE;
    }
    //not to any of our editors
    return CSplitterWndEx::OnCommand(wParam, lParam);
}

void CPaneSplitter::OnDrawSplitter(CDC * pDC, ESplitType nType, const CRect & rect)
{
    if (pDC)
        pDC->FillSolidRect(rect, RGB(255, 255, 255));
    else
        CSplitterWndEx::OnDrawSplitter(pDC, nType, rect);
}

/** Called if the selection changes.
 * If this is not a text change, we cancel partial csh matches.*/
void CEditorBar::SelChanged()
{
    if (m_bSuspendNotifications) {
        m_totalLenAtPreviousSelChange = m_ctrlEditor.GetTextLength();
        return;
    }
    UpdateLineNumberHighlight();
    //if we have just moved, not inserted or deleted cancel partial match and hints
    //(otherwise we will get an EN_CHANGE right after this and we redo
    //csh and hints all the same. This is to prevent hint window flicker.)
    if (m_totalLenAtPreviousSelChange == m_ctrlEditor.GetTextLength()) {
        m_ctrlEditor.CancelPartialMatch();
        CMscGenDoc *pDoc = GetMscGenDocument();
        if (pDoc)
            pDoc->OnInternalEditorSelChange();
    } else 
        m_totalLenAtPreviousSelChange = m_ctrlEditor.GetTextLength();

    //Adjust scrolling for the line numbers if needed
    int first1 = m_ctrlLinenum.GetFirstVisibleLine();
    int first2 = m_ctrlEditor.GetFirstVisibleLine();
    if (first1 != first2)
        m_ctrlLinenum.LineScroll(first2-first1);
}

void CEditorBar::UpdateLineNumberHighlight()
{
    CHARRANGE cr;
    m_ctrlEditor.GetSel(cr);
    std::pair<long, long> line(m_ctrlEditor.LineFromChar(cr.cpMin), m_ctrlEditor.LineFromChar(cr.cpMax));
    if (m_lastline != line) {
        CHARFORMAT cf;
        cf.cbSize = sizeof(cf);
        cf.dwMask = CFM_BOLD | CFM_COLOR;
        cf.dwEffects = 0;
        cf.crTextColor = RGB(128,128,128); //gray
        //cf.dwMask |= CFM_CHARSET | CFM_FACE;
        //cf.bCharSet = DEFAULT_CHARSET;
        //cf.bPitchAndFamily = FIXED_PITCH | FF_MODERN;
        //cf.szFaceName[0] = 0;
        m_ctrlLinenum.SetRedraw(false);
        m_ctrlLinenum.SetSel(0, -1); //all
        m_ctrlLinenum.SetSelectionCharFormat(cf);
        m_ctrlLinenum.SetSel(m_ctrlLinenum.LineIndex(line.first), m_ctrlLinenum.LineIndex(line.second+1));
        cf.dwEffects = CFM_BOLD;
        cf.crTextColor = RGB(0, 0, 0); //real black
        m_ctrlLinenum.SetSelectionCharFormat(cf);
        m_lastline = line;
        m_ctrlLinenum.SetRedraw(true);
        m_ctrlLinenum.Invalidate();
        m_ctrlLinenum.UpdateWindow();
        auto pConf = GetConf();
        if (pConf && pConf->m_bCurrentLineHighLight) {
            m_ctrlEditor.Invalidate();
            m_ctrlEditor.UpdateWindow();
        }
    }
}


//Find replace related

/**Copy the selected text to the search field.*/
bool CEditorBar::UpdateLastFindStringFromSelection()
{
	//See if there is a selection and if it is within a single line. If so, copy that to the search box
	long lStart, lEnd;
	m_ctrlEditor.GetSel(lStart, lEnd);
	if (lStart == lEnd) return false;
	if (m_ctrlEditor.LineFromChar(lStart) != m_ctrlEditor.LineFromChar(lEnd)) return false;
	m_sLastFindString = m_ctrlEditor.GetSelText();
	return true;
}

/** Updates the size of the line number pane and the numbers in it.
 * @param [in] char_width_change Check if the font or the zoom factor has changed.
 * @param [in] selection_change Update the bold mark for selection change.*/
void CEditorBar::UpdateNumbers(bool char_width_change, bool selection_change)
{
    unsigned lines = m_ctrlEditor.GetLineCount();
    if (m_linesNumbered != lines) {

        CString text;
        for (int i = 0; i < lines; i++) {
            CString l;
            l.Format(_T("%d\n"), i+1);
            text += l;
        }
        //At setting the text zoom is screwed up.
        DWORD hi, lo;
        m_ctrlLinenum.SendMessage(EM_GETZOOM, (WPARAM)&hi, (LPARAM)&lo);
        m_ctrlLinenum.SetRedraw(false);
        m_ctrlLinenum.SetWindowText(text);
        m_ctrlLinenum.SendMessage(EM_SETZOOM, (WPARAM)hi, (LPARAM)lo);
        //m_ctrlLinenum.SetRedraw(true); We keep redraw false and dont update the window - UpdateLineNumberHighLight() later will.
        m_linesNumbered = lines;
        char_width_change = true;
        selection_change = true;
    }
    if (char_width_change) {
    //Change width of linenum window
        CString l;
        l.Format(_T("%d"), lines);
        m_wndSplitter.UpdateLineWindowWidth(l.GetLength());
    }
    if (selection_change) {
        //Update the bold marking
        m_lastline = {-1,-1};
        UpdateLineNumberHighlight();
    }
}


/** Fill in a LOGFONT structure based on name and size.
* size is the point size, the user sees, times 10. We take screen DPI into account.*/
void CEditorBar::FillLogFont(LOGFONT &lf, const CString &font, unsigned size10, BYTE CharSet,
                             bool FixedPitchOnly)
{
    auto pConf = GetConf();
    if (!pConf) return;
    const int pixel_height = int(size10*pConf->GetCurrentDPIScalingFactor()*96./72./10.);
    lf.lfHeight = -pixel_height; //negative value: character height, positive: cell height
    lf.lfWidth = 0;    //just keep aspect ratio
    lf.lfEscapement = 0; //not italic not slanted
    lf.lfOrientation = 0;//not italic not slanted
    lf.lfWeight = FW_NORMAL; //not bold
    lf.lfItalic = FALSE;
    lf.lfUnderline = FALSE;
    lf.lfStrikeOut = FALSE;
    lf.lfCharSet = DEFAULT_CHARSET;
    lf.lfOutPrecision = OUT_TT_PRECIS; //use the TT font if several of the same name exists
    lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
    lf.lfQuality = ANTIALIASED_QUALITY; //antialiased only if user selected smooth screen fonts in control panel
    lf.lfPitchAndFamily = FixedPitchOnly ? FIXED_PITCH | FF_MODERN : DEFAULT_PITCH; //this is fixed pitch
    StrCpyN(lf.lfFaceName, font, 31);
}

/** Select new font in the internal editor.*/
void CEditorBar::SelectNewFont(const CString &font, unsigned size, BYTE CharSet, bool FixedPitchOnly)
{
    //m_Font.CreateStockObject(DEFAULT_GUI_FONT);
    LOGFONT lf;
    FillLogFont(lf, font, size, CharSet, FixedPitchOnly);
    auto f = std::make_unique<CFont>();
    f->CreateFontIndirect(&lf);
    //read height back
    f->GetLogFont(&lf);
    m_FontPixelHeight = lf.lfHeight;

    CHARRANGE cr;
    m_ctrlEditor.GetSel(cr);
    m_ctrlEditor.CancelHintMode();
    const auto state = m_ctrlEditor.DisableWindowUpdate();
    m_ctrlEditor.SetSel(0, -1); //all
    m_ctrlEditor.SetFont(f.get());
    m_ctrlLinenum.SetFont(f.get());
    if (IsInternalEditorVisible())
        m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::FORCE_CSH);
    m_ctrlEditor.SetSel(cr);
    m_ctrlEditor.RestoreWindowUpdate(state);
    std::swap(m_Font, f);
    UpdateNumbers(true, false);
    m_ctrlEditor.m_hintsPopup.m_listBox.SelectNewFont(font, size);

}



/**Called when the user initates a search or replace session.*/
void CEditorBar::OnEditFindReplace(bool findOnly)
{
	//If window is up, store location and destroy
	if (m_pFindReplaceDialog) {
		CRect r;
		m_pFindReplaceDialog->GetWindowRect(r);
		m_ptLastFindPos = r.TopLeft();
	}
	UpdateLastFindStringFromSelection();
	
	m_pFindReplaceDialog = std::make_unique<CFindReplaceDialog>();
	const DWORD dwFlags = FR_HIDEUPDOWN | (m_bLastMatchCase?FR_MATCHCASE:0) | (m_bLastMatchWholeWord?FR_WHOLEWORD:0);
	m_pFindReplaceDialog->Create(findOnly, m_sLastFindString, m_sLastReplaceString, dwFlags, this );
	m_pFindReplaceDialog->SetActiveWindow( );
	m_pFindReplaceDialog->ShowWindow( TRUE );
	if (m_ptLastFindPos != CPoint(0,0))
		m_pFindReplaceDialog->SetWindowPos(nullptr, m_ptLastFindPos.x, m_ptLastFindPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
}

/**Called when the user repeats a search.*/
void CEditorBar::OnEditRepeat()
{
	//If the find window is not there use the selection as search string
	if (!m_pFindReplaceDialog || !::IsWindow(*m_pFindReplaceDialog) || !m_pFindReplaceDialog->IsWindowVisible())
			UpdateLastFindStringFromSelection();
	//If there is nothing to search or we do not find it, beep
	if (m_sLastFindString.IsEmpty() || 
		!FindText(m_sLastFindString, m_bLastMatchCase, m_bLastMatchWholeWord))
		TextNotFound(m_sLastFindString);
}

/**This is called when a new text search is completed.*/
LRESULT CEditorBar::OnFindReplaceMessage(WPARAM /*wParam*/, LPARAM lParam)
{

	CFindReplaceDialog* pFindReplace = CFindReplaceDialog::GetNotifier(lParam);
    ASSERT(pFindReplace != nullptr);
    if (!pFindReplace) return 0;

	//Store values for next opening or OnEditRepeat
	m_sLastFindString = pFindReplace->GetFindString();
	m_sLastReplaceString = pFindReplace->GetReplaceString();
	m_bLastMatchCase = pFindReplace->MatchCase();
	m_bLastMatchWholeWord = pFindReplace->MatchWholeWord();
	CRect r;
	pFindReplace->GetWindowRect(r);
	m_ptLastFindPos = r.TopLeft();

	if (pFindReplace->IsTerminating()) {
		m_pFindReplaceDialog.release(); //The dialog box will delete itself from the heap
		return 0;
    } 

	if (pFindReplace->ReplaceAll()) {
		CWaitCursor wait;
        m_ctrlEditor.CancelHintMode();
        const auto state = m_ctrlEditor.DisableWindowUpdate();
		if (!SameAsSelected(pFindReplace->GetFindString(), pFindReplace->MatchCase())) {
			if (!FindText(pFindReplace->GetFindString(), pFindReplace->MatchCase(), pFindReplace->MatchWholeWord())) {
                m_ctrlEditor.RestoreWindowUpdate(state);
				TextNotFound(pFindReplace->GetFindString());
				return 0;
			}
		}
        do {
			m_ctrlEditor.ReplaceSelUtf8(AsUTF8(pFindReplace->GetReplaceString()));
		} while (FindText(pFindReplace->GetFindString(), pFindReplace->MatchCase(), pFindReplace->MatchWholeWord()));
		m_ctrlEditor.HideSelection(FALSE, FALSE);
        m_ctrlEditor.UpdateCSH(CCshRichEditCtrl::CSH);
        m_ctrlEditor.RestoreWindowUpdate(state);
        return 0;
	}
	bool findnext = pFindReplace->FindNext();
	if (pFindReplace->ReplaceCurrent()) {
		long start, end;
		m_ctrlEditor.GetSel(start, end);
		//if nothing is selected, skip replace, just go to find next
		if (start!=end) 
			m_ctrlEditor.ReplaceSelUtf8(AsUTF8(pFindReplace->GetReplaceString()), FALSE);
		findnext = true;
	}
	//Find next for both FindNext and ReplaceCurrent
	if (findnext) {
		if (FindText(pFindReplace->GetFindString(), pFindReplace->MatchCase(), pFindReplace->MatchWholeWord())) {
			AdjustDialogPosition(pFindReplace);
			CRect rect;
			pFindReplace->GetWindowRect(rect);
			m_ptLastFindPos = rect.TopLeft();
		} else
			TextNotFound(pFindReplace->GetFindString());
	} 
	return 0;
}

/** Called by the system when the context menu needs to be displayed. */
BOOL CEditorBar::OnShowControlBarMenu(CPoint pt)
{
    CMscGenApp *pApp = dynamic_cast<CMscGenApp *>(AfxGetApp());
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(AfxGetMainWnd());
    ASSERT(pApp != nullptr && pMainWnd!=nullptr);

    m_ctrlEditor.m_ctxMenuPos = pt;
    //convert from screen coordinates to the client area of the richedit
    m_ctrlEditor.ScreenToClient(&m_ctrlEditor.m_ctxMenuPos); 
    
    HMENU hMenu = theApp.GetContextMenuManager()->GetMenuById(IDR_POPUP_INTERNAL_EDITOR);
    if (hMenu) {
        HMENU hmenuPopup = ::GetSubMenu(hMenu, 0);
        if (hmenuPopup) {
            UINT selected = theApp.GetContextMenuManager()->
                TrackPopupMenu(hmenuPopup, pt.x, pt.y, pMainWnd, TRUE);
            if (selected==0)
                return true;
            pMainWnd->SendMessage(WM_COMMAND, selected, 0);
            //invalidate menu pos, so that if user presses F4, we
            //highlight the element under the cursor
            m_ctrlEditor.m_ctxMenuPos.x = -1;
        }
    }
    return true;
}

void CEditorBar::OnPressCloseButton()
{
    //Kill hint mode first
    m_ctrlEditor.CancelHintMode();
    //actually close the internal editor pane
    CDockablePane::OnPressCloseButton();
    //show informative balloon for the user, how to turn it back on
    CMainFrame *pMainWnd = dynamic_cast<CMainFrame*>(AfxGetMainWnd());
    ASSERT(pMainWnd!=nullptr);
    pMainWnd->ShowInternalEditorCloseBalloon();
}

/**See if 'lpszCompare' is the same as the selected text.*/
BOOL CEditorBar::SameAsSelected(LPCTSTR lpszCompare, BOOL bCase)
{
	// check length first
	size_t nLen = lstrlen(lpszCompare);
	long lStartChar, lEndChar;
	m_ctrlEditor.GetSel(lStartChar, lEndChar);
	if (nLen != (size_t)(lEndChar - lStartChar))
		return FALSE;

	// length is the same, check contents
	CString strSelect = m_ctrlEditor.GetSelText();
	return (bCase && lstrcmp(lpszCompare, strSelect) == 0) ||
		(!bCase && lstrcmpi(lpszCompare, strSelect) == 0);
}

/**Moves the search dialog to avoid the highlighted text.*/
void CEditorBar::AdjustDialogPosition(CDialog* pDlg)
{
	ASSERT(pDlg != nullptr);
    if (pDlg==nullptr) return;
	long lStart, lEnd;
	m_ctrlEditor.GetSel(lStart, lEnd);
	CPoint point = m_ctrlEditor.GetCharPos(lStart);
	ClientToScreen(&point);
	CRect rectDlg;
	pDlg->GetWindowRect(&rectDlg);
	if (rectDlg.PtInRect(point))
	{
		if (point.y > rectDlg.Height())
			rectDlg.OffsetRect(0, point.y - rectDlg.bottom - 20);
		else
		{
			int nVertExt = GetSystemMetrics(SM_CYSCREEN);
			if (point.y + rectDlg.Height() < nVertExt)
				rectDlg.OffsetRect(0, 40 + point.y - rectDlg.top);
		}
		pDlg->MoveWindow(&rectDlg);
	}
}

/**Actually find a piece of text in the editor.*/
BOOL CEditorBar::FindText(LPCTSTR lpszFind, BOOL bCase, BOOL bWord, BOOL bNext /* = TRUE */)
{
	ASSERT_VALID(this);
	ASSERT(lpszFind != nullptr);
	FINDTEXTEX ft;

	m_ctrlEditor.GetSel(ft.chrg);
	if (m_bFirstSearch) {
		if (bNext)
			m_lInitialSearchPos = ft.chrg.cpMin;
		else
			m_lInitialSearchPos = ft.chrg.cpMax;
		m_bFirstSearch = FALSE;
	}

	// lpstrText should be const
	ft.lpstrText = (LPTSTR) lpszFind;

	if (ft.chrg.cpMin != ft.chrg.cpMax) {// i.e. there is a selection
		if (bNext)
			ft.chrg.cpMin++;
		else {
			// won't wraparound backwards
			if (ft.chrg.cpMin<0) ft.chrg.cpMin = 0;
		}
	}

	DWORD dwFlags = bCase ? FR_MATCHCASE : 0;
	dwFlags |= bWord ? FR_WHOLEWORD : 0;

	ft.chrg.cpMax = m_ctrlEditor.GetTextLength() + m_lInitialSearchPos;

	if (bNext) {
		if (m_lInitialSearchPos >= 0)
			ft.chrg.cpMax = m_ctrlEditor.GetTextLength();

		dwFlags |= FR_DOWN;
		ASSERT(ft.chrg.cpMax >= ft.chrg.cpMin);
	} else {
		if (m_lInitialSearchPos >= 0)
			ft.chrg.cpMax = 0;

		dwFlags &= ~FR_DOWN;
		ASSERT(ft.chrg.cpMax <= ft.chrg.cpMin);
	}

	// if we find the text return TRUE
	if (FindAndSelect(dwFlags, ft) != -1)
		return TRUE;
	// if the original starting point was not the beginning of the buffer
	// and we haven't already been here
	else if (m_lInitialSearchPos > 0) {
		if (bNext) {
			ft.chrg.cpMin = 0;
			ft.chrg.cpMax = m_lInitialSearchPos;
		} else {
			ft.chrg.cpMin = m_ctrlEditor.GetTextLength();
			ft.chrg.cpMax = m_lInitialSearchPos;
		}
		m_lInitialSearchPos = m_lInitialSearchPos - m_ctrlEditor.GetTextLength();
		return FindAndSelect(dwFlags, ft) != -1;
	}
	// not found
	else
		return FALSE;
}

/**Find a piece of text in the editor and select it.*/
long CEditorBar::FindAndSelect(DWORD dwFlags, FINDTEXTEX& ft)
{
	long index = m_ctrlEditor.FindText(dwFlags, &ft);
	if (index != -1) // i.e. we found something
		m_ctrlEditor.SetSel(ft.chrgText);
	return index;
}

/**What to do on a failed text search - emit a beep*/
void CEditorBar::TextNotFound(LPCTSTR /*lpszFind*/)
{
	ASSERT_VALID(this);
	m_bFirstSearch = TRUE;
	MessageBeep(MB_ICONHAND);
}


/**Selet all of the text in the editor*/
void CEditorBar::SelectAll()
{
    CHARRANGE cr;
    cr.cpMin = 0;
    cr.cpMax = m_ctrlEditor.GetTextLength();
	m_ctrlEditor.SetSel(cr);
}


