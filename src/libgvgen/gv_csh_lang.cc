#define COLOR_SYNTAX_HIGHLIGHT 1 /* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         gvcsh_parse
#define yylex           gvcsh_lex
#define yyerror         gvcsh_error
#define yydebug         gvcsh_debug
#define yynerrs         gvcsh_nerrs

/* First part of user prologue.  */
#line 15 "dot.yy"

/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT //so that subsequent definition in msc_csh_lang2.h causes no warning
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #define YYMSC_RESULT_TYPE GraphCsh
    #define RESULT csh
    #include "gvcsh.h"
    #include "gvstyle.h"  //for GraphEdgeType
    #define YYGET_EXTRA gvcsh_get_extra
    #define YYLTYPE_IS_DECLARED
    #define YYLTYPE CshPos
    #define CHAR_IF_CSH(A) char
#else
    #define C_S_H (0)
    #define YYMSC_RESULT_TYPE GraphChart
    #define RESULT chart
    #define YYGET_EXTRA gv_get_extra
    #define CHAR_IF_CSH(A) A
    #include "graphchart.h"
#endif

using namespace graph;


#line 121 "gv_csh_lang.cc"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "gv_csh_lang.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* TOK_EOF  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_TOK_STRICT = 3,                 /* TOK_STRICT  */
  YYSYMBOL_TOK_GRAPH = 4,                  /* TOK_GRAPH  */
  YYSYMBOL_TOK_DIGRAPH = 5,                /* TOK_DIGRAPH  */
  YYSYMBOL_TOK_SUBGRAPH = 6,               /* TOK_SUBGRAPH  */
  YYSYMBOL_TOK_NODE = 7,                   /* TOK_NODE  */
  YYSYMBOL_TOK_EDGE = 8,                   /* TOK_EDGE  */
  YYSYMBOL_TOK_EDGEOP = 9,                 /* TOK_EDGEOP  */
  YYSYMBOL_TOK_EQUAL = 10,                 /* TOK_EQUAL  */
  YYSYMBOL_TOK_CLUSTER = 11,               /* TOK_CLUSTER  */
  YYSYMBOL_TOK_COLON_STRING = 12,          /* TOK_COLON_STRING  */
  YYSYMBOL_TOK_COLON_QUOTED_STRING = 13,   /* TOK_COLON_QUOTED_STRING  */
  YYSYMBOL_TOK_STRING = 14,                /* TOK_STRING  */
  YYSYMBOL_TOK_QSTRING = 15,               /* TOK_QSTRING  */
  YYSYMBOL_TOK_TILDE = 16,                 /* TOK_TILDE  */
  YYSYMBOL_TOK_SEMICOLON = 17,             /* TOK_SEMICOLON  */
  YYSYMBOL_TOK_PARAM_NAME = 18,            /* TOK_PARAM_NAME  */
  YYSYMBOL_TOK_OPARENTHESIS = 19,          /* TOK_OPARENTHESIS  */
  YYSYMBOL_TOK_CPARENTHESIS = 20,          /* TOK_CPARENTHESIS  */
  YYSYMBOL_TOK_COMMAND_DEFPROC = 21,       /* TOK_COMMAND_DEFPROC  */
  YYSYMBOL_TOK_COMMAND_REPLAY = 22,        /* TOK_COMMAND_REPLAY  */
  YYSYMBOL_TOK_COMMAND_SET = 23,           /* TOK_COMMAND_SET  */
  YYSYMBOL_TOK_BYE = 24,                   /* TOK_BYE  */
  YYSYMBOL_TOK_IF = 25,                    /* TOK_IF  */
  YYSYMBOL_TOK_THEN = 26,                  /* TOK_THEN  */
  YYSYMBOL_TOK_ELSE = 27,                  /* TOK_ELSE  */
  YYSYMBOL_TOK_COMMAND_INCLUDE = 28,       /* TOK_COMMAND_INCLUDE  */
  YYSYMBOL_TOK_OCBRACKET = 29,             /* TOK_OCBRACKET  */
  YYSYMBOL_TOK_CCBRACKET = 30,             /* TOK_CCBRACKET  */
  YYSYMBOL_TOK_COMMA = 31,                 /* TOK_COMMA  */
  YYSYMBOL_TOK_DEFDESIGN = 32,             /* TOK_DEFDESIGN  */
  YYSYMBOL_TOK_USEDESIGN = 33,             /* TOK_USEDESIGN  */
  YYSYMBOL_TOK_DEFSTYLE = 34,              /* TOK_DEFSTYLE  */
  YYSYMBOL_TOK_UNRECOGNIZED_CHAR = 35,     /* TOK_UNRECOGNIZED_CHAR  */
  YYSYMBOL_36_ = 36,                       /* ':'  */
  YYSYMBOL_37_ = 37,                       /* '['  */
  YYSYMBOL_38_ = 38,                       /* ']'  */
  YYSYMBOL_YYACCEPT = 39,                  /* $accept  */
  YYSYMBOL_main = 40,                      /* main  */
  YYSYMBOL_top_level_command = 41,         /* top_level_command  */
  YYSYMBOL_top_level_command_with_semi = 42, /* top_level_command_with_semi  */
  YYSYMBOL_list = 43,                      /* list  */
  YYSYMBOL_option = 44,                    /* option  */
  YYSYMBOL_usedesign = 45,                 /* usedesign  */
  YYSYMBOL_designdef = 46,                 /* designdef  */
  YYSYMBOL_designdefhdr = 47,              /* designdefhdr  */
  YYSYMBOL_stylename = 48,                 /* stylename  */
  YYSYMBOL_stylenamelist = 49,             /* stylenamelist  */
  YYSYMBOL_styledef = 50,                  /* styledef  */
  YYSYMBOL_defproc = 51,                   /* defproc  */
  YYSYMBOL_defprochelp1 = 52,              /* defprochelp1  */
  YYSYMBOL_defprochelp2 = 53,              /* defprochelp2  */
  YYSYMBOL_defprochelp3 = 54,              /* defprochelp3  */
  YYSYMBOL_defprochelp4 = 55,              /* defprochelp4  */
  YYSYMBOL_scope_open_proc_body = 56,      /* scope_open_proc_body  */
  YYSYMBOL_scope_close_proc_body = 57,     /* scope_close_proc_body  */
  YYSYMBOL_proc_def_arglist_tested = 58,   /* proc_def_arglist_tested  */
  YYSYMBOL_proc_def_arglist = 59,          /* proc_def_arglist  */
  YYSYMBOL_proc_def_param_list = 60,       /* proc_def_param_list  */
  YYSYMBOL_proc_def_param = 61,            /* proc_def_param  */
  YYSYMBOL_procedure_body = 62,            /* procedure_body  */
  YYSYMBOL_set = 63,                       /* set  */
  YYSYMBOL_opt_semi = 64,                  /* opt_semi  */
  YYSYMBOL_attrinst_opt_semi = 65,         /* attrinst_opt_semi  */
  YYSYMBOL_attrinstrlist = 66,             /* attrinstrlist  */
  YYSYMBOL_graph = 67,                     /* graph  */
  YYSYMBOL_body = 68,                      /* body  */
  YYSYMBOL_empty_open_scope = 69,          /* empty_open_scope  */
  YYSYMBOL_open_scope = 70,                /* open_scope  */
  YYSYMBOL_close_scope = 71,               /* close_scope  */
  YYSYMBOL_hdr = 72,                       /* hdr  */
  YYSYMBOL_graphtype = 73,                 /* graphtype  */
  YYSYMBOL_instrlist = 74,                 /* instrlist  */
  YYSYMBOL_instruction = 75,               /* instruction  */
  YYSYMBOL_completed_proc_invocation = 76, /* completed_proc_invocation  */
  YYSYMBOL_proc_invocation = 77,           /* proc_invocation  */
  YYSYMBOL_proc_param_list = 78,           /* proc_param_list  */
  YYSYMBOL_proc_invoc_param_list = 79,     /* proc_invoc_param_list  */
  YYSYMBOL_proc_invoc_param = 80,          /* proc_invoc_param  */
  YYSYMBOL_include = 81,                   /* include  */
  YYSYMBOL_instruction_no_semi = 82,       /* instruction_no_semi  */
  YYSYMBOL_chain_with_attrs = 83,          /* chain_with_attrs  */
  YYSYMBOL_edgeop = 84,                    /* edgeop  */
  YYSYMBOL_chain = 85,                     /* chain  */
  YYSYMBOL_nodes = 86,                     /* nodes  */
  YYSYMBOL_nodelist = 87,                  /* nodelist  */
  YYSYMBOL_node = 88,                      /* node  */
  YYSYMBOL_node0 = 89,                     /* node0  */
  YYSYMBOL_node1 = 90,                     /* node1  */
  YYSYMBOL_node2 = 91,                     /* node2  */
  YYSYMBOL_attrinst = 92,                  /* attrinst  */
  YYSYMBOL_attrtype = 93,                  /* attrtype  */
  YYSYMBOL_full_attrlist_with_label = 94,  /* full_attrlist_with_label  */
  YYSYMBOL_full_attrlist = 95,             /* full_attrlist  */
  YYSYMBOL_attrlist = 96,                  /* attrlist  */
  YYSYMBOL_colon_string = 97,              /* colon_string  */
  YYSYMBOL_attrdefs = 98,                  /* attrdefs  */
  YYSYMBOL_attrdef = 99,                   /* attrdef  */
  YYSYMBOL_attritem = 100,                 /* attritem  */
  YYSYMBOL_comp = 101,                     /* comp  */
  YYSYMBOL_condition = 102,                /* condition  */
  YYSYMBOL_ifthen_condition = 103,         /* ifthen_condition  */
  YYSYMBOL_else = 104,                     /* else  */
  YYSYMBOL_ifthen = 105,                   /* ifthen  */
  YYSYMBOL_graphattrdef = 106,             /* graphattrdef  */
  YYSYMBOL_subgraph_attrs = 107,           /* subgraph_attrs  */
  YYSYMBOL_subgraph = 108,                 /* subgraph  */
  YYSYMBOL_subghdr = 109,                  /* subghdr  */
  YYSYMBOL_optseparator = 110,             /* optseparator  */
  YYSYMBOL_string_single = 111,            /* string_single  */
  YYSYMBOL_reserved_word = 112,            /* reserved_word  */
  YYSYMBOL_string_single_or_reserved_word = 113, /* string_single_or_reserved_word  */
  YYSYMBOL_tok_param_name_as_multi = 114,  /* tok_param_name_as_multi  */
  YYSYMBOL_string_or_reserved_word_or_param = 115, /* string_or_reserved_word_or_param  */
  YYSYMBOL_string_or_param = 116,          /* string_or_param  */
  YYSYMBOL_multi_string_continuation = 117, /* multi_string_continuation  */
  YYSYMBOL_string_or_reserved_word = 118,  /* string_or_reserved_word  */
  YYSYMBOL_string = 119                    /* string  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;


/* Second part of user prologue.  */
#line 145 "dot.yy"

#include "gv_lang_misc.h"
#ifdef C_S_H_IS_COMPILED
    #include "gv_csh_lang2.h"  //Needs parse_param from gv_lang_misc.h
    /* yyerror
     *  Error handling function.*/
    void yyerror(YYLTYPE *loc, YYMSC_RESULT_TYPE &csh, void * /*yyscanner*/, const char *str)
    {
        csh.AddCSH_Error(*loc, str);
    }
#else
    #include "gv_lang2.h"      //Needs parse_param from msc_lang_misc.h
    /* yyerror
     *  Error handling function.*/
    void yyerror(YYLTYPE *loc, YYMSC_RESULT_TYPE &chart, void *yyscanner, const char *str)
    {
        chart.Error.Error(CHART_POS_START(*loc), str);
    }
#endif


#ifdef C_S_H_IS_COMPILED
void GraphCshParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#else
void GraphParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#endif
{
    std::remove_pointer<YY_EXTRA_TYPE>::type pp;
    pp.RESULT = &RESULT;
#ifdef C_S_H_IS_COMPILED
    pp.buff.buf = buff;
    pp.buff.length = len;
    pp.buff.pos = 0;
    gvcsh_lex_init(&pp.yyscanner);
    gvcsh_set_extra(&pp, pp.yyscanner);
    gvcsh_parse(RESULT, pp.yyscanner);
    gvcsh_lex_destroy(pp.yyscanner);
#else
    pp.buffs.emplace_back(buff, len);
    pp.pos_stack.file = RESULT.current_file;
    gv_lex_init(&pp.yyscanner);
    gv_set_extra(&pp, pp.yyscanner);
    gv_parse(RESULT, pp.yyscanner);
    gv_lex_destroy(pp.yyscanner);
    //on error we may have a parse stack or context stack left
#endif
}



#line 325 "gv_csh_lang.cc"


#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  85
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1543

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  39
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  81
/* YYNRULES -- Number of rules.  */
#define YYNRULES  268
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  307

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   290


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    36,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    37,     2,    38,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   199,   199,   210,   219,   219,   219,   219,   219,   219,
     220,   221,   236,   242,   254,   262,   268,   274,   282,   300,
     322,   338,   375,   393,   401,   406,   420,   434,   450,   473,
     490,   511,   521,   535,   574,   592,   609,   621,   632,   642,
     651,   659,   668,   683,   699,   710,   728,   740,   764,   773,
     784,   794,   806,   817,   827,   839,   850,   867,   885,   904,
     927,   939,   952,   968,   985,  1002,  1018,  1035,  1052,  1067,
    1078,  1084,  1087,  1093,  1105,  1105,  1105,  1107,  1117,  1132,
    1140,  1152,  1163,  1176,  1188,  1202,  1215,  1231,  1249,  1269,
    1285,  1307,  1319,  1352,  1365,  1382,  1402,  1416,  1427,  1446,
    1446,  1448,  1449,  1458,  1468,  1481,  1487,  1494,  1508,  1521,
    1536,  1556,  1562,  1565,  1581,  1604,  1622,  1640,  1665,  1685,
    1695,  1721,  1730,  1741,  1751,  1763,  1774,  1784,  1795,  1804,
    1817,  1827,  1843,  1862,  1874,  1888,  1889,  1890,  1891,  1892,
    1893,  1899,  1951,  1958,  1966,  1997,  2023,  2044,  2073,  2087,
    2087,  2087,  2090,  2105,  2118,  2127,  2138,  2138,  2138,  2140,
    2167,  2182,  2193,  2211,  2225,  2246,  2268,  2275,  2282,  2291,
    2300,  2314,  2335,  2349,  2362,  2363,  2383,  2404,  2416,  2430,
    2443,  2464,  2484,  2497,  2509,  2520,  2535,  2543,  2553,  2563,
    2574,  2587,  2598,  2599,  2609,  2630,  2647,  2665,  2673,  2686,
    2709,  2723,  2749,  2769,  2782,  2795,  2814,  2846,  2864,  2877,
    2888,  2900,  2914,  2927,  2949,  2962,  2977,  2979,  2994,  3000,
    3009,  3015,  3024,  3033,  3044,  3053,  3062,  3074,  3091,  3101,
    3112,  3133,  3139,  3145,  3147,  3147,  3149,  3150,  3151,  3152,
    3153,  3154,  3155,  3156,  3157,  3158,  3159,  3160,  3161,  3162,
    3163,  3164,  3165,  3167,  3167,  3169,  3200,  3204,  3206,  3210,
    3213,  3222,  3227,  3237,  3244,  3248,  3252,  3254,  3255
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "TOK_EOF", "error", "\"invalid token\"", "TOK_STRICT", "TOK_GRAPH",
  "TOK_DIGRAPH", "TOK_SUBGRAPH", "TOK_NODE", "TOK_EDGE", "TOK_EDGEOP",
  "TOK_EQUAL", "TOK_CLUSTER", "TOK_COLON_STRING",
  "TOK_COLON_QUOTED_STRING", "TOK_STRING", "TOK_QSTRING", "TOK_TILDE",
  "TOK_SEMICOLON", "TOK_PARAM_NAME", "TOK_OPARENTHESIS",
  "TOK_CPARENTHESIS", "TOK_COMMAND_DEFPROC", "TOK_COMMAND_REPLAY",
  "TOK_COMMAND_SET", "TOK_BYE", "TOK_IF", "TOK_THEN", "TOK_ELSE",
  "TOK_COMMAND_INCLUDE", "TOK_OCBRACKET", "TOK_CCBRACKET", "TOK_COMMA",
  "TOK_DEFDESIGN", "TOK_USEDESIGN", "TOK_DEFSTYLE",
  "TOK_UNRECOGNIZED_CHAR", "':'", "'['", "']'", "$accept", "main",
  "top_level_command", "top_level_command_with_semi", "list", "option",
  "usedesign", "designdef", "designdefhdr", "stylename", "stylenamelist",
  "styledef", "defproc", "defprochelp1", "defprochelp2", "defprochelp3",
  "defprochelp4", "scope_open_proc_body", "scope_close_proc_body",
  "proc_def_arglist_tested", "proc_def_arglist", "proc_def_param_list",
  "proc_def_param", "procedure_body", "set", "opt_semi",
  "attrinst_opt_semi", "attrinstrlist", "graph", "body",
  "empty_open_scope", "open_scope", "close_scope", "hdr", "graphtype",
  "instrlist", "instruction", "completed_proc_invocation",
  "proc_invocation", "proc_param_list", "proc_invoc_param_list",
  "proc_invoc_param", "include", "instruction_no_semi", "chain_with_attrs",
  "edgeop", "chain", "nodes", "nodelist", "node", "node0", "node1",
  "node2", "attrinst", "attrtype", "full_attrlist_with_label",
  "full_attrlist", "attrlist", "colon_string", "attrdefs", "attrdef",
  "attritem", "comp", "condition", "ifthen_condition", "else", "ifthen",
  "graphattrdef", "subgraph_attrs", "subgraph", "subghdr", "optseparator",
  "string_single", "reserved_word", "string_single_or_reserved_word",
  "tok_param_name_as_multi", "string_or_reserved_word_or_param",
  "string_or_param", "multi_string_continuation",
  "string_or_reserved_word", "string", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-189)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-234)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    1419,    35,    55,  -189,  -189,  -189,  -189,  -189,  -189,  1000,
    1509,    27,  1509,   104,   408,    48,    52,  -189,  1445,  -189,
    -189,  -189,    15,  -189,  -189,  -189,  -189,  1349,  1509,  -189,
     898,    52,  -189,  -189,  -189,    39,    68,  -189,  -189,  -189,
    1190,  1509,  -189,  -189,  -189,  -189,  -189,  -189,  -189,   478,
    -189,  -189,  -189,  -189,  -189,  -189,  -189,  -189,  -189,  -189,
    -189,   291,  -189,  -189,  -189,  1221,   105,  -189,  -189,    24,
      39,    57,  -189,  -189,  -189,  -189,  -189,    45,  -189,  -189,
      78,  -189,   548,  -189,  -189,  -189,  -189,  -189,  1384,  -189,
    -189,   234,    60,  -189,  -189,    67,  -189,   828,   933,  -189,
    1509,    80,   104,  -189,    47,  -189,  1509,  -189,  -189,  1509,
    -189,    98,  1035,  -189,  -189,  -189,  -189,  -189,  -189,  -189,
    1252,  -189,  -189,   164,  1128,  -189,   583,  -189,   513,  -189,
      85,   102,  -189,  -189,    45,  -189,  1066,  -189,  -189,  -189,
      44,    68,  -189,   121,   133,  -189,   443,  -189,  -189,   107,
    -189,   111,   369,  -189,   330,    68,  -189,  -189,  -189,  -189,
    1159,  -189,  -189,    80,  -189,   618,  1477,   653,    45,  -189,
    -189,    52,  -189,   197,    52,    68,  -189,  -189,   130,  -189,
     104,   863,  -189,  -189,   135,  -189,  -189,  -189,  -189,  -189,
    -189,  1509,  -189,  -189,  -189,  -189,  -189,  -189,   127,   128,
     968,  -189,  1283,  -189,  -189,   138,  -189,   139,  -189,   122,
    -189,  -189,   122,  -189,   688,    45,   131,   104,   104,   104,
    -189,   142,  1097,    38,  -189,  -189,    37,  -189,   104,   146,
    -189,    98,  -189,  -189,    65,  -189,  -189,    68,    54,  -189,
    -189,  -189,  -189,  1314,  -189,  -189,  -189,  -189,  -189,  -189,
    -189,  -189,  -189,  -189,   153,  -189,   104,  -189,  -189,  -189,
    -189,   104,  -189,   104,  -189,  -189,  -189,  -189,  -189,  -189,
    -189,  -189,    45,   723,  -189,  -189,  -189,  -189,  -189,   255,
     142,   255,  -189,   758,    60,  -189,  -189,  -189,  -189,  -189,
    -189,  -189,  -189,  -189,  -189,  -189,  -189,  -189,   793,  -189,
     255,  -189,    60,  -189,  -189,  -189,  -189
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       0,     0,    98,    99,   100,   234,   235,    14,   255,    34,
     119,   133,     0,    20,     0,     0,    71,    15,     0,     8,
       5,     4,     0,     6,     9,    33,     7,     0,    96,    10,
       0,    71,    18,   258,   259,   267,    11,    79,    92,    78,
       0,    97,   238,   243,   244,   239,   236,   237,   245,     0,
     249,   250,   251,   252,   246,   247,   248,    45,   241,   242,
     240,     0,    35,    38,    41,     0,    39,    47,    44,    42,
     264,    36,   266,   120,   134,    22,    19,     0,   144,    25,
      31,    24,     0,   174,    23,     1,    70,    12,     0,    16,
      91,    76,    81,    77,    95,     0,   113,     0,     0,    13,
     260,   268,   195,    89,     0,   166,   228,   167,   168,   229,
     111,    69,     0,    93,   138,   136,   137,   139,   151,    85,
       0,   101,   112,     0,     0,   141,     0,   145,     0,   152,
     156,   157,   158,   135,     0,   216,     0,   140,   165,   150,
       0,   159,    94,     0,    57,    48,     0,    54,   182,     0,
     183,   180,     0,   191,     0,   266,    65,    67,    46,    61,
       0,    40,    43,   265,    37,     0,    26,     0,     0,   175,
      17,    71,    74,     0,    71,     0,    80,   114,     0,   121,
     128,     0,   127,   132,     0,   116,   253,   254,   256,   257,
     261,   262,   194,    90,    86,   227,   230,    68,   204,   202,
       0,    87,   104,    83,   102,     0,   108,     0,   106,     0,
     187,   186,   148,   143,     0,   169,     0,   154,   161,   163,
     164,   209,     0,     0,   218,   217,     0,    49,    58,     0,
      53,    55,   185,   181,     0,   177,   188,   189,   233,   231,
     232,   192,    64,   104,    66,    60,    27,   176,    72,    21,
      75,    73,   122,   129,     0,   126,   130,   117,   263,   205,
     201,     0,   196,   198,    88,    84,   103,   110,   107,   147,
     159,   146,   172,     0,   155,   153,   160,   162,   206,   212,
       0,   210,   219,     0,   224,   220,    59,    51,    56,   178,
     190,   193,    63,    62,   124,   131,   200,   199,     0,   215,
     211,   213,   226,   221,   222,   214,   223
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -189,  -189,  -189,    23,  -189,  -189,    19,  -189,  -189,   -54,
    -189,    10,    22,  -189,  -189,    87,   108,  -189,  -148,  -189,
    -189,  -189,  -106,   126,  -189,   -27,     3,  -189,  -189,    -1,
    -189,  -189,   -88,  -189,   178,   125,  -114,    36,  -189,  -189,
    -189,  -163,    43,  -133,  -189,    70,  -189,   -77,  -189,   -18,
    -189,  -189,  -189,   -83,  -189,    74,   -12,    -8,    -7,  -189,
    -134,     9,  -189,  -189,  -189,  -188,  -189,  -189,   -21,  -189,
    -189,   -30,   -87,   -86,  -189,   -85,    18,  -189,   140,    21,
      11
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,    15,    16,    17,    18,    19,   114,    21,    22,    79,
      80,   115,   116,    25,    62,    63,    64,    65,   159,    66,
      67,   146,   147,    68,   117,    87,   172,   173,    26,   118,
      91,    40,   119,    27,    28,   120,   121,   122,    30,    98,
     181,   182,   123,   124,   125,    81,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   225,   214,    83,   215,   152,
     153,   135,   263,   199,   136,   279,   137,   138,   226,   139,
     140,   241,    33,    70,   188,    34,   190,    35,   101,    84,
     141
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      39,    69,    82,   222,    99,   197,   204,   151,   174,    32,
      23,    36,   245,   186,   187,   189,   194,   253,   236,    20,
      72,    72,    24,    72,    76,    72,    93,    32,    23,    36,
      71,    73,   203,    75,   281,    37,    29,    20,   284,    72,
      24,    89,    74,    31,    90,   223,   204,   193,    85,    94,
     210,   211,    72,    57,    29,   100,   210,   211,    69,     3,
       4,    31,   142,    69,    38,   165,    38,    38,   167,    86,
     154,   239,   155,    38,   169,    61,    49,   113,   102,     5,
       6,    61,    61,     8,   177,   240,    57,    39,   266,    38,
     174,   176,   300,   295,    61,   293,   191,    32,    23,    36,
     290,   171,   175,   289,   186,   187,   189,    20,   183,   166,
      24,   170,   246,   192,   265,    61,   144,    72,     5,     6,
      72,   218,     8,   200,    29,   288,   220,   195,   106,   266,
     196,    31,   269,   109,    57,   271,     5,     6,   219,   224,
       8,   227,    61,   228,   248,   232,   299,   251,   301,   233,
     252,    38,   257,   259,   260,   267,   268,   169,   164,   169,
     247,   154,   274,   237,  -109,   205,   287,   305,  -109,   278,
    -109,  -109,  -109,   294,   161,  -109,   250,    72,  -109,  -109,
      41,   206,  -109,   171,   175,  -109,  -109,  -109,  -109,  -109,
     160,   183,  -109,  -109,  -109,   162,   212,  -109,  -109,   275,
     213,   105,   283,   273,   107,   108,   169,   272,   291,   258,
     163,     5,     6,     0,     0,     8,     0,     0,     0,     0,
     270,     0,   282,   270,     0,   285,     0,   249,   270,   276,
     277,    14,     0,     0,     0,     0,     0,     0,   105,   286,
       0,   107,   108,   154,     0,   175,     0,     0,     5,     6,
       0,     0,     8,     0,     0,     0,     0,     0,     0,   105,
     298,   106,   107,   108,     0,   169,   109,   183,    14,     5,
       6,     0,   296,     8,   297,     0,     9,     0,   111,     0,
     112,     0,   303,   304,    38,     0,     0,     0,    13,    14,
     169,   148,   149,     0,    42,    43,    44,    45,    46,    47,
      78,   306,    48,  -184,  -184,     5,     6,     0,  -184,     8,
       0,     0,    50,    51,    52,    53,    54,    55,    56,  -184,
    -184,  -184,     0,    58,    59,    60,     0,     0,  -184,   150,
    -233,   238,     0,  -233,  -233,  -233,  -233,  -233,  -233,  -233,
       0,  -233,  -233,  -233,  -233,  -233,     0,   239,  -233,     0,
       0,  -233,  -233,  -233,  -233,  -233,     0,  -233,  -233,  -233,
    -233,   240,  -233,  -233,  -233,     0,     0,  -233,  -233,  -179,
     234,     0,  -179,  -179,  -179,  -179,  -179,  -179,  -179,     0,
    -179,  -179,  -179,     5,     6,     0,  -179,     8,     0,     0,
    -179,  -179,  -179,  -179,  -179,     0,  -179,  -179,  -179,  -179,
       0,  -179,  -179,  -179,     0,     0,  -179,   235,   -32,    77,
       0,    42,    43,    44,    45,    46,    47,    78,     0,    48,
       0,     0,     5,     6,     0,   -32,     8,     0,     0,    50,
      51,    52,    53,    54,    55,    56,   -32,   -32,   -32,     0,
      58,    59,    60,   -52,   229,    61,   -52,   -52,   -52,   -52,
     -52,   -52,     0,     0,   -52,     0,     0,   -52,   -52,     0,
     -52,   -52,     0,   230,   -52,   -52,   -52,   -52,   -52,     0,
     -52,   -52,   -52,   -52,   231,   -52,   -52,   -52,   -50,   143,
     -52,   -50,   -50,   -50,   -50,   -50,   -50,     0,     0,   -50,
       0,     0,   -50,   -50,     0,   -50,   144,     0,   145,   -50,
     -50,   -50,   -50,   -50,     0,   -50,   -50,   -50,   -50,     0,
     -50,   -50,   -50,  -149,   216,   -50,     0,  -149,     0,  -149,
    -149,  -149,  -149,     0,  -149,  -149,  -149,  -149,  -149,     0,
    -149,  -149,     0,     0,  -149,  -149,  -149,  -149,  -149,     0,
    -149,  -149,  -149,  -149,   217,     0,  -149,  -149,   -29,   168,
    -149,   -29,   -29,   -29,   -29,   -29,   -29,     0,     0,   -29,
       0,     0,   -29,   -29,     0,   -29,   -29,     0,     0,   -29,
     -29,   -29,   -29,   -29,     0,   -29,   -29,   -29,   -29,     0,
     -29,   -29,   -29,  -142,   209,    61,     0,  -142,     0,  -142,
    -142,  -142,    78,     0,  -142,   210,   211,  -142,  -142,     0,
    -142,  -142,     0,     0,  -142,  -142,  -142,  -142,  -142,     0,
    -142,  -142,  -142,  -142,     0,     0,  -142,  -142,   -30,   168,
      61,   -30,   -30,   -30,   -30,   -30,   -30,     0,     0,   -30,
       0,     0,   -30,   -30,     0,   -30,   -30,     0,     0,   -30,
     -30,   -30,   -30,   -30,     0,   -30,   -30,   -30,   -30,     0,
     -30,   -30,   -30,   -28,   168,    61,   -28,   -28,   -28,   -28,
     -28,   -28,     0,     0,   -28,     0,     0,   -28,   -28,     0,
     -28,   -28,     0,     0,   -28,   -28,   -28,   -28,   -28,     0,
     -28,   -28,   -28,   -28,     0,   -28,   -28,   -28,  -173,   168,
      61,     0,  -173,     0,  -173,  -173,  -173,  -173,     0,  -173,
     210,   211,  -173,  -173,     0,  -173,  -173,     0,     0,  -173,
    -173,  -173,  -173,  -173,     0,  -173,  -173,  -173,  -173,     0,
       0,  -173,  -173,  -170,   168,    61,     0,  -170,     0,  -170,
    -170,  -170,  -170,     0,  -170,  -170,  -170,  -170,  -170,     0,
    -170,  -170,     0,     0,  -170,  -170,  -170,  -170,  -170,     0,
    -170,  -170,  -170,  -170,     0,     0,  -170,  -170,  -225,   302,
      61,     0,  -225,     0,  -225,  -225,  -225,  -225,     0,  -225,
    -225,  -225,  -225,  -225,     0,  -225,  -225,     0,     0,  -225,
    -225,  -225,  -225,  -225,     0,  -225,  -225,    38,  -225,     0,
       0,  -225,  -225,  -171,   168,  -225,     0,  -171,     0,  -171,
    -171,  -171,  -171,     0,  -171,  -171,  -171,  -171,  -171,     0,
    -171,  -171,     0,     0,  -171,  -171,  -171,  -171,  -171,     0,
    -171,  -171,  -171,  -171,     0,     0,  -171,  -171,  -123,   178,
      61,  -123,  -123,  -123,  -123,  -123,  -123,     0,     0,  -123,
       0,     0,     5,     6,     0,  -123,     8,     0,   179,  -123,
    -123,  -123,  -123,  -123,     0,     0,  -123,  -123,  -123,   180,
    -123,  -123,  -123,  -125,   254,     0,  -125,  -125,  -125,  -125,
    -125,  -125,     0,     0,  -125,     0,     0,  -125,  -125,     0,
    -125,  -125,     0,   255,  -125,  -125,  -125,  -125,  -125,     0,
       0,  -125,  -125,  -125,   256,  -125,  -125,  -125,  -115,    95,
       0,  -115,  -115,  -115,  -115,  -115,  -115,     0,     0,  -115,
       0,     0,  -115,  -115,     0,    96,  -115,    97,     0,  -115,
    -115,  -115,  -115,  -115,     0,     0,  -115,  -115,  -115,     0,
    -115,  -115,  -115,  -118,   184,     0,  -118,  -118,  -118,  -118,
    -118,  -118,     0,     0,  -118,     0,     0,  -118,  -118,     0,
     185,  -118,     0,     0,  -118,  -118,  -118,  -118,  -118,     0,
       0,  -118,  -118,  -118,     0,  -118,  -118,  -118,  -197,   261,
       0,     0,  -197,     0,  -197,  -197,  -197,   262,     0,  -197,
       0,     0,  -197,  -197,     0,  -197,  -197,     0,     0,  -197,
    -197,  -197,  -197,  -197,  -197,  -197,  -197,  -197,  -197,     0,
       0,  -197,  -197,    42,    43,    44,    45,    46,    47,     0,
       0,    48,     0,     0,     5,     6,     0,     0,     8,    49,
       0,    50,    51,    52,    53,    54,    55,    56,     0,    57,
       0,     0,    58,    59,    60,  -203,   198,    61,     0,  -203,
       0,  -203,  -203,  -203,     0,     0,  -203,     0,     0,     5,
       6,     0,  -203,     8,     0,     0,  -203,  -203,  -203,  -203,
    -203,     0,  -203,  -203,  -203,  -203,  -208,   221,  -203,  -203,
     105,     0,   106,   107,   108,     0,     0,   109,     0,     0,
       5,     6,     0,  -208,     8,     0,     0,     9,  -208,   111,
    -208,   112,     0,  -208,  -208,    38,  -208,  -207,   280,    13,
      14,  -207,     0,  -207,  -207,  -207,     0,     0,  -207,     0,
       0,  -207,  -207,     0,  -207,  -207,     0,     0,  -207,  -207,
    -207,  -207,  -207,     0,   278,  -207,  -207,  -207,  -105,   207,
    -207,  -207,  -105,     0,  -105,  -105,  -105,     0,     0,  -105,
       0,     0,  -105,  -105,     0,   208,  -105,     0,     0,  -105,
    -105,  -105,  -105,  -105,     0,     0,  -105,  -105,  -105,   242,
     243,  -105,  -105,   105,     0,   106,   107,   108,     0,     0,
     109,     0,     0,     5,     6,     0,   110,     8,     0,     0,
       9,    10,   111,   244,   112,     0,     0,    11,    38,   158,
     103,   104,    13,    14,   105,     0,   106,   107,   108,     0,
       0,   109,     0,     0,     5,     6,     0,   110,     8,     0,
       0,     9,    10,   111,     0,   112,     0,     0,    11,    38,
     113,   156,     0,    13,    14,   105,     0,   106,   107,   108,
       0,     0,   109,     0,     0,     5,     6,     0,   110,     8,
       0,     0,     9,    10,   111,   157,   112,     0,     0,    11,
      38,   158,   201,   202,    13,    14,   105,     0,   106,   107,
     108,     0,     0,   109,     0,     0,     5,     6,     0,   110,
       8,     0,     0,     9,    10,   111,     0,   112,     0,     0,
      11,    38,   113,   264,     0,    13,    14,   105,     0,   106,
     107,   108,     0,     0,   109,     0,     0,     5,     6,     0,
     110,     8,     0,     0,     9,    10,   111,     0,   112,     0,
       0,    11,    38,   113,   292,     0,    13,    14,   105,     0,
     106,   107,   108,     0,     0,   109,     0,     0,     5,     6,
       0,   110,     8,     0,     0,     9,    10,   111,     0,   112,
       0,     0,    11,    38,   158,     0,     0,    13,    14,   -82,
      92,     0,   -82,   -82,   -82,     0,     0,     0,     0,     0,
       0,     0,     0,   -82,   -82,     0,   -82,   -82,     0,     0,
     -82,   -82,     0,     0,     0,     0,     0,   -82,    38,     0,
       0,   -82,   -82,   -82,    37,     1,     0,     2,     3,     4,
       0,     0,     0,     0,     0,     0,     0,     0,     5,     6,
       0,     7,     8,     0,     0,     9,    10,     0,     0,     0,
       0,     0,    11,    38,     0,     0,    12,    13,    14,    -3,
       1,     0,     2,     3,     4,     0,     0,     0,     0,     0,
       0,     0,     0,     5,     6,     0,     7,     8,     0,     0,
       9,    10,     0,     0,     0,    -2,    88,    11,     2,     3,
       4,    12,    13,    14,     0,     0,     0,     0,     0,     5,
       6,     0,     7,     8,     0,     0,     9,    10,     0,     0,
       0,     0,     0,    11,     0,     0,     0,    12,    13,    14,
      42,    43,    44,    45,    46,    47,    78,     0,    48,     0,
       0,     5,     6,     0,     0,     8,     0,     0,    50,    51,
      52,    53,    54,    55,    56,     0,     0,     0,     0,    58,
      59,    60,    42,    43,    44,    45,    46,    47,     0,     0,
      48,     0,     0,     5,     6,     0,     0,     8,     0,     0,
      50,    51,    52,    53,    54,    55,    56,     0,     0,     0,
       0,    58,    59,    60
};

static const yytype_int16 yycheck[] =
{
       1,     9,    14,   136,    31,   111,   120,    61,    91,     0,
       0,     0,   160,   100,   100,   100,   104,   180,   152,     0,
       9,    10,     0,    12,    13,    14,    27,    18,    18,    18,
       9,    10,   120,    12,   222,     0,     0,    18,     1,    28,
      18,    18,    15,     0,    29,     1,   160,     0,     0,    28,
      12,    13,    41,    29,    18,    16,    12,    13,    66,     4,
       5,    18,    41,    71,    29,    77,    29,    29,    80,    17,
      61,    17,    61,    29,    82,    37,    19,    30,    10,    14,
      15,    37,    37,    18,    17,    31,    29,    88,   202,    29,
     173,    92,   280,   256,    37,   243,    16,    88,    88,    88,
     234,    91,    91,    38,   191,   191,   191,    88,    97,    31,
      88,    88,   166,   102,   202,    37,    18,   106,    14,    15,
     109,    36,    18,   112,    88,   231,   134,   106,     6,   243,
     109,    88,   209,    11,    29,   212,    14,    15,    36,   140,
      18,    20,    37,    10,   171,    38,   279,   174,   281,    38,
      20,    29,    17,    26,    26,    17,    17,   165,    71,   167,
     168,   152,    31,   152,     0,     1,    20,   300,     4,    27,
       6,     7,     8,    20,    66,    11,   173,   166,    14,    15,
       2,    17,    18,   173,   173,    21,    22,    23,    24,    25,
      65,   180,    28,    29,    30,    69,   126,    33,    34,   217,
     126,     4,   223,   215,     7,     8,   214,   214,   238,   191,
      70,    14,    15,    -1,    -1,    18,    -1,    -1,    -1,    -1,
     209,    -1,   223,   212,    -1,   226,    -1,    30,   217,   218,
     219,    34,    -1,    -1,    -1,    -1,    -1,    -1,     4,   228,
      -1,     7,     8,   234,    -1,   234,    -1,    -1,    14,    15,
      -1,    -1,    18,    -1,    -1,    -1,    -1,    -1,    -1,     4,
     272,     6,     7,     8,    -1,   273,    11,   256,    34,    14,
      15,    -1,   261,    18,   263,    -1,    21,    -1,    23,    -1,
      25,    -1,   283,   284,    29,    -1,    -1,    -1,    33,    34,
     298,     0,     1,    -1,     3,     4,     5,     6,     7,     8,
       9,   302,    11,    12,    13,    14,    15,    -1,    17,    18,
      -1,    -1,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    -1,    32,    33,    34,    -1,    -1,    37,    38,
       0,     1,    -1,     3,     4,     5,     6,     7,     8,     9,
      -1,    11,    12,    13,    14,    15,    -1,    17,    18,    -1,
      -1,    21,    22,    23,    24,    25,    -1,    27,    28,    29,
      30,    31,    32,    33,    34,    -1,    -1,    37,    38,     0,
       1,    -1,     3,     4,     5,     6,     7,     8,     9,    -1,
      11,    12,    13,    14,    15,    -1,    17,    18,    -1,    -1,
      21,    22,    23,    24,    25,    -1,    27,    28,    29,    30,
      -1,    32,    33,    34,    -1,    -1,    37,    38,     0,     1,
      -1,     3,     4,     5,     6,     7,     8,     9,    -1,    11,
      -1,    -1,    14,    15,    -1,    17,    18,    -1,    -1,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    -1,
      32,    33,    34,     0,     1,    37,     3,     4,     5,     6,
       7,     8,    -1,    -1,    11,    -1,    -1,    14,    15,    -1,
      17,    18,    -1,    20,    21,    22,    23,    24,    25,    -1,
      27,    28,    29,    30,    31,    32,    33,    34,     0,     1,
      37,     3,     4,     5,     6,     7,     8,    -1,    -1,    11,
      -1,    -1,    14,    15,    -1,    17,    18,    -1,    20,    21,
      22,    23,    24,    25,    -1,    27,    28,    29,    30,    -1,
      32,    33,    34,     0,     1,    37,    -1,     4,    -1,     6,
       7,     8,     9,    -1,    11,    12,    13,    14,    15,    -1,
      17,    18,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      27,    28,    29,    30,    31,    -1,    33,    34,     0,     1,
      37,     3,     4,     5,     6,     7,     8,    -1,    -1,    11,
      -1,    -1,    14,    15,    -1,    17,    18,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    27,    28,    29,    30,    -1,
      32,    33,    34,     0,     1,    37,    -1,     4,    -1,     6,
       7,     8,     9,    -1,    11,    12,    13,    14,    15,    -1,
      17,    18,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      27,    28,    29,    30,    -1,    -1,    33,    34,     0,     1,
      37,     3,     4,     5,     6,     7,     8,    -1,    -1,    11,
      -1,    -1,    14,    15,    -1,    17,    18,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    27,    28,    29,    30,    -1,
      32,    33,    34,     0,     1,    37,     3,     4,     5,     6,
       7,     8,    -1,    -1,    11,    -1,    -1,    14,    15,    -1,
      17,    18,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      27,    28,    29,    30,    -1,    32,    33,    34,     0,     1,
      37,    -1,     4,    -1,     6,     7,     8,     9,    -1,    11,
      12,    13,    14,    15,    -1,    17,    18,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    27,    28,    29,    30,    -1,
      -1,    33,    34,     0,     1,    37,    -1,     4,    -1,     6,
       7,     8,     9,    -1,    11,    12,    13,    14,    15,    -1,
      17,    18,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      27,    28,    29,    30,    -1,    -1,    33,    34,     0,     1,
      37,    -1,     4,    -1,     6,     7,     8,     9,    -1,    11,
      12,    13,    14,    15,    -1,    17,    18,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    27,    28,    29,    30,    -1,
      -1,    33,    34,     0,     1,    37,    -1,     4,    -1,     6,
       7,     8,     9,    -1,    11,    12,    13,    14,    15,    -1,
      17,    18,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      27,    28,    29,    30,    -1,    -1,    33,    34,     0,     1,
      37,     3,     4,     5,     6,     7,     8,    -1,    -1,    11,
      -1,    -1,    14,    15,    -1,    17,    18,    -1,    20,    21,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    34,     0,     1,    -1,     3,     4,     5,     6,
       7,     8,    -1,    -1,    11,    -1,    -1,    14,    15,    -1,
      17,    18,    -1,    20,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    34,     0,     1,
      -1,     3,     4,     5,     6,     7,     8,    -1,    -1,    11,
      -1,    -1,    14,    15,    -1,    17,    18,    19,    -1,    21,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    -1,
      32,    33,    34,     0,     1,    -1,     3,     4,     5,     6,
       7,     8,    -1,    -1,    11,    -1,    -1,    14,    15,    -1,
      17,    18,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    -1,    32,    33,    34,     0,     1,
      -1,    -1,     4,    -1,     6,     7,     8,     9,    -1,    11,
      -1,    -1,    14,    15,    -1,    17,    18,    -1,    -1,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    -1,
      -1,    33,    34,     3,     4,     5,     6,     7,     8,    -1,
      -1,    11,    -1,    -1,    14,    15,    -1,    -1,    18,    19,
      -1,    21,    22,    23,    24,    25,    26,    27,    -1,    29,
      -1,    -1,    32,    33,    34,     0,     1,    37,    -1,     4,
      -1,     6,     7,     8,    -1,    -1,    11,    -1,    -1,    14,
      15,    -1,    17,    18,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    27,    28,    29,    30,     0,     1,    33,    34,
       4,    -1,     6,     7,     8,    -1,    -1,    11,    -1,    -1,
      14,    15,    -1,    17,    18,    -1,    -1,    21,    22,    23,
      24,    25,    -1,    27,    28,    29,    30,     0,     1,    33,
      34,     4,    -1,     6,     7,     8,    -1,    -1,    11,    -1,
      -1,    14,    15,    -1,    17,    18,    -1,    -1,    21,    22,
      23,    24,    25,    -1,    27,    28,    29,    30,     0,     1,
      33,    34,     4,    -1,     6,     7,     8,    -1,    -1,    11,
      -1,    -1,    14,    15,    -1,    17,    18,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,     0,
       1,    33,    34,     4,    -1,     6,     7,     8,    -1,    -1,
      11,    -1,    -1,    14,    15,    -1,    17,    18,    -1,    -1,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
       0,     1,    33,    34,     4,    -1,     6,     7,     8,    -1,
      -1,    11,    -1,    -1,    14,    15,    -1,    17,    18,    -1,
      -1,    21,    22,    23,    -1,    25,    -1,    -1,    28,    29,
      30,     0,    -1,    33,    34,     4,    -1,     6,     7,     8,
      -1,    -1,    11,    -1,    -1,    14,    15,    -1,    17,    18,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,     0,     1,    33,    34,     4,    -1,     6,     7,
       8,    -1,    -1,    11,    -1,    -1,    14,    15,    -1,    17,
      18,    -1,    -1,    21,    22,    23,    -1,    25,    -1,    -1,
      28,    29,    30,     0,    -1,    33,    34,     4,    -1,     6,
       7,     8,    -1,    -1,    11,    -1,    -1,    14,    15,    -1,
      17,    18,    -1,    -1,    21,    22,    23,    -1,    25,    -1,
      -1,    28,    29,    30,     0,    -1,    33,    34,     4,    -1,
       6,     7,     8,    -1,    -1,    11,    -1,    -1,    14,    15,
      -1,    17,    18,    -1,    -1,    21,    22,    23,    -1,    25,
      -1,    -1,    28,    29,    30,    -1,    -1,    33,    34,     0,
       1,    -1,     3,     4,     5,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    15,    -1,    17,    18,    -1,    -1,
      21,    22,    -1,    -1,    -1,    -1,    -1,    28,    29,    -1,
      -1,    32,    33,    34,     0,     1,    -1,     3,     4,     5,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    14,    15,
      -1,    17,    18,    -1,    -1,    21,    22,    -1,    -1,    -1,
      -1,    -1,    28,    29,    -1,    -1,    32,    33,    34,     0,
       1,    -1,     3,     4,     5,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    15,    -1,    17,    18,    -1,    -1,
      21,    22,    -1,    -1,    -1,     0,     1,    28,     3,     4,
       5,    32,    33,    34,    -1,    -1,    -1,    -1,    -1,    14,
      15,    -1,    17,    18,    -1,    -1,    21,    22,    -1,    -1,
      -1,    -1,    -1,    28,    -1,    -1,    -1,    32,    33,    34,
       3,     4,     5,     6,     7,     8,     9,    -1,    11,    -1,
      -1,    14,    15,    -1,    -1,    18,    -1,    -1,    21,    22,
      23,    24,    25,    26,    27,    -1,    -1,    -1,    -1,    32,
      33,    34,     3,     4,     5,     6,     7,     8,    -1,    -1,
      11,    -1,    -1,    14,    15,    -1,    -1,    18,    -1,    -1,
      21,    22,    23,    24,    25,    26,    27,    -1,    -1,    -1,
      -1,    32,    33,    34
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     1,     3,     4,     5,    14,    15,    17,    18,    21,
      22,    28,    32,    33,    34,    40,    41,    42,    43,    44,
      45,    46,    47,    50,    51,    52,    67,    72,    73,    76,
      77,    81,   100,   111,   114,   116,   119,     0,    29,    68,
      70,    73,     3,     4,     5,     6,     7,     8,    11,    19,
      21,    22,    23,    24,    25,    26,    27,    29,    32,    33,
      34,    37,    53,    54,    55,    56,    58,    59,    62,    96,
     112,   118,   119,   118,    15,   118,   119,     1,     9,    48,
      49,    84,    95,    96,   118,     0,    17,    64,     1,    42,
      29,    69,     1,    68,   118,     1,    17,    19,    78,    64,
      16,   117,    10,     0,     1,     4,     6,     7,     8,    11,
      17,    23,    25,    30,    45,    50,    51,    63,    68,    71,
      74,    75,    76,    81,    82,    83,    85,    86,    87,    88,
      89,    90,    91,    92,    93,   100,   103,   105,   106,   108,
     109,   119,   118,     1,    18,    20,    60,    61,     0,     1,
      38,    48,    98,    99,   100,   119,     0,    24,    30,    57,
      74,    55,    62,   117,    54,    95,    31,    95,     1,    96,
      42,    50,    65,    66,    92,   119,    68,    17,     1,    20,
      31,    79,    80,   119,     1,    17,   111,   112,   113,   114,
     115,    16,   119,     0,    71,   118,   118,    61,     1,   102,
     119,     0,     1,    71,    75,     1,    17,     1,    17,     1,
      12,    13,    84,    94,    95,    97,     1,    31,    36,    36,
      96,     1,    82,     1,    68,    94,   107,    20,    10,     1,
      20,    31,    38,    38,     1,    38,    99,   119,     1,    17,
      31,   110,     0,     1,    24,    57,    48,    96,    64,    30,
      65,    64,    20,    80,     1,    20,    31,    17,   115,    26,
      26,     1,     9,   101,     0,    71,    75,    17,    17,    86,
     119,    86,    97,    95,    31,    88,   119,   119,    27,   104,
       1,   104,    68,   107,     1,    68,   119,    20,    61,    38,
      99,   110,     0,    57,    20,    80,   119,   119,    95,    82,
     104,    82,     1,    68,    68,    82,    68
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    39,    40,    40,    41,    41,    41,    41,    41,    41,
      41,    41,    42,    42,    42,    43,    43,    43,    44,    45,
      45,    46,    47,    48,    48,    49,    49,    49,    50,    50,
      50,    50,    50,    51,    52,    52,    53,    53,    53,    54,
      54,    54,    55,    55,    55,    56,    57,    58,    59,    59,
      59,    59,    59,    59,    60,    60,    60,    61,    61,    61,
      62,    62,    62,    62,    62,    62,    62,    62,    63,    63,
      64,    64,    65,    65,    66,    66,    66,    67,    67,    67,
      67,    67,    67,    68,    68,    68,    68,    68,    68,    68,
      68,    69,    70,    71,    72,    72,    72,    72,    72,    73,
      73,    74,    74,    74,    74,    75,    75,    75,    75,    75,
      75,    75,    75,    76,    76,    76,    76,    76,    76,    77,
      77,    78,    78,    78,    78,    78,    78,    79,    79,    79,
      79,    79,    80,    81,    81,    82,    82,    82,    82,    82,
      82,    82,    83,    83,    84,    85,    85,    85,    85,    86,
      86,    86,    87,    87,    87,    87,    88,    88,    88,    89,
      90,    90,    91,    91,    92,    92,    93,    93,    93,    94,
      94,    94,    94,    94,    95,    95,    95,    96,    96,    96,
      96,    96,    96,    96,    96,    96,    97,    97,    98,    98,
      98,    98,    99,    99,   100,   100,   101,   102,   102,   102,
     102,   103,   103,   103,   103,   103,   104,   105,   105,   105,
     105,   105,   105,   105,   105,   105,   106,   107,   108,   108,
     108,   108,   108,   108,   108,   108,   108,   109,   109,   109,
     109,   110,   110,   110,   111,   111,   112,   112,   112,   112,
     112,   112,   112,   112,   112,   112,   112,   112,   112,   112,
     112,   112,   112,   113,   113,   114,   115,   115,   116,   116,
     117,   117,   117,   117,   118,   118,   118,   119,   119
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     0,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     2,     1,     1,     2,     3,     1,     2,
       1,     4,     2,     1,     1,     1,     2,     3,     3,     2,
       3,     2,     1,     1,     1,     2,     1,     2,     1,     1,
       2,     1,     1,     2,     1,     1,     1,     1,     2,     3,
       1,     4,     2,     3,     1,     2,     3,     1,     2,     3,
       3,     2,     4,     4,     3,     2,     3,     2,     2,     1,
       1,     0,     2,     2,     1,     2,     0,     2,     2,     2,
       3,     2,     1,     3,     4,     2,     3,     3,     4,     2,
       3,     1,     1,     1,     3,     2,     1,     2,     1,     1,
       1,     1,     2,     3,     2,     1,     2,     3,     2,     1,
       3,     1,     1,     2,     3,     1,     3,     4,     2,     1,
       2,     2,     3,     1,     4,     2,     3,     1,     1,     2,
       2,     3,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     1,     1,     3,     3,     2,     1,
       1,     1,     1,     3,     2,     3,     1,     1,     1,     1,
       3,     2,     3,     2,     2,     1,     1,     1,     1,     1,
       2,     3,     2,     1,     1,     2,     3,     3,     4,     2,
       2,     3,     2,     2,     1,     3,     1,     1,     2,     2,
       3,     1,     2,     3,     3,     2,     1,     1,     2,     3,
       3,     3,     2,     1,     2,     3,     1,     2,     1,     2,
       3,     4,     3,     4,     5,     4,     1,     1,     2,     3,
       3,     4,     4,     5,     3,     3,     4,     2,     1,     1,
       2,     1,     1,     0,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     2,     3,     1,     2,     1,     1,     2
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, RESULT, yyscanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, RESULT, yyscanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), RESULT, yyscanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, RESULT, yyscanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_TOK_COLON_STRING: /* TOK_COLON_STRING  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1625 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_QUOTED_STRING: /* TOK_COLON_QUOTED_STRING  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1631 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_STRING: /* TOK_STRING  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1637 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_QSTRING: /* TOK_QSTRING  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1643 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_PARAM_NAME: /* TOK_PARAM_NAME  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1649 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_OCBRACKET: /* TOK_OCBRACKET  */
#line 134 "dot.yy"
            {}
#line 1655 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CCBRACKET: /* TOK_CCBRACKET  */
#line 134 "dot.yy"
            {}
#line 1661 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_designdefhdr: /* designdefhdr  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1667 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_stylename: /* stylename  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1673 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_stylenamelist: /* stylenamelist  */
#line 131 "dot.yy"
            {delete ((*yyvaluep).stringlist);}
#line 1679 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp1: /* defprochelp1  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 1685 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp2: /* defprochelp2  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 1691 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp3: /* defprochelp3  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 1697 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp4: /* defprochelp4  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 1703 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_scope_open_proc_body: /* scope_open_proc_body  */
#line 134 "dot.yy"
            {}
#line 1709 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_scope_close_proc_body: /* scope_close_proc_body  */
#line 134 "dot.yy"
            {}
#line 1715 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_arglist_tested: /* proc_def_arglist_tested  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 1721 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_arglist: /* proc_def_arglist  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 1727 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_param_list: /* proc_def_param_list  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 1733 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_param: /* proc_def_param  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdef);}
#line 1739 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_procedure_body: /* procedure_body  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procedure);}
#line 1745 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_body: /* body  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).nodelist);}
#line 1751 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_close_scope: /* close_scope  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).nodelist);}
#line 1757 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invocation: /* proc_invocation  */
#line 134 "dot.yy"
            {}
#line 1763 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_proc_param_list: /* proc_param_list  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoclist);}
#line 1769 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invoc_param_list: /* proc_invoc_param_list  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoclist);}
#line 1775 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invoc_param: /* proc_invoc_param  */
#line 132 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoc);}
#line 1781 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_include: /* include  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1787 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_chain_with_attrs: /* chain_with_attrs  */
#line 124 "dot.yy"
            {
  #ifndef C_S_H_IS_COMPILED
    delete ((*yyvaluep).edgelist_attrlist).edgelist;
    delete ((*yyvaluep).edgelist_attrlist).style;
  #endif
}
#line 1798 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_chain: /* chain  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).edgelist);}
#line 1804 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_nodes: /* nodes  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).nodelist);}
#line 1810 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_nodelist: /* nodelist  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).nodelist);}
#line 1816 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_node: /* node  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).node);}
#line 1822 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_node0: /* node0  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).node);}
#line 1828 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_node1: /* node1  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).node);}
#line 1834 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_node2: /* node2  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).node);}
#line 1840 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_full_attrlist_with_label: /* full_attrlist_with_label  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).attrlist);}
#line 1846 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_full_attrlist: /* full_attrlist  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).attrlist);}
#line 1852 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_attrlist: /* attrlist  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).attrlist);}
#line 1858 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_colon_string: /* colon_string  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1864 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_attrdefs: /* attrdefs  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).attrlist);}
#line 1870 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_attrdef: /* attrdef  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).attr);}
#line 1876 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_attritem: /* attritem  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).attr);}
#line 1882 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_comp: /* comp  */
#line 134 "dot.yy"
            {}
#line 1888 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_condition: /* condition  */
#line 134 "dot.yy"
            {}
#line 1894 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_ifthen_condition: /* ifthen_condition  */
#line 135 "dot.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
  #else
    chart.PopContext();
  #endif
}
#line 1906 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_else: /* else  */
#line 135 "dot.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
  #else
    chart.PopContext();
  #endif
}
#line 1918 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_graphattrdef: /* graphattrdef  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).attr);}
#line 1924 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_subgraph: /* subgraph  */
#line 123 "dot.yy"
            {if (!C_S_H) delete ((*yyvaluep).nodelist);}
#line 1930 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_string_single: /* string_single  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1936 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_reserved_word: /* reserved_word  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1942 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_string_single_or_reserved_word: /* string_single_or_reserved_word  */
#line 130 "dot.yy"
            {free(((*yyvaluep).str));}
#line 1948 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_tok_param_name_as_multi: /* tok_param_name_as_multi  */
#line 133 "dot.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1954 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_string_or_reserved_word_or_param: /* string_or_reserved_word_or_param  */
#line 133 "dot.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1960 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_string_or_param: /* string_or_param  */
#line 133 "dot.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1966 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_multi_string_continuation: /* multi_string_continuation  */
#line 133 "dot.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1972 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_string_or_reserved_word: /* string_or_reserved_word  */
#line 133 "dot.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1978 "gv_csh_lang.cc"
        break;

    case YYSYMBOL_string: /* string  */
#line 133 "dot.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1984 "gv_csh_lang.cc"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */


/* User initialization code.  */
#line 8 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    yylloc.first_pos = 0;
    yylloc.last_pos = 0;
  #endif
}

#line 2086 "gv_csh_lang.cc"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, yyscanner);
    }

  if (yychar <= TOK_EOF)
    {
      yychar = TOK_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* main: list  */
#line 200 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintAfter((yylsp[0])) ||
        csh.CheckLineStartHintBefore((yylsp[0]))) {
        csh.AddLineBeginToHintsOutsideGraph(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 2307 "gv_csh_lang.cc"
    break;

  case 3: /* main: %empty  */
#line 210 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddLineBeginToHintsOutsideGraph(true, true);
    csh.hintStatus = HINT_READY;
    csh.hintSource = EHintSourceType::LINE_START;
    csh.hintsForcedOnly = true;
  #endif
}
#line 2320 "gv_csh_lang.cc"
    break;

  case 11: /* top_level_command: string  */
#line 222 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_LineBeginOutsideGraph((yylsp[0]), (yyvsp[0].multi_str).str);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHintsOutsideGraph(true,true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing keyword or chart option.");
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 2338 "gv_csh_lang.cc"
    break;

  case 12: /* top_level_command_with_semi: top_level_command opt_semi  */
#line 237 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
  #endif
}
#line 2348 "gv_csh_lang.cc"
    break;

  case 13: /* top_level_command_with_semi: include opt_semi  */
#line 243 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[-1].str)) {
        auto text = chart.Include((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
        if (text.first && text.first->length() && text.second.IsValid())
            GraphPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-1])), text.second, EInclusionReason::INCLUDE);
    }
  #endif
    if ((yyvsp[-1].str)) free((yyvsp[-1].str));
}
#line 2364 "gv_csh_lang.cc"
    break;

  case 14: /* top_level_command_with_semi: TOK_SEMICOLON  */
#line 255 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #endif
}
#line 2374 "gv_csh_lang.cc"
    break;

  case 15: /* list: top_level_command_with_semi  */
#line 263 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    gvcsh_get_extra(yyscanner)->last_top_level_command_pos = (yyloc);
  #endif
}
#line 2384 "gv_csh_lang.cc"
    break;

  case 16: /* list: list top_level_command_with_semi  */
#line 269 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    gvcsh_get_extra(yyscanner)->last_top_level_command_pos = (yylsp[0]);
  #endif
}
#line 2394 "gv_csh_lang.cc"
    break;

  case 17: /* list: list error top_level_command_with_semi  */
#line 275 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what this is.");
    gvcsh_get_extra(yyscanner)->last_top_level_command_pos = (yylsp[0]);
  #endif
}
#line 2405 "gv_csh_lang.cc"
    break;

  case 18: /* option: attritem  */
#line 283 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE)) {
        csh.AddOptionsValuesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
	if ((yyvsp[0].attr)) {
        chart.AddChartOption(*(yyvsp[0].attr));
		delete (yyvsp[0].attr);
	}
  #endif
}
#line 2426 "gv_csh_lang.cc"
    break;

  case 19: /* usedesign: TOK_USEDESIGN string  */
#line 301 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddDesignsToHints(true);
        csh.AddDesignsToHints(false);
        csh.hintStatus = HINT_READY; //dont add linebegin hints later
    }
  #else
    if (!(yyvsp[0].multi_str).had_error) {
        auto i = chart.Designs.find((yyvsp[0].multi_str).str);
        if (i==chart.Designs.end())
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Unknown design. Ignoring it.");
        else
            chart.MyCurrentContext().ApplyContextContent(i->second);
    }
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 2452 "gv_csh_lang.cc"
    break;

  case 20: /* usedesign: TOK_USEDESIGN  */
#line 323 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing design name to apply.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddDesignsToHints(true);
        csh.AddDesignsToHints(false);
        csh.hintStatus = HINT_READY; //dont add linebegin hints later
    }
  #else
     chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing design name to apply. Ignoring statement.");
  #endif
}
#line 2470 "gv_csh_lang.cc"
    break;

  case 21: /* designdef: designdefhdr empty_open_scope attrinstrlist TOK_CCBRACKET  */
#line 339 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.BracePairs.push_back((yylsp[-2])+(yylsp[0]));
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-2])+(yylsp[0]));
    if (!csh.SkipContent()) {
        auto &d = csh.CurrentContext().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-3].str));
        if (i == d.end())
            d.emplace((yyvsp[-3].str), csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
  #else
    if (!chart.SkipContent()) {
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-3].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-2]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    auto ret = chart.PopContext().release();
    chart.FinalizeGraph(false);
    delete ret;
  #endif
    free((yyvsp[-3].str));
    (yyvsp[0].input_text_ptr); //suppress
}
#line 2510 "gv_csh_lang.cc"
    break;

  case 22: /* designdefhdr: TOK_DEFDESIGN string_or_reserved_word  */
#line 376 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.SkipContent())
        csh.AddCSH_Error((yylsp[-1]), "Cannot define designs inside a procedure.");
    else
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define designs inside a procedure.");
    chart.NewGraph(true, false, ""); //parameters do not matter much
    chart.lst_scope_is_graph = true; //so that we do not add a subgraph for the design context
  #endif
    (yyval.str) = (yyvsp[0].multi_str).str;
}
#line 2531 "gv_csh_lang.cc"
    break;

  case 23: /* stylename: string_or_reserved_word  */
#line 394 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
  #endif
    (yyval.str) = (yyvsp[0].multi_str).str;
}
#line 2543 "gv_csh_lang.cc"
    break;

  case 24: /* stylename: edgeop  */
#line 402 "dot.yy"
{
    (yyval.str) = strdup((yyvsp[0].edgetype).AsText());
}
#line 2551 "gv_csh_lang.cc"
    break;

  case 25: /* stylenamelist: stylename  */
#line 407 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.stringlist) = new std::list<std::string>;
    if ((yyvsp[0].str)) {
        (yyval.stringlist)->emplace_back((yyvsp[0].str));
        free((yyvsp[0].str));
    }
}
#line 2569 "gv_csh_lang.cc"
    break;

  case 26: /* stylenamelist: stylenamelist TOK_COMMA  */
#line 421 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing another style name to define.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing another style name to define.");
  #endif
    (yyval.stringlist) = (yyvsp[-1].stringlist);
}
#line 2587 "gv_csh_lang.cc"
    break;

  case 27: /* stylenamelist: stylenamelist TOK_COMMA stylename  */
#line 435 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.stringlist) = (yyvsp[-2].stringlist);
    if ((yyvsp[0].str)) {
        (yyval.stringlist)->emplace_back((yyvsp[0].str));
        free((yyvsp[0].str));
    }
}
#line 2606 "gv_csh_lang.cc"
    break;

  case 28: /* styledef: TOK_DEFSTYLE stylenamelist full_attrlist  */
#line 451 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (!csh.SkipContent())
        for (auto &str : *((yyvsp[-1].stringlist)))
            if (csh.ForbiddenStyles.find(str) == csh.ForbiddenStyles.end())
                csh.CurrentContext().StyleNames.insert(str);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE)) {
        csh.AttributeValues(GraphStyle::ANY);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME)) {
        csh.AttributeNames(GraphStyle::ANY);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        delete (yyvsp[0].attrlist);
    else
	    chart.AddAttributeListToStyleList((yyvsp[0].attrlist), (yyvsp[-1].stringlist)); //deletes $3, as well
  #endif
    delete (yyvsp[-1].stringlist);
}
#line 2633 "gv_csh_lang.cc"
    break;

  case 29: /* styledef: TOK_DEFSTYLE full_attrlist  */
#line 474 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE)) {
        csh.AttributeValues(GraphStyle::ANY);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME)) {
        csh.AttributeNames(GraphStyle::ANY);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "Missing style name to define.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing style name to define. Ignoring statement.");
    if ((yyvsp[0].attrlist)) delete (yyvsp[0].attrlist);
  #endif
}
#line 2654 "gv_csh_lang.cc"
    break;

  case 30: /* styledef: TOK_DEFSTYLE error full_attrlist  */
#line 491 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[-1]), "Expecting a style name to define.");
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
    if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE)) {
        csh.AttributeValues(GraphStyle::ANY);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME)) {
        csh.AttributeNames(GraphStyle::ANY);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing style name to define. Ignoring statement.");
    if ((yyvsp[0].attrlist)) delete (yyvsp[0].attrlist);
  #endif
}
#line 2679 "gv_csh_lang.cc"
    break;

  case 31: /* styledef: TOK_DEFSTYLE stylenamelist  */
#line 512 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing attributes enclosed in between '[' and ']'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing attributes enclosed in between '[' and ']'. Statement has no effect.");
  #endif
    delete (yyvsp[0].stringlist);
}
#line 2693 "gv_csh_lang.cc"
    break;

  case 32: /* styledef: TOK_DEFSTYLE  */
#line 522 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Expecting a style name (or a comma separated list of styles) to define.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting a style name (or a comma separated list of styles) to define. Statement has no effect.");
  #endif
}
#line 2710 "gv_csh_lang.cc"
    break;

  case 33: /* defproc: defprochelp1  */
#line 536 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procdefhelper)) {
        if (chart.SkipContent()) {
            chart.Error.Error(CHART_POS_START((yyloc)), "Cannot define procedures inside a procedure.");
        } else if ((yyvsp[0].procdefhelper)->name.had_error) {
            //do nothing, error already reported
        } else if ((yyvsp[0].procdefhelper)->name.str==nullptr || (yyvsp[0].procdefhelper)->name.str[0]==0) {
            chart.Error.Error((yyvsp[0].procdefhelper)->linenum_name, "Missing a procedure name to (re-)define. Ignoring this.");
        } else if (!(yyvsp[0].procdefhelper)->had_error && (yyvsp[0].procdefhelper)->body) {
            if (chart.MyCurrentContext().num_error != chart.Error.GetErrorNum(true, false)) {
                chart.Error.Error(CHART_POS_START((yyloc)), "There are warnings or errors inside the procedure definition. Ignoring it.");
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].name = (yyvsp[0].procdefhelper)->name.str;
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].status = EDefProcResult::PROBLEM;
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].file_pos = (yyvsp[0].procdefhelper)->linenum_body;
            } else if ((yyvsp[0].procdefhelper)->body->status==EDefProcResult::OK || (yyvsp[0].procdefhelper)->body->status==EDefProcResult::EMPTY) {
                if ((yyvsp[0].procdefhelper)->parameters) {
                    auto &p = chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str] = *(yyvsp[0].procdefhelper)->body;
                    p.name = (yyvsp[0].procdefhelper)->name.str;
                    p.parameters = std::move(*(yyvsp[0].procdefhelper)->parameters);
                    if ((yyvsp[0].procdefhelper)->attrs) for (auto &a : (yyvsp[0].procdefhelper)->attrs->attributes)
                        p.AddAttribute(a.second.GenerateAttribute(a.first.c_str(), false), chart);
                    if ((yyvsp[0].procdefhelper)->body->status==EDefProcResult::EMPTY)
                        chart.Error.Warning((yyvsp[0].procdefhelper)->linenum_body, "Empty procedure. Is this what you want?");
                } else {
                     chart.Error.Error(CHART_POS_START((yyloc)), "Ill-formed procedure parameter list. Ignoring this procedure definition.");
                }
            } else {
                 chart.Error.Error(CHART_POS_START((yyloc)), "Ill-formed procedure body. Ignoring this procedure definition.");
            }
        }
        delete (yyvsp[0].procdefhelper);
    }
  #endif
}
#line 2751 "gv_csh_lang.cc"
    break;

  case 34: /* defprochelp1: TOK_COMMAND_DEFPROC  */
#line 575 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.SkipContent()) {
        csh.AddCSH_Error((yylsp[0]), "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing procedure name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<GraphStyle>;
    (yyval.procdefhelper)->linenum_name = CHART_POS_AFTER((yyloc));
  #endif
}
#line 2773 "gv_csh_lang.cc"
    break;

  case 35: /* defprochelp1: TOK_COMMAND_DEFPROC defprochelp2  */
#line 593 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.SkipContent()) {
        csh.AddCSH_Error((yylsp[-1]), "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    }
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
  #endif
}
#line 2793 "gv_csh_lang.cc"
    break;

  case 36: /* defprochelp2: string_or_reserved_word  */
#line 610 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PROCNAME);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
    free((yyvsp[0].multi_str).str);
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<GraphStyle>;
    (yyval.procdefhelper)->name = (yyvsp[0].multi_str);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[0]));
  #endif
}
#line 2809 "gv_csh_lang.cc"
    break;

  case 37: /* defprochelp2: string_or_reserved_word defprochelp3  */
#line 622 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PROCNAME);
    free((yyvsp[-1].multi_str).str);
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->name = (yyvsp[-1].multi_str);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[-1]));
  #endif
}
#line 2824 "gv_csh_lang.cc"
    break;

  case 38: /* defprochelp2: defprochelp3  */
#line 633 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(CshPos((yylsp[0]).first_pos, (yylsp[0]).first_pos), "Missing procedure name.");
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[0]));
  #endif
}
#line 2837 "gv_csh_lang.cc"
    break;

  case 39: /* defprochelp3: proc_def_arglist_tested  */
#line 643 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<GraphStyle>;
    (yyval.procdefhelper)->parameters = (yyvsp[0].procparamdeflist);
  #endif
}
#line 2850 "gv_csh_lang.cc"
    break;

  case 40: /* defprochelp3: proc_def_arglist_tested defprochelp4  */
#line 652 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->parameters = (yyvsp[-1].procparamdeflist);
  #endif
}
#line 2862 "gv_csh_lang.cc"
    break;

  case 41: /* defprochelp3: defprochelp4  */
#line 660 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->parameters = new ProcParamDefList;
  #endif
}
#line 2874 "gv_csh_lang.cc"
    break;

  case 42: /* defprochelp4: attrlist  */
#line 669 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        csh.AddYesNoToHints();
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<GraphStyle>;
    (yyval.procdefhelper)->attrs = (yyvsp[0].attrlist);
  #endif
}
#line 2893 "gv_csh_lang.cc"
    break;

  case 43: /* defprochelp4: attrlist procedure_body  */
#line 684 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        csh.AddYesNoToHints();
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<GraphStyle>;
    (yyval.procdefhelper)->body = (yyvsp[0].procedure);
    (yyval.procdefhelper)->linenum_body = CHART_POS_START((yylsp[0]));
    (yyval.procdefhelper)->attrs = (yyvsp[-1].attrlist);
  #endif
}
#line 2913 "gv_csh_lang.cc"
    break;

  case 44: /* defprochelp4: procedure_body  */
#line 700 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<GraphStyle>;
    (yyval.procdefhelper)->body = (yyvsp[0].procedure);
    (yyval.procdefhelper)->linenum_body = CHART_POS_START((yylsp[0]));
  #endif
}
#line 2926 "gv_csh_lang.cc"
    break;

  case 45: /* scope_open_proc_body: TOK_OCBRACKET  */
#line 711 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    chart.MyCurrentContext().num_error = chart.Error.GetErrorNum(true, false);
    chart.NewGraph(true, false, ""); //parameters do not matter much
    chart.lst_scope_is_graph = true; //so that we do not add a subgraph for the procedure definition context below in PushContext
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT, EContextCreate::EMPTY);
    chart.MyCurrentContext().parameters = std::move(YYGET_EXTRA(yyscanner)->last_procedure_params);
    chart.MyCurrentContext().starts_procedure = true;
    _ASSERT(YYGET_EXTRA(yyscanner)->open_context_mode == base_parse_parm::EScopeOpenMode::NORMAL);
    YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
  #endif
    (yyval.input_text_ptr) = (yyvsp[0].input_text_ptr);
}
#line 2947 "gv_csh_lang.cc"
    break;

  case 46: /* scope_close_proc_body: TOK_CCBRACKET  */
#line 729 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
  #else
    chart.PopContext();
    chart.FinalizeGraph(false);
  #endif
    (yyval.input_text_ptr) = (yyvsp[0].input_text_ptr);
}
#line 2962 "gv_csh_lang.cc"
    break;

  case 47: /* proc_def_arglist_tested: proc_def_arglist  */
#line 741 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparamdeflist)) {
        auto pair = Procedure::AreAllParameterNamesUnique(*(yyvsp[0].procparamdeflist));
        if (pair.first) {
            chart.Error.Error(pair.second->linenum_name, "This parameter name is already used.");
            chart.Error.Error(pair.first->linenum_name, pair.second->linenum_name, "This parameter name is already used.");
            delete (yyvsp[0].procparamdeflist);
            (yyval.procparamdeflist) = nullptr;
        } else {
            //Also copy to YYGET_EXTRA(yyscanner)->last_procedure_params and set open_context_mode
            auto &store = YYGET_EXTRA(yyscanner)->last_procedure_params;
            store.clear();
            for (const auto &p : *(yyvsp[0].procparamdeflist))
                store.emplace(p->name, ProcParamResolved(std::string(), FileLineCol(), true));
            (yyval.procparamdeflist) = (yyvsp[0].procparamdeflist);
        }
    } else
        (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 2989 "gv_csh_lang.cc"
    break;

  case 48: /* proc_def_arglist: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 765 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparamdeflist) = new ProcParamDefList;
  #endif
}
#line 3002 "gv_csh_lang.cc"
    break;

  case 49: /* proc_def_arglist: TOK_OPARENTHESIS error TOK_CPARENTHESIS  */
#line 774 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter definitions.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter definitions.", "Say something like '($first, $second=default)'.");
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 3017 "gv_csh_lang.cc"
    break;

  case 50: /* proc_def_arglist: TOK_OPARENTHESIS  */
#line 785 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter list closed by a parenthesis ')'.");
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 3031 "gv_csh_lang.cc"
    break;

  case 51: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list error TOK_CPARENTHESIS  */
#line 795 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter definitions.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter definitions.");
    delete (yyvsp[-2].procparamdeflist);
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 3047 "gv_csh_lang.cc"
    break;

  case 52: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list  */
#line 807 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing closing parenthesis ')'.");
    delete (yyvsp[0].procparamdeflist);
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 3062 "gv_csh_lang.cc"
    break;

  case 53: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list TOK_CPARENTHESIS  */
#line 818 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparamdeflist) = (yyvsp[-1].procparamdeflist);
  #endif
}
#line 3075 "gv_csh_lang.cc"
    break;

  case 54: /* proc_def_param_list: proc_def_param  */
#line 828 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    free((yyvsp[0].procparamdef));
  #else
    if ((yyvsp[0].procparamdef)) {
        (yyval.procparamdeflist) = new ProcParamDefList;
        ((yyval.procparamdeflist))->Append((yyvsp[0].procparamdef));
    } else
        (yyval.procparamdeflist)= nullptr;
  #endif
}
#line 3091 "gv_csh_lang.cc"
    break;

  case 55: /* proc_def_param_list: proc_def_param_list TOK_COMMA  */
#line 840 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter after the comma.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter after the comma.");
    delete (yyvsp[-1].procparamdeflist);
    (yyval.procparamdeflist)= nullptr;
  #endif
}
#line 3106 "gv_csh_lang.cc"
    break;

  case 56: /* proc_def_param_list: proc_def_param_list TOK_COMMA proc_def_param  */
#line 851 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    free((yyvsp[0].procparamdef));
  #else
    if ((yyvsp[-2].procparamdeflist) && (yyvsp[0].procparamdef)) {
        ((yyvsp[-2].procparamdeflist))->Append((yyvsp[0].procparamdef));
        (yyval.procparamdeflist) = (yyvsp[-2].procparamdeflist);
    } else {
        delete (yyvsp[-2].procparamdeflist);
        delete (yyvsp[0].procparamdef);
        (yyval.procparamdeflist)= nullptr;
    }
  #endif
}
#line 3126 "gv_csh_lang.cc"
    break;

  case 57: /* proc_def_param: TOK_PARAM_NAME  */
#line 868 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].str) && (yyvsp[0].str)[0]=='$' && (yyvsp[0].str)[1])
        csh.AddCSH((yylsp[0]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[0]), "Need name after the '$' sign.");
    (yyval.procparamdef) = nullptr; //no value
  #else
    if ((yyvsp[0].str) && (yyvsp[0].str)[0]=='$' && (yyvsp[0].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[0].str), CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[0].str));
}
#line 3148 "gv_csh_lang.cc"
    break;

  case 58: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL  */
#line 886 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[-1].str) && (yyvsp[-1].str)[0]=='$' && (yyvsp[-1].str)[1])
        csh.AddCSH((yylsp[-1]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-1]), "Need name after the '$' sign.");
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    (yyval.procparamdef) = nullptr; //no value
  #else
    if ((yyvsp[-1].str) && (yyvsp[-1].str)[0]=='$' && (yyvsp[-1].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-1].str));
}
#line 3171 "gv_csh_lang.cc"
    break;

  case 59: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL string  */
#line 905 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1])
        csh.AddCSH((yylsp[-2]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-2]), "Need name after the '$' sign.");
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    (yyval.procparamdef) = (yyvsp[0].multi_str).str;
  #else
    if ((yyvsp[0].multi_str).had_error) {
        (yyval.procparamdef) = nullptr;
    } else if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-2].str), CHART_POS_START((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-2])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
    free((yyvsp[0].multi_str).str);
  #endif
    free((yyvsp[-2].str));
}
#line 3197 "gv_csh_lang.cc"
    break;

  case 60: /* procedure_body: scope_open_proc_body instrlist scope_close_proc_body  */
#line 928 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::OK;
    tmp->text = std::string(((yyvsp[-2].input_text_ptr)), ((yyvsp[0].input_text_ptr))+1)+";";
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
  #endif
}
#line 3213 "gv_csh_lang.cc"
    break;

  case 61: /* procedure_body: scope_open_proc_body scope_close_proc_body  */
#line 940 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::EMPTY;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3230 "gv_csh_lang.cc"
    break;

  case 62: /* procedure_body: scope_open_proc_body instrlist error scope_close_proc_body  */
#line 953 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "syntax error.");
  #endif
    yyerrok;
  (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3250 "gv_csh_lang.cc"
    break;

  case 63: /* procedure_body: scope_open_proc_body instrlist error TOK_EOF  */
#line 969 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_START((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
  (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3271 "gv_csh_lang.cc"
    break;

  case 64: /* procedure_body: scope_open_proc_body instrlist TOK_EOF  */
#line 986 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
  (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3292 "gv_csh_lang.cc"
    break;

  case 65: /* procedure_body: scope_open_proc_body TOK_EOF  */
#line 1003 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3312 "gv_csh_lang.cc"
    break;

  case 66: /* procedure_body: scope_open_proc_body instrlist TOK_BYE  */
#line 1019 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
  #endif
    (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3333 "gv_csh_lang.cc"
    break;

  case 67: /* procedure_body: scope_open_proc_body TOK_BYE  */
#line 1036 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
    (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 3353 "gv_csh_lang.cc"
    break;

  case 68: /* set: TOK_COMMAND_SET proc_def_param  */
#line 1053 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if ((yyvsp[0].procparamdef))
        free((yyvsp[0].procparamdef));
    else
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing value.");
  #else
    if (!chart.SkipContent())
        chart.SetVariable((yyvsp[0].procparamdef), CHART_POS((yyloc)));
    else
        delete (yyvsp[0].procparamdef);
  #endif
}
#line 3372 "gv_csh_lang.cc"
    break;

  case 69: /* set: TOK_COMMAND_SET  */
#line 1068 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing variable or parameter name to set.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing variable or parameter name to set.");
  #endif
}
#line 3385 "gv_csh_lang.cc"
    break;

  case 70: /* opt_semi: TOK_SEMICOLON  */
#line 1079 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_SEMICOLON);
  #endif
}
#line 3395 "gv_csh_lang.cc"
    break;

  case 72: /* attrinst_opt_semi: styledef opt_semi  */
#line 1088 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
  #endif
}
#line 3405 "gv_csh_lang.cc"
    break;

  case 73: /* attrinst_opt_semi: attrinst opt_semi  */
#line 1094 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHintsInsideDesignDef();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 3419 "gv_csh_lang.cc"
    break;

  case 77: /* graph: hdr body  */
#line 1108 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.EntityNames.clear();
    csh.RefNames.clear();
  #else
    delete (yyvsp[0].nodelist); //discard returned references to nodes - nodes stored in graph
    chart.FinalizeGraph(true);
  #endif
}
#line 3433 "gv_csh_lang.cc"
    break;

  case 78: /* graph: error body  */
#line 1118 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error, chart must start with either of the 'graph' or 'digraph' keywords.");
    csh.EntityNames.clear();
    csh.RefNames.clear();
  #else
    //Warning, here we have a 'body', which will call chart.GetCurrentGraph, without a call to
    //chart.NewGraph(), which happens in 'hdr'
    //GraphChart must survive this
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error, chart must start with either of the 'graph' or 'digraph' keywords.");
    delete (yyvsp[0].nodelist);
    chart.FinalizeGraph(false);
  #endif
}
#line 3452 "gv_csh_lang.cc"
    break;

  case 79: /* graph: error TOK_EOF  */
#line 1133 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error, chart must start with either of the 'graph' or 'digraph' keywords.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error, chart must start with either of the 'graph' or 'digraph' keywords.");
  #endif
}
#line 3464 "gv_csh_lang.cc"
    break;

  case 80: /* graph: hdr error body  */
#line 1141 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error, opening brace ('{') expected.");
    csh.EntityNames.clear();
    csh.RefNames.clear();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error, opening brace ('{') expected.");
    delete (yyvsp[0].nodelist);
    chart.FinalizeGraph(true);
  #endif
}
#line 3480 "gv_csh_lang.cc"
    break;

  case 81: /* graph: hdr error  */
#line 1153 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Syntax error, opening brace ('{') expected.");
    csh.EntityNames.clear();
    csh.RefNames.clear();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Syntax error, opening brace ('{') expected.");
    chart.FinalizeGraph(false);
  #endif
}
#line 3495 "gv_csh_lang.cc"
    break;

  case 82: /* graph: hdr  */
#line 1164 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Syntax error, opening brace ('{') expected.");
    csh.EntityNames.clear();
    csh.RefNames.clear();
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Syntax error, opening brace ('{') expected.");
    chart.FinalizeGraph(false);
  #endif
}
#line 3510 "gv_csh_lang.cc"
    break;

  case 83: /* body: open_scope instrlist close_scope  */
#line 1177 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.BracePairs.push_back((yyloc));
    if (csh.CheckLineStartHintBetween((yylsp[-1]), (yylsp[0]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.nodelist) = (yyvsp[0].nodelist);
  #endif
}
#line 3526 "gv_csh_lang.cc"
    break;

  case 84: /* body: open_scope instrlist error close_scope  */
#line 1189 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Syntax error here.");
    if (csh.CheckLineStartHintBetween((yylsp[-2]), (yylsp[-1]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error here.");
    (yyval.nodelist) = (yyvsp[0].nodelist);
  #endif
}
#line 3544 "gv_csh_lang.cc"
    break;

  case 85: /* body: open_scope close_scope  */
#line 1203 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.BracePairs.push_back((yyloc));
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.nodelist) = nullptr;
    delete (yyvsp[0].nodelist);
  #endif
}
#line 3561 "gv_csh_lang.cc"
    break;

  case 86: /* body: open_scope error close_scope  */
#line 1216 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Syntax error here.");
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.nodelist) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error here.");
    delete (yyvsp[0].nodelist);
  #endif
}
#line 3580 "gv_csh_lang.cc"
    break;

  case 87: /* body: open_scope instrlist TOK_EOF  */
#line 1232 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yyloc), "Missing a closing brace ('}').");
    if (csh.CheckLineStartHintAfter((yylsp[-1]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    if (CHART_POS_START((yylsp[-1])).line != CHART_POS((yylsp[-1])).end.line)
        chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding open_scope.");
    //close context ourselves
	(yyval.nodelist) = chart.PopContext().release();
  #endif
}
#line 3602 "gv_csh_lang.cc"
    break;

  case 88: /* body: open_scope instrlist error TOK_EOF  */
#line 1250 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Syntax error here.");
    csh.AddCSH_ErrorAfter((yyloc), "Missing a closing brace ('}').");
    if (csh.CheckLineStartHintBetween((yylsp[-2]), (yylsp[-1]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    if (CHART_POS_START((yylsp[-1])).line != CHART_POS((yylsp[-1])).end.line)
        chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding open_scope.");
    //close context ourselves
	(yyval.nodelist) = chart.PopContext().release();
  #endif
}
#line 3626 "gv_csh_lang.cc"
    break;

  case 89: /* body: open_scope TOK_EOF  */
#line 1270 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yyloc), "Missing a closing brace ('}').");
    if (csh.CheckLineStartHintAfter((yylsp[-1]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    //close context ourselves
    (yyval.nodelist) = chart.PopContext().release(); //will be empty list of nodes anyway.
  #endif
}
#line 3646 "gv_csh_lang.cc"
    break;

  case 90: /* body: open_scope error TOK_EOF  */
#line 1286 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Syntax error here.");
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    if (CHART_POS_START((yylsp[-1])).line != CHART_POS((yylsp[-1])).end.line)
        chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
    //close context ourselves
    (yyval.nodelist) = chart.PopContext().release(); //will be empty list of nodes anyway.
  #endif
}
#line 3670 "gv_csh_lang.cc"
    break;

  case 91: /* empty_open_scope: TOK_OCBRACKET  */
#line 1308 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(false, EContextParse::NORMAL);
  #else
    //This is only called for design definitions
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::NORMAL, EContextCreate::CLEAR);
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 3685 "gv_csh_lang.cc"
    break;

  case 92: /* open_scope: TOK_OCBRACKET  */
#line 1320 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext();
  #else
    if (YYGET_EXTRA(yyscanner)->open_context_mode == base_parse_parm::EScopeOpenMode::PROC_REPLAY) {
        //Open a scope to replay a procedure. The text of the procedure has
        //already been placed to the lex buffer stack (in fact the '{' already comes
        //from there), and the parameters are in YYEXTRA.
        YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
        chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::REPARSING);
        chart.MyCurrentContext().starts_procedure = true;
        chart.MyCurrentContext().parameters = std::move(YYGET_EXTRA(yyscanner)->last_procedure_params);
        chart.MyCurrentContext().export_colors = YYGET_EXTRA(yyscanner)->last_procedure->export_colors;
        chart.MyCurrentContext().export_styles = YYGET_EXTRA(yyscanner)->last_procedure->export_styles;
        YYGET_EXTRA(yyscanner)->last_procedure = nullptr;
    } else {
        //Just open a regular scope
        chart.PushContext(CHART_POS_START((yylsp[0])));
        //if the scope is preceeded by a subgraph header, we have set the
        //file_pos of the subgraph created in PushContext(). If there were
        //no such header and this is an unnamed subgraph, we set it below.
        //SetLineEnd() will not change an already finalized file_pos, so
        //we can call it anyway. Howwever, if this scope is that of a graph
        //(and there are no subgraphs in the parse stack, we of course omit such a call).
        if (chart.parse_stack.size())
            chart.parse_stack.back()->SetLineEnd(CHART_POS((yyloc)));
    }
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 3721 "gv_csh_lang.cc"
    break;

  case 93: /* close_scope: TOK_CCBRACKET  */
#line 1353 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PopContext();
  #else
    (yyval.nodelist) = chart.PopContext().release();
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 3735 "gv_csh_lang.cc"
    break;

  case 94: /* hdr: TOK_STRICT graphtype string_or_reserved_word  */
#line 1366 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    gvcsh_get_extra(yyscanner)->directed = (yyvsp[-1].boolean);
  #else
    if ((yyvsp[0].multi_str).had_error)
        chart.NewGraph((yyvsp[-1].boolean), true, nullptr);
    else
        chart.NewGraph((yyvsp[-1].boolean), true, (yyvsp[0].multi_str).str);
    gv_get_extra(yyscanner)->directed = (yyvsp[-1].boolean);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 3756 "gv_csh_lang.cc"
    break;

  case 95: /* hdr: graphtype string_or_reserved_word  */
#line 1383 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckLineStartHintBetween(gvcsh_get_extra(yyscanner)->last_top_level_command_pos, (yylsp[-1]))) {
        csh.AddLineBeginToHintsOutsideGraph(true, false);
        csh.hintStatus = HINT_READY;
    }
    gvcsh_get_extra(yyscanner)->directed = (yyvsp[-1].boolean);
  #else
    if ((yyvsp[0].multi_str).had_error)
        chart.NewGraph((yyvsp[-1].boolean), true, nullptr);
    else
	chart.NewGraph((yyvsp[-1].boolean), false, (yyvsp[0].multi_str).str);
    gv_get_extra(yyscanner)->directed = (yyvsp[-1].boolean);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 3780 "gv_csh_lang.cc"
    break;

  case 96: /* hdr: graphtype  */
#line 1403 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    gvcsh_get_extra(yyscanner)->directed = (yyvsp[0].boolean);
    if (csh.CheckLineStartHintBetween(gvcsh_get_extra(yyscanner)->last_top_level_command_pos, (yylsp[0]))) {
        csh.AddLineBeginToHintsOutsideGraph(true, false);
        csh.hintStatus = HINT_READY;
    }
  #else
	chart.NewGraph((yyvsp[0].boolean), false, nullptr);
    gv_get_extra(yyscanner)->directed = (yyvsp[0].boolean);
  #endif
}
#line 3798 "gv_csh_lang.cc"
    break;

  case 97: /* hdr: TOK_STRICT graphtype  */
#line 1417 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
	csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    gvcsh_get_extra(yyscanner)->directed = (yyvsp[0].boolean);
  #else
	chart.NewGraph((yyvsp[0].boolean), true, nullptr);
    gv_get_extra(yyscanner)->directed = (yyvsp[0].boolean);
  #endif
}
#line 3813 "gv_csh_lang.cc"
    break;

  case 98: /* hdr: TOK_STRICT  */
#line 1428 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'graph' or 'digraph' keywords.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddLineBeginToHintsOutsideGraph(false, true);
        csh.hintStatus = HINT_READY;
    }
    gvcsh_get_extra(yyscanner)->directed = true;
  #else
	chart.NewGraph(true, true, nullptr);
    gv_get_extra(yyscanner)->directed =true;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing 'graph' or 'digraph' keywords.");
  #endif
}
#line 3833 "gv_csh_lang.cc"
    break;

  case 102: /* instrlist: instrlist instruction  */
#line 1450 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBetween((yylsp[-1]), (yylsp[0]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 3846 "gv_csh_lang.cc"
    break;

  case 103: /* instrlist: instrlist error instruction  */
#line 1459 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
    if (csh.CheckLineStartHintBetween((yylsp[-2]), (yylsp[-1]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 3860 "gv_csh_lang.cc"
    break;

  case 104: /* instrlist: instrlist error  */
#line 1469 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Syntax error.");
    if (csh.CheckLineStartHintBetween((yylsp[-1]), (yylsp[0]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 3874 "gv_csh_lang.cc"
    break;

  case 105: /* instruction: instruction_no_semi  */
#line 1482 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
  #endif
}
#line 3884 "gv_csh_lang.cc"
    break;

  case 106: /* instruction: instruction_no_semi TOK_SEMICOLON  */
#line 1488 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
	csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #endif
}
#line 3895 "gv_csh_lang.cc"
    break;

  case 107: /* instruction: instruction_no_semi error TOK_SEMICOLON  */
#line 1495 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
	csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintBetween((yylsp[-2]), (yylsp[-1]))) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
}
#line 3913 "gv_csh_lang.cc"
    break;

  case 108: /* instruction: include TOK_SEMICOLON  */
#line 1509 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-1].str)) {
        auto text = chart.Include((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
        if (text.first && text.first->length() && text.second.IsValid())
            GraphPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-1])), text.second, EInclusionReason::INCLUDE);
    }
  #endif
    if ((yyvsp[-1].str)) free((yyvsp[-1].str));
}
#line 3930 "gv_csh_lang.cc"
    break;

  case 109: /* instruction: include  */
#line 1522 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    if ((yyvsp[0].str)) {
        auto text = chart.Include((yyvsp[0].str), CHART_POS_START((yylsp[0])));
        if (text.first && text.first->length() && text.second.IsValid())
            GraphPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[0])), text.second, EInclusionReason::INCLUDE);
    }
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
  #endif
    if ((yyvsp[0].str)) free((yyvsp[0].str));
}
#line 3949 "gv_csh_lang.cc"
    break;

  case 110: /* instruction: include error TOK_SEMICOLON  */
#line 1537 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-2].str)) {
        auto text = chart.Include((yyvsp[-2].str), CHART_POS_START((yylsp[-2])));
        if (text.first && text.first->length() && text.second.IsValid())
            GraphPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-2])), text.second, EInclusionReason::INCLUDE);
    }
  #endif
    if ((yyvsp[-2].str)) free((yyvsp[-2].str));
}
#line 3973 "gv_csh_lang.cc"
    break;

  case 111: /* instruction: TOK_SEMICOLON  */
#line 1557 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #endif
}
#line 3983 "gv_csh_lang.cc"
    break;

  case 113: /* completed_proc_invocation: proc_invocation TOK_SEMICOLON  */
#line 1566 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-1].cprocedure)) {
        auto ctx = ((yyvsp[-1].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[-1])), &chart);
        if (!ctx.first) {
            GraphPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-1].cprocedure))->text.c_str(), ((yyvsp[-1].cprocedure))->text.length(), &((yylsp[-1])), ((yyvsp[-1].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-1].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
  #endif
}
#line 4003 "gv_csh_lang.cc"
    break;

  case 114: /* completed_proc_invocation: proc_invocation error TOK_SEMICOLON  */
#line 1582 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-2].cprocedure)) {
        auto ctx = ((yyvsp[-2].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[-2])), &chart);
        if (!ctx.first) {
            GraphPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-2].cprocedure))->text.c_str(), ((yyvsp[-2].cprocedure))->text.length(), &((yylsp[-2])), ((yyvsp[-2].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-2].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
  #endif
}
#line 4030 "gv_csh_lang.cc"
    break;

  case 115: /* completed_proc_invocation: proc_invocation  */
#line 1605 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[0].cprocedure)) {
        auto ctx = ((yyvsp[0].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[0])), &chart);
        if (!ctx.first) {
            GraphPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[0].cprocedure))->text.c_str(), ((yyvsp[0].cprocedure))->text.length(), &((yylsp[0])), ((yyvsp[0].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[0].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
  #endif
}
#line 4052 "gv_csh_lang.cc"
    break;

  case 116: /* completed_proc_invocation: proc_invocation proc_param_list TOK_SEMICOLON  */
#line 1623 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-2].cprocedure) && (yyvsp[-1].procparaminvoclist)) {
        auto ctx = ((yyvsp[-2].cprocedure))->MatchParameters((yyvsp[-1].procparaminvoclist), CHART_POS((yylsp[-1])).end, &chart);
        if (!ctx.first) {
            GraphPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-2].cprocedure))->text.c_str(), ((yyvsp[-2].cprocedure))->text.length(), &((yylsp[-2])), ((yyvsp[-2].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-2].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else {
        delete (yyvsp[-1].procparaminvoclist);
    }
  #endif
}
#line 4074 "gv_csh_lang.cc"
    break;

  case 117: /* completed_proc_invocation: proc_invocation proc_param_list error TOK_SEMICOLON  */
#line 1641 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-3].cprocedure) && (yyvsp[-2].procparaminvoclist)) {
        auto ctx = ((yyvsp[-3].cprocedure))->MatchParameters((yyvsp[-2].procparaminvoclist), CHART_POS((yylsp[-2])).end, &chart);
        if (!ctx.first) {
            GraphPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-3].cprocedure))->text.c_str(), ((yyvsp[-3].cprocedure))->text.length(), &((yylsp[-3])), ((yyvsp[-3].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-3].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else {
        delete (yyvsp[-2].procparaminvoclist);
    }
  #endif
}
#line 4103 "gv_csh_lang.cc"
    break;

  case 118: /* completed_proc_invocation: proc_invocation proc_param_list  */
#line 1666 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-1].cprocedure) && (yyvsp[0].procparaminvoclist)) {
        auto ctx = ((yyvsp[-1].cprocedure))->MatchParameters((yyvsp[0].procparaminvoclist), CHART_POS((yylsp[0])).end, &chart);
        if (!ctx.first) {
            GraphPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-1].cprocedure))->text.c_str(), ((yyvsp[-1].cprocedure))->text.length(), &((yylsp[-1])), ((yyvsp[-1].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-1].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else
        delete (yyvsp[0].procparaminvoclist);
  #endif
}
#line 4126 "gv_csh_lang.cc"
    break;

  case 119: /* proc_invocation: TOK_COMMAND_REPLAY  */
#line 1686 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing procedure name.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing procedure name.");
    (yyval.cprocedure) = nullptr;
  #endif
}
#line 4140 "gv_csh_lang.cc"
    break;

  case 120: /* proc_invocation: TOK_COMMAND_REPLAY string_or_reserved_word  */
#line 1696 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_PROCNAME);
  #else
    (yyval.cprocedure) = nullptr;
    if (!(yyvsp[0].multi_str).had_error) {
        auto proc = chart.GetProcedure((yyvsp[0].multi_str).str);
        if (proc==nullptr)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Undefined procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::PROBLEM)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Ill-formed procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::OK) {
            //Only return the procedure if we are not inside a procedure definition
            if (!chart.SkipContent())
                (yyval.cprocedure) = proc;
            //else just move on parsing - we do not reparse procedure replays during
            //the definition of an outer procedure.
        }
        //else return null, emit no error for EMPTY
    }
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 4169 "gv_csh_lang.cc"
    break;

  case 121: /* proc_param_list: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 1722 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparaminvoclist) = new ProcParamInvocationList;
  #endif
}
#line 4182 "gv_csh_lang.cc"
    break;

  case 122: /* proc_param_list: TOK_OPARENTHESIS error TOK_CPARENTHESIS  */
#line 1731 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter syntax.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter syntax. Ignoring procedure call.");
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 4197 "gv_csh_lang.cc"
    break;

  case 123: /* proc_param_list: TOK_OPARENTHESIS  */
#line 1742 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter list closed by a parenthesis ')'. Ignoring procedure call.");
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 4211 "gv_csh_lang.cc"
    break;

  case 124: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list error TOK_CPARENTHESIS  */
#line 1752 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter syntax.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter syntax. Ignoring procedure call.");
    delete (yyvsp[-2].procparaminvoclist);
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 4227 "gv_csh_lang.cc"
    break;

  case 125: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list  */
#line 1764 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing closing parenthesis ')'. Ignoring procedure call.");
    delete (yyvsp[0].procparaminvoclist);
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 4242 "gv_csh_lang.cc"
    break;

  case 126: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list TOK_CPARENTHESIS  */
#line 1775 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparaminvoclist) = (yyvsp[-1].procparaminvoclist);
  #endif
}
#line 4255 "gv_csh_lang.cc"
    break;

  case 127: /* proc_invoc_param_list: proc_invoc_param  */
#line 1785 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparaminvoc)) {
        (yyval.procparaminvoclist) = new ProcParamInvocationList;
        ((yyval.procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
    } else
        (yyval.procparaminvoclist)= nullptr;
  #endif
}
#line 4270 "gv_csh_lang.cc"
    break;

  case 128: /* proc_invoc_param_list: TOK_COMMA  */
#line 1796 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
  #else
    (yyval.procparaminvoclist) = new ProcParamInvocationList;
    ((yyval.procparaminvoclist))->Append(new ProcParamInvocation(CHART_POS_START((yylsp[0]))));
  #endif
}
#line 4283 "gv_csh_lang.cc"
    break;

  case 129: /* proc_invoc_param_list: TOK_COMMA proc_invoc_param  */
#line 1805 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[0].procparaminvoc)) {
        (yyval.procparaminvoclist) = new ProcParamInvocationList;
        ((yyval.procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_START((yylsp[-1]))));
        ((yyval.procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
    } else
        (yyval.procparaminvoclist)= nullptr;
  #endif
}
#line 4300 "gv_csh_lang.cc"
    break;

  case 130: /* proc_invoc_param_list: proc_invoc_param_list TOK_COMMA  */
#line 1818 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
  #else
    if ((yyvsp[-1].procparaminvoclist))
        ((yyvsp[-1].procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_AFTER((yylsp[0]))));
    (yyval.procparaminvoclist) = (yyvsp[-1].procparaminvoclist);
  #endif
}
#line 4314 "gv_csh_lang.cc"
    break;

  case 131: /* proc_invoc_param_list: proc_invoc_param_list TOK_COMMA proc_invoc_param  */
#line 1828 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[-2].procparaminvoclist) && (yyvsp[0].procparaminvoc)) {
        ((yyvsp[-2].procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
        (yyval.procparaminvoclist) = (yyvsp[-2].procparaminvoclist);
    } else {
        delete (yyvsp[-2].procparaminvoclist);
        delete (yyvsp[0].procparaminvoc);
        (yyval.procparaminvoclist)= nullptr;
    }
  #endif
}
#line 4333 "gv_csh_lang.cc"
    break;

  case 132: /* proc_invoc_param: string  */
#line 1844 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error) {
        //If this is a quoted string, color as a label, else as an attribute value
        if ((yylsp[0]).first_pos>0 && YYGET_EXTRA(yyscanner)->buff.buf[(yylsp[0]).first_pos-1]=='\"')
            csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, {});
        else
            csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.procparaminvoc) = nullptr;
    else
        (yyval.procparaminvoc) = new ProcParamInvocation((yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 4355 "gv_csh_lang.cc"
    break;

  case 133: /* include: TOK_COMMAND_INCLUDE  */
#line 1863 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a file name to include. You must use quotation marks ('\"').");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints();
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a file name to include. You must use quotation marks ('\"').");
  #endif
    (yyval.str) = nullptr;
}
#line 4371 "gv_csh_lang.cc"
    break;

  case 134: /* include: TOK_COMMAND_INCLUDE TOK_QSTRING  */
#line 1875 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_INCLUDEFILE);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints();
    else if(csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints((yyvsp[0].str), (yylsp[0]));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 4387 "gv_csh_lang.cc"
    break;

  case 140: /* instruction_no_semi: ifthen  */
#line 1894 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.IfThenElses.push_back((yyloc));
  #endif
}
#line 4397 "gv_csh_lang.cc"
    break;

  case 141: /* instruction_no_semi: chain_with_attrs  */
#line 1900 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE)) {
        csh.AttributeValues((yyvsp[0].edgelist_attrlist) ? GraphStyle::EDGE : GraphStyle::NODE);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME)) {
        csh.AttributeNames((yyvsp[0].edgelist_attrlist) ? GraphStyle::EDGE : GraphStyle::NODE);
        csh.hintStatus = HINT_READY;
    }
  #else
	if ((yyvsp[0].edgelist_attrlist).edgelist && (yyvsp[0].edgelist_attrlist).edgelist->size() && !chart.SkipContent()) {
		if ((yyvsp[0].edgelist_attrlist).edgelist->size()==1) {
			//This is just a list of nodes - no edges
			//Just apply the attributes to all of them - ignore ports
            if ((yyvsp[0].edgelist_attrlist).style) {
			    for (auto &pNodePort : (yyvsp[0].edgelist_attrlist).edgelist->front().nodes)
			        pNodePort.node->AddAttributeList((yyvsp[0].edgelist_attrlist).style, &chart);
                chart.GetCurrentGraph().AddAttrName(GraphStyle::EGraphElementType::NODE, *(yyvsp[0].edgelist_attrlist).style);
            }
		} else {
			//There is a list of edges, create all of them, and add
			//all attributes, as well.
            //First take out any key attribute
            std::string key;
            if ((yyvsp[0].edgelist_attrlist).style)
                for (auto i = (yyvsp[0].edgelist_attrlist).style->attributes.begin(); i!=(yyvsp[0].edgelist_attrlist).style->attributes.end(); i++)
                    if (i->first == "key") {
                        key = i->second.value;
                        (yyvsp[0].edgelist_attrlist).style->attributes.erase(i); //is becomes invalid
                        break;
                    }
			for (auto i = (yyvsp[0].edgelist_attrlist).edgelist->begin(), j = ++(yyvsp[0].edgelist_attrlist).edgelist->begin(); j!=(yyvsp[0].edgelist_attrlist).edgelist->end(); i++, j++)
				for (auto &pNodePort1 : i->nodes)
					for (auto &pNodePort2 : j->nodes) {
						auto pEdge = chart.CreateEdge(pNodePort1.node, pNodePort2.node, pNodePort1.port, pNodePort2.port, pNodePort1.port_file_pos, pNodePort2.port_file_pos);
                        pEdge->style = chart.MyCurrentContext().styles["edge"];
						pEdge->SetType(j->type, chart.MyCurrentContext().styles);
                        if ((yyvsp[0].edgelist_attrlist).style)
						    pEdge->AddAttributeList((yyvsp[0].edgelist_attrlist).style, &chart);
                        pEdge->name = key;
                        pEdge->SetLineEnd(j->pos);
					}
            if ((yyvsp[0].edgelist_attrlist).style)
                chart.GetCurrentGraph().AddAttrName(GraphStyle::EGraphElementType::EDGE, *(yyvsp[0].edgelist_attrlist).style);
		}
	}
	delete (yyvsp[0].edgelist_attrlist).edgelist;
	delete (yyvsp[0].edgelist_attrlist).style;
  #endif
}
#line 4452 "gv_csh_lang.cc"
    break;

  case 142: /* chain_with_attrs: chain  */
#line 1952 "dot.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.edgelist_attrlist).edgelist  = (yyvsp[0].edgelist);
    (yyval.edgelist_attrlist).style = nullptr;
  #endif
}
#line 4463 "gv_csh_lang.cc"
    break;

  case 143: /* chain_with_attrs: chain full_attrlist_with_label  */
#line 1959 "dot.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.edgelist_attrlist).edgelist  = (yyvsp[-1].edgelist);
    (yyval.edgelist_attrlist).style = (yyvsp[0].attrlist);
  #endif
}
#line 4474 "gv_csh_lang.cc"
    break;

  case 144: /* edgeop: TOK_EDGEOP  */
#line 1967 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
//    if (($1.dir==GraphEdgeType::NO_ARROW) == gvcsh_get_extra(yyscanner)->directed) {
//        if (gvcsh_get_extra(yyscanner)->directed)
//	        csh.AddCSH_Error(@1, "Directed graphs expect directed edges. Use '->', '=>', '>', '>>' or bidirectional variants.");
//        else
//            csh.AddCSH_Error(@1, "Undirected graphs expect non-directed edges. Use '--', '==', '..' or '++'.");
//    } else
        csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
  #else
    //If we are defining a proc (and the context is empty, hence no pedantic chart option set)
    //we can assume pedantic==false and ignore any errors from mixed dir/undir edges. When we
    //replay the procedure, pedantic will be set as in the caller's context and these errors will 
    //be emitted (if needed). If the context is empty for any other reason (not selected if/then/else
    //branch, defining a design), we better not emit an error, so assuming pedantic==false is OK overall..
    if (chart.MyCurrentContext().pedantic.value_or(false) && (((yyvsp[0].edgetype).dir==GraphEdgeType::NO_ARROW) == gv_get_extra(yyscanner)->directed)) {
        std::string msg;
        if (gv_get_extra(yyscanner)->directed) {
	        msg = "Directed graphs expect directed edges. Assuming '";
            (yyvsp[0].edgetype).dir = GraphEdgeType::FWD;
        } else {
	        msg = "Undirected graphs expect undirected edges. Assuming '";
            (yyvsp[0].edgetype).dir = GraphEdgeType::NO_ARROW;
        }
        chart.Error.Warning(CHART_POS_START((yylsp[0])), msg + (yyvsp[0].edgetype).AsText() + "'.", "Use 'pedantic=false' to turn off these warnings.");
    }
  #endif
  (yyval.edgetype) = (yyvsp[0].edgetype);
}
#line 4508 "gv_csh_lang.cc"
    break;

  case 145: /* chain: nodes  */
#line 1998 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.edgelist) = nullptr; //how many set of nodes
    /* We located hints of entity (or line_start) */
    if (csh.CheckHintLocated(EHintSourceType::ENTITY, (yylsp[0]))) {
        csh.AddEntitiesToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        if (csh.IsCursorAtLineBegin()) {
            csh.AddLineBeginToHintsInsideGraph();
            csh.hintStatus = HINT_READY;
        } else {
            csh.hintStatus = HINT_NONE;
        }
    }
  #else
	(yyval.edgelist) = new GraphEdgeList();
	if ((yyvsp[0].nodelist)) {
		(yyval.edgelist)->emplace_back();
        (yyval.edgelist)->back().type = {GraphEdgeType::SOLID, GraphEdgeType::NO_ARROW};
        (yyval.edgelist)->back().nodes = std::move(*(yyvsp[0].nodelist));
		delete (yyvsp[0].nodelist);
	}
  #endif
}
#line 4538 "gv_csh_lang.cc"
    break;

  case 146: /* chain: chain edgeop nodes  */
#line 2024 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.edgelist) = ++(yyvsp[-2].edgelist);
    if (csh.CheckHintLocated(EHintSourceType::ENTITY, (yylsp[0])) ||
        csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.AddEntitiesToHints();
        csh.hintSource = EHintSourceType::ENTITY;
        csh.hintStatus = HINT_READY;
    }
  #else
	if ((yyvsp[0].nodelist)) {
		(yyvsp[-2].edgelist)->emplace_back();
        (yyvsp[-2].edgelist)->back().type = (yyvsp[-1].edgetype);
        (yyvsp[-2].edgelist)->back().pos = CHART_POS((yylsp[-1]));
        (yyvsp[-2].edgelist)->back().nodes = std::move(*(yyvsp[0].nodelist));
		delete (yyvsp[0].nodelist);
	}
	(yyval.edgelist) = (yyvsp[-2].edgelist);
  #endif
}
#line 4563 "gv_csh_lang.cc"
    break;

  case 147: /* chain: chain error nodes  */
#line 2045 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (gvcsh_get_extra(yyscanner)->directed)
        csh.AddCSH_Error((yylsp[-1]), "Missing edge, e.g., '->'.");
    else
        csh.AddCSH_Error((yylsp[-1]), "Missing edge, e.g., '--'.");
    (yyval.edgelist) = ++(yyvsp[-2].edgelist);
    if (csh.CheckHintLocated(EHintSourceType::ENTITY, (yylsp[0])) ||
        csh.CheckHintLocated(EHintSourceType::LINE_START, (yylsp[0]))) {
        csh.AddEntitiesToHints();
        csh.hintSource = EHintSourceType::ENTITY;
        csh.hintStatus = HINT_READY;
    }
  #else
	if ((yyvsp[0].nodelist)) {
		(yyvsp[-2].edgelist)->emplace_back();
        (yyvsp[-2].edgelist)->back().type = {GraphEdgeType::SOLID, gv_get_extra(yyscanner)->directed ? GraphEdgeType::FWD : GraphEdgeType::NO_ARROW};
        (yyvsp[-2].edgelist)->back().pos = CHART_POS((yylsp[0]));
        (yyvsp[-2].edgelist)->back().nodes = std::move(*(yyvsp[0].nodelist));
		delete (yyvsp[0].nodelist);
	}
	(yyval.edgelist) = (yyvsp[-2].edgelist);
    if (gv_get_extra(yyscanner)->directed)
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing edge symbol (e.g., '->').");
    else
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing edge symbol (e.g., '--').");
  #endif
}
#line 4596 "gv_csh_lang.cc"
    break;

  case 148: /* chain: chain edgeop  */
#line 2074 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a node, a set of nodes or a subgraph here.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY)) {
        csh.AddEntitiesToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
	(yyval.edgelist) = (yyvsp[-1].edgelist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a node, a set of nodes or a subgraph here.");
  #endif
}
#line 4613 "gv_csh_lang.cc"
    break;

  case 152: /* nodelist: node  */
#line 2091 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ENTITY, (yylsp[0]))) {
        csh.hintStatus = HINT_LOCATED;
        csh.hintSource = EHintSourceType::LINE_START;
    }
  #else
    (yyval.nodelist) = new GraphNodePortList;
    if ((yyvsp[0].node)) {
	(yyval.nodelist)->push_back(std::move(*(yyvsp[0].node)));
	delete (yyvsp[0].node);
    }
  #endif
}
#line 4632 "gv_csh_lang.cc"
    break;

  case 153: /* nodelist: nodelist TOK_COMMA node  */
#line 2106 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ENTITY);
  #else
    if ((yyvsp[0].node)) {
	(yyvsp[-2].nodelist)->push_back(std::move(*(yyvsp[0].node)));
	delete (yyvsp[0].node);
    }
    (yyval.nodelist) = (yyvsp[-2].nodelist);
  #endif
}
#line 4649 "gv_csh_lang.cc"
    break;

  case 154: /* nodelist: nodelist TOK_COMMA  */
#line 2119 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY);
  #else
    (yyval.nodelist) = (yyvsp[-1].nodelist);
  #endif
}
#line 4662 "gv_csh_lang.cc"
    break;

  case 155: /* nodelist: nodelist error TOK_COMMA  */
#line 2128 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error");
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ENTITY);
  #else
    (yyval.nodelist) = (yyvsp[-2].nodelist);
  #endif
}
#line 4676 "gv_csh_lang.cc"
    break;

  case 159: /* node0: string  */
#line 2141 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckHintAt((yylsp[0]), EHintSourceType::ENTITY);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_KeywordOrEntity((yylsp[0]), (yyvsp[0].multi_str).str);   //Do it after AddLineBeginToHints so if this is a newly defined entity it one is not included among the hints
  #else
    if ((yyvsp[0].multi_str).had_error) {
        (yyval.node) = nullptr;
    } else {
        if (chart.SkipContent()) {
            (yyval.node) = nullptr;
        } else {
            (yyval.node) = new GraphNodePort;
            //If node already exists we just fetch pointer
            if (chart.parse_stack.size())
                (yyval.node)->node = chart.CreateNode(*chart.parse_stack.back(), (yyvsp[0].multi_str).str); //add to current subgraph (will add to main graph, too)
            else
                (yyval.node)->node = chart.CreateNode(chart.GetCurrentGraph(), (yyvsp[0].multi_str).str); //no subgraph: add to main graph
            (yyval.node)->node->SetLineEnd(CHART_POS((yyloc))); //If node already exists, it will not override previously set lineend
            (yyval.node)->file_pos = CHART_POS((yyloc));
        }
    }
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 4706 "gv_csh_lang.cc"
    break;

  case 160: /* node1: node0 ':' string  */
#line 2168 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if ((yyvsp[-2].node) && !(yyvsp[0].multi_str).had_error) {
       (yyvsp[-2].node)->port = (yyvsp[0].multi_str).str;
       (yyvsp[-2].node)->port_file_pos = CHART_POS((yylsp[0]));
    }
    (yyval.node) = (yyvsp[-2].node);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 4725 "gv_csh_lang.cc"
    break;

  case 161: /* node1: node0 ':'  */
#line 2183 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing port name.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing port name.");
    (yyval.node) = (yyvsp[-1].node);
  #endif
}
#line 4739 "gv_csh_lang.cc"
    break;

  case 162: /* node2: node1 ':' string  */
#line 2194 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SYMBOL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_Compass((yylsp[0]), (yyvsp[0].multi_str).str);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, "headport"))
        csh.AttributeValues(GraphStyle::EDGE);
  #else
    if ((yyvsp[-2].node) && !(yyvsp[0].multi_str).had_error) {
        (yyvsp[-2].node)->port += ':';
        (yyvsp[-2].node)->port += (yyvsp[0].multi_str).str;
        (yyvsp[-2].node)->compass_file_pos = CHART_POS((yylsp[0]));
    }
    (yyval.node) = (yyvsp[-2].node);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 4761 "gv_csh_lang.cc"
    break;

  case 163: /* node2: node1 ':'  */
#line 2212 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SYMBOL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing compass point.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, "headport"))
        csh.AttributeValues(GraphStyle::EDGE);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing compass point.");
    (yyval.node) = (yyvsp[-1].node);
  #endif
}
#line 4777 "gv_csh_lang.cc"
    break;

  case 164: /* attrinst: attrtype attrlist  */
#line 2226 "dot.yy"
{
    GraphStyle::EGraphElementType element_type;
    switch ((yyvsp[-1].i)) {
		case TOK_GRAPH: element_type = GraphStyle::GRAPH; break;
		case TOK_EDGE: element_type = GraphStyle::EDGE; break;
		case TOK_NODE: element_type = GraphStyle::NODE; break;
        default: element_type = GraphStyle::ANY; break;
    }
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE)) {
        csh.AttributeValues(element_type);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME)) {
        csh.AttributeNames(element_type);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.DoAttributeInstruction(element_type, (yyvsp[0].attrlist));
  #endif
}
#line 4802 "gv_csh_lang.cc"
    break;

  case 165: /* attrinst: graphattrdef  */
#line 2247 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE)) {
        csh.AttributeValues(GraphStyle::CLUSTER);
        csh.AttributeValues(GraphStyle::GRAPH);
        csh.AttributeValues(GraphStyle::SUBGRAPH);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME)) {
        csh.AttributeNames(GraphStyle::CLUSTER);
        csh.AttributeNames(GraphStyle::GRAPH);
        csh.AttributeNames(GraphStyle::SUBGRAPH);
        csh.hintStatus = HINT_READY;
    }
  #else
	if ((yyvsp[0].attr)) {
        chart.AddChartOption(*(yyvsp[0].attr));
		delete (yyvsp[0].attr);
	}
  #endif
}
#line 4827 "gv_csh_lang.cc"
    break;

  case 166: /* attrtype: TOK_GRAPH  */
#line 2269 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yyloc), COLOR_KEYWORD);
  #endif
  (yyval.i) = TOK_GRAPH;
}
#line 4838 "gv_csh_lang.cc"
    break;

  case 167: /* attrtype: TOK_NODE  */
#line 2276 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yyloc), COLOR_KEYWORD);
  #endif
  (yyval.i) = TOK_NODE;
}
#line 4849 "gv_csh_lang.cc"
    break;

  case 168: /* attrtype: TOK_EDGE  */
#line 2283 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yyloc), COLOR_KEYWORD);
  #endif
  (yyval.i) = TOK_EDGE;
}
#line 4860 "gv_csh_lang.cc"
    break;

  case 169: /* full_attrlist_with_label: colon_string  */
#line 2292 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.attrlist) = new GraphStyle(EStyleType::STYLE);
    (yyval.attrlist)->AddAttribute(Attribute("::", (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yyloc)).IncStartCol()), &chart);
  #endif
    free((yyvsp[0].str));
}
#line 4873 "gv_csh_lang.cc"
    break;

  case 170: /* full_attrlist_with_label: colon_string full_attrlist  */
#line 2301 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].attrlist)) {
        ((yyvsp[0].attrlist))->AddAttribute(Attribute("::", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()), &chart);
        (yyval.attrlist) = (yyvsp[0].attrlist);
    } else {
        (yyval.attrlist) = new GraphStyle(EStyleType::STYLE);
        (yyval.attrlist)->AddAttribute(Attribute("::", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()), &chart);
    }
  #endif
    free((yyvsp[-1].str));
}
#line 4891 "gv_csh_lang.cc"
    break;

  case 171: /* full_attrlist_with_label: full_attrlist colon_string full_attrlist  */
#line 2315 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[-2].attrlist)) {
        ((yyvsp[-2].attrlist))->AddAttribute(Attribute("::", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()), &chart);
        if ((yyvsp[0].attrlist)) {
            *((yyvsp[-2].attrlist)) += *((yyvsp[0].attrlist));
            delete ((yyvsp[0].attrlist));
        }
        (yyval.attrlist) = (yyvsp[-2].attrlist);
    } else if ((yyvsp[0].attrlist)) {
        ((yyvsp[0].attrlist))->AddAttribute(Attribute("::", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()), &chart);
        (yyval.attrlist) = (yyvsp[0].attrlist);
    } else {
        (yyval.attrlist) = new GraphStyle(EStyleType::STYLE);
        (yyval.attrlist)->AddAttribute(Attribute("::", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()), &chart);
    }
  #endif
    free((yyvsp[-1].str));
}
#line 4916 "gv_csh_lang.cc"
    break;

  case 172: /* full_attrlist_with_label: full_attrlist colon_string  */
#line 2336 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[-1].attrlist)) {
        ((yyvsp[-1].attrlist))->AddAttribute(Attribute("::", (yyvsp[0].str), CHART_POS((yylsp[0])), CHART_POS((yylsp[0])).IncStartCol()), &chart);
        (yyval.attrlist) = (yyvsp[-1].attrlist);
    } else {
        (yyval.attrlist) = new GraphStyle(EStyleType::STYLE);
        (yyval.attrlist)->AddAttribute(Attribute("::", (yyvsp[0].str), CHART_POS((yylsp[0])), CHART_POS((yylsp[0])).IncStartCol()), &chart);
    }
  #endif
    free((yyvsp[0].str));
}
#line 4934 "gv_csh_lang.cc"
    break;

  case 173: /* full_attrlist_with_label: full_attrlist  */
#line 2350 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].attrlist)) {
        (yyval.attrlist) = (yyvsp[0].attrlist);
    } else {
        (yyval.attrlist) = new GraphStyle(EStyleType::STYLE);
    }
  #endif
}
#line 4949 "gv_csh_lang.cc"
    break;

  case 175: /* full_attrlist: full_attrlist attrlist  */
#line 2364 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHintsInsideGraph();
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].attrlist)) {
        if ((yyvsp[0].attrlist)) {
            *(yyvsp[-1].attrlist) += *(yyvsp[0].attrlist);
            (yyval.attrlist) = (yyvsp[-1].attrlist);
            delete (yyvsp[0].attrlist);
        } else {
            (yyval.attrlist) = (yyvsp[-1].attrlist);
        }
    } else
        (yyval.attrlist) = (yyvsp[0].attrlist);
  #endif
}
#line 4973 "gv_csh_lang.cc"
    break;

  case 176: /* full_attrlist: full_attrlist error attrlist  */
#line 2384 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
    csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::LINE_START);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
    if ((yyvsp[-2].attrlist)) {
        if ((yyvsp[0].attrlist)) {
            *(yyvsp[-2].attrlist) += *(yyvsp[0].attrlist);
            (yyval.attrlist) = (yyvsp[-2].attrlist);
            delete (yyvsp[0].attrlist);
        } else {
            (yyval.attrlist) = (yyvsp[-2].attrlist);
        }
    } else
        (yyval.attrlist) = (yyvsp[0].attrlist);
  #endif
}
#line 4996 "gv_csh_lang.cc"
    break;

  case 177: /* attrlist: '[' attrdefs ']'  */
#line 2405 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
	csh.SqBracketPairs.push_back((yylsp[-2]) + (yylsp[0]));
  #else
	(yyval.attrlist) = (yyvsp[-1].attrlist);
  #endif
}
#line 5012 "gv_csh_lang.cc"
    break;

  case 178: /* attrlist: '[' attrdefs error ']'  */
#line 2417 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::ATTR_NAME);
    csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
	csh.SqBracketPairs.push_back((yylsp[-3]) + (yylsp[0]));
    csh.AddCSH_Error((yylsp[-1]), "Missing an attribute here ('name = value').");
  #else
	(yyval.attrlist) = (yyvsp[-2].attrlist);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing an attribute here ('name = value').");
  #endif
}
#line 5030 "gv_csh_lang.cc"
    break;

  case 179: /* attrlist: '[' attrdefs  */
#line 2431 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
    csh.CheckHintBetween((yylsp[0]), yylloc, EHintSourceType::ATTR_NAME);
	csh.SqBracketPairs.push_back((yylsp[-1]) + yylloc);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a closing bracket (']').");
  #else
	(yyval.attrlist) = (yyvsp[0].attrlist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 5047 "gv_csh_lang.cc"
    break;

  case 180: /* attrlist: '[' stylename  */
#line 2444 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME) && !csh.SkipContent())
        csh.AddStylesToHints(false, false); //leave in HINT_LOCATED to continue adding attr names
	csh.SqBracketPairs.push_back((yylsp[-1]) + yylloc);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a closing bracket (']').");
  #else
    (yyval.attrlist) = new GraphStyle(EStyleType::STYLE);
    if (!chart.SkipContent()) {
        auto i = chart.MyCurrentContext().styles.find((yyvsp[0].str));
        if (i!=chart.MyCurrentContext().styles.end())
            *(yyval.attrlist) += i->second.read();
        else
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Unknown style. Ignoring.");
    }
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
	free((yyvsp[0].str));
}
#line 5072 "gv_csh_lang.cc"
    break;

  case 181: /* attrlist: '[' stylename ']'  */
#line 2465 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME) && !csh.SkipContent())
        csh.AddStylesToHints(false, false); //leave in HINT_LOCATED to continue adding attr names
	csh.SqBracketPairs.push_back((yylsp[-2]) + (yylsp[0]));
  #else
    (yyval.attrlist) = new GraphStyle(EStyleType::STYLE);
    if (!chart.SkipContent()) {
        auto i = chart.MyCurrentContext().styles.find((yyvsp[-1].str));
        if (i!=chart.MyCurrentContext().styles.end())
            *(yyval.attrlist) += i->second.read();
        else
            chart.Error.Error(CHART_POS_START((yylsp[-1])), "Unknown style. Ignoring.");
    }
  #endif
	free((yyvsp[-1].str));
}
#line 5096 "gv_csh_lang.cc"
    break;

  case 182: /* attrlist: '[' TOK_EOF  */
#line 2485 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME))
        csh.AddStylesToHints(false, false); //leave in HINT_LOCATED to continue adding attr names
	csh.SqBracketPairs.push_back((yylsp[-1])+(yylsp[0]));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing bracket (']').");
  #else
	(yyval.attrlist) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing ']'.");
  #endif
}
#line 5113 "gv_csh_lang.cc"
    break;

  case 183: /* attrlist: '[' ']'  */
#line 2498 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME))
        csh.AddStylesToHints(false, false); //leave in HINT_LOCATED to continue adding attr names
	csh.SqBracketPairs.push_back((yylsp[-1])+(yylsp[0]));
  #else
	(yyval.attrlist) = nullptr;
  #endif
}
#line 5129 "gv_csh_lang.cc"
    break;

  case 184: /* attrlist: '['  */
#line 2510 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME))
        csh.AddStylesToHints(false, false); //leave in HINT_LOCATED to continue adding attr names
	csh.SqBracketPairs.push_back((yylsp[0])+yylloc);
  #else
	(yyval.attrlist) = nullptr;
  #endif
}
#line 5144 "gv_csh_lang.cc"
    break;

  case 185: /* attrlist: '[' error ']'  */
#line 2521 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    if (csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME))
        csh.AddStylesToHints(false, false); //leave in HINT_LOCATED to continue adding attr names
	csh.SqBracketPairs.push_back((yylsp[-2])+(yylsp[0]));
    csh.AddCSH_Error((yylsp[-1]), "Missing an attribute here ('name = value').");
  #else
	(yyval.attrlist) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing an attribute here ('name = value').");
  #endif
}
#line 5162 "gv_csh_lang.cc"
    break;

  case 186: /* colon_string: TOK_COLON_QUOTED_STRING  */
#line 2536 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), false, true);
    csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 5174 "gv_csh_lang.cc"
    break;

  case 187: /* colon_string: TOK_COLON_STRING  */
#line 2544 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), true, true);
	csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 5186 "gv_csh_lang.cc"
    break;

  case 188: /* attrdefs: attrdefs attrdef  */
#line 2554 "dot.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].attr)) {
	((yyvsp[-1].attrlist))->AddAttribute(*(yyvsp[0].attr), &chart);
	delete (yyvsp[0].attr);
    }
    (yyval.attrlist) = (yyvsp[-1].attrlist);
  #endif
}
#line 5200 "gv_csh_lang.cc"
    break;

  case 189: /* attrdefs: attrdefs string  */
#line 2564 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[0]), (yyvsp[0].multi_str).str, COLOR_ATTRNAME);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing equal sign ('=') to continue an attribute definition.");
    (yyval.attrlist) = (yyvsp[-1].attrlist);
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 5216 "gv_csh_lang.cc"
    break;

  case 190: /* attrdefs: attrdefs error attrdef  */
#line 2575 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
	if ((yyvsp[0].attr)) {
		((yyvsp[-2].attrlist))->AddAttribute(*(yyvsp[0].attr), &chart);
		delete (yyvsp[0].attr);
	}
	(yyval.attrlist) = (yyvsp[-2].attrlist);
  #endif
}
#line 5233 "gv_csh_lang.cc"
    break;

  case 191: /* attrdefs: attrdef  */
#line 2588 "dot.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.attrlist) = new GraphStyle(EStyleType::STYLE);
    if ((yyvsp[0].attr)) {
        ((yyval.attrlist))->AddAttribute(*(yyvsp[0].attr), &chart);
	    delete (yyvsp[0].attr);
    }
  #endif
}
#line 5247 "gv_csh_lang.cc"
    break;

  case 193: /* attrdef: attritem error optseparator  */
#line 2600 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
    (yyval.attr) = (yyvsp[-2].attr);
  #endif
}
#line 5260 "gv_csh_lang.cc"
    break;

  case 194: /* attritem: string TOK_EQUAL string  */
#line 2610 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        csh.AddCSH_AttrValue((yylsp[0]), (yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, COLOR_ATTRVALUE);
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error ||
        (((yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param) && chart.SkipContent()))
        (yyval.attr) = nullptr;
    else
        (yyval.attr) = new Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yyloc)), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 5285 "gv_csh_lang.cc"
    break;

  case 195: /* attritem: string TOK_EQUAL  */
#line 2631 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yyloc), "Missing attribute value.");
    csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing attribute value.");
    (yyval.attr) = nullptr;
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 5305 "gv_csh_lang.cc"
    break;

  case 196: /* comp: TOK_EDGEOP  */
#line 2648 "dot.yy"
{
    if ((yyvsp[0].edgetype).type==GraphEdgeType::DOUBLE && (yyvsp[0].edgetype).dir==GraphEdgeType::NO_ARROW)
        (yyval.compare_op) = ECompareOperator::EQUAL;
    else if ((yyvsp[0].edgetype).type==GraphEdgeType::DOUBLE && (yyvsp[0].edgetype).dir==GraphEdgeType::FWD)
        (yyval.compare_op) = ECompareOperator::GREATER_OR_EQUAL;
    else if ((yyvsp[0].edgetype).type==GraphEdgeType::DOUBLE && (yyvsp[0].edgetype).dir==GraphEdgeType::BACK)
        (yyval.compare_op) = ECompareOperator::SMALLER_OR_EQUAL;
    else if ((yyvsp[0].edgetype).type==GraphEdgeType::DOTTED && (yyvsp[0].edgetype).dir==GraphEdgeType::FWD)
        (yyval.compare_op) = ECompareOperator::GREATER;
    else if ((yyvsp[0].edgetype).type==GraphEdgeType::DOTTED && (yyvsp[0].edgetype).dir==GraphEdgeType::BACK)
        (yyval.compare_op) = ECompareOperator::SMALLER;
    else if ((yyvsp[0].edgetype).type==GraphEdgeType::DOTTED && (yyvsp[0].edgetype).dir==GraphEdgeType::BIDIR)
        (yyval.compare_op) = ECompareOperator::NOT_EQUAL;
    else
        (yyval.compare_op) = ECompareOperator::INVALID;
}
#line 5326 "gv_csh_lang.cc"
    break;

  case 197: /* condition: string  */
#line 2666 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #endif
    (yyval.condition) = (yyvsp[0].multi_str).had_error ? 2 : (yyvsp[0].multi_str).str && (yyvsp[0].multi_str).str[0];
    free((yyvsp[0].multi_str).str);
}
#line 5338 "gv_csh_lang.cc"
    break;

  case 198: /* condition: string comp  */
#line 2674 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to compare to.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to compare to.");
  #endif
    (yyval.condition) = 2;
    free((yyvsp[-1].multi_str).str);
    (yyvsp[0].compare_op); //to suppress
}
#line 5355 "gv_csh_lang.cc"
    break;

  case 199: /* condition: string comp string  */
#line 2687 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    if ((yyvsp[-1].compare_op)!=ECompareOperator::INVALID) {
        csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
        (yyval.condition) = (yyvsp[-2].multi_str).Compare((yyvsp[-1].compare_op), (yyvsp[0].multi_str));
    } else {
        csh.AddCSH_Error((yylsp[-1]), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        (yyval.condition) = 2;
    }
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if ((yyvsp[-1].compare_op)!=ECompareOperator::INVALID)
        (yyval.condition) = (yyvsp[-2].multi_str).Compare((yyvsp[-1].compare_op), (yyvsp[0].multi_str));
    else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        (yyval.condition) = 2;
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 5382 "gv_csh_lang.cc"
    break;

  case 200: /* condition: string error string  */
#line 2710 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    csh.AddCSH_Error((yylsp[-1]), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
     chart.Error.Error(CHART_POS_START((yylsp[-1])), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
    (yyval.condition) = 2;
}
#line 5399 "gv_csh_lang.cc"
    break;

  case 201: /* ifthen_condition: TOK_IF condition TOK_THEN  */
#line 2724 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = (yyvsp[-1].condition);
    const bool cond_true = (yyvsp[-1].condition)==1;
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (cond_true)
        csh.PushContext();
    else
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = (yyvsp[-1].condition);
    const bool cond_true = (yyvsp[-1].condition)==1;
    if (cond_true)
        chart.PushContext(CHART_POS_START((yylsp[-2])));
    else
        chart.PushContext(CHART_POS_START((yylsp[-2])), EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_true;
    chart.MyCurrentContext().export_styles = cond_true;
  #endif
}
#line 5429 "gv_csh_lang.cc"
    break;

  case 202: /* ifthen_condition: TOK_IF condition  */
#line 2750 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'then' keyword.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "then",
            "Continue the 'if' statement with 'then'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-1])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing 'then' keyword.");
  #endif
    (yyvsp[0].condition); //to supress warnings
}
#line 5453 "gv_csh_lang.cc"
    break;

  case 203: /* ifthen_condition: TOK_IF  */
#line 2770 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing condition.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing condition after 'if'.");
  #endif
}
#line 5470 "gv_csh_lang.cc"
    break;

  case 204: /* ifthen_condition: TOK_IF error  */
#line 2783 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[0]), "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-1])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing condition after 'if'.");
  #endif
}
#line 5487 "gv_csh_lang.cc"
    break;

  case 205: /* ifthen_condition: TOK_IF error TOK_THEN  */
#line 2796 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[-1]), "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-2])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing condition after 'if'.");
  #endif
}
#line 5508 "gv_csh_lang.cc"
    break;

  case 206: /* else: TOK_ELSE  */
#line 2815 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
    const bool cond_false = csh.Contexts.back().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    (yyval.condition) = csh.Contexts.back().if_condition;
    if (cond_false)
        csh.PushContext();
    else
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    //kill previous context and open new one - set laterreparse if error or if condition was true
    //this will ignore everything in the else clause
    chart.PopContext();
    const bool cond_false = chart.MyCurrentContext().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    (yyval.condition) = chart.MyCurrentContext().if_condition;
    if (cond_false)
        chart.PushContext(CHART_POS_START((yylsp[0])));
    else
        chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_false;
    chart.MyCurrentContext().export_styles = cond_false;
  #endif
}
#line 5543 "gv_csh_lang.cc"
    break;

  case 207: /* ifthen: ifthen_condition instruction_no_semi  */
#line 2847 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.PopContext();
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "else",
            "Continue the 'if/then' statement with 'else'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].condition)==1) {
    } else {
    }
    chart.PopContext();
  #endif
}
#line 5565 "gv_csh_lang.cc"
    break;

  case 208: /* ifthen: ifthen_condition  */
#line 2865 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].condition)!=2)
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'then'.");
    csh.PopContext();
  #else
    if ((yyvsp[0].condition)!=2)
        chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a well-formed command after 'then'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[0].condition); //suppress
}
#line 5582 "gv_csh_lang.cc"
    break;

  case 209: /* ifthen: ifthen_condition error  */
#line 2878 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Missing command after 'then'.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing a well-formed command. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-1].condition); //suppress
}
#line 5597 "gv_csh_lang.cc"
    break;

  case 210: /* ifthen: ifthen_condition instruction_no_semi else  */
#line 2889 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-1]));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-2].condition); (yyvsp[0].condition); //suppress
}
#line 5613 "gv_csh_lang.cc"
    break;

  case 211: /* ifthen: ifthen_condition instruction_no_semi error else  */
#line 2901 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-2]));
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "I am not sure what is coming here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-3].condition); (yyvsp[0].condition); //suppress
}
#line 5631 "gv_csh_lang.cc"
    break;

  case 212: /* ifthen: ifthen_condition error else  */
#line 2915 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "I am not sure what is coming here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-2].condition); (yyvsp[0].condition); //suppress
}
#line 5648 "gv_csh_lang.cc"
    break;

  case 213: /* ifthen: ifthen_condition instruction_no_semi else instruction_no_semi  */
#line 2928 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-2]));
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.PopContext();
  #else
    switch ((yyvsp[-3].condition)) {
    case 1: //original condition was true
        break;
    case 0: //original condition was false
        break;
    default:
        _ASSERT(0);
        FALLTHROUGH;
    case 2: //we had an error, but has reported the error - delete both branches
        break;
    }
    chart.PopContext();
  #endif
    (yyvsp[-1].condition); //suppress
}
#line 5674 "gv_csh_lang.cc"
    break;

  case 214: /* ifthen: ifthen_condition instruction_no_semi error else instruction_no_semi  */
#line 2950 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-3]));
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.AddCSH_Error((yylsp[-2]), "I am not sure what is coming here.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    (yyvsp[-4].condition); (yyvsp[-1].condition); //suppress
}
#line 5691 "gv_csh_lang.cc"
    break;

  case 215: /* ifthen: ifthen_condition error else instruction_no_semi  */
#line 2963 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.AddCSH_Error((yylsp[-2]), "I am not sure what is coming here.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    (yyvsp[-3].condition); (yyvsp[-1].condition); //suppress
}
#line 5707 "gv_csh_lang.cc"
    break;

  case 217: /* subgraph_attrs: full_attrlist_with_label  */
#line 2980 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE)) {
        csh.AttributeValues(GraphStyle::CLUSTER);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME)) {
        csh.AttributeNames(GraphStyle::CLUSTER);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.last_subgraphattrs += *(yyvsp[0].attrlist);
  #endif
}
#line 5725 "gv_csh_lang.cc"
    break;

  case 218: /* subgraph: subghdr body  */
#line 2995 "dot.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.nodelist) = (yyvsp[0].nodelist);
  #endif
}
#line 5735 "gv_csh_lang.cc"
    break;

  case 219: /* subgraph: subghdr error body  */
#line 3001 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
    (yyval.nodelist) = (yyvsp[0].nodelist);
  #endif
}
#line 5748 "gv_csh_lang.cc"
    break;

  case 220: /* subgraph: subghdr subgraph_attrs body  */
#line 3010 "dot.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.nodelist) = (yyvsp[0].nodelist);
  #endif
}
#line 5758 "gv_csh_lang.cc"
    break;

  case 221: /* subgraph: subghdr error subgraph_attrs body  */
#line 3016 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-2]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "Syntax error.");
    (yyval.nodelist) = (yyvsp[0].nodelist);
  #endif
}
#line 5771 "gv_csh_lang.cc"
    break;

  case 222: /* subgraph: subghdr subgraph_attrs error body  */
#line 3025 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
    (yyval.nodelist) = (yyvsp[0].nodelist);
  #endif
}
#line 5784 "gv_csh_lang.cc"
    break;

  case 223: /* subgraph: subghdr error subgraph_attrs error body  */
#line 3034 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-3]), "Syntax error.");
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-3])), "Syntax error.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
    (yyval.nodelist) = (yyvsp[0].nodelist);
  #endif
}
#line 5799 "gv_csh_lang.cc"
    break;

  case 224: /* subgraph: subghdr subgraph_attrs error  */
#line 3045 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Syntax error. Expecting a '{' to specify subgraph content.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Syntax error. Expecting a '{' to specify subgraph content.");
    (yyval.nodelist) = nullptr;
  #endif
}
#line 5812 "gv_csh_lang.cc"
    break;

  case 225: /* subgraph: subghdr error subgraph_attrs  */
#line 3054 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-2]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Syntax error.");
    (yyval.nodelist) = nullptr;
  #endif
}
#line 5825 "gv_csh_lang.cc"
    break;

  case 226: /* subgraph: subghdr error subgraph_attrs error  */
#line 3063 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-3]), "Syntax error.");
    csh.AddCSH_Error((yylsp[0]), "Syntax error. Expecting a '{' to specify subgraph content.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Syntax error. Expecting a '{' to specify subgraph content.");
    (yyval.nodelist) = nullptr;
  #endif
}
#line 5840 "gv_csh_lang.cc"
    break;

  case 227: /* subghdr: TOK_SUBGRAPH string_or_reserved_word  */
#line 3075 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error) {
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
        csh.AddCSH_Subgraphname((yylsp[0]), (yyvsp[0].multi_str).str);
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
	    chart.last_subgraphname.clear();
    else
        chart.last_subgraphname = (yyvsp[0].multi_str).str;
    chart.last_subgraphspos = CHART_POS((yyloc));
    chart.last_subgraphattrs.Empty();
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 5861 "gv_csh_lang.cc"
    break;

  case 228: /* subghdr: TOK_SUBGRAPH  */
#line 3092 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
    chart.last_subgraphname.clear();
    chart.last_subgraphspos = CHART_POS((yyloc));
    chart.last_subgraphattrs.Empty();
  #endif
}
#line 5875 "gv_csh_lang.cc"
    break;

  case 229: /* subghdr: TOK_CLUSTER  */
#line 3102 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
    chart.last_subgraphname = std::string("cluster_")+std::to_string(chart.cluser_counter++);
    chart.last_subgraphspos = CHART_POS((yyloc));
    chart.last_subgraphattrs.Empty();
    chart.last_subgraphattrs.AddAttribute(Attribute("label", "", FileLineColRange(), FileLineColRange()), &chart);
  #endif
}
#line 5890 "gv_csh_lang.cc"
    break;

  case 230: /* subghdr: TOK_CLUSTER string_or_reserved_word  */
#line 3113 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE_EMPH);
  #else
    chart.last_subgraphattrs.Empty();
    if ((yyvsp[0].multi_str).had_error) {
        chart.last_subgraphname = std::string("cluster_")+std::to_string(chart.cluser_counter++);
        chart.last_subgraphattrs.AddAttribute(Attribute("label", "", FileLineColRange(), FileLineColRange()), &chart);
    } else {
        chart.last_subgraphname = std::string("cluster_")+(yyvsp[0].multi_str).str;
        chart.last_subgraphattrs.AddAttribute(Attribute("label", (yyvsp[0].multi_str).str, FileLineColRange(), CHART_POS((yylsp[0]))), &chart);
    }
    chart.last_subgraphspos = CHART_POS((yyloc));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 5913 "gv_csh_lang.cc"
    break;

  case 231: /* optseparator: TOK_SEMICOLON  */
#line 3134 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_COMMA); //even semicolons colored like this in option list separators
  #endif
}
#line 5923 "gv_csh_lang.cc"
    break;

  case 232: /* optseparator: TOK_COMMA  */
#line 3140 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yyloc), COLOR_COMMA);
  #endif
}
#line 5933 "gv_csh_lang.cc"
    break;

  case 236: /* reserved_word: TOK_NODE  */
#line 3149 "dot.yy"
                        { (yyval.str) = strdup("node"); }
#line 5939 "gv_csh_lang.cc"
    break;

  case 237: /* reserved_word: TOK_EDGE  */
#line 3150 "dot.yy"
                        { (yyval.str) = strdup("edge"); }
#line 5945 "gv_csh_lang.cc"
    break;

  case 238: /* reserved_word: TOK_STRICT  */
#line 3151 "dot.yy"
                          { (yyval.str) = strdup("strict"); }
#line 5951 "gv_csh_lang.cc"
    break;

  case 239: /* reserved_word: TOK_SUBGRAPH  */
#line 3152 "dot.yy"
                            { (yyval.str) = strdup("subgraph"); }
#line 5957 "gv_csh_lang.cc"
    break;

  case 240: /* reserved_word: TOK_DEFSTYLE  */
#line 3153 "dot.yy"
                            { (yyval.str) = strdup("defstyle"); }
#line 5963 "gv_csh_lang.cc"
    break;

  case 241: /* reserved_word: TOK_DEFDESIGN  */
#line 3154 "dot.yy"
                             { (yyval.str) = strdup("defdesign"); }
#line 5969 "gv_csh_lang.cc"
    break;

  case 242: /* reserved_word: TOK_USEDESIGN  */
#line 3155 "dot.yy"
                             { (yyval.str) = strdup("usedesign"); }
#line 5975 "gv_csh_lang.cc"
    break;

  case 243: /* reserved_word: TOK_GRAPH  */
#line 3156 "dot.yy"
                         { (yyval.str) = strdup("graph"); }
#line 5981 "gv_csh_lang.cc"
    break;

  case 244: /* reserved_word: TOK_DIGRAPH  */
#line 3157 "dot.yy"
                           { (yyval.str) = strdup("digraph"); }
#line 5987 "gv_csh_lang.cc"
    break;

  case 245: /* reserved_word: TOK_CLUSTER  */
#line 3158 "dot.yy"
                           { (yyval.str) = strdup("cluster"); }
#line 5993 "gv_csh_lang.cc"
    break;

  case 246: /* reserved_word: TOK_IF  */
#line 3159 "dot.yy"
                      { (yyval.str) = strdup("if"); }
#line 5999 "gv_csh_lang.cc"
    break;

  case 247: /* reserved_word: TOK_THEN  */
#line 3160 "dot.yy"
                        { (yyval.str) = strdup("then"); }
#line 6005 "gv_csh_lang.cc"
    break;

  case 248: /* reserved_word: TOK_ELSE  */
#line 3161 "dot.yy"
                        { (yyval.str) = strdup("else"); }
#line 6011 "gv_csh_lang.cc"
    break;

  case 249: /* reserved_word: TOK_COMMAND_DEFPROC  */
#line 3162 "dot.yy"
                                   { (yyval.str) = strdup("defproc"); }
#line 6017 "gv_csh_lang.cc"
    break;

  case 250: /* reserved_word: TOK_COMMAND_REPLAY  */
#line 3163 "dot.yy"
                                  { (yyval.str) = strdup("replay"); }
#line 6023 "gv_csh_lang.cc"
    break;

  case 251: /* reserved_word: TOK_COMMAND_SET  */
#line 3164 "dot.yy"
                               { (yyval.str) = strdup("set"); }
#line 6029 "gv_csh_lang.cc"
    break;

  case 252: /* reserved_word: TOK_BYE  */
#line 3165 "dot.yy"
                       { (yyval.str) = strdup("bye"); }
#line 6035 "gv_csh_lang.cc"
    break;

  case 255: /* tok_param_name_as_multi: TOK_PARAM_NAME  */
#line 3170 "dot.yy"
{
    (yyval.multi_str).str = nullptr;
    (yyval.multi_str).multi = false;
    (yyval.multi_str).had_param = true;
    (yyval.multi_str).had_error = false;
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].str)==nullptr || (yyvsp[0].str)[0]!='$' || (yyvsp[0].str)[1]==0)
        csh.AddCSH_Error((yylsp[0]), "Need name after the '$' sign.");
  #else
    if ((yyvsp[0].str)==nullptr || (yyvsp[0].str)[0]!='$' || (yyvsp[0].str)[1]==0) {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Need name after the '$' sign.");
        (yyval.multi_str).had_error = true;
    } else if (!chart.SkipContent()) {
        //When parsing a procedure we we accept all params
        //as they may be variables defined later
        auto p = chart.GetParameter((yyvsp[0].str));
        if (p==nullptr) {
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Undefined parameter or variable name.");
            (yyval.multi_str).had_error = true;
        } else {
            (yyval.multi_str).str = strdup(StringFormat::PushPosEscapes(p->value.c_str(), CHART_POS_START((yylsp[0]))).c_str());
        }
    }
  #endif
    //avoid returning null
    if ((yyval.multi_str).str==nullptr)
        (yyval.multi_str).str = strdup("");
    free((yyvsp[0].str));
}
#line 6069 "gv_csh_lang.cc"
    break;

  case 256: /* string_or_reserved_word_or_param: string_single_or_reserved_word  */
#line 3201 "dot.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 6077 "gv_csh_lang.cc"
    break;

  case 258: /* string_or_param: string_single  */
#line 3207 "dot.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 6085 "gv_csh_lang.cc"
    break;

  case 260: /* multi_string_continuation: TOK_TILDE  */
#line 3214 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to concatenate after '~'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to concatenate after '~'.");
  #endif
    (yyval.multi_str) = multi_segment_string("");
}
#line 6098 "gv_csh_lang.cc"
    break;

  case 261: /* multi_string_continuation: TOK_TILDE string_or_reserved_word_or_param  */
#line 3223 "dot.yy"
{
    (yyval.multi_str) = (yyvsp[0].multi_str);
    (yyval.multi_str).multi = true;
}
#line 6107 "gv_csh_lang.cc"
    break;

  case 262: /* multi_string_continuation: multi_string_continuation TOK_TILDE  */
#line 3228 "dot.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to concatenate after '~'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to concatenate after '~'.");
  #endif
    (yyval.multi_str) = (yyvsp[-1].multi_str);
    (yyval.multi_str).had_error = true;
}
#line 6121 "gv_csh_lang.cc"
    break;

  case 263: /* multi_string_continuation: multi_string_continuation TOK_TILDE string_or_reserved_word_or_param  */
#line 3238 "dot.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-2].multi_str), (yyvsp[0].multi_str));
}
#line 6129 "gv_csh_lang.cc"
    break;

  case 264: /* string_or_reserved_word: reserved_word  */
#line 3245 "dot.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 6137 "gv_csh_lang.cc"
    break;

  case 265: /* string_or_reserved_word: reserved_word multi_string_continuation  */
#line 3249 "dot.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), (yyvsp[0].multi_str));
}
#line 6145 "gv_csh_lang.cc"
    break;

  case 268: /* string: string_or_param multi_string_continuation  */
#line 3256 "dot.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].multi_str));
}
#line 6153 "gv_csh_lang.cc"
    break;


#line 6157 "gv_csh_lang.cc"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= TOK_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == TOK_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, RESULT, yyscanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, RESULT, yyscanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, RESULT, yyscanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 3261 "dot.yy"

