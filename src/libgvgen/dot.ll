%option reentrant noyywrap nounput
%option bison-bridge bison-locations
%x comment quoted_string html_string html_string_will_come

%{
#include <stdio.h>
#include <string.h>
#include <iostream>

#ifndef HAVE_UNISTD_H
#define YY_NO_UNISTD_H
extern int isatty(int);
#endif

#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #include "gvcsh.h"
    #include "gvstyle.h"  //for EGraphEdgeType
    using namespace graph;
    #define YYMSC_RESULT_TYPE GraphCsh
    #define RESULT csh
    #define YYLTYPE_IS_DECLARED  //If we scan for color syntax highlight use this location type (YYLTYPE)
    #define YYLTYPE CshPos
    #define YYGET_EXTRA gvcsh_get_extra
    #define CHAR_IF_CSH(A) char
    #include "gv_csh_lang.h"   //Must be after language_misc.h
    #include "gv_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
#else
    #define C_S_H (0)
    #include "graphchart.h"
    using namespace graph;
    #define YYMSC_RESULT_TYPE GraphChart
    #define RESULT chart
    #define YYGET_EXTRA gv_get_extra
    #define CHAR_IF_CSH(A) A
    #include "gv_lang.h"
    #include "parse_tools.h"
    #include "gv_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined

    //this function must be in the lex file, so that lex replaces the "yy" in yy_create_buffer and
    //yypush_buffer_state to the appropriate chart-specific prefix.
    void GraphPushFlex(base_parse_parm &pp, const char *text, size_t len, YYLTYPE *old_pos, const FileLineCol &new_pos, EInclusionReason reason)
    {
        if (text==nullptr || len==0) return;
        pp.buffs.emplace_back(text, len);
        pp.pos_stack.line = old_pos->first_line;
        pp.pos_stack.col = old_pos->first_column-1; //not sure why -1 is needed
        pp.pos_stack.Push(new_pos);
        pp.pos_stack.reason = reason;
        yypush_buffer_state(yy_create_buffer( nullptr, YY_BUF_SIZE, pp.yyscanner ), pp.yyscanner);
        pp.chart->Progress.RegisterParse(len);
    }
#endif

/* The DOT language can have preprocessor directives in the form of
 * #line <num> "file", which act as the preprocessor directives in C.
 * We ignore these for now.*/
%}

LETTER    [A-Za-z_\x80-\xff]
DIGIT	  [0-9]
NAME	  {LETTER}({LETTER}|{DIGIT})*
NUMBER	  [-]?(({DIGIT}+(\.{DIGIT}*)?)|(\.{DIGIT}+))
ID		  ({NAME}|{NUMBER})

%%
<INITIAL,comment,quoted_string,html_string_will_come>\n %{
									  #ifndef C_S_H_IS_COMPILED
                                        language_jump_line(yylloc);
									  #endif
   								  %}
"/*"					BEGIN(comment);
<comment>[^*\n]*		/* eat anything not a '*' */
<comment>"*"+[^*/\n]*	/* eat up '*'s not followed by '/'s */
<comment>"*"+"/"		%{
							BEGIN(INITIAL);
                            #ifdef C_S_H_IS_COMPILED
								YYGET_EXTRA(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
							#endif
						%}
"//".*					%{
                            #ifdef C_S_H_IS_COMPILED
								YYGET_EXTRA(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
							#endif
						%}
^"#".*					%{  //We ignore directives - #s at the beginning of the line are also comments
                            #ifdef C_S_H_IS_COMPILED
								YYGET_EXTRA(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
							#endif
						%}
"#".*					%{
                            #ifdef C_S_H_IS_COMPILED
								YYGET_EXTRA(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
							#endif
						%}
<INITIAL,html_string_will_come>[ \t\r]					/* ignore whitespace */
<INITIAL,html_string_will_come>"\xEF\xBB\xBF"				/* ignore BOM */
"node"					return TOK_NODE;
"edge"					return TOK_EDGE;
"graph"					yylval->boolean = false; return TOK_GRAPH;
"digraph"				yylval->boolean = true;  return TOK_DIGRAPH;
"strict"				return TOK_STRICT;
"subgraph"				return TOK_SUBGRAPH;
"cluster"				return TOK_CLUSTER;
"defdesign"             return TOK_DEFDESIGN;
"defstyle"              return TOK_DEFSTYLE;
"usedesign"             return TOK_USEDESIGN;
"defproc"               return TOK_COMMAND_DEFPROC;
"replay"                return TOK_COMMAND_REPLAY;
"set"                   return TOK_COMMAND_SET; 
"include"               return TOK_COMMAND_INCLUDE;
"if"                    return TOK_IF;
"then"                  return TOK_THEN;
"else"                  return TOK_ELSE;
"->"					yylval->edgetype = {GraphEdgeType::SOLID, GraphEdgeType::FWD}; return TOK_EDGEOP;
"<->"					yylval->edgetype = {GraphEdgeType::SOLID, GraphEdgeType::BIDIR}; return TOK_EDGEOP;
"<-"					yylval->edgetype = {GraphEdgeType::SOLID, GraphEdgeType::BACK}; return TOK_EDGEOP;
"--"				    yylval->edgetype = {GraphEdgeType::SOLID, GraphEdgeType::NO_ARROW}; return TOK_EDGEOP;
"=>"					yylval->edgetype = {GraphEdgeType::DOUBLE, GraphEdgeType::FWD}; return TOK_EDGEOP;
"<=>"					yylval->edgetype = {GraphEdgeType::DOUBLE, GraphEdgeType::BIDIR}; return TOK_EDGEOP;
"<="					yylval->edgetype = {GraphEdgeType::DOUBLE, GraphEdgeType::BACK}; return TOK_EDGEOP;
"=="				    yylval->edgetype = {GraphEdgeType::DOUBLE, GraphEdgeType::NO_ARROW}; return TOK_EDGEOP;
">>"					yylval->edgetype = {GraphEdgeType::DASHED, GraphEdgeType::FWD}; return TOK_EDGEOP;
"<<>>"					yylval->edgetype = {GraphEdgeType::DASHED, GraphEdgeType::BIDIR}; return TOK_EDGEOP;
"<<"					yylval->edgetype = {GraphEdgeType::DASHED, GraphEdgeType::BACK}; return TOK_EDGEOP;
"++"				    yylval->edgetype = {GraphEdgeType::DASHED, GraphEdgeType::NO_ARROW}; return TOK_EDGEOP;
">"     				yylval->edgetype = {GraphEdgeType::DOTTED, GraphEdgeType::FWD}; return TOK_EDGEOP;
"<>"					yylval->edgetype = {GraphEdgeType::DOTTED, GraphEdgeType::BIDIR}; return TOK_EDGEOP;
"<" 					yylval->edgetype = {GraphEdgeType::DOTTED, GraphEdgeType::BACK}; return TOK_EDGEOP;
".."				    yylval->edgetype = {GraphEdgeType::DOTTED, GraphEdgeType::NO_ARROW}; return TOK_EDGEOP;
"->>"					yylval->edgetype = {GraphEdgeType::SOLID2, GraphEdgeType::FWD}; return TOK_EDGEOP;
"<<->>"					yylval->edgetype = {GraphEdgeType::SOLID2, GraphEdgeType::BIDIR}; return TOK_EDGEOP;
"<<-"					yylval->edgetype = {GraphEdgeType::SOLID2, GraphEdgeType::BACK}; return TOK_EDGEOP;
"---"				    yylval->edgetype = {GraphEdgeType::SOLID2, GraphEdgeType::NO_ARROW}; return TOK_EDGEOP;
"==>"					yylval->edgetype = {GraphEdgeType::DOUBLE2, GraphEdgeType::FWD}; return TOK_EDGEOP;
"<==>"					yylval->edgetype = {GraphEdgeType::DOUBLE2, GraphEdgeType::BIDIR}; return TOK_EDGEOP;
"<=="					yylval->edgetype = {GraphEdgeType::DOUBLE2, GraphEdgeType::BACK}; return TOK_EDGEOP;
"==="				    yylval->edgetype = {GraphEdgeType::DOUBLE2, GraphEdgeType::NO_ARROW}; return TOK_EDGEOP;
"="                     return TOK_EQUAL;
";"                     return TOK_SEMICOLON;
\+|\~                   return TOK_TILDE;
\,                      return TOK_COMMA;
\(                      return TOK_OPARENTHESIS;
\)                      return TOK_CPARENTHESIS;
\{       %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->input_text_ptr = nullptr;
  #else
    yylval_param->input_text_ptr = yyget_extra(yyscanner)->buffs.back().GetInputPosFrom_yytext(yytext);
  #endif
    return TOK_OCBRACKET;
%}

\}       %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->input_text_ptr = nullptr;
  #else
    yylval_param->input_text_ptr = yyget_extra(yyscanner)->buffs.back().GetInputPosFrom_yytext(yytext);
  #endif
    return TOK_CCBRACKET;
%}

\=/[ \t\r\n]*\<         BEGIN(html_string_will_come); return TOK_EQUAL; //equal sign followed by a html label
{NAME}					{ yylval->str = strdup(yytext); return TOK_STRING; }
{NUMBER}				{ yylval->str = strdup(yytext); return TOK_STRING; }
["]						BEGIN(quoted_string); YYGET_EXTRA(yyscanner)->str.clear(); YYGET_EXTRA(yyscanner)->str_start = *yylloc;
<quoted_string>["]		%{
							BEGIN(INITIAL);
							yylval->str = strdup(YYGET_EXTRA(yyscanner)->str.c_str());
								  #ifdef C_S_H_IS_COMPILED
									yylloc->first_pos = YYGET_EXTRA(yyscanner)->str_start.first_pos;
								  #else
									yylloc->first_line = YYGET_EXTRA(yyscanner)->str_start.first_line;
									yylloc->first_column = YYGET_EXTRA(yyscanner)->str_start.first_column;
                                  #endif
							return TOK_QSTRING;
						%}
<quoted_string>[\\]["]		YYGET_EXTRA(yyscanner)->str += '\"';
<quoted_string>[\\][\\]		YYGET_EXTRA(yyscanner)->str += "\\\\";
<quoted_string>[\\][\n]		%{ //Ignore escaped newlines - this results in no new line in the quoted string!!
								  #ifndef C_S_H_IS_COMPILED
									yylloc->last_line = yylloc->first_line+1;
									yylloc->last_column=1; //yacc column numbering starts at 1
								  #endif
   							%}
<quoted_string>([^"\\]*|[\\])		YYGET_EXTRA(yyscanner)->str += yytext;

<html_string_will_come>[<]	%{
                                    BEGIN(html_string);
                                    YYGET_EXTRA(yyscanner)->html_nest = 1
                                    ; YYGET_EXTRA(yyscanner)->str.clear();
                                    YYGET_EXTRA(yyscanner)->str_start = *yylloc;
                            %}
<html_string>[>]	%{
						YYGET_EXTRA(yyscanner)->html_nest--;
						if (YYGET_EXTRA(yyscanner)->html_nest) YYGET_EXTRA(yyscanner)->str += yytext;
						else {
							BEGIN(INITIAL);
							yylval->str = strdup(YYGET_EXTRA(yyscanner)->str.c_str());
								  #ifdef C_S_H_IS_COMPILED
									yylloc->first_pos = YYGET_EXTRA(yyscanner)->str_start.first_pos;
								  #else
									yylloc->first_line = YYGET_EXTRA(yyscanner)->str_start.first_line;
									yylloc->first_column = YYGET_EXTRA(yyscanner)->str_start.first_column;
                                  #endif
							return TOK_QSTRING;
						}
					%}
<html_string>[<]			YYGET_EXTRA(yyscanner)->html_nest++; YYGET_EXTRA(yyscanner)->str += yytext;
<html_string>[\n]			%{ //Here we do add newlines.
								  #ifndef C_S_H_IS_COMPILED
									yylloc->last_line = yylloc->first_line+1;
									yylloc->last_column=1; //yacc column numbering starts at 1
								  #endif
								  YYGET_EXTRA(yyscanner)->str += yytext;
   							%}
<html_string>([^><\n]*)		YYGET_EXTRA(yyscanner)->str += yytext;
.						return yytext[0];

<<EOF>> %{
  #ifdef C_S_H_IS_COMPILED
    return TOK_EOF;
  #else
    {
    auto &pp = *YYGET_EXTRA(yyscanner);
    if (pp.buffs.size()==0) {
        yyterminate();
        return TOK_EOF;
    }
    pp.buffs.pop_back();
    if (pp.buffs.size()==0) {
        yyterminate();
        return TOK_EOF;
    }
    //else just continue scanning the buffer above
    yypop_buffer_state(yyscanner);
    pp.pos_stack.Pop();
    yylloc->last_line = unsigned(pp.pos_stack.line);
    yylloc->last_column = unsigned(pp.pos_stack.col-1);
    }
  #endif
%}

 /* Parameter names: $ followed by a string not ending with a dot. We allow any non ASCII UTF-8 character through.
  * A single '$" also matches.*/
\$[A-Za-z_\x80-\xff]?([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])* %{
    yylval_param->str = strdup(yytext);
    return TOK_PARAM_NAME;
%}

 /* Parameter names: $$ representing a value unique to the actual invocation.*/
($$) %{
    yylval_param->str = strdup(yytext);
    return TOK_PARAM_NAME;
%}

 /* This is a colon-quoted string, finished by a quotation mark (UTF-8 allowed)
 ** :: "<string>"
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*\" %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    {
    /* after whitespaces we are guaranteed to have a tailing and heading quot */
    char *s = remove_head_tail_whitespace(yytext+2);
    /* s now points to the heading quotation marks.
    ** Now get rid of both quotation marks */
    std::string str(s+1);
    str.erase(str.length()-1);
    /* Calculate the position of the string and prepend a location escape */
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s+1 - yytext));
    yylval_param->str = strdup((pos.Print() + str).c_str());
    }
  #endif
    return TOK_COLON_QUOTED_STRING;
%}

 /* This is a colon-quoted string, finished by a newline (trailing context) (UTF-8 allowed)
 ** :: "<string>$
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*  %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
  #else
    {
    /* after whitespaces we are guaranteed to have a heading quot */
    const char *s = remove_head_tail_whitespace(yytext+2);
    // s now points to heading quotation mark
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s - yytext));
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    /* Advance pos beyond the leading quotation mark */
    pos.col++;
    yylval_param->str = strdup((pos.Print() + (s+1)).c_str());
    }
  #endif
    return TOK_COLON_QUOTED_STRING;
%}

 /* This is a non quoted colon-string
 ** :: <string>
 ** terminated by any of: [ { or ;
 ** Honors escaping of the above via a backslash. (UTF-8 allowed)
 ** Can contain quotation marks (escaped or unescaped), but can not start with it
 ** If it contains a hashmark, unescaped [ { or ; is allowed till the end of the line
 ** (representing a commented section inside a label)
 */
\:\:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc, true);
  #endif
    return TOK_COLON_STRING;
%}


 /* This is a degenerate non quoted colon-string
 ** two colons followed by a solo escape or just a colon
 */
\:\:[ \t]*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
   #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc, true);
  #endif
    return TOK_COLON_STRING;
%}

 /* This rule is redundant here.
. return TOK_UNRECOGNIZED_CHAR;
*/

%%
