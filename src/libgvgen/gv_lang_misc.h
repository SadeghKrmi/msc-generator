/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/* This file is included by parsers both for color syntax and language compilation */

#ifndef GVPARSERHELPER_H
#define GVPARSERHELPER_H

#include <string>
#include "gvgraphs.h"
#include "language_misc.h"




#ifdef C_S_H_IS_COMPILED
    /** Parser and lexer related stuff, the 'extra' for graphs */
    struct gv_parse_parm_csh : public base_parse_parm_csh
    {
        bool               directed = true;  ///<what kind of graph do we parse
        CshPos             last_top_level_command_pos = CshPos(0, 0); ///<The end position of the last graph, defdesign, defstyle, etc. command. Used to see if we need to hint a 'strict' before graph/digraph
        int                html_nest = 0;  ///< nesting level for html strings 
        std::string        str; ///< We collect the results of tokens here, that are made up of multiple lex rules
        YYLTYPE            str_start; ///<We record the starting position of tokens here, that are made up of multiple lex rules
    };

    /** Actual 'parse_param' structure (descendant of base_parse_param), shall be defined by chart.*/
    #define YY_EXTRA_TYPE gv_parse_parm_csh *

#else

    /** Parser and lexer related stuff, the 'extra' for graphs */
    struct gv_parse_parm : public base_parse_parm
    {
        bool               directed = true;  ///<what kind of graph do we parse
        CshPos             last_top_level_command_pos = CshPos(0, 0); ///<The end position of the last graph, defdesign, defstyle, etc. command. Used to see if we need to hint a 'strict' before graph/digraph
        int                html_nest = 0;  ///< nesting level for html strings 
        std::string        str; ///< We collect the results of tokens here, that are made up of multiple lex rules
        YYLTYPE            str_start; ///<We record the starting position of tokens here, that are made up of multiple lex rules
    };

    /** Actual 'parse_param' structure (descendant of base_parse_param), shall be defined by chart.*/
    #define YY_EXTRA_TYPE gv_parse_parm *

    void GraphPushFlex(base_parse_parm &pp, const char *text, size_t len, YYLTYPE *old_pos, const FileLineCol &new_pos, EInclusionReason reason);
#endif 


#endif