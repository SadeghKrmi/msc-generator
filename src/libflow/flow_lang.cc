/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         flow_parse
#define yylex           flow_lex
#define yyerror         flow_error
#define yydebug         flow_debug
#define yynerrs         flow_nerrs

/* First part of user prologue.  */
#line 15 "flow_lang.yy"

/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/* The language of flowcharts is the following
    [<refname>] BOX|IF|START|STOP|STEP: label '['attrs']';
    BRANCH|THEN|ELSE|YES|NO: branch-label '['attrs']' [{ content }]|GOTO <refname>|;
    GOTO <refname>;
    REPEAT { content };
    If you do not specify anything after a branch, the continuation will be used.
*/

#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT //so that subsequent definition in flow_csh_lang2.h causes no warning
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #define YYMSC_RESULT_TYPE FlowCsh
    #define RESULT csh
    #include "cgen_shapes.h"
    #include "flowcsh.h"
    #include "flowchart.h"
    #define YYGET_EXTRA flowcsh_get_extra
    #define YYLTYPE_IS_DECLARED
    #define YYLTYPE CshPos
    #define CHAR_IF_CSH(A) char
#else
    #define C_S_H (0)
    #define YYMSC_RESULT_TYPE FlowChart
    #define RESULT chart
    #define YYGET_EXTRA flow_get_extra
    #define CHAR_IF_CSH(A) A
    #include "cgen_shapes.h"
    #include "flowchart.h"
    #include "steps.h"
#endif

using namespace flow;



#line 133 "flow_lang.cc"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "flow_lang.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* TOK_EOF  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_TOK_STRING = 3,                 /* TOK_STRING  */
  YYSYMBOL_TOK_QSTRING = 4,                /* TOK_QSTRING  */
  YYSYMBOL_TOK_NUMBER = 5,                 /* TOK_NUMBER  */
  YYSYMBOL_TOK_DASH = 6,                   /* TOK_DASH  */
  YYSYMBOL_TOK_EQUAL = 7,                  /* TOK_EQUAL  */
  YYSYMBOL_TOK_COMMA = 8,                  /* TOK_COMMA  */
  YYSYMBOL_TOK_SEMICOLON = 9,              /* TOK_SEMICOLON  */
  YYSYMBOL_TOK_PLUS_PLUS = 10,             /* TOK_PLUS_PLUS  */
  YYSYMBOL_TOK_OCBRACKET = 11,             /* TOK_OCBRACKET  */
  YYSYMBOL_TOK_CCBRACKET = 12,             /* TOK_CCBRACKET  */
  YYSYMBOL_TOK_OSBRACKET = 13,             /* TOK_OSBRACKET  */
  YYSYMBOL_TOK_CSBRACKET = 14,             /* TOK_CSBRACKET  */
  YYSYMBOL_TOK_IF = 15,                    /* TOK_IF  */
  YYSYMBOL_TOK_STEP = 16,                  /* TOK_STEP  */
  YYSYMBOL_TOK_REPEAT = 17,                /* TOK_REPEAT  */
  YYSYMBOL_TOK_START = 18,                 /* TOK_START  */
  YYSYMBOL_TOK_STOP = 19,                  /* TOK_STOP  */
  YYSYMBOL_TOK_GOTO = 20,                  /* TOK_GOTO  */
  YYSYMBOL_TOK_BRANCH = 21,                /* TOK_BRANCH  */
  YYSYMBOL_TOK_THEN = 22,                  /* TOK_THEN  */
  YYSYMBOL_TOK_ELSE = 23,                  /* TOK_ELSE  */
  YYSYMBOL_TOK_YES = 24,                   /* TOK_YES  */
  YYSYMBOL_TOK_NO = 25,                    /* TOK_NO  */
  YYSYMBOL_TOK_SHAPE_COMMAND = 26,         /* TOK_SHAPE_COMMAND  */
  YYSYMBOL_TOK_BYE = 27,                   /* TOK_BYE  */
  YYSYMBOL_TOK_COLON_STRING = 28,          /* TOK_COLON_STRING  */
  YYSYMBOL_TOK_COLON_QUOTED_STRING = 29,   /* TOK_COLON_QUOTED_STRING  */
  YYSYMBOL_TOK_COLORDEF = 30,              /* TOK_COLORDEF  */
  YYSYMBOL_TOK_COMMAND_DEFSHAPE = 31,      /* TOK_COMMAND_DEFSHAPE  */
  YYSYMBOL_TOK_COMMAND_DEFCOLOR = 32,      /* TOK_COMMAND_DEFCOLOR  */
  YYSYMBOL_TOK_COMMAND_DEFSTYLE = 33,      /* TOK_COMMAND_DEFSTYLE  */
  YYSYMBOL_TOK_COMMAND_DEFDESIGN = 34,     /* TOK_COMMAND_DEFDESIGN  */
  YYSYMBOL_TOK__NEVER__HAPPENS = 35,       /* TOK__NEVER__HAPPENS  */
  YYSYMBOL_TOK__NEVER__HAPPENS2 = 36,      /* TOK__NEVER__HAPPENS2  */
  YYSYMBOL_TOK_UNRECOGNIZED_CHAR = 37,     /* TOK_UNRECOGNIZED_CHAR  */
  YYSYMBOL_YYACCEPT = 38,                  /* $accept  */
  YYSYMBOL_chart_with_bye = 39,            /* chart_with_bye  */
  YYSYMBOL_eof = 40,                       /* eof  */
  YYSYMBOL_chart = 41,                     /* chart  */
  YYSYMBOL_top_level_instrlist = 42,       /* top_level_instrlist  */
  YYSYMBOL_braced_instrlist = 43,          /* braced_instrlist  */
  YYSYMBOL_instrlist = 44,                 /* instrlist  */
  YYSYMBOL_several_instructions = 45,      /* several_instructions  */
  YYSYMBOL_instr_with_semicolon = 46,      /* instr_with_semicolon  */
  YYSYMBOL_optlist_with_semicolon = 47,    /* optlist_with_semicolon  */
  YYSYMBOL_instr_needs_semicolon = 48,     /* instr_needs_semicolon  */
  YYSYMBOL_step_types = 49,                /* step_types  */
  YYSYMBOL_actual_instr_needs_semicolon = 50, /* actual_instr_needs_semicolon  */
  YYSYMBOL_actual_instr = 51,              /* actual_instr  */
  YYSYMBOL_goto = 52,                      /* goto  */
  YYSYMBOL_goto_with_semicolon = 53,       /* goto_with_semicolon  */
  YYSYMBOL_branch_with_semicolon = 54,     /* branch_with_semicolon  */
  YYSYMBOL_branch_keyword = 55,            /* branch_keyword  */
  YYSYMBOL_branch_no_content = 56,         /* branch_no_content  */
  YYSYMBOL_optlist = 57,                   /* optlist  */
  YYSYMBOL_opt = 58,                       /* opt  */
  YYSYMBOL_styledeflist = 59,              /* styledeflist  */
  YYSYMBOL_styledef = 60,                  /* styledef  */
  YYSYMBOL_stylenamelist = 61,             /* stylenamelist  */
  YYSYMBOL_shapedef = 62,                  /* shapedef  */
  YYSYMBOL_shapedeflist = 63,              /* shapedeflist  */
  YYSYMBOL_shapeline = 64,                 /* shapeline  */
  YYSYMBOL_colordeflist = 65,              /* colordeflist  */
  YYSYMBOL_color_string = 66,              /* color_string  */
  YYSYMBOL_colordef = 67,                  /* colordef  */
  YYSYMBOL_designdef = 68,                 /* designdef  */
  YYSYMBOL_scope_open_empty = 69,          /* scope_open_empty  */
  YYSYMBOL_designelementlist = 70,         /* designelementlist  */
  YYSYMBOL_designelement = 71,             /* designelement  */
  YYSYMBOL_designoptlist = 72,             /* designoptlist  */
  YYSYMBOL_designopt = 73,                 /* designopt  */
  YYSYMBOL_colon_string = 74,              /* colon_string  */
  YYSYMBOL_full_attrlist_with_label = 75,  /* full_attrlist_with_label  */
  YYSYMBOL_full_attrlist = 76,             /* full_attrlist  */
  YYSYMBOL_attrlist = 77,                  /* attrlist  */
  YYSYMBOL_attr = 78,                      /* attr  */
  YYSYMBOL_entity_string = 79,             /* entity_string  */
  YYSYMBOL_reserved_word_string = 80,      /* reserved_word_string  */
  YYSYMBOL_symbol_string = 81,             /* symbol_string  */
  YYSYMBOL_alpha_string = 82,              /* alpha_string  */
  YYSYMBOL_string = 83,                    /* string  */
  YYSYMBOL_scope_open = 84,                /* scope_open  */
  YYSYMBOL_scope_close = 85                /* scope_close  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;


/* Second part of user prologue.  */
#line 123 "flow_lang.yy"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <iostream>

#ifdef C_S_H_IS_COMPILED
    #include "flow_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "flow_csh_lang2.h"  //Needs parse_param from flow_lang_misc.h
    /* yyerror
     *  Error handling function.  Do nothing for CSH */
    void yyerror(YYLTYPE* /*loc*/, Csh & /*csh*/, void * /*yyscanner*/, const char * /*str*/) {}
#else
    #include "flow_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "flow_lang2.h"      //Needs parse_param from flow_lang_misc.h
    /* Use verbose error reporting such that the expected token names are dumped */
    //#define YYERROR_VERBOSE
    void yyerror(YYLTYPE *loc, YYMSC_RESULT_TYPE &chart, void *yyscanner, const char *str)
    {
        chart.Error.Error(CHART_POS_START(*loc), str);
    }
#endif

#ifdef C_S_H_IS_COMPILED
void FlowCshParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#else
void FlowParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#endif
{
    std::remove_pointer<YY_EXTRA_TYPE>::type pp;
    pp.RESULT = &RESULT;
#ifdef C_S_H_IS_COMPILED
    pp.buff.buf = buff;
    pp.buff.length = len;
    pp.buff.pos = 0;
    flowcsh_lex_init(&pp.yyscanner);
    flowcsh_set_extra(&pp, pp.yyscanner);
    flowcsh_parse(RESULT, pp.yyscanner);
    flowcsh_lex_destroy(pp.yyscanner);
#else
    pp.buffs.emplace_back(buff, len);
    flow_lex_init(&pp.yyscanner);
    flow_set_extra(&pp, pp.yyscanner);
    flow_parse(RESULT, pp.yyscanner);
    flow_lex_destroy(pp.yyscanner);
#endif
}


#line 303 "flow_lang.cc"


#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  79
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   734

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  38
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  48
/* YYNRULES -- Number of rules.  */
#define YYNRULES  189
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  214

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   292


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   176,   176,   180,   191,   192,   201,   213,   224,   237,
     238,   247,   261,   270,   282,   294,   306,   318,   329,   342,
     356,   365,   372,   373,   382,   383,   393,   404,   420,   421,
     422,   435,   452,   456,   463,   474,   487,   491,   501,   516,
     517,   518,   537,   559,   581,   597,   615,   631,   649,   667,
     691,   700,   709,   718,   729,   749,   764,   781,   792,   809,
     820,   834,   849,   861,   872,   885,   901,   920,   931,   948,
     959,   973,   980,   988,   997,  1006,  1015,  1024,  1034,  1044,
    1057,  1071,  1089,  1098,  1113,  1126,  1127,  1143,  1162,  1181,
    1201,  1202,  1212,  1225,  1240,  1250,  1271,  1286,  1310,  1320,
    1332,  1351,  1368,  1387,  1399,  1412,  1424,  1433,  1449,  1477,
    1501,  1530,  1559,  1587,  1617,  1650,  1651,  1661,  1675,  1675,
    1677,  1700,  1724,  1742,  1759,  1798,  1840,  1851,  1852,  1863,
    1877,  1894,  1908,  1925,  1927,  1928,  1938,  1948,  1955,  1974,
    1993,  2011,  2019,  2028,  2036,  2044,  2056,  2064,  2067,  2078,
    2089,  2101,  2114,  2126,  2138,  2152,  2156,  2169,  2176,  2185,
    2196,  2199,  2214,  2229,  2244,  2258,  2272,  2277,  2305,  2306,
    2313,  2323,  2323,  2323,  2323,  2323,  2323,  2323,  2324,  2324,
    2324,  2324,  2327,  2332,  2332,  2334,  2334,  2336,  2351,  2353
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "TOK_EOF", "error", "\"invalid token\"", "TOK_STRING", "TOK_QSTRING",
  "TOK_NUMBER", "TOK_DASH", "TOK_EQUAL", "TOK_COMMA", "TOK_SEMICOLON",
  "TOK_PLUS_PLUS", "TOK_OCBRACKET", "TOK_CCBRACKET", "TOK_OSBRACKET",
  "TOK_CSBRACKET", "TOK_IF", "TOK_STEP", "TOK_REPEAT", "TOK_START",
  "TOK_STOP", "TOK_GOTO", "TOK_BRANCH", "TOK_THEN", "TOK_ELSE", "TOK_YES",
  "TOK_NO", "TOK_SHAPE_COMMAND", "TOK_BYE", "TOK_COLON_STRING",
  "TOK_COLON_QUOTED_STRING", "TOK_COLORDEF", "TOK_COMMAND_DEFSHAPE",
  "TOK_COMMAND_DEFCOLOR", "TOK_COMMAND_DEFSTYLE", "TOK_COMMAND_DEFDESIGN",
  "TOK__NEVER__HAPPENS", "TOK__NEVER__HAPPENS2", "TOK_UNRECOGNIZED_CHAR",
  "$accept", "chart_with_bye", "eof", "chart", "top_level_instrlist",
  "braced_instrlist", "instrlist", "several_instructions",
  "instr_with_semicolon", "optlist_with_semicolon",
  "instr_needs_semicolon", "step_types", "actual_instr_needs_semicolon",
  "actual_instr", "goto", "goto_with_semicolon", "branch_with_semicolon",
  "branch_keyword", "branch_no_content", "optlist", "opt", "styledeflist",
  "styledef", "stylenamelist", "shapedef", "shapedeflist", "shapeline",
  "colordeflist", "color_string", "colordef", "designdef",
  "scope_open_empty", "designelementlist", "designelement",
  "designoptlist", "designopt", "colon_string", "full_attrlist_with_label",
  "full_attrlist", "attrlist", "attr", "entity_string",
  "reserved_word_string", "symbol_string", "alpha_string", "string",
  "scope_open", "scope_close", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-142)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-155)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     670,  -142,  -142,  -142,   -21,  -142,  -142,   109,  -142,  -142,
      66,  -142,  -142,  -142,  -142,  -142,  -142,    26,   694,   599,
      21,    34,    12,  -142,  -142,   644,  -142,  -142,  -142,   338,
     268,    45,  -142,    59,  -142,  -142,    66,    44,   303,  -142,
     715,   478,  -142,   139,  -142,  -142,  -142,    65,    82,    52,
    -142,    37,  -142,    87,  -142,  -142,  -142,  -142,  -142,  -142,
    -142,  -142,  -142,  -142,  -142,    92,  -142,  -142,  -142,    83,
    -142,   102,  -142,    24,  -142,  -142,  -142,    95,  -142,  -142,
    -142,    15,   112,  -142,   670,  -142,  -142,   114,    90,    37,
    -142,  -142,  -142,  -142,    37,  -142,  -142,  -142,   117,   197,
      93,   161,    98,  -142,  -142,  -142,  -142,   373,  -142,   122,
    -142,  -142,   233,  -142,   134,   137,  -142,  -142,    65,  -142,
     408,   694,   503,   599,   599,  -142,  -142,    79,  -142,  -142,
    -142,   110,  -142,  -142,  -142,   111,  -142,  -142,  -142,   162,
    -142,  -142,  -142,  -142,  -142,    23,  -142,  -142,  -142,   168,
     527,  -142,    41,   148,  -142,   158,   180,   443,   177,  -142,
     551,  -142,  -142,  -142,  -142,  -142,   694,   599,   178,  -142,
      88,  -142,   181,  -142,  -142,  -142,  -142,  -142,  -142,  -142,
    -142,   551,  -142,  -142,   182,   184,   190,  -142,   183,  -142,
    -142,    92,   102,   625,  -142,    26,   575,  -142,  -142,    17,
    -142,  -142,   191,  -142,  -142,  -142,  -142,  -142,   185,  -142,
    -142,   189,   199,  -142
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       7,   168,   169,    30,   187,    51,    50,    57,    52,    53,
      63,    73,    74,    76,    75,    77,   170,    43,    45,    47,
      49,     0,     0,     8,    25,     9,    22,    20,    24,     0,
       0,    59,    33,    67,    36,    32,    78,    69,     0,    82,
       0,     0,   188,     0,   142,   141,    62,   143,    58,   147,
      65,    64,    42,    98,   171,   172,   173,   175,   176,   177,
     174,   178,   179,   180,   181,    44,   115,   183,   184,   123,
     182,    46,    90,    94,   186,   185,    95,     0,    48,     1,
       4,     0,     5,     2,    10,    23,    21,     0,    27,     0,
      55,    60,    68,    80,    79,    70,    72,    71,     0,     0,
      38,     0,    89,    34,    17,   189,    19,     0,    13,   156,
     148,   155,     0,   157,   185,   167,   144,    61,   146,    66,
       0,   117,   122,    92,    96,    93,   126,     0,     3,     6,
      11,    31,    28,    56,    81,    41,    86,    85,    83,     0,
      39,    35,    87,    88,    16,     0,    18,    12,   151,   153,
     159,   149,   165,     0,   145,     0,   107,     0,     0,   116,
       0,   118,   120,   119,    91,    97,   130,   132,     0,   127,
       0,   134,     0,    29,    40,    15,    14,   150,   160,   158,
     164,   163,   161,   166,     0,   108,   106,   101,     0,   103,
     121,   129,   131,     0,   137,   136,   140,   162,   104,   109,
     102,   105,     0,   124,   128,   135,   138,   139,   111,   110,
     125,   112,   113,   114
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -142,  -142,   115,  -142,   123,     1,   169,   -22,   -19,  -142,
    -142,  -142,  -142,   -38,  -142,   174,  -142,  -142,  -142,  -142,
     126,    60,   103,  -142,  -142,  -142,  -139,    69,  -141,   118,
    -142,  -142,  -142,    47,  -142,    43,   194,    -3,   -42,  -142,
      96,     0,  -142,  -142,   -17,   -15,  -142,   -98
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,    21,    83,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    71,    72,    73,    52,   157,   158,    65,   162,    66,
      78,   127,   168,   169,   170,   171,    47,    48,    49,   112,
     113,    67,    68,    74,    75,   163,    41,   108
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      40,    69,   103,    85,    76,   116,    86,    50,    46,   147,
      51,   182,    80,    81,    42,    80,   184,    53,   188,   190,
       1,     2,   208,   175,    77,    40,   114,    90,   115,     1,
       2,   125,   124,    93,    79,   105,    94,    43,    96,    82,
     197,    40,    82,    16,     1,     2,   180,   176,   119,   117,
      43,   181,    16,    95,    91,     4,    54,    55,    56,    57,
      58,    59,    60,   141,    10,    44,    45,    16,    92,     1,
       2,   161,    61,    62,    63,    64,   154,    70,    43,    43,
      44,    45,     1,     2,    40,    85,   133,   143,    86,   194,
     122,   134,    16,     4,    44,    45,   195,  -133,   120,   139,
     121,     1,     2,   142,    69,    16,   126,    40,    76,   165,
     123,   166,   167,    54,    55,    56,    57,    58,    59,    60,
       4,   129,    43,   131,    16,   132,   135,   172,   140,    61,
      62,    63,    64,   114,    70,   115,   148,    44,    45,  -154,
     109,   152,     1,     2,   153,   173,   174,  -154,  -154,    69,
    -154,  -154,    76,   110,    54,    55,    56,    57,    58,    59,
      60,  -154,  -154,  -154,  -154,    16,  -154,  -154,  -154,   102,
      61,    62,    63,    64,   111,    70,     5,     6,     7,     8,
       9,   207,   177,   183,   156,   185,   189,   193,   196,   199,
     211,   198,   201,   172,   212,   172,   128,   -84,   136,   209,
       1,     2,   200,   210,   213,   -84,   -84,   130,   -84,   -84,
     107,    97,   -84,   -84,   -84,   -84,   -84,   -84,   -84,   -84,
     -84,   -84,   -84,    16,   -84,   138,   164,   192,   -84,   -84,
     -84,   -84,   137,  -152,   149,   191,  -152,  -152,   205,   159,
     204,   150,  -152,   118,  -152,  -152,   179,   151,  -152,  -152,
    -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,
    -152,  -152,  -152,     0,  -152,  -152,  -152,  -152,   -54,    89,
       0,   -54,   -54,     0,     0,     0,     0,   -54,     0,   -54,
     -54,    43,     0,   -54,   -54,   -54,   -54,   -54,   -54,   -54,
     -54,   -54,   -54,   -54,   -54,   -54,    44,    45,     0,   -54,
     -54,   -54,   -54,   -37,    98,     0,   -37,   -37,     0,     0,
       0,    99,   100,     0,   -37,   -37,     0,     0,   -37,   -37,
     -37,   -37,   -37,   -37,   -37,   -37,   -37,   -37,   -37,   -37,
     -37,     0,     0,     0,   -37,   -37,   -37,   -37,   -26,    87,
       0,   -26,   -26,     0,     0,     0,     0,    88,     0,   -26,
     -26,     0,     0,   -26,   -26,   -26,   -26,   -26,   -26,   -26,
     -26,   -26,   -26,   -26,   -26,   -26,     0,     0,     0,   -26,
     -26,   -26,   -26,   144,   145,     0,     1,     2,     0,     0,
       0,     0,     3,     0,     4,   105,     0,     0,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
     146,     0,     0,     0,    17,    18,    19,    20,   -99,   155,
       0,   -99,   -99,     0,     0,     0,     0,   -99,     0,   -99,
     -99,     0,     0,   -99,   -99,   -99,   -99,   -99,   -99,   -99,
     -99,   -99,   -99,   -99,   156,   -99,     0,     0,     0,   -99,
     -99,   -99,   -99,  -100,   186,     0,  -100,  -100,     0,     0,
       0,     0,  -100,     0,  -100,   187,     0,     0,  -100,  -100,
    -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,   156,
    -100,     0,     0,     0,  -100,  -100,  -100,  -100,   104,     0,
       0,     1,     2,     0,     0,     0,     0,     3,     0,     4,
     105,     0,     0,     5,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,   106,     1,     2,     0,    17,
      18,    19,    20,   160,     0,     0,     0,     0,    54,    55,
      56,    57,    58,    59,    60,     0,     0,     0,     0,    16,
       1,     2,     0,   161,    61,    62,    63,    64,     0,    70,
       0,     0,    54,    55,    56,    57,    58,    59,    60,     0,
       0,     0,     0,    16,     1,     2,     0,     0,    61,    62,
      63,    64,   178,    70,     0,     0,    54,    55,    56,    57,
      58,    59,    60,     0,     0,     0,     0,    16,     1,     2,
     206,   161,    61,    62,    63,    64,     0,    70,     0,     0,
      54,    55,    56,    57,    58,    59,    60,     0,     0,     0,
       0,    16,     1,     2,     0,     0,    61,    62,    63,    64,
       0,    70,     0,     0,    54,    55,    56,    57,    58,    59,
      60,     0,     0,     0,     0,    16,   202,     0,     1,     2,
      61,    62,    63,    64,     0,    70,     0,   203,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     1,     2,     0,
       0,    16,     0,     3,     0,     4,    84,   166,   167,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,     0,     0,     1,     2,    17,    18,    19,    20,     3,
       0,     4,     0,     0,     0,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,     1,     2,     0,
       0,    17,    18,    19,    20,     0,     0,     0,     0,    54,
      55,    56,    57,    58,    59,    60,   101,     0,     0,     0,
      16,     0,   102,     0,     0,    61,    62,    63,    64,     0,
       5,     6,     7,     8,     9
};

static const yytype_int16 yycheck[] =
{
       0,    18,    40,    25,    19,    47,    25,    10,     7,   107,
      10,   152,     0,     1,    35,     0,   155,    17,   157,   160,
       3,     4,     5,     0,     3,    25,    43,    30,    43,     3,
       4,    73,     8,    36,     0,    12,    36,    13,    37,    27,
     181,    41,    27,    26,     3,     4,     5,   145,    51,    48,
      13,    10,    26,     9,     9,    11,    15,    16,    17,    18,
      19,    20,    21,   101,    20,    28,    29,    26,     9,     3,
       4,    30,    31,    32,    33,    34,   118,    36,    13,    13,
      28,    29,     3,     4,    84,   107,    89,   102,   107,     1,
       7,    94,    26,    11,    28,    29,     8,     9,    11,    99,
       8,     3,     4,     5,   121,    26,    11,   107,   123,   124,
       8,    32,    33,    15,    16,    17,    18,    19,    20,    21,
      11,     9,    13,     9,    26,    35,     9,   127,    35,    31,
      32,    33,    34,   150,    36,   150,    14,    28,    29,     0,
       1,     7,     3,     4,     7,    35,    35,     8,     9,   166,
      11,    12,   167,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,     7,
      31,    32,    33,    34,    35,    36,    15,    16,    17,    18,
      19,   196,    14,    35,    26,     5,     9,     9,     7,     5,
       5,     9,     9,   193,     5,   195,    81,     0,     1,   199,
       3,     4,    12,    12,     5,     8,     9,    84,    11,    12,
      41,    37,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    99,   123,   167,    31,    32,
      33,    34,    35,     0,     1,   166,     3,     4,   195,   121,
     193,     8,     9,    49,    11,    12,   150,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    -1,    31,    32,    33,    34,     0,     1,
      -1,     3,     4,    -1,    -1,    -1,    -1,     9,    -1,    11,
      12,    13,    -1,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    -1,    31,
      32,    33,    34,     0,     1,    -1,     3,     4,    -1,    -1,
      -1,     8,     9,    -1,    11,    12,    -1,    -1,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    -1,    -1,    -1,    31,    32,    33,    34,     0,     1,
      -1,     3,     4,    -1,    -1,    -1,    -1,     9,    -1,    11,
      12,    -1,    -1,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    -1,    -1,    -1,    31,
      32,    33,    34,     0,     1,    -1,     3,     4,    -1,    -1,
      -1,    -1,     9,    -1,    11,    12,    -1,    -1,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    -1,    -1,    -1,    31,    32,    33,    34,     0,     1,
      -1,     3,     4,    -1,    -1,    -1,    -1,     9,    -1,    11,
      12,    -1,    -1,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    -1,    -1,    -1,    31,
      32,    33,    34,     0,     1,    -1,     3,     4,    -1,    -1,
      -1,    -1,     9,    -1,    11,    12,    -1,    -1,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    -1,    -1,    -1,    31,    32,    33,    34,     0,    -1,
      -1,     3,     4,    -1,    -1,    -1,    -1,     9,    -1,    11,
      12,    -1,    -1,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,     3,     4,    -1,    31,
      32,    33,    34,    10,    -1,    -1,    -1,    -1,    15,    16,
      17,    18,    19,    20,    21,    -1,    -1,    -1,    -1,    26,
       3,     4,    -1,    30,    31,    32,    33,    34,    -1,    36,
      -1,    -1,    15,    16,    17,    18,    19,    20,    21,    -1,
      -1,    -1,    -1,    26,     3,     4,    -1,    -1,    31,    32,
      33,    34,    35,    36,    -1,    -1,    15,    16,    17,    18,
      19,    20,    21,    -1,    -1,    -1,    -1,    26,     3,     4,
       5,    30,    31,    32,    33,    34,    -1,    36,    -1,    -1,
      15,    16,    17,    18,    19,    20,    21,    -1,    -1,    -1,
      -1,    26,     3,     4,    -1,    -1,    31,    32,    33,    34,
      -1,    36,    -1,    -1,    15,    16,    17,    18,    19,    20,
      21,    -1,    -1,    -1,    -1,    26,     1,    -1,     3,     4,
      31,    32,    33,    34,    -1,    36,    -1,    12,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,    -1,
      -1,    26,    -1,     9,    -1,    11,    12,    32,    33,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    -1,    -1,     3,     4,    31,    32,    33,    34,     9,
      -1,    11,    -1,    -1,    -1,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,     3,     4,    -1,
      -1,    31,    32,    33,    34,    -1,    -1,    -1,    -1,    15,
      16,    17,    18,    19,    20,    21,     1,    -1,    -1,    -1,
      26,    -1,     7,    -1,    -1,    31,    32,    33,    34,    -1,
      15,    16,    17,    18,    19
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,     4,     9,    11,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    31,    32,    33,
      34,    39,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      79,    84,    35,    13,    28,    29,    43,    74,    75,    76,
      75,    79,    62,    79,    15,    16,    17,    18,    19,    20,
      21,    31,    32,    33,    34,    65,    67,    79,    80,    82,
      36,    59,    60,    61,    81,    82,    83,     3,    68,     0,
       0,     1,    27,    40,    12,    45,    46,     1,     9,     1,
      75,     9,     9,    75,    79,     9,    43,    53,     1,     8,
       9,     1,     7,    51,     0,    12,    27,    44,    85,     1,
      14,    35,    77,    78,    82,    83,    76,    43,    74,    75,
      11,     8,     7,     8,     8,    76,    11,    69,    40,     9,
      42,     9,    35,    75,    75,     9,     1,    35,    58,    79,
      35,    51,     5,    83,     0,     1,    27,    85,    14,     1,
       8,    14,     7,     7,    76,     1,    26,    63,    64,    67,
      10,    30,    66,    83,    60,    83,    32,    33,    70,    71,
      72,    73,    79,    35,    35,     0,    85,    14,    35,    78,
       5,    10,    66,    35,    64,     5,     1,    12,    64,     9,
      66,    65,    59,     9,     1,     8,     7,    66,     9,     5,
      12,     9,     1,    12,    71,    73,     5,    83,     5,    79,
      12,     5,     5,     5
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    38,    39,    39,    40,    40,    40,    41,    41,    42,
      42,    42,    43,    43,    43,    43,    43,    43,    43,    43,
      44,    44,    44,    44,    45,    45,    46,    46,    46,    46,
      46,    46,    46,    46,    46,    46,    46,    47,    47,    47,
      47,    47,    48,    48,    48,    48,    48,    48,    48,    48,
      49,    49,    49,    49,    50,    50,    50,    50,    50,    51,
      51,    51,    51,    52,    52,    52,    52,    53,    53,    54,
      54,    54,    54,    55,    55,    55,    55,    55,    56,    56,
      56,    56,    57,    57,    57,    57,    57,    58,    58,    58,
      59,    59,    59,    60,    60,    61,    61,    61,    62,    62,
      62,    62,    62,    63,    63,    63,    63,    64,    64,    64,
      64,    64,    64,    64,    64,    65,    65,    65,    66,    66,
      67,    67,    67,    67,    68,    68,    69,    70,    70,    71,
      71,    71,    71,    71,    72,    72,    72,    72,    73,    73,
      73,    74,    74,    75,    75,    75,    75,    75,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    77,    77,    77,
      77,    78,    78,    78,    78,    78,    78,    78,    79,    79,
      79,    80,    80,    80,    80,    80,    80,    80,    80,    80,
      80,    80,    81,    82,    82,    83,    83,    84,    84,    85
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     3,     1,     1,     2,     0,     1,     1,
       2,     3,     3,     2,     4,     4,     3,     2,     3,     2,
       1,     2,     1,     2,     1,     1,     1,     2,     3,     4,
       1,     3,     1,     1,     2,     3,     1,     1,     2,     3,
       4,     3,     2,     1,     2,     1,     2,     1,     2,     1,
       1,     1,     1,     1,     1,     2,     3,     1,     2,     1,
       2,     3,     2,     1,     2,     2,     3,     1,     2,     1,
       2,     2,     2,     1,     1,     1,     1,     1,     1,     2,
       2,     3,     1,     3,     2,     3,     3,     3,     3,     2,
       1,     3,     2,     2,     1,     1,     2,     3,     1,     2,
       3,     4,     5,     2,     3,     3,     2,     1,     2,     3,
       4,     4,     5,     6,     7,     1,     3,     2,     1,     1,
       3,     4,     2,     1,     5,     6,     1,     1,     3,     2,
       1,     2,     1,     1,     1,     3,     2,     2,     3,     3,
       2,     1,     1,     1,     2,     3,     2,     1,     2,     3,
       4,     3,     2,     3,     1,     2,     2,     1,     3,     2,
       3,     3,     4,     3,     3,     2,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, RESULT, yyscanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, RESULT, yyscanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), RESULT, yyscanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, RESULT, yyscanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_TOK_STRING: /* TOK_STRING  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1374 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_QSTRING: /* TOK_QSTRING  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1380 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_NUMBER: /* TOK_NUMBER  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1386 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_IF: /* TOK_IF  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1392 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_STEP: /* TOK_STEP  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1398 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_REPEAT: /* TOK_REPEAT  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1404 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_START: /* TOK_START  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1410 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_STOP: /* TOK_STOP  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1416 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_GOTO: /* TOK_GOTO  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1422 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_BRANCH: /* TOK_BRANCH  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1428 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_THEN: /* TOK_THEN  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1434 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_ELSE: /* TOK_ELSE  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1440 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_YES: /* TOK_YES  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1446 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_NO: /* TOK_NO  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1452 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_SHAPE_COMMAND: /* TOK_SHAPE_COMMAND  */
#line 119 "flow_lang.yy"
            { }
#line 1458 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_BYE: /* TOK_BYE  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1464 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_STRING: /* TOK_COLON_STRING  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1470 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_QUOTED_STRING: /* TOK_COLON_QUOTED_STRING  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1476 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_COLORDEF: /* TOK_COLORDEF  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1482 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFSHAPE: /* TOK_COMMAND_DEFSHAPE  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1488 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFCOLOR: /* TOK_COMMAND_DEFCOLOR  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1494 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFSTYLE: /* TOK_COMMAND_DEFSTYLE  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1500 "flow_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFDESIGN: /* TOK_COMMAND_DEFDESIGN  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1506 "flow_lang.cc"
        break;

    case YYSYMBOL_top_level_instrlist: /* top_level_instrlist  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1512 "flow_lang.cc"
        break;

    case YYSYMBOL_braced_instrlist: /* braced_instrlist  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1518 "flow_lang.cc"
        break;

    case YYSYMBOL_instrlist: /* instrlist  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1524 "flow_lang.cc"
        break;

    case YYSYMBOL_several_instructions: /* several_instructions  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1530 "flow_lang.cc"
        break;

    case YYSYMBOL_instr_with_semicolon: /* instr_with_semicolon  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1536 "flow_lang.cc"
        break;

    case YYSYMBOL_optlist_with_semicolon: /* optlist_with_semicolon  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1542 "flow_lang.cc"
        break;

    case YYSYMBOL_instr_needs_semicolon: /* instr_needs_semicolon  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1548 "flow_lang.cc"
        break;

    case YYSYMBOL_step_types: /* step_types  */
#line 119 "flow_lang.yy"
            { }
#line 1554 "flow_lang.cc"
        break;

    case YYSYMBOL_actual_instr_needs_semicolon: /* actual_instr_needs_semicolon  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1560 "flow_lang.cc"
        break;

    case YYSYMBOL_actual_instr: /* actual_instr  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1566 "flow_lang.cc"
        break;

    case YYSYMBOL_goto: /* goto  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1572 "flow_lang.cc"
        break;

    case YYSYMBOL_goto_with_semicolon: /* goto_with_semicolon  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1578 "flow_lang.cc"
        break;

    case YYSYMBOL_branch_with_semicolon: /* branch_with_semicolon  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).branch);}
#line 1584 "flow_lang.cc"
        break;

    case YYSYMBOL_branch_keyword: /* branch_keyword  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1590 "flow_lang.cc"
        break;

    case YYSYMBOL_branch_no_content: /* branch_no_content  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).branch);}
#line 1596 "flow_lang.cc"
        break;

    case YYSYMBOL_optlist: /* optlist  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1602 "flow_lang.cc"
        break;

    case YYSYMBOL_opt: /* opt  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1608 "flow_lang.cc"
        break;

    case YYSYMBOL_stylenamelist: /* stylenamelist  */
#line 120 "flow_lang.yy"
            {delete ((*yyvaluep).stringlist);}
#line 1614 "flow_lang.cc"
        break;

    case YYSYMBOL_shapedeflist: /* shapedeflist  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).shape);}
#line 1620 "flow_lang.cc"
        break;

    case YYSYMBOL_shapeline: /* shapeline  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).shapeelement);}
#line 1626 "flow_lang.cc"
        break;

    case YYSYMBOL_color_string: /* color_string  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1632 "flow_lang.cc"
        break;

    case YYSYMBOL_colon_string: /* colon_string  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1638 "flow_lang.cc"
        break;

    case YYSYMBOL_full_attrlist_with_label: /* full_attrlist_with_label  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attributelist);}
#line 1644 "flow_lang.cc"
        break;

    case YYSYMBOL_full_attrlist: /* full_attrlist  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attributelist);}
#line 1650 "flow_lang.cc"
        break;

    case YYSYMBOL_attrlist: /* attrlist  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attributelist);}
#line 1656 "flow_lang.cc"
        break;

    case YYSYMBOL_attr: /* attr  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attribute);}
#line 1662 "flow_lang.cc"
        break;

    case YYSYMBOL_entity_string: /* entity_string  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1668 "flow_lang.cc"
        break;

    case YYSYMBOL_reserved_word_string: /* reserved_word_string  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1674 "flow_lang.cc"
        break;

    case YYSYMBOL_symbol_string: /* symbol_string  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1680 "flow_lang.cc"
        break;

    case YYSYMBOL_alpha_string: /* alpha_string  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1686 "flow_lang.cc"
        break;

    case YYSYMBOL_string: /* string  */
#line 118 "flow_lang.yy"
            {free(((*yyvaluep).str));}
#line 1692 "flow_lang.cc"
        break;

    case YYSYMBOL_scope_close: /* scope_close  */
#line 121 "flow_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1698 "flow_lang.cc"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */


/* User initialization code.  */
#line 8 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    yylloc.first_pos = 0;
    yylloc.last_pos = 0;
  #endif
}

#line 1800 "flow_lang.cc"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, yyscanner);
    }

  if (yychar <= TOK_EOF)
    {
      yychar = TOK_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* chart_with_bye: chart eof  */
#line 177 "flow_lang.yy"
{
	YYACCEPT;
}
#line 2015 "flow_lang.cc"
    break;

  case 3: /* chart_with_bye: chart error eof  */
#line 181 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
	YYACCEPT;
}
#line 2028 "flow_lang.cc"
    break;

  case 5: /* eof: TOK_BYE  */
#line 193 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
	csh.AddCSH_AllCommentBeyond((yylsp[0]));
  #else
  #endif
    free((yyvsp[0].str));
}
#line 2041 "flow_lang.cc"
    break;

  case 6: /* eof: TOK_BYE TOK_SEMICOLON  */
#line 202 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
	csh.AddCSH_AllCommentBeyond((yylsp[0]));
  #else
  #endif
    free((yyvsp[-1].str));
}
#line 2055 "flow_lang.cc"
    break;

  case 7: /* chart: %empty  */
#line 213 "flow_lang.yy"
{
  //Add here what to do for an empty chart
  #ifdef C_S_H_IS_COMPILED
    csh.AddLineBeginToHints();
    csh.hintStatus = HINT_READY;
    csh.hintSource = EHintSourceType::LINE_START;
    csh.hintsForcedOnly = true;
  #else
    //no action for empty file
  #endif
}
#line 2071 "flow_lang.cc"
    break;

  case 8: /* chart: top_level_instrlist  */
#line 225 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBefore((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    chart.AddElements((yyvsp[0].instruction_list));
  #endif
}
#line 2086 "flow_lang.cc"
    break;

  case 10: /* top_level_instrlist: instrlist TOK_CCBRACKET  */
#line 239 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Closing brace missing its opening pair.");
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Unexpected '}'.");
  #endif
}
#line 2099 "flow_lang.cc"
    break;

  case 11: /* top_level_instrlist: instrlist TOK_CCBRACKET top_level_instrlist  */
#line 248 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Closing brace missing its opening pair.");
  #else
    //Merge $3 into $1
    ((yyvsp[-2].instruction_list))->splice(((yyvsp[-2].instruction_list))->end(), *((yyvsp[0].instruction_list)));
    delete ((yyvsp[0].instruction_list));
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Unexpected '}'.");
  #endif
}
#line 2115 "flow_lang.cc"
    break;

  case 12: /* braced_instrlist: scope_open instrlist scope_close  */
#line 262 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    if ((yyvsp[0].instruction)) ((yyvsp[-1].instruction_list))->Append((yyvsp[0].instruction)); //Append any potential CommandNumbering
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
  #endif
}
#line 2128 "flow_lang.cc"
    break;

  case 13: /* braced_instrlist: scope_open scope_close  */
#line 271 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    (yyval.instruction_list) = new FElementList;
    //scope_close should not return here with a CommandNumbering
    //but just in case
    if ((yyvsp[0].instruction))
        delete((yyvsp[0].instruction));
  #endif
}
#line 2144 "flow_lang.cc"
    break;

  case 14: /* braced_instrlist: scope_open instrlist error scope_close  */
#line 283 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    if ((yyvsp[0].instruction)) ((yyvsp[-2].instruction_list))->Append((yyvsp[0].instruction));
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
    yyerrok;
}
#line 2160 "flow_lang.cc"
    break;

  case 15: /* braced_instrlist: scope_open instrlist error TOK_EOF  */
#line 295 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_START((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
}
#line 2176 "flow_lang.cc"
    break;

  case 16: /* braced_instrlist: scope_open instrlist TOK_EOF  */
#line 307 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
}
#line 2192 "flow_lang.cc"
    break;

  case 17: /* braced_instrlist: scope_open TOK_EOF  */
#line 319 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
  #else
    (yyval.instruction_list) = nullptr;
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
}
#line 2207 "flow_lang.cc"
    break;

  case 18: /* braced_instrlist: scope_open instrlist TOK_BYE  */
#line 330 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
  #endif
  free((yyvsp[0].str));
}
#line 2224 "flow_lang.cc"
    break;

  case 19: /* braced_instrlist: scope_open TOK_BYE  */
#line 343 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
  #else
    (yyval.instruction_list) = nullptr;
    //Do not pop context, as the missing scope_close would have done
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
  free((yyvsp[0].str));
}
#line 2240 "flow_lang.cc"
    break;

  case 20: /* instrlist: instr_with_semicolon  */
#line 357 "flow_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction))
        (yyval.instruction_list) = (new FElementList)->Append((yyvsp[0].instruction)); /* New list */
    else
        (yyval.instruction_list) = new FElementList;
  #endif
}
#line 2253 "flow_lang.cc"
    break;

  case 21: /* instrlist: instrlist instr_with_semicolon  */
#line 366 "flow_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction)) ((yyvsp[-1].instruction_list))->Append((yyvsp[0].instruction));     /* Add to existing list */
    (yyval.instruction_list) = ((yyvsp[-1].instruction_list));
  #endif
}
#line 2264 "flow_lang.cc"
    break;

  case 23: /* instrlist: instrlist several_instructions  */
#line 374 "flow_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    //TODO: Add a nested instructionlist to another instructionslist
    if ((yyvsp[0].instruction_list)) ((yyvsp[-1].instruction_list))->Append((yyvsp[0].instruction_list));     /* Add to existing list */
    (yyval.instruction_list) = ((yyvsp[-1].instruction_list));
  #endif
}
#line 2276 "flow_lang.cc"
    break;

  case 25: /* several_instructions: braced_instrlist  */
#line 384 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    //Standalone braced instruction lists are instructions - for the purpose of indentation
    csh.AddInstruction((yylsp[0]));
  #endif
  (yyval.instruction_list) = (yyvsp[0].instruction_list);
}
#line 2288 "flow_lang.cc"
    break;

  case 26: /* instr_with_semicolon: instr_needs_semicolon  */
#line 394 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[0].instruction);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 2303 "flow_lang.cc"
    break;

  case 27: /* instr_with_semicolon: instr_needs_semicolon TOK_SEMICOLON  */
#line 405 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[-1].instruction);
  #endif
}
#line 2321 "flow_lang.cc"
    break;

  case 30: /* instr_with_semicolon: TOK_SEMICOLON  */
#line 423 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction)=nullptr;
  #endif
}
#line 2338 "flow_lang.cc"
    break;

  case 31: /* instr_with_semicolon: instr_needs_semicolon error TOK_SEMICOLON  */
#line 436 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[-2].instruction);
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
  #endif
}
#line 2359 "flow_lang.cc"
    break;

  case 32: /* instr_with_semicolon: branch_with_semicolon  */
#line 453 "flow_lang.yy"
{
    (yyval.instruction) = (yyvsp[0].branch); //to suppress warning of incompatible types.
}
#line 2367 "flow_lang.cc"
    break;

  case 33: /* instr_with_semicolon: actual_instr  */
#line 457 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.instruction) = (yyvsp[0].instruction);
  #endif
}
#line 2378 "flow_lang.cc"
    break;

  case 34: /* instr_with_semicolon: entity_string actual_instr  */
#line 464 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_MARKERNAME);
  #else
    (yyval.instruction) = (yyvsp[0].instruction);
    if ((yyvsp[-1].str) && (yyval.instruction))
        (yyval.instruction)->AddRefName((yyvsp[-1].str));
  #endif
    free((yyvsp[-1].str));
}
#line 2393 "flow_lang.cc"
    break;

  case 35: /* instr_with_semicolon: entity_string error actual_instr  */
#line 475 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_MARKERNAME);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is this.");
  #else
    (yyval.instruction) = (yyvsp[0].instruction);
    if ((yyvsp[-2].str) && (yyval.instruction))
        (yyval.instruction)->AddRefName((yyvsp[-2].str));
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error here.");
  #endif
    free((yyvsp[-2].str));
}
#line 2410 "flow_lang.cc"
    break;

  case 37: /* optlist_with_semicolon: optlist  */
#line 492 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    (yyval.instruction_list)=(yyvsp[0].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 2424 "flow_lang.cc"
    break;

  case 38: /* optlist_with_semicolon: optlist TOK_SEMICOLON  */
#line 502 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list)=(yyvsp[-1].instruction_list);
  #endif
}
#line 2441 "flow_lang.cc"
    break;

  case 41: /* optlist_with_semicolon: optlist error TOK_SEMICOLON  */
#line 519 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list)=(yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';' after option(s).");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the option list as I understood it.");
  #endif
}
#line 2461 "flow_lang.cc"
    break;

  case 42: /* instr_needs_semicolon: TOK_COMMAND_DEFSHAPE shapedef  */
#line 538 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.SkipContent())
        csh.AddCSH_Error((yylsp[-1]), "Cannot define designs inside a procedure.");
    else
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define designs inside a procedure.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 2487 "flow_lang.cc"
    break;

  case 43: /* instr_needs_semicolon: TOK_COMMAND_DEFSHAPE  */
#line 560 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.SkipContent())
        csh.AddCSH_Error((yylsp[0]), "Cannot define designs inside a procedure.");
    else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing shape name and definition.");
    }
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Cannot define shapes inside a procedure.");
    else
        chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing shape name and definition.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 2513 "flow_lang.cc"
    break;

  case 44: /* instr_needs_semicolon: TOK_COMMAND_DEFCOLOR colordeflist  */
#line 582 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 2533 "flow_lang.cc"
    break;

  case 45: /* instr_needs_semicolon: TOK_COMMAND_DEFCOLOR  */
#line 598 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 2555 "flow_lang.cc"
    break;

  case 46: /* instr_needs_semicolon: TOK_COMMAND_DEFSTYLE styledeflist  */
#line 616 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 2575 "flow_lang.cc"
    break;

  case 47: /* instr_needs_semicolon: TOK_COMMAND_DEFSTYLE  */
#line 632 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing style name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 2597 "flow_lang.cc"
    break;

  case 48: /* instr_needs_semicolon: TOK_COMMAND_DEFDESIGN designdef  */
#line 650 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.SkipContent())
        csh.AddCSH_Error((yylsp[-1]), "Cannot define designs inside a procedure.");
    else
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define designs inside a procedure.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 2619 "flow_lang.cc"
    break;

  case 49: /* instr_needs_semicolon: TOK_COMMAND_DEFDESIGN  */
#line 668 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.SkipContent())
        csh.AddCSH_Error((yylsp[0]), "Cannot define designs inside a procedure.");
    else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing design name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Cannot define designs inside a procedure.");
    else
        chart.Error.Error(CHART_POS((yyloc)).end, "Missing a design name to (re-)define.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 2646 "flow_lang.cc"
    break;

  case 50: /* step_types: TOK_STEP  */
#line 692 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
     (yyval.steptype) = true;
     free((yyvsp[0].str));
  #else
     (yyval.steptype) = {Step::BOX, (yyvsp[0].str), true};
  #endif
}
#line 2659 "flow_lang.cc"
    break;

  case 51: /* step_types: TOK_IF  */
#line 701 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
     (yyval.steptype) = true;
     free((yyvsp[0].str));
  #else
     (yyval.steptype) = {Step::IF, (yyvsp[0].str), true};
  #endif
}
#line 2672 "flow_lang.cc"
    break;

  case 52: /* step_types: TOK_START  */
#line 710 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
     (yyval.steptype) = false;
     free((yyvsp[0].str));
  #else
     (yyval.steptype) = {Step::START, (yyvsp[0].str), false};
  #endif
}
#line 2685 "flow_lang.cc"
    break;

  case 53: /* step_types: TOK_STOP  */
#line 719 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
     (yyval.steptype) = false;
     free((yyvsp[0].str));
  #else
     (yyval.steptype) = {Step::STOP, (yyvsp[0].str), false};
  #endif
}
#line 2698 "flow_lang.cc"
    break;

  case 54: /* actual_instr_needs_semicolon: step_types  */
#line 730 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if ((yyvsp[0].steptype))
        csh.AddCSH_ErrorAfter((yyloc), "Missing label.");
  #else
    if ((yyvsp[0].steptype).must_have_label) {
        (yyval.instruction) = nullptr;
        chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing label. Ignoring it.");
    } else {
        (yyval.instruction) = new Step(&chart, (yyvsp[0].steptype).type);
        (yyval.instruction)->SetLineEnd(CHART_POS((yyloc)));
        AttributeList *list = new AttributeList;
        list->Append(std::make_unique<Attribute>("label", (yyvsp[0].steptype).keyword, CHART_POS((yylsp[0])), CHART_POS((yylsp[0])), true));
        (yyval.instruction)->AddAttributeList(list); //destroys list
    }
    free((yyvsp[0].steptype).keyword);
  #endif
}
#line 2722 "flow_lang.cc"
    break;

  case 55: /* actual_instr_needs_semicolon: step_types full_attrlist_with_label  */
#line 750 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Step::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Step::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.instruction) = new Step(&chart, (yyvsp[-1].steptype).type);
    (yyval.instruction)->SetLineEnd(CHART_POS((yyloc)));
    (yyval.instruction)->AddAttributeList((yyvsp[0].attributelist));
    free((yyvsp[-1].steptype).keyword);
  #endif
}
#line 2741 "flow_lang.cc"
    break;

  case 56: /* actual_instr_needs_semicolon: step_types error full_attrlist_with_label  */
#line 765 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is this.");
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Step::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Step::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.instruction) = new Step(&chart, (yyvsp[-2].steptype).type);
    (yyval.instruction)->SetLineEnd(CHART_POS((yyloc)));
    (yyval.instruction)->AddAttributeList((yyvsp[0].attributelist));
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error here.");
    free((yyvsp[-2].steptype).keyword);
  #endif
}
#line 2762 "flow_lang.cc"
    break;

  case 57: /* actual_instr_needs_semicolon: TOK_REPEAT  */
#line 782 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yyloc), "Missing steps to repeat.");
  #else
    (yyval.instruction) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing steps to repeat. Ignoring 'repeat'.");
  #endif
    free((yyvsp[0].str));
}
#line 2777 "flow_lang.cc"
    break;

  case 58: /* actual_instr_needs_semicolon: TOK_REPEAT full_attrlist_with_label  */
#line 793 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yyloc), "Missing steps to repeat.");
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.instruction) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing steps to repeat. Ignoring 'repeat'.");
    delete (yyvsp[0].attributelist);
  #endif
    free((yyvsp[-1].str));
}
#line 2797 "flow_lang.cc"
    break;

  case 59: /* actual_instr: actual_instr_needs_semicolon  */
#line 810 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[0].instruction);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 2812 "flow_lang.cc"
    break;

  case 60: /* actual_instr: actual_instr_needs_semicolon TOK_SEMICOLON  */
#line 821 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[-1].instruction);
  #endif
}
#line 2830 "flow_lang.cc"
    break;

  case 61: /* actual_instr: TOK_REPEAT full_attrlist_with_label braced_instrlist  */
#line 835 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.instruction) = new Repeat(&chart, (yyvsp[0].instruction_list));
    (yyval.instruction)->SetLineEnd(CHART_POS2((yylsp[-2]),(yylsp[-1])));
    (yyval.instruction)->AddAttributeList((yyvsp[-1].attributelist));
  #endif
    free((yyvsp[-2].str));
}
#line 2849 "flow_lang.cc"
    break;

  case 62: /* actual_instr: TOK_REPEAT braced_instrlist  */
#line 850 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
  #else
    (yyval.instruction) = new Repeat(&chart, (yyvsp[0].instruction_list));
    (yyval.instruction)->SetLineEnd(CHART_POS((yylsp[-1])));
    (yyval.instruction)->AddAttributeList(nullptr);
  #endif
    free((yyvsp[-1].str));
}
#line 2864 "flow_lang.cc"
    break;

  case 63: /* goto: TOK_GOTO  */
#line 862 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yyloc), "Missing step name.");
  #else
    (yyval.instruction) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing step name. Ignoring 'goto'.");
  #endif
    free((yyvsp[0].str));
}
#line 2879 "flow_lang.cc"
    break;

  case 64: /* goto: TOK_GOTO entity_string  */
#line 873 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_MARKERNAME);
  #else
    (yyval.instruction) = new Goto(&chart, (yyvsp[0].str));
    (yyval.instruction)->SetLineEnd(CHART_POS((yyloc)));
    (yyval.instruction)->AddAttributeList(nullptr);
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 2896 "flow_lang.cc"
    break;

  case 65: /* goto: TOK_GOTO full_attrlist_with_label  */
#line 886 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing step name.");
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.instruction) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yyloc)), "Missing step name. Ignoring 'goto'.");
    delete (yyvsp[0].attributelist);
  #endif
    free((yyvsp[-1].str));
}
#line 2916 "flow_lang.cc"
    break;

  case 66: /* goto: TOK_GOTO entity_string full_attrlist_with_label  */
#line 902 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[-1]), COLOR_MARKERNAME);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.instruction) = new Goto(&chart, (yyvsp[-1].str));
    (yyval.instruction)->SetLineEnd(CHART_POS((yyloc)));
    (yyval.instruction)->AddAttributeList((yyvsp[0].attributelist));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
}
#line 2937 "flow_lang.cc"
    break;

  case 67: /* goto_with_semicolon: goto  */
#line 921 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[0].instruction);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 2952 "flow_lang.cc"
    break;

  case 68: /* goto_with_semicolon: goto TOK_SEMICOLON  */
#line 932 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[-1].instruction);
  #endif
}
#line 2970 "flow_lang.cc"
    break;

  case 69: /* branch_with_semicolon: branch_no_content  */
#line 949 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.branch)=(yyvsp[0].branch);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 2985 "flow_lang.cc"
    break;

  case 70: /* branch_with_semicolon: branch_no_content TOK_SEMICOLON  */
#line 960 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.branch)=(yyvsp[-1].branch);
  #endif
}
#line 3003 "flow_lang.cc"
    break;

  case 71: /* branch_with_semicolon: branch_no_content goto_with_semicolon  */
#line 974 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.branch) = (yyvsp[-1].branch)->AddStep((yyvsp[0].instruction));
  #endif
}
#line 3014 "flow_lang.cc"
    break;

  case 72: /* branch_with_semicolon: branch_no_content braced_instrlist  */
#line 981 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.branch) = (yyvsp[-1].branch)->AddSteps((yyvsp[0].instruction_list));
  #endif
}
#line 3025 "flow_lang.cc"
    break;

  case 73: /* branch_keyword: TOK_BRANCH  */
#line 989 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
    (yyval.str) = strdup("");
    free((yyvsp[0].str));
  #endif
}
#line 3038 "flow_lang.cc"
    break;

  case 74: /* branch_keyword: TOK_THEN  */
#line 998 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
    (yyval.str) = strdup("yes");
    free((yyvsp[0].str));
  #endif
}
#line 3051 "flow_lang.cc"
    break;

  case 75: /* branch_keyword: TOK_YES  */
#line 1007 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
    (yyval.str) = strdup("yes");
    free((yyvsp[0].str));
  #endif
}
#line 3064 "flow_lang.cc"
    break;

  case 76: /* branch_keyword: TOK_ELSE  */
#line 1016 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
    (yyval.str) = strdup("no");
    free((yyvsp[0].str));
  #endif
}
#line 3077 "flow_lang.cc"
    break;

  case 77: /* branch_keyword: TOK_NO  */
#line 1025 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
    (yyval.str) = strdup("no");
    free((yyvsp[0].str));
  #endif
}
#line 3090 "flow_lang.cc"
    break;

  case 78: /* branch_no_content: branch_keyword  */
#line 1035 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.branch) = new Branch(&chart);
    (yyval.branch)->SetLineEnd(CHART_POS((yyloc)));
    (yyval.branch)->AddAttributeList(nullptr);
  #endif
    free((yyvsp[0].str));
}
#line 3104 "flow_lang.cc"
    break;

  case 79: /* branch_no_content: branch_keyword entity_string  */
#line 1045 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    (yyval.branch) = new Branch(&chart);
    (yyval.branch)->AddLabel((yyvsp[0].str));
    (yyval.branch)->SetLineEnd(CHART_POS((yyloc)));
    (yyval.branch)->AddAttributeList(nullptr);
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].str));
}
#line 3121 "flow_lang.cc"
    break;

  case 80: /* branch_no_content: branch_keyword full_attrlist_with_label  */
#line 1058 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.branch) = new Branch(&chart);
    (yyval.branch)->AddAttributeList((yyvsp[0].attributelist));
    (yyval.branch)->SetLineEnd(CHART_POS((yyloc)));
  #endif
    free((yyvsp[-1].str));
}
#line 3139 "flow_lang.cc"
    break;

  case 81: /* branch_no_content: branch_keyword entity_string full_attrlist_with_label  */
#line 1072 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        Arrow::AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        Arrow::AttributeValues(csh.hintAttrName, csh);
  #else
    (yyval.branch) = new Branch(&chart);
    (yyval.branch)->AddLabel((yyvsp[-1].str))->AddAttributeList((yyvsp[0].attributelist));
    (yyval.branch)->SetLineEnd(CHART_POS((yyloc)));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[-1].str));
}
#line 3159 "flow_lang.cc"
    break;

  case 82: /* optlist: opt  */
#line 1090 "flow_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction))
        (yyval.instruction_list) = (new FElementList)->Append((yyvsp[0].instruction)); /* New list */
    else
        (yyval.instruction_list) = new FElementList;
  #endif
}
#line 3172 "flow_lang.cc"
    break;

  case 83: /* optlist: optlist TOK_COMMA opt  */
#line 1099 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction)) ((yyvsp[-2].instruction_list))->Append((yyvsp[0].instruction));     /* Add to existing list */
    (yyval.instruction_list) = ((yyvsp[-2].instruction_list));
  #endif
  #endif
}
#line 3191 "flow_lang.cc"
    break;

  case 84: /* optlist: optlist TOK_COMMA  */
#line 1114 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an option here.");
  #endif
}
#line 3208 "flow_lang.cc"
    break;

  case 86: /* optlist: optlist TOK_COMMA error  */
#line 1128 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "An option expected here.");
  #else
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "I am not sure what is coming here.");
  #endif
}
#line 3226 "flow_lang.cc"
    break;

  case 87: /* opt: entity_string TOK_EQUAL TOK_NUMBER  */
#line 1144 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].str), COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str))) {
        FlowChart::AttributeValues((yyvsp[-2].str), csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction) = chart.AddAttribute(Attribute((yyvsp[-2].str), (yyvsp[0].str), CHART_POS((yylsp[-2])), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 3249 "flow_lang.cc"
    break;

  case 88: /* opt: entity_string TOK_EQUAL string  */
#line 1163 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].str), COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), (yyvsp[-2].str));
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str))) {
        FlowChart::AttributeValues((yyvsp[-2].str), csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction) = chart.AddAttribute(Attribute((yyvsp[-2].str), (yyvsp[0].str), CHART_POS((yylsp[-2])), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 3272 "flow_lang.cc"
    break;

  case 89: /* opt: entity_string TOK_EQUAL  */
#line 1182 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].str), COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing option value.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].str))) {
        FlowChart::AttributeValues((yyvsp[-1].str), csh);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 3295 "flow_lang.cc"
    break;

  case 91: /* styledeflist: styledeflist TOK_COMMA styledef  */
#line 1203 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 3309 "flow_lang.cc"
    break;

  case 92: /* styledeflist: styledeflist TOK_COMMA  */
#line 1213 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing style definition here.", "Try just removing the comma.");
#endif
}
#line 3325 "flow_lang.cc"
    break;

  case 93: /* styledef: stylenamelist full_attrlist  */
#line 1226 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    for (auto &str : *((yyvsp[-1].stringlist)))
        if (csh.ForbiddenStyles.find(str) == csh.ForbiddenStyles.end())
            csh.CurrentContext().StyleNames.insert(str);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        FlowStyle().AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        FlowStyle().AttributeValues(csh.hintAttrName, csh);
  #else
	chart.AddAttributeListToStyleList((yyvsp[0].attributelist), (yyvsp[-1].stringlist)); //deletes $2, as well
  #endif
    delete((yyvsp[-1].stringlist));
}
#line 3344 "flow_lang.cc"
    break;

  case 94: /* styledef: stylenamelist  */
#line 1241 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH_ErrorAfter((yyloc), "Missing attribute definitons in square brackets ('[' and ']').");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing attribute definitons in square brackets ('[' and ']').");
  #endif
    delete((yyvsp[0].stringlist));
}
#line 3357 "flow_lang.cc"
    break;

  case 95: /* stylenamelist: string  */
#line 1251 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
    (yyval.stringlist) = new std::list<string>;
    if (strcmp((yyvsp[0].str), "emphasis")==0)
        ((yyval.stringlist))->push_back("box");
    else if (strcmp((yyvsp[0].str), "emptyemphasis")==0)
        ((yyval.stringlist))->push_back("emptybox");
    else ((yyval.stringlist))->push_back((yyvsp[0].str));
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
	csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.stringlist) = new std::list<string>;
    if ((yyvsp[0].str))
        ((yyval.stringlist))->emplace_back((yyvsp[0].str));
  #endif
    free((yyvsp[0].str));
}
#line 3382 "flow_lang.cc"
    break;

  case 96: /* stylenamelist: stylenamelist TOK_COMMA  */
#line 1272 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
	csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a style name to (re-)define.");
    if (csh.CheckHintAfter((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
		csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
	(yyval.stringlist) = (yyvsp[-1].stringlist);
  #else
    (yyval.stringlist) = (yyvsp[-1].stringlist);
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
  #endif
}
#line 3401 "flow_lang.cc"
    break;

  case 97: /* stylenamelist: stylenamelist TOK_COMMA string  */
#line 1287 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
    (yyval.stringlist) = (yyvsp[-2].stringlist);
    if (strcmp((yyvsp[0].str), "emphasis")==0)
        ((yyval.stringlist))->push_back("box");
    else if (strcmp((yyvsp[0].str), "emptyemphasis")==0)
        ((yyval.stringlist))->push_back("emptybox");
    else ((yyval.stringlist))->push_back((yyvsp[0].str));
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
		csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[0].str))
        ((yyvsp[-2].stringlist))->emplace_back((yyvsp[0].str));
    (yyval.stringlist) = (yyvsp[-2].stringlist);
  #endif
    free((yyvsp[0].str));
}
#line 3427 "flow_lang.cc"
    break;

  case 98: /* shapedef: entity_string  */
#line 1311 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.AddCSH_ErrorAfter((yyloc), ("Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '"+string((yyvsp[0].str) ? (yyvsp[0].str) : "") +"'.").c_str());
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '"+string((yyvsp[0].str) ? (yyvsp[0].str) : "") +"'.");
  #endif
  free((yyvsp[0].str));
}
#line 3441 "flow_lang.cc"
    break;

  case 99: /* shapedef: entity_string TOK_OCBRACKET  */
#line 1321 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yylsp[0]));
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.AddCSH_ErrorAfter((yyloc), ("Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '"+string((yyvsp[-1].str) ? (yyvsp[-1].str): "") +"'.").c_str());
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '"+string((yyvsp[-1].str) ? (yyvsp[-1].str): "") +"'.");
  #endif
  free((yyvsp[-1].str));
}
#line 3457 "flow_lang.cc"
    break;

  case 100: /* shapedef: entity_string TOK_OCBRACKET shapedeflist  */
#line 1333 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yylsp[-1])+(yylsp[0]));
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-1]), COLOR_BRACE);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a closing brace ('}').");
	csh.AddShapeName((yyvsp[-2].str));
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), CHART_POS_AFTER((yylsp[0])), "Here is the corresponding '{'.");
    if ((yyvsp[0].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-2].str)), CHART_POS_START((yylsp[-2])), chart.file_url, chart.file_info, std::move(*(yyvsp[0].shape)), chart.Error);
	delete (yyvsp[0].shape);
    }
  #endif
  free((yyvsp[-2].str));
}
#line 3480 "flow_lang.cc"
    break;

  case 101: /* shapedef: entity_string TOK_OCBRACKET shapedeflist TOK_CCBRACKET  */
#line 1352 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-2]), COLOR_BRACE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
	csh.AddShapeName((yyvsp[-3].str));
    csh.BracePairs.push_back((yylsp[-2])+(yylsp[0]));
  #else
    if ((yyvsp[-1].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-3].str)), CHART_POS_START((yylsp[-3])), chart.file_url, chart.file_info, std::move(*(yyvsp[-1].shape)), chart.Error);
	delete (yyvsp[-1].shape);
    }
  #endif
  free((yyvsp[-3].str));
}
#line 3501 "flow_lang.cc"
    break;

  case 102: /* shapedef: entity_string TOK_OCBRACKET shapedeflist error TOK_CCBRACKET  */
#line 1369 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-3]), COLOR_BRACE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-3])+(yylsp[0]));
	csh.AddShapeName((yyvsp[-4].str));
    csh.AddCSH_Error((yylsp[-1]), "Only numbers can come after shape commands.");
  #else
    if ((yyvsp[-2].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-4].str)), CHART_POS_START((yylsp[-4])), chart.file_url, chart.file_info, std::move(*(yyvsp[-2].shape)), chart.Error);
	delete (yyvsp[-2].shape);
    }
  #endif
  free((yyvsp[-4].str));
}
#line 3523 "flow_lang.cc"
    break;

  case 103: /* shapedeflist: shapeline TOK_SEMICOLON  */
#line 1388 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    (yyval.shape) = new Shape;
	if ((yyvsp[-1].shapeelement)) {
		((yyval.shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
  #endif
}
#line 3539 "flow_lang.cc"
    break;

  case 104: /* shapedeflist: error shapeline TOK_SEMICOLON  */
#line 1400 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-2]), "I do not understand this.");
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
#else
    (yyval.shape) = new Shape;
	if ((yyvsp[-1].shapeelement)) {
		((yyval.shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
  #endif
}
#line 3556 "flow_lang.cc"
    break;

  case 105: /* shapedeflist: shapedeflist shapeline TOK_SEMICOLON  */
#line 1413 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
	if ((yyvsp[-1].shapeelement)) {
		((yyvsp[-2].shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
    (yyval.shape) = (yyvsp[-2].shape);
  #endif
}
#line 3572 "flow_lang.cc"
    break;

  case 106: /* shapedeflist: shapedeflist error  */
#line 1425 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Only numbers can come after shape commands.");
  #else
    (yyval.shape) = (yyvsp[-1].shape);
  #endif
}
#line 3584 "flow_lang.cc"
    break;

  case 107: /* shapeline: TOK_SHAPE_COMMAND  */
#line 1434 "flow_lang.yy"
{
    const int num_args = 0;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[0].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
	if (should_args != num_args)
		csh.AddCSH_ErrorAfter((yyloc), ShapeElement::ErrorMsg((yyvsp[0].shapecommand), num_args));
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args != num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[0].shapecommand), num_args).append(" Ignoring line."));
	else
	    (yyval.shapeelement) = new ShapeElement((yyvsp[0].shapecommand));
  #endif
}
#line 3604 "flow_lang.cc"
    break;

  case 108: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER  */
#line 1450 "flow_lang.yy"
{
    const int num_args = 1;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-1].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
	if (should_args != num_args) {
		string msg = ShapeElement::ErrorMsg((yyvsp[-1].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-1].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	} else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG && (((yyvsp[0].str))[0]<'0' || ((yyvsp[0].str))[0]>'2' || ((yyvsp[0].str))[1]!=0))
		csh.AddCSH_Error((yylsp[0]), "S (section) commands require an integer between 0 and 2.");
  #else
	(yyval.shapeelement) = nullptr;
	const double a = atof((yyvsp[0].str));
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yylsp[0])).end, ShapeElement::ErrorMsg((yyvsp[-1].shapecommand), num_args).append(" Ignoring line."));
	else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG && (a!=0 && a!=1 && a!=2))
		chart.Error.Error(CHART_POS_START((yylsp[0])), "S (section) commands require an integer between 0 and 2. Ignoring line.");
	else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG)
	    (yyval.shapeelement) = new ShapeElement(ShapeElement::Type((yyvsp[-1].shapecommand) + unsigned(a)));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-1].shapecommand), a);
  #endif
  free((yyvsp[0].str));
}
#line 3636 "flow_lang.cc"
    break;

  case 109: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER  */
#line 1478 "flow_lang.yy"
{
    const int num_args = 2;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-2].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
	if (should_args != num_args) {
		string msg = ShapeElement::ErrorMsg((yyvsp[-2].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-2].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-2].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-2].shapecommand), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 3664 "flow_lang.cc"
    break;

  case 110: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER entity_string  */
#line 1502 "flow_lang.yy"
{
    const int num_args = 3;
	  const int should_args = ShapeElement::GetNumArgs((yyvsp[-3].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
	  if (should_args != num_args) {
        std::string msg = ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args);
        switch (ShapeElement::GetNumArgs((yyvsp[-3].shapecommand))) {
        case 0:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
        case 1:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
        case 2:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
        }
  	} else if ((yyvsp[-3].shapecommand)!=ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[0]), "You need to specify a number here.");
  #else
  	(yyval.shapeelement) = nullptr;
	  if (should_args > num_args)
	      chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args).append(" Ignoring line."));
   	else if ((yyvsp[-3].shapecommand)!=ShapeElement::PORT)
	      chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a number here. Ignoring line.");
    else
		    (yyval.shapeelement) = new ShapeElement((yyvsp[-3].shapecommand), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 3697 "flow_lang.cc"
    break;

  case 111: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 1531 "flow_lang.yy"
{
    const int num_args = 3;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-3].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-3].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	} else if ((yyvsp[-3].shapecommand)==ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[0]), "You need to specify a port name here starting with a letter.");
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args).append(" Ignoring line."));
	else if ((yyvsp[-3].shapecommand)==ShapeElement::PORT)
		chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a port name here. Ignoring line.");
    else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-3].shapecommand), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 3730 "flow_lang.cc"
    break;

  case 112: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 1560 "flow_lang.yy"
{
    const int num_args = 4;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-4].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
	if (should_args != num_args) {
		string msg = ShapeElement::ErrorMsg((yyvsp[-4].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-4].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-4].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-4].shapecommand), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 3762 "flow_lang.cc"
    break;

  case 113: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 1588 "flow_lang.yy"
{
    const int num_args = 5;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-5].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-5].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-5].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-4]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 4:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-5].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-5].shapecommand), atof((yyvsp[-4].str)), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-4].str));
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 3796 "flow_lang.cc"
    break;

  case 114: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 1618 "flow_lang.yy"
{
    const int num_args = 6;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-6].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-6]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-6].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-6].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-5]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-4]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 4:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 5:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-6].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-6].shapecommand), atof((yyvsp[-5].str)), atof((yyvsp[-4].str)), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-5].str));
  free((yyvsp[-4].str));
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 3832 "flow_lang.cc"
    break;

  case 116: /* colordeflist: colordeflist TOK_COMMA colordef  */
#line 1652 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
  #endif
}
#line 3846 "flow_lang.cc"
    break;

  case 117: /* colordeflist: colordeflist TOK_COMMA  */
#line 1662 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
  #endif
}
#line 3863 "flow_lang.cc"
    break;

  case 120: /* colordef: alpha_string TOK_EQUAL color_string  */
#line 1678 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_COLORNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    ColorType color = csh.CurrentContext().Colors.GetColor((yyvsp[0].str));
    if (color.type!=ColorType::INVALID)
        csh.CurrentContext().Colors[(yyvsp[-2].str)] = color;
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent())
        chart.MyCurrentContext().colors.AddColor((yyvsp[-2].str), (yyvsp[0].str), chart.Error, CHART_POS((yyloc)));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 3890 "flow_lang.cc"
    break;

  case 121: /* colordef: alpha_string TOK_EQUAL TOK_PLUS_PLUS color_string  */
#line 1701 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_COLORNAME);
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    csh.AddCSH((yylsp[-1]), COLOR_COLORDEF);
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    ColorType color = csh.CurrentContext().Colors.GetColor("++"+string((yyvsp[0].str)));
    if (color.type!=ColorType::INVALID)
        csh.CurrentContext().Colors[(yyvsp[-3].str)] = color;
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent())
        chart.MyCurrentContext().colors.AddColor((yyvsp[-3].str), "++"+string((yyvsp[0].str)), chart.Error, CHART_POS((yyloc)));
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[0].str));
}
#line 3918 "flow_lang.cc"
    break;

  case 122: /* colordef: alpha_string TOK_EQUAL  */
#line 1725 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COLORNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing color definition.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing color definition.");
  #endif
    free((yyvsp[-1].str));
}
#line 3940 "flow_lang.cc"
    break;

  case 123: /* colordef: alpha_string  */
#line 1743 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COLORNAME);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing equal sign ('=') and a color definition.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing equal sign ('=') and a color definition.");
  #endif
    free((yyvsp[0].str));
}
#line 3958 "flow_lang.cc"
    break;

  case 124: /* designdef: TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON TOK_CCBRACKET  */
#line 1760 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_DESIGNNAME);
    csh.AddCSH((yylsp[-1]), COLOR_SEMICOLON);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-3])+(yylsp[0]));
    if (!csh.SkipContent()) {
        auto &d = csh.CurrentContext().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-4].str));
        if (i == d.end())
            d.emplace((yyvsp[-4].str), csh.CurrentContext());
        else
            i->second += csh.CurrentContext();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::LINE_START) ||
         csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) ) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent()) {
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-4].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-3]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
    free((yyvsp[-4].str));
}
#line 4001 "flow_lang.cc"
    break;

  case 125: /* designdef: TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON error TOK_CCBRACKET  */
#line 1799 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_DESIGNNAME);
    csh.AddCSH((yylsp[-2]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as part of a design definition.");
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-4])+(yylsp[0]));
    if (!csh.SkipContent()) {
        auto &d = csh.CurrentContext().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-5].str));
        if (i == d.end())
            d.emplace((yyvsp[-5].str), csh.CurrentContext());
        else
            i->second += csh.CurrentContext();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween((yylsp[-4]), (yylsp[-3]), EHintSourceType::LINE_START) ||
         csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::LINE_START))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    if (!chart.SkipContent()) {
        //if closing brace missing, still do the design definition
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-5].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-4]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
}
#line 4045 "flow_lang.cc"
    break;

  case 126: /* scope_open_empty: TOK_OCBRACKET  */
#line 1841 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(true, EContextParse::NORMAL);
  #else
    //push empty color & style sets for design definition
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::NORMAL, EContextCreate::CLEAR);
  #endif
}
#line 4059 "flow_lang.cc"
    break;

  case 128: /* designelementlist: designelementlist TOK_SEMICOLON designelement  */
#line 1853 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SEMICOLON);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#endif
}
#line 4073 "flow_lang.cc"
    break;

  case 129: /* designelement: TOK_COMMAND_DEFCOLOR colordeflist  */
#line 1864 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    free((yyvsp[-1].str));
}
#line 4091 "flow_lang.cc"
    break;

  case 130: /* designelement: TOK_COMMAND_DEFCOLOR  */
#line 1878 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
  #endif
    free((yyvsp[0].str));
}
#line 4112 "flow_lang.cc"
    break;

  case 131: /* designelement: TOK_COMMAND_DEFSTYLE styledeflist  */
#line 1895 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
  #endif
    free((yyvsp[-1].str));
}
#line 4130 "flow_lang.cc"
    break;

  case 132: /* designelement: TOK_COMMAND_DEFSTYLE  */
#line 1909 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing style name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
  #endif
    free((yyvsp[0].str));
}
#line 4151 "flow_lang.cc"
    break;

  case 135: /* designoptlist: designoptlist TOK_COMMA designopt  */
#line 1929 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 4165 "flow_lang.cc"
    break;

  case 136: /* designoptlist: designoptlist TOK_COMMA  */
#line 1939 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 4179 "flow_lang.cc"
    break;

  case 137: /* designoptlist: designoptlist error  */
#line 1949 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Extra stuff after design options. Maybe missing a comma?");
  #endif
}
#line 4189 "flow_lang.cc"
    break;

  case 138: /* designopt: entity_string TOK_EQUAL TOK_NUMBER  */
#line 1956 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].str), COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str))) {
        FlowChart::AttributeValues((yyvsp[-2].str), csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.AddDesignAttribute(Attribute((yyvsp[-2].str), (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 4212 "flow_lang.cc"
    break;

  case 139: /* designopt: entity_string TOK_EQUAL string  */
#line 1975 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].str), COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str))) {
        FlowChart::AttributeValues((yyvsp[-2].str), csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.AddDesignAttribute(Attribute((yyvsp[-2].str), (yyvsp[0].str), CHART_POS((yylsp[-2])), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 4235 "flow_lang.cc"
    break;

  case 140: /* designopt: entity_string TOK_EQUAL  */
#line 1994 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].str), COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].str))) {
        FlowChart::AttributeValues((yyvsp[-1].str), csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value. Ignoring this.");
#endif
    free((yyvsp[-1].str));
}
#line 4256 "flow_lang.cc"
    break;

  case 141: /* colon_string: TOK_COLON_QUOTED_STRING  */
#line 2012 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), false);
    csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 4268 "flow_lang.cc"
    break;

  case 142: /* colon_string: TOK_COLON_STRING  */
#line 2020 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), true);
	csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 4280 "flow_lang.cc"
    break;

  case 143: /* full_attrlist_with_label: colon_string  */
#line 2029 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attributelist) = (new AttributeList)->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yyloc)).IncStartCol(), true));
  #endif
    free((yyvsp[0].str));
}
#line 4292 "flow_lang.cc"
    break;

  case 144: /* full_attrlist_with_label: colon_string full_attrlist  */
#line 2037 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attributelist) = ((yyvsp[0].attributelist))->Prepend(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol(), true));
  #endif
    free((yyvsp[-1].str));
}
#line 4304 "flow_lang.cc"
    break;

  case 145: /* full_attrlist_with_label: full_attrlist colon_string full_attrlist  */
#line 2045 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attributelist) = ((yyvsp[-2].attributelist))->Append(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol(), true));
        //Merge $3 at the end of $1
        ((yyvsp[-2].attributelist))->splice(((yyvsp[-2].attributelist))->end(), *((yyvsp[0].attributelist)));
        delete ((yyvsp[0].attributelist)); //empty list now
        (yyval.attributelist) = (yyvsp[-2].attributelist);
  #endif
    free((yyvsp[-1].str));
}
#line 4320 "flow_lang.cc"
    break;

  case 146: /* full_attrlist_with_label: full_attrlist colon_string  */
#line 2057 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attributelist) = ((yyvsp[-1].attributelist))->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yylsp[0])), CHART_POS((yylsp[0])).IncStartCol(), true));
  #endif
    free((yyvsp[0].str));
}
#line 4332 "flow_lang.cc"
    break;

  case 148: /* full_attrlist: TOK_OSBRACKET TOK_CSBRACKET  */
#line 2068 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = new AttributeList;
  #endif
}
#line 4347 "flow_lang.cc"
    break;

  case 149: /* full_attrlist: TOK_OSBRACKET attrlist TOK_CSBRACKET  */
#line 2079 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
	csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
  #endif
}
#line 4362 "flow_lang.cc"
    break;

  case 150: /* full_attrlist: TOK_OSBRACKET attrlist error TOK_CSBRACKET  */
#line 2090 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[-1]), "Extra stuff after an attribute list. Maybe missing a comma?");
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = (yyvsp[-2].attributelist);
  #endif
}
#line 4378 "flow_lang.cc"
    break;

  case 151: /* full_attrlist: TOK_OSBRACKET error TOK_CSBRACKET  */
#line 2102 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as an attribute or style name.");
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting an attribute or style name. Ignoring all until the closing square bracket (']').");
#endif
}
#line 4395 "flow_lang.cc"
    break;

  case 152: /* full_attrlist: TOK_OSBRACKET attrlist  */
#line 2115 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a square bracket (']').");
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
	csh.SqBracketPairs.push_back((yyloc));
  #else
    (yyval.attributelist) = (yyvsp[0].attributelist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 4411 "flow_lang.cc"
    break;

  case 153: /* full_attrlist: TOK_OSBRACKET attrlist error  */
#line 2127 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[0]), "Missing a ']'.");
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing ']'.");
  #endif
}
#line 4427 "flow_lang.cc"
    break;

  case 154: /* full_attrlist: TOK_OSBRACKET  */
#line 2139 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a square bracket (']').");
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 4443 "flow_lang.cc"
    break;

  case 155: /* full_attrlist: TOK_OSBRACKET TOK__NEVER__HAPPENS  */
#line 2153 "flow_lang.yy"
{
    (yyval.attributelist) = nullptr;
}
#line 4451 "flow_lang.cc"
    break;

  case 156: /* full_attrlist: TOK_OSBRACKET error  */
#line 2157 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[0]), "Missing a ']'.");
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing ']'.");
  #endif
}
#line 4467 "flow_lang.cc"
    break;

  case 157: /* attrlist: attr  */
#line 2170 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.attributelist) = (new AttributeList)->Append((yyvsp[0].attribute));
  #endif
}
#line 4478 "flow_lang.cc"
    break;

  case 158: /* attrlist: attrlist TOK_COMMA attr  */
#line 2177 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = ((yyvsp[-2].attributelist))->Append((yyvsp[0].attribute));
  #endif
}
#line 4491 "flow_lang.cc"
    break;

  case 159: /* attrlist: attrlist TOK_COMMA  */
#line 2186 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing attribute or style name.");
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an attribute or style name here.");
  #endif
}
#line 4506 "flow_lang.cc"
    break;

  case 161: /* attr: alpha_string TOK_EQUAL color_string  */
#line 2200 "flow_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].str), COLOR_ATTRNAME);
        csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), (yyvsp[-2].str));
        csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str));
  #else
        (yyval.attribute) = new Attribute((yyvsp[-2].str), (yyvsp[0].str), CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 4525 "flow_lang.cc"
    break;

  case 162: /* attr: alpha_string TOK_EQUAL TOK_PLUS_PLUS color_string  */
#line 2215 "flow_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
        csh.AddCSH_AttrName((yylsp[-3]), (yyvsp[-3].str), COLOR_ATTRNAME);
        csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[-1])+(yylsp[0]), (string("++")+(yyvsp[0].str)).c_str(), (yyvsp[-3].str));
        csh.CheckHintAt((yylsp[-3]), EHintSourceType::ATTR_NAME);
        csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1])+(yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-3].str));
  #else
        (yyval.attribute) = new Attribute((yyvsp[-3].str), string("++")+(yyvsp[0].str), CHART_POS((yylsp[-3])), CHART_POS2((yylsp[-1]),(yylsp[0])));
  #endif
    free((yyvsp[-3].str));
    free((yyvsp[0].str));
}
#line 4544 "flow_lang.cc"
    break;

  case 163: /* attr: alpha_string TOK_EQUAL TOK_PLUS_PLUS  */
#line 2230 "flow_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].str), COLOR_ATTRNAME);
        csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), "++", (yyvsp[-2].str));
		csh.AddCSH_ErrorAfter((yylsp[0]), "Continue with a color name or definition.");
        csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str));
  #else
        (yyval.attribute) = new Attribute((yyvsp[-2].str), "++", CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].str));
}
#line 4563 "flow_lang.cc"
    break;

  case 164: /* attr: alpha_string TOK_EQUAL TOK_NUMBER  */
#line 2245 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].str), COLOR_ATTRNAME);
        csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
        csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].str));
  #else
        (yyval.attribute) = new Attribute((yyvsp[-2].str), (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 4581 "flow_lang.cc"
    break;

  case 165: /* attr: alpha_string TOK_EQUAL  */
#line 2259 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].str), COLOR_ATTRNAME);
        csh.AddCSH((yylsp[0]), COLOR_EQUAL);
        csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME);
        csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].str));
  #else
        (yyval.attribute) = new Attribute((yyvsp[-1].str), {}, CHART_POS((yyloc)), CHART_POS((yyloc)));
  #endif
    free((yyvsp[-1].str));
}
#line 4597 "flow_lang.cc"
    break;

  case 166: /* attr: string TOK_EQUAL TOK__NEVER__HAPPENS  */
#line 2273 "flow_lang.yy"
{
    (yyval.attribute) = nullptr;
    free((yyvsp[-2].str));
}
#line 4606 "flow_lang.cc"
    break;

  case 167: /* attr: string  */
#line 2278 "flow_lang.yy"
{
  //here we accept non alpha strings for "->" and similar style names
  #ifdef C_S_H_IS_COMPILED
        csh.AddCSH_StyleOrAttrName((yylsp[0]), (yyvsp[0].str));
        csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
        (yyval.attribute) = new Attribute((yyvsp[0].str), CHART_POS((yyloc)));
  #endif
    free((yyvsp[0].str));
}
#line 4621 "flow_lang.cc"
    break;

  case 169: /* entity_string: TOK_QSTRING  */
#line 2307 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddQuotedString((yylsp[0]));
  #endif
  (yyval.str) = (yyvsp[0].str);
}
#line 4632 "flow_lang.cc"
    break;

  case 170: /* entity_string: TOK_SHAPE_COMMAND  */
#line 2314 "flow_lang.yy"
{
	(yyval.str) = (char*)malloc(2);
	((yyval.str))[0] = ShapeElement::act_code[(yyvsp[0].shapecommand)];
	((yyval.str))[1] = 0;
}
#line 4642 "flow_lang.cc"
    break;

  case 182: /* symbol_string: TOK__NEVER__HAPPENS2  */
#line 2328 "flow_lang.yy"
{
    (yyval.str) = nullptr;
}
#line 4650 "flow_lang.cc"
    break;

  case 187: /* scope_open: TOK_OCBRACKET  */
#line 2337 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext();
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.PushContext(CHART_POS_START((yylsp[0])));
  #endif
}
#line 4667 "flow_lang.cc"
    break;

  case 189: /* scope_close: TOK_CCBRACKET  */
#line 2354 "flow_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.instruction) = nullptr;
    csh.PopContext();
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
  #else
    (yyval.instruction) = chart.PopContext().release();
  #endif
}
#line 4681 "flow_lang.cc"
    break;


#line 4685 "flow_lang.cc"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= TOK_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == TOK_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, RESULT, yyscanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, RESULT, yyscanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, RESULT, yyscanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 2365 "flow_lang.yy"



/* END OF FILE */
