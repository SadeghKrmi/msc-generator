%option reentrant noyywrap nounput
%option bison-bridge bison-locations

%{
    /*
        This file is part of Msc-generator.
        Copyright (C) 2008-2022 Zoltan Turanyi
        Distributed under GNU Affero General Public License.

        Msc-generator is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        Msc-generator is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
        */

#include <stdio.h>
#include <string.h>
#include <iostream>

#ifndef HAVE_UNISTD_H
#define YY_NO_UNISTD_H
extern int isatty(int);
#endif

#include "cgen_shapes.h"
#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #include "flowcsh.h"
    #define YYMSC_RESULT_TYPE flow::FlowCsh
    #define RESULT csh
    #define YYLTYPE_IS_DECLARED  //If we scan for color syntax highlight use this location type (YYLTYPE)
    #define YYLTYPE CshPos
    #define YYGET_EXTRA flowcsh_get_extra
    #define CHAR_IF_CSH(A) char
    #include "flow_csh_lang.h"   //Must be after language_misc.h
#else
    #define C_S_H (0)
    #include "flowchart.h"
    #include "steps.h"
    #define YYMSC_RESULT_TYPE flow::FlowChart
    #define RESULT chart
    #define YYGET_EXTRA flow_get_extra
    #define CHAR_IF_CSH(A) A
    #include "parse_tools.h"
    using namespace flow;
    #include "flow_lang.h"
#endif

#include "flow_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined and after 'using namespace'

%}

%%

 /* Newline characters in all forms accepted */
\x0d\x0a     %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

\x0a         %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

\x0d         %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

 /* # starts a comment last until end of line */
#[^\x0d\x0a]* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #endif
%}

 /* // starts a comment last until end of line */
\/\/[^\x0d\x0a]* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #endif
%}

 /* / * .. * / block comments*/
\/\*([^\*]*(\*[^\/])?)*\*\/ %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #else
    language_process_block_comments(yytext, yylloc);
  #endif
%}

 /* / * ... unclosed block comments */
\/\* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
    yyget_extra(yyscanner)->csh->AddCSH_Error(*yylloc, "Unpaired beginning of block comment '/" "*'.");
  #else
    yyget_extra(yyscanner)->chart->Error.Error(CHART_POS_START(*yylloc),
         "Unpaired beginning of block comment '/" "*'.");
  #endif
%}

[ \t]+    /* ignore whitespace */;

 /* These shape definition keywords are case sensitive */
M	yylval_param->shapecommand = ShapeElement::MOVE_TO; return TOK_SHAPE_COMMAND;
L	yylval_param->shapecommand = ShapeElement::LINE_TO; return TOK_SHAPE_COMMAND;
C	yylval_param->shapecommand = ShapeElement::CURVE_TO; return TOK_SHAPE_COMMAND;
E	yylval_param->shapecommand = ShapeElement::CLOSE_PATH; return TOK_SHAPE_COMMAND;
S	yylval_param->shapecommand = ShapeElement::SECTION_BG; return TOK_SHAPE_COMMAND;
T	yylval_param->shapecommand = ShapeElement::TEXT_AREA; return TOK_SHAPE_COMMAND;
H	yylval_param->shapecommand = ShapeElement::HINT_AREA; return TOK_SHAPE_COMMAND;
P	yylval_param->shapecommand = ShapeElement::PORT; return TOK_SHAPE_COMMAND;


 /* These keywords are case insensitive */
(?i:if)        yylval_param->str = strdup(yytext); return TOK_IF;
(?i:step)      yylval_param->str = strdup(yytext); return TOK_STEP;
(?i:box)       yylval_param->str = strdup(yytext); return TOK_STEP;
(?i:repeat)    yylval_param->str = strdup(yytext); return TOK_REPEAT;
(?i:branch)    yylval_param->str = strdup(yytext); return TOK_BRANCH;
(?i:then)      yylval_param->str = strdup(yytext); return TOK_THEN;
(?i:else)      yylval_param->str = strdup(yytext); return TOK_ELSE;
(?i:yes)       yylval_param->str = strdup(yytext); return TOK_YES;
(?i:no)        yylval_param->str = strdup(yytext); return TOK_NO;
(?i:start)     yylval_param->str = strdup(yytext); return TOK_START;
(?i:stop)      yylval_param->str = strdup(yytext); return TOK_STOP;
(?i:goto)      yylval_param->str = strdup(yytext); return TOK_GOTO;
(?i:bye)       yylval_param->str = strdup(yytext); return TOK_BYE;
(?i:defshape)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFSHAPE;
(?i:defcolor)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFCOLOR;
(?i:defstyle)  yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFSTYLE;
(?i:defdesign) yylval_param->str = strdup(yytext); return TOK_COMMAND_DEFDESIGN;


=        return TOK_EQUAL;
,        return TOK_COMMA;
\;       return TOK_SEMICOLON;
\[       return TOK_OSBRACKET;
\]       return TOK_CSBRACKET;
\{       return TOK_OCBRACKET;
\}       return TOK_CCBRACKET;
\+\+     return TOK_PLUS_PLUS;


 /* This is a colon-quoted string, finished by a quotation mark (UTF-8 allowed)
 ** : "<string>"
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*\" %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    {
    /* after whitespaces we are guaranteed to have a tailing and heading quot */
    char *s = remove_head_tail_whitespace(yytext+1);
    /* s now points to the heading quotation marks.
    ** Now get rid of both quotation marks */
    std::string str(s+1);
    str.erase(str.length()-1);
    /* Calculate the position of the string and prepend a location escape */
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s+1 - yytext));
    yylval_param->str = strdup((pos.Print() + str).c_str());
    }
  #endif
    return TOK_COLON_QUOTED_STRING;
%}

 /* This is a colon-quoted string, finished by a newline (trailing context) (UTF-8 allowed)
 ** : "<string>$
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*  %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
  #else
    {
    /* after whitespaces we are guaranteed to have a heading quot */
    const char *s = remove_head_tail_whitespace(yytext+1);
    // s now points to heading quotation mark
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s - yytext));
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    /* Advance pos beyond the leading quotation mark */
    pos.col++;
    yylval_param->str = strdup((pos.Print() + (s+1)).c_str());
    }
  #endif
    return TOK_COLON_QUOTED_STRING;
%}

 /* This is a non quoted colon-string
 ** : <string>
 ** terminated by any of: [ { or ;
 ** Honors escaping of the above via a backslash. (UTF-8 allowed)
 ** Can contain quotation marks (escaped or unescaped), but can not start with it
 ** If it contains a hashmark, unescaped [ { or ; is allowed till the end of the line
 ** (representing a commented section inside a label)
 ** Not available in mscgen compatibility mode. There we use the one below
 *  \:[\t]*(((#[^\x0d\x0a]*)|[^\"\;\[\{\\]|(\\.))((#[^\x0d\x0a]*)|[^\;\[\{\\]|(\\.))*(\\)?|\\)
 * \:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*
 */
<INITIAL>\:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
  #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc);
  #endif
    return TOK_COLON_STRING;
%}

 /* This is a degenerate non quoted colon-string
 ** a colon followed by a solo escape or just a colon
 */
\:[ \t]*\\? %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext);
   #else
    yylval_param->str = process_colon_string(yytext, CHART_POS_START(*yylloc), yylloc);
  #endif
    return TOK_COLON_STRING;
%}


 /* A simple quoted string, that can have escaped quotation marks inside. (UTF-8 allowed)*/
\"([^\"\x0d\x0a]|(\\\"))*\" %{
    yylval_param->str = strdup(yytext+1);
    yylval_param->str[strlen(yylval_param->str) - 1] = '\0';
    return TOK_QSTRING;
%}

 /* A simple quoted string, missing a closing quotation mark (UTF-8 allowed)*/
\"([^\"\x0d\x0a]|(\\\"))* %{
  #ifdef C_S_H_IS_COMPILED
    yylval_param->str = strdup(yytext+1);
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
#else
    {
    yylval_param->str = strdup(yytext+1);
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column);
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    }
  #endif
    return TOK_QSTRING;
%}


 /* Numbers */
[+\-]?[0-9]+\.?[0-9]*  %{
    yylval_param->str = strdup(yytext);
    return TOK_NUMBER;
%}

 /* Strings not ending with a dot. We allow any non ASCII UTF-8 character through. */
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])* %{
    yylval_param->str = strdup(yytext);
    return TOK_STRING;
%}

 /* Strings ending with a dot, not followed by a second dot .
  * We allow any non ASCII UTF-8 character through.
  * Two dots one after another shall be parsed a '..' into TOK_EMPH*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*\./[^\.] %{
    yylval_param->str = strdup(yytext);
    return TOK_STRING;
%}

 /* Color definitions. We allow any non ASCII UTF-8 character in the color name. */
 /* string+-number[,number]*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*(\.)?[\+\-][0-9]+(\.[0-9]*)?(\,[0-9]+(\.[0-9]*)?)? %{
    yylval_param->str = strdup(yytext);
    return TOK_COLORDEF;
%}

 /* string,number[+-number]. We allow any non ASCII UTF-8 character in 'string'.*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*(\.)?\,[0-9]+(\.[0-9]*)?([\+\-][0-9]+(\.[0-9]*)?)? %{
    yylval_param->str = strdup(yytext);
    return TOK_COLORDEF;
%}

 /* number,number,number[,number] */
[0-9]+(\.[0-9]*)?\,[0-9]+(\.[0-9]*)?\,[0-9]+(\.[0-9]*)?(\,[0-9]+(\.[0-9]*)?)? %{
    yylval_param->str = strdup(yytext);
    return TOK_COLORDEF;
%}

<<EOF>> return TOK_EOF;

 /* Ignore (terminating) zero */
\0

. return TOK_UNRECOGNIZED_CHAR;

%%

/* END OF FILE */
