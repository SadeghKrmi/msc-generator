/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_FLOWCSH_FLOW_CSH_LANG_H_INCLUDED
# define YY_FLOWCSH_FLOW_CSH_LANG_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int flowcsh_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    TOK_EOF = 0,                   /* TOK_EOF  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    TOK_STRING = 258,              /* TOK_STRING  */
    TOK_QSTRING = 259,             /* TOK_QSTRING  */
    TOK_NUMBER = 260,              /* TOK_NUMBER  */
    TOK_DASH = 261,                /* TOK_DASH  */
    TOK_EQUAL = 262,               /* TOK_EQUAL  */
    TOK_COMMA = 263,               /* TOK_COMMA  */
    TOK_SEMICOLON = 264,           /* TOK_SEMICOLON  */
    TOK_PLUS_PLUS = 265,           /* TOK_PLUS_PLUS  */
    TOK_OCBRACKET = 266,           /* TOK_OCBRACKET  */
    TOK_CCBRACKET = 267,           /* TOK_CCBRACKET  */
    TOK_OSBRACKET = 268,           /* TOK_OSBRACKET  */
    TOK_CSBRACKET = 269,           /* TOK_CSBRACKET  */
    TOK_IF = 270,                  /* TOK_IF  */
    TOK_STEP = 271,                /* TOK_STEP  */
    TOK_REPEAT = 272,              /* TOK_REPEAT  */
    TOK_START = 273,               /* TOK_START  */
    TOK_STOP = 274,                /* TOK_STOP  */
    TOK_GOTO = 275,                /* TOK_GOTO  */
    TOK_BRANCH = 276,              /* TOK_BRANCH  */
    TOK_THEN = 277,                /* TOK_THEN  */
    TOK_ELSE = 278,                /* TOK_ELSE  */
    TOK_YES = 279,                 /* TOK_YES  */
    TOK_NO = 280,                  /* TOK_NO  */
    TOK_SHAPE_COMMAND = 281,       /* TOK_SHAPE_COMMAND  */
    TOK_BYE = 282,                 /* TOK_BYE  */
    TOK_COLON_STRING = 283,        /* TOK_COLON_STRING  */
    TOK_COLON_QUOTED_STRING = 284, /* TOK_COLON_QUOTED_STRING  */
    TOK_COLORDEF = 285,            /* TOK_COLORDEF  */
    TOK_COMMAND_DEFSHAPE = 286,    /* TOK_COMMAND_DEFSHAPE  */
    TOK_COMMAND_DEFCOLOR = 287,    /* TOK_COMMAND_DEFCOLOR  */
    TOK_COMMAND_DEFSTYLE = 288,    /* TOK_COMMAND_DEFSTYLE  */
    TOK_COMMAND_DEFDESIGN = 289,   /* TOK_COMMAND_DEFDESIGN  */
    TOK__NEVER__HAPPENS = 290,     /* TOK__NEVER__HAPPENS  */
    TOK__NEVER__HAPPENS2 = 291,    /* TOK__NEVER__HAPPENS2  */
    TOK_UNRECOGNIZED_CHAR = 292    /* TOK_UNRECOGNIZED_CHAR  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 84 "flow_lang.yy"

    gsl::owner<char*>                       str;
    gsl::owner<std::list<std::string>*>     stringlist;
    ShapeElement::Type                      shapecommand;
    CHAR_IF_CSH(Step::KeywordHelper)        steptype; 
    gsl::owner<CHAR_IF_CSH(Shape)*>         shape;
    gsl::owner<CHAR_IF_CSH(ShapeElement)*>  shapeelement;
    gsl::owner<CHAR_IF_CSH(Attribute)*>     attribute;
    gsl::owner<CHAR_IF_CSH(AttributeList)*> attributelist;
    gsl::owner<CHAR_IF_CSH(FlowElement)*>   instruction;
    gsl::owner<CHAR_IF_CSH(FElementList)*>  instruction_list;
    gsl::owner<CHAR_IF_CSH(Branch)*>        branch;

#line 115 "flow_csh_lang.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif




int flowcsh_parse (YYMSC_RESULT_TYPE &RESULT, void *yyscanner);


#endif /* !YY_FLOWCSH_FLOW_CSH_LANG_H_INCLUDED  */
