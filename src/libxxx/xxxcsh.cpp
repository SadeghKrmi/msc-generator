/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file xxxcsh.cpp The definition for the XxxCsh class and coloring for the "xxx" language.
* @ingroup libxxx_files */


#include <cstring>
#include "xxxcsh.h"
#include "xxxstyle.h"

using namespace xxx;

XxxCsh::XxxCsh(Csh::FileListProc proc, void *param) :
    Csh(XxxContext(true, EContextParse::NORMAL, EContextCreate::PLAIN, FileLineCol()), proc, param)
{
    FillNamesHints();
}

void XxxCsh::FillNamesHints()
{
    //Uncomment if you have options
    //AddOptionsToHints();
    //MoveHintsToOptionNames();

    //Uncomment if you have keywords
    //AddKeywordToHints();
    //MoveHintsToKeywordNames();

    //Uncomment if you do attributes
    //AttributeNames(GraphStyle::ANY);
    //MoveHintsToAttrNames();
}

/** Parse chart text for color syntax and hint info
 * @param [in] input The chart text
 * @param [in] cursor_p The current position of the cursor.
 * @param [in] pedantic The initial value of the pedantic chart option.*/
void XxxCsh::ParseText(const std::string& input, int cursor_p, bool pedantic)
{
    //initialize data struct
    BeforeYaccParse(input, cursor_p);
    (void)pedantic;
    //call parsing
    XxxCshParse(*this, input_text.c_str(), unsigned(input_text.length()+1)); //include terminating zero
    //Tidy up afterwards
    AfterYaccParse();
}
