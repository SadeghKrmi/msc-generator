/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file xxxcsh.h The declaration for the XxxCsh class.
* @ingroup libxxx_files */

#ifndef XXX_CSH_H
#define XXX_CSH_H

#include "csh.h"
#include "xxxstyle.h"

namespace xxx {


/** Coloring for Xxx. */
class XxxCsh : public Csh
{
public:
    XxxCsh(Csh::FileListProc proc, void *param);
    ~XxxCsh() override = default;
    std::unique_ptr<Csh> Clone() const override { return std::make_unique<XxxCsh>(*this); }

    // Inherited via Csh
    void FillNamesHints() override;
    void ParseText(const std::string& input, int cursor_p, bool pedantic) override;

};

}; //namespace

/** Yacc generated function to do the csh parsing of a graph. defined in gv_csh_lang.cpp */
extern void XxxCshParse(xxx::XxxCsh &csh, const char *buff, unsigned len);

#endif