/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file xxxstyle.h Xxx specific style and context declarations.
* @ingroup libxxx_files */

#ifndef XXX_STYLE_H
#define XXX_STYLE_H

#include "style.h"

namespace xxx {


/** Style for Xxx charts.*/
class XxxStyle : public SimpleStyleWithArrow<FullArrowAttr>
{
    friend class XxxContext;
    XxxStyle(EStyleType tt, EColorMeaning cm, EArcArrowType a,
             bool t, bool l, bool f, bool s, bool nu, bool shp, bool x); ///Control which components are used
    bool f_xxx;
public:
    ///Add your own attributes
    XxxStyle(EStyleType tt = EStyleType::STYLE, EColorMeaning cm = EColorMeaning::NOHOW); //Has all the components, but is empty
    XxxStyle(const XxxStyle&) = default;
    XxxStyle(XxxStyle&&) = default;
    XxxStyle &operator=(const XxxStyle&) = default;
    XxxStyle &operator=(XxxStyle&&) = default;
    void Empty() override;
    bool IsEmpty() const noexcept override { return SimpleStyleWithArrow<FullArrowAttr>::IsEmpty(); }
    void MakeCompleteButText() override;
    Style &operator +=(const Style &toadd) override;
    bool AddAttribute(const Attribute &a, Chart *) override;
    void AttributeNames(Csh &csh) const override;
    bool AttributeValues(std::string_view attr, Csh &csh) const override;
};

/** Context for Graphs. Just a styleset and colors, really.*/
class XxxContext : public ContextBase<XxxStyle>
{
public:
    /** This constructor is used to create an empty context or one set to plain.
     * Used an initializing the first context at parse, when starting to record 
     * a design, storing a procedure or moving a newly defined design to the design 
     * store.
     * @param [in] f If true, the context contains a value for all styles and attributes (Full)
     * @param [in] p Tells us what components to observe and how to behave during parsing.
     * @param [in] t Tells us with what content to create the context. It should not be 'COPY
     * @param [in] l The first character of the context in the input file.*/
    XxxContext(bool f, EContextParse p, EContextCreate t, const FileLineCol &l) : 
        ContextBase<XxxStyle>(f, p, EContextCreate::CLEAR, l)
    { switch (t) {
        default: _ASSERT(0); FALLTHROUGH;
        case EContextCreate::PLAIN: Plain(); break;
        case EContextCreate::EMPTY: Empty(); break; 
        case EContextCreate::CLEAR: /*clear all members you add */ break;}}
    /** This constructor is used when a context needs to be duplicated due to 
     * the opening of a new scope. You can change the parse mode, e.g., when
     * parsing the not-selected branch of an ifthenelse.*/
    XxxContext(const XxxContext &o, EContextParse p, const FileLineCol &l) :
        ContextBase<XxxStyle>(o, p, l) /* copy initialize your added members*/
        {}
    /** Semi copy operator: copy only user-defined context content. If o is full, we overwrite us, else just merge.*/
    void ApplyContextContent(const XxxContext &o);
    /** Semi move operator: move only user-defined context content. If o is full, we overwrite us, else just merge.*/
    void ApplyContextContent(XxxContext &&o);
    void Empty() override;
    void Plain() override;
};

}; //namespace

#endif //XXX_STYLE_H
