#!/bin/sh
if [ "$3" == ""]; then
    echo "I need the name of the new module as a parameter."
    echo "Like: 'change.sh new New NEW'."
    exit 1
fi
echo "Removing garbage..."
rm -f *.o
rm -f *.output
rm -f *~
rm -f *.pre_cc
echo "Renaming files..."
rename "s/new/$1/" *
echo "Changing file content..."
sed -i "s/new/$1/g" *
sed -i "s/New/$2/g" *
sed -i "s/NEW/$3/g" *
echo "Done."


