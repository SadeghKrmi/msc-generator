%skeleton "lalr1.cc" /* -*- C++ -*- */
%require "3.0.4"
%locations
%defines
%define api.namespace {yyns}
%define api.value.type variant
%define api.token.constructor
%define parse.assert
%define api.location.type {YYMSC_LOCATION_TYPE}
%param{YYMSC_RESULT_TYPE &RESULT}
%param{YYMSC_PARAM_TYPE &param}
%initial-action
{
  #ifdef C_S_H_IS_COMPILED
    @$.first_pos = 0;
    @$.last_pos = 0;
  #else 
    @$.start.file = chart.Error.Files.size()-1;
    @$.end.file = chart.Error.Files.size()-1;
  #endif
};

%code requires{
/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2021 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT //so that subsequent definition in xxx_csh_lang2.h causes no warning
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #define YYMSC_LOCATION_TYPE CshPos
    #define YYMSC_RESULT_TYPE XxxCsh
    #define YYMSC_PARAM_TYPE xxx_parse_parm_csh
    #define RESULT csh
    #define CHAR_IF_CSH(A) char
    #define UPTR_NOT_CSH(A) char
    #define yyns xxx::cshparser //namespace
    #define YY_DECL yyns::parser::symbol_type xxxcsh_lex(YYMSC_RESULT_TYPE &RESULT, YYMSC_PARAM_TYPE &param)
    #include "cgen_shapes.h"
    #include "xxxcsh.h"
    #include "xxxchart.h"
#else
    #define C_S_H (0)
    #define YYMSC_LOCATION_TYPE FileLineColRange
    #define YYMSC_RESULT_TYPE XxxChart
    #define YYMSC_PARAM_TYPE xxx_parse_parm
    #define RESULT chart
    #define CHAR_IF_CSH(A) A
    #define UPTR_NOT_CSH(A) std::unique_ptr<A>
    #define AFTER(pos) pos.end.NextChar()
    #define yyns xxx::parser //namespace
    #define YY_DECL yyns::parser::symbol_type xxx_lex(YYMSC_RESULT_TYPE &RESULT, YYMSC_PARAM_TYPE &param)
    #include "cgen_shapes.h"
    #include "xxxchart.h"

    #define YYLLOC_DEFAULT(Current, Rhs, N)                                 \
        do                                                                  \
          if (N)                                                            \
            {                                                               \
              (Current).start  = YYRHSLOC (Rhs, 1).start;                   \
              (Current).end    = YYRHSLOC (Rhs, N).end;                     \
            }                                                               \
          else                                                              \
            {                                                               \
              (Current).start = (Current).end = YYRHSLOC (Rhs, 0).end;      \
            }                                                               \
        while (/*CONSTCOND*/ false)

    using const_Procedure_ptr = const Procedure *; //Need this, since bison fails on plain 'const Procedure *' as %type

#endif

using const_char_ptr = const char *;

#include "xxx_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined

using namespace xxx;

}

%token TOK_DASH TOK_EQUAL TOK_COMMA
       TOK_SEMICOLON TOK_PLUS_PLUS 
       TOK_OSBRACKET TOK_CSBRACKET
       TOK_TILDE TOK_OPARENTHESIS TOK_CPARENTHESIS
       TOK_UNRECOGNIZED_CHAR TOK__NEVER__HAPPENS TOK_EOF 0

%type <MultiString> entity_string string alpha_string
                   entity_string_single_or_param 
                   multi_string_continuation tok_param_name_as_multi
%token <std::string> TOK_STRING TOK_QSTRING TOK_COLON_STRING TOK_COLON_QUOTED_STRING TOK_COLORDEF
                     TOK_NUMBER TOK_BYE TOK_PARAM_NAME
                     TOK_COMMAND_DEFSHAPE TOK_COMMAND_DEFCOLOR TOK_COMMAND_DEFSTYLE TOK_COMMAND_DEFDESIGN
                     TOK_COMMAND_DEFPROC TOK_COMMAND_REPLAY TOK_COMMAND_SET 
                     TOK_IF TOK_THEN TOK_ELSE TOK_COMMAND_INCLUDE
                     TOK_XXX
%type <std::string> include entity_string_single string_single alpha_string_single
                     reserved_word_string symbol_string color_string colon_string
%type <std::list<std::string>> stylenamelist
%token <ShapeElement::Type> TOK_SHAPE_COMMAND
%type <CHAR_IF_CSH(std::optional<ShapeElement>)> shapeline
%type <UPTR_NOT_CSH(Shape)> shapedeflist
%type <UPTR_NOT_CSH(Attribute)> attr
%type <CHAR_IF_CSH(AttributeList)> full_attrlist_with_label full_attrlist attrlist
%type <UPTR_NOT_CSH(XxxInstruction)> instr instr_with_semicolon opt ifthen
%type <CHAR_IF_CSH(XxxInstrList)> instrlist braced_instrlist top_level_instrlist optlist
                                  several_instructions optlist_with_semicolon scope_close
%type <int> condition ifthen_condition else
%type <ECompareOperator> comp 
%token <ECompareOperator> TOK_COMP_OP
%token <const_char_ptr> TOK_OCBRACKET TOK_CCBRACKET 
%type <const_char_ptr> scope_open_proc_body scope_close_proc_body
%type <CHAR_IF_CSH(const_Procedure_ptr)> proc_invocation
%type <UPTR_NOT_CSH(Procedure)> procedure_body
%type <CHAR_IF_CSH(ProcParamDefList)> proc_def_param_list proc_def_arglist proc_def_arglist_tested
%type <UPTR_NOT_CSH(ProcParamDef)> proc_def_param 
%type <CHAR_IF_CSH(ProcParamInvocationList)> proc_param_list proc_invoc_param_list
%type <UPTR_NOT_CSH(ProcParamInvocation)> proc_invoc_param
%type <CHAR_IF_CSH(CppProcDefParseHelper<AttributeList>)> defprochelp1 defprochelp2 defprochelp3 defprochelp4

%code {
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <iostream>

YY_DECL;

#ifdef C_S_H_IS_COMPILED
    #include "xxx_csh_lang2.h"  //Needs parse_param from xxx_lang_misc.h
    /* yyerror
     *  Error handling function.  Do nothing for CSH */
    void yyerror(YYLTYPE* /*loc*/, Csh & /*csh*/, void * /*yyscanner*/, const char * /*str*/) {}
#else
    #include "xxx_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "xxx_lang2.h"      //Needs parse_param from xxx_lang_misc.h
    /* Use verbose error reporting such that the expected token names are dumped */
    //#define YYERROR_VERBOSE
    void yyerror(YYMSC_LOCATION_TYPE *loc, YYMSC_RESULT_TYPE &chart, void *yyscanner, const char *str)
    {
        chart.Error.Error(loc->start, str);
    }
#endif

#ifdef C_S_H_IS_COMPILED
void XxxCshParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#else
void XxxParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#endif
{
    YYMSC_PARAM_TYPE pp;
    pp.RESULT = &RESULT;
#ifdef C_S_H_IS_COMPILED
    pp.buff.buf = buff;
    pp.buff.length = len;
    pp.buff.pos = 0;
    xxxcsh_lex_init(&pp.yyscanner);
    xxxcsh_set_extra(&pp, pp.yyscanner);
    xxxcsh_parse(RESULT, pp.yyscanner);
    xxxcsh_lex_destroy(pp.yyscanner);
#else
    pp.buffs.emplace_back(buff, len);
    pp.pos_stack.file = RESULT.current_file;
    xxx_lex_init(&pp.yyscanner);
    xxx_set_extra(&pp, pp.yyscanner);
    xxx_parse(RESULT, pp.yyscanner);
    xxx_lex_destroy(pp.yyscanner);
#endif
}

}

%%

chart_with_bye: chart eof
{
	YYACCEPT;
}
              | chart error eof
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(@2, "Syntax error.");
  #else
    chart.Error.Error(@2.start, "Syntax error.");
  #endif
	YYACCEPT;
};

eof:   TOK_EOF 
     | TOK_BYE 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
	csh.AddCSH_AllCommentBeyond(@1);
  #else
  #endif
} 
     | TOK_BYE TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH(@2, COLOR_SEMICOLON);
	csh.AddCSH_AllCommentBeyond(@2);
  #else
  #endif
}

chart:
{
  //Add here what to do for an empty chart
  #ifdef C_S_H_IS_COMPILED
    csh.AddLineBeginToHints();
    csh.hintStatus = HINT_READY;
    csh.hintSource = EHintSourceType::LINE_START;
    csh.hintsForcedOnly = true;
  #else
    //no action for empty file
  #endif
}
           | top_level_instrlist
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBefore(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    //TODO: Add the instructionlist to the chart
    (void)$1;
  #endif
};

 /* This instruction list allows an extra closing brace and provides an error msg for it */
top_level_instrlist: instrlist 
                 | instrlist TOK_CCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(@2, "Closing brace missing its opening pair.");
  #else
    $$ = std::move($1);
    chart.Error.Error(@2.start, "Unexpected '}'.");
  #endif
    $2; //suppress
}
                 | instrlist TOK_CCBRACKET top_level_instrlist
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(@2, "Closing brace missing its opening pair.");
  #else
    //Merge $3 into $1 
    $1.splice($1.end(), $3);
    $$ = std::move($1);
    chart.Error.Error(@3.start, "Unexpected '}'.");
  #endif
    $2; //suppress
};


braced_instrlist: scope_open instrlist scope_close
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
  #else
    $2.Append(std::move($3)); //Append any potential CommandNumbering
    $$ = std::move($2);
  #endif
}
            | scope_open scope_close
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
  #else
    //scope_close should not return here with a CommandNumbering
    //but even if so, we let it get erased
    (void)$2;
  #endif
}
            | scope_open instrlist error scope_close
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
    csh.AddCSH_Error(@3, "Could not recognize this as a valid line.");
  #else
    $2.Append(std::move($4)); 
    $$ = std::move($2);
    chart.Error.Error(@3.start, "Syntax error.");
  #endif
    yyerrok;
}
            | scope_open instrlist error TOK_EOF
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair(@$);
    csh.AddCSH_Error(@3, "Could not recognize this as a valid line.");
    csh.PopContext();
  #else
    $$ = std::move($2);
    $$.Append(chart.PopContext()); //will be empty list of nodes anyway.
    chart.Error.Error(@3.start, "Missing '}'.");
    chart.Error.Error(@1.start, @3.start, "Here is the corresponding '{'.");
  #endif
}
            | scope_open instrlist TOK_EOF
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair(@$);
    csh.AddCSH_ErrorAfter(@2, "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    $$ = std::move($2);
    $$.Append(chart.PopContext()); //will be empty list of nodes anyway.
    chart.Error.Error(AFTER(@2), "Missing '}'.");
    chart.Error.Error(@1.start, AFTER(@2), "Here is the corresponding '{'.");
  #endif
}
            | scope_open TOK_EOF
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair(@$);
    csh.AddCSH_ErrorAfter(@1, "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    $$ = chart.PopContext(); //will be empty list of nodes anyway.
    chart.Error.Error(AFTER(@1), "Missing a corresponding '}'.");
  #endif
}
            | scope_open instrlist TOK_BYE
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
    csh.AddCSH_Error(@3, "The command 'bye' can only be used at the top level.");
    csh.PopContext();
  #else
    $$ = std::move($2);
    $$.Append(chart.PopContext()); //will be empty list of nodes anyway.
    chart.Error.Error(@3.start, "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(@1.start, @3.start, "Here is the opening '{'.");
  #endif
    $3;
}
            | scope_open TOK_BYE
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
    csh.AddCSH_Error(@2, "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
  #else
    $$ = chart.PopContext(); //will be empty list of nodes anyway.
    chart.Error.Error(@2.start, "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
};


instrlist:    instr_with_semicolon
{
  #ifndef C_S_H_IS_COMPILED
    $$.Append(std::move($1)); /* New list */
  #endif
}
            | instrlist instr_with_semicolon
{
  #ifndef C_S_H_IS_COMPILED
    $1.Append(std::move($2));     /* Add to existing list */
    $$ = std::move($1);
  #endif
}
             | several_instructions
             | instrlist several_instructions
{
  #ifndef C_S_H_IS_COMPILED
    //TODO: Add a nested instructionlist to another instructionslist
    $1.Append(std::move($2));     /* Add to existing list */
    $$ = std::move($1);
  #endif
};

several_instructions: optlist_with_semicolon
                    | braced_instrlist
{
  #ifdef C_S_H_IS_COMPILED
    //Standalone braced instruction lists are instructions - for the purpose of indentation
    csh.AddInstruction(@1);
  #endif
  $$ = std::move($1);
};


instr_with_semicolon: instr 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction(@$);
    csh.AddCSH_ErrorAfter(@1, "Missing semicolon (';').");
  #else
    //if ($1) ($1)->SetLineEnd(@$);
    $$ = std::move($1);
    chart.Error.Error(AFTER(@1), "Missing a semicolon ';'.");
  #endif
}
            | instr TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction(@$);
    csh.AddCSH(@2, COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter(@2)) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(@$);
    $$ = std::move($1);
  #endif
}
              | TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction(@$);
    csh.AddCSH(@1, COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    $$=nullptr;
  #endif
}
              | instr error TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction(@$);
    csh.AddCSH(@3, COLOR_SEMICOLON);
    csh.AddCSH_Error(@2, "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter(@3)) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(@$);
    $$ = std::move($1);
    chart.Error.Error(AFTER(@1), "Missing a semicolon ';'.");
    chart.Error.Error(@1.start, AFTER(@1), "Here is the beginning of the command as I understood it.");
  #endif
}
             | proc_invocation TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_SEMICOLON);
  #else
    if ($1) {
        auto ctx = ($1)->MatchParameters(nullptr, AFTER(@1), &chart);
        if (!ctx.first) {
            XxxPushFlex(param, ($1)->text, &(@1), ($1)->file_pos, EInclusionReason::PROCEDURE);
            param.last_procedure = $1;
            param.last_procedure_params = std::move(ctx.second);
            param.open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    $$ = nullptr;
  #endif 
}
             | proc_invocation error TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@3, COLOR_SEMICOLON);
    csh.AddCSH_Error(@2, "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter(@3)) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(AFTER(@1), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(@1.start, AFTER(@1), "Here is the beginning of the command as I understood it.");
    if ($1) {
        auto ctx = ($1)->MatchParameters(nullptr, AFTER(@1), &chart);
        if (!ctx.first) {
            XxxPushFlex(param, ($1)->text, &(@1), ($1)->file_pos, EInclusionReason::PROCEDURE);
            param.last_procedure = $1;
            param.last_procedure_params = std::move(ctx.second);
            param.open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    $$ = nullptr;
  #endif 
}
             | proc_invocation  
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter(@1, "Missing semicolon.");
  #else
    chart.Error.Error(AFTER(@1), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(@1.start, AFTER(@1), "Here is the beginning of the command as I understood it.");
    if ($1) {
        auto ctx = ($1)->MatchParameters(nullptr, AFTER(@1), &chart);
        if (!ctx.first) {
            XxxPushFlex(param, ($1)->text, &(@1), ($1)->file_pos, EInclusionReason::PROCEDURE);
            param.last_procedure = $1;
            param.last_procedure_params = std::move(ctx.second);
            param.open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    $$ = nullptr;
  #endif 
}
             | proc_invocation proc_param_list TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@3, COLOR_SEMICOLON);
  #else
    if ($1) {
        auto ctx = ($1)->MatchParameters($2, @2.end, &chart);
        if (!ctx.first) {
            XxxPushFlex(param, ($1)->text, &(@1), ($1)->file_pos, EInclusionReason::PROCEDURE);
            param.last_procedure = $1;
            param.last_procedure_params = std::move(ctx.second);
            param.open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    $$ = nullptr;
  #endif 
}
             | proc_invocation proc_param_list error TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@4, COLOR_SEMICOLON);
    csh.AddCSH_Error(@3, "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter(@4)) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(AFTER(@2), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(@1.start, AFTER(@2), "Here is the beginning of the command as I understood it.");
    if ($1) {
        auto ctx = ($1)->MatchParameters($2, @2.end, &chart);
        if (!ctx.first) {
            XxxPushFlex(param, ($1)->text, &(@1), ($1)->file_pos, EInclusionReason::PROCEDURE);
            param.last_procedure = $1;
            param.last_procedure_params = std::move(ctx.second);
            param.open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    $$ = nullptr;
  #endif 
}
             | proc_invocation proc_param_list 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter(@2, "Missing semicolon.");
  #else
    chart.Error.Error(AFTER(@2), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(@1.start, AFTER(@2), "Here is the beginning of the command as I understood it.");
    if ($1) {
        auto ctx = ($1)->MatchParameters($2, @2.end, &chart);
        if (!ctx.first) {
            XxxPushFlex(param, ($1)->text, &(@1), ($1)->file_pos, EInclusionReason::PROCEDURE);
            param.last_procedure = $1;
            param.last_procedure_params = std::move(ctx.second);
            param.open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    $$ = nullptr;
  #endif 
}
             | include TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_SEMICOLON);
  #else
    auto text = chart.Include($1, @1.start);
    if (text.first && text.first->length() && text.second.IsValid()) 
        XxxPushFlex(param, *text.first, &(@1), text.second, EInclusionReason::INCLUDE);
    $$ = nullptr;
  #endif 
}
             | include 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter(@1, "Missing semicolon.");
  #else
    auto text = chart.Include($1, @1.start);
    if (text.first && text.first->length() && text.second.IsValid()) 
        XxxPushFlex(param, *text.first, &(@1), text.second, EInclusionReason::INCLUDE);
    chart.Error.Error(AFTER(@1), "Missing a semicolon ';'.");
    chart.Error.Error(@1.start, AFTER(@1), "Here is the beginning of the command as I understood it.");
    $$ = nullptr;
  #endif 
}
             | include error TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@3, COLOR_SEMICOLON); 
    csh.AddCSH_Error(@2, "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter(@3)) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(AFTER(@1), "Missing a semicolon ';'.");
    chart.Error.Error(@1.start, AFTER(@1), "Here is the beginning of the command as I understood it.");
    auto text = chart.Include($1, @1.start);
    if (text.first && text.first->length() && text.second.IsValid()) 
        XxxPushFlex(param, *text.first, &(@1), text.second, EInclusionReason::INCLUDE);
    $$ = nullptr;
  #endif 
};

proc_invocation: TOK_COMMAND_REPLAY 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter(@1, "Missing procedure name.");
  #else
    chart.Error.Error(AFTER(@1), "Missing procedure name.");
    $$ = nullptr;
  #endif 
}
               | TOK_COMMAND_REPLAY alpha_string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH(@2, COLOR_PROCNAME);
  #else 
    $$ = nullptr;
    if (!$2.had_error) {
        auto proc = chart.GetProcedure($2);
        if (proc==nullptr) 
            chart.Error.Error(@2.start, "Undefined procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::PROBLEM)
            chart.Error.Error(@2.start, "Ill-formed procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::OK) {
            //Only return the procedure if we are not inside a procedure definition
            if (!chart.SkipContent())
                $$ = proc;
            //else just move on parsing - we do not reparse procedure replays during 
            //the definition of an outer procedure.
        }
        //else return null, emit no error for EMPTY
    }
  #endif 
};

proc_param_list: TOK_OPARENTHESIS TOK_CPARENTHESIS
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH(@2, COLOR_PARENTHESIS);
  #endif
}
              | TOK_OPARENTHESIS error TOK_CPARENTHESIS
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH_Error(@2, "Invalid parameter syntax.");
    csh.AddCSH(@3, COLOR_PARENTHESIS);
  #else
    chart.Error.Error(@2.start, "Invalid parameter syntax. Ignoring procedure call.");
  #endif
}
              | TOK_OPARENTHESIS 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter(@1, "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(AFTER(@1), "Missing parameter list closed by a parenthesis ')'. Ignoring procedure call.");
  #endif
}
              | TOK_OPARENTHESIS proc_invoc_param_list error TOK_CPARENTHESIS
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH_Error(@3, "Invalid parameter syntax.");
    csh.AddCSH(@4, COLOR_PARENTHESIS);
  #else
    chart.Error.Error(@3.start, "Invalid parameter syntax. Ignoring procedure call.");
  #endif
}
              | TOK_OPARENTHESIS proc_invoc_param_list 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter(@2, "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(AFTER(@2), "Missing closing parenthesis ')'. Ignoring procedure call.");
  #endif
}
              | TOK_OPARENTHESIS proc_invoc_param_list TOK_CPARENTHESIS
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH(@3, COLOR_PARENTHESIS);
  #else
    $$ = std::move($2);
  #endif
};

proc_invoc_param_list: proc_invoc_param
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ($1) 
        $$.Append(std::move($1));
  #endif
}
                   | TOK_COMMA
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_COMMA);
  #else
    $$.Append(std::make_unique<ProcParamInvocation>(@1.start));
  #endif
}
                   | TOK_COMMA proc_invoc_param
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_COMMA);
  #else
    if ($2) {
        $$.Append(std::make_unique<ProcParamInvocation>(@1.start));
        $$.Append(std::move($2));
    } 
  #endif
}
                   | proc_invoc_param_list TOK_COMMA
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
  #else
    $1.Append(std::make_unique<ProcParamInvocation>(AFTER(@2)));
    $$ = std::move($1);
  #endif
}
                   | proc_invoc_param_list TOK_COMMA proc_invoc_param
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
  #else
    if ($3) 
        $1.Append(std::move($3));
    $$ = std::move($1);
  #endif
};

proc_invoc_param: string
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error) {
        //If this is a quoted string, color as a label, else as an attribute value
        if (@1.first_pos>0 && param.buff.buf[@1.first_pos-1]=='\"')
            csh.AddCSH_AttrValue_CheckAndAddEscapeHint(@1, $1, {});
        else
            csh.AddCSH(@1, COLOR_ATTRVALUE);
    }
  #else
    if (!$1.had_error)
        $$ = std::make_unique<ProcParamInvocation>($1, @1.start);
  #endif
}
                | TOK_NUMBER
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_ATTRVALUE);
  #else
    $$ = std::make_unique<ProcParamInvocation>($1, @1.start);
  #endif
};

include: TOK_COMMAND_INCLUDE
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter(@1, "Missing a file name to include. You must use quotation marks ('\"').");
    if (csh.CheckHintAfter(@1, EHintSourceType::ATTR_VALUE)) 
        csh.AddIncludeFilesToHints();
  #else
    chart.Error.Error(AFTER(@1), "Missing a file name to include. You must use quotation marks ('\"').");
  #endif 
}
               | TOK_COMMAND_INCLUDE TOK_QSTRING
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH(@2, COLOR_INCLUDEFILE);
    if (csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_VALUE)) 
        csh.AddIncludeFilesToHints();
    else if(csh.CheckHintAt(@2, EHintSourceType::ATTR_VALUE)) 
        csh.AddIncludeFilesToHints($2, @2);
  #endif 
    $$ = std::move($2);
};


optlist_with_semicolon: optlist
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction(@$);
    csh.AddCSH_ErrorAfter(@1, "Missing semicolon (';').");
  #else
    $$=std::move($1);
    chart.Error.Error(AFTER(@1), "Missing a semicolon ';'.");
  #endif
}
            | optlist TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction(@$);
    csh.AddCSH(@2, COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter(@2)) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    $$=std::move($1);
  #endif
}
              | optlist error TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction(@$);
    csh.AddCSH(@3, COLOR_SEMICOLON);
    csh.AddCSH_Error(@2, "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter(@3)) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    $$=std::move($1);
    chart.Error.Error(AFTER(@1), "Missing a semicolon ';' after option(s).");
    chart.Error.Error(@1.start, AFTER(@1), "Here is the beginning of the option list as I understood it.");
  #endif
};



instr:         TOK_COMMAND_DEFSHAPE shapedef
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error(@1, "Cannot define shapes as part of a procedure.");
    else
        csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } 
  #else
    if (chart.SkipContent())
        chart.Error.Error(@1.start, "Cannot define shapes as part of a procedure.");
  #endif
}
              | TOK_COMMAND_DEFSHAPE 
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error(@1, "Cannot define shapes as part of a procedure.");
    else
        csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } 
    csh.AddCSH_ErrorAfter(@$, "Missing shape name and definition.");
  #else
    if (chart.SkipContent())
        chart.Error.Error(@1.start, "Cannot define shapes as part of a procedure.");
    else
        chart.Error.Error(@1.start, "Missing shape name and definition.");
  #endif
}
              | TOK_COMMAND_DEFCOLOR colordeflist
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
  #endif
}
              | TOK_COMMAND_DEFCOLOR 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter(@1, EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter(@$, "Missing color name to (re-)define.");
  #else
    chart.Error.Error(@$.end, "Missing a color name to (re-)define.");
  #endif
}
              | TOK_COMMAND_DEFSTYLE styledeflist
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
  #endif
}
              | TOK_COMMAND_DEFSTYLE 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter(@1, EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter(@$, "Missing style name to (re-)define.");
  #else
    chart.Error.Error(@$.end, "Missing a style name to (re-)define.");
  #endif
}
              | TOK_COMMAND_DEFDESIGN designdef
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error(@1, "Cannot define designs inside a procedure.");
    else
        csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } 
  #else
    if (chart.SkipContent())
        chart.Error.Error(@1.start, "Cannot define designs inside a procedure.");
  #endif
}
              | TOK_COMMAND_DEFDESIGN 
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error(@1, "Cannot define designs inside a procedure.");
    else {
        csh.AddCSH(@1, COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter(@$, "Missing design name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } 
  #else
    if (chart.SkipContent())
        chart.Error.Error(@1.start, "Cannot define designs inside a procedure.");
    else
        chart.Error.Error(@$.end, "Missing a design name to (re-)define.");
  #endif
}
              | defproc
{
  #ifndef C_S_H_IS_COMPILED
    $$ = nullptr;
  #endif
}
              | set
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $$ = nullptr;
  #endif
}
              | ifthen
{
  #ifdef C_S_H_IS_COMPILED
    csh.IfThenElses.push_back(@$);
  #endif
    $$ = std::move($1);
}
              | TOK_XXX
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
  #else
    //TODO: Create a new instruction
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent())
        $$ = nullptr;
    else
        $$ = nullptr;
  #endif
}
              | TOK_XXX full_attrlist_with_label
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
  #else
    //TODO: Create a new instruction
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent())
        $$ = nullptr;
    else
        $$ = nullptr;
  #endif
};


optlist:     opt
{
  #ifndef C_S_H_IS_COMPILED
    if ($1)
        $$.Append(std::move($1)); /* New list */
  #endif
}
           | optlist TOK_COMMA opt
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintBetween(@2, @3, EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if ($3) $1.Append(std::move($3));     /* Add to existing list */
    $$ = std::move($1);
  #endif
}
           | optlist TOK_COMMA
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintAfter(@2, EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    $$ = std::move($1);
    chart.Error.Error(AFTER(@2), "Expecting an option here.");
  #endif
}
           | optlist TOK_COMMA error
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintBetween(@2, @3, EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error(@3, "An option expected here.");
  #else
    $$ = std::move($1);
    chart.Error.Error(@3.start, "I am not sure what is coming here.");
  #endif
};


opt:         entity_string TOK_EQUAL TOK_NUMBER
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_OPTIONNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    csh.AddCSH(@3, COLOR_ATTRVALUE);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!$1.had_error && csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_VALUE, $1)) {
        XxxChart::AttributeValues($1, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ($1.had_error || chart.SkipContent())
        $$ = nullptr;
    else
        $$ = chart.AddAttribute(Attribute($1, $3, @1, @3));
  #endif
}
            | entity_string TOK_EQUAL string
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_OPTIONNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    if (!$1.had_error && !$3.had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint(@3, $3, $1);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!$1.had_error && csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_VALUE, $1)) {
        XxxChart::AttributeValues($1, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ($1.had_error || $3.had_error || chart.SkipContent())
        $$ = nullptr;
    else
        $$ = chart.AddAttribute(Attribute($1, $3, @1, @3));
  #endif
}
            | entity_string TOK_EQUAL
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_OPTIONNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    csh.AddCSH_ErrorAfter(@2, "Missing option value.");
    if (!$1.had_error && csh.CheckHintAfter(@2, EHintSourceType::ATTR_VALUE, $1)) {
        XxxChart::AttributeValues($1, csh);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(AFTER(@2), "Missing option value.");
    $$ = nullptr;
  #endif
};

styledeflist: styledef
             | styledeflist TOK_COMMA styledef
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintBetween(@2, @3, EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
}
             | styledeflist TOK_COMMA 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintAfter(@2, EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else 
    chart.Error.Error(@$.end, "Missing style definition here.", "Try just removing the comma.");
#endif
};

styledef: stylenamelist full_attrlist
{
  #ifdef C_S_H_IS_COMPILED
    if (!csh.Contexts.back().SkipContent())
        for (auto &str : *($1))
            if (csh.ForbiddenStyles.find(str) == csh.ForbiddenStyles.end())
                csh.Contexts.back().StyleNames.insert(str);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, @2))
        XxxStyle().AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, @2))
        XxxStyle().AttributeValues(csh.hintAttrName, csh);
  #else
    if (chart.SkipContent())
    	  chart.AddAttributeListToStyleList($2, $1); 
  #endif
}  
                 | stylenamelist 
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH_ErrorAfter(@$, "Missing attribute definitons in square brackets ('[' and ']').");
  #else
    chart.Error.Error(@$.end, "Missing attribute definitons in square brackets ('[' and ']').");
  #endif
};

stylenamelist: string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_STYLENAME);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
	    csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    if (!$1.had_error && $1.str.length())
        $$.push_back($1);
}
             | stylenamelist TOK_COMMA 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
	csh.AddCSH_ErrorAfter(@2, "Missing a style name to (re-)define.");
    if (csh.CheckHintAfter(@1, EHintSourceType::ATTR_NAME)) {
	csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(@$.end, "Missing a style name to (re-)define.");
  #endif
    $$ = std::move($1);
};
             | stylenamelist TOK_COMMA string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    csh.AddCSH(@3, COLOR_STYLENAME);
    if (csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_NAME)) {
		csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    if (!$3.had_error && $3.str.length())
        $1.push_back($3);
    $$ = std::move($1);
};

shapedef: entity_string
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH(@1, COLOR_ATTRVALUE);
    csh.AddCSH_ErrorAfter(@$, StrCat("Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '", $1, "'."));
  #else
    chart.Error.Error(@$.end, StrCat("Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '", $1, "'."));
  #endif	
}
		| entity_string TOK_OCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH(@1, COLOR_ATTRVALUE);
    csh.AddCSH(@2, COLOR_BRACE);
    csh.AddCSH_ErrorAfter(@$, StrCat("Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '", $1, "'."));
  #else
    chart.Error.Error(@$.end, StrCat("Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '", $1, "'."));
  #endif	
    $2; //suppress
}
		| entity_string TOK_OCBRACKET shapedeflist
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH(@1, COLOR_ATTRVALUE);
    csh.AddCSH(@2, COLOR_BRACE);
    csh.AddCSH_ErrorAfter(@3, "Missing a closing brace ('}').");
    if (!$1.had_error)
        csh.AddShapeName($1);
  #else
    chart.Error.Error(AFTER(@3), "Missing '}'.");
    chart.Error.Error(@2.start, AFTER(@3), "Here is the corresponding '{'.");
    if (!$1.had_error && !chart.SkipContent())
        chart.Shapes.Add($1, @1.start, chart.file_url, chart.file_info, std::move(*$3), chart.Error);
  #endif	
    $2; //suppress
}
		| entity_string TOK_OCBRACKET shapedeflist TOK_CCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH(@1, COLOR_ATTRVALUE);
    csh.AddCSH(@2, COLOR_BRACE);
    csh.AddCSH(@4, COLOR_BRACE);
	csh.AddShapeName($1);
  #else
    if (!$1.had_error && !chart.SkipContent())
        chart.Shapes.Add($1, @1.start, chart.file_url, chart.file_info, std::move(*$3), chart.Error);
  #endif	
    $2; //suppress
    $4; //suppress
}
		| entity_string TOK_OCBRACKET shapedeflist error TOK_CCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH(@1, COLOR_ATTRVALUE);
    csh.AddCSH(@2, COLOR_BRACE);
    csh.AddCSH(@5, COLOR_BRACE);
	csh.AddShapeName($1);
    csh.AddCSH_Error(@4, "Only numbers can come after shape commands.");
  #else
    if (!$1.had_error && !chart.SkipContent())
        chart.Shapes.Add($1, @1.start, chart.file_url, chart.file_info, std::move(*$3), chart.Error);
  #endif	
    $2; //suppress
    $5; //suppress
};

shapedeflist: shapeline TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_SEMICOLON);
  #else
    $$ = std::make_unique<Shape>();
    if ($1)
		  $$->Add(std::move($1.value()));
  #endif	
}
             | error shapeline TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(@1, "I do not understand this.");
    csh.AddCSH(@3, COLOR_SEMICOLON);
  #else
    $$ = std::make_unique<Shape>();
		if ($2)
		  $$->Add(std::move($2.value()));
  #endif	
}
             | shapedeflist shapeline TOK_SEMICOLON
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@3, COLOR_SEMICOLON);
  #else
		if ($2)
		  $1->Add(std::move($2.value()));
    $$ = std::move($1);
  #endif	
}
             | shapedeflist error  
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(@2, "Only numbers can come after shape commands.");
  #else
    $$ = std::move($1);
  #endif	
};

shapeline: TOK_SHAPE_COMMAND
{
    const int num_args = 0;
	  const int should_args = ShapeElement::GetNumArgs($1);
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
	  if (should_args != num_args)
		    csh.AddCSH_ErrorAfter(@$, ShapeElement::ErrorMsg($1, num_args));
  #else
  	if (should_args != num_args)
	  	  chart.Error.Error(@$.end, ShapeElement::ErrorMsg($1, num_args).append(" Ignoring line."));
  	else 
	      $$.emplace($1);
  #endif	
}
         | TOK_SHAPE_COMMAND TOK_NUMBER
{
    const int num_args = 1;
	  const int should_args = ShapeElement::GetNumArgs($1);
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (should_args != num_args) {
		    std::string msg = ShapeElement::ErrorMsg($1, num_args);
        switch (ShapeElement::GetNumArgs($1)) {
        case 0:  csh.AddCSH_Error(@2, std::move(msg)); break;
		    default: csh.AddCSH_ErrorAfter(@$, std::move(msg)); break;
		    }
	  } else if ($1>=ShapeElement::SECTION_BG && (($2)[0]<'0' || ($2)[0]>'2' || ($2)[1]!=0))
		    csh.AddCSH_Error(@2, "S (section) commands require an integer between 0 and 2.");
  #else
    const double a = atof($2.c_str());
    if (should_args > num_args)
        chart.Error.Error(@2.end, ShapeElement::ErrorMsg($1, num_args).append(" Ignoring line."));
    else if ($1>=ShapeElement::SECTION_BG && (a!=0 && a!=1 && a!=2))
        chart.Error.Error(@2.start, "S (section) commands require an integer between 0 and 2. Ignoring line.");
    else if ($1>=ShapeElement::SECTION_BG)
        $$.emplace(ShapeElement::Type($1 + unsigned(a)));
    else 
        $$.emplace($1, a);
  #endif	
}
         | TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER
{
    const int num_args = 2;
	  const int should_args = ShapeElement::GetNumArgs($1);
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
  	if (should_args != num_args) {
        std::string msg = ShapeElement::ErrorMsg($1, num_args);
        switch (ShapeElement::GetNumArgs($1)) {
        case 0:  csh.AddCSH_Error(@2 + @3, std::move(msg)); break;
        case 1:  csh.AddCSH_Error(@3, std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter(@$, std::move(msg)); break;
        }
    }
    #else
      if (should_args > num_args)
          chart.Error.Error(@$.end, ShapeElement::ErrorMsg($1, num_args).append(" Ignoring line."));
      else 
          $$.emplace($1, atof($2.c_str()), atof($3.c_str()));
  #endif	
}
         | TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER alpha_string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if ($1!=ShapeElement::PORT)
        csh.AddCSH_Error(@4, "You need to specify a number here."); 
  #else
    if ($1!=ShapeElement::PORT)
        chart.Error.Error(@4.start, "Expecting a number here. Ignoring line.");
    else 
        $$.emplace(atof($2.c_str()), atof($3.c_str()), $4);
  #endif	
}
         | TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER alpha_string TOK_NUMBER
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if ($1!=ShapeElement::PORT)
        csh.AddCSH_Error(@4, "You need to specify a number here."); 
  #else
    if ($1!=ShapeElement::PORT)
        chart.Error.Error(@4.start, "Expecting a number here. Ignoring line.");
    else 
        $$.emplace(atof($2.c_str()), atof($3.c_str()), $4, atof($5.c_str()));
  #endif	
}
         | TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER
{
    const int num_args = 3;
  	const int should_args = ShapeElement::GetNumArgs($1);
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
	  if (should_args != num_args) {
		    std::string msg = ShapeElement::ErrorMsg($1, num_args);
	      switch (ShapeElement::GetNumArgs($1)) {
        case 0:  csh.AddCSH_Error(@2 + @4, std::move(msg)); break;
        case 1:  csh.AddCSH_Error(@3 + @4, std::move(msg)); break;
        case 2:  csh.AddCSH_Error(@4, std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter(@$, std::move(msg)); break;
        }
  	} else if ($1==ShapeElement::PORT)
        csh.AddCSH_Error(@4, "You need to specify a port name here starting with a letter."); 
  #else
    if (should_args > num_args)
        chart.Error.Error(@$.end, ShapeElement::ErrorMsg($1, num_args).append(" Ignoring line."));
    else if ($1==ShapeElement::PORT)
        chart.Error.Error(@4.start, "Expecting a port name here. Ignoring line.");
    else
        $$.emplace($1, atof($2.c_str()), atof($3.c_str()), atof($4.c_str()));
  #endif	
}
         | TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER
{
    const int num_args = 4;
  	const int should_args = ShapeElement::GetNumArgs($1);
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
	  if (should_args != num_args) {
		    std::string msg = ShapeElement::ErrorMsg($1, num_args);
	      switch (ShapeElement::GetNumArgs($1)) {
        case 0:  csh.AddCSH_Error(@2 + @5, std::move(msg)); break;
        case 1:  csh.AddCSH_Error(@3 + @5, std::move(msg)); break;
        case 2:  csh.AddCSH_Error(@4 + @5, std::move(msg)); break;
        case 3:  csh.AddCSH_Error(@5, std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter(@$, std::move(msg)); break;
        }
    }
  #else
    if (should_args > num_args)
        chart.Error.Error(@$.end, ShapeElement::ErrorMsg($1, num_args).append(" Ignoring line."));
    else 
        $$.emplace($1, atof($2.c_str()), atof($3.c_str()), atof($4.c_str()), atof($5.c_str()));
  #endif	
}
         | TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER
{
    const int num_args = 5;
	  const int should_args = ShapeElement::GetNumArgs($1);
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
	  if (should_args != num_args) {
		    std::string msg = ShapeElement::ErrorMsg($1, num_args);
	      switch (ShapeElement::GetNumArgs($1)) {
        case 0:  csh.AddCSH_Error(@2 + @6, std::move(msg)); break;
        case 1:  csh.AddCSH_Error(@3 + @6, std::move(msg)); break;
        case 2:  csh.AddCSH_Error(@4 + @6, std::move(msg)); break;
        case 3:  csh.AddCSH_Error(@5 + @6, std::move(msg)); break;
        case 4:  csh.AddCSH_Error(@6, std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter(@$, std::move(msg)); break;
        }
    }
  #else
    if (should_args > num_args)
        chart.Error.Error(@$.end, ShapeElement::ErrorMsg($1, num_args).append(" Ignoring line."));
    else 
        $$.emplace($1, atof($2.c_str()), atof($3.c_str()), atof($4.c_str()), atof($5.c_str()), atof($6.c_str()));
  #endif	
}
         | TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER
{
    const int num_args = 6;
  	const int should_args = ShapeElement::GetNumArgs($1);
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
	  if (should_args != num_args) {
		    std::string msg = ShapeElement::ErrorMsg($1, num_args);
	      switch (ShapeElement::GetNumArgs($1)) {
        case 0:  csh.AddCSH_Error(@2 + @7, std::move(msg)); break;
        case 1:  csh.AddCSH_Error(@3 + @7, std::move(msg)); break;
        case 2:  csh.AddCSH_Error(@4 + @7, std::move(msg)); break;
        case 3:  csh.AddCSH_Error(@5 + @7, std::move(msg)); break;
        case 4:  csh.AddCSH_Error(@6 + @7, std::move(msg)); break;
        case 5:  csh.AddCSH_Error(@7, std::move(msg)); break;
        default: csh.AddCSH_ErrorAfter(@$, std::move(msg)); break;
        }
    }
  #else
  	if (should_args > num_args)
	  	  chart.Error.Error(@$.end, ShapeElement::ErrorMsg($1, num_args).append(" Ignoring line."));
	  else 
		    $$ =  ShapeElement($1, atof($2.c_str()), atof($3.c_str()), atof($4.c_str()), atof($5.c_str()), atof($6.c_str()), atof($7.c_str()));
  #endif	
};

colordeflist: colordef
             | colordeflist TOK_COMMA colordef
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
  #endif
}
             | colordeflist TOK_COMMA 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintAfter(@2, EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter(@$, "Missing color name to (re-)define.");
  #else
    chart.Error.Error(@$.end, "Missing a color name to (re-)define.");
  #endif
};

color_string: TOK_COLORDEF 
        | string
{
    if (!$1.had_error) 
        $$ = std::move($1.str);
};

colordef: alpha_string TOK_EQUAL color_string
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error) {
        csh.AddCSH(@1, COLOR_COLORNAME);
        ColorType color = csh.Contexts.back().Colors.GetColor($3);
        if (color.type!=ColorType::INVALID)
            csh.Contexts.back().Colors[$1.str] = color;
    }
    csh.AddCSH(@2, COLOR_EQUAL);
    csh.AddCSH(@3, COLOR_COLORDEF);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent() && !$1.had_error)
        chart.MyCurrentContext().colors.AddColor($1, $3, chart.Error, @$);
  #endif
}
           |alpha_string TOK_EQUAL TOK_PLUS_PLUS color_string
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error) {
        csh.AddCSH(@1, COLOR_COLORNAME);
        ColorType color = csh.Contexts.back().Colors.GetColor("++"+$4);
        if (color.type!=ColorType::INVALID)
            csh.Contexts.back().Colors[$1] = color;
    }
    csh.AddCSH(@2, COLOR_EQUAL);
    csh.AddCSH(@3, COLOR_COLORDEF);
    csh.AddCSH(@4, COLOR_COLORDEF);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt(@2, @4, EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent() && !$1.had_error)
        chart.MyCurrentContext().colors.AddColor($1, "++"+$4, chart.Error, @$);
  #endif
}
           | alpha_string TOK_EQUAL 
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH(@1, COLOR_COLORNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter(@2, EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter(@$, "Missing color definition.");
  #else
    chart.Error.Error(@$.end, "Missing color definition.");
  #endif
}
           | alpha_string 
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH(@1, COLOR_COLORNAME);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter(@$, "Missing equal sign ('=') and a color definition.");
  #else
    chart.Error.Error(@$.end, "Missing equal sign ('=') and a color definition.");
  #endif
};



designdef : TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON TOK_CCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_DESIGNNAME);
    csh.AddCSH(@4, COLOR_SEMICOLON);
    csh.AddCSH(@5, COLOR_BRACE);
    if (!csh.Contexts.back().SkipContent()) {
        auto &d = csh.Contexts.back().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find($1);
        if (i == d.end())
            d.emplace($1, csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween(@2, @3, EHintSourceType::LINE_START) || 
         csh.CheckHintBetween(@4, @5, EHintSourceType::LINE_START)) ) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent()) {
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple($1),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(), 
                                                             EContextParse::NORMAL, 
                                                             EContextCreate::EMPTY, 
                                                             @2.start));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
    $5; //supress
}
           |TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON error TOK_CCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_DESIGNNAME);
    csh.AddCSH(@4, COLOR_SEMICOLON);
    csh.AddCSH_Error(@5, "Could not recognize this as part of a design definition.");
    csh.AddCSH(@6, COLOR_BRACE);
    if (!csh.Contexts.back().SkipContent()) {
        auto &d = csh.Contexts.back().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find($1);
        if (i == d.end())
            d.emplace($1, csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween(@2, @3, EHintSourceType::LINE_START) ||
         csh.CheckHintBetween(@4, @5, EHintSourceType::LINE_START))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    if (!chart.SkipContent()) {
        //if closing brace missing, still do the design definition
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple($1),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(), 
                                                             EContextParse::NORMAL, 
                                                             EContextCreate::EMPTY, 
                                                             @2.start));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
    $6; //supress
};


scope_open_empty: TOK_OCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACE);
    csh.PushContext(true, EContextParse::NORMAL);
  #else
    //push empty color & style sets for design definition
    chart.PushContext(@1.start, EContextParse::NORMAL, EContextCreate::CLEAR);
  #endif
    $1; //supress
};

designelementlist: designelement
                   | designelementlist TOK_SEMICOLON designelement
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_SEMICOLON);
    if (csh.CheckHintBetween(@2, @3, EHintSourceType::LINE_START)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#endif
};

designelement: TOK_COMMAND_DEFCOLOR colordeflist
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #endif
}
              | TOK_COMMAND_DEFCOLOR 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter(@$, "Missing color name to (re-)define.");
  #else
    chart.Error.Error(@$.end, "Missing a color name to (re-)define.");
  #endif
}
			  | TOK_COMMAND_DEFSTYLE styledeflist
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
  #endif
}
              | TOK_COMMAND_DEFSTYLE 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter(@$, "Missing style name to (re-)define.");
  #else
    chart.Error.Error(@$.end, "Missing a style name to (re-)define.");
  #endif
}
              | designoptlist;

designoptlist: designopt
               | designoptlist TOK_COMMA designopt
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintBetween(@2, @3, EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }   
  #endif
}
               | designoptlist TOK_COMMA 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    if (csh.CheckHintAfter(@2, EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }   
  #endif
}
               | designoptlist error
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(@2, "Extra stuff after design options. Maybe missing a comma?");
  #endif
};

designopt:         entity_string TOK_EQUAL TOK_NUMBER
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_OPTIONNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    csh.AddCSH(@3, COLOR_ATTRVALUE);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!$1.had_error && csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_VALUE, $1)) {
        XxxChart::AttributeValues($1, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!$1.had_error)
        chart.AddDesignAttribute(Attribute($1, $3, @$, @3));
  #endif
}
            | entity_string TOK_EQUAL string
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_OPTIONNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    if (!$3.had_error)
        csh.AddCSH(@3, COLOR_ATTRVALUE);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!$1.had_error && csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_VALUE, $1)) {
        XxxChart::AttributeValues($1, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!$1.had_error)
        chart.AddDesignAttribute(Attribute($1, $3, @1, @3));
  #endif
}
            | entity_string TOK_EQUAL 
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_OPTIONNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    if (csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!$1.had_error && csh.CheckHintAfter(@2, EHintSourceType::ATTR_VALUE, $1)) {
        XxxChart::AttributeValues($1, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(AFTER(@2), "Missing option value. Ignoring this.");
#endif
};


defproc: defprochelp1
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if (chart.SkipContent()) {
        chart.Error.Error(@$.start, "Cannot define procedures inside a procedure.");
    } else if ($1.name.had_error) {
        //do nothing, error already reported
    } else if ($1.name.str.length()==0) {
        chart.Error.Error($1.linenum_name, "Missing a procedure name to (re-)define. Ignoring this.");
    } else if (!$1.had_error && $1.body) {
        if (chart.MyCurrentContext().num_error != chart.Error.GetErrorNum(true)) {
            chart.Error.Error(@$.start, "There are warnings or errors inside the procedure definition. Ignoring it.");
            chart.MyCurrentContext().Procedures[$1.name].name = $1.name.str;
            chart.MyCurrentContext().Procedures[$1.name].status = EDefProcResult::PROBLEM;
            chart.MyCurrentContext().Procedures[$1.name].file_pos = $1.linenum_body;
        } else if ($1.body->status==EDefProcResult::OK || $1.body->status==EDefProcResult::EMPTY) {
            if ($1.parameters) {
                auto &p = chart.MyCurrentContext().Procedures[$1.name.str] = *$1.body;
                p.name = $1.name.str;
                p.parameters = std::move(*$1.parameters);
                for (auto &a : $1.attrs)
                    p.AddAttribute(*a, chart);
                if ($1.body->status==EDefProcResult::EMPTY)
                    chart.Error.Warning($1.linenum_body, "Empty procedure. Is this what you want?");
            } else {
                  chart.Error.Error(@$.start, "Ill-formed procedure parameter list. Ignoring this procedure definition.");
            }
        } else {
              chart.Error.Error(@$.start, "Ill-formed procedure body. Ignoring this procedure definition.");
        }
    }
  #endif
}


defprochelp1: TOK_COMMAND_DEFPROC
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent()) {
        csh.AddCSH_Error(@1, "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH(@1, COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter(@$, "Missing procedure name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } 
  #else
    $$.linenum_name = AFTER(@$);
  #endif
}
              | TOK_COMMAND_DEFPROC defprochelp2 
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent()) {
        csh.AddCSH_Error(@1, "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH(@1, COLOR_KEYWORD);
    }
    if (csh.CheckLineStartHintAt(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } 
  #else
    $$ = std::move($2);
  #endif
};

defprochelp2: alpha_string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PROCNAME);
    csh.AddCSH_ErrorAfter(@1, "Missing a procedure definition starting with '{'.");
  #else
    $$.name = std::move($1);
    $$.linenum_name = @1.start;
  #endif
}
              | alpha_string defprochelp3
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PROCNAME);
  #else
    $$ = std::move($2);
    $$.name = std::move($1);
    $$.linenum_name = @1.start;
  #endif
}
              | defprochelp3
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(CshPos(@1.first_pos, @1.first_pos), "Missing procedure name.");
  #else
    $$ = std::move($1);
    $$.linenum_name = @1.start;
  #endif
};

defprochelp3: proc_def_arglist_tested
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter(@1, "Missing a procedure definition starting with '{'.");
  #else
    $$.parameters = std::move($1);
  #endif
}
              | proc_def_arglist_tested defprochelp4
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $$ = std::move($2);
    $$.parameters = std::move($1);
  #endif
}
              | defprochelp4
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $$ = std::move($1);
    $$.parameters.emplace();
  #endif
};

defprochelp4: full_attrlist
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter(@1, "Missing a procedure definition starting with '{'.");
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, @1))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, @1))
        csh.AddYesNoToHints();
  #else
    $$.attrs = std::move($1);
  #endif
}
              | full_attrlist procedure_body
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, @1))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, @1))
        csh.AddYesNoToHints();
  #else
    $$.body = std::move($2);
    $$.linenum_body = @2.start;
    $$.attrs = std::move($1);
  #endif
}
              | procedure_body
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $$.body = std::move($1);
    $$.linenum_body = @1.start;
  #endif
};


scope_open_proc_body: TOK_OCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACE);
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    chart.MyCurrentContext().num_error = chart.Error.GetErrorNum(true);
    chart.PushContext(@1.start, EContextParse::SKIP_CONTENT, EContextCreate::EMPTY);
    chart.MyCurrentContext().parameters = std::move(param.last_procedure_params);
    chart.MyCurrentContext().starts_procedure = true;
    _ASSERT(param.open_context_mode == base_parse_parm::EScopeOpenMode::NORMAL);
    param.open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
  #endif
    $$ = std::move($1);
};

scope_close_proc_body: TOK_CCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
    csh.AddCSH(@1, COLOR_BRACE);
  #else
    chart.PopContext(); //discards returned instrlist
  #endif
    $$ = std::move($1);
};

proc_def_arglist_tested: proc_def_arglist
{
  #ifdef C_S_H_IS_COMPILED
  #else
    auto pair = Procedure::AreAllParameterNamesUnique($1);
    if (pair.first) {
        chart.Error.Error(pair.second->linenum_name, "This parameter name is already used.");
        chart.Error.Error(pair.first->linenum_name, pair.second->linenum_name, "This parameter name is already used.");
    } else {
        //Also copy to param.last_procedure_params and set open_context_mode
        param.last_procedure_params.clear();
        for (const auto &p : $1) 
            param.last_procedure_params.emplace(p->name, ProcParamResolved(std::string(), FileLineCol(), true));
        $$ = std::move($1);
    }
  #endif
};

proc_def_arglist: TOK_OPARENTHESIS TOK_CPARENTHESIS
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH(@2, COLOR_PARENTHESIS);
  #else
  #endif
}
              | TOK_OPARENTHESIS error TOK_CPARENTHESIS
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH_Error(@2, "Invalid parameter definitions.");
    csh.AddCSH(@3, COLOR_PARENTHESIS);
  #else
    chart.Error.Error(@2.start, "Invalid parameter definitions.", "Say something like '($first, $second=default)'.");
  #endif
}
              | TOK_OPARENTHESIS 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter(@1, "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(AFTER(@1), "Missing parameter list closed by a parenthesis ')'.");
  #endif
}
              | TOK_OPARENTHESIS proc_def_param_list error TOK_CPARENTHESIS
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH_Error(@3, "Invalid parameter definitions.");
    csh.AddCSH(@4, COLOR_PARENTHESIS);
  #else
    chart.Error.Error(@3.start, "Invalid parameter definitions.");
  #endif
}
              | TOK_OPARENTHESIS proc_def_param_list 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter(@2, "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(AFTER(@2), "Missing closing parenthesis ')'.");
  #endif
}
              | TOK_OPARENTHESIS proc_def_param_list TOK_CPARENTHESIS
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_PARENTHESIS);
    csh.AddCSH(@3, COLOR_PARENTHESIS);
  #else
    $$ = std::move($2);
  #endif
};

proc_def_param_list: proc_def_param
{
  #ifdef C_S_H_IS_COMPILED
  #else 
    $$.Append(std::move($1));
  #endif
}
                   | proc_def_param_list TOK_COMMA
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    csh.AddCSH_ErrorAfter(@2, "Missing parameter after the comma.");
  #else
    chart.Error.Error(AFTER(@2), "Missing parameter after the comma.");
  #endif
}
                   | proc_def_param_list TOK_COMMA proc_def_param
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
  #else
    $1.Append(std::move($3));
    $$ = std::move($1);
  #endif
};

proc_def_param: TOK_PARAM_NAME
{
  #ifdef C_S_H_IS_COMPILED
    if ($1.length()>1 && $1.front()=='$') 
        csh.AddCSH(@1, COLOR_PARAMNAME);
    else
        csh.AddCSH_Error(@1, "Need name after the '$' sign.");
    $$ = 0; //has no value
  #else
    if ($1.length()>1 && $1.front()=='$') 
        $$ = std::make_unique<ProcParamDef>($1, @1.start);
    else 
        chart.Error.Error(@1.start, "Need name after the '$' sign.");
  #endif
}
              | TOK_PARAM_NAME TOK_EQUAL
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_EQUAL);
    if ($1.length()>1 && $1.front()=='$') 
        csh.AddCSH(@1, COLOR_PARAMNAME);
    else
        csh.AddCSH_Error(@1, "Need name after the '$' sign.");
    $$ = 0; //has no value
  #else
    if ($1.length()>1 && $1.front()=='$') 
        $$ = std::make_unique<ProcParamDef>($1, @1.start);
    else 
        chart.Error.Error(@1.start, "Need name after the '$' sign.");
  #endif
}
              | TOK_PARAM_NAME TOK_EQUAL string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_EQUAL);
    if ($1.length()>1 && $1.front()=='$') 
        csh.AddCSH(@1, COLOR_PARAMNAME);
    else
        csh.AddCSH_Error(@1, "Need name after the '$' sign.");
    $$ = 1; //has value
  #else
    if (!$3.had_error) {
      if ($1.length()>1 && $1.front()=='$') 
          $$ = std::make_unique<ProcParamDef>($1, @1.start, $3, @3.start);
      else 
          chart.Error.Error(@1.start, "Need name after the '$' sign.");
    }
  #endif
}
              | TOK_PARAM_NAME TOK_EQUAL TOK_NUMBER
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_EQUAL);
    if ($1 && $1[0]=='$' && $1[1]) 
        csh.AddCSH(@1, COLOR_PARAMNAME);
    else
        csh.AddCSH_Error(@1, "Need name after the '$' sign.");
    $$ = 1; //has value
  #else
    if ($1.length()>1 && $1.front()=='$')
        $$ = std::make_unique<ProcParamDef>($1, @1.start, $3, @3.start);
    else 
        chart.Error.Error(@1.start, "Need name after the '$' sign.");
  #endif
};


procedure_body: scope_open_proc_body instrlist scope_close_proc_body
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
  #else
    $$ = std::make_unique<Procedure>();
    $$->status = EDefProcResult::OK;
    $$->text = StrCat(std::string_view($1, $3-$1), ";"); //$1 and $3 are char* pointers to the input file text
    $$->file_pos = @$.start;
  #endif
}
            | scope_open_proc_body scope_close_proc_body
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
  #else
    $$ = std::make_unique<Procedure>();
    $$->status = EDefProcResult::EMPTY;
    $$->file_pos = @$.start;
  #endif
  $1; //to silence 'unused parameter' warnings
  $2; //to silence 'unused parameter' warnings
}
            | scope_open_proc_body instrlist error scope_close_proc_body
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
    csh.AddCSH_Error(@3, "Could not recognize this as a valid line.");
  #else
    $$ = std::make_unique<Procedure>();
    $$->status = EDefProcResult::PROBLEM;
    $$->file_pos = @$.start;
    chart.Error.Error(@3.start, "syntax error.");
  #endif
    yyerrok;
    $1; //to silence 'unused parameter' warnings
    $4; //to silence 'unused parameter' warnings
}
            | scope_open_proc_body instrlist error TOK_EOF
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair(@$);
    csh.AddCSH_Error(@3, "Could not recognize this as a valid line.");
    csh.PopContext();
  #else
    $$ = std::make_unique<Procedure>();
    $$->status = EDefProcResult::PROBLEM;
    $$->file_pos = @$.start;
    chart.PopContext(); //ignore instruction list returned
    chart.Error.Error(@3.start, "Missing '}'.");
    chart.Error.Error(@1.start, @3.start, "Here is the corresponding '{'.");
  #endif
    $1; //to silence 'unused parameter' warnings
}
            | scope_open_proc_body instrlist TOK_EOF
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair(@$);
    csh.AddCSH_ErrorAfter(@2, "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    $$ = std::make_unique<Procedure>();
    $$->status = EDefProcResult::PROBLEM;
    $$->file_pos = @$.start;
    chart.PopContext(); //ignore instruction list returned
    chart.Error.Error(AFTER(@2), "Missing '}'.");
    chart.Error.Error(@1.start, AFTER(@2), "Here is the corresponding '{'.");
  #endif
    $1; //to silence 'unused parameter' warnings
}
            | scope_open_proc_body TOK_EOF
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair(@$);
    csh.AddCSH_ErrorAfter(@1, "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    $$ = std::make_unique<Procedure>();
    $$->status = EDefProcResult::PROBLEM;
    $$->file_pos = @$.start;
    chart.PopContext(); //ignore instruction list returned
    chart.Error.Error(AFTER(@1), "Missing a corresponding '}'.");
  #endif
    $1; //to silence 'unused parameter' warnings
}
            | scope_open_proc_body instrlist TOK_BYE
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
    csh.AddCSH_Error(@3, "The command 'bye' can only be used at the top level.");
    csh.PopContext();
  #else
    $$ = std::make_unique<Procedure>();
    $$->status = EDefProcResult::PROBLEM;
    $$->file_pos = @$.start;
    chart.PopContext(); //ignore instrlist retured
    chart.Error.Error(@3.start, "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(@1.start, @3.start, "Here is the opening '{'.");
  #endif
    $1; //to silence 'unused parameter' warnings
}
            | scope_open_proc_body TOK_BYE
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back(@$);
    csh.AddCSH_Error(@2, "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
  #else
    $$ = std::make_unique<Procedure>();
    $$->status = EDefProcResult::PROBLEM;
    $$->file_pos = @$.start;
    chart.PopContext(); //ignore instrlist retured
    chart.Error.Error(@2.start, "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
    $1; //to silence 'unused parameter' warnings
};

set: TOK_COMMAND_SET proc_def_param
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (!$2) 
        csh.AddCSH_ErrorAfter(@2, "Missing value.");
  #else
    chart.SetVariable(std::move(*$2), @$);
  #endif
} 
    | TOK_COMMAND_SET 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter(@1, "Missing variable or parameter name to set.");
  #else
    chart.Error.Error(AFTER(@1), "Missing variable or parameter name to set.");
  #endif
}; 

comp: TOK_COMP_OP;

condition: string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_ATTRVALUE);
  #endif
    $$ = $1.had_error ? 2 : $1.str.length()>0;
}
         | string comp
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_ATTRVALUE);
    csh.AddCSH(@2, COLOR_EQUAL);
    csh.AddCSH_ErrorAfter(@2, "Missing string to compare to.");
  #else  
    chart.Error.Error(AFTER(@2), "Missing string to compare to.");
  #endif
    $$ = 2;
    $2; //to suppress
}
         | string comp string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_ATTRVALUE);
    if ($2!=ECompareOperator::INVALID) {
        csh.AddCSH(@2, COLOR_EQUAL);
        $$ = $1.Compare($2, $3);
    } else {
        csh.AddCSH_Error(@2, "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        $$ = 2;
    }
    csh.AddCSH(@3, COLOR_ATTRVALUE);
  #else  
    if ($2!=ECompareOperator::INVALID)
        $$ = $1.Compare($2, $3);
    else {
        chart.Error.Error(@2.start, "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        $$ = 2;
    }
  #endif
}
         | string error string
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_ATTRVALUE);
    csh.AddCSH_Error(@2, "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
    csh.AddCSH(@3, COLOR_ATTRVALUE);
  #else  
     chart.Error.Error(@2.start, "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
  #endif
    $$ = 2;
};

ifthen_condition: TOK_IF condition TOK_THEN
{
  #ifdef C_S_H_IS_COMPILED
    $$ = csh.Contexts.back().if_condition = $2;
    const bool cond_true = $2==1;
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH(@3, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter(@3)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (cond_true)
        csh.PushContext();
    else 
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    $$ = chart.MyCurrentContext().if_condition = $2;
    const bool cond_true = $2==1;
    if (cond_true)
        chart.PushContext(@1.start);
    else
        chart.PushContext(@1.start, EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_true;
    chart.MyCurrentContext().export_styles = cond_true;
  #endif
}
                | TOK_IF condition
{
  #ifdef C_S_H_IS_COMPILED
    $$ = csh.Contexts.back().if_condition = 2;
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter(@2, "Missing 'then' keyword.");
    if (csh.CheckHintAfter(@2, EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "then",
            "Continue the 'if' statement with 'then'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    $$ = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(@1.start, EContextParse::SKIP_CONTENT);
    chart.Error.Error(AFTER(@2), "Missing 'then' keyword.");
  #endif
    $2; //to supress warnings
}
                | TOK_IF 
{
  #ifdef C_S_H_IS_COMPILED
    $$ = csh.Contexts.back().if_condition = 2;
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter(@1, "Missing condition.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    $$ = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(@1.start, EContextParse::SKIP_CONTENT);
    chart.Error.Error(AFTER(@1), "Missing condition after 'if'.");
  #endif
}
                | TOK_IF error
{
  #ifdef C_S_H_IS_COMPILED
    $$ = csh.Contexts.back().if_condition = 2;
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH_Error(@2, "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    $$ = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(@1.start, EContextParse::SKIP_CONTENT);
    chart.Error.Error(@2.start, "Missing condition after 'if'.");
  #endif
}
                | TOK_IF error TOK_THEN
{
  #ifdef C_S_H_IS_COMPILED
    $$ = csh.Contexts.back().if_condition = 2;
    csh.AddCSH(@1, COLOR_KEYWORD);
    csh.AddCSH_Error(@2, "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
    if (csh.CheckLineStartHintAfter(@3)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    $$ = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(@1.start, EContextParse::SKIP_CONTENT);
    chart.Error.Error(@2.start, "Missing condition after 'if'.");
  #endif
};


else: TOK_ELSE
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter(@1)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
    const bool cond_false = csh.Contexts.back().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    $$ = csh.Contexts.back().if_condition; 
    if (cond_false)
        csh.PushContext();
    else 
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    //kill previous context and open new one - set laterreparse if error or if condition was true
    //this will ignore everything in the else clause
    chart.PopContext();
    const bool cond_false = chart.MyCurrentContext().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    $$ = chart.MyCurrentContext().if_condition; 
    if (cond_false)
        chart.PushContext(@1.start);
    else
        chart.PushContext(@1.start, EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_false;
    chart.MyCurrentContext().export_styles = cond_false;
  #endif
};

ifthen: ifthen_condition instr
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace(@2);
    csh.PopContext();
    if (csh.CheckHintAfter(@2, EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "else",
            "Continue the 'if/then' statement with 'else'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    if ($1==1) 
        $$ = std::move($2);
    chart.PopContext();
  #endif
}
            | ifthen_condition 
{
  #ifdef C_S_H_IS_COMPILED
    if ($1!=2)
        csh.AddCSH_ErrorAfter(@1, "Missing command after 'then'.");
    csh.PopContext();
  #else
    if ($1!=2)
        chart.Error.Error(AFTER(@1), "Missing a well-formed command after 'then'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    $1; //suppress
}
            | ifthen_condition error
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(@2, "Missing command after 'then'.");
    csh.PopContext();
  #else
    chart.Error.Error(@2.start, "Missing a well-formed command. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    $1; //suppress
}
          | ifthen_condition instr else
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace(@2);
    csh.AddCSH_ErrorAfter(@3, "Missing command after 'else'.");
    csh.PopContext();
  #else
    chart.Error.Error(AFTER(@3), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    $1; $3; //suppress
}
          | ifthen_condition instr error else
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace(@2);
    csh.AddCSH_Error(@3, "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter(@4, "Missing command after 'else'.");
    csh.PopContext();
  #else
    chart.Error.Error(@3.start, "I am not sure what is coming here.");
    chart.Error.Error(AFTER(@4), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    $1; $4; //suppress
}
          | ifthen_condition error else
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(@2, "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter(@3, "Missing command after 'else'.");
    csh.PopContext();
  #else
    chart.Error.Error(@2.start, "I am not sure what is coming here.");
    chart.Error.Error(AFTER(@3), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    $1; $3; //suppress
}
          | ifthen_condition instr else instr 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace(@2);
    csh.AddInstructionIfNotBrace(@4);
    csh.PopContext();
  #else
    switch ($1) {
    case 1: //original condition was true
        $$ = std::move($2);   //take 'then' branch, ignore 'else' branch (gets destoyed)
        break;
    case 0: //original condition was false
        $$ = std::move($4); //take 'else' branch, ignore 'then' branch (gets destoyed)
        break;
    default:
        _ASSERT(0);
        FALLTHROUGH;
    case 2: //we had an error, but has reported the error - ignore both branches
        break;
    }
    chart.PopContext();
  #endif
    $3; //suppress
}
          | ifthen_condition instr error else instr
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace(@2);
    csh.AddInstructionIfNotBrace(@5);
    csh.AddCSH_Error(@3, "I am not sure what is coming here.");
    csh.PopContext();
  #else
    chart.Error.Error(@3.start, "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    $1; $4; //suppress
}
          | ifthen_condition error else instr 
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace(@4);
    csh.AddCSH_Error(@2, "I am not sure what is coming here.");
    csh.PopContext();
  #else
    chart.Error.Error(@2.start, "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    $1; $3; //suppress
};


colon_string: TOK_COLON_QUOTED_STRING
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint(@1, $1, false);
    csh.AddColonLabel(@1, $1);
  #endif
    $$ = std::move($1);
}
             | TOK_COLON_STRING
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint(@1, $1, true);
	csh.AddColonLabel(@1, $1);
  #endif
    $$ = std::move($1);
};

full_attrlist_with_label: colon_string
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $$.Append(std::make_unique<Attribute>("label", $1, @$, @$.IncStartCol()));
  #endif
}
              | colon_string full_attrlist
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $2.Prepend(std::make_unique<Attribute>("label", $1, @1, @1.IncStartCol()));
    $$ = std::move($2);
  #endif
}
              | full_attrlist colon_string full_attrlist
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $1.Append(std::make_unique<Attribute>("label", $2, @2, @2.IncStartCol()));
    //Merge $3 at the end of $1
    $1.splice($1.end(), $3);
    $$ = std::move($1);
  #endif
}
              | full_attrlist colon_string
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $1.Append(std::make_unique<Attribute>("label", $2, @2, @2.IncStartCol()));
    $$ = std::move($1);
  #endif
}
              | full_attrlist;


full_attrlist: TOK_OSBRACKET TOK_CSBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACKET);
    csh.AddCSH(@2, COLOR_BRACKET);
    csh.SqBracketPairs.push_back(@$);
    csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME);
  #else
  #endif
}
                   | TOK_OSBRACKET attrlist TOK_CSBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACKET);
    csh.AddCSH(@3, COLOR_BRACKET);
    csh.SqBracketPairs.push_back(@$);
    csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME);
  #else
    $$ = std::move($2);
  #endif
}
                   | TOK_OSBRACKET attrlist error TOK_CSBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACKET);
    csh.AddCSH_Error(@3, "Extra stuff after an attribute list. Maybe missing a comma?");
    csh.AddCSH(@4, COLOR_BRACKET);
    csh.SqBracketPairs.push_back(@$);
    csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME);
  #else
    $$ = std::move($2);
  #endif
}
                   | TOK_OSBRACKET error TOK_CSBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACKET);
    csh.AddCSH_Error(@2, "Could not recognize this as an attribute or style name.");
    csh.AddCSH(@3, COLOR_BRACKET);
    csh.SqBracketPairs.push_back(@$);
    csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME);
  #else
    chart.Error.Error(@2.start, "Expecting an attribute or style name. Ignoring all until the closing square bracket (']').");
#endif
}
                   | TOK_OSBRACKET attrlist
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACKET);
    csh.AddCSH_ErrorAfter(@2, "Missing a square bracket (']').");
    csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME);
  	csh.SqBracketPairs.push_back(@$);
  #else
    $$ = std::move($2);
    chart.Error.Error(AFTER(@2), "Missing ']'.");
  #endif
}
                   | TOK_OSBRACKET attrlist error
{
  #ifdef C_S_H_IS_COMPILED
	  csh.AddCSH(@1, COLOR_BRACKET);
    csh.AddCSH_Error(@3, "Missing a ']'.");
	  csh.SqBracketPairs.push_back(@$);
	  csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME);
  #else
    $$ = std::move($2);
    chart.Error.Error(@3.start, "Missing ']'.");
  #endif
}
                   | TOK_OSBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACKET);
    csh.AddCSH_ErrorAfter(@1, "Missing a square bracket (']').");
  	csh.SqBracketPairs.push_back(@$);
    csh.CheckHintAfter(@1, EHintSourceType::ATTR_NAME);
  #else
    chart.Error.Error(AFTER(@1), "Missing ']'.");
  #endif
}
                   | TOK_OSBRACKET error
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACKET);
    csh.AddCSH_Error(@2, "Missing a ']'.");
  	csh.SqBracketPairs.push_back(@$);
    csh.CheckHintBetween(@1, @2, EHintSourceType::ATTR_NAME);
  #else
    chart.Error.Error(@2.start, "Missing ']'.");
  #endif
};

attrlist:    attr
{
  #ifdef C_S_H_IS_COMPILED
  #else
    $$.Append(std::move($1));
  #endif
}
              | attrlist TOK_COMMA attr
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    csh.CheckHintBetween(@2, @3, EHintSourceType::ATTR_NAME);
  #else
    $1.Append(std::move($3));
    $$ = std::move($1);
  #endif
}
              | attrlist TOK_COMMA
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@2, COLOR_COMMA);
    csh.CheckHintAfter(@2, EHintSourceType::ATTR_NAME);
    csh.AddCSH_ErrorAfter(@2, "Missing attribute or style name.");
  #else
    $$ = std::move($1);
    chart.Error.Error(AFTER(@2), "Expecting an attribute or style name here.");
  #endif
};


attr:         alpha_string TOK_EQUAL color_string
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_ATTRNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    if (!$1.had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint(@3, $3, $1);
    csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME);
    if (!$1.had_error)
        csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_VALUE, $1);
  #else
    if (!$1.had_error && !chart.SkipContent()) 
        $$ = std::make_unique<Attribute>($1, $3, @1, @3);
  #endif
}
                | alpha_string TOK_EQUAL TOK_PLUS_PLUS color_string
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_ATTRNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    if (!$1.had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint(@3+@4, (string("++")+$4).c_str(), $1);
    csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME);
    if (!$1.had_error)
        csh.CheckHintBetweenAndAt(@2, @3+@4, EHintSourceType::ATTR_VALUE, $1);
  #else
    if (!$1.had_error || !chart.SkipContent())
        $$ = std::make_unique<Attribute>($1, StrCat("++", $4), @1, FileLineColRange(@3.start, @4.end));
  #endif
}
                | alpha_string TOK_EQUAL TOK_PLUS_PLUS
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
    csh.AddCSH_AttrName(@1, $1, COLOR_ATTRNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    if (!$1.had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint(@3, "++", $1);
  	csh.AddCSH_ErrorAfter(@3, "Continue with a color name or definition.");
    csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME);
    if (!$1.had_error)
        csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_VALUE, $1);
  #else
    if (!$1.had_error || !chart.SkipContent())
        $$ = std::make_unique<Attribute>($1, "++", @1, @3);
  #endif
}
        | alpha_string TOK_EQUAL TOK_NUMBER
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_ATTRNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    csh.AddCSH(@3, COLOR_ATTRVALUE);
    csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME);
    if (!$1.had_error)
        csh.CheckHintBetweenAndAt(@2, @3, EHintSourceType::ATTR_VALUE, $1);
  #else
    if (!$1.had_error || !chart.SkipContent())
        $$ = std::make_unique<Attribute>($1, $3, @$, @3);
  #endif
}
        | alpha_string TOK_EQUAL
{
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_AttrName(@1, $1, COLOR_ATTRNAME);
    csh.AddCSH(@2, COLOR_EQUAL);
    csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME);
    if (!$1.had_error)
        csh.CheckHintAfter(@2, EHintSourceType::ATTR_VALUE, $1);
  #else
    if (!$1.had_error || !chart.SkipContent())
        $$ = std::make_unique<Attribute>($1, std::string(), @$, @$);
  #endif
}
        | string
{
  //here we accept non alpha strings for "->" and similar style names
  #ifdef C_S_H_IS_COMPILED
    if (!$1.had_error)
        csh.AddCSH_StyleOrAttrName(@1, $1);
    csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME);
  #else
    if (!$1.had_error || !chart.SkipContent())
        $$ = std::make_unique<Attribute>($1, @$);
  #endif
}
 /* 'string' does not match "++", so we list it separately
     UNCOMMENT if you have ++ as a style
        | TOK_PLUS_PLUS
{
  //here we accept non alpha strings for "->" and similar style names
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_StyleOrAttrName(@1, "++");
    csh.CheckHintAt(@1, EHintSourceType::ATTR_NAME);
  #else
    if (!chart.SkipContent()) 
        $$ = std::make_unique<Attribute>("++", @$);
  #endif
}*/
;



/* Add all your keywords here (that parse to a separate token of their own)
 * so that they can be used as style names*/
reserved_word_string: TOK_XXX 
    | TOK_COMMAND_DEFSHAPE | TOK_COMMAND_DEFCOLOR | TOK_COMMAND_DEFSTYLE | TOK_COMMAND_DEFDESIGN
    | TOK_COMMAND_DEFPROC | TOK_COMMAND_REPLAY | TOK_COMMAND_SET 
    | TOK_IF | TOK_THEN | TOK_ELSE | TOK_BYE;

/* List here all your symbols, such as -> that are not alpahnumeric but have default style names.*/
symbol_string: TOK__NEVER__HAPPENS
{    
    $$ = nullptr;
};

//will not be a reserved word, symbol or style name
entity_string_single: TOK_STRING
             | TOK_QSTRING
{
  #ifdef C_S_H_IS_COMPILED
	  csh.AddQuotedString(@1);
  #endif
    $$ = std::move($1);
}
               | TOK_SHAPE_COMMAND
{
	  $$.push_back(ShapeElement::act_code[$1]);
};

alpha_string_single: reserved_word_string;

string_single: symbol_string;

tok_param_name_as_multi: TOK_PARAM_NAME
{
  #ifdef C_S_H_IS_COMPILED  
    if ($1.length()<2 || $1.front()!='$') {
        csh.AddCSH_Error(@1, "Need name after the '$' sign.");
        $$.had_error = true;
    }
  #else
    if ($1.length()<2 || $1.front()!='$') {
        chart.Error.Error(@1.start, "Need name after the '$' sign.");
        $$.had_error = true;
    } else if (!chart.SkipContent()) {
        //When parsing a procedure we we accept all params
        //as they may be variables defined later
        auto p = chart.GetParameter($1);
        if (p==nullptr) {
            chart.Error.Error(@1.start, "Undefined parameter or variable name.");
            $$.had_error = true;
        } else { 
            $$.str = StringFormat::PushPosEscapes(p->value, @1.start);
        }
    } 
  #endif
};



multi_string_continuation: TOK_TILDE
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter(@1, "Missing string to concatenate after '~'.");
  #else
    chart.Error.Error(AFTER(@1), "Missing string to concatenate after '~'.");
  #endif
    $$.had_error = true;
} 
                     | TOK_TILDE string
{
    $$ = std::move($2);
};

entity_string_single_or_param: entity_string_single 
{
    $$.str = std::move($1);
} 
                    | tok_param_name_as_multi;

entity_string: entity_string_single_or_param 
{
    $$ = std::move($1);
} 
               | entity_string_single_or_param multi_string_continuation
{
    $$.CombineThemToMe($1, $2);
};

alpha_string: entity_string 
               | alpha_string_single
{
    $$.str = std::move($1);
} 
               | alpha_string_single multi_string_continuation
{
    $$.CombineThemToMe($1, $2);
};

string: alpha_string
        | string_single 
{
    $$.str = std::move($1);
} 
        | string_single multi_string_continuation
{
    $$.CombineThemToMe($1, $2);
};


scope_open: TOK_OCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH(@1, COLOR_BRACE);
    csh.PushContext();
    if (csh.CheckHintAfter(@1, EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (param.open_context_mode == base_parse_parm::EScopeOpenMode::PROC_REPLAY) {
        //Open a scope to replay a procedure. The text of the procedure has
        //already been placed to the lex buffer stack (in fact the '{' already comes
        //from there), and the parameters are in param.
        param.open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
        chart.PushContext(@1.start, EContextParse::REPARSING);
        chart.MyCurrentContext().starts_procedure = true;
        chart.MyCurrentContext().parameters = std::move(param.last_procedure_params);
        chart.MyCurrentContext().export_colors = param.last_procedure->export_colors;
        chart.MyCurrentContext().export_styles = param.last_procedure->export_styles;
        param.last_procedure = nullptr;
    } else {
        //Just open a regular scope
        chart.PushContext(@1.start);
    }
  #endif
    $1; //suppress
};

scope_close: TOK_CCBRACKET
{
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
    csh.AddCSH(@1, COLOR_BRACE);
  #else
    $$ = chart.PopContext();
  #endif
    $1; //suppress
};


%%


/* END OF FILE */
