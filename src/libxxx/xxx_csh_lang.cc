#define COLOR_SYNTAX_HIGHLIGHT 1 /* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         xxxcsh_parse
#define yylex           xxxcsh_lex
#define yyerror         xxxcsh_error
#define yydebug         xxxcsh_debug
#define yynerrs         xxxcsh_nerrs

/* First part of user prologue.  */
#line 15 "xxx_lang.yy"

/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT //so that subsequent definition in xxx_csh_lang2.h causes no warning
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #define YYMSC_RESULT_TYPE XxxCsh
    #define RESULT csh
    #include "cgen_shapes.h"
    #include "xxxcsh.h"
    #include "xxxchart.h"
    #define YYGET_EXTRA xxxcsh_get_extra
    #define YYLTYPE_IS_DECLARED
    #define YYLTYPE CshPos
    #define CHAR_IF_CSH(A) char
#else
    #define C_S_H (0)
    #define YYMSC_RESULT_TYPE XxxChart
    #define RESULT chart
    #define YYGET_EXTRA xxx_get_extra
    #define CHAR_IF_CSH(A) A
    #include "cgen_shapes.h"
    #include "xxxchart.h"
#endif

using namespace xxx;


#line 123 "xxx_csh_lang.cc"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "xxx_csh_lang.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* TOK_EOF  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_TOK_STRING = 3,                 /* TOK_STRING  */
  YYSYMBOL_TOK_QSTRING = 4,                /* TOK_QSTRING  */
  YYSYMBOL_TOK_NUMBER = 5,                 /* TOK_NUMBER  */
  YYSYMBOL_TOK_DASH = 6,                   /* TOK_DASH  */
  YYSYMBOL_TOK_EQUAL = 7,                  /* TOK_EQUAL  */
  YYSYMBOL_TOK_COMMA = 8,                  /* TOK_COMMA  */
  YYSYMBOL_TOK_SEMICOLON = 9,              /* TOK_SEMICOLON  */
  YYSYMBOL_TOK_PLUS_PLUS = 10,             /* TOK_PLUS_PLUS  */
  YYSYMBOL_TOK_OCBRACKET = 11,             /* TOK_OCBRACKET  */
  YYSYMBOL_TOK_CCBRACKET = 12,             /* TOK_CCBRACKET  */
  YYSYMBOL_TOK_OSBRACKET = 13,             /* TOK_OSBRACKET  */
  YYSYMBOL_TOK_CSBRACKET = 14,             /* TOK_CSBRACKET  */
  YYSYMBOL_TOK_XXX = 15,                   /* TOK_XXX  */
  YYSYMBOL_TOK_SHAPE_COMMAND = 16,         /* TOK_SHAPE_COMMAND  */
  YYSYMBOL_TOK_COMP_OP = 17,               /* TOK_COMP_OP  */
  YYSYMBOL_TOK_COLON_STRING = 18,          /* TOK_COLON_STRING  */
  YYSYMBOL_TOK_COLON_QUOTED_STRING = 19,   /* TOK_COLON_QUOTED_STRING  */
  YYSYMBOL_TOK_COLORDEF = 20,              /* TOK_COLORDEF  */
  YYSYMBOL_TOK_COMMAND_DEFSHAPE = 21,      /* TOK_COMMAND_DEFSHAPE  */
  YYSYMBOL_TOK_COMMAND_DEFCOLOR = 22,      /* TOK_COMMAND_DEFCOLOR  */
  YYSYMBOL_TOK_COMMAND_DEFSTYLE = 23,      /* TOK_COMMAND_DEFSTYLE  */
  YYSYMBOL_TOK_COMMAND_DEFDESIGN = 24,     /* TOK_COMMAND_DEFDESIGN  */
  YYSYMBOL_TOK_TILDE = 25,                 /* TOK_TILDE  */
  YYSYMBOL_TOK_PARAM_NAME = 26,            /* TOK_PARAM_NAME  */
  YYSYMBOL_TOK_OPARENTHESIS = 27,          /* TOK_OPARENTHESIS  */
  YYSYMBOL_TOK_CPARENTHESIS = 28,          /* TOK_CPARENTHESIS  */
  YYSYMBOL_TOK_COMMAND_DEFPROC = 29,       /* TOK_COMMAND_DEFPROC  */
  YYSYMBOL_TOK_COMMAND_REPLAY = 30,        /* TOK_COMMAND_REPLAY  */
  YYSYMBOL_TOK_COMMAND_SET = 31,           /* TOK_COMMAND_SET  */
  YYSYMBOL_TOK_BYE = 32,                   /* TOK_BYE  */
  YYSYMBOL_TOK_IF = 33,                    /* TOK_IF  */
  YYSYMBOL_TOK_THEN = 34,                  /* TOK_THEN  */
  YYSYMBOL_TOK_ELSE = 35,                  /* TOK_ELSE  */
  YYSYMBOL_TOK_COMMAND_INCLUDE = 36,       /* TOK_COMMAND_INCLUDE  */
  YYSYMBOL_TOK_UNRECOGNIZED_CHAR = 37,     /* TOK_UNRECOGNIZED_CHAR  */
  YYSYMBOL_TOK__NEVER__HAPPENS = 38,       /* TOK__NEVER__HAPPENS  */
  YYSYMBOL_YYACCEPT = 39,                  /* $accept  */
  YYSYMBOL_chart_with_bye = 40,            /* chart_with_bye  */
  YYSYMBOL_eof = 41,                       /* eof  */
  YYSYMBOL_chart = 42,                     /* chart  */
  YYSYMBOL_top_level_instrlist = 43,       /* top_level_instrlist  */
  YYSYMBOL_braced_instrlist = 44,          /* braced_instrlist  */
  YYSYMBOL_instrlist = 45,                 /* instrlist  */
  YYSYMBOL_several_instructions = 46,      /* several_instructions  */
  YYSYMBOL_instr_with_semicolon = 47,      /* instr_with_semicolon  */
  YYSYMBOL_proc_invocation = 48,           /* proc_invocation  */
  YYSYMBOL_proc_param_list = 49,           /* proc_param_list  */
  YYSYMBOL_proc_invoc_param_list = 50,     /* proc_invoc_param_list  */
  YYSYMBOL_proc_invoc_param = 51,          /* proc_invoc_param  */
  YYSYMBOL_include = 52,                   /* include  */
  YYSYMBOL_optlist_with_semicolon = 53,    /* optlist_with_semicolon  */
  YYSYMBOL_instr = 54,                     /* instr  */
  YYSYMBOL_optlist = 55,                   /* optlist  */
  YYSYMBOL_opt = 56,                       /* opt  */
  YYSYMBOL_styledeflist = 57,              /* styledeflist  */
  YYSYMBOL_styledef = 58,                  /* styledef  */
  YYSYMBOL_stylenamelist = 59,             /* stylenamelist  */
  YYSYMBOL_shapedef = 60,                  /* shapedef  */
  YYSYMBOL_shapedeflist = 61,              /* shapedeflist  */
  YYSYMBOL_shapeline = 62,                 /* shapeline  */
  YYSYMBOL_colordeflist = 63,              /* colordeflist  */
  YYSYMBOL_color_string = 64,              /* color_string  */
  YYSYMBOL_colordef = 65,                  /* colordef  */
  YYSYMBOL_designdef = 66,                 /* designdef  */
  YYSYMBOL_scope_open_empty = 67,          /* scope_open_empty  */
  YYSYMBOL_designelementlist = 68,         /* designelementlist  */
  YYSYMBOL_designelement = 69,             /* designelement  */
  YYSYMBOL_designoptlist = 70,             /* designoptlist  */
  YYSYMBOL_designopt = 71,                 /* designopt  */
  YYSYMBOL_defproc = 72,                   /* defproc  */
  YYSYMBOL_defprochelp1 = 73,              /* defprochelp1  */
  YYSYMBOL_defprochelp2 = 74,              /* defprochelp2  */
  YYSYMBOL_defprochelp3 = 75,              /* defprochelp3  */
  YYSYMBOL_defprochelp4 = 76,              /* defprochelp4  */
  YYSYMBOL_scope_open_proc_body = 77,      /* scope_open_proc_body  */
  YYSYMBOL_scope_close_proc_body = 78,     /* scope_close_proc_body  */
  YYSYMBOL_proc_def_arglist_tested = 79,   /* proc_def_arglist_tested  */
  YYSYMBOL_proc_def_arglist = 80,          /* proc_def_arglist  */
  YYSYMBOL_proc_def_param_list = 81,       /* proc_def_param_list  */
  YYSYMBOL_proc_def_param = 82,            /* proc_def_param  */
  YYSYMBOL_procedure_body = 83,            /* procedure_body  */
  YYSYMBOL_set = 84,                       /* set  */
  YYSYMBOL_comp = 85,                      /* comp  */
  YYSYMBOL_condition = 86,                 /* condition  */
  YYSYMBOL_ifthen_condition = 87,          /* ifthen_condition  */
  YYSYMBOL_else = 88,                      /* else  */
  YYSYMBOL_ifthen = 89,                    /* ifthen  */
  YYSYMBOL_colon_string = 90,              /* colon_string  */
  YYSYMBOL_full_attrlist_with_label = 91,  /* full_attrlist_with_label  */
  YYSYMBOL_full_attrlist = 92,             /* full_attrlist  */
  YYSYMBOL_attrlist = 93,                  /* attrlist  */
  YYSYMBOL_attr = 94,                      /* attr  */
  YYSYMBOL_reserved_word_string = 95,      /* reserved_word_string  */
  YYSYMBOL_symbol_string = 96,             /* symbol_string  */
  YYSYMBOL_entity_string_single = 97,      /* entity_string_single  */
  YYSYMBOL_alpha_string_single = 98,       /* alpha_string_single  */
  YYSYMBOL_string_single = 99,             /* string_single  */
  YYSYMBOL_tok_param_name_as_multi = 100,  /* tok_param_name_as_multi  */
  YYSYMBOL_multi_string_continuation = 101, /* multi_string_continuation  */
  YYSYMBOL_entity_string_single_or_param = 102, /* entity_string_single_or_param  */
  YYSYMBOL_entity_string = 103,            /* entity_string  */
  YYSYMBOL_alpha_string = 104,             /* alpha_string  */
  YYSYMBOL_string = 105,                   /* string  */
  YYSYMBOL_scope_open = 106,               /* scope_open  */
  YYSYMBOL_scope_close = 107               /* scope_close  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;


/* Second part of user prologue.  */
#line 144 "xxx_lang.yy"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <iostream>

#ifdef C_S_H_IS_COMPILED
    #include "xxx_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "xxx_csh_lang2.h"  //Needs parse_param from xxx_lang_misc.h
    /* yyerror
     *  Error handling function.  Do nothing for CSH */
    void yyerror(YYLTYPE* /*loc*/, Csh & /*csh*/, void * /*yyscanner*/, const char * /*str*/) {}
#else
    #include "xxx_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
    #include "xxx_lang2.h"      //Needs parse_param from xxx_lang_misc.h
    /* Use verbose error reporting such that the expected token names are dumped */
    //#define YYERROR_VERBOSE
    void yyerror(YYLTYPE *loc, YYMSC_RESULT_TYPE &chart, void *yyscanner, const char *str)
    {
        chart.Error.Error(CHART_POS_START(*loc), str);
    }
#endif

#ifdef C_S_H_IS_COMPILED
void XxxCshParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#else
void XxxParse(YYMSC_RESULT_TYPE &RESULT, const char *buff, unsigned len)
#endif
{
    std::remove_pointer<YY_EXTRA_TYPE>::type pp;
    pp.RESULT = &RESULT;
#ifdef C_S_H_IS_COMPILED
    pp.buff.buf = buff;
    pp.buff.length = len;
    pp.buff.pos = 0;
    xxxcsh_lex_init(&pp.yyscanner);
    xxxcsh_set_extra(&pp, pp.yyscanner);
    xxxcsh_parse(RESULT, pp.yyscanner);
    xxxcsh_lex_destroy(pp.yyscanner);
#else
    pp.buffs.emplace_back(buff, len);
    pp.pos_stack.file = RESULT.current_file;
    xxx_lex_init(&pp.yyscanner);
    xxx_set_extra(&pp, pp.yyscanner);
    xxx_parse(RESULT, pp.yyscanner);
    xxx_lex_destroy(pp.yyscanner);
#endif
}


#line 316 "xxx_csh_lang.cc"


#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  94
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1341

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  39
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  69
/* YYNRULES -- Number of rules.  */
#define YYNRULES  245
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  278

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   293


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   198,   198,   202,   212,   213,   222,   234,   245,   259,
     260,   270,   285,   294,   306,   318,   331,   344,   355,   369,
     383,   392,   399,   400,   409,   410,   420,   431,   445,   458,
     475,   492,   516,   535,   554,   580,   600,   614,   630,   652,
     663,   690,   699,   710,   720,   732,   743,   753,   764,   773,
     786,   796,   812,   830,   840,   853,   868,   878,   891,   910,
     928,   949,   965,   983,   999,  1017,  1035,  1057,  1063,  1070,
    1077,  1091,  1108,  1117,  1132,  1145,  1161,  1184,  1208,  1229,
    1230,  1240,  1253,  1270,  1280,  1294,  1308,  1324,  1335,  1349,
    1371,  1391,  1413,  1425,  1438,  1450,  1459,  1475,  1503,  1527,
    1544,  1562,  1591,  1619,  1649,  1682,  1683,  1693,  1707,  1708,
    1717,  1742,  1768,  1787,  1805,  1845,  1888,  1900,  1901,  1912,
    1926,  1943,  1957,  1974,  1976,  1977,  1987,  1997,  2004,  2025,
    2047,  2067,  2106,  2125,  2143,  2155,  2166,  2176,  2185,  2193,
    2202,  2217,  2233,  2244,  2260,  2271,  2295,  2304,  2315,  2325,
    2337,  2348,  2358,  2369,  2380,  2396,  2414,  2433,  2455,  2477,
    2491,  2504,  2522,  2541,  2560,  2576,  2596,  2614,  2626,  2637,
    2639,  2647,  2660,  2683,  2697,  2725,  2746,  2760,  2774,  2795,
    2828,  2849,  2863,  2875,  2889,  2905,  2919,  2948,  2964,  2980,
    2988,  2997,  3005,  3013,  3025,  3033,  3036,  3047,  3058,  3070,
    3083,  3095,  3107,  3119,  3132,  3139,  3148,  3161,  3182,  3203,
    3224,  3243,  3260,  3296,  3297,  3297,  3297,  3297,  3298,  3298,
    3298,  3299,  3299,  3299,  3299,  3302,  3308,  3309,  3316,  3322,
    3324,  3327,  3359,  3371,  3376,  3380,  3382,  3386,  3391,  3392,
    3396,  3401,  3402,  3406,  3412,  3441
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "TOK_EOF", "error", "\"invalid token\"", "TOK_STRING", "TOK_QSTRING",
  "TOK_NUMBER", "TOK_DASH", "TOK_EQUAL", "TOK_COMMA", "TOK_SEMICOLON",
  "TOK_PLUS_PLUS", "TOK_OCBRACKET", "TOK_CCBRACKET", "TOK_OSBRACKET",
  "TOK_CSBRACKET", "TOK_XXX", "TOK_SHAPE_COMMAND", "TOK_COMP_OP",
  "TOK_COLON_STRING", "TOK_COLON_QUOTED_STRING", "TOK_COLORDEF",
  "TOK_COMMAND_DEFSHAPE", "TOK_COMMAND_DEFCOLOR", "TOK_COMMAND_DEFSTYLE",
  "TOK_COMMAND_DEFDESIGN", "TOK_TILDE", "TOK_PARAM_NAME",
  "TOK_OPARENTHESIS", "TOK_CPARENTHESIS", "TOK_COMMAND_DEFPROC",
  "TOK_COMMAND_REPLAY", "TOK_COMMAND_SET", "TOK_BYE", "TOK_IF", "TOK_THEN",
  "TOK_ELSE", "TOK_COMMAND_INCLUDE", "TOK_UNRECOGNIZED_CHAR",
  "TOK__NEVER__HAPPENS", "$accept", "chart_with_bye", "eof", "chart",
  "top_level_instrlist", "braced_instrlist", "instrlist",
  "several_instructions", "instr_with_semicolon", "proc_invocation",
  "proc_param_list", "proc_invoc_param_list", "proc_invoc_param",
  "include", "optlist_with_semicolon", "instr", "optlist", "opt",
  "styledeflist", "styledef", "stylenamelist", "shapedef", "shapedeflist",
  "shapeline", "colordeflist", "color_string", "colordef", "designdef",
  "scope_open_empty", "designelementlist", "designelement",
  "designoptlist", "designopt", "defproc", "defprochelp1", "defprochelp2",
  "defprochelp3", "defprochelp4", "scope_open_proc_body",
  "scope_close_proc_body", "proc_def_arglist_tested", "proc_def_arglist",
  "proc_def_param_list", "proc_def_param", "procedure_body", "set", "comp",
  "condition", "ifthen_condition", "else", "ifthen", "colon_string",
  "full_attrlist_with_label", "full_attrlist", "attrlist", "attr",
  "reserved_word_string", "symbol_string", "entity_string_single",
  "alpha_string_single", "string_single", "tok_param_name_as_multi",
  "multi_string_continuation", "entity_string_single_or_param",
  "entity_string", "alpha_string", "string", "scope_open", "scope_close", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-177)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-203)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    1206,  -177,  -177,  -177,  -177,    86,  -177,    61,  1287,  1142,
      32,  -177,  1230,  1287,    -5,   244,    63,    83,    18,  -177,
    -177,  1175,  -177,  -177,   465,   715,  -177,   749,   499,  -177,
    -177,  -177,  -177,   533,  -177,  -177,  -177,    45,    78,   885,
     157,  -177,  -177,    75,  -177,    95,  -177,    90,  -177,  -177,
    -177,  -177,  -177,  -177,  -177,  -177,  -177,  -177,  -177,  -177,
      81,  -177,  -177,    45,  -177,    99,  -177,    89,  -177,    50,
    -177,    45,  -177,  -177,    97,  -177,  -177,   357,  -177,  -177,
    -177,   919,    28,  -177,  -177,   104,    20,  -177,   109,  -177,
      84,    85,   394,  -177,  -177,  -177,    29,   108,  -177,  1206,
    -177,  -177,   111,  -177,   202,   783,   112,  -177,   114,  -177,
     116,   570,  -177,    91,   604,  1142,  -177,    22,  -177,  -177,
    -177,   817,  -177,   113,  -177,   283,  -177,   121,  -177,  -177,
      75,   641,  1287,  -177,   986,  1142,  1142,  -177,  -177,  -177,
      70,   103,  -177,   320,  -177,  -177,  -177,  -177,   851,  -177,
    -177,  -177,  -177,  1022,  -177,  -177,  1142,  -177,  1142,  -177,
    -177,  -177,  -177,   105,  -177,  1058,  -177,   431,  -177,  -177,
     123,  -177,  -177,  -177,  -177,  -177,  -177,  -177,  1308,    91,
    1308,  -177,  -177,  -177,  -177,    24,  -177,  -177,  -177,   124,
    1142,  -177,   953,  -177,   119,   135,   678,   133,  -177,  1082,
    -177,  -177,  -177,  -177,  -177,  1287,  1142,   134,  -177,    94,
    -177,   137,  -177,   117,    -5,  -177,  -177,    30,  -177,  -177,
    -177,  -177,  -177,  -177,  -177,  -177,   118,  1058,  -177,  -177,
    -177,  1308,  -177,  -177,  -177,  -177,  -177,  -177,  1082,  -177,
     139,   149,   143,  -177,   147,  -177,  -177,    81,    89,    68,
    -177,    61,  1118,  -177,  -177,  -177,  -177,  -177,  -177,  -177,
    -177,  -177,  1263,  -177,  -177,   150,  -177,  -177,  -177,  -177,
    -177,   158,   159,  -177,   162,  -177,   165,  -177
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       7,   226,   227,    28,   244,    70,   228,    60,    62,    64,
      66,   231,   132,    39,   168,     0,    54,     0,     0,     8,
      25,     9,    22,    20,     0,     0,    24,     0,     0,    72,
      67,   131,    68,     0,    69,   234,   235,   236,     0,     0,
       0,   190,   189,   191,    71,   195,    59,    87,   213,   214,
     215,   216,   217,   218,   219,   220,   224,   221,   222,   223,
      61,   105,   229,   239,   238,   113,   225,    63,    79,    83,
     230,   242,   241,    84,     0,    65,   143,     0,   133,   136,
     139,     0,   137,   145,   142,   140,   134,    40,   155,   167,
     177,   175,     0,    55,     1,     4,     0,     5,     2,    10,
      23,    21,     0,    30,     0,     0,     0,    36,     0,    27,
       0,     0,    57,   182,     0,   232,   237,    78,    17,   245,
      19,     0,    13,   203,   196,     0,   204,   241,   212,   192,
     194,     0,   107,   240,   112,    81,    85,    82,   243,   116,
       0,     0,   146,     0,   152,   164,   144,   166,     0,   160,
     138,   141,   135,   156,   178,   174,     0,   169,   171,     3,
       6,    11,    31,     0,    53,    48,    41,     0,    47,    52,
       0,    33,    38,    29,    58,    75,    73,   179,   185,     0,
     183,   233,    76,    77,    16,     0,    18,    12,   199,   201,
     206,   197,   211,   193,     0,    96,     0,     0,   106,     0,
     108,   110,   109,    80,    86,   120,   122,     0,   117,     0,
     124,     0,   147,     0,   153,   151,   163,     0,   165,   159,
     158,   157,   173,   172,    42,    49,     0,    50,    46,    34,
     188,   184,   186,    15,    14,   198,   205,   210,   209,   207,
       0,    97,    95,    90,     0,    92,   111,   119,   121,     0,
     127,   126,   130,   149,   154,   162,   161,    44,    51,   187,
     208,    93,    98,    91,    94,     0,   114,   118,   125,   128,
     129,   101,    99,   115,   102,   100,   103,   104
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -177,  -177,    98,  -177,   100,  -177,   -22,   -14,   -11,  -177,
    -177,  -177,  -151,  -177,  -177,   -27,  -177,    66,   -32,    73,
    -177,  -177,  -177,  -128,   -20,  -176,    64,  -177,  -177,  -177,
     -51,  -177,   -50,  -177,  -177,  -177,   126,   127,  -177,  -136,
    -177,  -177,  -177,   -75,   130,  -177,  -177,  -177,  -177,  -101,
    -177,   171,  -177,     6,  -177,    37,  -177,  -177,  -177,  -177,
    -177,  -177,   -43,  -177,     1,    -8,    -6,  -177,  -106
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,    17,    98,    18,    19,    20,    21,    22,    23,    24,
     105,   167,   168,    25,    26,    27,    28,    29,    67,    68,
      69,    46,   196,   197,    60,   201,    61,    75,   140,   207,
     208,   209,   210,    30,    31,    78,    79,    80,    81,   149,
      82,    83,   143,    89,    84,    32,   158,    91,    33,   178,
      34,    43,    44,    85,   125,   126,    62,    70,    35,    63,
      71,    36,   116,    37,    64,    72,   202,    39,   122
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      65,    38,   144,    73,    86,    87,   114,   100,    47,    92,
     101,    45,   219,   180,   225,   187,   239,   121,    95,    96,
     133,    88,    38,   246,   233,     1,     2,   182,   138,    95,
     255,    76,   127,    40,   128,    74,   119,    48,     6,    76,
      38,    40,   146,    49,    50,    51,    52,    77,    11,   129,
      97,    53,    54,    55,    56,    57,    58,    59,   136,   148,
      66,    97,   260,    40,     1,     2,   240,    93,   244,   265,
     115,     1,     2,     1,     2,   137,   258,     6,   231,   234,
     266,   256,    38,    94,     6,   117,     6,    11,    40,   132,
     205,   206,   205,   206,    11,   250,    11,   135,   169,    40,
      38,   131,   251,  -123,    41,    42,   134,   100,   139,   181,
     101,   183,    38,    41,    42,    76,   153,   160,   154,   155,
     162,   172,    38,   173,    65,   174,   177,   188,   192,    73,
     204,   212,   229,   224,   100,   195,   193,   101,   235,   254,
     241,   211,   245,   249,   252,   253,   257,   221,   261,    38,
     222,   230,   223,   232,   262,   263,   264,  -202,   123,   169,
       1,     2,   273,   274,   275,  -202,  -202,   276,  -202,  -202,
     277,   124,    48,     6,   248,  -202,  -202,   176,    49,    50,
      51,    52,   127,    11,   128,   247,    53,    54,    55,    56,
      57,    58,    59,  -202,   159,    66,   198,    65,   267,   161,
      73,   268,   -43,   163,   259,     1,     2,   164,   203,   150,
     165,   -43,   152,   -43,   -43,   151,   130,    48,     6,     0,
       0,   169,     0,    49,    50,    51,    52,   236,    11,     0,
     166,    53,    54,    55,    56,    57,    58,    59,   -43,     0,
      66,     0,     0,     0,  -176,    90,   270,     1,     2,     0,
     211,     0,   211,  -176,   272,  -176,  -176,     0,     0,    48,
       6,     0,     0,     0,     0,    49,    50,    51,    52,     0,
      11,     0,     0,    53,    54,    55,    56,    57,    58,    59,
    -176,     0,    66,  -200,   189,     0,  -200,  -200,     0,     0,
       0,   190,  -200,     0,  -200,  -200,     0,   191,  -200,  -200,
       0,  -200,  -200,     0,  -200,  -200,  -200,  -200,     0,  -200,
       0,     0,  -200,  -200,  -200,  -200,  -200,     0,  -200,  -200,
    -150,   213,     0,  -150,  -150,     0,     0,     0,   214,  -150,
       0,  -150,  -150,  -150,     0,  -150,  -150,     0,     0,     0,
       0,  -150,  -150,  -150,  -150,     0,  -150,     0,   215,  -150,
    -150,  -150,  -150,  -150,     0,  -150,  -150,  -148,   141,     0,
    -148,  -148,     0,     0,     0,     0,  -148,     0,  -148,  -148,
    -148,     0,  -148,  -148,     0,     0,     0,     0,  -148,  -148,
    -148,  -148,     0,    88,     0,   142,  -148,  -148,  -148,  -148,
    -148,     0,  -148,  -148,  -170,   156,     0,  -170,  -170,     0,
       0,     0,     0,  -170,     0,  -170,  -170,     0,     0,  -170,
    -170,   157,     0,     0,     0,  -170,  -170,  -170,  -170,     0,
    -170,     0,     0,  -170,  -170,  -170,  -170,  -170,  -170,  -170,
    -170,   -45,   226,     0,   -45,   -45,     0,     0,     0,   227,
     -45,     0,   -45,   -45,     0,     0,   -45,   -45,     0,     0,
       0,     0,   -45,   -45,   -45,   -45,     0,   -45,     0,   228,
     -45,   -45,   -45,   -45,   -45,   -32,   102,   -45,   -32,   -32,
       0,     0,     0,     0,   103,     0,   -32,   -32,     0,     0,
     -32,   -32,     0,     0,     0,     0,   -32,   -32,   -32,   -32,
       0,   -32,   104,     0,   -32,   -32,   -32,   -32,   -32,   -56,
     110,   -32,   -56,   -56,     0,     0,     0,   111,   112,     0,
     -56,   -56,     0,     0,   -56,   -56,     0,     0,     0,     0,
     -56,   -56,   -56,   -56,     0,   -56,     0,     0,   -56,   -56,
     -56,   -56,   -56,  -181,   113,   -56,  -181,  -181,     0,     0,
       0,     0,  -181,     0,  -181,  -181,     0,     0,     5,  -181,
       0,     0,     0,     0,     7,     8,     9,    10,     0,  -181,
       0,     0,    12,  -181,    14,  -181,    15,     0,  -181,  -181,
     -74,   175,     0,     1,     2,     0,     0,     0,   -74,   -74,
       0,   -74,   -74,     0,     0,   -74,     6,     0,     0,     0,
       0,   -74,   -74,   -74,   -74,     0,    11,     0,     0,   -74,
     -74,   -74,   -74,   -74,  -180,   179,   -74,  -180,  -180,     0,
       0,     0,     0,  -180,     0,  -180,  -180,     0,     0,  -180,
    -180,     0,     0,     0,     0,  -180,  -180,  -180,  -180,     0,
    -180,     0,     0,  -180,  -180,  -180,  -180,  -180,     0,   177,
    -180,   -88,   194,     0,   -88,   -88,     0,     0,     0,     0,
     -88,     0,   -88,   -88,     0,     0,   -88,   195,     0,     0,
       0,     0,   -88,   -88,   -88,   -88,     0,   -88,     0,     0,
     -88,   -88,   -88,   -88,   -88,     0,   -88,   -88,   -89,   242,
       0,   -89,   -89,     0,     0,     0,     0,   -89,     0,   -89,
     243,     0,     0,   -89,   195,     0,     0,     0,     0,   -89,
     -89,   -89,   -89,     0,   -89,     0,     0,   -89,   -89,   -89,
     -89,   -89,     0,   -89,   -89,   -37,   106,     0,   -37,   -37,
       0,     0,     0,     0,   107,     0,   -37,   -37,     0,     0,
     -37,   -37,     0,     0,     0,     0,   -37,   -37,   -37,   -37,
       0,   -37,     0,     0,   -37,   -37,   -37,   -37,   -37,   -26,
     108,   -37,   -26,   -26,     0,     0,     0,     0,   109,     0,
     -26,   -26,     0,     0,   -26,   -26,     0,     0,     0,     0,
     -26,   -26,   -26,   -26,     0,   -26,     0,     0,   -26,   -26,
     -26,   -26,   -26,   -35,   170,   -26,   -35,   -35,     0,     0,
       0,     0,   171,     0,   -35,   -35,     0,     0,   -35,   -35,
       0,     0,     0,     0,   -35,   -35,   -35,   -35,     0,   -35,
       0,     0,   -35,   -35,   -35,   -35,   -35,   184,   185,   -35,
       1,     2,     0,     0,     0,     0,     3,     0,     4,   119,
       0,     0,     5,     6,     0,     0,     0,     0,     7,     8,
       9,    10,     0,    11,     0,     0,    12,    13,    14,   186,
      15,   216,   217,    16,     1,     2,     0,     0,     0,     0,
       3,     0,     4,   146,     0,     0,     5,     6,     0,     0,
       0,     0,     7,     8,     9,    10,     0,    11,     0,     0,
      12,    13,    14,   218,    15,   118,     0,    16,     1,     2,
       0,     0,     0,     0,     3,     0,     4,   119,     0,     0,
       5,     6,     0,     0,     0,     0,     7,     8,     9,    10,
       0,    11,     0,     0,    12,    13,    14,   120,    15,   145,
       0,    16,     1,     2,     0,     0,     0,     0,     3,     0,
       4,   146,     0,     0,     5,     6,     0,     0,     0,     0,
       7,     8,     9,    10,     0,    11,     0,     0,    12,    13,
      14,   147,    15,     0,     0,    16,     1,     2,   237,     0,
       0,     0,     0,   238,     0,     0,     0,     0,    48,     6,
       0,     0,     0,   200,    49,    50,    51,    52,     0,    11,
       0,     0,    53,    54,    55,    56,    57,    58,    59,     1,
       2,    66,     0,     0,     0,     0,   199,     0,     0,     0,
       0,    48,     6,     0,     0,     0,   200,    49,    50,    51,
      52,     0,    11,     0,     0,    53,    54,    55,    56,    57,
      58,    59,     0,     0,    66,     1,     2,   220,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    48,     6,     0,
       0,     0,     0,    49,    50,    51,    52,     0,    11,     0,
       0,    53,    54,    55,    56,    57,    58,    59,     0,     0,
      66,     1,     2,   164,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    48,     6,     0,     0,     0,     0,    49,
      50,    51,    52,     0,    11,     1,     2,    53,    54,    55,
      56,    57,    58,    59,     0,     0,    66,    48,     6,     0,
       0,     0,   200,    49,    50,    51,    52,     0,    11,     0,
       0,    53,    54,    55,    56,    57,    58,    59,     0,     0,
      66,     1,     2,   269,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    48,     6,     0,     0,     0,     0,    49,
      50,    51,    52,     0,    11,     1,     2,    53,    54,    55,
      56,    57,    58,    59,     0,     0,    66,    48,     6,     0,
       0,     0,     0,    49,    50,    51,    52,     0,    11,     0,
       0,    53,    54,    55,    56,    57,    58,    59,     1,     2,
      66,     0,     0,     0,     3,     0,     4,    99,     0,     0,
       5,     6,     0,     0,     0,     0,     7,     8,     9,    10,
       0,    11,     0,     0,    12,    13,    14,     0,    15,     1,
       2,    16,     0,     0,     0,     3,     0,     4,     0,     0,
       0,     5,     6,     0,     0,     0,     0,     7,     8,     9,
      10,     0,    11,     1,     2,    12,    13,    14,     0,    15,
       0,    76,    16,    40,     0,    48,     6,     0,     0,     0,
       0,    49,    50,    51,    52,     0,    11,    77,     0,    53,
      54,    55,    56,    57,    58,    59,     1,     2,   271,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    48,     6,
       0,     0,     0,     0,    49,    50,    51,    52,     0,    11,
       1,     2,    53,    54,    55,    56,    57,    58,    59,     0,
       0,     0,    48,     6,     0,     0,     0,     0,    49,    50,
      51,    52,     0,    11,     0,     0,    53,    54,    55,    56,
      57,    58,    59,     5,     0,     0,     0,     0,     0,     7,
       8,     9,    10,     0,     0,     0,     0,    12,     0,    14,
       0,    15
};

static const yytype_int16 yycheck[] =
{
       8,     0,    77,     9,    12,    13,    33,    21,     7,    15,
      21,     5,   148,   114,   165,   121,   192,    39,     0,     1,
      63,    26,    21,   199,     0,     3,     4,     5,    71,     0,
       0,    11,    40,    13,    40,     3,    12,    15,    16,    11,
      39,    13,    12,    21,    22,    23,    24,    27,    26,    43,
      32,    29,    30,    31,    32,    33,    34,    35,     8,    81,
      38,    32,   238,    13,     3,     4,   194,     4,   196,     1,
      25,     3,     4,     3,     4,    69,   227,    16,   179,   185,
      12,   217,    81,     0,    16,     7,    16,    26,    13,     8,
      22,    23,    22,    23,    26,     1,    26,     8,   104,    13,
      99,    11,     8,     9,    18,    19,     7,   121,    11,   115,
     121,   117,   111,    18,    19,    11,     7,     9,    34,    34,
       9,     9,   121,     9,   132,     9,    35,    14,     7,   135,
     136,    28,     9,    28,   148,    16,   130,   148,    14,   214,
       5,   140,     9,     9,     7,    28,    28,   153,     9,   148,
     156,   178,   158,   180,     5,    12,     9,     0,     1,   165,
       3,     4,    12,     5,     5,     8,     9,     5,    11,    12,
       5,    14,    15,    16,   206,    18,    19,   111,    21,    22,
      23,    24,   190,    26,   190,   205,    29,    30,    31,    32,
      33,    34,    35,    36,    96,    38,   132,   205,   249,    99,
     206,   251,     0,     1,   231,     3,     4,     5,   135,    82,
       8,     9,    86,    11,    12,    85,    45,    15,    16,    -1,
      -1,   227,    -1,    21,    22,    23,    24,   190,    26,    -1,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    -1,
      38,    -1,    -1,    -1,     0,     1,   252,     3,     4,    -1,
     249,    -1,   251,     9,   262,    11,    12,    -1,    -1,    15,
      16,    -1,    -1,    -1,    -1,    21,    22,    23,    24,    -1,
      26,    -1,    -1,    29,    30,    31,    32,    33,    34,    35,
      36,    -1,    38,     0,     1,    -1,     3,     4,    -1,    -1,
      -1,     8,     9,    -1,    11,    12,    -1,    14,    15,    16,
      -1,    18,    19,    -1,    21,    22,    23,    24,    -1,    26,
      -1,    -1,    29,    30,    31,    32,    33,    -1,    35,    36,
       0,     1,    -1,     3,     4,    -1,    -1,    -1,     8,     9,
      -1,    11,    12,    13,    -1,    15,    16,    -1,    -1,    -1,
      -1,    21,    22,    23,    24,    -1,    26,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,    36,     0,     1,    -1,
       3,     4,    -1,    -1,    -1,    -1,     9,    -1,    11,    12,
      13,    -1,    15,    16,    -1,    -1,    -1,    -1,    21,    22,
      23,    24,    -1,    26,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,    36,     0,     1,    -1,     3,     4,    -1,
      -1,    -1,    -1,     9,    -1,    11,    12,    -1,    -1,    15,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    -1,
      26,    -1,    -1,    29,    30,    31,    32,    33,    34,    35,
      36,     0,     1,    -1,     3,     4,    -1,    -1,    -1,     8,
       9,    -1,    11,    12,    -1,    -1,    15,    16,    -1,    -1,
      -1,    -1,    21,    22,    23,    24,    -1,    26,    -1,    28,
      29,    30,    31,    32,    33,     0,     1,    36,     3,     4,
      -1,    -1,    -1,    -1,     9,    -1,    11,    12,    -1,    -1,
      15,    16,    -1,    -1,    -1,    -1,    21,    22,    23,    24,
      -1,    26,    27,    -1,    29,    30,    31,    32,    33,     0,
       1,    36,     3,     4,    -1,    -1,    -1,     8,     9,    -1,
      11,    12,    -1,    -1,    15,    16,    -1,    -1,    -1,    -1,
      21,    22,    23,    24,    -1,    26,    -1,    -1,    29,    30,
      31,    32,    33,     0,     1,    36,     3,     4,    -1,    -1,
      -1,    -1,     9,    -1,    11,    12,    -1,    -1,    15,    16,
      -1,    -1,    -1,    -1,    21,    22,    23,    24,    -1,    26,
      -1,    -1,    29,    30,    31,    32,    33,    -1,    35,    36,
       0,     1,    -1,     3,     4,    -1,    -1,    -1,     8,     9,
      -1,    11,    12,    -1,    -1,    15,    16,    -1,    -1,    -1,
      -1,    21,    22,    23,    24,    -1,    26,    -1,    -1,    29,
      30,    31,    32,    33,     0,     1,    36,     3,     4,    -1,
      -1,    -1,    -1,     9,    -1,    11,    12,    -1,    -1,    15,
      16,    -1,    -1,    -1,    -1,    21,    22,    23,    24,    -1,
      26,    -1,    -1,    29,    30,    31,    32,    33,    -1,    35,
      36,     0,     1,    -1,     3,     4,    -1,    -1,    -1,    -1,
       9,    -1,    11,    12,    -1,    -1,    15,    16,    -1,    -1,
      -1,    -1,    21,    22,    23,    24,    -1,    26,    -1,    -1,
      29,    30,    31,    32,    33,    -1,    35,    36,     0,     1,
      -1,     3,     4,    -1,    -1,    -1,    -1,     9,    -1,    11,
      12,    -1,    -1,    15,    16,    -1,    -1,    -1,    -1,    21,
      22,    23,    24,    -1,    26,    -1,    -1,    29,    30,    31,
      32,    33,    -1,    35,    36,     0,     1,    -1,     3,     4,
      -1,    -1,    -1,    -1,     9,    -1,    11,    12,    -1,    -1,
      15,    16,    -1,    -1,    -1,    -1,    21,    22,    23,    24,
      -1,    26,    -1,    -1,    29,    30,    31,    32,    33,     0,
       1,    36,     3,     4,    -1,    -1,    -1,    -1,     9,    -1,
      11,    12,    -1,    -1,    15,    16,    -1,    -1,    -1,    -1,
      21,    22,    23,    24,    -1,    26,    -1,    -1,    29,    30,
      31,    32,    33,     0,     1,    36,     3,     4,    -1,    -1,
      -1,    -1,     9,    -1,    11,    12,    -1,    -1,    15,    16,
      -1,    -1,    -1,    -1,    21,    22,    23,    24,    -1,    26,
      -1,    -1,    29,    30,    31,    32,    33,     0,     1,    36,
       3,     4,    -1,    -1,    -1,    -1,     9,    -1,    11,    12,
      -1,    -1,    15,    16,    -1,    -1,    -1,    -1,    21,    22,
      23,    24,    -1,    26,    -1,    -1,    29,    30,    31,    32,
      33,     0,     1,    36,     3,     4,    -1,    -1,    -1,    -1,
       9,    -1,    11,    12,    -1,    -1,    15,    16,    -1,    -1,
      -1,    -1,    21,    22,    23,    24,    -1,    26,    -1,    -1,
      29,    30,    31,    32,    33,     0,    -1,    36,     3,     4,
      -1,    -1,    -1,    -1,     9,    -1,    11,    12,    -1,    -1,
      15,    16,    -1,    -1,    -1,    -1,    21,    22,    23,    24,
      -1,    26,    -1,    -1,    29,    30,    31,    32,    33,     0,
      -1,    36,     3,     4,    -1,    -1,    -1,    -1,     9,    -1,
      11,    12,    -1,    -1,    15,    16,    -1,    -1,    -1,    -1,
      21,    22,    23,    24,    -1,    26,    -1,    -1,    29,    30,
      31,    32,    33,    -1,    -1,    36,     3,     4,     5,    -1,
      -1,    -1,    -1,    10,    -1,    -1,    -1,    -1,    15,    16,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,    26,
      -1,    -1,    29,    30,    31,    32,    33,    34,    35,     3,
       4,    38,    -1,    -1,    -1,    -1,    10,    -1,    -1,    -1,
      -1,    15,    16,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    -1,    26,    -1,    -1,    29,    30,    31,    32,    33,
      34,    35,    -1,    -1,    38,     3,     4,     5,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    15,    16,    -1,
      -1,    -1,    -1,    21,    22,    23,    24,    -1,    26,    -1,
      -1,    29,    30,    31,    32,    33,    34,    35,    -1,    -1,
      38,     3,     4,     5,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    15,    16,    -1,    -1,    -1,    -1,    21,
      22,    23,    24,    -1,    26,     3,     4,    29,    30,    31,
      32,    33,    34,    35,    -1,    -1,    38,    15,    16,    -1,
      -1,    -1,    20,    21,    22,    23,    24,    -1,    26,    -1,
      -1,    29,    30,    31,    32,    33,    34,    35,    -1,    -1,
      38,     3,     4,     5,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    15,    16,    -1,    -1,    -1,    -1,    21,
      22,    23,    24,    -1,    26,     3,     4,    29,    30,    31,
      32,    33,    34,    35,    -1,    -1,    38,    15,    16,    -1,
      -1,    -1,    -1,    21,    22,    23,    24,    -1,    26,    -1,
      -1,    29,    30,    31,    32,    33,    34,    35,     3,     4,
      38,    -1,    -1,    -1,     9,    -1,    11,    12,    -1,    -1,
      15,    16,    -1,    -1,    -1,    -1,    21,    22,    23,    24,
      -1,    26,    -1,    -1,    29,    30,    31,    -1,    33,     3,
       4,    36,    -1,    -1,    -1,     9,    -1,    11,    -1,    -1,
      -1,    15,    16,    -1,    -1,    -1,    -1,    21,    22,    23,
      24,    -1,    26,     3,     4,    29,    30,    31,    -1,    33,
      -1,    11,    36,    13,    -1,    15,    16,    -1,    -1,    -1,
      -1,    21,    22,    23,    24,    -1,    26,    27,    -1,    29,
      30,    31,    32,    33,    34,    35,     3,     4,     5,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    15,    16,
      -1,    -1,    -1,    -1,    21,    22,    23,    24,    -1,    26,
       3,     4,    29,    30,    31,    32,    33,    34,    35,    -1,
      -1,    -1,    15,    16,    -1,    -1,    -1,    -1,    21,    22,
      23,    24,    -1,    26,    -1,    -1,    29,    30,    31,    32,
      33,    34,    35,    15,    -1,    -1,    -1,    -1,    -1,    21,
      22,    23,    24,    -1,    -1,    -1,    -1,    29,    -1,    31,
      -1,    33
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,     4,     9,    11,    15,    16,    21,    22,    23,
      24,    26,    29,    30,    31,    33,    36,    40,    42,    43,
      44,    45,    46,    47,    48,    52,    53,    54,    55,    56,
      72,    73,    84,    87,    89,    97,   100,   102,   103,   106,
      13,    18,    19,    90,    91,    92,    60,   103,    15,    21,
      22,    23,    24,    29,    30,    31,    32,    33,    34,    35,
      63,    65,    95,    98,   103,   104,    38,    57,    58,    59,
      96,    99,   104,   105,     3,    66,    11,    27,    74,    75,
      76,    77,    79,    80,    83,    92,   104,   104,    26,    82,
       1,    86,   105,     4,     0,     0,     1,    32,    41,    12,
      46,    47,     1,     9,    27,    49,     1,     9,     1,     9,
       1,     8,     9,     1,    54,    25,   101,     7,     0,    12,
      32,    45,   107,     1,    14,    93,    94,   104,   105,    92,
      90,    11,     8,   101,     7,     8,     8,    92,   101,    11,
      67,     1,    28,    81,    82,     0,    12,    32,    45,    78,
      76,    83,    75,     7,    34,    34,     1,    17,    85,    41,
       9,    43,     9,     1,     5,     8,    28,    50,    51,   105,
       1,     9,     9,     9,     9,     1,    56,    35,    88,     1,
      88,   105,     5,   105,     0,     1,    32,   107,    14,     1,
       8,    14,     7,    92,     1,    16,    61,    62,    65,    10,
      20,    64,   105,    58,   105,    22,    23,    68,    69,    70,
      71,   103,    28,     1,     8,    28,     0,     1,    32,    78,
       5,   105,   105,   105,    28,    51,     1,     8,    28,     9,
      54,    88,    54,     0,   107,    14,    94,     5,    10,    64,
      62,     5,     1,    12,    62,     9,    64,    63,    57,     9,
       1,     8,     7,    28,    82,     0,    78,    28,    51,    54,
      64,     9,     5,    12,     9,     1,    12,    69,    71,     5,
     105,     5,   104,    12,     5,     5,     5,     5
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    39,    40,    40,    41,    41,    41,    42,    42,    43,
      43,    43,    44,    44,    44,    44,    44,    44,    44,    44,
      45,    45,    45,    45,    46,    46,    47,    47,    47,    47,
      47,    47,    47,    47,    47,    47,    47,    47,    47,    48,
      48,    49,    49,    49,    49,    49,    49,    50,    50,    50,
      50,    50,    51,    51,    52,    52,    53,    53,    53,    54,
      54,    54,    54,    54,    54,    54,    54,    54,    54,    54,
      54,    54,    55,    55,    55,    55,    56,    56,    56,    57,
      57,    57,    58,    58,    59,    59,    59,    60,    60,    60,
      60,    60,    61,    61,    61,    61,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    63,    63,    63,    64,    64,
      65,    65,    65,    65,    66,    66,    67,    68,    68,    69,
      69,    69,    69,    69,    70,    70,    70,    70,    71,    71,
      71,    72,    73,    73,    74,    74,    74,    75,    75,    75,
      76,    76,    76,    77,    78,    79,    80,    80,    80,    80,
      80,    80,    81,    81,    81,    82,    82,    82,    82,    83,
      83,    83,    83,    83,    83,    83,    83,    84,    84,    85,
      86,    86,    86,    86,    87,    87,    87,    87,    87,    88,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    90,
      90,    91,    91,    91,    91,    91,    92,    92,    92,    92,
      92,    92,    92,    92,    93,    93,    93,    94,    94,    94,
      94,    94,    94,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    96,    97,    97,    97,    98,
      99,   100,   101,   101,   102,   102,   103,   103,   104,   104,
     104,   105,   105,   105,   106,   107
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     3,     1,     1,     2,     0,     1,     1,
       2,     3,     3,     2,     4,     4,     3,     2,     3,     2,
       1,     2,     1,     2,     1,     1,     1,     2,     1,     3,
       2,     3,     1,     3,     4,     2,     2,     1,     3,     1,
       2,     2,     3,     1,     4,     2,     3,     1,     1,     2,
       2,     3,     1,     1,     1,     2,     1,     2,     3,     2,
       1,     2,     1,     2,     1,     2,     1,     1,     1,     1,
       1,     2,     1,     3,     2,     3,     3,     3,     2,     1,
       3,     2,     2,     1,     1,     2,     3,     1,     2,     3,
       4,     5,     2,     3,     3,     2,     1,     2,     3,     4,
       5,     4,     5,     6,     7,     1,     3,     2,     1,     1,
       3,     4,     2,     1,     5,     6,     1,     1,     3,     2,
       1,     2,     1,     1,     1,     3,     2,     2,     3,     3,
       2,     1,     1,     2,     1,     2,     1,     1,     2,     1,
       1,     2,     1,     1,     1,     1,     2,     3,     1,     4,
       2,     3,     1,     2,     3,     1,     2,     3,     3,     3,
       2,     4,     4,     3,     2,     3,     2,     2,     1,     1,
       1,     2,     3,     3,     3,     2,     1,     2,     3,     1,
       2,     1,     2,     3,     4,     3,     4,     5,     4,     1,
       1,     1,     2,     3,     2,     1,     2,     3,     4,     3,
       2,     3,     1,     2,     1,     3,     2,     3,     4,     3,
       3,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     1,     1,     1,     2,     1,     1,
       2,     1,     1,     2,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, RESULT, yyscanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, RESULT, yyscanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), RESULT, yyscanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, RESULT, yyscanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (RESULT);
  YY_USE (yyscanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_TOK_STRING: /* TOK_STRING  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1555 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_QSTRING: /* TOK_QSTRING  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1561 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_NUMBER: /* TOK_NUMBER  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1567 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_OCBRACKET: /* TOK_OCBRACKET  */
#line 135 "xxx_lang.yy"
            {}
#line 1573 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_CCBRACKET: /* TOK_CCBRACKET  */
#line 135 "xxx_lang.yy"
            {}
#line 1579 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_XXX: /* TOK_XXX  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1585 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_SHAPE_COMMAND: /* TOK_SHAPE_COMMAND  */
#line 130 "xxx_lang.yy"
            { }
#line 1591 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMP_OP: /* TOK_COMP_OP  */
#line 135 "xxx_lang.yy"
            {}
#line 1597 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_STRING: /* TOK_COLON_STRING  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1603 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLON_QUOTED_STRING: /* TOK_COLON_QUOTED_STRING  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1609 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COLORDEF: /* TOK_COLORDEF  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1615 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFSHAPE: /* TOK_COMMAND_DEFSHAPE  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1621 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFCOLOR: /* TOK_COMMAND_DEFCOLOR  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1627 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFSTYLE: /* TOK_COMMAND_DEFSTYLE  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1633 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFDESIGN: /* TOK_COMMAND_DEFDESIGN  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1639 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_PARAM_NAME: /* TOK_PARAM_NAME  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1645 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_DEFPROC: /* TOK_COMMAND_DEFPROC  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1651 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_REPLAY: /* TOK_COMMAND_REPLAY  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1657 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_SET: /* TOK_COMMAND_SET  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1663 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_BYE: /* TOK_BYE  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1669 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_IF: /* TOK_IF  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1675 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_THEN: /* TOK_THEN  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1681 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_ELSE: /* TOK_ELSE  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1687 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_TOK_COMMAND_INCLUDE: /* TOK_COMMAND_INCLUDE  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1693 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_top_level_instrlist: /* top_level_instrlist  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1699 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_braced_instrlist: /* braced_instrlist  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1705 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_instrlist: /* instrlist  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1711 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_several_instructions: /* several_instructions  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1717 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_instr_with_semicolon: /* instr_with_semicolon  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1723 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invocation: /* proc_invocation  */
#line 135 "xxx_lang.yy"
            {}
#line 1729 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_proc_param_list: /* proc_param_list  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoclist);}
#line 1735 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invoc_param_list: /* proc_invoc_param_list  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoclist);}
#line 1741 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_proc_invoc_param: /* proc_invoc_param  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparaminvoc);}
#line 1747 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_include: /* include  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1753 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_optlist_with_semicolon: /* optlist_with_semicolon  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1759 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_instr: /* instr  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1765 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_optlist: /* optlist  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 1771 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_opt: /* opt  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1777 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_stylenamelist: /* stylenamelist  */
#line 131 "xxx_lang.yy"
            {delete ((*yyvaluep).stringlist);}
#line 1783 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_shapedeflist: /* shapedeflist  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).shape);}
#line 1789 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_shapeline: /* shapeline  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).shapeelement);}
#line 1795 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_color_string: /* color_string  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1801 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp1: /* defprochelp1  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 1807 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp2: /* defprochelp2  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 1813 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp3: /* defprochelp3  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 1819 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_defprochelp4: /* defprochelp4  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procdefhelper);}
#line 1825 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_scope_open_proc_body: /* scope_open_proc_body  */
#line 135 "xxx_lang.yy"
            {}
#line 1831 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_scope_close_proc_body: /* scope_close_proc_body  */
#line 135 "xxx_lang.yy"
            {}
#line 1837 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_arglist_tested: /* proc_def_arglist_tested  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 1843 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_arglist: /* proc_def_arglist  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 1849 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_param_list: /* proc_def_param_list  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdeflist);}
#line 1855 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_proc_def_param: /* proc_def_param  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procparamdef);}
#line 1861 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_procedure_body: /* procedure_body  */
#line 133 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).procedure);}
#line 1867 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_comp: /* comp  */
#line 135 "xxx_lang.yy"
            {}
#line 1873 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_condition: /* condition  */
#line 135 "xxx_lang.yy"
            {}
#line 1879 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_ifthen_condition: /* ifthen_condition  */
#line 136 "xxx_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
  #else
    chart.PopContext();
  #endif
}
#line 1891 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_else: /* else  */
#line 136 "xxx_lang.yy"
            {
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
  #else
    chart.PopContext();
  #endif
}
#line 1903 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_ifthen: /* ifthen  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction);}
#line 1909 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_colon_string: /* colon_string  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1915 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_full_attrlist_with_label: /* full_attrlist_with_label  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attributelist);}
#line 1921 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_full_attrlist: /* full_attrlist  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attributelist);}
#line 1927 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_attrlist: /* attrlist  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attributelist);}
#line 1933 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_attr: /* attr  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).attribute);}
#line 1939 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_reserved_word_string: /* reserved_word_string  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1945 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_symbol_string: /* symbol_string  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1951 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string_single: /* entity_string_single  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1957 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_alpha_string_single: /* alpha_string_single  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1963 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_string_single: /* string_single  */
#line 129 "xxx_lang.yy"
            {free(((*yyvaluep).str));}
#line 1969 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_tok_param_name_as_multi: /* tok_param_name_as_multi  */
#line 134 "xxx_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1975 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_multi_string_continuation: /* multi_string_continuation  */
#line 134 "xxx_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1981 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string_single_or_param: /* entity_string_single_or_param  */
#line 134 "xxx_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1987 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_entity_string: /* entity_string  */
#line 134 "xxx_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1993 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_alpha_string: /* alpha_string  */
#line 134 "xxx_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 1999 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_string: /* string  */
#line 134 "xxx_lang.yy"
            {free(((*yyvaluep).multi_str).str);}
#line 2005 "xxx_csh_lang.cc"
        break;

    case YYSYMBOL_scope_close: /* scope_close  */
#line 132 "xxx_lang.yy"
            {if (!C_S_H) delete ((*yyvaluep).instruction_list);}
#line 2011 "xxx_csh_lang.cc"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (YYMSC_RESULT_TYPE &RESULT, void *yyscanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */


/* User initialization code.  */
#line 8 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    yylloc.first_pos = 0;
    yylloc.last_pos = 0;
  #endif
}

#line 2113 "xxx_csh_lang.cc"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, yyscanner);
    }

  if (yychar <= TOK_EOF)
    {
      yychar = TOK_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* chart_with_bye: chart eof  */
#line 199 "xxx_lang.yy"
{
	YYACCEPT;
}
#line 2328 "xxx_csh_lang.cc"
    break;

  case 3: /* chart_with_bye: chart error eof  */
#line 203 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Syntax error.");
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
	YYACCEPT;
}
#line 2341 "xxx_csh_lang.cc"
    break;

  case 5: /* eof: TOK_BYE  */
#line 214 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
	csh.AddCSH_AllCommentBeyond((yylsp[0]));
  #else
  #endif
    free((yyvsp[0].str));
}
#line 2354 "xxx_csh_lang.cc"
    break;

  case 6: /* eof: TOK_BYE TOK_SEMICOLON  */
#line 223 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
	csh.AddCSH_AllCommentBeyond((yylsp[0]));
  #else
  #endif
    free((yyvsp[-1].str));
}
#line 2368 "xxx_csh_lang.cc"
    break;

  case 7: /* chart: %empty  */
#line 234 "xxx_lang.yy"
{
  //Add here what to do for an empty chart
  #ifdef C_S_H_IS_COMPILED
    csh.AddLineBeginToHints();
    csh.hintStatus = HINT_READY;
    csh.hintSource = EHintSourceType::LINE_START;
    csh.hintsForcedOnly = true;
  #else
    //no action for empty file
  #endif
}
#line 2384 "xxx_csh_lang.cc"
    break;

  case 8: /* chart: top_level_instrlist  */
#line 246 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckLineStartHintBefore((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    //TODO: Add the instructionlist to the chart
    (void)(yyvsp[0].instruction_list);
  #endif
}
#line 2400 "xxx_csh_lang.cc"
    break;

  case 10: /* top_level_instrlist: instrlist TOK_CCBRACKET  */
#line 261 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Closing brace missing its opening pair.");
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Unexpected '}'.");
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 2414 "xxx_csh_lang.cc"
    break;

  case 11: /* top_level_instrlist: instrlist TOK_CCBRACKET top_level_instrlist  */
#line 271 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "Closing brace missing its opening pair.");
  #else
    //Merge $3 into $1
    ((yyvsp[-2].instruction_list))->splice(((yyvsp[-2].instruction_list))->end(), *((yyvsp[0].instruction_list)));
    delete ((yyvsp[0].instruction_list));
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Unexpected '}'.");
  #endif
    (yyvsp[-1].input_text_ptr); //suppress
}
#line 2431 "xxx_csh_lang.cc"
    break;

  case 12: /* braced_instrlist: scope_open instrlist scope_close  */
#line 286 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    if ((yyvsp[0].instruction_list)) ((yyvsp[-1].instruction_list))->Append((yyvsp[0].instruction_list)); //Append any potential CommandNumbering
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
  #endif
}
#line 2444 "xxx_csh_lang.cc"
    break;

  case 13: /* braced_instrlist: scope_open scope_close  */
#line 295 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    (yyval.instruction_list) = new XxxInstrList;
    //scope_close should not return here with a CommandNumbering
    //but just in case
    if ((yyvsp[0].instruction_list))
        delete((yyvsp[0].instruction_list));
  #endif
}
#line 2460 "xxx_csh_lang.cc"
    break;

  case 14: /* braced_instrlist: scope_open instrlist error scope_close  */
#line 307 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    if ((yyvsp[0].instruction_list)) ((yyvsp[-2].instruction_list))->Append((yyvsp[0].instruction_list));
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Syntax error.");
  #endif
    yyerrok;
}
#line 2476 "xxx_csh_lang.cc"
    break;

  case 15: /* braced_instrlist: scope_open instrlist error TOK_EOF  */
#line 319 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    (yyval.instruction_list)->Append(chart.PopContext()); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_START((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
}
#line 2493 "xxx_csh_lang.cc"
    break;

  case 16: /* braced_instrlist: scope_open instrlist TOK_EOF  */
#line 332 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    (yyval.instruction_list)->Append(chart.PopContext()); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
  #endif
}
#line 2510 "xxx_csh_lang.cc"
    break;

  case 17: /* braced_instrlist: scope_open TOK_EOF  */
#line 345 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    (yyval.instruction_list) = chart.PopContext().release(); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
}
#line 2525 "xxx_csh_lang.cc"
    break;

  case 18: /* braced_instrlist: scope_open instrlist TOK_BYE  */
#line 356 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    (yyval.instruction_list)->Append(chart.PopContext()); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
  #endif
  free((yyvsp[0].str));
}
#line 2543 "xxx_csh_lang.cc"
    break;

  case 19: /* braced_instrlist: scope_open TOK_BYE  */
#line 370 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
  #else
    (yyval.instruction_list) = chart.PopContext().release(); //will be empty list of nodes anyway.
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
  free((yyvsp[0].str));
}
#line 2559 "xxx_csh_lang.cc"
    break;

  case 20: /* instrlist: instr_with_semicolon  */
#line 384 "xxx_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction))
        (yyval.instruction_list) = (new XxxInstrList)->Append((yyvsp[0].instruction)); /* New list */
    else
        (yyval.instruction_list) = new XxxInstrList;
  #endif
}
#line 2572 "xxx_csh_lang.cc"
    break;

  case 21: /* instrlist: instrlist instr_with_semicolon  */
#line 393 "xxx_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction)) ((yyvsp[-1].instruction_list))->Append((yyvsp[0].instruction));     /* Add to existing list */
    (yyval.instruction_list) = ((yyvsp[-1].instruction_list));
  #endif
}
#line 2583 "xxx_csh_lang.cc"
    break;

  case 23: /* instrlist: instrlist several_instructions  */
#line 401 "xxx_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    //TODO: Add a nested instructionlist to another instructionslist
    if ((yyvsp[0].instruction_list)) ((yyvsp[-1].instruction_list))->Append((yyvsp[0].instruction_list));     /* Add to existing list */
    (yyval.instruction_list) = ((yyvsp[-1].instruction_list));
  #endif
}
#line 2595 "xxx_csh_lang.cc"
    break;

  case 25: /* several_instructions: braced_instrlist  */
#line 411 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    //Standalone braced instruction lists are instructions - for the purpose of indentation
    csh.AddInstruction((yylsp[0]));
  #endif
  (yyval.instruction_list) = (yyvsp[0].instruction_list);
}
#line 2607 "xxx_csh_lang.cc"
    break;

  case 26: /* instr_with_semicolon: instr  */
#line 421 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[0].instruction);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 2622 "xxx_csh_lang.cc"
    break;

  case 27: /* instr_with_semicolon: instr TOK_SEMICOLON  */
#line 432 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[-1].instruction);
  #endif
}
#line 2640 "xxx_csh_lang.cc"
    break;

  case 28: /* instr_with_semicolon: TOK_SEMICOLON  */
#line 446 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction)=nullptr;
  #endif
}
#line 2657 "xxx_csh_lang.cc"
    break;

  case 29: /* instr_with_semicolon: instr error TOK_SEMICOLON  */
#line 459 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    //if ($1) ($1)->SetLineEnd(CHART_POS(@$));
    (yyval.instruction)=(yyvsp[-2].instruction);
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
  #endif
}
#line 2678 "xxx_csh_lang.cc"
    break;

  case 30: /* instr_with_semicolon: proc_invocation TOK_SEMICOLON  */
#line 476 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-1].cprocedure)) {
        auto ctx = ((yyvsp[-1].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[-1])), &chart);
        if (!ctx.first) {
            XxxPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-1].cprocedure))->text.c_str(), ((yyvsp[-1].cprocedure))->text.length(), &((yylsp[-1])), ((yyvsp[-1].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-1].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 2699 "xxx_csh_lang.cc"
    break;

  case 31: /* instr_with_semicolon: proc_invocation error TOK_SEMICOLON  */
#line 493 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-2].cprocedure)) {
        auto ctx = ((yyvsp[-2].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[-2])), &chart);
        if (!ctx.first) {
            XxxPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-2].cprocedure))->text.c_str(), ((yyvsp[-2].cprocedure))->text.length(), &((yylsp[-2])), ((yyvsp[-2].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-2].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 2727 "xxx_csh_lang.cc"
    break;

  case 32: /* instr_with_semicolon: proc_invocation  */
#line 517 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[0].cprocedure)) {
        auto ctx = ((yyvsp[0].cprocedure))->MatchParameters(nullptr, CHART_POS_AFTER((yylsp[0])), &chart);
        if (!ctx.first) {
            XxxPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[0].cprocedure))->text.c_str(), ((yyvsp[0].cprocedure))->text.length(), &((yylsp[0])), ((yyvsp[0].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[0].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 2750 "xxx_csh_lang.cc"
    break;

  case 33: /* instr_with_semicolon: proc_invocation proc_param_list TOK_SEMICOLON  */
#line 536 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-2].cprocedure) && (yyvsp[-1].procparaminvoclist)) {
        auto ctx = ((yyvsp[-2].cprocedure))->MatchParameters((yyvsp[-1].procparaminvoclist), CHART_POS((yylsp[-1])).end, &chart);
        if (!ctx.first) {
            XxxPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-2].cprocedure))->text.c_str(), ((yyvsp[-2].cprocedure))->text.length(), &((yylsp[-2])), ((yyvsp[-2].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-2].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else {
        delete (yyvsp[-1].procparaminvoclist);
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 2773 "xxx_csh_lang.cc"
    break;

  case 34: /* instr_with_semicolon: proc_invocation proc_param_list error TOK_SEMICOLON  */
#line 555 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-3].cprocedure) && (yyvsp[-2].procparaminvoclist)) {
        auto ctx = ((yyvsp[-3].cprocedure))->MatchParameters((yyvsp[-2].procparaminvoclist), CHART_POS((yylsp[-2])).end, &chart);
        if (!ctx.first) {
            XxxPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-3].cprocedure))->text.c_str(), ((yyvsp[-3].cprocedure))->text.length(), &((yylsp[-3])), ((yyvsp[-3].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-3].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else {
        delete (yyvsp[-2].procparaminvoclist);
    }
    (yyval.instruction) = nullptr;
  #endif
}
#line 2803 "xxx_csh_lang.cc"
    break;

  case 35: /* instr_with_semicolon: proc_invocation proc_param_list  */
#line 581 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon TOK_SEMICOLON.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-1].cprocedure) && (yyvsp[0].procparaminvoclist)) {
        auto ctx = ((yyvsp[-1].cprocedure))->MatchParameters((yyvsp[0].procparaminvoclist), CHART_POS((yylsp[0])).end, &chart);
        if (!ctx.first) {
            XxxPushFlex(*YYGET_EXTRA(yyscanner), ((yyvsp[-1].cprocedure))->text.c_str(), ((yyvsp[-1].cprocedure))->text.length(), &((yylsp[-1])), ((yyvsp[-1].cprocedure))->file_pos, EInclusionReason::PROCEDURE);
            YYGET_EXTRA(yyscanner)->last_procedure = (yyvsp[-1].cprocedure);
            YYGET_EXTRA(yyscanner)->last_procedure_params = std::move(ctx.second);
            YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::PROC_REPLAY;
        }
    } else
        delete (yyvsp[0].procparaminvoclist);
    (yyval.instruction) = nullptr;
  #endif
}
#line 2827 "xxx_csh_lang.cc"
    break;

  case 36: /* instr_with_semicolon: include TOK_SEMICOLON  */
#line 601 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    if ((yyvsp[-1].str)) {
        auto text = chart.Include((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
        if (text.first && text.first->length() && text.second.IsValid())
            XxxPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-1])), text.second, EInclusionReason::INCLUDE);
    }
    (yyval.instruction) = nullptr;
  #endif
    if ((yyvsp[-1].str)) free((yyvsp[-1].str));
}
#line 2845 "xxx_csh_lang.cc"
    break;

  case 37: /* instr_with_semicolon: include  */
#line 615 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon.");
  #else
    if ((yyvsp[0].str)) {
        auto text = chart.Include((yyvsp[0].str), CHART_POS_START((yylsp[0])));
        if (text.first && text.first->length() && text.second.IsValid())
            XxxPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[0])), text.second, EInclusionReason::INCLUDE);
    }
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[0])), CHART_POS_AFTER((yylsp[0])), "Here is the beginning of the command as I understood it.");
    (yyval.instruction) = nullptr;
  #endif
    if ((yyvsp[0].str)) free((yyvsp[0].str));
}
#line 2865 "xxx_csh_lang.cc"
    break;

  case 38: /* instr_with_semicolon: include error TOK_SEMICOLON  */
#line 631 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the command as I understood it.");
    if ((yyvsp[-2].str)) {
        auto text = chart.Include((yyvsp[-2].str), CHART_POS_START((yylsp[-2])));
        if (text.first && text.first->length() && text.second.IsValid())
            XxxPushFlex(*YYGET_EXTRA(yyscanner), text.first->c_str(), text.first->length(), &((yylsp[-2])), text.second, EInclusionReason::INCLUDE);
    }
    (yyval.instruction) = nullptr;
  #endif
    if ((yyvsp[-2].str)) free((yyvsp[-2].str));
}
#line 2890 "xxx_csh_lang.cc"
    break;

  case 39: /* proc_invocation: TOK_COMMAND_REPLAY  */
#line 653 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing procedure name.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing procedure name.");
    (yyval.cprocedure) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 2905 "xxx_csh_lang.cc"
    break;

  case 40: /* proc_invocation: TOK_COMMAND_REPLAY alpha_string  */
#line 664 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_PROCNAME);
  #else
    (yyval.cprocedure) = nullptr;
    if (!(yyvsp[0].multi_str).had_error) {
        auto proc = chart.GetProcedure((yyvsp[0].multi_str).str);
        if (proc==nullptr)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Undefined procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::PROBLEM)
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Ill-formed procedure. Ignoring procedure call.");
        else if (proc->status==EDefProcResult::OK) {
            //Only return the procedure if we are not inside a procedure definition
            if (!chart.SkipContent())
                (yyval.cprocedure) = proc;
            //else just move on parsing - we do not reparse procedure replays during
            //the definition of an outer procedure.
        }
        //else return null, emit no error for EMPTY
    }
  #endif
    free((yyvsp[-1].str));
    free((yyvsp[0].multi_str).str);
}
#line 2935 "xxx_csh_lang.cc"
    break;

  case 41: /* proc_param_list: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 691 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparaminvoclist) = new ProcParamInvocationList;
  #endif
}
#line 2948 "xxx_csh_lang.cc"
    break;

  case 42: /* proc_param_list: TOK_OPARENTHESIS error TOK_CPARENTHESIS  */
#line 700 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter syntax.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter syntax. Ignoring procedure call.");
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 2963 "xxx_csh_lang.cc"
    break;

  case 43: /* proc_param_list: TOK_OPARENTHESIS  */
#line 711 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter list closed by a parenthesis ')'. Ignoring procedure call.");
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 2977 "xxx_csh_lang.cc"
    break;

  case 44: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list error TOK_CPARENTHESIS  */
#line 721 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter syntax.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter syntax. Ignoring procedure call.");
    delete (yyvsp[-2].procparaminvoclist);
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 2993 "xxx_csh_lang.cc"
    break;

  case 45: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list  */
#line 733 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing closing parenthesis ')'. Ignoring procedure call.");
    delete (yyvsp[0].procparaminvoclist);
    (yyval.procparaminvoclist) = nullptr;
  #endif
}
#line 3008 "xxx_csh_lang.cc"
    break;

  case 46: /* proc_param_list: TOK_OPARENTHESIS proc_invoc_param_list TOK_CPARENTHESIS  */
#line 744 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparaminvoclist) = (yyvsp[-1].procparaminvoclist);
  #endif
}
#line 3021 "xxx_csh_lang.cc"
    break;

  case 47: /* proc_invoc_param_list: proc_invoc_param  */
#line 754 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparaminvoc)) {
        (yyval.procparaminvoclist) = new ProcParamInvocationList;
        ((yyval.procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
    } else
        (yyval.procparaminvoclist)= nullptr;
  #endif
}
#line 3036 "xxx_csh_lang.cc"
    break;

  case 48: /* proc_invoc_param_list: TOK_COMMA  */
#line 765 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
  #else
    (yyval.procparaminvoclist) = new ProcParamInvocationList;
    ((yyval.procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_START((yylsp[0]))));
  #endif
}
#line 3049 "xxx_csh_lang.cc"
    break;

  case 49: /* proc_invoc_param_list: TOK_COMMA proc_invoc_param  */
#line 774 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[0].procparaminvoc)) {
        (yyval.procparaminvoclist) = new ProcParamInvocationList;
        ((yyval.procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_START((yylsp[-1]))));
        ((yyval.procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
    } else
        (yyval.procparaminvoclist)= nullptr;
  #endif
}
#line 3066 "xxx_csh_lang.cc"
    break;

  case 50: /* proc_invoc_param_list: proc_invoc_param_list TOK_COMMA  */
#line 787 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
  #else
    if ((yyvsp[-1].procparaminvoclist))
        ((yyvsp[-1].procparaminvoclist))->Append(std::make_unique<ProcParamInvocation>(CHART_POS_AFTER((yylsp[0]))));
    (yyval.procparaminvoclist) = (yyvsp[-1].procparaminvoclist);
  #endif
}
#line 3080 "xxx_csh_lang.cc"
    break;

  case 51: /* proc_invoc_param_list: proc_invoc_param_list TOK_COMMA proc_invoc_param  */
#line 797 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[-2].procparaminvoclist) && (yyvsp[0].procparaminvoc)) {
        ((yyvsp[-2].procparaminvoclist))->Append((yyvsp[0].procparaminvoc));
        (yyval.procparaminvoclist) = (yyvsp[-2].procparaminvoclist);
    } else {
        delete (yyvsp[-2].procparaminvoclist);
        delete (yyvsp[0].procparaminvoc);
        (yyval.procparaminvoclist)= nullptr;
    }
  #endif
}
#line 3099 "xxx_csh_lang.cc"
    break;

  case 52: /* proc_invoc_param: string  */
#line 813 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error) {
        //If this is a quoted string, color as a label, else as an attribute value
        if ((yylsp[0]).first_pos>0 && YYGET_EXTRA(yyscanner)->buff.buf[(yylsp[0]).first_pos-1]=='\"')
            csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, {});
        else
            csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    }
  #else
    if ((yyvsp[0].multi_str).had_error)
        (yyval.procparaminvoc) = nullptr;
    else
        (yyval.procparaminvoc) = new ProcParamInvocation((yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 3121 "xxx_csh_lang.cc"
    break;

  case 53: /* proc_invoc_param: TOK_NUMBER  */
#line 831 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    (yyval.procparaminvoc) = new ProcParamInvocation((yyvsp[0].str), CHART_POS_START((yylsp[0])));
  #endif
    free((yyvsp[0].str));
}
#line 3134 "xxx_csh_lang.cc"
    break;

  case 54: /* include: TOK_COMMAND_INCLUDE  */
#line 841 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a file name to include. You must use quotation marks ('\"').");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints();
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a file name to include. You must use quotation marks ('\"').");
  #endif
    (yyval.str) = nullptr;
    free((yyvsp[0].str));
}
#line 3151 "xxx_csh_lang.cc"
    break;

  case 55: /* include: TOK_COMMAND_INCLUDE TOK_QSTRING  */
#line 854 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_INCLUDEFILE);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints();
    else if(csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_VALUE))
        csh.AddIncludeFilesToHints((yyvsp[0].str), (yylsp[0]));
  #endif
    (yyval.str) = (yyvsp[0].str);
    free((yyvsp[-1].str));
}
#line 3168 "xxx_csh_lang.cc"
    break;

  case 56: /* optlist_with_semicolon: optlist  */
#line 869 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing semicolon (';').");
  #else
    (yyval.instruction_list)=(yyvsp[0].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a semicolon ';'.");
  #endif
}
#line 3182 "xxx_csh_lang.cc"
    break;

  case 57: /* optlist_with_semicolon: optlist TOK_SEMICOLON  */
#line 879 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list)=(yyvsp[-1].instruction_list);
  #endif
}
#line 3199 "xxx_csh_lang.cc"
    break;

  case 58: /* optlist_with_semicolon: optlist error TOK_SEMICOLON  */
#line 892 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstruction((yyloc));
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
       csh.AddLineBeginToHints();
       csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list)=(yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[-2])), "Missing a semicolon ';' after option(s).");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-2])), "Here is the beginning of the option list as I understood it.");
  #endif
}
#line 3219 "xxx_csh_lang.cc"
    break;

  case 59: /* instr: TOK_COMMAND_DEFSHAPE shapedef  */
#line 911 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error((yylsp[-1]), "Cannot define shapes as part of a procedure.");
    else
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define shapes as part of a procedure.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 3241 "xxx_csh_lang.cc"
    break;

  case 60: /* instr: TOK_COMMAND_DEFSHAPE  */
#line 929 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error((yylsp[0]), "Cannot define shapes as part of a procedure.");
    else
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing shape name and definition.");
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Cannot define shapes as part of a procedure.");
    else
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing shape name and definition.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 3266 "xxx_csh_lang.cc"
    break;

  case 61: /* instr: TOK_COMMAND_DEFCOLOR colordeflist  */
#line 950 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 3286 "xxx_csh_lang.cc"
    break;

  case 62: /* instr: TOK_COMMAND_DEFCOLOR  */
#line 966 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 3308 "xxx_csh_lang.cc"
    break;

  case 63: /* instr: TOK_COMMAND_DEFSTYLE styledeflist  */
#line 984 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 3328 "xxx_csh_lang.cc"
    break;

  case 64: /* instr: TOK_COMMAND_DEFSTYLE  */
#line 1000 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing style name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 3350 "xxx_csh_lang.cc"
    break;

  case 65: /* instr: TOK_COMMAND_DEFDESIGN designdef  */
#line 1018 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error((yylsp[-1]), "Cannot define designs inside a procedure.");
    else
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Cannot define designs inside a procedure.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].str));
}
#line 3372 "xxx_csh_lang.cc"
    break;

  case 66: /* instr: TOK_COMMAND_DEFDESIGN  */
#line 1036 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent())
        csh.AddCSH_Error((yylsp[0]), "Cannot define designs inside a procedure.");
    else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing design name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (chart.SkipContent())
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Cannot define designs inside a procedure.");
    else
        chart.Error.Error(CHART_POS((yyloc)).end, "Missing a design name to (re-)define.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 3398 "xxx_csh_lang.cc"
    break;

  case 67: /* instr: defproc  */
#line 1058 "xxx_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    (yyval.instruction) = nullptr;
  #endif
}
#line 3408 "xxx_csh_lang.cc"
    break;

  case 68: /* instr: set  */
#line 1064 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.instruction) = nullptr;
  #endif
}
#line 3419 "xxx_csh_lang.cc"
    break;

  case 69: /* instr: ifthen  */
#line 1071 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.IfThenElses.push_back((yyloc));
  #endif
    (yyval.instruction) = (yyvsp[0].instruction);
}
#line 3430 "xxx_csh_lang.cc"
    break;

  case 70: /* instr: TOK_XXX  */
#line 1078 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
  #else
    //TODO: Create a new instruction
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent())
        (yyval.instruction) = nullptr;
    else
        (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[0].str));
}
#line 3448 "xxx_csh_lang.cc"
    break;

  case 71: /* instr: TOK_XXX full_attrlist_with_label  */
#line 1092 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
  #else
    //TODO: Create a new instruction
    //Take care not to make changes to the chart if we are storing a procedure
    if (!chart.SkipContent())
        (yyval.instruction) = nullptr;
    else
        (yyval.instruction) = nullptr;
    delete (yyvsp[0].attributelist);
  #endif
    free((yyvsp[-1].str));
}
#line 3467 "xxx_csh_lang.cc"
    break;

  case 72: /* optlist: opt  */
#line 1109 "xxx_lang.yy"
{
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction))
        (yyval.instruction_list) = (new XxxInstrList)->Append((yyvsp[0].instruction)); /* New list */
    else
        (yyval.instruction_list) = new XxxInstrList;
  #endif
}
#line 3480 "xxx_csh_lang.cc"
    break;

  case 73: /* optlist: optlist TOK_COMMA opt  */
#line 1118 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
  #ifndef C_S_H_IS_COMPILED
    if ((yyvsp[0].instruction)) ((yyvsp[-2].instruction_list))->Append((yyvsp[0].instruction));     /* Add to existing list */
    (yyval.instruction_list) = ((yyvsp[-2].instruction_list));
  #endif
  #endif
}
#line 3499 "xxx_csh_lang.cc"
    break;

  case 74: /* optlist: optlist TOK_COMMA  */
#line 1133 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.instruction_list) = (yyvsp[-1].instruction_list);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an option here.");
  #endif
}
#line 3516 "xxx_csh_lang.cc"
    break;

  case 75: /* optlist: optlist TOK_COMMA error  */
#line 1146 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_Error((yylsp[0]), "An option expected here.");
  #else
    (yyval.instruction_list) = (yyvsp[-2].instruction_list);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "I am not sure what is coming here.");
  #endif
}
#line 3534 "xxx_csh_lang.cc"
    break;

  case 76: /* opt: entity_string TOK_EQUAL TOK_NUMBER  */
#line 1162 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        XxxChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-2].multi_str).had_error || (chart.SkipContent() && (yyvsp[-2].multi_str).had_param))
        (yyval.instruction) = nullptr;
    else
        (yyval.instruction) = chart.AddAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])))).release();
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 3561 "xxx_csh_lang.cc"
    break;

  case 77: /* opt: entity_string TOK_EQUAL string  */
#line 1185 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error && !(yyvsp[0].multi_str).had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].multi_str).str, (yyvsp[-2].multi_str).str);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        XxxChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-2].multi_str).had_error || (yyvsp[0].multi_str).had_error || (chart.SkipContent() && ((yyvsp[-2].multi_str).had_param || (yyvsp[0].multi_str).had_param)))
        (yyval.instruction) = nullptr;
    else
        (yyval.instruction) = chart.AddAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])))).release();
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 3589 "xxx_csh_lang.cc"
    break;

  case 78: /* opt: entity_string TOK_EQUAL  */
#line 1209 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing option value.");
    if (!(yyvsp[-1].multi_str).had_error && csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str)) {
        XxxChart::AttributeValues((yyvsp[-1].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value.");
    (yyval.instruction) = nullptr;
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 3613 "xxx_csh_lang.cc"
    break;

  case 80: /* styledeflist: styledeflist TOK_COMMA styledef  */
#line 1231 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 3627 "xxx_csh_lang.cc"
    break;

  case 81: /* styledeflist: styledeflist TOK_COMMA  */
#line 1241 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing style definition here.", "Try just removing the comma.");
#endif
}
#line 3643 "xxx_csh_lang.cc"
    break;

  case 82: /* styledef: stylenamelist full_attrlist  */
#line 1254 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!csh.Contexts.back().SkipContent())
        for (auto &str : *((yyvsp[-1].stringlist)))
            if (csh.ForbiddenStyles.find(str) == csh.ForbiddenStyles.end())
                csh.Contexts.back().StyleNames.insert(str);
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        XxxStyle().AttributeNames(csh);
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        XxxStyle().AttributeValues(csh.hintAttrName, csh);
  #else
    if (chart.SkipContent())
    	chart.AddAttributeListToStyleList((yyvsp[0].attributelist), (yyvsp[-1].stringlist)); //deletes $2, as well
  #endif
    delete((yyvsp[-1].stringlist));
}
#line 3664 "xxx_csh_lang.cc"
    break;

  case 83: /* styledef: stylenamelist  */
#line 1271 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH_ErrorAfter((yyloc), "Missing attribute definitons in square brackets ('[' and ']').");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing attribute definitons in square brackets ('[' and ']').");
  #endif
    delete((yyvsp[0].stringlist));
}
#line 3677 "xxx_csh_lang.cc"
    break;

  case 84: /* stylenamelist: string  */
#line 1281 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
	    csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    (yyval.stringlist) = new std::list<string>;
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyval.stringlist)->push_back((yyvsp[0].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 3695 "xxx_csh_lang.cc"
    break;

  case 85: /* stylenamelist: stylenamelist TOK_COMMA  */
#line 1295 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
	csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a style name to (re-)define.");
    if (csh.CheckHintAfter((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
	csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
  #endif
    (yyval.stringlist) = (yyvsp[-1].stringlist);
}
#line 3713 "xxx_csh_lang.cc"
    break;

  case 86: /* stylenamelist: stylenamelist TOK_COMMA string  */
#line 1309 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.AddCSH((yylsp[0]), COLOR_STYLENAME);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
		csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    if (!(yyvsp[0].multi_str).had_error && (yyvsp[0].multi_str).str)
        (yyvsp[-2].stringlist)->push_back((yyvsp[0].multi_str).str);
    (yyval.stringlist) = (yyvsp[-2].stringlist);
    free((yyvsp[0].multi_str).str);
}
#line 3732 "xxx_csh_lang.cc"
    break;

  case 87: /* shapedef: entity_string  */
#line 1325 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.AddCSH_ErrorAfter((yyloc), ("Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '"+string((yyvsp[0].multi_str).str ? (yyvsp[0].multi_str).str : "") +"'.").c_str());
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Here should come a shape definition beginning with '{'. Ignoring this malformed shape definition for '"+string((yyvsp[0].multi_str).str ? (yyvsp[0].multi_str).str : "") +"'.");
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 3747 "xxx_csh_lang.cc"
    break;

  case 88: /* shapedef: entity_string TOK_OCBRACKET  */
#line 1336 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yylsp[0]));
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.AddCSH_ErrorAfter((yyloc), ("Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '"+string((yyvsp[-1].multi_str).str ? (yyvsp[-1].multi_str).str: "") +"'.").c_str());
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Here should come a shape definition beginning with 'T', 'H', 'M', 'L', 'C', 'S', 'P' or 'E'. Ignoring this malformed shape definition for '"+string((yyvsp[-1].multi_str).str ? (yyvsp[-1].multi_str).str: "") +"'.");
  #endif
    free((yyvsp[-1].multi_str).str);
    (yyvsp[0].input_text_ptr); //suppress
}
#line 3765 "xxx_csh_lang.cc"
    break;

  case 89: /* shapedef: entity_string TOK_OCBRACKET shapedeflist  */
#line 1350 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddOpenBracePair((yylsp[-1])+(yylsp[0]));
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-1]), COLOR_BRACE);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a closing brace ('}').");
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddShapeName((yyvsp[-2].multi_str).str);
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-1])), CHART_POS_AFTER((yylsp[0])), "Here is the corresponding '{'.");
    if (!(yyvsp[-2].multi_str).had_error && (yyvsp[-2].multi_str).str && (yyvsp[0].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-2].multi_str).str), CHART_POS_START((yylsp[-2])), chart.file_url, chart.file_info, std::move(*(yyvsp[0].shape)), chart.Error);
	delete (yyvsp[0].shape);
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    (yyvsp[-1].input_text_ptr); //suppress
}
#line 3791 "xxx_csh_lang.cc"
    break;

  case 90: /* shapedef: entity_string TOK_OCBRACKET shapedeflist TOK_CCBRACKET  */
#line 1372 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yylsp[-2])+(yylsp[0]));
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH((yylsp[-3]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-2]), COLOR_BRACE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
	csh.AddShapeName((yyvsp[-3].multi_str).str);
  #else
    if (!(yyvsp[-3].multi_str).had_error && (yyvsp[-3].multi_str).str && (yyvsp[-1].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-3].multi_str).str), CHART_POS_START((yylsp[-3])), chart.file_url, chart.file_info, std::move(*(yyvsp[-1].shape)), chart.Error);
	delete (yyvsp[-1].shape);
    }
  #endif
    free((yyvsp[-3].multi_str).str);
    (yyvsp[-2].input_text_ptr); //suppress
    (yyvsp[0].input_text_ptr); //suppress
}
#line 3815 "xxx_csh_lang.cc"
    break;

  case 91: /* shapedef: entity_string TOK_OCBRACKET shapedeflist error TOK_CCBRACKET  */
#line 1392 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yylsp[-3])+(yylsp[0]));
    if (!(yyvsp[-4].multi_str).had_error)
        csh.AddCSH((yylsp[-4]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[-3]), COLOR_BRACE);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
	csh.AddShapeName((yyvsp[-4].multi_str).str);
    csh.AddCSH_Error((yylsp[-1]), "Only numbers can come after shape commands.");
  #else
    if (!(yyvsp[-4].multi_str).had_error && (yyvsp[-4].multi_str).str && (yyvsp[-2].shape)) {
        if (!chart.SkipContent())
            chart.Shapes.Add(std::string((yyvsp[-4].multi_str).str), CHART_POS_START((yylsp[-4])), chart.file_url, chart.file_info, std::move(*(yyvsp[-2].shape)), chart.Error);
	delete (yyvsp[-2].shape);
    }
  #endif
    free((yyvsp[-4].multi_str).str);
    (yyvsp[-3].input_text_ptr); //suppress
    (yyvsp[0].input_text_ptr); //suppress
}
#line 3840 "xxx_csh_lang.cc"
    break;

  case 92: /* shapedeflist: shapeline TOK_SEMICOLON  */
#line 1414 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
    (yyval.shape) = new Shape;
	if ((yyvsp[-1].shapeelement)) {
		((yyval.shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
  #endif
}
#line 3856 "xxx_csh_lang.cc"
    break;

  case 93: /* shapedeflist: error shapeline TOK_SEMICOLON  */
#line 1426 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-2]), "I do not understand this.");
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
#else
    (yyval.shape) = new Shape;
	if ((yyvsp[-1].shapeelement)) {
		((yyval.shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
  #endif
}
#line 3873 "xxx_csh_lang.cc"
    break;

  case 94: /* shapedeflist: shapedeflist shapeline TOK_SEMICOLON  */
#line 1439 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_SEMICOLON);
  #else
	if ((yyvsp[-1].shapeelement)) {
		((yyvsp[-2].shape))->Add(std::move(*((yyvsp[-1].shapeelement))));
		delete (yyvsp[-1].shapeelement);
	}
    (yyval.shape) = (yyvsp[-2].shape);
  #endif
}
#line 3889 "xxx_csh_lang.cc"
    break;

  case 95: /* shapedeflist: shapedeflist error  */
#line 1451 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Only numbers can come after shape commands.");
  #else
    (yyval.shape) = (yyvsp[-1].shape);
  #endif
}
#line 3901 "xxx_csh_lang.cc"
    break;

  case 96: /* shapeline: TOK_SHAPE_COMMAND  */
#line 1460 "xxx_lang.yy"
{
    const int num_args = 0;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[0].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
	if (should_args != num_args)
		csh.AddCSH_ErrorAfter((yyloc), ShapeElement::ErrorMsg((yyvsp[0].shapecommand), num_args));
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args != num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[0].shapecommand), num_args).append(" Ignoring line."));
	else
	    (yyval.shapeelement) = new ShapeElement((yyvsp[0].shapecommand));
  #endif
}
#line 3921 "xxx_csh_lang.cc"
    break;

  case 97: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER  */
#line 1476 "xxx_lang.yy"
{
    const int num_args = 1;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-1].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-1].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-1].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	} else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG && (((yyvsp[0].str))[0]<'0' || ((yyvsp[0].str))[0]>'2' || ((yyvsp[0].str))[1]!=0))
		csh.AddCSH_Error((yylsp[0]), "S (section) commands require an integer between 0 and 2.");
  #else
	(yyval.shapeelement) = nullptr;
	const double a = atof((yyvsp[0].str));
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yylsp[0])).end, ShapeElement::ErrorMsg((yyvsp[-1].shapecommand), num_args).append(" Ignoring line."));
	else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG && (a!=0 && a!=1 && a!=2))
		chart.Error.Error(CHART_POS_START((yylsp[0])), "S (section) commands require an integer between 0 and 2. Ignoring line.");
	else if ((yyvsp[-1].shapecommand)>=ShapeElement::SECTION_BG)
	    (yyval.shapeelement) = new ShapeElement(ShapeElement::Type((yyvsp[-1].shapecommand) + unsigned(a)));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-1].shapecommand), a);
  #endif
  free((yyvsp[0].str));
}
#line 3953 "xxx_csh_lang.cc"
    break;

  case 98: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER  */
#line 1504 "xxx_lang.yy"
{
    const int num_args = 2;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-2].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-2].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-2].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-2].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-2].shapecommand), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 3981 "xxx_csh_lang.cc"
    break;

  case 99: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER alpha_string  */
#line 1528 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
    if ((yyvsp[-3].shapecommand)!=ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[0]), "You need to specify a number here.");
  #else
    (yyval.shapeelement) = nullptr;
    if ((yyvsp[-3].shapecommand)!=ShapeElement::PORT)
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a number here. Ignoring line.");
    else
        (yyval.shapeelement) = new ShapeElement(atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), (yyvsp[0].multi_str).str);
  #endif
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].multi_str).str);
}
#line 4002 "xxx_csh_lang.cc"
    break;

  case 100: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER alpha_string TOK_NUMBER  */
#line 1545 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
    if ((yyvsp[-4].shapecommand)!=ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[-1]), "You need to specify a number here.");
  #else
    (yyval.shapeelement) = nullptr;
    if ((yyvsp[-4].shapecommand)!=ShapeElement::PORT)
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting a number here. Ignoring line.");
    else
        (yyval.shapeelement) = new ShapeElement(atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), (yyvsp[-1].multi_str).str, atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].multi_str).str);
  free((yyvsp[0].str));
}
#line 4024 "xxx_csh_lang.cc"
    break;

  case 101: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 1563 "xxx_lang.yy"
{
    const int num_args = 3;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-3].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-3].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	} else if ((yyvsp[-3].shapecommand)==ShapeElement::PORT)
        csh.AddCSH_Error((yylsp[0]), "You need to specify a port name here starting with a letter.");
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-3].shapecommand), num_args).append(" Ignoring line."));
	else if ((yyvsp[-3].shapecommand)==ShapeElement::PORT)
		chart.Error.Error(CHART_POS_START((yylsp[0])), "Expecting a port name here. Ignoring line.");
    else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-3].shapecommand), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 4057 "xxx_csh_lang.cc"
    break;

  case 102: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 1592 "xxx_lang.yy"
{
    const int num_args = 4;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-4].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-4].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-4].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-4].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-4].shapecommand), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 4089 "xxx_csh_lang.cc"
    break;

  case 103: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 1620 "xxx_lang.yy"
{
    const int num_args = 5;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-5].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-5].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-5].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-4]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 4:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-5].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-5].shapecommand), atof((yyvsp[-4].str)), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-4].str));
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 4123 "xxx_csh_lang.cc"
    break;

  case 104: /* shapeline: TOK_SHAPE_COMMAND TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER TOK_NUMBER  */
#line 1650 "xxx_lang.yy"
{
    const int num_args = 6;
	const int should_args = ShapeElement::GetNumArgs((yyvsp[-6].shapecommand));
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-6]), COLOR_KEYWORD);
	if (should_args != num_args) {
		std::string msg = ShapeElement::ErrorMsg((yyvsp[-6].shapecommand), num_args);
	    switch (ShapeElement::GetNumArgs((yyvsp[-6].shapecommand))) {
		case 0:  csh.AddCSH_Error((yylsp[-5]) + (yylsp[0]), std::move(msg)); break;
		case 1:  csh.AddCSH_Error((yylsp[-4]) + (yylsp[0]), std::move(msg)); break;
		case 2:  csh.AddCSH_Error((yylsp[-3]) + (yylsp[0]), std::move(msg)); break;
		case 3:  csh.AddCSH_Error((yylsp[-2]) + (yylsp[0]), std::move(msg)); break;
		case 4:  csh.AddCSH_Error((yylsp[-1]) + (yylsp[0]), std::move(msg)); break;
		case 5:  csh.AddCSH_Error((yylsp[0]), std::move(msg)); break;
		default: csh.AddCSH_ErrorAfter((yyloc), std::move(msg)); break;
		}
	}
  #else
	(yyval.shapeelement) = nullptr;
	if (should_args > num_args)
		chart.Error.Error(CHART_POS((yyloc)).end, ShapeElement::ErrorMsg((yyvsp[-6].shapecommand), num_args).append(" Ignoring line."));
	else
		(yyval.shapeelement) = new ShapeElement((yyvsp[-6].shapecommand), atof((yyvsp[-5].str)), atof((yyvsp[-4].str)), atof((yyvsp[-3].str)), atof((yyvsp[-2].str)), atof((yyvsp[-1].str)), atof((yyvsp[0].str)));
  #endif
  free((yyvsp[-5].str));
  free((yyvsp[-4].str));
  free((yyvsp[-3].str));
  free((yyvsp[-2].str));
  free((yyvsp[-1].str));
  free((yyvsp[0].str));
}
#line 4159 "xxx_csh_lang.cc"
    break;

  case 106: /* colordeflist: colordeflist TOK_COMMA colordef  */
#line 1684 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
  #endif
}
#line 4173 "xxx_csh_lang.cc"
    break;

  case 107: /* colordeflist: colordeflist TOK_COMMA  */
#line 1694 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
  #endif
}
#line 4190 "xxx_csh_lang.cc"
    break;

  case 109: /* color_string: string  */
#line 1709 "xxx_lang.yy"
{
    if ((yyvsp[0].multi_str).had_error) {
        (yyval.str) = strdup("");
        free((yyvsp[0].multi_str).str);
    } else
        (yyval.str) = (yyvsp[0].multi_str).str;
}
#line 4202 "xxx_csh_lang.cc"
    break;

  case 110: /* colordef: alpha_string TOK_EQUAL color_string  */
#line 1718 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error) {
        csh.AddCSH((yylsp[-2]), COLOR_COLORNAME);
        ColorType color = csh.Contexts.back().Colors.GetColor((yyvsp[0].str));
        if (color.type!=ColorType::INVALID)
            csh.Contexts.back().Colors[(yyvsp[-2].multi_str).str] = color;
    }
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent() && !(yyvsp[-2].multi_str).had_error)
        chart.MyCurrentContext().colors.AddColor((yyvsp[-2].multi_str).str, (yyvsp[0].str), chart.Error, CHART_POS((yyloc)));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 4231 "xxx_csh_lang.cc"
    break;

  case 111: /* colordef: alpha_string TOK_EQUAL TOK_PLUS_PLUS color_string  */
#line 1743 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-3].multi_str).had_error) {
        csh.AddCSH((yylsp[-3]), COLOR_COLORNAME);
        ColorType color = csh.Contexts.back().Colors.GetColor("++"+string((yyvsp[0].str)));
        if (color.type!=ColorType::INVALID)
            csh.Contexts.back().Colors[(yyvsp[-3].multi_str).str] = color;
    }
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    csh.AddCSH((yylsp[-1]), COLOR_COLORDEF);
    csh.AddCSH((yylsp[0]), COLOR_COLORDEF);
    if (csh.CheckHintAt((yylsp[-3]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent() && !(yyvsp[-3].multi_str).had_error)
        chart.MyCurrentContext().colors.AddColor((yyvsp[-3].multi_str).str, "++"+string((yyvsp[0].str)), chart.Error, CHART_POS((yyloc)));
  #endif
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[0].str));
}
#line 4261 "xxx_csh_lang.cc"
    break;

  case 112: /* colordef: alpha_string TOK_EQUAL  */
#line 1769 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH((yylsp[-1]), COLOR_COLORNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE)) {
        csh.AddColorValuesToHints(false);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing color definition.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing color definition.");
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 4284 "xxx_csh_lang.cc"
    break;

  case 113: /* colordef: alpha_string  */
#line 1788 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_COLORNAME);
    if (csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
    csh.AddCSH_ErrorAfter((yyloc), "Missing equal sign ('=') and a color definition.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing equal sign ('=') and a color definition.");
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 4303 "xxx_csh_lang.cc"
    break;

  case 114: /* designdef: TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON TOK_CCBRACKET  */
#line 1806 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-4]), COLOR_DESIGNNAME);
    csh.AddCSH((yylsp[-1]), COLOR_SEMICOLON);
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-3])+(yylsp[0]));
    if (!csh.Contexts.back().SkipContent()) {
        auto &d = csh.Contexts.back().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-4].str));
        if (i == d.end())
            d.emplace((yyvsp[-4].str), csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::LINE_START) ||
         csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) ) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!chart.SkipContent()) {
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-4].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-3]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
    free((yyvsp[-4].str));
    (yyvsp[0].input_text_ptr); //supress
}
#line 4347 "xxx_csh_lang.cc"
    break;

  case 115: /* designdef: TOK_STRING scope_open_empty designelementlist TOK_SEMICOLON error TOK_CCBRACKET  */
#line 1846 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-5]), COLOR_DESIGNNAME);
    csh.AddCSH((yylsp[-2]), COLOR_SEMICOLON);
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as part of a design definition.");
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.BracePairs.push_back((yylsp[-4])+(yylsp[0]));
    if (!csh.Contexts.back().SkipContent()) {
        auto &d = csh.Contexts.back().IsFull() ? csh.FullDesigns : csh.PartialDesigns;
        auto i = d.find((yyvsp[-5].str));
        if (i == d.end())
            d.emplace((yyvsp[-5].str), csh.Contexts.back());
        else
            i->second += csh.Contexts.back();
    }
    csh.PopContext();
    if ((csh.CheckHintBetween((yylsp[-4]), (yylsp[-3]), EHintSourceType::LINE_START) ||
         csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::LINE_START))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#else
    if (!chart.SkipContent()) {
        //if closing brace missing, still do the design definition
        //'scope_open_empty' pushed an empty color & style set onto the stack
        //then designelementlist added color & style definitions, procedures, etc., now we harvest those.
        //This is either a lookup if the design exists or creates a new empty design of this name
        auto i = chart.Designs.emplace(std::piecewise_construct,
                                       std::forward_as_tuple((yyvsp[-5].str)),
                                       std::forward_as_tuple(chart.MyCurrentContext().IsFull(),
                                                             EContextParse::NORMAL,
                                                             EContextCreate::EMPTY,
                                                             CHART_POS_START((yylsp[-4]))));
        //we apply the content from current context all the same if inserted or existing
        i.first->second.ApplyContextContent(std::move(chart.MyCurrentContext())); //we can move, will pop below
    }
    chart.PopContext();
  #endif
    (yyvsp[0].input_text_ptr); //supress
}
#line 4392 "xxx_csh_lang.cc"
    break;

  case 116: /* scope_open_empty: TOK_OCBRACKET  */
#line 1889 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(true, EContextParse::NORMAL);
  #else
    //push empty color & style sets for design definition
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::NORMAL, EContextCreate::CLEAR);
  #endif
    (yyvsp[0].input_text_ptr); //supress
}
#line 4407 "xxx_csh_lang.cc"
    break;

  case 118: /* designelementlist: designelementlist TOK_SEMICOLON designelement  */
#line 1902 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_SEMICOLON);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
#endif
}
#line 4421 "xxx_csh_lang.cc"
    break;

  case 119: /* designelement: TOK_COMMAND_DEFCOLOR colordeflist  */
#line 1913 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
    }
  #endif
    free((yyvsp[-1].str));
}
#line 4439 "xxx_csh_lang.cc"
    break;

  case 120: /* designelement: TOK_COMMAND_DEFCOLOR  */
#line 1927 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddColorValuesToHints(true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing color name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a color name to (re-)define.");
  #endif
    free((yyvsp[0].str));
}
#line 4460 "xxx_csh_lang.cc"
    break;

  case 121: /* designelement: TOK_COMMAND_DEFSTYLE styledeflist  */
#line 1944 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
  #endif
    free((yyvsp[-1].str));
}
#line 4478 "xxx_csh_lang.cc"
    break;

  case 122: /* designelement: TOK_COMMAND_DEFSTYLE  */
#line 1958 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddDesignLineBeginToHints();
        csh.hintStatus = HINT_READY;
    } else if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddStylesToHints(true, true);
        csh.hintStatus = HINT_READY;
	}
	csh.AddCSH_ErrorAfter((yyloc), "Missing style name to (re-)define.");
  #else
    chart.Error.Error(CHART_POS((yyloc)).end, "Missing a style name to (re-)define.");
  #endif
    free((yyvsp[0].str));
}
#line 4499 "xxx_csh_lang.cc"
    break;

  case 125: /* designoptlist: designoptlist TOK_COMMA designopt  */
#line 1978 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    if (csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 4513 "xxx_csh_lang.cc"
    break;

  case 126: /* designoptlist: designoptlist TOK_COMMA  */
#line 1988 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    }
  #endif
}
#line 4527 "xxx_csh_lang.cc"
    break;

  case 127: /* designoptlist: designoptlist error  */
#line 1998 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Extra stuff after design options. Maybe missing a comma?");
  #endif
}
#line 4537 "xxx_csh_lang.cc"
    break;

  case 128: /* designopt: entity_string TOK_EQUAL TOK_NUMBER  */
#line 2005 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        XxxChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[-2].multi_str).had_error)
        chart.AddDesignAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 4562 "xxx_csh_lang.cc"
    break;

  case 129: /* designopt: entity_string TOK_EQUAL string  */
#line 2026 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    if (csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-2].multi_str).had_error && csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str)) {
        XxxChart::AttributeValues((yyvsp[-2].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    if (!(yyvsp[-2].multi_str).had_error)
        chart.AddDesignAttribute(Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].multi_str).str, CHART_POS((yylsp[-2])), CHART_POS((yylsp[0]))));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 4588 "xxx_csh_lang.cc"
    break;

  case 130: /* designopt: entity_string TOK_EQUAL  */
#line 2048 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_OPTIONNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    if (csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME)) {
        csh.AddDesignOptionsToHints();
        csh.hintStatus = HINT_READY;
    } else if (!(yyvsp[-1].multi_str).had_error && csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str)) {
        XxxChart::AttributeValues((yyvsp[-1].multi_str).str, csh);
        csh.hintStatus = HINT_READY;
    }
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing option value. Ignoring this.");
#endif
    free((yyvsp[-1].multi_str).str);
}
#line 4610 "xxx_csh_lang.cc"
    break;

  case 131: /* defproc: defprochelp1  */
#line 2068 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procdefhelper)) {
        if (chart.SkipContent()) {
            chart.Error.Error(CHART_POS_START((yyloc)), "Cannot define procedures inside a procedure.");
        } else if ((yyvsp[0].procdefhelper)->name.had_error) {
            //do nothing, error already reported
        } else if ((yyvsp[0].procdefhelper)->name.str==nullptr || (yyvsp[0].procdefhelper)->name.str[0]==0) {
            chart.Error.Error((yyvsp[0].procdefhelper)->linenum_name, "Missing a procedure name to (re-)define. Ignoring this.");
        } else if (!(yyvsp[0].procdefhelper)->had_error && (yyvsp[0].procdefhelper)->body) {
            if (chart.MyCurrentContext().num_error != chart.Error.GetErrorNum(true, false)) {
                chart.Error.Error(CHART_POS_START((yyloc)), "There are warnings or errors inside the procedure definition. Ignoring it.");
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].name = (yyvsp[0].procdefhelper)->name.str;
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].status = EDefProcResult::PROBLEM;
                chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str].file_pos = (yyvsp[0].procdefhelper)->linenum_body;
            } else if ((yyvsp[0].procdefhelper)->body->status==EDefProcResult::OK || (yyvsp[0].procdefhelper)->body->status==EDefProcResult::EMPTY) {
                if ((yyvsp[0].procdefhelper)->parameters) {
                    auto &p = chart.MyCurrentContext().Procedures[(yyvsp[0].procdefhelper)->name.str] = *(yyvsp[0].procdefhelper)->body;
                    p.name = (yyvsp[0].procdefhelper)->name.str;
                    p.parameters = std::move(*(yyvsp[0].procdefhelper)->parameters);
                    if ((yyvsp[0].procdefhelper)->attrs) for (auto &a : *(yyvsp[0].procdefhelper)->attrs)
                        p.AddAttribute(*a, chart);
                    if ((yyvsp[0].procdefhelper)->body->status==EDefProcResult::EMPTY)
                        chart.Error.Warning((yyvsp[0].procdefhelper)->linenum_body, "Empty procedure. Is this what you want?");
                } else {
                     chart.Error.Error(CHART_POS_START((yyloc)), "Ill-formed procedure parameter list. Ignoring this procedure definition.");
                }
            } else {
                 chart.Error.Error(CHART_POS_START((yyloc)), "Ill-formed procedure body. Ignoring this procedure definition.");
            }
        }
        delete (yyvsp[0].procdefhelper);
    }
  #endif
}
#line 4651 "xxx_csh_lang.cc"
    break;

  case 132: /* defprochelp1: TOK_COMMAND_DEFPROC  */
#line 2107 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent()) {
        csh.AddCSH_Error((yylsp[0]), "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
        csh.AddCSH_ErrorAfter((yyloc), "Missing procedure name to (re-)define.");
    }
    if (csh.CheckLineStartHintAt((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->linenum_name = CHART_POS_AFTER((yyloc));
  #endif
    free((yyvsp[0].str));
}
#line 4674 "xxx_csh_lang.cc"
    break;

  case 133: /* defprochelp1: TOK_COMMAND_DEFPROC defprochelp2  */
#line 2126 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.Contexts.back().SkipContent()) {
        csh.AddCSH_Error((yylsp[-1]), "Cannot define procedures inside a procedure.");
    } else {
        csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    }
    if (csh.CheckLineStartHintAt((yylsp[-1]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
  #endif
    free((yyvsp[-1].str));
}
#line 4695 "xxx_csh_lang.cc"
    break;

  case 134: /* defprochelp2: alpha_string  */
#line 2144 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PROCNAME);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
    free((yyvsp[0].multi_str).str);
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->name = (yyvsp[0].multi_str);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[0]));
  #endif
}
#line 4711 "xxx_csh_lang.cc"
    break;

  case 135: /* defprochelp2: alpha_string defprochelp3  */
#line 2156 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PROCNAME);
    free((yyvsp[-1].multi_str).str);
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->name = (yyvsp[-1].multi_str);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[-1]));
  #endif
}
#line 4726 "xxx_csh_lang.cc"
    break;

  case 136: /* defprochelp2: defprochelp3  */
#line 2167 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error(CshPos((yylsp[0]).first_pos, (yylsp[0]).first_pos), "Missing procedure name.");
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->linenum_name = CHART_POS_START((yylsp[0]));
  #endif
}
#line 4739 "xxx_csh_lang.cc"
    break;

  case 137: /* defprochelp3: proc_def_arglist_tested  */
#line 2177 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->parameters = (yyvsp[0].procparamdeflist);
  #endif
}
#line 4752 "xxx_csh_lang.cc"
    break;

  case 138: /* defprochelp3: proc_def_arglist_tested defprochelp4  */
#line 2186 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->parameters = (yyvsp[-1].procparamdeflist);
  #endif
}
#line 4764 "xxx_csh_lang.cc"
    break;

  case 139: /* defprochelp3: defprochelp4  */
#line 2194 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = (yyvsp[0].procdefhelper);
    (yyval.procdefhelper)->parameters = new ProcParamDefList;
  #endif
}
#line 4776 "xxx_csh_lang.cc"
    break;

  case 140: /* defprochelp4: full_attrlist  */
#line 2203 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a procedure definition starting with '{'.");
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[0])))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[0])))
        csh.AddYesNoToHints();
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->attrs = (yyvsp[0].attributelist);
  #endif
}
#line 4795 "xxx_csh_lang.cc"
    break;

  case 141: /* defprochelp4: full_attrlist procedure_body  */
#line 2218 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (csh.CheckHintLocated(EHintSourceType::ATTR_NAME, (yylsp[-1])))
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_ATTRNAME) + "export",
            "Set if styles and colors defined in the procedure remain valid after calling it.",
            EHintType::ATTR_NAME));
    else if (csh.CheckHintLocated(EHintSourceType::ATTR_VALUE, (yylsp[-1])))
        csh.AddYesNoToHints();
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->body = (yyvsp[0].procedure);
    (yyval.procdefhelper)->linenum_body = CHART_POS_START((yylsp[0]));
    (yyval.procdefhelper)->attrs = (yyvsp[-1].attributelist);
  #endif
}
#line 4815 "xxx_csh_lang.cc"
    break;

  case 142: /* defprochelp4: procedure_body  */
#line 2234 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.procdefhelper) = new ProcDefParseHelper<AttributeList>;
    (yyval.procdefhelper)->body = (yyvsp[0].procedure);
    (yyval.procdefhelper)->linenum_body = CHART_POS_START((yylsp[0]));
  #endif
}
#line 4828 "xxx_csh_lang.cc"
    break;

  case 143: /* scope_open_proc_body: TOK_OCBRACKET  */
#line 2245 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    chart.MyCurrentContext().num_error = chart.Error.GetErrorNum(true, false);
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT, EContextCreate::EMPTY);
    chart.MyCurrentContext().parameters = std::move(YYGET_EXTRA(yyscanner)->last_procedure_params);
    chart.MyCurrentContext().starts_procedure = true;
    _ASSERT(YYGET_EXTRA(yyscanner)->open_context_mode == base_parse_parm::EScopeOpenMode::NORMAL);
    YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
  #endif
    (yyval.input_text_ptr) = (yyvsp[0].input_text_ptr);
}
#line 4847 "xxx_csh_lang.cc"
    break;

  case 144: /* scope_close_proc_body: TOK_CCBRACKET  */
#line 2261 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.PopContext();
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
  #else
    chart.PopContext();
  #endif
    (yyval.input_text_ptr) = (yyvsp[0].input_text_ptr);
}
#line 4861 "xxx_csh_lang.cc"
    break;

  case 145: /* proc_def_arglist_tested: proc_def_arglist  */
#line 2272 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparamdeflist)) {
        auto pair = Procedure::AreAllParameterNamesUnique(*(yyvsp[0].procparamdeflist));
        if (pair.first) {
            chart.Error.Error(pair.second->linenum_name, "This parameter name is already used.");
            chart.Error.Error(pair.first->linenum_name, pair.second->linenum_name, "This parameter name is already used.");
            delete (yyvsp[0].procparamdeflist);
            (yyval.procparamdeflist) = nullptr;
        } else {
            //Also copy to YYGET_EXTRA(yyscanner)->last_procedure_params and set open_context_mode
            auto &store = YYGET_EXTRA(yyscanner)->last_procedure_params;
            store.clear();
            for (const auto &p : *(yyvsp[0].procparamdeflist))
                store.emplace(p->name, ProcParamResolved(std::string(), FileLineCol(), true));
            (yyval.procparamdeflist) = (yyvsp[0].procparamdeflist);
        }
    } else
        (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4888 "xxx_csh_lang.cc"
    break;

  case 146: /* proc_def_arglist: TOK_OPARENTHESIS TOK_CPARENTHESIS  */
#line 2296 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparamdeflist) = new ProcParamDefList;
  #endif
}
#line 4901 "xxx_csh_lang.cc"
    break;

  case 147: /* proc_def_arglist: TOK_OPARENTHESIS error TOK_CPARENTHESIS  */
#line 2305 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter definitions.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter definitions.", "Say something like '($first, $second=default)'.");
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4916 "xxx_csh_lang.cc"
    break;

  case 148: /* proc_def_arglist: TOK_OPARENTHESIS  */
#line 2316 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter list closed by a parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter list closed by a parenthesis ')'.");
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4930 "xxx_csh_lang.cc"
    break;

  case 149: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list error TOK_CPARENTHESIS  */
#line 2326 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_PARENTHESIS);
    csh.AddCSH_Error((yylsp[-1]), "Invalid parameter definitions.");
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Invalid parameter definitions.");
    delete (yyvsp[-2].procparamdeflist);
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4946 "xxx_csh_lang.cc"
    break;

  case 150: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list  */
#line 2338 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_PARENTHESIS);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing closing parenthesis ')'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing closing parenthesis ')'.");
    delete (yyvsp[0].procparamdeflist);
    (yyval.procparamdeflist) = nullptr;
  #endif
}
#line 4961 "xxx_csh_lang.cc"
    break;

  case 151: /* proc_def_arglist: TOK_OPARENTHESIS proc_def_param_list TOK_CPARENTHESIS  */
#line 2349 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_PARENTHESIS);
    csh.AddCSH((yylsp[0]), COLOR_PARENTHESIS);
  #else
    (yyval.procparamdeflist) = (yyvsp[-1].procparamdeflist);
  #endif
}
#line 4974 "xxx_csh_lang.cc"
    break;

  case 152: /* proc_def_param_list: proc_def_param  */
#line 2359 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    if ((yyvsp[0].procparamdef)) {
        (yyval.procparamdeflist) = new ProcParamDefList;
        ((yyval.procparamdeflist))->Append((yyvsp[0].procparamdef));
    } else
        (yyval.procparamdeflist)= nullptr;
  #endif
}
#line 4989 "xxx_csh_lang.cc"
    break;

  case 153: /* proc_def_param_list: proc_def_param_list TOK_COMMA  */
#line 2370 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing parameter after the comma.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing parameter after the comma.");
    delete (yyvsp[-1].procparamdeflist);
    (yyval.procparamdeflist)= nullptr;
  #endif
}
#line 5004 "xxx_csh_lang.cc"
    break;

  case 154: /* proc_def_param_list: proc_def_param_list TOK_COMMA proc_def_param  */
#line 2381 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
  #else
    if ((yyvsp[-2].procparamdeflist) && (yyvsp[0].procparamdef)) {
        ((yyvsp[-2].procparamdeflist))->Append((yyvsp[0].procparamdef));
        (yyval.procparamdeflist) = (yyvsp[-2].procparamdeflist);
    } else {
        delete (yyvsp[-2].procparamdeflist);
        delete (yyvsp[0].procparamdef);
        (yyval.procparamdeflist)= nullptr;
    }
  #endif
}
#line 5023 "xxx_csh_lang.cc"
    break;

  case 155: /* proc_def_param: TOK_PARAM_NAME  */
#line 2397 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].str) && (yyvsp[0].str)[0]=='$' && (yyvsp[0].str)[1])
        csh.AddCSH((yylsp[0]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[0]), "Need name after the '$' sign.");
    (yyval.procparamdef) = nullptr; //no value
  #else
    if ((yyvsp[0].str) && (yyvsp[0].str)[0]=='$' && (yyvsp[0].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[0].str), CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[0].str));
}
#line 5045 "xxx_csh_lang.cc"
    break;

  case 156: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL  */
#line 2415 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if ((yyvsp[-1].str) && (yyvsp[-1].str)[0]=='$' && (yyvsp[-1].str)[1])
        csh.AddCSH((yylsp[-1]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-1]), "Need name after the '$' sign.");
    (yyval.procparamdef) = nullptr; //no value
  #else
    if ((yyvsp[-1].str) && (yyvsp[-1].str)[0]=='$' && (yyvsp[-1].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-1].str), CHART_POS_START((yylsp[-1])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-1].str));
}
#line 5068 "xxx_csh_lang.cc"
    break;

  case 157: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL string  */
#line 2434 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1])
        csh.AddCSH((yylsp[-2]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-2]), "Need name after the '$' sign.");
    (yyval.procparamdef) = (char*)1; //has value
  #else
    if ((yyvsp[0].multi_str).had_error) {
        (yyval.procparamdef) = nullptr;
    } else if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-2].str), CHART_POS_START((yylsp[-2])), (yyvsp[0].multi_str).str, CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-2])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].multi_str).str);
}
#line 5094 "xxx_csh_lang.cc"
    break;

  case 158: /* proc_def_param: TOK_PARAM_NAME TOK_EQUAL TOK_NUMBER  */
#line 2456 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1])
        csh.AddCSH((yylsp[-2]), COLOR_PARAMNAME);
    else
        csh.AddCSH_Error((yylsp[-2]), "Need name after the '$' sign.");
    (yyval.procparamdef) = (char*)1; //has value
  #else
    if ((yyvsp[-2].str) && (yyvsp[-2].str)[0]=='$' && (yyvsp[-2].str)[1]) {
        (yyval.procparamdef) = new ProcParamDef((yyvsp[-2].str), CHART_POS_START((yylsp[-2])), (yyvsp[0].str), CHART_POS_START((yylsp[0])));
    } else {
        chart.Error.Error(CHART_POS_START((yylsp[-2])), "Need name after the '$' sign.");
        (yyval.procparamdef) = nullptr;
    }
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 5118 "xxx_csh_lang.cc"
    break;

  case 159: /* procedure_body: scope_open_proc_body instrlist scope_close_proc_body  */
#line 2478 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::OK;
    tmp->text = std::string(((yyvsp[-2].input_text_ptr)), ((yyvsp[0].input_text_ptr))+1)+";";
    tmp->file_pos = CHART_POS_START((yyloc));
    if ((yyvsp[-1].instruction_list))
        delete (yyvsp[-1].instruction_list);
    (yyval.procedure) = tmp;
  #endif
}
#line 5136 "xxx_csh_lang.cc"
    break;

  case 160: /* procedure_body: scope_open_proc_body scope_close_proc_body  */
#line 2492 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::EMPTY;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 5153 "xxx_csh_lang.cc"
    break;

  case 161: /* procedure_body: scope_open_proc_body instrlist error scope_close_proc_body  */
#line 2505 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "syntax error.");
    if ((yyvsp[-2].instruction_list))
        delete (yyvsp[-2].instruction_list);
  #endif
    yyerrok;
  (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
  (yyvsp[0].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 5175 "xxx_csh_lang.cc"
    break;

  case 162: /* procedure_body: scope_open_proc_body instrlist error TOK_EOF  */
#line 2523 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as a valid line.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-3])), CHART_POS_START((yylsp[-1])), "Here is the corresponding '{'.");
    if ((yyvsp[-2].instruction_list))
        delete (yyvsp[-2].instruction_list);
  #endif
  (yyvsp[-3].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 5198 "xxx_csh_lang.cc"
    break;

  case 163: /* procedure_body: scope_open_proc_body instrlist TOK_EOF  */
#line 2542 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_AFTER((yylsp[-1])), "Here is the corresponding '{'.");
    if ((yyvsp[-1].instruction_list))
        delete (yyvsp[-1].instruction_list);
  #endif
  (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 5221 "xxx_csh_lang.cc"
    break;

  case 164: /* procedure_body: scope_open_proc_body TOK_EOF  */
#line 2561 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddOpenBracePair((yyloc));
    csh.AddCSH_ErrorAfter((yylsp[-1]), "Missing a closing brace ('}').");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_AFTER((yylsp[-1])), "Missing a corresponding '}'.");
  #endif
  (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
}
#line 5241 "xxx_csh_lang.cc"
    break;

  case 165: /* procedure_body: scope_open_proc_body instrlist TOK_BYE  */
#line 2577 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
    chart.Error.Error(CHART_POS_START((yylsp[-2])), CHART_POS_START((yylsp[0])), "Here is the opening '{'.");
    if ((yyvsp[-1].instruction_list))
        delete (yyvsp[-1].instruction_list);
  #endif
    (yyvsp[-2].input_text_ptr); //to silence 'unused parameter' warnings
    free((yyvsp[0].str));
}
#line 5265 "xxx_csh_lang.cc"
    break;

  case 166: /* procedure_body: scope_open_proc_body TOK_BYE  */
#line 2597 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.BracePairs.push_back((yyloc));
    csh.AddCSH_Error((yylsp[0]), "The command 'bye' can only be used at the top level and not inside curly braces '{' and '}'.");
    csh.PopContext();
  #else
    auto tmp = new Procedure;
    tmp->status = EDefProcResult::PROBLEM;
    tmp->file_pos = CHART_POS_START((yyloc));
    (yyval.procedure) = tmp;
    chart.PopContext();
    chart.Error.Error(CHART_POS_START((yylsp[0])), "The command 'bye' can not be used between curly braces '{' and '}'.");
  #endif
    (yyvsp[-1].input_text_ptr); //to silence 'unused parameter' warnings
    free((yyvsp[0].str));
}
#line 5286 "xxx_csh_lang.cc"
    break;

  case 167: /* set: TOK_COMMAND_SET proc_def_param  */
#line 2615 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    if (!(yyvsp[0].procparamdef))
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing value.");
  #else
    if (!chart.SkipContent())
        chart.SetVariable((yyvsp[0].procparamdef), CHART_POS((yyloc)));
  #endif
    free((yyvsp[-1].str));
}
#line 5302 "xxx_csh_lang.cc"
    break;

  case 168: /* set: TOK_COMMAND_SET  */
#line 2627 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing variable or parameter name to set.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing variable or parameter name to set.");
  #endif
    free((yyvsp[0].str));
}
#line 5316 "xxx_csh_lang.cc"
    break;

  case 170: /* condition: string  */
#line 2640 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #endif
    (yyval.condition) = (yyvsp[0].multi_str).had_error ? 2 : (yyvsp[0].multi_str).str && (yyvsp[0].multi_str).str[0];
    free((yyvsp[0].multi_str).str);
}
#line 5328 "xxx_csh_lang.cc"
    break;

  case 171: /* condition: string comp  */
#line 2648 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_ATTRVALUE);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to compare to.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to compare to.");
  #endif
    (yyval.condition) = 2;
    free((yyvsp[-1].multi_str).str);
    (yyvsp[0].compare_op); //to suppress
}
#line 5345 "xxx_csh_lang.cc"
    break;

  case 172: /* condition: string comp string  */
#line 2661 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    if ((yyvsp[-1].compare_op)!=ECompareOperator::INVALID) {
        csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
        (yyval.condition) = (yyvsp[-2].multi_str).Compare((yyvsp[-1].compare_op), (yyvsp[0].multi_str));
    } else {
        csh.AddCSH_Error((yylsp[-1]), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        (yyval.condition) = 2;
    }
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
    if ((yyvsp[-1].compare_op)!=ECompareOperator::INVALID)
        (yyval.condition) = (yyvsp[-2].multi_str).Compare((yyvsp[-1].compare_op), (yyvsp[0].multi_str));
    else {
        chart.Error.Error(CHART_POS_START((yylsp[-1])), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
        (yyval.condition) = 2;
    }
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
}
#line 5372 "xxx_csh_lang.cc"
    break;

  case 173: /* condition: string error string  */
#line 2684 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_ATTRVALUE);
    csh.AddCSH_Error((yylsp[-1]), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
  #else
     chart.Error.Error(CHART_POS_START((yylsp[-1])), "Bad comparison operator. Use one of '==', '<>', '<=', '=>', '<' or '>'.");
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].multi_str).str);
    (yyval.condition) = 2;
}
#line 5389 "xxx_csh_lang.cc"
    break;

  case 174: /* ifthen_condition: TOK_IF condition TOK_THEN  */
#line 2698 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = (yyvsp[-1].condition);
    const bool cond_true = (yyvsp[-1].condition)==1;
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    if (cond_true)
        csh.PushContext();
    else
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = (yyvsp[-1].condition);
    const bool cond_true = (yyvsp[-1].condition)==1;
    if (cond_true)
        chart.PushContext(CHART_POS_START((yylsp[-2])));
    else
        chart.PushContext(CHART_POS_START((yylsp[-2])), EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_true;
    chart.MyCurrentContext().export_styles = cond_true;
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 5421 "xxx_csh_lang.cc"
    break;

  case 175: /* ifthen_condition: TOK_IF condition  */
#line 2726 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing 'then' keyword.");
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "then",
            "Continue the 'if' statement with 'then'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-1])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing 'then' keyword.");
  #endif
    free((yyvsp[-1].str));
    (yyvsp[0].condition); //to supress warnings
}
#line 5446 "xxx_csh_lang.cc"
    break;

  case 176: /* ifthen_condition: TOK_IF  */
#line 2747 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing condition.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[0].str));
}
#line 5464 "xxx_csh_lang.cc"
    break;

  case 177: /* ifthen_condition: TOK_IF error  */
#line 2761 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-1]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[0]), "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-1])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[-1].str));
}
#line 5482 "xxx_csh_lang.cc"
    break;

  case 178: /* ifthen_condition: TOK_IF error TOK_THEN  */
#line 2775 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.condition) = csh.Contexts.back().if_condition = 2;
    csh.AddCSH((yylsp[-2]), COLOR_KEYWORD);
    csh.AddCSH_Error((yylsp[-1]), "Missing condition instead of this.");
    csh.PushContext(true, EContextParse::SKIP_CONTENT);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    (yyval.condition) = chart.MyCurrentContext().if_condition = 2;
    chart.PushContext(CHART_POS_START((yylsp[-2])), EContextParse::SKIP_CONTENT);
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Missing condition after 'if'.");
  #endif
    free((yyvsp[-2].str));
    free((yyvsp[0].str));
}
#line 5505 "xxx_csh_lang.cc"
    break;

  case 179: /* else: TOK_ELSE  */
#line 2796 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_KEYWORD);
    if (csh.CheckLineStartHintAfter((yylsp[0]))) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
    csh.PopContext();
    const bool cond_false = csh.Contexts.back().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    (yyval.condition) = csh.Contexts.back().if_condition;
    if (cond_false)
        csh.PushContext();
    else
        csh.PushContext(true, EContextParse::SKIP_CONTENT);
  #else
    //kill previous context and open new one - set laterreparse if error or if condition was true
    //this will ignore everything in the else clause
    chart.PopContext();
    const bool cond_false = chart.MyCurrentContext().if_condition==0;
    //Return value not used, just there so that nonterminal 'else' can have destructor
    (yyval.condition) = chart.MyCurrentContext().if_condition;
    if (cond_false)
        chart.PushContext(CHART_POS_START((yylsp[0])));
    else
        chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::SKIP_CONTENT);
    chart.MyCurrentContext().export_colors = cond_false;
    chart.MyCurrentContext().export_styles = cond_false;
  #endif
    free((yyvsp[0].str));
}
#line 5541 "xxx_csh_lang.cc"
    break;

  case 180: /* ifthen: ifthen_condition instr  */
#line 2829 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.PopContext();
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::KEYWORD)) {
        csh.AddToHints(CshHint(csh.HintPrefix(COLOR_KEYWORD) + "else",
            "Continue the 'if/then' statement with 'else'.",
            EHintType::KEYWORD, true));
        csh.hintStatus = HINT_READY;
    }
  #else
    if ((yyvsp[-1].condition)==1) {
        (yyval.instruction) = (yyvsp[0].instruction);
    } else {
        (yyval.instruction) = nullptr;
        delete (yyvsp[0].instruction);
    }
    chart.PopContext();
  #endif
}
#line 5566 "xxx_csh_lang.cc"
    break;

  case 181: /* ifthen: ifthen_condition  */
#line 2850 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].condition)!=2)
        csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'then'.");
    csh.PopContext();
  #else
    if ((yyvsp[0].condition)!=2)
        chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing a well-formed command after 'then'. Ignoring 'if' clause.");
    chart.PopContext();
    (yyval.instruction) = nullptr;
  #endif
    (yyvsp[0].condition); //suppress
}
#line 5584 "xxx_csh_lang.cc"
    break;

  case 182: /* ifthen: ifthen_condition error  */
#line 2864 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[0]), "Missing command after 'then'.");
    csh.PopContext();
  #else
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing a well-formed command. Ignoring 'if' clause.");
    chart.PopContext();
    (yyval.instruction) = nullptr;
  #endif
    (yyvsp[-1].condition); //suppress
}
#line 5600 "xxx_csh_lang.cc"
    break;

  case 183: /* ifthen: ifthen_condition instr else  */
#line 2876 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-1]));
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    delete (yyvsp[-1].instruction);
    (yyval.instruction) = nullptr;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-2].condition); (yyvsp[0].condition); //suppress
}
#line 5618 "xxx_csh_lang.cc"
    break;

  case 184: /* ifthen: ifthen_condition instr error else  */
#line 2890 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-2]));
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    delete (yyvsp[-2].instruction);
    (yyval.instruction) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "I am not sure what is coming here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-3].condition); (yyvsp[0].condition); //suppress
}
#line 5638 "xxx_csh_lang.cc"
    break;

  case 185: /* ifthen: ifthen_condition error else  */
#line 2906 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_Error((yylsp[-1]), "I am not sure what is coming here.");
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing command after 'else'.");
    csh.PopContext();
  #else
    (yyval.instruction) = nullptr;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "I am not sure what is coming here.");
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing command after 'else'. Ignoring 'if' clause.");
    chart.PopContext();
  #endif
    (yyvsp[-2].condition); (yyvsp[0].condition); //suppress
}
#line 5656 "xxx_csh_lang.cc"
    break;

  case 186: /* ifthen: ifthen_condition instr else instr  */
#line 2920 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-2]));
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.PopContext();
  #else
    switch ((yyvsp[-3].condition)) {
    case 1: //original condition was true
        (yyval.instruction) = (yyvsp[-2].instruction);   //take 'then' branch
        delete (yyvsp[0].instruction); //delete 'else' branch
        break;
    case 0: //original condition was false
        (yyval.instruction) = (yyvsp[0].instruction); //take 'else' branch
        delete (yyvsp[-2].instruction); //delete 'then' branch
        break;
    default:
        _ASSERT(0);
        FALLTHROUGH;
    case 2: //we had an error, but has reported the error - delete both branches
        (yyval.instruction) = nullptr;
        delete (yyvsp[-2].instruction);
        delete (yyvsp[0].instruction);
        break;
    }
    chart.PopContext();
  #endif
    (yyvsp[-1].condition); //suppress
}
#line 5689 "xxx_csh_lang.cc"
    break;

  case 187: /* ifthen: ifthen_condition instr error else instr  */
#line 2949 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[-3]));
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.AddCSH_Error((yylsp[-2]), "I am not sure what is coming here.");
    csh.PopContext();
  #else
    (yyval.instruction) = nullptr;
    delete (yyvsp[-3].instruction);
    delete (yyvsp[0].instruction);
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    (yyvsp[-4].condition); (yyvsp[-1].condition); //suppress
}
#line 5709 "xxx_csh_lang.cc"
    break;

  case 188: /* ifthen: ifthen_condition error else instr  */
#line 2965 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddInstructionIfNotBrace((yylsp[0]));
    csh.AddCSH_Error((yylsp[-2]), "I am not sure what is coming here.");
    csh.PopContext();
  #else
    (yyval.instruction) = nullptr;
    delete (yyvsp[0].instruction);
    chart.Error.Error(CHART_POS_START((yylsp[-2])), "I am not sure what is coming here. Ignoring 'if' command.");
    chart.PopContext();
  #endif
    (yyvsp[-3].condition); (yyvsp[-1].condition); //suppress
}
#line 5727 "xxx_csh_lang.cc"
    break;

  case 189: /* colon_string: TOK_COLON_QUOTED_STRING  */
#line 2981 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), false);
    csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 5739 "xxx_csh_lang.cc"
    break;

  case 190: /* colon_string: TOK_COLON_STRING  */
#line 2989 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ColonString_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), true);
	csh.AddColonLabel((yylsp[0]), (yyvsp[0].str));
  #endif
    (yyval.str) = (yyvsp[0].str);
}
#line 5751 "xxx_csh_lang.cc"
    break;

  case 191: /* full_attrlist_with_label: colon_string  */
#line 2998 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attributelist) = (new AttributeList)->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yyloc)).IncStartCol()));
  #endif
    free((yyvsp[0].str));
}
#line 5763 "xxx_csh_lang.cc"
    break;

  case 192: /* full_attrlist_with_label: colon_string full_attrlist  */
#line 3006 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attributelist) = ((yyvsp[0].attributelist))->Prepend(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()));
  #endif
    free((yyvsp[-1].str));
}
#line 5775 "xxx_csh_lang.cc"
    break;

  case 193: /* full_attrlist_with_label: full_attrlist colon_string full_attrlist  */
#line 3014 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attributelist) = ((yyvsp[-2].attributelist))->Append(std::make_unique<Attribute>("label", (yyvsp[-1].str), CHART_POS((yylsp[-1])), CHART_POS((yylsp[-1])).IncStartCol()));
        //Merge $3 at the end of $1
        ((yyvsp[-2].attributelist))->splice(((yyvsp[-2].attributelist))->end(), *((yyvsp[0].attributelist)));
        delete ((yyvsp[0].attributelist)); //empty list now
        (yyval.attributelist) = (yyvsp[-2].attributelist);
  #endif
    free((yyvsp[-1].str));
}
#line 5791 "xxx_csh_lang.cc"
    break;

  case 194: /* full_attrlist_with_label: full_attrlist colon_string  */
#line 3026 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
        (yyval.attributelist) = ((yyvsp[-1].attributelist))->Append(std::make_unique<Attribute>("label", (yyvsp[0].str), CHART_POS((yylsp[0])), CHART_POS((yylsp[0])).IncStartCol()));
  #endif
    free((yyvsp[0].str));
}
#line 5803 "xxx_csh_lang.cc"
    break;

  case 196: /* full_attrlist: TOK_OSBRACKET TOK_CSBRACKET  */
#line 3037 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = new AttributeList;
  #endif
}
#line 5818 "xxx_csh_lang.cc"
    break;

  case 197: /* full_attrlist: TOK_OSBRACKET attrlist TOK_CSBRACKET  */
#line 3048 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
	csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
  #endif
}
#line 5833 "xxx_csh_lang.cc"
    break;

  case 198: /* full_attrlist: TOK_OSBRACKET attrlist error TOK_CSBRACKET  */
#line 3059 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-3]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[-1]), "Extra stuff after an attribute list. Maybe missing a comma?");
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-3]), (yylsp[-2]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = (yyvsp[-2].attributelist);
  #endif
}
#line 5849 "xxx_csh_lang.cc"
    break;

  case 199: /* full_attrlist: TOK_OSBRACKET error TOK_CSBRACKET  */
#line 3071 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[-1]), "Could not recognize this as an attribute or style name.");
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_START((yylsp[-1])), "Expecting an attribute or style name. Ignoring all until the closing square bracket (']').");
#endif
}
#line 5866 "xxx_csh_lang.cc"
    break;

  case 200: /* full_attrlist: TOK_OSBRACKET attrlist  */
#line 3084 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a square bracket (']').");
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
	csh.SqBracketPairs.push_back((yyloc));
  #else
    (yyval.attributelist) = (yyvsp[0].attributelist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 5882 "xxx_csh_lang.cc"
    break;

  case 201: /* full_attrlist: TOK_OSBRACKET attrlist error  */
#line 3096 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddCSH((yylsp[-2]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[0]), "Missing a ']'.");
	csh.SqBracketPairs.push_back((yyloc));
	csh.CheckHintBetween((yylsp[-2]), (yylsp[-1]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing ']'.");
  #endif
}
#line 5898 "xxx_csh_lang.cc"
    break;

  case 202: /* full_attrlist: TOK_OSBRACKET  */
#line 3108 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACKET);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing a square bracket (']').");
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing ']'.");
  #endif
}
#line 5914 "xxx_csh_lang.cc"
    break;

  case 203: /* full_attrlist: TOK_OSBRACKET error  */
#line 3120 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_BRACKET);
    csh.AddCSH_Error((yylsp[0]), "Missing a ']'.");
	csh.SqBracketPairs.push_back((yyloc));
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = new AttributeList;
    chart.Error.Error(CHART_POS_START((yylsp[0])), "Missing ']'.");
  #endif
}
#line 5930 "xxx_csh_lang.cc"
    break;

  case 204: /* attrlist: attr  */
#line 3133 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
  #else
    (yyval.attributelist) = (new AttributeList)->Append((yyvsp[0].attribute));
  #endif
}
#line 5941 "xxx_csh_lang.cc"
    break;

  case 205: /* attrlist: attrlist TOK_COMMA attr  */
#line 3140 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[-1]), COLOR_COMMA);
    csh.CheckHintBetween((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    (yyval.attributelist) = ((yyvsp[-2].attributelist))->Append((yyvsp[0].attribute));
  #endif
}
#line 5954 "xxx_csh_lang.cc"
    break;

  case 206: /* attrlist: attrlist TOK_COMMA  */
#line 3149 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_COMMA);
    csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_NAME);
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing attribute or style name.");
  #else
    (yyval.attributelist) = (yyvsp[-1].attributelist);
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Expecting an attribute or style name here.");
  #endif
}
#line 5969 "xxx_csh_lang.cc"
    break;

  case 207: /* attr: alpha_string TOK_EQUAL color_string  */
#line 3162 "xxx_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), (yyvsp[0].str), (yyvsp[-2].multi_str).str);
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
  #else
    if ((yyvsp[-2].multi_str).had_error || (chart.SkipContent() && (yyvsp[-2].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 5994 "xxx_csh_lang.cc"
    break;

  case 208: /* attr: alpha_string TOK_EQUAL TOK_PLUS_PLUS color_string  */
#line 3183 "xxx_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-3]), (yyvsp[-3].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[-2]), COLOR_EQUAL);
    if (!(yyvsp[-3].multi_str).had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[-1])+(yylsp[0]), (string("++")+(yyvsp[0].str)).c_str(), (yyvsp[-3].multi_str).str);
    csh.CheckHintAt((yylsp[-3]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-3].multi_str).had_error)
        csh.CheckHintBetweenAndAt((yylsp[-2]), (yylsp[-1])+(yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-3].multi_str).str);
  #else
    if ((yyvsp[-3].multi_str).had_error || (chart.SkipContent() && (yyvsp[-3].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[-3].multi_str).str, string("++")+(yyvsp[0].str), CHART_POS((yylsp[-3])), CHART_POS2((yylsp[-1]),(yylsp[0])));
  #endif
    free((yyvsp[-3].multi_str).str);
    free((yyvsp[0].str));
}
#line 6019 "xxx_csh_lang.cc"
    break;

  case 209: /* attr: alpha_string TOK_EQUAL TOK_PLUS_PLUS  */
#line 3204 "xxx_lang.yy"
{
  //string=string
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
    csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrValue_CheckAndAddEscapeHint((yylsp[0]), "++", (yyvsp[-2].multi_str).str);
	csh.AddCSH_ErrorAfter((yylsp[0]), "Continue with a color name or definition.");
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
  #else
    if ((yyvsp[-2].multi_str).had_error || (chart.SkipContent() && (yyvsp[-2].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[-2].multi_str).str, "++", CHART_POS((yylsp[-2])), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
}
#line 6044 "xxx_csh_lang.cc"
    break;

  case 210: /* attr: alpha_string TOK_EQUAL TOK_NUMBER  */
#line 3225 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-2].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-2]), (yyvsp[-2].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[-1]), COLOR_EQUAL);
    csh.AddCSH((yylsp[0]), COLOR_ATTRVALUE);
    csh.CheckHintAt((yylsp[-2]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-2].multi_str).had_error)
        csh.CheckHintBetweenAndAt((yylsp[-1]), (yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-2].multi_str).str);
  #else
    if ((yyvsp[-2].multi_str).had_error || (chart.SkipContent() && (yyvsp[-2].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[-2].multi_str).str, (yyvsp[0].str), CHART_POS((yyloc)), CHART_POS((yylsp[0])));
  #endif
    free((yyvsp[-2].multi_str).str);
    free((yyvsp[0].str));
}
#line 6067 "xxx_csh_lang.cc"
    break;

  case 211: /* attr: alpha_string TOK_EQUAL  */
#line 3244 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[-1].multi_str).had_error)
        csh.AddCSH_AttrName((yylsp[-1]), (yyvsp[-1].multi_str).str, COLOR_ATTRNAME);
    csh.AddCSH((yylsp[0]), COLOR_EQUAL);
    csh.CheckHintAt((yylsp[-1]), EHintSourceType::ATTR_NAME);
    if (!(yyvsp[-1].multi_str).had_error)
        csh.CheckHintAfter((yylsp[0]), EHintSourceType::ATTR_VALUE, (yyvsp[-1].multi_str).str);
  #else
    if ((yyvsp[-1].multi_str).had_error || (chart.SkipContent() && (yyvsp[-1].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[-1].multi_str).str, {}, CHART_POS((yyloc)), CHART_POS((yyloc)));
  #endif
    free((yyvsp[-1].multi_str).str);
}
#line 6088 "xxx_csh_lang.cc"
    break;

  case 212: /* attr: string  */
#line 3261 "xxx_lang.yy"
{
  //here we accept non alpha strings for "->" and similar style names
  #ifdef C_S_H_IS_COMPILED
    if (!(yyvsp[0].multi_str).had_error)
        csh.AddCSH_StyleOrAttrName((yylsp[0]), (yyvsp[0].multi_str).str);
    csh.CheckHintAt((yylsp[0]), EHintSourceType::ATTR_NAME);
  #else
    if ((yyvsp[0].multi_str).had_error || (chart.SkipContent() && (yyvsp[0].multi_str).had_param))
        (yyval.attribute) = nullptr;
    else
        (yyval.attribute) = new Attribute((yyvsp[0].multi_str).str, CHART_POS((yyloc)));
  #endif
    free((yyvsp[0].multi_str).str);
}
#line 6107 "xxx_csh_lang.cc"
    break;

  case 225: /* symbol_string: TOK__NEVER__HAPPENS  */
#line 3303 "xxx_lang.yy"
{
    (yyval.str) = nullptr;
}
#line 6115 "xxx_csh_lang.cc"
    break;

  case 227: /* entity_string_single: TOK_QSTRING  */
#line 3310 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
	csh.AddQuotedString((yylsp[0]));
  #endif
  (yyval.str) = (yyvsp[0].str);
}
#line 6126 "xxx_csh_lang.cc"
    break;

  case 228: /* entity_string_single: TOK_SHAPE_COMMAND  */
#line 3317 "xxx_lang.yy"
{
	(yyval.str) = (char*)malloc(2);
	((yyval.str))[0] = ShapeElement::act_code[(yyvsp[0].shapecommand)];
	((yyval.str))[1] = 0;
}
#line 6136 "xxx_csh_lang.cc"
    break;

  case 231: /* tok_param_name_as_multi: TOK_PARAM_NAME  */
#line 3328 "xxx_lang.yy"
{
    (yyval.multi_str).str = nullptr;
    (yyval.multi_str).multi = true;
    (yyval.multi_str).had_param = true;
    (yyval.multi_str).had_error = false;
  #ifdef C_S_H_IS_COMPILED
    if ((yyvsp[0].str)==nullptr || (yyvsp[0].str)[0]!='$' || (yyvsp[0].str)[1]==0)
        csh.AddCSH_Error((yylsp[0]), "Need name after the '$' sign.");
  #else
    if ((yyvsp[0].str)==nullptr || (yyvsp[0].str)[0]!='$' || (yyvsp[0].str)[1]==0) {
        chart.Error.Error(CHART_POS_START((yylsp[0])), "Need name after the '$' sign.");
        (yyval.multi_str).had_error = true;
    } else if (!chart.SkipContent()) {
        //When parsing a procedure we we accept all params
        //as they may be variables defined later
        auto p = chart.GetParameter((yyvsp[0].str));
        if (p==nullptr) {
            chart.Error.Error(CHART_POS_START((yylsp[0])), "Undefined parameter or variable name.");
            (yyval.multi_str).had_error = true;
        } else {
            (yyval.multi_str).str = strdup(StringFormat::PushPosEscapes(p->value.c_str(), CHART_POS_START((yylsp[0]))).c_str());
        }
    }
  #endif
  //avoid returning null
  if ((yyval.multi_str).str==nullptr)
        (yyval.multi_str).str = strdup("");
}
#line 6169 "xxx_csh_lang.cc"
    break;

  case 232: /* multi_string_continuation: TOK_TILDE  */
#line 3360 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH_ErrorAfter((yylsp[0]), "Missing string to concatenate after '~'.");
  #else
    chart.Error.Error(CHART_POS_AFTER((yylsp[0])), "Missing string to concatenate after '~'.");
  #endif
    (yyval.multi_str).str = strdup("");
    (yyval.multi_str).multi = true;
    (yyval.multi_str).had_param = false;
    (yyval.multi_str).had_error = true;
}
#line 6185 "xxx_csh_lang.cc"
    break;

  case 233: /* multi_string_continuation: TOK_TILDE string  */
#line 3372 "xxx_lang.yy"
{
    (yyval.multi_str) = (yyvsp[0].multi_str);
}
#line 6193 "xxx_csh_lang.cc"
    break;

  case 234: /* entity_string_single_or_param: entity_string_single  */
#line 3377 "xxx_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 6201 "xxx_csh_lang.cc"
    break;

  case 236: /* entity_string: entity_string_single_or_param  */
#line 3383 "xxx_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].multi_str));
}
#line 6209 "xxx_csh_lang.cc"
    break;

  case 237: /* entity_string: entity_string_single_or_param multi_string_continuation  */
#line 3387 "xxx_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].multi_str), (yyvsp[0].multi_str));
}
#line 6217 "xxx_csh_lang.cc"
    break;

  case 239: /* alpha_string: alpha_string_single  */
#line 3393 "xxx_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 6225 "xxx_csh_lang.cc"
    break;

  case 240: /* alpha_string: alpha_string_single multi_string_continuation  */
#line 3397 "xxx_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), (yyvsp[0].multi_str));
}
#line 6233 "xxx_csh_lang.cc"
    break;

  case 242: /* string: string_single  */
#line 3403 "xxx_lang.yy"
{
    (yyval.multi_str) = multi_segment_string((yyvsp[0].str));
}
#line 6241 "xxx_csh_lang.cc"
    break;

  case 243: /* string: string_single multi_string_continuation  */
#line 3407 "xxx_lang.yy"
{
    (yyval.multi_str).CombineThemToMe((yyvsp[-1].str), (yyvsp[0].multi_str));
}
#line 6249 "xxx_csh_lang.cc"
    break;

  case 244: /* scope_open: TOK_OCBRACKET  */
#line 3413 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
    csh.PushContext();
    if (csh.CheckHintAfter((yylsp[0]), EHintSourceType::LINE_START)) {
        csh.AddLineBeginToHints();
        csh.hintStatus = HINT_READY;
    }
  #else
    if (YYGET_EXTRA(yyscanner)->open_context_mode == base_parse_parm::EScopeOpenMode::PROC_REPLAY) {
        //Open a scope to replay a procedure. The text of the procedure has
        //already been placed to the lex buffer stack (in fact the '{' already comes
        //from there), and the parameters are in YYEXTRA.
        YYGET_EXTRA(yyscanner)->open_context_mode = base_parse_parm::EScopeOpenMode::NORMAL;
        chart.PushContext(CHART_POS_START((yylsp[0])), EContextParse::REPARSING);
        chart.MyCurrentContext().starts_procedure = true;
        chart.MyCurrentContext().parameters = std::move(YYGET_EXTRA(yyscanner)->last_procedure_params);
        chart.MyCurrentContext().export_colors = YYGET_EXTRA(yyscanner)->last_procedure->export_colors;
        chart.MyCurrentContext().export_styles = YYGET_EXTRA(yyscanner)->last_procedure->export_styles;
        YYGET_EXTRA(yyscanner)->last_procedure = nullptr;
    } else {
        //Just open a regular scope
        chart.PushContext(CHART_POS_START((yylsp[0])));
    }
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 6281 "xxx_csh_lang.cc"
    break;

  case 245: /* scope_close: TOK_CCBRACKET  */
#line 3442 "xxx_lang.yy"
{
  #ifdef C_S_H_IS_COMPILED
    (yyval.instruction_list) = nullptr;
    csh.PopContext();
    csh.AddCSH((yylsp[0]), COLOR_BRACE);
  #else
    (yyval.instruction_list) = chart.PopContext().release();
  #endif
    (yyvsp[0].input_text_ptr); //suppress
}
#line 6296 "xxx_csh_lang.cc"
    break;


#line 6300 "xxx_csh_lang.cc"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, RESULT, yyscanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= TOK_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == TOK_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, RESULT, yyscanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, RESULT, yyscanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, RESULT, yyscanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, RESULT, yyscanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 3454 "xxx_lang.yy"



/* END OF FILE */
