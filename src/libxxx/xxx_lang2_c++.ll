%option reentrant noyywrap nounput
%option bison-bridge bison-locations

%{
    /*
        This file is part of Msc-generator.
        Copyright (C) 2008-2021 Zoltan Turanyi
        Distributed under GNU Affero General Public License.

        Msc-generator is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        Msc-generator is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
        */

#include <stdio.h>
#include <string.h>
#include <iostream>

#ifndef HAVE_UNISTD_H
#define YY_NO_UNISTD_H
extern int isatty(int);
#endif

#include "cgen_shapes.h"
#ifdef COLOR_SYNTAX_HIGHLIGHT
    #undef COLOR_SYNTAX_HIGHLIGHT
    #define C_S_H (1)
    #define C_S_H_IS_COMPILED
    #include "xxxcsh.h"
    #define YYMSC_RESULT_TYPE xxx::XxxCsh
    #define RESULT csh
    #define YYLTYPE_IS_DECLARED  //If we scan for color syntax highlight use this location type (YYLTYPE)
    #define YYLTYPE CshPos
    #define YYGET_EXTRA xxxcsh_get_extra
    #define CHAR_IF_CSH(A) char
    #include "xxx_csh_lang.h"   //Must be after language_misc.h
    #include "xxx_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined
#else
    #define C_S_H (0)
    #include "xxxchart.h"
    #define YYMSC_RESULT_TYPE xxx::XxxChart
    #define RESULT chart
    #define YYGET_EXTRA xxx_get_extra
    #define CHAR_IF_CSH(A) A
    #include "parse_tools.h"
    using namespace xxx;
    #include "xxx_lang.h"
    #include "xxx_lang_misc.h"  //Must be after RESULT and YYMSC_RESULT_TYPE is defined


    YY_DECL;

    //this function must be in the lex file, so that lex replaces the "yy" in yy_create_buffer and
    //yypush_buffer_state to the appropriate chart-specific prefix.
    void XxxPushFlex(base_parse_parm &pp, const char *text, size_t len, YYLTYPE *old_pos, const FileLineCol &new_pos, EInclusionReason reason)
    {
        if (text==nullptr || len==0) return;
        pp.buffs.emplace_back(text, len);
        pp.pos_stack.line = old_pos->first_line;
        pp.pos_stack.col = old_pos->first_column-1; //not sure why -1 is needed
        pp.pos_stack.Push(new_pos);
        pp.pos_stack.reason = reason;
        yypush_buffer_state(yy_create_buffer( nullptr, YY_BUF_SIZE, pp.yyscanner ), pp.yyscanner);
    }
#endif

%}

%%

 /* Newline characters in all forms accepted */
\x0d\x0a     %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

\x0a         %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

\x0d         %{
  #ifndef C_S_H_IS_COMPILED
    language_jump_line(yylloc);
  #endif
%}

 /* # starts a comment last until end of line */
#[^\x0d\x0a]* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #endif
%}

 /* // starts a comment last until end of line */
\/\/[^\x0d\x0a]* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #endif
%}

 /* / * .. * / block comments*/
\/\*([^\*]*\*)*\/ %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
  #else
    language_process_block_comments(yytext, yylloc);
  #endif
%}

 /* / * ... unclosed block comments */
\/\* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH(*yylloc, COLOR_COMMENT);
    yyget_extra(yyscanner)->csh->AddCSH_Error(*yylloc, "Unpaired beginning of block comment '/" "*'.");
  #else
    yyget_extra(yyscanner)->chart->Error.Error(CHART_POS_START(*yylloc),
         "Unpaired beginning of block comment '/" "*'.");
  #endif
%}

[ \t]+    /* ignore whitespace */;

 /* These shape definition keywords are case sensitive */
M	return yyns::parser::make_TOK_SHAPE_COMMAND(ShapeElement::MOVE_TO, loc);
L	return yyns::parser::make_TOK_SHAPE_COMMAND(ShapeElement::LINE_TO, loc);
C	return yyns::parser::make_TOK_SHAPE_COMMAND(ShapeElement::CURVE_TO, loc);
E	return yyns::parser::make_TOK_SHAPE_COMMAND(ShapeElement::CLOSE_PATH, loc);
S	return yyns::parser::make_TOK_SHAPE_COMMAND(ShapeElement::SECTION_BG, loc);
T	return yyns::parser::make_TOK_SHAPE_COMMAND(ShapeElement::TEXT_AREA, loc);
H	return yyns::parser::make_TOK_SHAPE_COMMAND(ShapeElement::HINT_AREA, loc);
P	return yyns::parser::make_TOK_SHAPE_COMMAND(ShapeElement::PORT, loc);


 /* These keywords are case insensitive */
(?i:xxx)       return yyns::parser::make_TOK_XXX(yytext, loc);
(?i:bye)       return yyns::parser::make_TOK_BYE(yytext, loc);
(?i:defshape)  return yyns::parser::make_TOK_COMMAND_DEFSHAPE(yytext, loc);
(?i:defcolor)  return yyns::parser::make_TOK_COMMAND_DEFCOLOR(yytext, loc);
(?i:defstyle)  return yyns::parser::make_TOK_COMMAND_DEFSTYLE(yytext, loc);
(?i:defdesign) return yyns::parser::make_TOK_COMMAND_DEFDESIGN(yytext, loc);
(?i:defproc)   return yyns::parser::make_TOK_COMMAND_DEFPROC(yytext, loc);
(?i:replay)    return yyns::parser::make_TOK_COMMAND_REPLAY(yytext, loc);
(?i:set)       return yyns::parser::make_TOK_COMMAND_SET(yytext, loc); 
(?i:include)   return yyns::parser::make_TOK_COMMAND_INCLUDE(yytext, loc);
(?i:if)        return yyns::parser::make_TOK_IF(yytext, loc);
(?i:then)      return yyns::parser::make_TOK_THEN(yytext, loc);
(?i:else)      return yyns::parser::make_TOK_ELSE(yytext, loc);


=        return yyns::parser::make_TOK_EQUAL(loc);
,        return yyns::parser::make_TOK_COMMA(loc);
\(loc);       return yyns::parser::make_TOK_SEMICOLON(loc);
\[       return yyns::parser::make_TOK_OSBRACKET(loc);
\]       return yyns::parser::make_TOK_CSBRACKET(loc);
\+\+     return yyns::parser::make_TOK_PLUS_PLUS(loc);
\~       return yyns::parser::make_TOK_TILDE(loc);
\(       return yyns::parser::make_TOK_OPARENTHESIS(loc);
\)       return yyns::parser::make_TOK_CPARENTHESIS(loc);
\{       %{
  #ifdef C_S_H_IS_COMPILED
    return yyns::parser::make_TOK_OCBRACKET(nullptr,  loc);
  #else
    return yyns::parser::make_TOK_OCBRACKET(yyget_extra(yyscanner)->buffs.back().GetInputPosFrom_yytext(yytext), loc);
  #endif
%}

\}       %{
  #ifdef C_S_H_IS_COMPILED
    return yyns::parser::make_TOK_CCBRACKET(nullptr, loc);
  #else
    return yyns::parser::make_TOK_CCBRACKET(yyget_extra(yyscanner)->buffs.back().GetInputPosFrom_yytext(yytext), loc);
  #endif
%}

\<     return yyns::parser::make_TOK_COMP_OP(ECompareOperator::SMALLER, loc);          
\<=    return yyns::parser::make_TOK_COMP_OP(ECompareOperator::SMALLER_OR_EQUAL, loc); 
==     return yyns::parser::make_TOK_COMP_OP(ECompareOperator::EQUAL, loc);            
\<\>   return yyns::parser::make_TOK_COMP_OP(ECompareOperator::NOT_EQUAL, loc);        
=\>    return yyns::parser::make_TOK_COMP_OP(ECompareOperator::GREATER_OR_EQUAL, loc); 
\>     return yyns::parser::make_TOK_COMP_OP(ECompareOperator::GREATER, loc);          


 /* This is a colon-quoted string, finished by a quotation mark (UTF-8 allowed)
 ** : "<string>"
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*\" %{
  #ifdef C_S_H_IS_COMPILED
    return yyns::parser::make_TOK_COLON_QUOTED_STRING(yytext, loc);
  #else
    {
    /* after whitespaces we are guaranteed to have a tailing and heading quot */
    char *s = remove_head_tail_whitespace(yytext+1);
    /* s now points to the heading quotation marks.
    ** Now get rid of both quotation marks */
    std::string_view str(s+1);
    str.remove_suffix(1);
    /* Calculate the position of the string and prepend a location escape */
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s+1 - yytext));
    return yyns::parser::make_TOK_COLON_QUOTED_STRING(pos.Print() + str, loc);
    }
  #endif
%}

 /* This is a colon-quoted string, finished by a newline (trailing context) (UTF-8 allowed)
 ** : "<string>$
 ** <string> can contain escaped quotation marks, hashmarks, but no line breaks
 */
\:[ \t]*\"([^\"\x0d\x0a]|(\\\"))*  %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
    return yyns::parser::make_TOK_COLON_QUOTED_STRING(yytext, loc);
  #else
    {
    /* after whitespaces we are guaranteed to have a heading quot */
    const char *s = remove_head_tail_whitespace(yytext+1);
    // s now points to heading quotation mark
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column + unsigned(s - yytext));
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    /* Advance pos beyond the leading quotation mark */
    pos.col++;
    return yyns::parser::make_TOK_COLON_QUOTED_STRING(pos.Print() + (s+1), loc);
    }
  #endif
%}

 /* This is a non quoted colon-string
 ** : <string>
 ** terminated by any of: [ { or ;
 ** Honors escaping of the above via a backslash. (UTF-8 allowed)
 ** Can contain quotation marks (escaped or unescaped), but can not start with it
 ** If it contains a hashmark, unescaped [ { or ; is allowed till the end of the line
 ** (representing a commented section inside a label)
 ** Not available in mscgen compatibility mode. There we use the one below
 *  \:[\t]*(((#[^\x0d\x0a]*)|[^\"\;\[\{\\]|(\\.))((#[^\x0d\x0a]*)|[^\;\[\{\\]|(\\.))*(\\)?|\\)
 * \:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*
 */
<INITIAL>\:[ \t]*((#[^\x0d\x0a]*|[^\"\;\{\[\\#\ \t]|(\\[^\x0d\x0a])))((#[^\x0d\x0a]*|[^\;\{\[\\#]|(\\[^\x0d\x0a])))*\\? %{
  #ifdef C_S_H_IS_COMPILED
    return yyns::parser::make_TOK_COLON_STRING(yytext, loc);
  #else
    return yyns::parser::make_TOK_COLON_STRING(cppprocess_colon_string(yytext, CHART_POS_START(*yylloc), yylloc), loc);
  #endif
%}

 /* This is a degenerate non quoted colon-string
 ** a colon followed by a solo escape or just a colon
 */
\:[ \t]*\\? %{
  #ifdef C_S_H_IS_COMPILED
    return yyns::parser::make_TOK_COLON_STRING(yytext, loc);
   #else
    return yyns::parser::make_TOK_COLON_STRING(cppprocess_colon_string(yytext, CHART_POS_START(*yylloc), yylloc), loc);
  #endif
%}


 /* A simple quoted string, that can have escaped quotation marks inside. (UTF-8 allowed)*/
\"([^\"\x0d\x0a]|(\\\"))*\" %{
    {
    std::string_view str = yytext+1;
    str.remove_suffix(1);
    return yyns::parser::make_TOK_QSTRING(str, loc);
    }
%}

 /* A simple quoted string, missing a closing quotation mark (UTF-8 allowed)*/
\"([^\"\x0d\x0a]|(\\\"))* %{
  #ifdef C_S_H_IS_COMPILED
    yyget_extra(yyscanner)->csh->AddCSH_ErrorAfter(*yylloc, "Missing closing quotation mark.");
#else
    {
    yylval_param->str = strdup(yytext+1);
    FileLineCol pos(yyget_extra(yyscanner)->chart->current_file,
                 yylloc->first_line, yylloc->first_column);
    yyget_extra(yyscanner)->chart->Error.Error(pos,
         "This opening quotation mark misses its closing pair. "
         "Assuming string termination at line-end.",
         "Quoted strings cannot have line breaks. Use \'\\n\' to insert a line break.");
    }
  #endif
    return yyns::parser::make_TOK_QSTRING(yytext+1, loc);
%}

 /* Numbers */
[+\-]?[0-9]+\.?[0-9]*  %{
    return yyns::parser::make_TOK_NUMBER(yytext, loc);
%}

 /* Strings not ending with a dot. We allow any non ASCII UTF-8 character through. */
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])* %{
    return yyns::parser::make_TOK_STRING(yytext, loc);
%}

 /* Strings ending with a dot, not followed by a second dot .
  * We allow any non ASCII UTF-8 character through.
  * Two dots one after another shall be parsed a '..' into TOK_EMPH*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*\./[^\.] %{
    return yyns::parser::make_TOK_STRING(yytext, loc);
%}

 /* Color definitions. We allow any non ASCII UTF-8 character in the color name. */
 /* string+-number[,number]*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*(\.)?[\+\-][0-9]+(\.[0-9]*)?(\,[0-9]+(\.[0-9]*)?)? %{
    return yyns::parser::make_TOK_COLORDEF(yytext, loc);
%}

 /* string,number[+-number]. We allow any non ASCII UTF-8 character in 'string'.*/
[A-Za-z_\x80-\xff]([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])*(\.)?\,[0-9]+(\.[0-9]*)?([\+\-][0-9]+(\.[0-9]*)?)? %{
    yylval_param->str = strdup(yytext);
    return yyns::parser::make_TOK_COLORDEF(yytext, loc);
%}

 /* number,number,number[,number] */
[0-9]+(\.[0-9]*)?\,[0-9]+(\.[0-9]*)?\,[0-9]+(\.[0-9]*)?(\,[0-9]+(\.[0-9]*)?)? %{
    yylval_param->str = strdup(yytext);
    return yyns::parser::make_TOK_COLORDEF(yytext, loc);
%}

 /* Parameter names: $ followed by a string not ending with a dot. We allow any non ASCII UTF-8 character through.
  * A single '$" also matches.*/
\$[A-Za-z_\x80-\xff]?([A-Za-z0-9_\x80-\xff\.]?[A-Za-z0-9_\x80-\xff])* %{
    return yyns::parser::make_TOK_PARAM_NAME(yytext, loc);
%}

 /* Parameter names: $$ representing a value unique to the actual invocation.*/
($$) %{
    return yyns::parser::make_TOK_PARAM_NAME(yytext, loc);
%}

<<EOF>> %{
  #ifdef C_S_H_IS_COMPILED
    return yyns::parser::make_TOK_EOF(loc);
  #else
    {
    auto &pp = *YYGET_EXTRA(yyscanner);
    if (pp.buffs.size()==0) {
        yyterminate();
        return yyns::parser::make_TOK_EOF(loc);
    }
    pp.buffs.pop_back();
    if (pp.buffs.size()==0) {
        yyterminate();
        return yyns::parser::make_TOK_EOF(loc);
    }
    //else just continue scanning the buffer above
    yypop_buffer_state(yyscanner);
    pp.pos_stack.Pop();
    yylloc->last_line = pp.pos_stack.line;
    yylloc->last_column = pp.pos_stack.col-1;
    }
  #endif
%}

. return     return yyns::parser::make_TOK_UNRECOGNIZED_CHAR(loc);

%%

/* END OF FILE */
