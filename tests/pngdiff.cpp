#include "cgen_color.h"
#include <cairo.h>
#include <iostream>
#include <filesystem>
// Compare two pngs, save the diff
// Return
//  3: file error
//  2: diff size (we also create diff file named '*_diff*')
//  1: diff (we also create diff file named '*_diff*')
//  0: same
int main(int argc, char** argv) {
    if (argc != 3) {
        std::cerr << "Usage: " << std::filesystem::path(argv[0]).filename() << " new.png canonical.png" << '\n';
        return 3;
    }
    // std::cerr << "doing " << argv[1] << '\n';
    auto s1 = cairo_image_surface_create_from_png(argv[1]),
         s2 = cairo_image_surface_create_from_png(argv[2]);
    if (cairo_surface_status(s1) != CAIRO_STATUS_SUCCESS || cairo_surface_status(s2) != CAIRO_STATUS_SUCCESS)
        return 3;
    // std::cerr << "succ reading\n";
    const auto w1 = cairo_image_surface_get_width(s1), h1 = cairo_image_surface_get_height(s1),
               w2 = cairo_image_surface_get_width(s2), h2 = cairo_image_surface_get_height(s2);
    auto f1 = cairo_image_surface_get_format(s1);
    _ASSERT(CAIRO_FORMAT_RGB24 == f1 || CAIRO_FORMAT_ARGB32 == f1); //we ignore 'a' cannel anyway
    cairo_surface_flush(s1);
    cairo_surface_flush(s2);
    auto p1 = reinterpret_cast<uint32_t*>(cairo_image_surface_get_data(s1));
    auto p2 = reinterpret_cast<uint32_t*>(cairo_image_surface_get_data(s2));
    auto diff = [&] {
        bool ret = false;
        for (auto u = 0u; u < std::min(h1, h2); ++u)
            for (auto v = 0u; v < std::min(w1, w2); ++v)
                if ((p1[u*w1+v] & 0xffffff) != (p2[u*w2+v] & 0xffffff)) {
                    p1[u*w1+v] = 0x00ff0000; //red
                    ret = true;
                } else
                    p1[u*w1+v] = ColorType(p1[u*w1+v]).Lighter(0.85).ConvertToUnsigned(); //quite transparent
        return ret;
    }();
    if (diff) {
        cairo_surface_mark_dirty(s1);
        // I'm lazy for a regex
        std::string df = argv[1];
        auto dot = df.rfind('.');
        if (dot == df.npos)
            dot = df.size();
        // df = df.substr(0, dot) + "_diff" + df.substr(dot);
        df.insert(dot, "_diff");
        // std::cerr << "df " << df << '\n';
        cairo_surface_write_to_png(s1, df.c_str());
    }
    cairo_surface_destroy(s1);
    cairo_surface_destroy(s2);
    if (w1 != w2 || h1 != h2)
        return 2;
    return diff > 0;
}
