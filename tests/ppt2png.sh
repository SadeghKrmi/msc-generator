#!/bin/bash -e
# LOFFICE=... ppt2png.sh some/path/to/orig.pptx new-filename.png
D=tmp-lo-$RANDOM
until E=$($LOFFICE "-env:UserInstallation=file:///$PWD/$D" --convert-to png --outdir $D "$1" 2>&1); do :; done
echo $E |\
 grep -v 'convert .* using filter : impress_png_Export$' |\
 awk -vx="$1" '{print x ":", $0}' \
 >&2
F="`basename ${1%.*}`.png"
[[ -f "$D/$F" ]] && mv "$D/$F" "$2" || touch "$2"
rm -rf $D
