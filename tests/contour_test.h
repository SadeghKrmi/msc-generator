/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2021 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/


/** @file contour_test.h Declares contour_test().
 * @ingroup contour_files
 */

#ifndef CONTOUR_TEST_H
#define CONTOUR_TEST_H

#include "contour.h"
#include "cgen_arrowhead.h"

class CairoContext {
    FILE* outFile = nullptr;
    cairo_surface_t* surface = nullptr;
    const double x;
    const Block p;
public:
    static std::string font_face;
    cairo_t* cr = nullptr;
    CairoContext(unsigned i, const Block& place, const char* text = nullptr, bool two = true, int sub = -1);
    ~CairoContext();
    void Draw(const Contour& area, bool shifted, double r, double g, double b, bool fill, double center = 5, int vertices = 0, const std::map<size_t, XY>& cps = std::map<size_t, XY>());
    void Draw1(const Contour& area, bool other = false, double center = 5) { Draw(area, false, other ? 1 : 0, other ? 0 : 1, 0, true, center); }
    void Draw2(const Contour& area, double center = 5) { Draw(area, true, 0, 0, 1, true, center); }
    void ClipInverse(const Contour& clip);
};

void Draw(unsigned i, const Contour &area1, const Contour& area2, const Contour& area3, const char *text = nullptr, double center = 5);
void Draw(unsigned i, const Contour &area1, const Contour& area2, const char *text = nullptr, double center = 5);
void Draw(unsigned i, const Contour &area1, const char *text=nullptr, double center = 5);
void Draw(unsigned i, std::vector<Contour>&& v, double scale = 1, const char* text = nullptr);
void DrawPaths(unsigned num, const std::list<std::pair<Path, ColorType>>& list, const char* msg = nullptr);

bool shouldnt_do(int i) noexcept;
bool shouldnt_do(int i, int j) noexcept;
bool shall_draw() noexcept;

void contour_test(int max_threads);
void contour_test_experiment();
void contour_unit_tests(bool do_image);

#endif // !CONTOUR_TEST_H
