defproc procedure {
x {a,b,c;};
y {d,e,f;};
z {g,h,i;};
x0 {j,k,l;};
x1 {m, n, o;};

hscale=auto;
compress=yes;
defstyle arrow [arrow.endtype=empty, arrow.skiptype=empty_dot];
{
    #test joins and curly joins in a series. parallel works
    parallel a->:a;
    join box c--c:aaa;
    join ->;
    join box d--d:bbb;
    join ->f;
    join f->b:a;
    join b->c;
    join c->d->|;
    hspace f-g:beside;
    box g--g:beside;

    a->:a;
    join box c--c:aaa;
    join ->;
    join box d--d:bbb;
    join ->f;
    join f->b:a;
    join b->c;
    join c->d->|;
    join d<-|;
    hspace f-g:beside;
    box g--g: not\nbeside;  #because no 'parallel' before the join series

    #testing self arrows in a join series
    a->b;
    join b->b:aaaa
              bbb;
    b->c;
    join c->c:aa;
} {

    #testing ->| and width calculation
    defstyle arrow [arrow.size=huge, arrow.skiptype=none];
    {
        i->|;
        i->| end after i 10;
        i->| end after i 20;
        i->| start before i;
        i->j start before i 10;
        -> start before i end after i;
        i--j;
        i<-|;
        i<-| end after i 10;
        i<-| end after i 20;
        i<-| start before i;
        i<-j start before j 10;
        <- start before i end after i;
    } {
        m->|:aa;
        m->| end after m 10:aa;
        m->| end after m 20:aa;
        m->| start before m:aa;
        m->n start before m 10:aa;
        -> start before m end after m:aaaaaaaa;
    };
};


a->:a;
join box c--c:aaa;
join ->;
join box d--d:bbb;
join ->f;
join f->b:a;
join b->c;
join c->|;

b->;
join box g--i:boox;
join ->;
join box j--m:box;


x2 {a1,b1,c1;};
y2 {d1,e1,f1;};
z2 {g1,h1,i1;};

#these show joining abd special endings for block arrows
parallel block a1->:a;
join box c1--c1:aaa;
join block ->;
join box d1--d1:bbb;
join block ->f1;
join f1->b1:a;
join b1->c1;
join block c1->d1->|;
join d1<-|;

#These show auto-scaling for block arrows
block e1->f1->g1:\plaaaa;
block e1->f1->g1-i1:aaaaaaaa [text.ident=right];
};
replay procedure;

