/*
    This file is part of Msc-generator.
    Copyright (C) 2008-2022 Zoltan Turanyi
    Distributed under GNU Affero General Public License.

    Msc-generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Msc-generator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Msc-generator.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file contour_test.cpp Defines contour_test().
 * @ingroup contour_files
 */
#define _CRT_SECURE_NO_DEPRECATE //For visual studio to allow sscanf
#define _CRT_NONSTDC_NO_DEPRECATE
#include <string>
#include <future>
#include <algorithm>
#include <iostream>
#include "contour_test.h"
#include "canvas.h"

using namespace contour;

cairo_status_t write_func4test(void * closure, const unsigned char *data, unsigned length)
{
    if (closure==nullptr) return CAIRO_STATUS_SUCCESS;
    if (length == fwrite(data, 1, length, (FILE*)closure))
        return CAIRO_STATUS_SUCCESS;
    else
        return CAIRO_STATUS_WRITE_ERROR;
}

constexpr double expand_by = 20;
constexpr double size_by = 8;

std::string CairoContext::font_face;

CairoContext::CairoContext(unsigned i, const Block &pl, const char *text, bool two, int sub) :
    x(two?pl.x.Spans()+10:0), p(pl)
{
    if (shouldnt_do(i)) return;
    Block place(pl);
    place.Expand(expand_by);
    char fileName[40];
    if (sub<0 && i>999) {
        if (i<=9999) {sub = i%10; i/=10;}
        else if (i<=99999) {sub = i%100; i/=100;}
        else if (i<=999999) {sub = i%1000; i/=1000;}
        else if (i<=9999999) {sub = i%10000; i/=10000;}
        else {_ASSERT(0);}
    }
    if (sub>=0)
        snprintf(fileName, sizeof(fileName), "test%u_%03u.png", i, sub);
    else
        snprintf(fileName, sizeof(fileName), "test%u.png", i);
    printf("%s\n", fileName);
    surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, int(two?size_by*2*place.x.Spans():size_by*place.x.Spans()), int(size_by*place.y.Spans()));
    auto st = cairo_surface_status(surface);
    if (st != CAIRO_STATUS_SUCCESS) {
        fprintf(stderr, "Cannot create surface for %s: %s [%d]\n", fileName, cairo_status_to_string(st), int(st));
        goto error;
    }
    cr = cairo_create(surface);
    st = cairo_status(cr);
    if (st != CAIRO_STATUS_SUCCESS) {
        fprintf(stderr, "Cannot create context for %s: %s [%d]\n", fileName, cairo_status_to_string(st), int(st));
        goto error;
    }
    if (font_face.size())
        cairo_select_font_face(cr, font_face.c_str(), CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_rectangle(cr, 0, 0, int(2*size_by*place.x.Spans()), int(size_by*place.y.Spans()));
    cairo_fill(cr);
    if (text) {
        cairo_set_source_rgb(cr, 0,0,0);
        cairo_move_to(cr, 5,15);
        thread_safe_cairo_show_text(cr, text);
    }
    cairo_translate(cr, 15, 15);
    cairo_scale(cr, size_by, size_by);
    cairo_translate(cr, -place.x.from, -place.y.from);
    outFile = fopen(fileName, "wb");
    if (outFile) return;
    fprintf(stderr, "Cannot open %s.\n", fileName);
error:
    if (cr) cairo_destroy(cr);
    if (surface) cairo_surface_destroy(surface);
    outFile=nullptr;
    surface=nullptr;
    cr=nullptr;
}

CairoContext::~CairoContext()
{
    if (cr) cairo_destroy(cr);
    if (outFile && surface)
        cairo_surface_write_to_png_stream (surface, write_func4test, outFile);
    if (surface) cairo_surface_destroy(surface);
    if (outFile)
        fclose(outFile);
};

void CairoContext::ClipInverse(const Contour &area)
{
    if (!cr) return;
    cairo_save(cr);
    Block outer(p);
    outer += area.GetBoundingBox();
    outer.Expand(1);
    cairo_rectangle(cr, outer.x.from, outer.y.from, outer.x.Spans(), outer.y.Spans());
    cairo_new_sub_path(cr);
    area.CairoPath(cr, true);
    cairo_fill_rule_t old = cairo_get_fill_rule(cr);
    cairo_set_fill_rule(cr, CAIRO_FILL_RULE_EVEN_ODD);
    cairo_clip(cr);
    cairo_set_fill_rule(cr, old);
}

void CairoContext::Draw(const Contour& area, bool shifted, double r, double g, double b, bool fill,
                        double center, int vertices, const std::map<size_t, XY> &cps)
{
    if (!cr) return;
    if (shifted)
        cairo_translate(cr, x, 0);
    if (fill) {
        cairo_set_source_rgba(cr, r, g, b, 0.2);
        area.Fill(cr);
    }
    cairo_set_source_rgb(cr, r, g, b);
    area.Line2(cr);
    if (center) {
        const XY c = area.Centroid();
        cairo_arc(cr, c.x, c.y, center, 0, 2*M_PI);
        cairo_fill(cr);
    }
    cairo_set_font_size(cr, 1);
    if (vertices) {
        for (unsigned u = 0; u<area.front().Outline().size(); u++) {
            cairo_new_sub_path(cr);
            cairo_arc(cr, area.front().Outline()[u].GetStart().x, area.front().Outline()[u].GetStart().y, 1, 0, 2*M_PI);
            cairo_close_path(cr);
        }
        cairo_stroke(cr);
        cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
        if (vertices==2) {
            cairo_set_font_size(cr, 1);
        }
        for (auto &e: cps) {
            cairo_new_sub_path(cr);
            cairo_arc(cr, e.second.x, e.second.y, 1, 0, 2*M_PI);
            cairo_close_path(cr);
            cairo_stroke(cr);
            cairo_move_to(cr, e.second.x+1, e.second.y+1);
            char buff[100];
            sprintf(buff, "(%u): %lg; %lg", (unsigned)e.first, e.second.x, e.second.y);
            thread_safe_cairo_show_text(cr, buff);
        }
    }
    if (shifted)
        cairo_translate(cr, -x, 0);
}


void Draw(unsigned i, const Contour &area1, const Contour &area2, const Contour &area3, const char *text, double center)
{
    if (shouldnt_do(i)) return;
    Block b = area1.GetBoundingBox();
    b += area2.GetBoundingBox();
    b += area3.GetBoundingBox();
    CairoContext c(i, b, text);
    c.Draw1(area1, false, center);
    c.Draw1(area2, true, center);
    c.Draw2(area3, center);
}

void Draw(unsigned i, const Contour &area1, const Contour &area2, const char *text, double center)
    {Draw(i, area1, Contour(), area2, text, center);}
void Draw(unsigned i, const Contour& area1, const char *text, double center)
    {Draw(i, Contour(), Contour(), area1, text, center);}

void Draw(unsigned i, std::vector<Contour>&& v, double scale, const char *text) {
    if (shouldnt_do(i)) return;
    Block b(false);
    for (const Contour& c : v)
        b+=c.GetBoundingBox();
    if (scale==0)
        scale = std::min(20., 1000/std::max(b.x.Spans(), b.y.Spans()));
    if (scale!=1) {
        for (Contour& c : v)
            c.Scale(scale);
        b.Scale(scale);
    }
    CairoContext context(i, b, text, false);
    static std::array colors = {ColorType::red(), ColorType::blue(), ColorType::green(),
                                ColorType::blue().MixWith(ColorType::green()), ColorType::red().MixWith(ColorType::green()),
                                ColorType::blue().MixWith(ColorType::red())};
    for (unsigned u = 0; u<v.size(); u++)
        context.Draw(v[u], false, colors[u%colors.size()].r, colors[u%colors.size()].g, colors[u%colors.size()].b, true, 5);
}

void DrawExpand(unsigned i, EExpandType et, double limit, const Contour area1, const char *text=nullptr)
{
    if (shouldnt_do(i)) return;
    Block size = area1.GetBoundingBox().CreateExpand(100);
    size.x.till += 000;
    CairoContext context(i, size, text, false);
    const unsigned NUM=3;
    const double r[NUM] = {1,0,0};
    const double g[NUM] = {0,1,0};
    const double b[NUM] = {0,0,1};
    unsigned num=0;
    const double step = 4;
    double gap = -step;
    bool shrinking = true;
    double max_gap=100;
    //first we find how small we can shrink it (until it disappears),
    //then we do an expand phase to the same extent
    while(shrinking || gap>=0) {
        if (gap<-40 && shrinking) {
            max_gap = gap = 100;
            shrinking = false;
            continue;
        }
        if (gap==0) {
            if (shall_draw()) context.Draw(area1, false, 0, 0, 0, false);
        } else {
            if (shall_draw()) context.Draw(area1.CreateExpand(gap, et, et, limit, limit), false, r[num%NUM], g[num%NUM], b[num%NUM], false);
        }
        num++;
        gap-=step;
    }
}

void DrawExpand2DBig(unsigned i, const XY &start_gap, const Contour area1, const char *text = nullptr)
{
    if (shouldnt_do(i)) return;
    Block size = area1.GetBoundingBox().CreateExpand(100);
    size.x.till += 000;
    CairoContext context(i, size, text, false);
    const unsigned NUM = 3;
    const double r[NUM] = {1, 0, 0};
    const double g[NUM] = {0, 1, 0};
    const double b[NUM] = {0, 0, 1};
    unsigned num = 0;
    const double step = 4;
    double gap = -step;
    bool shrinking = true;
    double max_gap = 100;
    //first we find how small we can shrink it (until it disappears),
    //then we do an expand phase to the same extent
    while (shrinking || gap>=0) {
        Contour a = gap==0 ? area1 : area1.CreateExpand2D(XY(gap, gap) + (gap>0 ? start_gap : -start_gap));
        if (gap<-40 && shrinking) {
            max_gap = gap = 100;
            shrinking = false;
            continue;
        }
        if (gap==0) {
            if (shall_draw()) context.Draw(a, false, 0, 0, 0, false);
        } else {
            if (shall_draw()) context.Draw(a, false, r[num%NUM], g[num%NUM], b[num%NUM], false);
        }
        num++;
        gap -= step;
    }
}

void DrawIsinside(unsigned i, const Contour &a, double gap=5, const char *text=nullptr)
{
    if (shouldnt_do(i)) return;
    Block b = a.GetBoundingBox();
    CairoContext c(i, b, text);
    c.Draw(a, false, 0.5, 0.5, 0.5, true);
    for (double x = b.x.from; x<b.x.till; x+=gap)
        for (double y = b.y.from; y<b.y.till; y+=gap) {
            cairo_arc(c.cr, x, y, std::min(2., gap/2), 0, 2*M_PI);
            switch(a.IsWithin(XY(x,y))) {
            case WI_INSIDE: cairo_set_source_rgba(c.cr, 0, 0, 1, 0.5); break;
            case WI_ON_EDGE: cairo_set_source_rgba(c.cr, 1, 0, 0, 0.5); break;
            case WI_ON_VERTEX: cairo_set_source_rgba(c.cr, 0, 1, 0, 0.5); break;
            case WI_IN_HOLE: cairo_set_source_rgba(c.cr, 0.5, 0.5, 0.5, 0.5); break;
            case WI_OUTSIDE: cairo_set_source_rgba(c.cr, 0, 0, 0, 0.5); break;
            }
            cairo_fill(c.cr);
        }
}

void DrawRelation(unsigned i, const Contour &c1, const Contour &c2, const char *text="")
{
    if (shouldnt_do(i)) return;
    static const char relnames[][20] = {"OVERLAP", "A_IS_EMPTY", "B_IS_EMPTY", "BOTH_EMPTY",
                                        "A_INSIDE_B", "B_INSIDE_A", "SAME", "APART",
                                        "A_IN_HOLE_OF_B", "B_IN_HOLE_OF_A", "IN_HOLE_APART"};
    Block b = c1.GetBoundingBox();
    b += c2.GetBoundingBox();
    b += XY(100,100);
    std::string s = std::string("'A' is red: ") + relnames[(unsigned)c1.RelationTo(c2, false)] + ", should be: " + text;
    CairoContext c(i, b, s.c_str(), false);
    c.Draw(c1, false, 1, 0, 0, true, 0);
    c.Draw(c2, false, 0, 1, 0, true, 0);
};

void DrawDistance(unsigned i, const Contour &c1, const Contour &c2, const char *text="")
{
    if (shouldnt_do(i)) return;
    Block b = c1.GetBoundingBox();
    b += c2.GetBoundingBox();
    const DistanceType dist = c1.Distance(c2);
    b += XY(100,100);
    if (dist.IsValid())
        b += XY(fabs(dist.distance)+20, fabs(dist.distance));
    char uhh[1000];
    const bool bad = !dist.IsZero() && !test_equal(fabs(dist.point_on_me.Distance(dist.point_on_other)), fabs(dist.distance));
    sprintf(uhh, "Distance is %g. %s %s", dist.distance ? dist.distance : 0, bad ? "BAAAAD" : "", text); //kill "-0"
    CairoContext c(i, b, uhh, false);
    if (dist.IsValid() && dist.distance>=0) {
        c.Draw(c1.CreateExpand(dist.distance, contour::EXPAND_ROUND), false, 1, 0.9, 0.9, true);
        c.Draw(c2.CreateExpand(dist.distance, contour::EXPAND_ROUND), false, 0.9, 1, 0.9, true);
    }
    c.Draw(c1, false, 1, 0, 0, true, 0);
    c.Draw(c2, false, 0, 1, 0, true, 0);
    if (dist.IsValid() && dist.distance<0) {
        c.Draw(c1.CreateExpand(dist.distance, contour::EXPAND_ROUND), false, 1, 0.9, 0.9, true);
        c.Draw(c2.CreateExpand(dist.distance, contour::EXPAND_ROUND), false, 0.9, 1, 0.9, true);
    }
    if (dist.IsValid()) {
        cairo_set_source_rgb(c.cr, 0,0,0);
        cairo_move_to(c.cr, 10, 20);
        cairo_line_to(c.cr, 10, 30);
        cairo_new_sub_path(c.cr);
        cairo_move_to(c.cr, fabs(dist.distance)+10, 20);
        cairo_line_to(c.cr, fabs(dist.distance)+10, 30);
        cairo_new_sub_path(c.cr);
        cairo_move_to(c.cr, 10, 25);
        cairo_line_to(c.cr, fabs(dist.distance)+10, 25);
        cairo_set_line_width(c.cr, 2);
        if (dist.point_on_me.test_equal(dist.point_on_other)) {
            cairo_arc(c.cr, dist.point_on_me.x, dist.point_on_me.y, 5, 0, 2*M_PI);
            cairo_fill(c.cr);
        } else if (!dist.IsZero()) {
            cairo_move_to(c.cr, dist.point_on_me.x, dist.point_on_me.y);
            cairo_line_to(c.cr, dist.point_on_other.x, dist.point_on_other.y);
            cairo_stroke(c.cr);
        }
    }
};

void DrawCut(unsigned i, const Contour &c1, unsigned num, const XY s[], const XY e[])
{
    if (shouldnt_do(i)) return;
    Block b = c1.GetBoundingBox();
    b.Expand(10);
    const XY origo = b.UpperLeft();
    b += XY(100,100);
    b += XY(0,0);
    CairoContext c(i, b, "", false);
    c.Draw(c1, false, 1, 0, 0, true, 0);
    for (unsigned u=0; u<num; u++) if (shall_draw()) {
        const Range r = c1.Cut(s[u]+origo, e[u]+origo);
        XY start = origo + s[u], end = origo + e[u];
        const XY from = origo + s[u] + (e[u]-s[u])*r.from;
        const XY till = origo + s[u] + (e[u]-s[u])*r.till;
        if (r.from<0) start = from;
        if (r.till>1) end = till;
        cairo_move_to(c.cr, start.x, start.y);
        cairo_line_to(c.cr, end.x, end.y);
        cairo_set_source_rgb(c.cr, 0.5, 1, 0.5);
        cairo_stroke(c.cr);
        cairo_set_source_rgb(c.cr, 0, 1, 0);
        cairo_arc(c.cr, s[u].x + origo.x, s[u].y + origo.y, 3, 0, 2*M_PI);
        cairo_fill(c.cr);
        cairo_arc(c.cr, e[u].x + origo.x, e[u].y + origo.y, 3, 0, 2*M_PI);
        cairo_fill(c.cr);
        if (r.IsInvalid()) continue;
        cairo_set_source_rgb(c.cr, 0, 0, 1);
        cairo_arc(c.cr, from.x, from.y, 3, 0, 2*M_PI);
        cairo_fill(c.cr);
        cairo_arc(c.cr, till.x, till.y, 3, 0, 2*M_PI);
        cairo_fill(c.cr);
    }
}

void DrawExpand2D(unsigned i, const Contour &c1, const XY &gap, const char *text="")
{
    if (shouldnt_do(i)) return;
    const Contour c2 = c1.CreateExpand2D(gap);
    Block b = c1.GetBoundingBox();
    b += c2.GetBoundingBox();
    if (b.IsInvalid()) return;
    b.Expand(10);
    CairoContext c(i, b, text, false);
    c.Draw(c1, false, 1, 0, 0, true, 0);
    c.Draw(c2, false, 0.5, 1, 0.5, true, 0);
    cairo_set_source_rgb(c.cr, 0, 0, 0);
    cairo_rectangle(c.cr, c1.front().Outline()[0].GetStart().x, c1.front().Outline()[0].GetStart().y, gap.x, gap.y);
    cairo_stroke(c.cr);
    const unsigned edge = 3;
    if (c1.front().Outline().size()>edge)
        cairo_rectangle(c.cr, c1.front().Outline()[edge].GetStart().x, c1.front().Outline()[edge].GetStart().y, gap.x, -gap.y);
    cairo_stroke(c.cr);
};

void DrawTangent(unsigned i, const Contour &c1, const Contour &c2, const char *text="")
{
    if (shouldnt_do(i)) return;
    Block b = c1.GetBoundingBox();
    b += c2.GetBoundingBox();
    XY C[2], CC[2];
    bool was = c1.TangentFrom(c2, C, CC);
    CairoContext c(i, b, text, false);
    c.Draw(c1, false, 1, 0, 0, true, 0);
    c.Draw(c2, false, 0, 1, 0, true, 0);
    if (was) {
        cairo_set_source_rgb(c.cr, 0,0,0);
        cairo_move_to(c.cr, C[0].x, C[0].y);
        cairo_line_to(c.cr, C[1].x, C[1].y);
        cairo_stroke(c.cr);
        cairo_arc(c.cr, C[0].x, C[0].y, 5, 0, 2*M_PI);
        cairo_fill(c.cr);
        cairo_arc(c.cr, C[1].x, C[1].y, 5, 0, 2*M_PI);
        cairo_fill(c.cr);
        cairo_move_to(c.cr, CC[0].x, CC[0].y);
        cairo_line_to(c.cr, CC[1].x, CC[1].y);
        cairo_stroke(c.cr);
        cairo_arc(c.cr, CC[0].x, CC[0].y, 5, 0, 2*M_PI);
        cairo_stroke(c.cr);
        cairo_arc(c.cr, CC[1].x, CC[1].y, 5, 0, 2*M_PI);
        cairo_stroke(c.cr);
    }
};

void DrawTangents(unsigned i, const Contour &c1, const char *text = "")
{
    if (shouldnt_do(i)) return;
    Block b = c1.GetBoundingBox();
    const double radius = std::max(b.x.Spans(), b.y.Spans());
    const XY cc = b.Centroid();
    CairoContext c(i, Block(cc,cc).Expand(radius), text, false);
    c.Draw(c1, false, 1, 0, 0, true, 0);
    for (unsigned u = 0; u<360; u+=20) {
        const XY c2 = cc+XY(radius, 0).Rotate(cos(u*M_PI/180), sin(u*M_PI/180));
        XY C, CC;
        bool was = c1.TangentFrom(c2, C, CC);
        if (was && shall_draw()) {
            cairo_set_source_rgb(c.cr, 0, 0, 0);
            cairo_move_to(c.cr, C.x, C.y);
            cairo_line_to(c.cr, c2.x, c2.y);
            cairo_stroke(c.cr);
            cairo_arc(c.cr, C.x, C.y, 5, 0, 2*M_PI);
            cairo_fill(c.cr);
            cairo_arc(c.cr, c2.x, c2.y, 5, 0, 2*M_PI);
            cairo_fill(c.cr);
            cairo_move_to(c.cr, CC.x, CC.y);
            cairo_line_to(c.cr, c2.x, c2.y);
            cairo_stroke(c.cr);
            cairo_arc(c.cr, CC.x, CC.y, 5, 0, 2*M_PI);
            cairo_stroke(c.cr);
            cairo_arc(c.cr, c2.x, c2.y, 5, 0, 2*M_PI);
            cairo_stroke(c.cr);
        }
    }
};

void DrawTangents(unsigned i, const Edge &c1, const Edge &c2,  const char *text = "")
{
    if (shouldnt_do(i)) return;
    Block b = c1.CreateBoundingBox()+c2.CreateBoundingBox();
    const double radius = std::max(b.x.Spans(), b.y.Spans());
    const XY cc = b.Centroid();
    CairoContext c(i, Block(cc, cc).Expand(2*radius), text, false);
    cairo_set_source_rgb(c.cr, 1, 0, 0);
    cairo_move_to(c.cr, c1.GetStart().x, c1.GetStart().y);
    c1.PathTo(c.cr);
    cairo_stroke(c.cr);
    for (unsigned u = 0; u<360; u += 20) if (shall_draw()) {
        const XY p = XY(radius, 0).Rotate(cos(u*M_PI/180), sin(u*M_PI/180));
        const Edge c2m = c2.CreateShifted(p);
        cairo_set_source_rgb(c.cr, 0, 1, 0);
        cairo_move_to(c.cr, c2m.GetStart().x, c2m.GetStart().y);
        c2m.PathTo(c.cr);
        cairo_stroke(c.cr);
        XY C[2] = {c1.GetStart(), c2m.GetStart()}, CC[2] = {c1.GetStart(), c2m.GetStart()};
        c1.TangentFrom<true>(c2m, C, CC);
        cairo_set_source_rgb(c.cr, 0, 0, 0);
        cairo_move_to(c.cr, C[0].x, C[0].y);
        cairo_line_to(c.cr, C[1].x, C[1].y);
        cairo_stroke(c.cr);
        cairo_arc(c.cr, C[0].x, C[0].y, 5, 0, 2*M_PI);
        cairo_fill(c.cr);
        cairo_arc(c.cr, C[1].x, C[1].y, 5, 0, 2*M_PI);
        cairo_fill(c.cr);
        cairo_move_to(c.cr, CC[0].x, CC[0].y);
        cairo_line_to(c.cr, CC[1].x, CC[1].y);
        cairo_stroke(c.cr);
        cairo_arc(c.cr, CC[0].x, CC[0].y, 5, 0, 2*M_PI);
        cairo_stroke(c.cr);
        cairo_arc(c.cr, CC[1].x, CC[1].y, 5, 0, 2*M_PI);
        cairo_stroke(c.cr);
    }
};

namespace generated_forms {
    Contour tri, boxhole, cooomplex, cooomplex2, cooomplex3;
    Contour variable, variable_clear, custom, later, raster;
    Contour circle, circle2, circle3, huhu, boxhole2;
    Contour part, spart, partxy, spartxy, forexpbevel;
    Contour lohere1, lohere2, lohere3, lohere4, lohere5, lohere6;
    Contour rombusz, concave, triangle, box_circle;
    std::mutex m;
    bool done = false;
    Contour ChoppedBox;
    Contour form1, form2, form3, form4, form5;
} //namespace local

void generate_forms()
{
    using namespace generated_forms;
    std::scoped_lock _(m);
    if (done) return;
    done = true;
    tri = Contour(XY(50,90), XY(100,60), XY(40,20));
    tri +=  Contour(30,70,60,70);

    boxhole = Contour(130,170,60,70);
    boxhole += Contour(160,170,60,140);
    boxhole += Contour(130,140,60,140);
    boxhole += Contour(130,170,130,140);
    boxhole += Contour(148,153, 85, 115);

    cooomplex=boxhole+tri;

    cooomplex2 = Contour(110, 200, 80, 120);
    cooomplex2 -= Contour(120, 190, 90, 110);

    variable = boxhole + cooomplex2;
    cooomplex2 += cooomplex;
    later = cooomplex2;

    custom = cooomplex2;
    cooomplex2 += cooomplex2.CreateShifted(XY(15,15));

    cooomplex2.Shift(XY(200,0));
    custom. Shift(XY(200,0));

    const int num_x = 10;
    const int num_y = 10;
    for (int i=0; i<num_x; i++)
        raster += Contour(200+i*20, 215+i*20, 200, 190+num_y*20);
    for (int j=0; j<num_y; j++)
        raster += Contour(200, 190+num_x*20, 200+j*20, 215+j*20);

    cooomplex2.ClearHoles();

    cooomplex2 *= Contour(XY(300,101), 100, 50);

    circle = Contour(XY(200, 200), 60, 30, 30);
    circle += Contour(200,300, 170,190);

    double x=220, y=610;
    circle2= Contour(XY(x, y), 60, 30, fabs(x-y));
    circle3 = circle2;
    circle2 += Contour(x,x+100, y+15,y+30);

    boxhole2 = Contour(110, 200, 80, 120);
    boxhole2 -= Contour(120, 190, 90, 110);
    huhu = boxhole2;
    huhu.ClearHoles();
    huhu *= Contour(XY(130,101), 30,20);

    cooomplex3 = cooomplex2;
    cooomplex2 = cooomplex3;
    cooomplex2.RotateAround(XY(350,100), 39);
    part = cooomplex2[1];
    spart = cooomplex3[1];
    partxy = part.CreateSwapXYd();
    partxy.IsSane();
    spartxy = spart.CreateSwapXYd();
    spartxy.IsSane();

    variable.ClearHoles();

    const XY forexpbevel_points[] = {XY(100,100), XY(130, 100), XY(100, 80), XY(150, 80),
        XY(150,160), XY(100,160)};
    forexpbevel.assign(forexpbevel_points);

    lohere1 = Contour(XY(25,25), 25) + Contour(XY(75,25), 25);
    lohere1 += Contour(XY(25,75), 25);
    lohere1 += Contour(XY(75,75), 25);
    lohere2 = Contour(XY(100,100), 100) - Contour(XY(140,100), 120);
    lohere3 = Contour(0,100, 0,100) - lohere1;
    lohere4 = Contour(0,100, 0,100) +
              Contour(XY(0,0), 50) - Contour(XY(50,0), 50) +
              Contour(XY(0,50), 50) - Contour(XY(50,50), 50);
    lohere5 = lohere3 * Contour(75,100, 75,100);
    lohere6 = lohere3 - Contour(0, 25, 0, 25) -Contour(75, 100, 0, 25) - Contour(0, 25, 75, 100) - Contour(75, 100, 75, 100);

    rombusz = Contour(40,80,40,80).CreateRotatedAround(XY(60,60),45);
    concave = Contour(0,40,0,100) - Contour(0,20,30,60) + Contour(XY(20,45), 15);
    triangle = Contour(XY(0,0), XY(100,10), XY(0,20));

    ChoppedBox = Contour(100, 200, 100, 200) - Contour(100, 110, 100, 110);
    variable_clear = variable;
    variable_clear.ClearHoles();
    box_circle = Contour(10, 110, 10, 110) - Contour(30, 40, 30, 80) - Contour(80, 90, 30, 80);

    form1 = Contour(0, 100, 0, 50) + Contour(XY(0, 25), 10, 25) - Contour(XY(100, 25), 10, 25);
    form2 = Contour(0, 100, 0, 50) - Contour(XY(0, 25), 10, 25) + Contour(XY(100, 25), 10, 25);
    form3 = Contour(0, 100, 0, 50) + Contour(XY(0, 25), 10, 35) - Contour(XY(100, 25), 10, 35);
    form4 = Contour(0, 100, 0, 50) - Contour(XY(0, 25), 10, 35) + Contour(XY(100, 25), 10, 35);
    form5 = Contour(0, 100, 0, 50) - Contour(XY(0, 15), 15) -Contour(XY(0, 40), 10);
}

void contour_test_basic(void)
{
    if (shouldnt_do(1000, 1199) && shouldnt_do(100, 119)) return;
    Contour tri = Contour(XY(50, 90), XY(100, 60), XY(40, 20));
    Draw(100, tri, Contour(30, 170, 60, 70), tri ^ Contour(30, 170, 60, 70));
    Draw(1001, tri, tri.CreateSwapXYd());
    Draw(1002, tri, tri.CreateRotated(30));
    Draw(1003, Contour(XY(-1, 103), XY(25, 37), XY(56, 102)), tri.CreateRotated(30));
    tri += Contour(30, 70, 60, 70);

    Draw(101, tri, tri.CreateShifted(XY(15, 15)), tri ^ tri.CreateShifted(XY(15, 15)));

    Contour boxhole = Contour(130, 170, 60, 70);
    boxhole += Contour(160, 170, 60, 140);
    Draw(1020, boxhole);
    boxhole += Contour(130, 140, 60, 140);
    Draw(1021, boxhole);
    boxhole += Contour(130, 170, 130, 140);
    Draw(1022, boxhole, Contour(148, 153, 85, 115), boxhole + Contour(148, 153, 85, 115));
    boxhole += Contour(148, 153, 85, 115);

    Contour cooomplex;
    Draw(103, boxhole, tri, boxhole + tri);
    cooomplex = boxhole+tri;

    Contour cooomplex2 = Contour(110, 200, 80, 120);
    Draw(104, cooomplex2, Contour(120, 190, 90, 110), cooomplex2 - Contour(120, 190, 90, 110));
    cooomplex2 -= Contour(120, 190, 90, 110);

    Draw(1051, boxhole, cooomplex2, boxhole + cooomplex2, "ADD");
    Draw(1052, boxhole, cooomplex2, boxhole * cooomplex2, "MUL");
    Draw(1053, boxhole, cooomplex2, boxhole ^ cooomplex2, "XOR");
    Draw(1054, boxhole, cooomplex2, boxhole - cooomplex2, "SUB");

    Contour variable = boxhole + cooomplex2;
    DrawIsinside(1055, variable);

    Draw(106, cooomplex2, cooomplex, cooomplex2 + cooomplex);
    cooomplex2 += cooomplex;
    const Contour later = cooomplex2;

    Contour custom = cooomplex2;
    Draw(107, cooomplex2, cooomplex2.CreateShifted(XY(15, 15)), cooomplex2 ^ cooomplex2.CreateShifted(XY(15, 15)));
    cooomplex2 += cooomplex2.CreateShifted(XY(15, 15));

    cooomplex2.Shift(XY(200, 0));
    custom.Shift(XY(200, 0));

    cooomplex.clear();
    const int num_x = 10;
    const int num_y = 10;
    for (int i = 0; i<num_x; i++)
        cooomplex += Contour(200+i*20, 215+i*20, 200, 190+num_y*20);
    for (int j = 0; j<num_y; j++)
        cooomplex += Contour(200, 190+num_x*20, 200+j*20, 215+j*20);
    Draw(108, cooomplex);

    cooomplex2.ClearHoles();


    Draw(1091, Contour(100, 300, 60, 70), Contour(XY(300, 100), 100, 50), Contour(100, 300, 60, 70) + Contour(XY(300, 100), 100, 50));
    Draw(1092, Contour(100, 300, 60, 70), Contour(XY(300, 100), 100, 50), Contour(100, 300, 60, 70) * Contour(XY(300, 100), 100, 50));
    Draw(1093, Contour(100, 300, 60, 70), Contour(XY(300, 100), 100, 50), Contour(100, 300, 60, 70) ^ Contour(XY(300, 100), 100, 50));
    Draw(1094, Contour(100, 300, 60, 70), Contour(XY(300, 100), 100, 50), Contour(100, 300, 60, 70) - Contour(XY(300, 100), 100, 50));

    Draw(109, cooomplex2, Contour(XY(300, 101), 100, 50), cooomplex2 ^ Contour(XY(300, 101), 100, 50));
    DrawIsinside(1091, cooomplex2 ^ Contour(XY(300, 101), 100, 50), 5);

    Draw(110, tri, tri.CreateSwapXYd());
    Draw(1101, boxhole, boxhole.CreateSwapXYd());
    Draw(1102, cooomplex, cooomplex.CreateSwapXYd());
    Draw(1103, Contour(XY(300, 101), 100, 50), Contour(XY(300, 101), 100, 50).CreateSwapXYd());
    Draw(1104, cooomplex2 ^ Contour(XY(300, 101), 100, 50), (cooomplex2 ^ Contour(XY(300, 101), 100, 50)).CreateSwapXYd());

    if (!shouldnt_do(111, 112)) {
        Contour custom;
        double x = 150, y = 250;
        const XY v1[] = {XY(50, 200),
                        XY(x, y),
                        XY(50, 300),
                        XY(x+30, y+40),
                        XY(100, 300),
                        XY(x, y),
                        XY(90, 300)};
        custom.assign(v1);
        Draw(111, custom);

        Contour custom2;

        x = 214, y = 610;
        const XY v2[] = {XY(50, 200),
                        XY(x, y),
                        XY(50, 300),
                        XY(x+30, y+40),
                        XY(100, 300),
                        XY(x, y),
                        XY(90, 300)};
        custom.assign(v2);

        x = 220, y = 610;
        const XY v3[] = {XY(50, 200),
                        XY(x, y),
                        XY(50, 300),
                        XY(x+30, y+40),
                        XY(100, 300),
                        XY(x, y),
                        XY(90, 300)};
        custom2.assign(v3);
        Draw(112, custom, custom2);
    }
    Contour circle = Contour(XY(200, 200), 60, 30, 30);
    Draw(113, circle, Contour(200, 300, 170, 190), circle + Contour(200, 300, 170, 190));
    circle += Contour(200, 300, 170, 190);

    double x = 220, y = 610;
    Contour circle2 = Contour(XY(x, y), 60, 30, abs(x-y));
    Contour circle3 = circle2;
    Draw(114, circle2, Contour(x, x+100, y+15, y+30), circle2 + Contour(x, x+100, y+15, y+30));
    circle2 += Contour(x, x+100, y+15, y+30);

    Contour boxhole2 = Contour(110, 200, 80, 120);
    boxhole2 -= Contour(120, 190, 90, 110);
    Contour huhu = boxhole2;
    huhu.ClearHoles();
    Draw(115, huhu, Contour(XY(130, 101), 30, 20), huhu + Contour(XY(130, 101), 30, 20));

    Draw(116, huhu, Contour(XY(130, 101), 30, 20), huhu * Contour(XY(130, 101), 30, 20));
    huhu *= Contour(XY(130, 101), 30, 20);
    DrawIsinside(1161, huhu, 2);
    DrawIsinside(1162, huhu.CreateSwapXYd(), 2);
    DrawIsinside(1163, (boxhole2 - Contour(XY(130, 101), 30, 20)).CreateSwapXYd(), 2);

    cooomplex2 *= Contour(XY(300, 101), 100, 50);
    Contour cooomplex3 = cooomplex2;
    cooomplex2 = cooomplex3;
    cooomplex2.RotateAround(XY(350, 100), 39);
    Contour part = cooomplex2[1];
    Contour spart = cooomplex3[1];

    Draw(117, cooomplex3, cooomplex2, "SwapXY");
    Draw(1171, cooomplex2, cooomplex2.CreateSwapXYd(), "Rotated ellipse swapXY()");
    DrawIsinside(1172, cooomplex2, 2, "Rotated ellipse is inside");
    DrawIsinside(1173, cooomplex2.CreateSwapXYd(), 2, "Rotated ellipse swapXY()");
    DrawIsinside(1174, part, 2);
    DrawIsinside(1175, spart, 2);
    Contour partxy = part.CreateSwapXYd();
    partxy.IsSane();
    Contour spartxy = spart.CreateSwapXYd();
    spartxy.IsSane();
    DrawIsinside(1176, partxy, 2, "Rotated");
    DrawIsinside(1177, spartxy, 2, "Rotated");

    Contour A = Contour(10, 100, 10, 100)-Contour(40, 70, 40, 70);
    Contour B = Contour(20,  90, 20,  90)-Contour(30, 80, 30, 80);

    Draw(1180, A-B, "difference with holes");
}

void contour_test_expand(void)
{
    if (shouldnt_do(112, 199)) return;
    using namespace generated_forms;

    DrawExpand(120, EXPAND_MITER, CONTOUR_INFINITY, ChoppedBox);

    DrawExpand(121, EXPAND_MITER, CONTOUR_INFINITY, forexpbevel);
    DrawExpand(122, EXPAND_ROUND, CONTOUR_INFINITY, forexpbevel);
    DrawExpand(123, EXPAND_BEVEL, CONTOUR_INFINITY, forexpbevel);

    DrawExpand(124, EXPAND_MITER, CONTOUR_INFINITY, box_circle, "box with miter");
    DrawExpand(125, EXPAND_ROUND, CONTOUR_INFINITY, box_circle, "box with round");
    DrawExpand(126, EXPAND_BEVEL, CONTOUR_INFINITY, box_circle, "box with bevel");
    DrawExpand(127, EXPAND_MITER, CONTOUR_INFINITY, variable_clear, "boxhole with miter");
    DrawExpand(128, EXPAND_ROUND, CONTOUR_INFINITY, variable_clear, "boxhole with round");
    DrawExpand(129, EXPAND_BEVEL, CONTOUR_INFINITY, variable_clear, "boxhole with bevel");

    DrawExpand(130, EXPAND_MITER, CONTOUR_INFINITY, later, "later with miter");
    DrawExpand(131, EXPAND_BEVEL, CONTOUR_INFINITY, later, "later with bevel");
    DrawExpand(132, EXPAND_ROUND, CONTOUR_INFINITY, later, "later with round");
    DrawExpand(133, EXPAND_MITER, CONTOUR_INFINITY, cooomplex, "complex with miter");
    DrawExpand(134, EXPAND_MITER, CONTOUR_INFINITY, huhu, "huhu with miter");

    DrawExpand(135, EXPAND_MITER, CONTOUR_INFINITY, part, "part with miter");
    DrawExpand(136, EXPAND_ROUND, CONTOUR_INFINITY, part, "part with round");
    DrawExpand(137, EXPAND_BEVEL, CONTOUR_INFINITY, part, "part with bevel");

    DrawExpand(138, EXPAND_MITER, CONTOUR_INFINITY, spart, "spart with round");
    DrawExpand(139, EXPAND_ROUND, CONTOUR_INFINITY, spart, "spart with round");
    DrawExpand(140, EXPAND_BEVEL, CONTOUR_INFINITY, spart, "spart with round");

    DrawExpand(150, EXPAND_MITER, CONTOUR_INFINITY, cooomplex3, "complex3 with miter");
    DrawExpand(151, EXPAND_BEVEL, CONTOUR_INFINITY, cooomplex3, "complex3 with miter");
    DrawExpand(152, EXPAND_ROUND, CONTOUR_INFINITY, cooomplex3, "complex3 with miter");
    DrawExpand(153, EXPAND_MITER, CONTOUR_INFINITY, cooomplex2, "rounded complex3 with miter");
    DrawExpand(154, EXPAND_BEVEL, CONTOUR_INFINITY, cooomplex2, "rounded complex3 with miter");
    DrawExpand(155, EXPAND_ROUND, CONTOUR_INFINITY, cooomplex2, "rounded complex3 with miter");

    DrawExpand(160, EXPAND_MITER, CONTOUR_INFINITY, form1, "pipe with miter");
    DrawExpand(161, EXPAND_MITER, CONTOUR_INFINITY, form2, "reverse pipe with miter");
    DrawExpand(162, EXPAND_MITER, CONTOUR_INFINITY, form3, "pipe with bigger circle with miter");
    DrawExpand(163, EXPAND_MITER, CONTOUR_INFINITY, form4, "reverse pipe with bigger circle with miter");
    DrawExpand(164, EXPAND_MITER, CONTOUR_INFINITY, form5, "two inverse circles with miter");
    DrawExpand(170, EXPAND_BEVEL, CONTOUR_INFINITY, form1, "pipe with miter");
    DrawExpand(171, EXPAND_BEVEL, CONTOUR_INFINITY, form2, "reverse pipe with miter");
    DrawExpand(172, EXPAND_BEVEL, CONTOUR_INFINITY, form3, "pipe with bigger circle with miter");
    DrawExpand(173, EXPAND_BEVEL, CONTOUR_INFINITY, form4, "reverse pipe with bigger circle with miter");
    DrawExpand(174, EXPAND_BEVEL, CONTOUR_INFINITY, form5, "two inverse circles with miter");
    DrawExpand(180, EXPAND_ROUND, CONTOUR_INFINITY, form1, "pipe with miter");
    DrawExpand(181, EXPAND_ROUND, CONTOUR_INFINITY, form2, "reverse pipe with miter");
    DrawExpand(182, EXPAND_ROUND, CONTOUR_INFINITY, form3, "pipe with bigger circle with miter");
    DrawExpand(183, EXPAND_ROUND, CONTOUR_INFINITY, form4, "reverse pipe with bigger circle with miter");
    DrawExpand(184, EXPAND_ROUND, CONTOUR_INFINITY, form5, "two inverse circles with miter");

    DrawExpand(185, EXPAND_MITER_BEVEL, CONTOUR_INFINITY, form1, "pipe with miter");
    DrawExpand(186, EXPAND_MITER_BEVEL, CONTOUR_INFINITY, form2, "reverse pipe with miter");
    DrawExpand(187, EXPAND_MITER_BEVEL, CONTOUR_INFINITY, form3, "pipe with bigger circle with miter");
    DrawExpand(188, EXPAND_MITER_BEVEL, CONTOUR_INFINITY, form4, "reverse pipe with bigger circle with miter");
    DrawExpand(189, EXPAND_MITER_BEVEL, CONTOUR_INFINITY, form5, "two inverse circles with miter");

    DrawExpand(190, EXPAND_MITER_ROUND, CONTOUR_INFINITY, form1, "pipe with miter");
    DrawExpand(191, EXPAND_MITER_ROUND, CONTOUR_INFINITY, form2, "reverse pipe with miter");
    DrawExpand(192, EXPAND_MITER_ROUND, CONTOUR_INFINITY, form3, "pipe with bigger circle with miter");
    DrawExpand(193, EXPAND_MITER_ROUND, CONTOUR_INFINITY, form4, "reverse pipe with bigger circle with miter");
    DrawExpand(194, EXPAND_MITER_ROUND, CONTOUR_INFINITY, form5, "two inverse circles with miter");

    DrawExpand(195, EXPAND_MITER_SQUARE, CONTOUR_INFINITY, form1, "pipe with miter");
    DrawExpand(196, EXPAND_MITER_SQUARE, CONTOUR_INFINITY, form2, "reverse pipe with miter");
    DrawExpand(197, EXPAND_MITER_SQUARE, CONTOUR_INFINITY, form3, "pipe with bigger circle with miter");
    DrawExpand(198, EXPAND_MITER_SQUARE, CONTOUR_INFINITY, form4, "reverse pipe with bigger circle with miter");
    DrawExpand(199, EXPAND_MITER_SQUARE, CONTOUR_INFINITY, form5, "two inverse circles with miter");
};


void contour_test_lohere(void)
{
    if (shouldnt_do(2181, 2199) && shouldnt_do(250, 360)) return;
    Draw(2181, Contour(XY(25, 25), 25) + Contour(XY(75, 25), 25));
    Draw(2182, Contour(XY(25,25), 25) + Contour(XY(75,25), 25) + Contour(XY(25,75), 25));
    Draw(2183, generated_forms::lohere1);

    Draw(2184, Contour(0,100, 0,100), generated_forms::lohere1, Contour(0,100, 0,100) + generated_forms::lohere1);
    Draw(2185, Contour(0,100, 0,100), generated_forms::lohere1, Contour(0,100, 0,100) - generated_forms::lohere1);

    Draw(2190, generated_forms::lohere1);
    Draw(2191, Contour(XY(100,100), 100), Contour(XY(140,100), 120), generated_forms::lohere2);
    Draw(2192, generated_forms::lohere3);
    Draw(2193, generated_forms::lohere4);
    Draw(2194, generated_forms::lohere5);

    Draw(2195, generated_forms::lohere6);

    Contour a = generated_forms::lohere3.CreateExpand(4, EXPAND_BEVEL, EXPAND_BEVEL);
    Contour b = a; b.ClearHoles();
    Draw(2196, a, b);
    Draw (2197,b, b - Contour(10,40,10,40));

    DrawExpand(250, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::lohere1);
    DrawExpand(251, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::lohere2);
    DrawExpand(252, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::lohere3);
    DrawExpand(253, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::lohere4);
    DrawExpand(254, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::lohere5);
    DrawExpand(255, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::lohere6);

    DrawExpand(260, EXPAND_BEVEL, CONTOUR_INFINITY, generated_forms::lohere1);
    DrawExpand(261, EXPAND_BEVEL, CONTOUR_INFINITY, generated_forms::lohere2);
    DrawExpand(262, EXPAND_BEVEL, CONTOUR_INFINITY, generated_forms::lohere3);
    DrawExpand(263, EXPAND_BEVEL, CONTOUR_INFINITY, generated_forms::lohere4);
    DrawExpand(264, EXPAND_BEVEL, CONTOUR_INFINITY, generated_forms::lohere5);
    DrawExpand(265, EXPAND_BEVEL, CONTOUR_INFINITY, generated_forms::lohere6);

    DrawExpand(270, EXPAND_ROUND, CONTOUR_INFINITY, generated_forms::lohere1);
    DrawExpand(271, EXPAND_ROUND, CONTOUR_INFINITY, generated_forms::lohere2);
    DrawExpand(272, EXPAND_ROUND, CONTOUR_INFINITY, generated_forms::lohere3);
    DrawExpand(273, EXPAND_ROUND, CONTOUR_INFINITY, generated_forms::lohere4);
    DrawExpand(274, EXPAND_ROUND, CONTOUR_INFINITY, generated_forms::lohere5);
    DrawExpand(275, EXPAND_ROUND, CONTOUR_INFINITY, generated_forms::lohere6);

    DrawExpand(350, EXPAND_MITER, CONTOUR_INFINITY,Contour(0,40, 0,100) + Contour(XY(60,50), 20));
    DrawExpand(351, EXPAND_MITER, CONTOUR_INFINITY,Contour(0,40, 0,100) + Contour(XY(50,50), 20));
    DrawExpand(352, EXPAND_MITER, CONTOUR_INFINITY,Contour(0,40, 0,100) + Contour(XY(50,50), 30,15, 130));

    DrawExpand(353, EXPAND_MITER, CONTOUR_INFINITY,Contour(0,40, 0,100) + generated_forms::rombusz);
    DrawExpand(354, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::rombusz + generated_forms::rombusz.CreateShifted(XY(40,0)));

    DrawExpand(355, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::concave);

    DrawExpand(356, EXPAND_MITER, CONTOUR_INFINITY, generated_forms::triangle);
    DrawExpand(357, EXPAND_MITER, 1.2, generated_forms::triangle);
    DrawExpand(358, EXPAND_MITER, 2, generated_forms::triangle);
    DrawExpand(359, EXPAND_MITER, 1, generated_forms::triangle);
    DrawExpand(360, EXPAND_MITER, 0, generated_forms::triangle);
};

void contour_test_area()
{
    if (shouldnt_do(400, 429)) return;
    //Test area and circumference
    Contour poly(60,200, 90,110); //30 degree rotated
    for (unsigned i = 0; i<10; i++) {
        const Contour block(0, 65+i*5, 0, 200);
        Contour res = poly - block;
        Contour rot = res.CreateRotatedAround(XY(100,100),29);
        char buff[200];
        sprintf(buff, "Normal Area: %g, Circumference: %g\nRotate Area: %g, Circumference: %g", res.GetArea(), res.GetCircumference(), rot.GetArea(), rot.GetCircumference());
        DrawIsinside(400+i, rot, 10, buff);
    }

    //End of exclusion for speed */

    Contour Circle(XY(100,100), 30, 30, 30); //30 degree rotated
    for (unsigned i = 2; i<10; i++) {
        const Contour block(0, 65+i*5, 0, 200);
        Contour res = Circle - block;
        char buff[200];
        sprintf(buff, "Area: %g, Circumference: %g", res.GetArea(), res.GetCircumference());
        DrawIsinside(410+i, res, 10, buff);
    }

    Contour ell(XY(100,100), 40, 10, 30); //30 degree rotated
    for (unsigned i = 0; i<10; i++) {
        const Contour block(0, 65+i*5, 0, 200);
        Contour res = ell - block;
        char buff[200];
        sprintf(buff, "Area: %g, Circumference: %g", res.GetArea(), res.GetCircumference());
        DrawIsinside(420+i, res, 10, buff);
    }
}


void contour_test_relations()
{
    if (shouldnt_do(8000, 8020)) return;
    Contour c1[10], c2[10], c3[10];
    c1[0] = Block(22,28, 22,28); //tiny
    c1[1] = Block(20,30, 20,30); //small
    c1[2] = Block(10,40, 10,40); //medium
    c1[3] = Block(0,50, 0,50);   //large
    c1[4] = c1[3] - c1[2];       //large with hole
    c1[5] = c1[4] + c1[1];       //small in large with hole

    for (unsigned u=0; u<10; u++) {
        c2[u] = c1[u].CreateShifted(XY(60,0));
        c3[u] = c1[u].CreateShifted(XY(120,0));
    }

    DrawRelation(8000, c1[3], c1[9], "B_EMPTY");
    DrawRelation(8001, c1[9], c1[1], "A_EMPTY");
    DrawRelation(8002, c1[9], c1[9], "BOTH_EMPTY");

    DrawRelation(8003, c1[3], c1[2], "B_IN_A");
    DrawRelation(8004, c1[0], c2[1], "APART");

    DrawRelation(8005, c1[3], c1[2], "B_IN_A");
    DrawRelation(8006, c1[0], c1[2], "A_IN_B");
    DrawRelation(8007, c1[1], c1[1], "SAME");
    DrawRelation(8008, c1[0], c2[1], "APART");
    DrawRelation(8009, c1[3], c1[2].CreateShifted(XY(20,0)), "OVERLAP");

    DrawRelation(8010, c1[4], c1[1], "B_IN_HOLE_A");
    DrawRelation(8011, c1[1], c1[4], "A_IN_HOLE_B");

    DrawRelation(8020, c1[1]+c2[1], c1[4], "HOLE_MIXED");
};

void contour_test_distance()
{
    if (shouldnt_do(8100, 8119)) return;
    Contour c1(30, 100, 30, 100);
    Contour c2(130, 150, 140, 200);
    DrawDistance(8100, c1, c2, "");

    Contour c3(XY(120, 200), 40, 60, 20);
    DrawDistance(8101, c1, c3);

    c3.Shift(XY(30,0));
    DrawDistance(8102, c1, c3);

    c3.Shift(XY(70,0));
    DrawDistance(8103, c1, c3);
    DrawDistance(8104, c1, c3 * Block(180, 400, 0, 500));
    DrawDistance(8105, c1, c3 * Block(200, 400, 0, 500));
    DrawDistance(8106, c1, c3 * Block(2300, 400, 0, 500));

    DrawDistance(8107, c3.CreateRotated(30), c3);
    DrawDistance(8108, c3 * Block(180, 400, 0, 500), (c3 * Block(180, 400, 0, 500)).Rotate(30));
    DrawDistance(8109, c3 * Block(200, 400, 0, 500), (c3 * Block(200, 400, 0, 500)).Rotate(30));

    Contour c4 = c1 - Block(40, 90, 40, 90);
    DrawDistance(8110, c4, Contour(XY(60,60), 40, 20));
    DrawDistance(8111, c4, Contour(XY(60,60), 30, 20));
    DrawDistance(8112, c4, Contour(XY(60,60), 30, 15));
    DrawDistance(8113, c4, Contour(XY(60,60), 20, 15));
    DrawDistance(8114, c4, Contour(XY(60,60), 15, 10));

    DrawDistance(8115, c1, Block(40, 90, 40, 90));
    DrawDistance(8116, c1, Contour(40, 90, 40, 90) + Contour(140,150,30,40));
    DrawDistance(8117, c1 - Block(40, 50, 40, 50), Block(60, 70, 60, 70));
    DrawDistance(8118, c1 - Block(40, 50, 40, 50) - Block (65, 75, 65, 75), Block(60, 70, 60, 70));
    DrawDistance(8119, c4 + Block(50, 80, 50, 80), Block(60, 70, 60, 70));
}

void contour_test_cut()
{
    if (shouldnt_do(8300, 8304)) return;
    const XY start[] = {XY(0,0), XY(20,30), XY(100, 200)};
    const XY end[] = {XY(30,40), XY(210,20), XY(10, 30)};
    DrawCut(8300, generated_forms::boxhole.GetBoundingBox(), sizeof(start)/sizeof(XY), start, end);
    DrawCut(8301, generated_forms::tri, sizeof(start)/sizeof(XY), start, end);
    DrawCut(8302, generated_forms::raster, sizeof(start)/sizeof(XY), start, end);
    DrawCut(8303, generated_forms::boxhole, sizeof(start)/sizeof(XY), start, end);
    DrawCut(8304, generated_forms::cooomplex3, sizeof(start)/sizeof(XY), start, end);
}


void contour_test_expand2D()
{
    if (shouldnt_do(8700, 8900)) return;

    DrawExpand2D(8700, generated_forms::boxhole, XY(10,10));
    DrawExpand2D(8701, generated_forms::boxhole, XY(10,20));
    DrawExpand2D(8702, generated_forms::boxhole, XY(10,30));
    DrawExpand2D(8703, generated_forms::boxhole, XY(30,10));
    DrawExpand2D(8704, generated_forms::boxhole, XY(0,100));

    DrawExpand2D(8705, Contour(XY(50,50),40), XY(30,10));
    DrawExpand2D(8706, Contour(XY(50,50),40), -XY(5,10));

    DrawExpand2D(8707, Contour(XY(50,50),40, 20, 00), XY(30,10));
    DrawExpand2D(8708, Contour(XY(50,50),40, 20, 10), XY(30,10));
    DrawExpand2D(8709, Contour(XY(50,50),40, 20, 20), XY(30,10));
    DrawExpand2D(8710, Contour(XY(50,50),40, 20, 30), XY(30,10));
    DrawExpand2D(8711, Contour(XY(50,50),40, 20, 40), XY(30,10));
    DrawExpand2D(8712, Contour(XY(50,50),40, 20, 50), XY(30,10));
    DrawExpand2D(8713, Contour(XY(50,50),40, 20, 60), XY(30,10));
    DrawExpand2D(8714, Contour(XY(50,50),40, 20, 70), XY(30,10));
    DrawExpand2D(8715, Contour(XY(50,50),40, 20, 80), XY(30,10));

    DrawExpand2D(8716, Contour(XY(50,50),40, 20, 00), -XY(10,5));
    DrawExpand2D(8717, Contour(XY(50,50),40, 20, 10), -XY(10,5));
    DrawExpand2D(8718, Contour(XY(50,50),40, 20, 20), -XY(10,5));
    DrawExpand2D(8719, Contour(XY(50,50),40, 20, 30), -XY(10,5));
    DrawExpand2D(8720, Contour(XY(50,50),40, 20, 40), -XY(10,5));
    DrawExpand2D(8721, Contour(XY(50,50),40, 20, 50), -XY(10,5));
    DrawExpand2D(8722, Contour(XY(50,50),40, 20, 60), -XY(10,5));
    DrawExpand2D(8723, Contour(XY(50,50),40, 20, 70), -XY(10,5));
    DrawExpand2D(8724, Contour(XY(50,50),40, 20, 80), -XY(10,5));

    DrawExpand2D(8725, generated_forms::tri, XY(30,10));
    DrawExpand2D(8726, generated_forms::tri.CreateRotated(10), XY(30,10));
    DrawExpand2D(8727, generated_forms::tri.CreateRotated(20), XY(30,10));
    DrawExpand2D(8728, generated_forms::tri.CreateRotated(30), XY(30,10));
    DrawExpand2D(8729, generated_forms::tri.CreateRotated(40), XY(30,10));
    DrawExpand2D(8730, generated_forms::tri.CreateRotated(50), XY(30,10));
    DrawExpand2D(8731, generated_forms::tri.CreateRotated(60), XY(30,10));
    DrawExpand2D(8732, generated_forms::tri.CreateRotated(70), XY(30,10));
    DrawExpand2D(8733, generated_forms::tri.CreateRotated(80), XY(30,10));

    DrawExpand2D(8734, generated_forms::tri, -XY(10,5));
    DrawExpand2D(8735, generated_forms::tri.CreateRotated(10), -XY(10,5));
    DrawExpand2D(8736, generated_forms::tri.CreateRotated(20), -XY(10,5));
    DrawExpand2D(8737, generated_forms::tri.CreateRotated(30), -XY(10,5));
    DrawExpand2D(8738, generated_forms::tri.CreateRotated(40), -XY(10,5));
    DrawExpand2D(8739, generated_forms::tri.CreateRotated(50), -XY(10,5));
    DrawExpand2D(8740, generated_forms::tri.CreateRotated(60), -XY(10,5));
    DrawExpand2D(8741, generated_forms::tri.CreateRotated(70), -XY(10,5));
    DrawExpand2D(8742, generated_forms::tri.CreateRotated(80), -XY(10,5));

    DrawExpand2D(8743, generated_forms::circle, XY(30,10));
    DrawExpand2D(8744, generated_forms::concave, XY(30,10));
    DrawExpand2D(8745, generated_forms::cooomplex2[0], XY(30,10));
    DrawExpand2D(8746, generated_forms::cooomplex2[1], XY(30,10));
    DrawExpand2D(8747, generated_forms::cooomplex2, XY(30,10));

    DrawExpand2DBig(875, XY(4, 12), generated_forms::ChoppedBox);

    DrawExpand2DBig(876, XY(4, 12), generated_forms::forexpbevel);

    DrawExpand2DBig(877, XY(4, 12), generated_forms::box_circle, "box with miter");
    DrawExpand2DBig(878, XY(4, 12), generated_forms::variable_clear, "boxhole with miter");

    DrawExpand2DBig(879, XY(4, 12), generated_forms::later, "later with miter");
    DrawExpand2DBig(880, XY(4, 12), generated_forms::cooomplex, "complex with miter");
    DrawExpand2DBig(881, XY(4, 12), generated_forms::huhu, "huhu with miter");

    DrawExpand2DBig(882, XY(4, 12), generated_forms::part, "part with miter");

    DrawExpand2DBig(883, XY(4, 12), generated_forms::spart, "spart with round");

    DrawExpand2DBig(884, XY(4, 12), generated_forms::cooomplex3, "complex3 with miter");
    DrawExpand2DBig(885, XY(4, 12), generated_forms::cooomplex2, "rounded complex3 with miter");

    DrawExpand2DBig(886, XY(4, 12), generated_forms::form1, "pipe with miter");
    DrawExpand2DBig(887, XY(4, 12), generated_forms::form2, "reverse pipe with miter");
    DrawExpand2DBig(888, XY(4, 12), generated_forms::form3, "pipe with bigger circle with miter");
    DrawExpand2DBig(889, XY(4, 12), generated_forms::form4, "reverse pipe with bigger circle with miter");
    DrawExpand2DBig(890, XY(4, 12), generated_forms::form5, "two inverse circles with miter");
}


void contour_test_tangent()
{
    if (shouldnt_do(8400, 8423)) return;
    DrawTangent(8400, generated_forms::boxhole, generated_forms::boxhole.CreateShifted(XY(50,20)));
    DrawTangent(8401, generated_forms::boxhole, generated_forms::boxhole.CreateShifted(XY(50,-20)));
    DrawTangent(8402, generated_forms::boxhole, generated_forms::tri.CreateShifted(XY(150,20)));
    DrawTangent(8403, generated_forms::boxhole, Contour(XY(150,20), 10, 20, 30));
    DrawTangent(8404, Contour(XY(10,50), 30, 10, -10), Contour(XY(150,20), 10, 20, 30));
    DrawTangent(8405, generated_forms::boxhole, Contour(generated_forms::cooomplex2[0]).CreateShifted(XY(50,20)));
    DrawTangent(8406, generated_forms::boxhole, Contour(generated_forms::cooomplex2[1]).CreateShifted(XY(50,20)));
    DrawTangent(8407, generated_forms::boxhole, generated_forms::cooomplex2.CreateShifted(XY(50,20)));
    DrawTangent(8408, generated_forms::cooomplex2, Contour(XY(150,20), 10, 20, 30));
    DrawTangent(8409, generated_forms::cooomplex2.CreateRotated(10), generated_forms::cooomplex2.CreateShifted(XY(100,20)));

    DrawTangents(8420, Contour(XY(50, 250), 150, 50, -10));

    Edge tmp[1] = {Edge(XY(50, 250), XY(50, 300), XY(250, 50), XY(250, 500))};
    Contour cc;
    cc.assign<Edge>(std::span<Edge, 1>(tmp), ECloseType::CLOSE_ALL_CONNECTED);
    DrawTangents(8421, cc);

    Edge tmp2[1] = {Edge(XY(50, 250), XY(50, 300), XY(250, -250), XY(250, 50))};
    cc.assign<Edge>(std::span<Edge, 1>(tmp2), ECloseType::CLOSE_ALL_CONNECTED);
    DrawTangents(8422, cc);

    DrawTangents(8423, tmp[0], tmp2[0]);
}


void DrawBezier(CairoContext &c, const Edge &A, bool cp=false)
{
    if (!c.cr) return;
    cairo_move_to(c.cr, A.GetStart().x, A.GetStart().y);
    A.PathTo(c.cr);
    cairo_set_line_width(c.cr, 1);
    cairo_stroke(c.cr);
    if (!cp || A.IsStraight())
        return;
    cairo_set_line_width(c.cr, 0.1);
    cairo_move_to(c.cr, A.GetStart().x, A.GetStart().y);
    cairo_line_to(c.cr, A.GetC1().x, A.GetC1().y);
    cairo_new_sub_path(c.cr);
    cairo_arc(c.cr, A.GetC1().x, A.GetC1().y, 2, 0, 2*M_PI);
    cairo_close_path(c.cr);
    cairo_stroke(c.cr);
    cairo_move_to(c.cr, A.GetEnd().x, A.GetEnd().y);
    cairo_line_to(c.cr, A.GetC2().x, A.GetC2().y);
    cairo_new_sub_path(c.cr);
    cairo_arc(c.cr, A.GetC2().x, A.GetC2().y, 2, 0, 2*M_PI);
    cairo_close_path(c.cr);
    cairo_stroke(c.cr);
}

void DrawDot(CairoContext &c, XY p)
{
    if (!c.cr) return;
    cairo_arc(c.cr, p.x, p.y, 5, 0, 2*M_PI);
    cairo_fill(c.cr);
}

void DrawIntersection(CairoContext &c, Edge &A, Path &OK,
    double off=0)
{
    DrawBezier(c, A);
    for (unsigned uu = 0; uu<OK.size(); uu++) if (shall_draw()) {
        Edge edge(OK[uu].CreateShifted(XY(0, off*(uu+1))));
        DrawBezier(c, edge);
        double m[10], o[10];
        XY r[10];
        unsigned num = A.Crossing(OK[uu], false, r, m, o);
        for (unsigned u = 0; u<num; u++)
            DrawDot(c, r[u]+XY(0, off*(uu+1)));
    }
}

void DrawSelfIntersection(CairoContext &c, Edge &A)
{
    DrawBezier(c, A);
    double m[10], o[10];
    XY r[10];
    unsigned num = A.SelfCrossing(r, m, o);
    for (unsigned u = 0; u<num; u++)
        DrawDot(c, r[u]);
}

void DrawAngle(unsigned num, const Edge &a, double from, double step)
{
    if (shouldnt_do(num)) return;
    Block bbb(-10, 200, -10, 220);
    bbb += a.CreateBoundingBox().Expand(10);
    CairoContext ctx(num, bbb);
    cairo_set_source_rgb(ctx.cr, 0, 0, 0);
    //cairo_arc(ctx.cr, a.start.x, a.start.y, 2, 0, 2*M_PI);
    //cairo_fill(ctx.cr);
    cairo_move_to(ctx.cr, a.GetStart().x, a.GetStart().y);
    a.PathTo(ctx.cr);
    cairo_stroke(ctx.cr);
    for (unsigned u = 0; u<=10; u++) if (shall_draw()) {
        double pos = from+step*u;
        if (pos==0) continue; //no incoming at pos==0
        cairo_move_to(ctx.cr, 100, u*10);
        char buff[400];
        RayAngle angle = a.Angle(true, pos);
        sprintf(buff, "pos: %g, in, angle: (%g, %g)", pos, angle.angle, angle.curve);
        thread_safe_cairo_show_text(ctx.cr, buff);
    }
    for (unsigned u = 0; u<=10; u++) if (shall_draw()) {
        double pos = from+step*u;
        if (pos==1) continue;
        cairo_move_to(ctx.cr, 100, 110+u*10);
        char buff[400];
        RayAngle angle = a.Angle(false, pos);
        sprintf(buff, "pos: %g, out, angle: (%g, %g)", pos, angle.angle, angle.curve);
        thread_safe_cairo_show_text(ctx.cr, buff);
    }

}

void contour_test_bezier()
{
    if (shouldnt_do(8500, 8511)) return;

    Edge A(XY(10, 100), XY(110, 100));
    Path OK = {
        Edge(XY(20, 100), XY(120, 100)),
        Edge(XY(0, 100), XY(120, 100)),
        Edge(XY(20, 100), XY(90, 100)),
        Edge(XY(0, 100), XY(10, 100)),
        Edge(XY(0, 100), XY(5, 100)),
        Edge(XY(110, 100), XY(120, 100)),
        Edge(XY(115, 100), XY(120, 100)),
        Edge(XY(50, 50), XY(75, 150))
    };
    {
        CairoContext ctx(8500, Block(-10, 200, -10, 200), "bezier Intersection");
        DrawIntersection(ctx, A, OK, 10);
    }

    {
        CairoContext c(8501, Block(-10, 200, -10, 200), "bezier split");
        Edge B(XY(10, 100), XY(110, 100), XY(10, 50), XY(110, 50)), C, D, E, F;
        DrawBezier(c, B);
        B.Split(C, D);
        C.Split(0.2, E, F);
        DrawBezier(c, C.CreateShifted(XY(2, 2)));
        DrawBezier(c, D.CreateShifted(XY(4, 4)));
        DrawBezier(c, E.CreateShifted(XY(6, 6)));
        DrawBezier(c, F.CreateShifted(XY(8, 8)));
    }


    Edge B(XY(10, 100), XY(110, 100), XY(10, 50), XY(110, 50));
    Path OK2 = {
        Edge(XY(50, 50), XY(75, 150)),
        Edge(XY(10, 50), XY(75, 150)),
        Edge(XY(80, 50), XY(45, 150), XY(100, 90), XY(70, 150)),
        Edge(XY(100, 50), XY(45, 50), XY(120, 140), XY(20, 150))
    };
    {
        CairoContext ctx(8502, Block(-10, 200, -10, 200), "bezier Intersection 2");
        DrawIntersection(ctx, B, OK2);
    }

    auto b = B.Angle(false, 0);
    auto c = B.Angle(true, 0.5);
    auto d = B.Angle(false, 0.5);
    auto e = B.Angle(true, 1);

    e = b = c = d = e; //to supress unused variable warning


    {
        CairoContext c(8503, Block(-10, 200, -10, 200), "bezier bounding box");
        Edge B(XY(10, 100), XY(110, 100), XY(10, 50), XY(110, 50)), C, D, E, F;
        DrawBezier(c, B);
        B.Split(C, D);
        C.Split(E, F);
        Block BB = B.CreateBoundingBox();
        if (c.cr) {
            cairo_rectangle(c.cr, BB.x.from, BB.y.from, BB.x.Spans(), BB.y.Spans());
            cairo_stroke(c.cr);
        }
    }

    {
        CairoContext c(8504, Block(-10, 200, -10, 200), "bezier self intersection");
        Edge s(XY(50, 100), XY(100, 100), XY(100, 50), XY(50, 50));
        DrawSelfIntersection(c, s);
        Edge s2(XY(50, 100), XY(100, 100), XY(200, 50), XY(00, 50)); s2.Shift(XY(0, 100));
        DrawSelfIntersection(c, s2);
        DrawSelfIntersection(c, s2.SwapXY());
    }

    Edge B1(B, 0, 0.7), B2(B, 0.3, 1);
    XY r[Edge::MAX_CP];
    double p1[Edge::MAX_CP], p2[Edge::MAX_CP];
    unsigned ret = B1.Crossing(B2, false, r, p1, p2);
    ret++; //to supress unused variable warning
    {
        Path OK3 = {B2};
        CairoContext ctx(8505, Block(-10, 200, -10, 200), "bezier Intersection self");
        DrawIntersection(ctx, B1, OK3);
    }

    Path a;
    a.AppendEllipse(XY(10, 100), 10, 50, 0, 270, 360);
    DrawAngle(8506, a.back(), 0, 0.1);
    a.AppendEllipse(XY(10, 100), 10, 50, 0,360, 270, false);
    DrawAngle(8507, a.back(), 0, 0.1);
    a.AppendEllipse(XY(285, 20), 10, 18, 0, 270, 360);
    DrawAngle(8508, a.back(), 0, 0.1);
    a.AppendEllipse(XY(285, 20), 10, 18, 0, 180, 270);
    DrawAngle(8509, a.back(), 0, 0.1);

    Edge e1(XY(283.83345408981000, 6.9949537647505968), XY(285, 6.5),
        XY(284.51611207506727, 6.4752218017438317), XY(284.45140398179484, 6.5));
    DrawAngle(8510, e1, 0.9, 0.01);
    Edge e2(XY(286, 6.5), XY(286.16654591019000, 6.99495376475059680),
        XY(285.54859601820516, 6.5), XY(285.48388792493273, 6.4752218017438317));
    DrawAngle(8511, e2, 0, 0.01);
}



void DrawExpandedEdge(unsigned num, const Edge &B, double from, double to, double step=4)
{
    if (shouldnt_do(num)) return;
    CairoContext c(num, B.CreateBoundingBox().Expand(100) , "bezier split");
    cairo_set_source_rgb(c.cr, 0, 1, 0);
    for (double u = from; u<=to; u += step) if (shall_draw()) {
        if (u==0) {
            cairo_set_source_rgb(c.cr, 0, 0, 1);
            continue;
        }
        std::list<Edge> expanded;
        XY t1, t2;
        B.CreateExpand(u, expanded, t1, t2);
        for (auto &e: expanded) {
            DrawBezier(c, e);
            cairo_arc(c.cr, e.GetStart().x, e.GetStart().y, 1.5, 0, 2*M_PI);
            cairo_fill(c.cr);
        }
    }
    cairo_set_source_rgb(c.cr, 0, 0, 0);
    DrawBezier(c, B, true);
};


void contour_test_expand_edge()
{
    if (shouldnt_do(371, 376)) return;

    Edge B(XY(10, 100), XY(110, 100), XY(10, 50), XY(110, 50));
    DrawExpandedEdge(371, B, -100, 100);
    Edge C(XY(363.35930691517569, 112.10491300035402), XY(360.17853270272951, 111.28309269662574),
        XY(361.51724681455380, 114.13457366503485), XY(364.78943702329889, 111.59984873101280));
    DrawExpandedEdge(372, C.CreateScaled(XY(10,10)), -4, 4);
    Edge D(XY(10, 10), XY(100, 20), XY(60, 10), XY(100, 14));
    DrawExpandedEdge(373, D, -100, 100);
    DrawExpandedEdge(374, D.CreateScaled(XY(0.5, 5)), -100, 100);
    Edge E(XY(358.54443175183923, 112.67285885729200), XY(359.15216313510109, 111.24507956375152),
        XY(358.51438629292340, 112.76784570986187), XY(359.13316525536862, 111.28086760759919));
    DrawExpandedEdge(375, E.CreateScaled(XY(50, 50)), 1, -1);
    Edge F(XY(358.54443175183923, 112.67285885729200), XY(359.15216313510109, 111.24507956375152),
        XY(358.51438629292340, 112.76784570986187), XY(359.13316525536862, 111.28086760759919));
    //DrawExpandedEdge(376, E.CreateScaled(XY(500, 500)), 1, -1);
    XY r[9];
    double p1[9], p2[9];
    unsigned n = F.CreateScaled(XY(500, 100)).SelfCrossing(r, p1, p2);
    n++; //to supress unused variable warning
    n = F.SelfCrossing(r, p1, p2);
}



void DrawPath(unsigned num, const Path &p, double r=0, double g=0, double b=0)
{
    if (shouldnt_do(num)) return;
    Block bb = p.CalculateBoundingBox().Expand(10);
    Block cc = bb;
    cc += bb.UpperLeft()+bb.Spans().Scale(XY(1.5, 2));  //3x2 panels, x will be doubled
    CairoContext c(num, cc, "How we untangle paths to countour. Original is up-left.");
    p.CairoPath(c.cr, false);
    cairo_set_source_rgb(c.cr, r, g, b);
    cairo_stroke(c.cr);
    for (unsigned u = 0; u<4; u++) if (shall_draw()) {
        XY sh = bb.Spans().Scale(XY((u+1)%3, (u+1)/3));
        Contour contour(p, u&1 ? ECloseType::CLOSE_ALL_CONNECTED : ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT, (u&2) != 0);
        contour.Shift(sh);
        contour.CairoPath(c.cr, false);
        cairo_set_source_rgba(c.cr, r, g, b, 0.5);
        cairo_fill(c.cr);
        contour.CairoPath(c.cr, false);
        cairo_set_source_rgb(c.cr, r, g, b);
        cairo_stroke(c.cr);
        sh += bb.UpperLeft();
        cairo_move_to(c.cr, sh.x, sh.y);
        char buff[400];
        sprintf(buff, "%s, %s", u&1 ? "close_all" : "not close_all", u&2 ? "winding rule" : "evenodd rule");
        cairo_set_font_size(c.cr, 10);
        thread_safe_cairo_show_text(c.cr, buff);
    }
}


void contour_test_path_untangle()
{
    if (shouldnt_do(8600)) return;
    std::vector<XY> a = {
        XY(10, 10), XY(200, 10), XY(200, 50), XY(180, 50), XY(180, 30),
        XY(220, 30), XY(220, 150), XY(10,150)}; //clockwise, but open
    std::vector<XY> b = {
        XY(20, 20), XY(30, 20), XY(30, 30), XY(20, 30), XY(20,20)};  //clockwise inside "a" and closed
    std::vector<XY> c = {
        XY(20, 80), XY(30, 80), XY(30, 70), XY(20, 70)}; //counterclockwise, inside "a" and open
    std::vector<XY> d = {
        XY(20, 180), XY(30, 180), XY(30, 170), XY(20,170)}; //counterclockwise and open
    std::vector<XY> e = {
        XY(100, 80), XY(110, 80), XY(110, 70), XY(100, 70), XY(100, 80)}; //counterclockwise, inside "a" and closed
    Path p(a);
    p.append(b).append(c).append(d).append(e);
    DrawPath(8600, p);
}


void DrawPaths(unsigned num, const std::list<std::pair<Path, ColorType>> &list, const char *msg)
{
    if (shouldnt_do(num)) return;
    Block bb(false);
    for (auto &p : list)
        bb += p.first.CalculateBoundingBox();
    if (bb.IsInvalid()) bb = Block(0, 0, 0, 0);
    bb.Expand(10);
    CairoContext c(num, bb, msg);
    for (auto &p : list) {
        p.first.CairoPath(c.cr, false);
        cairo_set_source_rgb(c.cr, p.second.r/255., p.second.g/255., p.second.b/255.);
        cairo_stroke(c.cr);
    }
}

void linear_helper(unsigned num, std::list<std::pair<Path, ColorType>> &list,
                   const PathPos &pos, unsigned snum,
                   const double *gapl, const double *gapr, const double *length,
                   const char *msg, bool trim = false)
{
    const auto &path = list.back();
    Edge::Update u = gapr[1] ? Edge::Update() : Edge::Update::clear_visible();
    list.emplace_back(path.first.LinearWidenAsymetric(
        pos, snum, gapl, gapr, length,
        Edge::Update(), u, trim
    ).SetVisible(true), ColorType(0, 255, 0));
    list.emplace_back(path.first.LinearWidenAsymetric(
        pos, snum, gapl, gapr, length,
        Edge::Update(), u, trim
    ), ColorType(255, 0, 0));
    DrawPaths(num, list, msg);
    list.pop_back();
    list.pop_back();
}

void contour_test_path_linear()
{
    if (shouldnt_do(8601, 8691)) return;

    const double shape[] = {0, 20, 10, 0};
    const double shape_null[] = {0, 0, 0, 0};
    const double fw_len[] = {  20, 10, 20};
    const double bw_len[] = { -20,-10,-20};
    Path p;
    std::list<std::pair<Path, ColorType>> list;

    p = Path({Edge(XY(30, 30), XY(50, 50), XY(50, 40), XY(30, 40))});
    list.emplace_back(p.LinearExtend(10, true), ColorType(0, 255, 0));
    list.emplace_back(p, ColorType(0, 0, 0));
    DrawPaths(8601, list, "LinearExtend - forward 10 of a bezier");
    list.clear();
    list.emplace_back(p.LinearExtend(10, false), ColorType(0, 255, 0));
    list.emplace_back(p, ColorType(0, 0, 0));
    DrawPaths(8602, list, "LinearExtend - backward 10 of a bezier");
    list.clear();
    p = Path({Edge(XY(10, 30), XY(50, 30))});
    p = Path({Edge(XY(50, 30), XY(50, 70))});
    list.emplace_back(p.LinearExtend(10, true), ColorType(0, 255, 0));
    list.emplace_back(p, ColorType(0, 0, 0));
    DrawPaths(8603, list, "LinearExtend - forward 10 of a straight ");
    list.clear();
    list.emplace_back(p.LinearExtend(10, false), ColorType(0, 255, 0));
    list.emplace_back(p, ColorType(0, 0, 0));
    DrawPaths(8691, list, "LinearExtend - backward 10 of a straight ");

    list.clear();
    p = Path({Edge(XY(10, 30), XY(80, 30))});
    list.emplace_back(p, ColorType(0, 0, 0));
    linear_helper(8604, list, PathPos(0, 0.1), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on straight - this is the basic shape");
    linear_helper(8605, list, PathPos(0, 0.5), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on straight - should extend");
    linear_helper(8606, list, PathPos(0, 0.5), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on straight - should trim", true);
    linear_helper(8607, list, PathPos(0, 1), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on straight - totally after");

    list.clear();
    p = Path({Edge(XY(10, 30), XY(80, 30))});
    list.emplace_back(p, ColorType(0, 0, 0));
    linear_helper(8608, list, PathPos(0, 0.9), sizeof(bw_len)/sizeof(double), shape, shape, bw_len, "LinearWiden - backwards");
    linear_helper(8609, list, PathPos(0, 0.5), sizeof(bw_len)/sizeof(double), shape, shape, bw_len, "LinearWiden - backwards - should extend");
    linear_helper(8610, list, PathPos(0, 0.5), sizeof(bw_len)/sizeof(double), shape, shape, bw_len, "LinearWiden - backwards - should trim", true);
    linear_helper(8611, list, PathPos(0, 0.1), sizeof(bw_len)/sizeof(double), shape, shape, bw_len, "LinearWiden - backwards - totally before");

    list.clear();
    p = Path({Edge(XY(10, 30), XY(80, 30))});
    list.emplace_back(p, ColorType(0, 0, 0));
    linear_helper(8612, list, PathPos(0, 0.1), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on straight - this is the basic shape", false);
    linear_helper(8613, list, PathPos(0, 0.5), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on straight - should extend", false);
    linear_helper(8614, list, PathPos(0, 0.5), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on straight - should trim", true);
    linear_helper(8615, list, PathPos(0, 1), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on straight - totally after", false);

    list.clear();
    p = Path({Edge(XY(10, 30), XY(80, 30))});
    list.emplace_back(p, ColorType(0, 0, 0));
    linear_helper(8616, list, PathPos(0, 0.9), sizeof(bw_len)/sizeof(double), shape, shape_null, bw_len, "LinearWiden - one only - backwards", false);
    linear_helper(8617, list, PathPos(0, 0.5), sizeof(bw_len)/sizeof(double), shape, shape_null, bw_len, "LinearWiden - one only - backwards - should extend", false);
    linear_helper(8618, list, PathPos(0, 0.5), sizeof(bw_len)/sizeof(double), shape, shape_null, bw_len, "LinearWiden - one only - backwards - should trim", true);
    linear_helper(8619, list, PathPos(0, 0.1), sizeof(bw_len)/sizeof(double), shape, shape_null, bw_len, "LinearWiden - one only - backwards - totally before", false);

    list.clear();
    p = Path({Edge(XY(10, 30), XY(30, 30))});
    p.append(Edge(XY(30, 30), XY(50, 50), XY(50, 30), XY(50, 30)));
    p.append(Edge(XY(50, 50), XY(50, 70)));
    list.emplace_back(p, ColorType(0, 0, 0));
    linear_helper(8620, list, PathPos(0, 0), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier");
    linear_helper(8621, list, PathPos(0, 0.2), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier");
    linear_helper(8622, list, PathPos(0, 0.4), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier");
    linear_helper(8623, list, PathPos(0, 0.6), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier");
    linear_helper(8624, list, PathPos(0, 0.8), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier");
    linear_helper(8625, list, PathPos(0, 1), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier");
    linear_helper(8626, list, PathPos(1, 0.2), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should extend");
    linear_helper(8627, list, PathPos(1, 0.4), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should extend");
    linear_helper(8628, list, PathPos(1, 0.6), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should extend");
    linear_helper(8629, list, PathPos(1, 0.8), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should extend");
    linear_helper(8630, list, PathPos(1, 1), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should extend");
    linear_helper(8631, list, PathPos(1, 0.2), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should trim", true);
    linear_helper(8632, list, PathPos(1, 0.4), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should trim", true);
    linear_helper(8633, list, PathPos(1, 0.6), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should trim", true);
    linear_helper(8634, list, PathPos(1, 0.8), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should trim", true);
    linear_helper(8635, list, PathPos(1, 1), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - on bezier - should trim", true);

    linear_helper(8636, list, PathPos(0, 0), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier", false);
    linear_helper(8637, list, PathPos(0, 0.2), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier", false);
    linear_helper(8638, list, PathPos(0, 0.6), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier", false);
    linear_helper(8639, list, PathPos(0, 0.8), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier", false);
    linear_helper(8640, list, PathPos(0, 1), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier", false);
    linear_helper(8641, list, PathPos(1, 0.2), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier - should extend", false);
    linear_helper(8642, list, PathPos(1, 0.6), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier - should extend", false);
    linear_helper(8643, list, PathPos(1, 1), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier - should extend", false);
    linear_helper(8644, list, PathPos(1, 0.4), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier - should trim", true);
    linear_helper(8645, list, PathPos(1, 0.8), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier - should trim", true);
    linear_helper(8646, list, PathPos(1, 1), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - on bezier - should trim", true);

    list.clear();
    p = Path({Edge(XY(10, 30), XY(50, 30))});
    p.append(Edge(XY(50, 30), XY(80, 70)));
    list.emplace_back(p, ColorType(0, 0, 0));
    linear_helper(8647, list, PathPos(0, 0), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - break at angle");
    linear_helper(8648, list, PathPos(0, 0.2), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - break at angle");
    linear_helper(8649, list, PathPos(0, 0.4), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - break at angle");
    linear_helper(8650, list, PathPos(0, 0.5), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - break at angle");
    linear_helper(8651, list, PathPos(0, 0.6), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - break at angle");
    linear_helper(8652, list, PathPos(0, 0.7), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - break at angle");
    linear_helper(8653, list, PathPos(0, 0.8), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - break at angle");
    linear_helper(8654, list, PathPos(0, 0.9), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - break at angle");
    linear_helper(8655, list, PathPos(1, 0), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - should extend");
    linear_helper(8656, list, PathPos(1, 0.2), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - should extend");
    linear_helper(8657, list, PathPos(1, 0.4), sizeof(fw_len)/sizeof(double), shape, shape, fw_len, "LinearWiden - should extend");

    linear_helper(8658, list, PathPos(0, 0), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - break at angle", false);
    linear_helper(8659, list, PathPos(0, 0.4), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - break at angle", false);
    linear_helper(8660, list, PathPos(0, 0.7), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - break at angle", false);
    linear_helper(8661, list, PathPos(0, 0.8), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - break at angle", false);
    linear_helper(8662, list, PathPos(1, 0), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - should extend", false);
    linear_helper(8663, list, PathPos(1, 0.4), sizeof(fw_len)/sizeof(double), shape, shape_null, fw_len, "LinearWiden - one only - should extend", false);
}


void DrawCoordinateSpace(unsigned num, std::list<std::pair<Path, ColorType>> &list, const char *msg = nullptr)
{
    if (shouldnt_do(num)) return;
    Block bb(false);
    for (auto &p : list)
        bb += p.first.CalculateBoundingBox();
    bb += Block(-10, 10, -10, 10); //include origin
    bb.Expand(10);
    CairoContext c(num, bb, msg);
    cairo_move_to(c.cr, bb.x.from, 0);
    cairo_line_to(c.cr, bb.x.till, 0);
    cairo_stroke(c.cr);
    cairo_move_to(c.cr, 0, bb.y.from);
    cairo_line_to(c.cr, 0, bb.y.till);
    cairo_stroke(c.cr);
    for (unsigned u = 0; u<4; u++) {
        XY incr(10, 0);
        XY delta(0, 3);
        if (u&1) { incr.SwapXY(); delta.SwapXY(); }
        if (u&2) { incr *= -1; }
        for (XY run = incr; bb.IsWithinBool(run); run += incr) {
            cairo_move_to(c.cr, (run+delta).x, (run+delta).y);
            cairo_line_to(c.cr, (run-delta).x, (run-delta).y);
            cairo_stroke(c.cr);
        }
    }
    for (auto &p : list) {
        p.first.CairoPath(c.cr, false);
        cairo_set_source_rgb(c.cr, p.second.r/255., p.second.g/255., p.second.b/255.);
        cairo_stroke(c.cr);
    }
}

void contour_test_transform()
{
    if (shouldnt_do(8800, 8812)) return;

    const Contour c = Contour(10, 30, 20, 50) + Contour(30, 40, 30, 40);
    std::list<std::pair<Path, ColorType>> list;
    TRMatrix M;

    list.emplace_back(c, ColorType(0, 0, 0));
    list.emplace_back(c.CreateRotatedAround(XY(10, 10), 30), ColorType(128, 0, 0));
    DrawCoordinateSpace(8800, list, "Rotate around 10,10 by 30 degrees (clockwise).");
    list.pop_back();

    list.emplace_back(c.CreateScaled(1.5), ColorType(128, 0, 0));
    DrawCoordinateSpace(8801, list, "Scaled x1.5.");
    list.pop_back();

    list.emplace_back(c.CreateScaled(XY(1.5, 1)), ColorType(128, 0, 0));
    DrawCoordinateSpace(8802, list, "Scaled x1.5 only x dir.");
    list.pop_back();

    list.emplace_back(c.CreateSwapXYd(), ColorType(128, 0, 0));
    DrawCoordinateSpace(8803, list, "SwapXY");
    list.pop_back();

    M = TRMatrix::CreateFlipXAxis(30);
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8804, list, "Transform: FlipXAxis(y=30)");
    list.pop_back();

    M = TRMatrix::CreateFlipYAxis(15);
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8805, list, "Transform: FlipYAxis(x=15)");
    list.pop_back();

    M = TRMatrix::CreateScale(XY(1.5, 2));
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8806, list, "Transform: Scale(1.5, 2)");
    list.pop_back();

    M = TRMatrix::CreateScale(-2);
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8807, list, "Transform: Scale(-2)");
    list.pop_back();

    M = TRMatrix::CreateRotate(cos(M_PI/6), sin(M_PI/6));
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8808, list, "Transform: Rotate(PI/6==30 degrees, clockwise)");
    list.pop_back();

    M = TRMatrix::CreateRotate(cos(-M_PI/3), sin(-M_PI/3));
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8809, list, "Transform: Rotate(-PI/3==60 degrees counterclockwise)");
    list.pop_back();

    M = TRMatrix::CreateShift(XY(-30,10));
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8810, list, "Transform: Shift(-30,10)");
    list.pop_back();

    M = TRMatrix::CreateRotate(15) * TRMatrix::CreateRotate(15);
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8811, list, "Transform: Rotate(15) * Rotate(15) == Rotate(30)");
    list.pop_back();


    M = TRMatrix::CreateShift(XY(-10, -10)) *
        TRMatrix::CreateRotate(15) *
        TRMatrix::CreateRotate(15) *
        TRMatrix::CreateShift(XY(10, 10));
    list.emplace_back(c.CreateTransformed(M), ColorType(128, 0, 0));
    DrawCoordinateSpace(8812, list, "Transform: Shift(-10,-10) * Rotate(15) * Rotate(15) * Shif(10,10) == RotateAround(10,10,30)");
    list.pop_back();
}

class TestCanvas
{
public:
    std::unique_ptr<Canvas> canvas;
    TestCanvas(unsigned i, const Block &place, const char *text = nullptr);
};


TestCanvas::TestCanvas(unsigned i, const Block &place, const char *text)
{
    if (shouldnt_do(i)) {
        canvas = std::make_unique<Canvas>(Canvas::Empty::Dummy); //dont draw or save anything
        return;
    }

    int sub = -1;
    char fileName[40];
    if (i>999) {
        if (i<=9999) { sub = i%10; i /= 10; } else if (i<=99999) { sub = i%100; i /= 100; } else if (i<=999999) { sub = i%1000; i /= 1000; } else if (i<=9999999) { sub = i%10000; i /= 10000; } else { _ASSERT(0); }
    }
    if (sub>=0)
        snprintf(fileName, sizeof(fileName), "test%u_%03u.png", i, sub);
    else
        snprintf(fileName, sizeof(fileName), "test%u.png", i);
    printf("%s\n", fileName);
    const Block total = place.CreateExpand(expand_by);
    canvas = std::make_unique<Canvas>(Canvas::PNG, total, 0, fileName,
                                      XY(size_by/2, size_by/2));
    if (CairoContext::font_face.size())
        cairo_select_font_face(canvas->GetContext(), CairoContext::font_face.c_str(), CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    canvas->Fill(total, FillAttr()); //white background
    if (text) {
        cairo_save(canvas->GetContext());
        cairo_scale(canvas->GetContext(), 8/size_by, 8/size_by);
        cairo_set_source_rgb(canvas->GetContext(), 0, 0, 0);
        cairo_move_to(canvas->GetContext(), 0, 0);
        thread_safe_cairo_show_text(canvas->GetContext(), text);
        cairo_restore(canvas->GetContext());
    }
}

Contour DrawStraightArrowhead(
    Canvas &c, const Edge &t, double pos, double twidth2,
    const ArrowHead &ah, const LineAttr &line1, const LineAttr &line2,
    bool cont, bool bidir)
{
    if (bidir) cont = true;
    const XY tip = t.Pos2Point(pos);
    const XY dir = (t.GetEnd()-tip).Rotate90CW().Normalize();
    const XY from = tip + dir*(50+twidth2);
    const XY to = tip - dir*(50+twidth2);
    Contour clip = ah.ClipForLine(tip+dir*twidth2, from, false, false, line1, line2, false);
    if (bidir) {
        Contour clip2 = ah.ClipForLine(tip-dir*twidth2, to, true, false, line2, line1, true);
#ifdef _DEBUG
        if (0)
            contour::debug::Snapshot("test.pdf", EC::STRINGFORMAT, "\\mn(1)", EC::START, "%n", EC::LINEWIDTH, 0.1, EC::MARKERWIDTH, 0.5,
                                 ColorType(255, 0, 0), clip[0].Outline(), ColorType(0, 0, 255), clip2[0].Outline());
#endif
        clip += std::move(clip2);
    }
    c.ClipInverse(clip);
    c.Line(from, tip+dir*twidth2, line1);
    if (cont)
        c.Line(tip-dir*twidth2, to, line2);
    c.UnClip();
    ah.Draw(c, tip+dir*twidth2, from, false, line1, line2, false);
    if (bidir)
        ah.Draw(c, tip-dir*twidth2, to, true, line2, line1, true);
    c.Fill(clip, FillAttr::Solid(ColorType(200, 200, 255, 200)));
    c.Line(clip, LineAttr(ELineType::SOLID, ColorType::blue(), 0.2));
    clip = ah.TargetCover(tip+dir*twidth2, from, false, line1, line2);
    if (bidir)
        clip += ah.TargetCover(tip-dir*twidth2, to, true, line2, line1);
    Contour cover = ah.Cover(tip+dir*twidth2, from, false, line1, line2, false).Expand(1);
    if (bidir)
        cover += ah.Cover(tip-dir*twidth2, to, true, line2, line1, true).Expand(1);
    cover.SetVisible(true);
    c.Line(cover, LineAttr(ELineType::SOLID, ColorType::red(), 0.2));
    return clip;
}

Contour DrawBentArrowhead(
    Canvas &c, const Edge &t, double pos, double twidth2,
    Path path, const PathPos &tip_pos,
    const ArrowHead &ah, const LineAttr &line1, const LineAttr &line2,
    bool cont, bool bidir)
{
    const XY tip = t.Pos2Point(pos);
    path.Shift(tip-path.GetPoint(tip_pos));//move tip to the right place on 't'arget
    path.RotateAround(tip,
        GetDegree(tip, t.GetEnd())-GetDegree(tip, path.GetTangent(tip_pos, true, false))-90);
    PathPos tip_pos1(tip_pos); path.MovePos(tip_pos1, -twidth2);
    PathPos tip_pos2(tip_pos); path.MovePos(tip_pos2,  twidth2);
    ArrowHeadCoverCache cache1, cache2;
    Contour cover;
    cover = ah.Cover(path, tip_pos1, true, false, line1, line2, false, &cache1).Expand(1, EXPAND_MITER, EXPAND_MITER, 2, 2);
    Contour clip = ah.ClipForLine(path, tip_pos1, true, false, false, line1, line2, false, &cache1);
    if (bidir) {
        Contour cover2 = ah.Cover(path, tip_pos2, false, true, line2, line1, true, &cache2).Expand(1, EXPAND_MITER, EXPAND_MITER, 2, 2);
#ifdef _DEBUG
        if (0) {
            contour::debug::Snapshot("test.pdf", EC::STRINGFORMAT, "\\mn(1)", EC::START, "%n", EC::LINEWIDTH, 0.1, EC::MARKERWIDTH, 0.5,
                                     ColorType(255, 0, 0), cover[0].Outline(), ColorType(0, 0, 255), cover2[0].Outline());
            std::string a = cover.Dump(true);
            std::string b = cover2.Dump(true);
        }
#endif
        cover += std::move(cover2);
        Contour clip2 = ah.ClipForLine(path, tip_pos2, false, true, false, line2, line1, true, &cache2);
#ifdef _DEBUG
        if (0)
            contour::debug::Snapshot("test.pdf", EC::STRINGFORMAT, "\\mn(1)", EC::START, "%n", EC::LINEWIDTH, 0.1, EC::MARKERWIDTH, 0.5,
                                     ColorType(255, 0, 0), clip[0].Outline(), ColorType(0, 0, 255), clip2[0].Outline());
#endif
        clip += std::move(clip2);
    }
    c.ClipInverse(clip);
    c.Line(path.CopyPart(path.GetStartPos(), tip_pos1), line1);
    c.Line(path.CopyPart(tip_pos2, path.GetEndPos()), line2);
    c.UnClip();
    ah.Draw(c, path, tip_pos1, true, false, line1, line2, false, &cache1);
    if (bidir)
        ah.Draw(c, path, tip_pos2, false, true, line2, line1, true, &cache2);
    c.Fill(clip, FillAttr::Solid(ColorType(200, 200, 255, 200)));
    c.Line(clip, LineAttr(ELineType::SOLID, ColorType::blue(), 0.2));
    clip = ah.TargetCover(path, tip_pos1, true, false, line1, line2, false, &cache1);
    if (bidir)
        clip += ah.TargetCover(path, tip_pos2, false, true, line2, line1, true, &cache2);
    cover.SetVisible(true);
    c.Line(cover, LineAttr(ELineType::SOLID, ColorType::red(), 0.2));
    return clip;
}

void DrawBlockArrowhead(
    Canvas &c, double x1, double x2a, double x2b, double x3, double sy, double dy,
    const ArrowHead &ah, const LineAttr &line, const FillAttr &fill)
{
    auto whoe = ah.BlockWidthHeight(dy-sy, line, true);
    auto whom = ah.BlockWidthHeight(dy-sy, line, false);
    Contour area;
    Path mid_path;
    Contour text[4][4];
    const double mid_y = (sy+dy)/2;
    const double inner_height = (dy-sy)-2*line.LineWidth();
    for (unsigned u = 0; u<4; u++)
        for (unsigned v=0; v<4; v++)
            text[u][v] = Block(0, 20, mid_y-inner_height/2*(v+1)*0.2, mid_y+inner_height/2*(v+1)*0.2);
    if (BreaksBlockArrow(ah.front().type)) {
        area = ah.BlockContour(x1, sy, dy, line, false).Transform(TRMatrix::CreateFlipYAxis(x1));
        area += Block(x1+whoe.body_connect_before, x2a-whom.body_connect_before, sy, dy);
        area += ah.BlockContour(x2a, sy, dy, line, true);
        area += ah.BlockContour(x2b, sy, dy, line, true).Transform(TRMatrix::CreateFlipYAxis(x2b));
        area += Block(x2b+whom.body_connect_before, x3-whoe.body_connect_before, sy, dy);
        area += ah.BlockContour(x3, sy, dy, line, false);
        for (unsigned u = 1; u<3; u++)
            for (unsigned v = 0; v<4; v++) {
                const double margin = ah.BlockTextMargin(text[u][v], u==2, sy, dy, line);
                const double target = u==1 ? x2a-margin : x2b+margin;
                const double source = u==1 ? text[u][v].GetBoundingBox().x.till : text[u][v].GetBoundingBox().x.from;
                text[u][v].Shift(XY(target-source, 0));
            }
    } else {
        area = ah.BlockContour(x1, sy, dy, line, false).Transform(TRMatrix::CreateFlipYAxis(x1));
        area += Block(x1+whoe.body_connect_before, (x2a+x2b)/2-whom.body_connect_before, sy, dy);
        area += ah.BlockContour((x2a+x2b)/2, sy, dy, line, true);
        area += Block((x2a+x2b)/2+whom.body_connect_after, x3-whoe.body_connect_before, sy, dy);
        area += ah.BlockContour(x3, sy, dy, line, false);
        mid_path = ah.BlockMidPath((x2a+x2b)/2, sy, dy, line);
        for (unsigned u = 1; u<3; u++)
            for (unsigned v = 0; v<4; v++)
                text[u][v].clear();
    }
    for (unsigned u = 0; u<=3; u += 3)
        for (unsigned v = 0; v<4; v++) {
            const double margin = ah.BlockTextMargin(text[u][v], u==0, sy, dy, line);
            const double target = u==0 ? x1+margin : x3-margin;
            const double source = u==0 ? text[u][v].GetBoundingBox().x.from : text[u][v].GetBoundingBox().x.till;
            text[u][v].Shift(XY(target-source, 0));
        }
    c.Fill(area.CreateExpand(*line.width/2-line.LineWidth()), fill);
    c.Line(area.CreateExpand(-line.LineWidth()/2), line);
    c.Line(mid_path, line);
    ColorType color[4] = {{128,128,128,192}, {255,128,128,192},{128,255,128,192},{128,128,255,192}};
    for (unsigned u = 0; u<4; u++)
        for (unsigned v = 0; v<4; v++) {
            c.Fill(text[u][v], FillAttr::Solid(color[v]));
            c.Line(text[u][v], LineAttr(ELineType::SOLID, color[v].MoreOpaque(1), 0.2));
        }
    const double lm2 = 0.5;
    LineAttr lmargin(ELineType::DASHED, ColorType::black(), 2*lm2);
    const double y1 = sy-(dy-sy)/2;
    const double y2 = dy+(dy-sy)/2;
    c.Line(XY(x1-whoe.after_margin-lm2, y1), XY(x1-whoe.after_margin-lm2, y2), lmargin);
    c.Line(XY(x1+whoe.before_margin+lm2, y1), XY(x1+whoe.before_margin+lm2, y2), lmargin);
    c.Line(XY(x2a-whom.before_margin-lm2, y1), XY(x2a-whom.before_margin-lm2, y2), lmargin);
    c.Line(XY(x2b+whom.before_margin+lm2, y1), XY(x2b+whom.before_margin+lm2, y2), lmargin);
    c.Line(XY(x3-whoe.before_margin-lm2, y1), XY(x3-whoe.before_margin-lm2, y2), lmargin);
    c.Line(XY(x3+whoe.after_margin+lm2, y1), XY(x3+whoe.after_margin+lm2, y2), lmargin);
}

void test_arrowheads()
{
    if (shouldnt_do(8900, 8940)) return;
    if (!shouldnt_do(8900)) {
        const double l = 2;
        LineAttr tline(ELineType::SOLID, ColorType(0, 0, 128), l*2);
        LineAttr aline(ELineType::DOUBLE, ColorType(0, 0, 0), 2);
        FillAttr afill = FillAttr::Solid(ColorType(255, 255, 240));
        const Block place(0, 500, 0, 800);
        const double l1 = place.x.from + place.x.Spans()*0.1, l2 = place.x.MidPoint(), l3 = place.x.from + place.x.Spans()*0.9;
        const double x1 = l1+l, x2a = l2-l, x2b = l2+l, x3 = l3-l;
        TestCanvas tc(8900, place, "Block arrows");
        double pos = 10, increment = 50, size = 0.2;
        tc.canvas->Line(Edge(XY(l1, place.y.from), XY(l1, place.y.till)), tline);
        tc.canvas->Line(Edge(XY(l2, place.y.from), XY(l2, place.y.till)), tline);
        tc.canvas->Line(Edge(XY(l3, place.y.from), XY(l3, place.y.till)), tline);
        for (unsigned u = 1; u<unsigned(ESingleArrowType::LINE); u++, pos += increment) if (shall_draw())
            DrawBlockArrowhead(*tc.canvas, x1, x2a, x2b, x3, pos, pos+increment/2,
                ArrowHead(ESingleArrowType(u), ESingleArrowSide::BOTH, true, size, aline),
                aline, afill);
        for (unsigned u = unsigned(ESingleArrowType::STRIPES); u<=unsigned(ESingleArrowType::TRIANGLE_STRIPES); u++, pos += increment) if (shall_draw())
            DrawBlockArrowhead(*tc.canvas, x1, x2a, x2b, x3, pos, pos+increment/2,
                ArrowHead(ESingleArrowType(u), ESingleArrowSide::BOTH, true, size, aline),
                aline, afill);
        for (unsigned u = 0; u<3; u++, pos += increment) if (shall_draw())
            DrawBlockArrowhead(*tc.canvas, x1, x2a, x2b, x3, pos, pos+increment/2,
                ArrowHead(ESingleArrowType::INV, ESingleArrowSide(u), true, size, aline),
                aline, afill);
    }
    Block place(0, 1300, 0, 390);
    Edge target(XY(0, 50), XY(1250, 300));
    LineAttr tline(ELineType::SOLID, ColorType(0, 0, 128), 4);
    LineAttr aline1(ELineType::SOLID, ColorType(0, 0, 0));
    LineAttr aline2(ELineType::DOUBLE, ColorType(0, 0, 0));
    LineAttr arr(ELineType::SOLID, ColorType(0, 128, 0), 1, ECornerType::BEVEL);

    const ArrowHead exotics[] = {
        ArrowHead(EArrowType::LINE, false, 1, arr) + ArrowHead(EArrowType::HALF, false, 1, arr) +
            ArrowHead(EArrowType::LINE, false, 1, arr) + ArrowHead(EArrowType::HALF, false, 1, arr),
        ArrowHead(EArrowType::DOT, false, 0.3, LineAttr(ELineType::SOLID, ColorType::red()))+
            ArrowHead(EArrowType::EMPTY, false, 1, arr),
        ArrowHead(EArrowType::LINE, false, 0.5, LineAttr(ELineType::SOLID, ColorType::blue(), 2)) +
            ArrowHead(EArrowType::DIAMOND, false, 1, arr) +
            ArrowHead(EArrowType::LINE, false, 1, arr) + ArrowHead(EArrowType::HALF, false, 1, arr),
        ArrowHead(EArrowType::LINE, false, 0.5, LineAttr(ELineType::SOLID, ColorType::blue(), 2)) +
            ArrowHead(EArrowType::SOLID, false, 1, arr),
        ArrowHead(EArrowType::DOT, false, 0.2, arr) + ArrowHead(EArrowType::DOT_EMPTY, false, 1, arr),
        ArrowHead(EArrowType::DIAMOND, false, 0.2, arr) + ArrowHead(EArrowType::NSDIAMOND, false, 1, arr),
        ArrowHead(EArrowType::NSDIAMOND, false, 1, arr) + ArrowHead(EArrowType::NSDIAMOND, false, 0.2, arr),
        ArrowHead(EArrowType::VEE, false, 1, arr) + ArrowHead(EArrowType::VEE_EMPTY, false, 0.2, arr),
        ArrowHead(EArrowType::SHARP, false, 1, arr) + ArrowHead(EArrowType::VEE, false, 1, arr),
        ArrowHead(EArrowType::LINE, false, 1, arr) + ArrowHead(EArrowType::SOLID, false, XY(1,0.5), arr),
    };

    std::vector<LineAttr> lines = {arr, LineAttr(*arr.type, *arr.color, 10, *arr.corner, *arr.radius)};
    for (unsigned uu = 0; uu<lines.size(); uu++) {
        if (!shouldnt_do(8901+uu*6)) {
            TestCanvas tc(8901 + uu*6, place, "Straight, All heads, part 1");
            Contour clip;
            double pos = 0, increment = 0.016;
            for (unsigned u = 1; u<unsigned(ESingleArrowType::INV); u++)
                for (unsigned b = 0; b<=1; b++)
                    for (unsigned v = 0; v<=unsigned(ESingleArrowSide::RIGHT); v++, pos += increment) if (shall_draw())
                        clip += DrawStraightArrowhead(*tc.canvas, target, pos,
                            IsSymmetricAndNotNone(ESingleArrowType(u)) ? 0 : tline.LineWidth()/2,
                            ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), b==1),
                            aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8902+uu*6)) {
            TestCanvas tc(8902 + uu*6, place, "Straight, All heads, part 2");
            Contour clip;
            double pos = 0, increment = 0.016;
            for (unsigned u = unsigned(ESingleArrowType::INV); u<=unsigned(ESingleArrowType::JUMPOVER); u++)
                for (unsigned b = 0; b<=1; b++)
                    for (unsigned v = 0; v<=unsigned(ESingleArrowSide::RIGHT); v++, pos += increment) if (shall_draw())
                        clip += DrawStraightArrowhead(*tc.canvas, target, pos,
                            IsSymmetricAndNotNone(ESingleArrowType(u)) ? 0 : tline.LineWidth()/2,
                            ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), b==1),
                            aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8903+uu*6)) {
            TestCanvas tc(8903 + uu*6, place, "Straight, All heads three times");
            Contour clip;
            double pos = 0, increment = 0.016;
            for (unsigned u = 1; u<=unsigned(ESingleArrowType::JUMPOVER); u++)
                for (unsigned v = 0; v<=unsigned(ESingleArrowSide::RIGHT); v++, pos += increment) if (shall_draw())
                    clip += DrawStraightArrowhead(*tc.canvas, target, pos,
                        IsSymmetricAndNotNone(ESingleArrowType(u)) ? 0 : tline.LineWidth()/2,
                        ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), false) +
                        ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), true) +
                        ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), false),
                        aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8904+uu*6)) {
            TestCanvas tc(8904 + uu*6, place, "Straight, single Arrowheads");
            Contour clip;
            for (unsigned u = 1; u<=unsigned(EArrowType::TRIPLE_HALF); u++) if (shall_draw())
                clip += DrawStraightArrowhead(*tc.canvas, target, u*0.038 - 0.03,
                    IsSymmetricAndNotNone(EArrowType(u)) ? 0 : tline.LineWidth()/2,
                    ArrowHead(EArrowType(u), false, 1, lines[uu]),
                    aline1, aline2, true, false);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8905+uu*6)) {
            TestCanvas tc(8905 + uu*6, place, "Straight, bidir Arrowheads");
            Contour clip;
            for (unsigned u = 1; u<=unsigned(EArrowType::TRIPLE_HALF); u++) if (shall_draw())
                clip += DrawStraightArrowhead(*tc.canvas, target, u*0.038 - 0.03,
                    IsSymmetricAndNotNone(EArrowType(u)) ? 0 : tline.LineWidth()/2,
                    ArrowHead(EArrowType(u), false, 1, lines[uu]),
                    aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8906+uu*6)) {
            TestCanvas tc(8906 + uu*6, place, "Straight, exotic mix");
            Contour clip;
            for (unsigned u = 0; u<sizeof(exotics)/sizeof(*exotics); u++) if (shall_draw())
                clip += DrawStraightArrowhead(*tc.canvas, target, 0.05*(u+1),
                    exotics[u].StartsWithSymmetricAndNotNone() ? 0 : tline.LineWidth()/2,
                    exotics[u], aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
    }
    Path curvy({Edge(XY(0, 50), XY(50, 0), XY(30, 50), XY(50, 30))});
    curvy.append({XY(50, 0), XY(60, -20), XY(90, -40)});
    curvy.Scale(1.2);
    std::vector<PathPos> poss = {{0,0.4},{0,0.9},{1,1},{1,0.6}};
    lines = {arr, arr, arr, arr, LineAttr(*arr.type, *arr.color, 10, *arr.corner, *arr.radius)};
    for (unsigned uu = 0; uu<poss.size(); uu++) {
        if (!shouldnt_do(8913+uu*7)) {
            TestCanvas tc(8913 + uu*7, place, "Bent, All heads, part 1");
            Contour clip;
            double pos = 0, increment = 0.016;
            for (unsigned u = 1; u<unsigned(ESingleArrowType::INV); u++)
                for (unsigned b = 0; b<=1; b++)
                    for (unsigned v = 0; v<=unsigned(ESingleArrowSide::RIGHT); v++, pos += increment) if (shall_draw())
                        clip += DrawBentArrowhead(*tc.canvas, target, pos,
                            IsSymmetricAndNotNone(ESingleArrowType(u)) ? 0 : tline.LineWidth()/2,
                            curvy, poss[uu],
                            ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), b==1),
                            aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8914+uu*7)) {
            TestCanvas tc(8914+ uu*7, place, "Bent, All heads, part 2");
            Contour clip;
            double pos = 0, increment = 0.016;
            for (unsigned u = unsigned(ESingleArrowType::INV); u<=unsigned(ESingleArrowType::JUMPOVER); u++)
                for (unsigned b = 0; b<=1; b++)
                    for (unsigned v = 0; v<=unsigned(ESingleArrowSide::RIGHT); v++, pos += increment) if (shall_draw())
                        clip += DrawBentArrowhead(*tc.canvas, target, pos,
                            IsSymmetricAndNotNone(ESingleArrowType(u)) ? 0 : tline.LineWidth()/2,
                            curvy, poss[uu],
                            ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), b==1),
                            aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8915+uu*7)) {
            TestCanvas tc(8915 + uu*7, place, "Bent, All heads three times");
            Contour clip;
            double pos = 0, increment = 0.016;
            for (unsigned u = 1; u<=unsigned(ESingleArrowType::JUMPOVER); u++)
                for (unsigned v = 0; v<=unsigned(ESingleArrowSide::RIGHT); v++, pos += increment) if (shall_draw())
                    clip += DrawBentArrowhead(*tc.canvas, target, pos,
                        IsSymmetricAndNotNone(ESingleArrowType(u)) ? 0 : tline.LineWidth()/2,
                        curvy, poss[uu],
                        ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), false) +
                        ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), true) +
                        ArrowHead(ESingleArrowType(u), ESingleArrowSide(v), false),
                        aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8916+uu*7)) {
            TestCanvas tc(8916 + uu*7, place, "Bent, single Arrowheads");
            Contour clip;
            for (unsigned u = 1; u<=unsigned(EArrowType::TRIPLE_HALF); u++) if (shall_draw())
                clip += DrawBentArrowhead(*tc.canvas, target, u*0.038 - 0.03,
                    IsSymmetricAndNotNone(EArrowType(u)) ? 0 : tline.LineWidth()/2,
                    curvy, poss[uu],
                    ArrowHead(EArrowType(u), false, 1, lines[uu]),
                    aline1, aline2, true, false);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8917+uu*7)) {
            TestCanvas tc(8917 + uu*7, place, "Bent, bidir Arrowheads");
            Contour clip;
            for (unsigned u = 1; u<=unsigned(EArrowType::TRIPLE_HALF); u++) if (shall_draw())
                clip += DrawBentArrowhead(*tc.canvas, target, u*0.038 - 0.03,
                    IsSymmetricAndNotNone(EArrowType(u)) ? 0 : tline.LineWidth()/2,
                    curvy, poss[uu],
                    ArrowHead(EArrowType(u), false, 1, lines[uu]),
                    aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
        if (!shouldnt_do(8918+uu*7)) {
            TestCanvas tc(8918 + uu*7, place, "Bent, exotic mix");
            Contour clip;
            for (unsigned u = 0; u<sizeof(exotics)/sizeof(*exotics); u++) if (shall_draw())
                clip += DrawBentArrowhead(*tc.canvas, target, 0.05*(u+1),
                    exotics[u].StartsWithSymmetricAndNotNone() ? 0 : tline.LineWidth()/2,
                    curvy, poss[uu], exotics[u], aline1, aline2, true, true);
            tc.canvas->ClipInverse(clip);
            tc.canvas->Line(target, tline);
        }
    }
    if (!shouldnt_do(8940)) {
        Path abrupt = {XY(100, 0), XY(50,50), XY(40,40)};
        TestCanvas tc(8940, place, "abrupt");
        Contour clip;
        double pos = 0, increment = 0.016;
        for (double path_pos = 0.8; path_pos<0.95; path_pos+=0.005, pos += increment) if (shall_draw())
            clip += DrawBentArrowhead(*tc.canvas, target, pos,
                                      IsSymmetricAndNotNone(ESingleArrowType::NORMAL) ? 0 : tline.LineWidth()/2,
                                      abrupt, PathPos{0,path_pos},
                                      ArrowHead(ESingleArrowType::NORMAL, ESingleArrowSide::BOTH, false),
                                      aline1, aline1, true, true);
        //tc.canvas->ClipInverse(clip);
        //tc.canvas->Line(target, tline);
    }
}


void test_createrounded()
{
    if (shouldnt_do(9000, 9011)) return;

    ColorType colors[] = {ColorType::red(), ColorType::blue(), ColorType::green()};
    std::list<std::pair<Path, ColorType>> list;

    struct mm : public Edge
    {
        using Edge::Edge;
        bool eli = true;
        bool Eligible() const { return eli; }
    };

    contour::EdgeVector<mm> mmpath = {
        mm(XY(10,10), XY(10,50)),
        mm(XY(10,50), XY(50,50), XY(20,60), XY(40, 60)),
        mm(XY(50,50), XY(90, 50)),
        mm(XY(90,50), XY(90, 10)),
        mm(XY(90, 10), XY(130, 10)),
        mm(XY(130, 10), XY(170, 50))
    };
    mmpath[4].eli = false;

    list.clear();
    list.push_back({Path(mmpath.begin(), mmpath.end()), ColorType::black()});
    for (unsigned u = 0; u<1; u++) {
        auto mm = mmpath;
        contour::Smoothen(mm.begin(), mm.end(), 0.2 + 0.1*u, 3, false, false);
        list.push_back({Path(mm.begin(), mm.end()), colors[u%(sizeof(colors)/sizeof(ColorType))]});
    }
    DrawPaths(9011, list, "Mixed Smoothen with Eligible(), 0.2 + 0.1*u");


    list.clear();
    const XY pp[] = {{10, 10}, {50, 10}, {50, 50}, {100, 50}, {100, 100}, {150, 100}, { 150, 50 }, {200, 100}};
    Path p = pp;
    list.push_back({p, ColorType::black()});
    for (unsigned u = 0; u<7; u++)
        list.push_back({p.CreateRounded(u*3+3, false), colors[u%(sizeof(colors)/sizeof(ColorType))]});
    DrawPaths(9000, list, "Straight CreateRounded - round");

    list.clear();
    list.push_back({p, ColorType::black()});
    for (unsigned u = 0; u<7; u++) {
        Path x = p;
        contour::Smoothen(x.begin(), x.end(), 0.2 + 0.1*u, 3, false, false);
        list.push_back({x, colors[u%(sizeof(colors)/sizeof(ColorType))]});
    }
    DrawPaths(9010, list, "Straight Smoothen, 0.2 + 0.1*u");

    const XY pp2[] = {{10, 10},{50, 10},{50, 50},{100, 50},{150, 100},{150, 50},{200, 100}};
    p = Path(pp2);
    list.clear();
    list.push_back({p, ColorType::black()});
    for (unsigned u = 0; u<7; u++)
        list.push_back({p.CreateRounded(u*2+1, false), colors[u%(sizeof(colors)/sizeof(ColorType))]});
    DrawPaths(9001, list, "Straight 2 CreateRounded - round");

    const XY ppd[] = {{10, 10},{50, 10},{50, 50},{100, 50},{100, 100},{150, 100},{150, 50},{200, 100}};
    p = Path(ppd);
    p.erase(p.begin()+3);
    list.clear();
    list.push_back({p, ColorType::black()});
    for (unsigned u = 0; u<7; u++)
        list.push_back({p.CreateRounded(u*2+1, false), colors[u%(sizeof(colors)/sizeof(ColorType))]});
    DrawPaths(9002, list, "Straight disconnect CreateRounded - round");

    const XY ppc[] = {{10, 10},{50, 10},{50, 50},{100, 50},{100, 100},{150, 100},{150, 50},{200, 200},{10, 200},{10, 10}};
    p = Path(ppc);
    list.clear();
    list.push_back({p, ColorType::black()});
    for (unsigned u = 0; u<7; u++)
        list.push_back({p.CreateRounded(u*2+1, false), colors[u%(sizeof(colors)/sizeof(ColorType))]});
    DrawPaths(9003, list, "Straight circular CreateRounded - round");

    list.clear();
    list.push_back({p, ColorType::black()});
    for (unsigned u = 0; u<7; u++)
        list.push_back({p.CreateRounded(u*2+1, true), colors[u%(sizeof(colors)/sizeof(ColorType))]});
    DrawPaths(9004, list, "Straight circular CreateRounded - bevel");

    p = Path(generated_forms::spart);
    list.clear();
    list.push_back({p, ColorType::black()});
    for (unsigned u = 0; u<7; u++)
        list.push_back({p.CreateRounded(u*2+1, false), colors[u%(sizeof(colors)/sizeof(ColorType))]});
    DrawPaths(9005, list, "curvy circular CreateRounded - round");

    p = Contour({100,100}, 50) - Block(10, 85, 0, 200) - Block(115, 200, 0, 200);
    list.clear();
    list.push_back({p, ColorType::black()});
    for (unsigned u = 0; u<7; u++)
        list.push_back({p.CreateRounded(u*3+3, false), colors[u%(sizeof(colors)/sizeof(ColorType))]});
    DrawPaths(9006, list, "curvy 2 circular CreateRounded - round");
}


void test_convexhull()
{
    if (shouldnt_do(9020, 9030)) return;
    using contour::ConvexHull;
    using contour::Path;

    std::list<std::pair<Path, ColorType>> list;
    Path tmp2;
    Path tmp = Contour(XY(50, 50), 40) +
                  Contour(XY(80, 50), 40) +
                  Contour(XY(110, 50), 40);

    XY p[] = {{10, 50},{50,10},{90,60}, {130,10}, {170,50}};
    tmp = Path(p);
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9020, list, "Convex Hull - left to right path, red:left, blue:right");

    tmp = Path(p).Invert();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9021, list, "Convex Hull - right to  left path, red:left, blue:right");


    tmp = Path(XY(50, 50), 40, 0, 0, 180, 360, true, false).append(
        Path(XY(130, 50), 40, 0, 0, 180, 360, true, false)).append(
            Path(XY(220, 50), 50, 0, 0, 180, 360, true, false));
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    tmp2 = tmp.CopyPart({1, 0.5}, {4, 0.5}).Shift({0, 100});
    list.push_back({tmp2, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp2.begin(), tmp2.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp2.begin(), tmp2.end()), ColorType::blue()});
    tmp2 = tmp.CopyPart({2, 0.5}, {3, 0.5}).Shift({0, 200});
    list.push_back({tmp2, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp2.begin(), tmp2.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp2.begin(), tmp2.end()), ColorType::blue()});
    DrawPaths(9022, list, "Convex Hull");


    tmp = Contour(XY(50, 50), 40) +
        Contour(XY(100, 50), 40) +
        Contour(XY(130, 50), 40) +
        Contour(XY(180, 50), 50) +
        Contour(XY(250, 90), 40);

    tmp.resize(tmp.size()/2);
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9023, list, "Convex Hull");

    {
        LineAttr line(ELineType::SOLID, ColorType::black(), 1, ECornerType::ROUND, 20);
        tmp = (line.CreateRectangle_Midline(50, 150, 10, 90) +
               Block(10, 190, 40, 60)) - Block(0, 200, 0, 50);
    }
    tmp.Cycle(XY(10, 50));
    tmp.erase(tmp.begin());
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9024, list, "Convex Hull");

    tmp = Path{XY(10,10), XY(10,50), XY(10,100),
               XY(50,100), XY(100,100),
               XY(100,200),
               XY(150,200), XY(200,200),
               XY(200,250) };
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9025, list, "Convex Hull");

    tmp = Path{XY(10,10), XY(50,50), XY(50,70), XY(100,70)};
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9026, list, "Convex Hull");

    tmp.Invert();
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9027, list, "Convex Hull");

    tmp = Path{
        XY(18.3477, 14),
        XY(53.6953, 20.5),
        XY(54.6157, 21.5),
        XY(70.1953, 21.5),
        XY(70.1953, 38.4279),
        XY(73.9431, 42.5),
        XY(83.1953, 42.5),
        XY(83.1953, 40.5),
        XY(100.195, 40.5),
        XY(100.195, 42.5),
        XY(112.195, 42.5),
        XY(112.195, 55.5),
        XY(100.195, 55.5),
        XY(100.195, 56.5),
        XY(112.195, 56.5),
        XY(112.195, 69.5),
        XY(100.195, 69.5),
        XY(100.195, 71.024),
    };
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9028, list, "Convex Hull");

    tmp.Invert();
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9029, list, "Convex Hull");

    tmp = Path{
        {{755.41, 189.213},{735.016, 240.713}},
        {{735.016, 240.713},{734.016, 273.713}},
        {{734.016, 273.713},{727.843, 292.713}},
        {{727.843, 292.713},{717.031, 292.713}},
        {{717.031, 292.713},{712.081, 294.763},{715.191, 292.713},{713.272, 293.573}},
        {{712.081, 294.763},{710.031, 299.713},{710.89, 295.954},{710.031, 297.874}},
        {{710.031, 299.713},{710.031, 347.54}},
        {{710.031, 347.54},{708.684, 351.684}},
        {{708.684, 351.684},{708.684, 351.684}},
        {{708.684, 351.684},{708.116, 352.999},{708.535, 352.142},{708.343, 352.585}},
        {{708.116, 352.999},{581.558, 584.164}}};
    list.clear();
    list.push_back({tmp, ColorType::black()});
    list.push_back({ConvexHull<Path>(true, tmp.begin(), tmp.end()), ColorType::red()});
    list.push_back({ConvexHull<Path>(false, tmp.begin(), tmp.end()), ColorType::blue()});
    DrawPaths(9030, list, "Convex Hull");
}

void contour_test_additonal()
{
    if (!shouldnt_do(4522)) {
        Path R = Contour(0, 100, 0, 100);
        Contour Tri(50, 150, 50, 150);
        R.Clip(Tri, {}, Edge::Update::clear_visible());

        DrawPaths(4522, {{R, ColorType::red()}, {Tri, ColorType::blue()}});
    }
    if (!shouldnt_do(4521)) {
        Path R = Contour(0, 100, 0, 100);
        Contour Tri(50, 150, 50, 150);
        R.Clip(Tri);

        DrawPaths(4521, {{R, ColorType::red()}, {Tri, ColorType::blue()}});
    }
    if (!shouldnt_do(4520)) {
        Path R = Contour(0, 100, 0, 100);
        Contour Tri(XY(50, -50), XY(0, 150), XY(100, 150));
        R.Clip(Tri, {}, Edge::Update::clear_visible());

        DrawPaths(4520, {{R, ColorType::red()}, {Tri, ColorType::blue()}});
    }
    if (!shouldnt_do(4519)) {
        Path R = Contour(0, 100, 0, 100);
        Contour Tri(XY(50, -50), XY(0, 150), XY(100, 150));
        R.Clip(Tri);

        DrawPaths(4519, {{R, ColorType::red()}, {Tri, ColorType::blue()}});
    }
    if (!shouldnt_do(4518)) {
        Contour A(Path{
            Edge(XY(0x1.f209200000000p+9,0x1.6380000000000p+9), XY(0x1.f209200000000p+9,0x1.6700000000000p+9)),
            Edge(XY(0x1.f209200000000p+9,0x1.6700000000000p+9), XY(0x1.d18005a00b8cfp+9,0x1.6d80000000000p+9)),
            Edge(XY(0x1.d18005a00b8cfp+9,0x1.6d80000000000p+9), XY(0x1.93cc8f6c02c6ap+9,0x1.6d80000000000p+9)),
            Edge(XY(0x1.93cc8f6c02c6ap+9,0x1.6d80000000000p+9), XY(0x1.a8bd931b1e9a8p+9,0x1.6380000000000p+9)),
            Edge(XY(0x1.a8bd931b1e9a8p+9,0x1.6380000000000p+9), XY(0x1.f209200000000p+9,0x1.6380000000000p+9)),
            Edge(XY(0x1.5740000000000p+9,0x1.8600000000000p+9), XY(0x1.5400000000000p+9,0x1.8940000000000p+9), XY(0x1.5740000000000p+9,0x1.87cb803bc2078p+9), XY(0x1.55cb803bc2078p+9,0x1.8940000000000p+9)),
            Edge(XY(0x1.5400000000000p+9,0x1.8940000000000p+9), XY(0x1.50c0000000000p+9,0x1.8600000000000p+9), XY(0x1.52347fc43df88p+9,0x1.8940000000000p+9), XY(0x1.50c0000000000p+9,0x1.87cb803bc2078p+9)),
            Edge(XY(0x1.50c0000000000p+9,0x1.8600000000000p+9), XY(0x1.5400000000000p+9,0x1.82c0000000000p+9), XY(0x1.50c0000000000p+9,0x1.84347fc43df88p+9), XY(0x1.52347fc43df88p+9,0x1.82c0000000000p+9)),
            Edge(XY(0x1.5400000000000p+9,0x1.82c0000000000p+9), XY(0x1.5740000000000p+9,0x1.8600000000000p+9), XY(0x1.55cb803bc2078p+9,0x1.82c0000000000p+9), XY(0x1.5740000000000p+9,0x1.84347fc43df88p+9)),
            Edge(XY(0x1.5740000000000p+9,0x1.8580000000000p+9), XY(0x1.db40000000000p+9,0x1.8580000000000p+9)),
            Edge(XY(0x1.db40000000000p+9,0x1.8580000000000p+9), XY(0x1.db40000000000p+9,0x1.8500000000000p+9)),
            Edge(XY(0x1.db40000000000p+9,0x1.8500000000000p+9), XY(0x1.8d6f000000000p+9,0x1.8500000000000p+9)),
            Edge(XY(0x1.8d6f000000000p+9,0x1.8500000000000p+9), XY(0x1.8d6f000000000p+9,0x1.7b80000000000p+9)),
            Edge(XY(0x1.8d6f000000000p+9,0x1.7b80000000000p+9), XY(0x1.db40000000000p+9,0x1.7b80000000000p+9)),
            Edge(XY(0x1.db40000000000p+9,0x1.7b80000000000p+9), XY(0x1.db40000000000p+9,0x1.8280000000000p+9)),
            Edge(XY(0x1.db40000000000p+9,0x1.8280000000000p+9), XY(0x1.e080000000000p+9,0x1.8600000000000p+9)),
            Edge(XY(0x1.e080000000000p+9,0x1.8600000000000p+9), XY(0x1.db40000000000p+9,0x1.8980000000000p+9)),
            Edge(XY(0x1.db40000000000p+9,0x1.8980000000000p+9), XY(0x1.db40000000000p+9,0x1.8680000000000p+9)),
            Edge(XY(0x1.db40000000000p+9,0x1.8680000000000p+9), XY(0x1.5740000000000p+9,0x1.8680000000000p+9)),
            Edge(XY(0x1.5740000000000p+9,0x1.8680000000000p+9), XY(0x1.5740000000000p+9,0x1.8580000000000p+9)),
            Edge(XY(0x1.9c80000000000p+8,0x1.8600000000000p+9), XY(0x1.9600000000000p+8,0x1.8940000000000p+9), XY(0x1.9c80000000000p+8,0x1.87cb803bc2078p+9), XY(0x1.99970077840f0p+8,0x1.8940000000000p+9)),
            Edge(XY(0x1.9600000000000p+8,0x1.8940000000000p+9), XY(0x1.8f80000000000p+8,0x1.8600000000000p+9), XY(0x1.9268ff887bf10p+8,0x1.8940000000000p+9), XY(0x1.8f80000000000p+8,0x1.87cb803bc2078p+9)),
            Edge(XY(0x1.8f80000000000p+8,0x1.8600000000000p+9), XY(0x1.9600000000000p+8,0x1.82c0000000000p+9), XY(0x1.8f80000000000p+8,0x1.84347fc43df88p+9), XY(0x1.9268ff887bf10p+8,0x1.82c0000000000p+9)),
            Edge(XY(0x1.9600000000000p+8,0x1.82c0000000000p+9), XY(0x1.9c80000000000p+8,0x1.8600000000000p+9), XY(0x1.99970077840f0p+8,0x1.82c0000000000p+9), XY(0x1.9c80000000000p+8,0x1.84347fc43df88p+9)),
            Edge(XY(0x1.9c80000000000p+8,0x1.8580000000000p+9), XY(0x1.50c0000000000p+9,0x1.8580000000000p+9)),
            Edge(XY(0x1.50c0000000000p+9,0x1.8580000000000p+9), XY(0x1.50c0000000000p+9,0x1.8680000000000p+9)),
            Edge(XY(0x1.50c0000000000p+9,0x1.8680000000000p+9), XY(0x1.9c80000000000p+8,0x1.8680000000000p+9)),
            Edge(XY(0x1.9c80000000000p+8,0x1.8680000000000p+9), XY(0x1.9c80000000000p+8,0x1.8580000000000p+9)),
            Edge(XY(0x1.1b80000000000p+8,0x1.8600000000000p+9), XY(0x1.1500000000000p+8,0x1.8940000000000p+9), XY(0x1.1b80000000000p+8,0x1.87cb803bc2078p+9), XY(0x1.18970077840f0p+8,0x1.8940000000000p+9)),
            Edge(XY(0x1.1500000000000p+8,0x1.8940000000000p+9), XY(0x1.0e80000000000p+8,0x1.8600000000000p+9), XY(0x1.1168ff887bf10p+8,0x1.8940000000000p+9), XY(0x1.0e80000000000p+8,0x1.87cb803bc2078p+9)),
            Edge(XY(0x1.0e80000000000p+8,0x1.8600000000000p+9), XY(0x1.1500000000000p+8,0x1.82c0000000000p+9), XY(0x1.0e80000000000p+8,0x1.84347fc43df88p+9), XY(0x1.1168ff887bf10p+8,0x1.82c0000000000p+9)),
            Edge(XY(0x1.1500000000000p+8,0x1.82c0000000000p+9), XY(0x1.1b80000000000p+8,0x1.8600000000000p+9), XY(0x1.18970077840f0p+8,0x1.82c0000000000p+9), XY(0x1.1b80000000000p+8,0x1.84347fc43df88p+9)),
            Edge(XY(0x1.1b80000000000p+8,0x1.8580000000000p+9), XY(0x1.8f80000000000p+8,0x1.8580000000000p+9)),
            Edge(XY(0x1.8f80000000000p+8,0x1.8580000000000p+9), XY(0x1.8f80000000000p+8,0x1.8680000000000p+9)),
            Edge(XY(0x1.8f80000000000p+8,0x1.8680000000000p+9), XY(0x1.1b80000000000p+8,0x1.8680000000000p+9)),
            Edge(XY(0x1.1b80000000000p+8,0x1.8680000000000p+9), XY(0x1.1b80000000000p+8,0x1.8580000000000p+9)),
            Edge(XY(0x1.0e80000000000p+8,0x1.8680000000000p+9), XY(0x1.b400000000000p+5,0x1.8680000000000p+9)),
            Edge(XY(0x1.b400000000000p+5,0x1.8680000000000p+9), XY(0x1.b400000000000p+5,0x1.8580000000000p+9)),
            Edge(XY(0x1.b400000000000p+5,0x1.8580000000000p+9), XY(0x1.0e80000000000p+8,0x1.8580000000000p+9)),
            Edge(XY(0x1.0e80000000000p+8,0x1.8580000000000p+9), XY(0x1.0e80000000000p+8,0x1.8680000000000p+9)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour B(Path{
            Edge(XY(0x1.f209200000000p+9,0x1.6700000000000p+9), XY(0x1.e080000000000p+9,0x1.8600000000000p+9)),
            Edge(XY(0x1.e080000000000p+9,0x1.8600000000000p+9), XY(0x1.b400000000000p+5,0x1.8580000000000p+9)),
            Edge(XY(0x1.b400000000000p+5,0x1.8580000000000p+9), XY(0x1.f209200000000p+9,0x1.6700000000000p+9)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        //The resulting Walked shape got two of its edges combined (an upwards poing at index #8 and a
        //leftward pointing shorter one at index #9), which resulted in the combined (left-up pointing)
        //edge having a crosspoint with the next (curvy) edge. The tolerance for CheckAndCombine after
        //a Walk have been tightened in the commit adding this comment to fix this.
        Draw(4518, A, B, A+B, "A CheckAndCombine issue");
    }
    if (!shouldnt_do(4517)) {
        const Contour a(Path{Edge(XY(0, 10), XY(20, 10), XY(0, 0), XY(20, 0)), Edge(XY(20, 10), XY(0, 10), XY(20, 20), XY(0, 20))});
        Contour b = a, c = a;
        const Contour d = std::move(b) + std::move(c);
        Draw(4517, a, d, "Critical during shape forming", 0);
    }
    if (!shouldnt_do(4516)) {
        Contour a(Path{
            Edge(XY(0x1.417cca28282e2p+9,0x1.69892f11eaa46p+7), XY(0x1.40a9d3fd0e401p+9,0x1.74c353e47aaf8p+7), XY(0x1.4143f2212e823p+9,0x1.6d3f52ea02469p+7), XY(0x1.40fb7f4b9b771p+9,0x1.711aeebdfbb18p+7)),
            Edge(XY(0x1.40a9d3fd0e401p+9,0x1.74c353e47aaf8p+7), XY(0x1.3f02d4896476cp+9,0x1.72750c89462dcp+7), true, 1),
            Edge(XY(0x1.3f02d4896476cp+9,0x1.72750c89462dcp+7), XY(0x1.3fc9177e07a2ep+9,0x1.67e81f02cb4abp+7), XY(0x1.3f50ca8e245d5p+9,0x1.6ef72ba6ce9f3p+7), XY(0x1.3f92e1f751a95p+9,0x1.6b7238a14f19fp+7)),
            Edge(XY(0x1.3fc9177e07a2ep+9,0x1.67e81f02cb4abp+7), XY(0x1.417cca28282e2p+9,0x1.69892f11eaa46p+7), true, 1),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour b(Path{
            Edge(XY(0x1.3c6a1e8a7e3a8p+9,0x1.6ed577d56160fp+7), XY(0x1.3feab9b65da9fp+9,0x1.6812348dad3cdp+7), XY(0x1.3cea3105ade1ap+9,0x1.6919077efeea8p+7), XY(0x1.3e7b9da0c50c5p+9,0x1.6611eaa0eea03p+7)),
            Edge(XY(0x1.3feab9b65da9fp+9,0x1.6812348dad3cdp+7), XY(0x1.419b8a884ab3p+9,0x1.7614a13d2afa9p+7), XY(0x1.4159d5cbf6479p+9,0x1.6a127e7a6bd96p+7), XY(0x1.421b9d037a5a2p+9,0x1.705830e6c8841p+7)),
            Edge(XY(0x1.419b8a884ab3p+9,0x1.7614a13d2afa9p+7), XY(0x1.3c6a1e8a7e3a8p+9,0x1.6ed577d56160fp+7), false),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = a, subst = b;
        comb -= std::move(subst);
        a.Scale(10); b.Scale(10); comb.Scale(10);
        //This should result in a two-element contour, with no holes
        std::string txt = "Critical during arrowhead 13";
        const bool fail = comb.size()!=2
            || std::ranges::any_of(comb, [](const HoledSimpleContour& c) { return c.HasHoles(); });
        if (fail) txt.append(" - FAIL: result must be 2 simplecontours with no holes");
        Draw(4516, a, b, comb, txt.c_str(), 0);
    }
    if (!shouldnt_do(4515)) {
        Contour a(Path{
            Edge(XY(213.95390734419240175157029, 108.98681204058789262489881), XY(217.70616546393384282964689, 99.19969596078155404939025), XY(215.45942803126018816328724, 105.91018605423833776058018), XY(216.70626255859016850990884, 102.66831353126475789849792)),
            Edge(XY(217.70616546393384282964689, 99.19969596078155404939025), XY(217.74509872873133531356871, 99.06366378692192142807471), true, 1),
            Edge(XY(217.74509872873133531356871, 99.06366378692192142807471), XY(219.87817397164377553053782, 88.45439326379768374408741), XY(218.71142758503614800247306, 95.68732697905285533579445), XY(219.42259602099375115358271, 92.17178033647182644472196)),
            Edge(XY(219.87817397164377553053782, 88.45439326379768374408741), XY(219.99285233184400567552075, 87.51865031768819847002305), true, 1),
            Edge(XY(219.99285233184400567552075, 87.51865031768819847002305), XY(221.27475487839828360847605, 87.62622560800905091582536)),
            Edge(XY(221.27475487839828360847605, 87.62622560800905091582536), XY(226.50106062387104088884371, 93.99413978192490048968466), XY(224.25081083404970172523463, 88.22143679913934022351896), XY(226.50106062387106931055314, 91.07973336939343766971433)),
            Edge(XY(226.50106062387104088884371, 93.99413978192490048968466), XY(226.37377439199104856015765, 95.27475487839824097591190), XY(226.50106062387104088884371, 94.39991734153721836264594), XY(226.45650755376303209231992, 94.86108906953822383911756)),
            Edge(XY(226.37377439199104856015765, 95.27475487839824097591190), XY(221.75963308016852693071996, 100.22963099241617612733535), XY(225.89977958966372284521640, 97.64472889003472744207102), XY(223.99060628247929116696469, 99.55441915917475625974475)),
            Edge(XY(221.75963308016852693071996, 100.22963099241617612733535), XY(223.46965323378080370275711, 104.56300891028772070967534), XY(222.83278056941398403978383, 101.43894535882699869944190), XY(223.46965323378077528104768, 103.02015271369374715959566)),
            Edge(XY(223.46965323378080370275711, 104.56300891028772070967534), XY(223.03882211864390683331294, 106.88508074960903115879773), XY(223.46965323378080370275711, 105.30404716340677850894281), XY(223.32046722369543090280786, 106.14912020493909494689433)),
            Edge(XY(223.03882211864390683331294, 106.88508074960903115879773), XY(217.61237159204264912659710, 111.02252118338812181264075), XY(222.16548253393710865566391, 109.16718494710660536384239), XY(219.90949626148031370576064, 110.76921492187692308561964)),
            Edge(XY(217.61237159204264912659710, 111.02252118338812181264075), XY(218.60387186845082396757789, 114.43140007745014941065165), XY(218.25133814736847170934197, 112.06980312196428428705985), XY(218.60399257789833882270614, 113.27543312105808581691235)),
            Edge(XY(218.60387186845082396757789, 114.43140007745014941065165), XY(218.59337229639749011766980, 114.78611378202512582902273)),
            Edge(XY(218.59337229639749011766980, 114.78611378202512582902273), XY(217.62641160279446239655954, 117.83857344599088889935956), XY(218.53343702880641785668558, 115.80093451146912286731094), XY(218.19492176688498830117169, 116.92239414505078798356408)),
            Edge(XY(217.62641160279446239655954, 117.83857344599088889935956), XY(212.08331766262801920674974, 120.91191421937836025790602), XY(216.43883017648337840910244, 119.75241349616442221304169), XY(214.17942690410714590143471, 120.91213310123053759070899)),
            Edge(XY(212.08331766262801920674974, 120.91191421937836025790602), XY(211.72860395805295752325037, 120.90141464732499798628851)),
            Edge(XY(211.72860395805295752325037, 120.90141464732499798628851), XY(208.67614429408723708547768, 119.93445395372197026517824), XY(210.71378322860908838265459, 120.84147937973395414701372), XY(209.59232359502735221212788, 120.50296411781249616979039)),
            Edge(XY(208.67614429408723708547768, 119.93445395372197026517824), XY(207.58869859728272899701551, 119.08724533864986483422399)),
            Edge(XY(207.58869859728272899701551, 119.08724533864986483422399), XY(208.18331409998350522982946, 118.31607550180365251435433), true, 1),
            Edge(XY(208.18331409998350522982946, 118.31607550180365251435433), XY(213.86657162665719056349189, 109.16439128929017954305891), XY(210.35488443445120765318279, 115.49971848970896814989828), XY(212.23991057837423568344093, 112.47187252376717481183732)),
            Edge(XY(213.86657162665719056349189, 109.16439128929017954305891), XY(213.95390734419240175157029, 108.98681204058789262489881), true, 1),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour b(Path{
            Edge(XY(222.96238352777524482917215, 78.76613321225345032416953), XY(223.95988725997986534821393, 77.03264358025739966251422)),
            Edge(XY(223.95988725997986534821393, 77.03264358025739966251422), XY(229.44615778710536346807203, 67.49845060427908549627318)),
            Edge(XY(229.44615778710536346807203, 67.49845060427908549627318), XY(230.31290260310339590432704, 67.99720247038139575579407), true, 1),
            Edge(XY(230.31290260310339590432704, 67.99720247038139575579407), XY(233.57163351168031795168645, 73.65370851008142949467583), XY(232.32946452392957326082978, 69.15759478258094361535768), XY(233.57194122867315400071675, 71.48208319203850180656445)),
            Edge(XY(233.57163351168031795168645, 73.65370851008142949467583), XY(233.55689615589875529622077, 74.07355880391529012740648)),
            Edge(XY(233.55689615589875529622077, 74.07355880391529012740648), XY(232.70485677742553320968000, 76.87293090403365170004690), XY(233.49190323075697506283177, 75.00215971598339592674165), XY(233.19340851642937195720151, 76.02391215160749027290876)),
            Edge(XY(232.70485677742553320968000, 76.87293090403365170004690), XY(227.28719311312278250625241, 80.12325030082352839144733), XY(231.58685268489665531888022, 78.81582940191150044029200), XY(229.38824617653062887256965, 80.04016320694927344447933)),
            Edge(XY(227.28719311312278250625241, 80.12325030082352839144733), XY(228.08536298455481983182835, 83.18790148605975787177158), XY(227.80348746290758299437584, 81.08366583283267914339376), XY(228.08550960956426933989860, 82.15313710787344803065935)),
            Edge(XY(228.08536298455481983182835, 83.18790148605975787177158), XY(228.07062562877325717636268, 83.60775177989359008279280)),
            Edge(XY(228.07062562877325717636268, 83.60775177989359008279280), XY(227.21858625030003508982190, 86.40712388001196586628794), XY(228.00563270363147694297368, 84.53635269196171009298268), XY(227.70713798930387383734342, 85.55810512758580443914980)),
            Edge(XY(227.21858625030003508982190, 86.40712388001196586628794), XY(221.56633101854794176688301, 89.66434174507345744586928), XY(226.05907395617941801901907, 88.42215648103055514184234), XY(223.73673038457891948382894, 89.66465618741166565541789)),
            Edge(XY(221.56633101854794176688301, 89.66434174507345744586928), XY(221.12740016712800183995569, 89.64799745874012160129496)),
            Edge(XY(221.12740016712800183995569, 89.64799745874012160129496), XY(219.75196985525911941294908, 89.47804264867275492179033)),
            Edge(XY(219.75196985525911941294908, 89.47804264867275492179033), XY(219.87316758417904338784865, 88.49495932493096006510314), true, 1),
            Edge(XY(219.87316758417904338784865, 88.49495932493096006510314), XY(220.31829956330471986802877, 83.57276962046010737594770), XY(220.07232702201440588396508, 86.87949735343576662671694), XY(220.22011118978835497728141, 85.24898542548939417429210)),
            Edge(XY(220.31829956330471986802877, 83.57276962046010737594770), XY(220.33210414939046017934743, 83.33710561513930770161096), true, 1),
            Edge(XY(220.33210414939046017934743, 83.33710561513930770161096), XY(222.96238352777524482917215, 78.76613321225345032416953)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = a+b;
        Draw(4515, a, b, comb, "Critical during arrowhead 12", 0);
    }
    if (!shouldnt_do(4514)) {
        Contour a(Path{
            Edge(XY(191.47493426788463466436951, 103.95140417331961657509964), XY(190.46673917653691887608147, 100.56026025835885207015963), XY(190.83148892346309821732575, 102.91861636071934071878786), XY(190.46673917653691887608147, 101.72723597695701869270124)),
            Edge(XY(190.46673917653691887608147, 100.56026025835885207015963), XY(190.89751593734510493050038, 98.23871827860483563199523), XY(190.46673917653683361095318, 99.81941908481439895695075), XY(190.61593534241089287206705, 98.97451025332544816137670)),
            Edge(XY(190.89751593734510493050038, 98.23871827860483563199523), XY(195.22522052831669725492247, 94.32778353743536570163997), XY(191.63033272055534439459734, 96.32381108071091091460403), XY(193.33719692710644721955759, 94.88722778862270956778957)),
            Edge(XY(195.22522052831669725492247, 94.32778353743536570163997), XY(193.49894537375698178038874, 90.00575614020681314286776), XY(194.15420276447460423696612, 93.13657360831635401154927), XY(193.49894537375698178038874, 91.57784514149932419968536)),
            Edge(XY(193.49894537375698178038874, 90.00575614020681314286776), XY(193.62622560800903670497064, 88.72524512160178744579753), XY(193.49894537375698178038874, 89.60001617331775491948065), XY(193.54349953917051152529893, 89.13887546579441334415606)),
            Edge(XY(193.62622560800903670497064, 88.72524512160178744579753), XY(199.98391560478356154817448, 83.50387470215636653847469), XY(194.22063325630111307873449, 85.75320688014143399868772), XY(197.07301689241137410135707, 83.50387470215636653847469)),
            Edge(XY(199.98391560478356154817448, 83.50387470215636653847469), XY(201.03188963829205704314518, 83.58910517391220196259383), XY(200.31638804639752038383449, 83.50387470215636653847469), XY(200.69345445081967227451969, 83.53384697364754174486734)),
            Edge(XY(201.03188963829205704314518, 83.58910517391220196259383), XY(201.98022491077807671899791, 83.74394516202470128973800), true, 1),
            Edge(XY(201.98022491077807671899791, 83.74394516202470128973800), XY(201.86331926463225272527779, 84.69769999470399568508583), true, 1),
            Edge(XY(201.86331926463225272527779, 84.69769999470399568508583), XY(199.92121468043126242264407, 94.70441801984952689963393), XY(201.43884854491508917817555, 88.16067217447499615445849), XY(200.78372251789278379874304, 91.51961339200745726429886)),
            Edge(XY(199.92121468043126242264407, 94.70441801984952689963393), XY(199.92760849622308683137817, 94.70656712215129857668217), true, 1),
            Edge(XY(199.92760849622308683137817, 94.70656712215129857668217), XY(199.66483544085264156819903, 95.62510629804067718851002), true, 1),
            Edge(XY(199.66483544085264156819903, 95.62510629804067718851002), XY(199.62699873037175279932853, 95.75684432729097750325309), XY(199.65226649136425862707256, 95.66904182441558646132762), XY(199.63965422624019652175775, 95.71295456034897597419331)),
            Edge(XY(199.62699873037175279932853, 95.75684432729097750325309), XY(199.40447653812637440751132, 96.53415617366509593466617), true, 1),
            Edge(XY(199.40447653812637440751132, 96.53415617366509593466617), XY(199.39808623335053994196642, 96.53262646607842611956585), true, 1),
            Edge(XY(199.39808623335053994196642, 96.53262646607842611956585), XY(196.07506093438496463932097, 105.19181147660772523977357), XY(198.48609652801522429399483, 99.55412795906775613730133), XY(197.36901137325807553679624, 102.46031489796105518053082)),
            Edge(XY(196.07506093438496463932097, 105.19181147660772523977357), XY(196.08108086977784978444106, 105.19516785507143197264668), true, 1),
            Edge(XY(196.08108086977784978444106, 105.19516785507143197264668), XY(195.72662870031467718945350, 105.91441075877035871144471), true, 1),
            Edge(XY(195.72662870031467718945350, 105.91441075877035871144471), XY(195.66567142124839051575691, 106.03824211594233872801851), XY(195.70635040700756235310109, 105.95573009243010176305688), XY(195.68603128026140325346205, 105.99700728241842284660379)),
            Edge(XY(195.66567142124839051575691, 106.03824211594233872801851), XY(195.24439987315335542916728, 106.89144361124485271830054), true, 1),
            Edge(XY(195.24439987315335542916728, 106.89144361124485271830054), XY(195.23796872782941136392765, 106.88865621603376609982661), true, 1),
            Edge(XY(195.23796872782941136392765, 106.88865621603376609982661), XY(189.76758308393883112330514, 115.53676846650526499615808), XY(193.65610447445638442331983, 109.97096600966357016204711), XY(191.81307454098566722677788, 112.88015190352498962056416)),
            Edge(XY(189.76758308393883112330514, 115.53676846650526499615808), XY(189.18918504404231839544082, 116.28797268566285083579714), true, 1),
            Edge(XY(189.18918504404231839544082, 116.28797268566285083579714), XY(188.40823727363269313173078, 115.75041092053395175298647), true, 1),
            Edge(XY(188.40823727363269313173078, 115.75041092053395175298647), XY(185.60305832170126905111829, 110.38953485138989663028042), XY(186.65841899979253071251151, 114.54593165113681152433855), XY(185.60283321464908112830017, 112.38862375278903016351251)),
            Edge(XY(185.60305832170126905111829, 110.38953485138989663028042), XY(185.61384293095068187540164, 110.03026708512030040765239)),
            Edge(XY(185.61384293095068187540164, 110.03026708512030040765239), XY(186.58026378635616993051372, 106.98418664501483021922468), XY(185.67450333317350441575400, 109.01741113471373978427437), XY(186.01295731841804581563338, 107.89842613631019219155860)),
            Edge(XY(186.58026378635616993051372, 106.98418664501483021922468), XY(191.47493426788463466436951, 103.95140417331961657509964), XY(187.64605715202782221240341, 105.26661344688248789225327), XY(189.57570071653719878668198, 104.15599136078145647843485)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour b(Path{
            Edge(XY(201.36856730662833570022485, 72.67303022389195632513292), XY(200.57039743519629837464890, 69.60837903865572684480867), XY(200.85227295684353521210141, 71.71261469188279136233177), XY(200.57025081018684886657866, 70.64314341684202247506619)),
            Edge(XY(200.57039743519629837464890, 69.60837903865572684480867), XY(200.58513479097786103011458, 69.18852874482186621207802)),
            Edge(XY(200.58513479097786103011458, 69.18852874482186621207802), XY(201.43717416945108311665535, 66.38915664470351885029231), XY(200.65012771611964126350358, 68.25992783275376041274285), XY(200.94862243044724436913384, 67.23817539712966606657574)),
            Edge(XY(201.43717416945108311665535, 66.38915664470351885029231), XY(207.09368020915110264468240, 63.13042573612662522464234), XY(202.59756648165060255450953, 64.37259472387736991549900), XY(204.92205489110824601084460, 63.13011801913376075390261)),
            Edge(XY(207.09368020915110264468240, 63.13042573612662522464234), XY(207.51353050298493485570361, 63.14516309190817366925330)),
            Edge(XY(207.51353050298493485570361, 63.14516309190817366925330), XY(210.31290260310333906090818, 63.99720247038139575579407), XY(208.44213141505309749845765, 63.21015601704993258636023), XY(209.46388385067714921206061, 63.50865073137752858656313)),
            Edge(XY(210.31290260310333906090818, 63.99720247038139575579407), XY(211.17964741910137149716320, 64.49595433648372022616968), true, 1),
            Edge(XY(211.17964741910137149716320, 64.49595433648372022616968), XY(205.69337689197587337730511, 74.03014731246203439241071)),
            Edge(XY(205.69337689197587337730511, 74.03014731246203439241071), XY(204.69587315977125285826332, 75.76363694445807084321132)),
            Edge(XY(204.69587315977125285826332, 75.76363694445807084321132), XY(202.30068644520025600286317, 79.92605880049156041877723)),
            Edge(XY(202.30068644520025600286317, 79.92605880049156041877723), XY(201.85813990528478711894422, 84.73967320774940503724793), XY(202.20054704371941056706419, 81.55402585218602951044886), XY(202.05187735543168514595891, 83.16819115011490737288113)),
            Edge(XY(201.85813990528478711894422, 84.73967320774940503724793), XY(201.73517088827873067202745, 85.73712415862868851945677), true, 1),
            Edge(XY(201.73517088827873067202745, 85.73712415862868851945677), XY(200.73834595399662816816999, 85.60917952627984561786434), true, 1),
            Edge(XY(200.73834595399662816816999, 85.60917952627984561786434), XY(198.34285781664775072385964, 84.79907805433407474993146), XY(199.95073929674265400535660, 85.50808851146646816232533), XY(199.07553671579432830185397, 85.22068423121366720351944)),
            Edge(XY(198.34285781664775072385964, 84.79907805433407474993146), XY(195.08412690807082867650024, 79.14257201463402680019499), XY(196.32629589582157336735690, 83.63868574213452689036785), XY(195.08381919107799262746994, 81.31419733267696869916108)),
            Edge(XY(195.08412690807082867650024, 79.14257201463402680019499), XY(195.09886426385241975367535, 78.72272172080019458917377)),
            Edge(XY(195.09886426385241975367535, 78.72272172080019458917377), XY(195.95090364232561341850669, 75.92334962068181880567863), XY(195.16385718899417156535492, 77.79412080873207457898388), XY(195.46235190332177467098518, 76.77236837310798023281677)),
            Edge(XY(195.95090364232561341850669, 75.92334962068181880567863), XY(201.36856730662833570022485, 72.67303022389195632513292), XY(197.06890773485449130930647, 73.98045112280398427628825), XY(199.26751424322051775561704, 72.75611731776622548295563)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = a+b;
        Draw(4514, a, b, comb, "Critical during arrowhead 11", 0);
    }
    if (!shouldnt_do(4513)) {
        Contour a(Path{
            Edge(XY(135.82603773420299830831937, 91.33665367862754180805496), XY(135.03604422416435681952862, 89.40753644548732381736045)),
            Edge(XY(135.03604422416435681952862, 89.40753644548732381736045), XY(134.17876980628039973453269, 87.51805638398256803611730)),
            Edge(XY(134.17876980628039973453269, 87.51805638398256803611730), XY(133.25707213379362769956060, 85.66991437582548485352163)),
            Edge(XY(133.25707213379362769956060, 85.66991437582548485352163), XY(132.27155260007558013057860, 83.86374528557797702887910)),
            Edge(XY(132.27155260007558013057860, 83.86374528557797702887910), XY(134.09448917945476864588272, 83.01254439022505948742037)),
            Edge(XY(134.09448917945476864588272, 83.01254439022505948742037), XY(135.92112025914195783116156, 82.07771341871250569965923)),
            Edge(XY(135.92112025914195783116156, 82.07771341871250569965923), XY(137.75032791367408435689867, 81.05906266373548874071275)),
            Edge(XY(137.75032791367408435689867, 81.05906266373548874071275), XY(139.57934025812105005570629, 79.95661625817166395790991)),
            Edge(XY(139.57934025812105005570629, 79.95661625817166395790991), XY(129.87135419163217875393457, 101.73077860354976564849494), XY(137.67548688240216847589181, 88.22658679705169504359219), XY(134.37017336087126295751659, 95.54605680402723066890758)),
            Edge(XY(129.87135419163217875393457, 101.73077860354976564849494), XY(129.50256336295973369487911, 99.67076356724467700587411)),
            Edge(XY(129.50256336295973369487911, 99.67076356724467700587411), XY(129.05257335743647217896068, 97.64710204023833739483962)),
            Edge(XY(129.05257335743647217896068, 97.64710204023833739483962), XY(128.52419911645580441472703, 95.66167392614966047403868)),
            Edge(XY(128.52419911645580441472703, 95.66167392614966047403868), XY(127.91816974213769242396666, 93.71540307162611327385093)),
            Edge(XY(127.91816974213769242396666, 93.71540307162611327385093), XY(129.87256452597119960046257, 93.25684476952001489280519)),
            Edge(XY(129.87256452597119960046257, 93.25684476952001489280519), XY(131.84337554117391277941351, 92.70783034939888977987721)),
            Edge(XY(131.84337554117391277941351, 92.70783034939888977987721), XY(133.82929771436243981952430, 92.06792151320679806758562)),
            Edge(XY(133.82929771436243981952430, 92.06792151320679806758562), XY(135.82603773420299830831937, 91.33665367862754180805496)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour b(Path{
            Edge(XY(129.87160546551092465961119, 101.73043316356617538076534), XY(126.07584852919728746201145, 106.37394512222756759456388), XY(128.68775556132783322027535, 103.35795015973324950664392), XY(127.42124249066833385768405, 104.90690870725094896442897)),
            Edge(XY(126.07584852919728746201145, 106.37394512222756759456388), XY(121.77519220463830151857110, 110.55330868478009165301046), XY(124.72100209006165982827952, 107.85128865121158980855398), XY(123.28616246264557787526428, 109.24555486571206586177141)),
            Edge(XY(121.77519220463830151857110, 110.55330868478009165301046), XY(121.88118239731750236387597, 108.45203710377893457916798)),
            Edge(XY(121.88118239731750236387597, 108.45203710377893457916798), XY(121.89494919816678475399385, 106.37743448286283864945290)),
            Edge(XY(121.89494919816678475399385, 106.37743448286283864945290), XY(121.81892848556478270438674, 104.33254540418924705136305)),
            Edge(XY(121.81892848556478270438674, 104.33254540418924705136305), XY(121.65384054917376488447189, 102.31859736591761134150147)),
            Edge(XY(121.65384054917376488447189, 102.31859736591761134150147), XY(123.66426501428097139978490, 102.30895985085898303168506)),
            Edge(XY(123.66426501428097139978490, 102.30895985085898303168506), XY(125.70525812905287921239506, 102.20840078069518597203569)),
            Edge(XY(125.70525812905287921239506, 102.20840078069518597203569), XY(127.77550791291618281775300, 102.01620576079116631262877)),
            Edge(XY(127.77550791291618281775300, 102.01620576079116631262877), XY(129.87160546551092465961119, 101.73043316356617538076534)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = a+b;
        Draw(4513, a, b, comb, "Critical during arrowhead 10", 0);
    }
    if (!shouldnt_do(4512)) {
        Contour a(Path{
            Edge(XY(158.59854608711981427404680, 83.72038903900184436679410), XY(159.06400889078847171731468, 81.77530693831793939807540), true, 1),
            Edge(XY(159.06400889078847171731468, 81.77530693831793939807540), XY(159.42326006534145221849030, 81.72194425847217758018814), true, 1),
            Edge(XY(159.42326006534145221849030, 81.72194425847217758018814), XY(161.51663425844625976424140, 85.29634963422974180957681)),
            Edge(XY(161.51663425844625976424140, 85.29634963422974180957681), XY(162.55028162318032514122024, 87.23894547293778600760561)),
            Edge(XY(162.55028162318032514122024, 87.23894547293778600760561), XY(163.52112633190193946575164, 89.25551818025246575416531)),
            Edge(XY(163.52112633190193946575164, 89.25551818025246575416531), XY(164.79441551222680573118851, 92.18911686371399127892801)),
            Edge(XY(164.79441551222680573118851, 92.18911686371399127892801), XY(161.97823612028039974575222, 93.62649549271125692939677)),
            Edge(XY(161.97823612028039974575222, 93.62649549271125692939677), XY(159.95991329608889941482630, 94.56017783935092779756815)),
            Edge(XY(159.95991329608889941482630, 94.56017783935092779756815), XY(157.92137217522846981410112, 95.40816718860006062641332)),
            Edge(XY(157.92137217522846981410112, 95.40816718860006062641332), XY(154.00893673010656925725925, 96.86114505363107696211955)),
            Edge(XY(154.00893673010656925725925, 96.86114505363107696211955), XY(153.76657118125450551815447, 96.59499354699896400688885), true, 1),
            Edge(XY(153.76657118125450551815447, 96.59499354699896400688885), XY(154.63482760920845748842112, 94.79329135870128197893791), true, 1),
            Edge(XY(154.63482760920845748842112, 94.79329135870128197893791), XY(156.91458300624768185116409, 89.36076173008217438109568), XY(155.50243462998221843918145, 92.99293674273360466031590), XY(156.25564310044998705961916, 91.19665392579885576651577)),
            Edge(XY(156.91458300624768185116409, 89.36076173008217438109568), XY(158.59854608711981427404680, 83.72038903900184436679410), XY(157.58728187678147492079006, 87.48653527086435133242048), XY(158.14340357621099997231795, 85.62234456423703932159697)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour b(Path{
            Edge(XY(154.63468447267828764779551, 94.79358837640337753782660), XY(155.50295112273718700635072, 92.99189111426538545401854), true, 1),
            Edge(XY(155.50295112273718700635072, 92.99189111426538545401854), XY(155.86088837018951380741782, 93.01545048148749117444822), true, 1),
            Edge(XY(155.86088837018951380741782, 93.01545048148749117444822), XY(157.15073204367354264832102, 96.93884939705650083396904)),
            Edge(XY(157.15073204367354264832102, 96.93884939705650083396904), XY(157.75768372209637391279102, 99.04185805269314357701660)),
            Edge(XY(157.75768372209637391279102, 99.04185805269314357701660), XY(158.29832216273968015229912, 101.20427617743342807443696)),
            Edge(XY(158.29832216273968015229912, 101.20427617743342807443696), XY(158.96555792062187606461521, 104.33357257649670657428942)),
            Edge(XY(158.96555792062187606461521, 104.33357257649670657428942), XY(155.92939211313284886273323, 105.15963235337079595410614)),
            Edge(XY(155.92939211313284886273323, 105.15963235337079595410614), XY(153.78012493260075643775053, 105.66610400670656133570446)),
            Edge(XY(153.78012493260075643775053, 105.66610400670656133570446), XY(151.62273306347776724578580, 106.09676455401400119171740)),
            Edge(XY(151.62273306347776724578580, 106.09676455401400119171740), XY(147.50242945953837647721230, 106.77525819118130812057643)),
            Edge(XY(147.50242945953837647721230, 106.77525819118130812057643), XY(147.31806110736707182695682, 106.47290988494285102206049), true, 1),
            Edge(XY(147.31806110736707182695682, 106.47290988494285102206049), XY(148.51057673995924801602087, 104.86732386543991424332489), true, 1),
            Edge(XY(148.51057673995924801602087, 104.86732386543991424332489), XY(154.63468447267828764779551, 94.79358837640337753782660), XY(150.94425762030132887048239, 101.59065067926535164133384), XY(152.96937670116540175513364, 98.24918632243574734275171)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = a+b;
        Draw(4512, a, b, comb, "Critical during arrowhead 9", 0);
    }
    if (!shouldnt_do(4511)) {
        Contour a(Path{
            Edge(XY(159.57108713746177386383351, 83.95312044083617308842804), XY(160.64341509854170908511151, 85.78410452446740919185686)),
            Edge(XY(160.64341509854170908511151, 85.78410452446740919185686), XY(161.65800679577398568653734, 87.69088789786496818123851)),
            Edge(XY(161.65800679577398568653734, 87.69088789786496818123851), XY(162.61160783565574661224673, 89.67164322713351509719359)),
            Edge(XY(162.61160783565574661224673, 89.67164322713351509719359), XY(163.50305789457797800423577, 91.72550244437645972084283)),
            Edge(XY(163.50305789457797800423577, 91.72550244437645972084283), XY(161.54084104193665893944853, 92.72701839094958131681778)),
            Edge(XY(161.54084104193665893944853, 92.72701839094958131681778), XY(159.55779551746496736086556, 93.64438135022925280281925)),
            Edge(XY(159.55779551746496736086556, 93.64438135022925280281925), XY(157.55512777838760030135745, 94.47744814267426249898563)),
            Edge(XY(157.55512777838760030135745, 94.47744814267426249898563), XY(155.53567870335729139696923, 95.22741957267825796407124)),
            Edge(XY(155.53567870335729139696923, 95.22741957267825796407124), XY(157.85579370458057724135870, 89.69858183212421920416091), XY(156.41458096340016936665052, 93.40362642328507547517802), XY(157.18799315233110291956109, 91.55916094014764894382097)),
            Edge(XY(157.85579370458057724135870, 89.69858183212421920416091), XY(159.57108713746177386383351, 83.95312044083617308842804), XY(158.53773204247391959142988, 87.79861301632919889925688), XY(159.10953967045202261942904, 85.88184108106966618834122)),
            Edge(XY(155.53553310374726947884483, 95.22772170143281300624949), XY(156.19502872065496035247634, 97.23375114623294734883530)),
            Edge(XY(156.19502872065496035247634, 97.23375114623294734883530), XY(156.79190903530977152513515, 99.30186383579653863762360)),
            Edge(XY(156.79190903530977152513515, 99.30186383579653863762360), XY(157.32394824853577119938564, 101.42988720828090265513310)),
            Edge(XY(157.32394824853577119938564, 101.42988720828090265513310), XY(157.79028553616021213201748, 103.61698161632631354223122)),
            Edge(XY(157.79028553616021213201748, 103.61698161632631354223122), XY(155.68337199242932911147363, 104.19021662461682353750803)),
            Edge(XY(155.68337199242932911147363, 104.19021662461682353750803), XY(153.56749962133091003124719, 104.68881884017402228437277)),
            Edge(XY(153.56749962133091003124719, 104.68881884017402228437277), XY(151.44356121005648674326949, 105.11280137748588003887562)),
            Edge(XY(151.44356121005648674326949, 105.11280137748588003887562), XY(149.31336974971071640538867, 105.46358168173600233785692)),
            Edge(XY(149.31336974971071640538867, 105.46358168173600233785692), XY(152.66680382307595209567808, 100.49449300374641325106495), XY(150.52506699252350585993554, 103.83216981613090013070178), XY(151.64291246783133715325675, 102.17452751648927744554385)),
            Edge(XY(152.66680382307595209567808, 100.49449300374641325106495), XY(155.53553310374726947884483, 95.22772170143281300624949), XY(153.72307490049718126101652, 98.76132878100931122844486), XY(154.67935545182984924394987, 97.00433368032055625462817)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour e(Path{
            Edge(XY(149.31124726705823491101910, 105.46643920601935917602532), XY(149.58498180592101789443404, 107.55334415244156787139218)),
            Edge(XY(149.58498180592101789443404, 107.55334415244156787139218), XY(149.79651354805596952246560, 109.68825642172842549371126)),
            Edge(XY(149.79651354805596952246560, 109.68825642172842549371126), XY(149.94452226918144788214704, 111.86918046865471865203290)),
            Edge(XY(149.94452226918144788214704, 111.86918046865471865203290), XY(150.02840737141465865533974, 114.09540224068287272984890)),
            Edge(XY(150.02840737141465865533974, 114.09540224068287272984890), XY(147.86898983068701340926054, 114.28086450442501131874451)),
            Edge(XY(147.86898983068701340926054, 114.28086450442501131874451), XY(145.70847924356917246768717, 114.40254876793588323380391)),
            Edge(XY(145.70847924356917246768717, 114.40254876793588323380391), XY(143.54749072809642029824317, 114.46051784787943006449495)),
            Edge(XY(143.54749072809642029824317, 114.46051784787943006449495), XY(141.38705640843383548599377, 114.45602812999210584621324)),
            Edge(XY(141.38705640843383548599377, 114.45602812999210584621324), XY(145.53184567487443246136536, 110.12288000547673050277808), XY(142.85022003357360631525808, 113.03979460636665521633404), XY(144.23184417089237285836134, 111.59437257924884079329786)),
            Edge(XY(145.53184567487443246136536, 110.12288000547673050277808), XY(149.31124726705823491101910, 105.46643920601935917602532), XY(146.87937732527808520899271, 108.59758729587751702183596), XY(148.13920872361114788873238, 107.04428282753278267591668)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = e+a;
        Contour comb2 = comb.Expand(1, EXPAND_MITER, EXPAND_MITER, 2, 2);
        Draw(4511, e, a, comb2, "Critical during arrowhead 8", 0);
    }
    if (!shouldnt_do(4510)) {
        Contour a(Path{
            Edge(XY(723.784733, 196.849219), XY(720.726598, 206.646210), XY(723.001134, 200.253018), XY(721.963646, 203.566222)),
            Edge(XY(720.726598, 206.646210), XY(717.478770, 205.341750), true, 1),
            Edge(XY(717.478770, 205.341750), XY(720.373948, 196.064013), XY(718.664622, 202.389231), XY(719.620672, 199.336098)),
            Edge(XY(720.373948, 196.064013), XY(723.784733, 196.849219), true, 1),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour e(Path{
            Edge(XY(709.823176, 202.266953), XY(722.829711, 196.714472), XY(711.881556, 197.142022), XY(717.704780, 194.656092)),
            Edge(XY(722.829711, 196.714472), XY(728.382192, 209.721007), XY(727.954642, 198.772852), XY(730.440572, 204.596076)),
            Edge(XY(728.382192, 209.721007), XY(715.375657, 215.273488), XY(726.323812, 214.845938), XY(720.500588, 217.331868)),
            Edge(XY(715.375657, 215.273488), XY(709.823176, 202.266953), XY(710.250726, 213.215108), XY(707.764796, 207.391884)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = e+a;
        Draw(4510, e, a, comb, "Critical during arrowhead 7", 0);
    }
    if (!shouldnt_do(4509)) {
        Path p{
            Edge(XY(813.875854, 225.440387), XY(815.538091, 220.669927)),
            Edge(XY(815.538091, 220.669927), XY(810.060000, 213.500000), XY(812.379453, 219.815850), XY(810.060000, 216.928794)),
            Edge(XY(810.060000, 213.500000), XY(817.500000, 206.060000), XY(810.060000, 209.391001), XY(813.391001, 206.060000)),
            Edge(XY(817.500000, 206.060000), XY(818.437786, 206.125439), XY(817.818376, 206.060000), XY(818.132082, 206.079998)),
            Edge(XY(818.437786, 206.125439), XY(818.709273, 201.225274)),
        };
        Path pp = p.DoubleWiden(1);
        pp.Scale(5); p.Scale(5);
        std::list<std::pair<Path, ColorType>> l{ {p,ColorType::red()}, {pp, ColorType::blue()} };
        DrawPaths(4509, l);
    }
    if (!shouldnt_do(4508)) {
        Contour a(Path{
            Edge(XY(1190.084099, 262.818046), XY(1188.226432, 261.522348)),
            Edge(XY(1188.226432, 261.522348), XY(1186.166713, 260.235874)),
            Edge(XY(1186.166713, 260.235874), XY(1184.058913, 259.055408)),
            Edge(XY(1184.058913, 259.055408), XY(1181.901456, 257.983953)),
            Edge(XY(1181.901456, 257.983953), XY(1191.575564, 252.748339)),
            Edge(XY(1191.575564, 252.748339), XY(1195.261994, 260.948466), XY(1193.034598, 255.444271), XY(1194.263279, 258.182476)),
            Edge(XY(1195.261994, 260.948466), XY(1197.626525, 269.621541), XY(1196.297021, 263.815024), XY(1197.085054, 266.711425)),
            Edge(XY(1197.626525, 269.621541), XY(1198.514499, 278.566677), XY(1198.179719, 272.594670), XY(1198.475558, 275.582114)),
            Edge(XY(1198.514499, 278.566677), XY(1197.855150, 287.531498), XY(1198.553601, 281.563661), XY(1198.333664, 284.557739)),
            Edge(XY(1197.855150, 287.531498), XY(1196.877948, 285.313931)),
            Edge(XY(1196.877948, 285.313931), XY(1195.810064, 283.192606)),
            Edge(XY(1195.810064, 283.192606), XY(1194.657819, 281.169354)),
            Edge(XY(1194.657819, 281.169354), XY(1193.424073, 279.298622)),
            Edge(XY(1193.424073, 279.298622), XY(1192.045709, 277.268143)),
            Edge(XY(1192.045709, 277.268143), XY(1190.817231, 275.789214)),
            Edge(XY(1190.817231, 275.789214), XY(1189.530697, 274.323932)),
            Edge(XY(1189.530697, 274.323932), XY(1188.196524, 272.938418)),
            Edge(XY(1188.196524, 272.938418), XY(1186.812130, 271.633720)),
            Edge(XY(1186.812130, 271.633720), XY(1192.197538, 270.631685)),
            Edge(XY(1192.197538, 270.631685), XY(1191.701146, 270.122495)),
            Edge(XY(1191.701146, 270.122495), XY(1190.084877, 268.618828)),
            Edge(XY(1190.084877, 268.618828), XY(1188.411268, 267.212499)),
            Edge(XY(1188.411268, 267.212499), XY(1186.688992, 265.900228)),
            Edge(XY(1186.688992, 265.900228), XY(1184.915763, 264.684174)),
            Edge(XY(1184.915763, 264.684174), XY(1190.084099, 262.818046)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = a.Expand(1, EXPAND_MITER, EXPAND_MITER, 2, 2);
        Draw(4508, a, comb, "Critical during arrowhead 7", 0);
    }
    if (!shouldnt_do(4507)) {
        Contour a(Path{
            Edge(XY(779.607768, 202.461161), XY(779.607768, 213.461161)),
            Edge(XY(779.607768, 213.461161), XY(774.107768, 213.461161)),
            Edge(XY(774.107768, 213.461161), XY(774.107768, 202.461161)),
            Edge(XY(774.107768, 202.461161), XY(779.607768, 202.461161)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour e(Path{
            Edge(XY(768.607768, 202.461161), XY(774.107768, 202.461161)),
            Edge(XY(774.107768, 202.461161), XY(774.107768, 213.461161)),
            Edge(XY(774.107768, 213.461161), XY(768.607768, 213.461161)),
            Edge(XY(768.607768, 213.461161), XY(774.107768, 207.961161), XY(771.645334, 213.461161), XY(774.107768, 210.998727)),
            Edge(XY(774.107768, 207.961161), XY(768.607768, 202.461161), XY(774.107768, 204.923595), XY(771.645334, 202.461161)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = e+a;
        Draw(4507, e, a, comb, "Critical during arrowhead 6", 0);
    }
    if (!shouldnt_do(4506)) {
        Contour e(Path{
            Edge(XY(1192.197538, 270.631685), XY(1191.701146, 270.122495)),
            Edge(XY(1191.701146, 270.122495), XY(1190.084877, 268.618828)),
            Edge(XY(1190.084877, 268.618828), XY(1188.411268, 267.212499)),
            Edge(XY(1188.411268, 267.212499), XY(1186.688992, 265.900228)),
            Edge(XY(1186.688992, 265.900228), XY(1184.915763, 264.684174)),
            Edge(XY(1184.915763, 264.684174), XY(1195.261994, 260.948466)),
            Edge(XY(1195.261994, 260.948466), XY(1197.626525, 269.621541), XY(1196.297021, 263.815024), XY(1197.085054, 266.711425)),
            Edge(XY(1197.626525, 269.621541), XY(1198.514499, 278.566677), XY(1198.179719, 272.594670), XY(1198.475558, 275.582114)),
            Edge(XY(1198.514499, 278.566677), XY(1197.855150, 287.531498), XY(1198.553601, 281.563661), XY(1198.333664, 284.557739)),
            Edge(XY(1197.855150, 287.531498), XY(1196.877948, 285.313931)),
            Edge(XY(1196.877948, 285.313931), XY(1195.810064, 283.192606)),
            Edge(XY(1195.810064, 283.192606), XY(1194.657819, 281.169354)),
            Edge(XY(1194.657819, 281.169354), XY(1193.424073, 279.298622)),
            Edge(XY(1193.424073, 279.298622), XY(1192.045709, 277.268143)),
            Edge(XY(1192.045709, 277.268143), XY(1190.817231, 275.789214)),
            Edge(XY(1190.817231, 275.789214), XY(1189.530697, 274.323932)),
            Edge(XY(1189.530697, 274.323932), XY(1188.196524, 272.938418)),
            Edge(XY(1188.196524, 272.938418), XY(1186.812130, 271.633720)),
            Edge(XY(1186.812130, 271.633720), XY(1192.197538, 270.631685)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);

        Contour a(Path{
            Edge(XY(1197.626568, 269.621773), XY(1195.880029, 267.777568)),
            Edge(XY(1195.880029, 267.777568), XY(1194.061639, 266.046298)),
            Edge(XY(1194.061639, 266.046298), XY(1192.177612, 264.426857)),
            Edge(XY(1192.177612, 264.426857), XY(1190.230094, 262.919876)),
            Edge(XY(1190.230094, 262.919876), XY(1188.226432, 261.522348)),
            Edge(XY(1188.226432, 261.522348), XY(1186.166713, 260.235874)),
            Edge(XY(1186.166713, 260.235874), XY(1184.058913, 259.055408)),
            Edge(XY(1184.058913, 259.055408), XY(1181.901456, 257.983953)),
            Edge(XY(1181.901456, 257.983953), XY(1191.575564, 252.748339)),
            Edge(XY(1191.575564, 252.748339), XY(1197.626568, 269.621773), XY(1194.546721, 258.238297), XY(1196.562635, 263.903558)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = e+a;
        Draw(4506, e, a, comb, "Critical during arrowhead 5", 0);
    }
    if (!shouldnt_do(4505)) {
        Contour e(Path{
            Edge(XY(120.355150, 72.031498), XY(119.082387, 70.347727)),
            Edge(XY(119.082387, 70.347727), XY(117.758960, 68.746994)),
            Edge(XY(117.758960, 68.746994), XY(116.388721, 67.230247)),
            Edge(XY(116.388721, 67.230247), XY(114.973086, 65.797938)),
            Edge(XY(114.973086, 65.797938), XY(116.168982, 64.797837)),
            Edge(XY(116.168982, 64.797837), XY(117.342408, 63.783769)),
            Edge(XY(117.342408, 63.783769), XY(119.477581, 61.623957)),
            Edge(XY(119.477581, 61.623957), XY(120.889051, 60.069647)),
            Edge(XY(120.889051, 60.069647), XY(122.468984, 61.508482)),
            Edge(XY(122.468984, 61.508482), XY(124.695444, 63.706768)),
            Edge(XY(124.695444, 63.706768), XY(125.842957, 65.013877)),
            Edge(XY(125.842957, 65.013877), XY(126.961122, 66.333658)),
            Edge(XY(126.961122, 66.333658), XY(125.368727, 67.890966)),
            Edge(XY(125.368727, 67.890966), XY(123.735566, 69.359969)),
            Edge(XY(123.735566, 69.359969), XY(122.063081, 70.740152)),
            Edge(XY(122.063081, 70.740152), XY(120.355150, 72.031498)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour a(Path{
            Edge(XY(120.889064, 60.069825), XY(119.275596, 58.728680)),
            Edge(XY(119.275596, 58.728680), XY(117.625500, 57.473318)),
            Edge(XY(117.625500, 57.473318), XY(115.942470, 56.303478)),
            Edge(XY(115.942470, 56.303478), XY(114.227813, 55.219360)),
            Edge(XY(114.227813, 55.219360), XY(115.425048, 53.607874)),
            Edge(XY(115.425048, 53.607874), XY(116.571771, 51.916331)),
            Edge(XY(116.571771, 51.916331), XY(117.666638, 50.145303)),
            Edge(XY(117.666638, 50.145303), XY(118.706255, 48.296396)),
            Edge(XY(118.706255, 48.296396), XY(120.580613, 49.350104)),
            Edge(XY(120.580613, 49.350104), XY(122.427153, 50.489036)),
            Edge(XY(122.427153, 50.489036), XY(124.242503, 51.713888)),
            Edge(XY(124.242503, 51.713888), XY(126.025347, 53.024322)),
            Edge(XY(126.025347, 53.024322), XY(124.820019, 54.909441)),
            Edge(XY(124.820019, 54.909441), XY(123.560910, 56.712642)),
            Edge(XY(123.560910, 56.712642), XY(122.249351, 58.433174)),
            Edge(XY(122.249351, 58.433174), XY(120.889064, 60.069825)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = e+a;
        Draw(4505, e, a, comb, "Critical during arrowhead 4", 0);
    }
    if (!shouldnt_do(4504)) {
        Contour cimbi(Path{
            Edge(XY(677.686554, 170.170739), XY(677.341978, 169.752435)),
            Edge(XY(677.341978, 169.752435), XY(675.734114, 167.995151)),
            Edge(XY(675.734114, 167.995151), XY(674.057143, 166.347258)),
            Edge(XY(674.057143, 166.347258), XY(679.934497, 165.140317)),
            Edge(XY(679.934497, 165.140317), XY(680.682346, 169.829521), XY(680.254733, 166.699746), XY(680.503994, 168.263644)),
            Edge(XY(680.682346, 169.829521), XY(681.005572, 174.566656), XY(680.862065, 171.407406), XY(680.969785, 172.987300)),
            Edge(XY(681.005572, 174.566656), XY(681.005573, 174.566720), XY(681.005572, 174.566677), XY(681.005573, 174.566699)),
            Edge(XY(681.005573, 174.566720), XY(680.896291, 179.313839), XY(681.041458, 176.150490), XY(681.005008, 177.733720)),
            Edge(XY(680.896291, 179.313839), XY(680.355150, 184.031498), XY(680.787824, 180.890338), XY(680.607421, 182.463742)),
            Edge(XY(680.355150, 184.031498), XY(679.338277, 181.809156)),
            Edge(XY(679.338277, 181.809156), XY(678.230480, 179.686233)),
            Edge(XY(678.230480, 179.686233), XY(677.038277, 177.664574)),
            Edge(XY(677.038277, 177.664574), XY(675.764212, 175.775037)),
            Edge(XY(675.764212, 175.775037), XY(675.005957, 174.634635)),
            Edge(XY(675.005957, 174.634635), XY(677.965294, 174.601104)),
            Edge(XY(677.965294, 174.601104), XY(677.338541, 173.680405)),
            Edge(XY(677.338541, 173.680405), XY(676.059684, 172.071162)),
            Edge(XY(676.059684, 172.071162), XY(674.720890, 170.508526)),
            Edge(XY(674.720890, 170.508526), XY(677.686554, 170.170739)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour cexp = cimbi.CreateExpand(1, EXPAND_MITER, EXPAND_MITER, 2, 2);
        Draw(4504, cimbi, cexp, "Critical during arrowhead 3 exp", 0);
    }
    if (!shouldnt_do(4503) || !shouldnt_do(4502)) {
        Contour ret(Path{
            Edge(XY(673.816421, 195.767746), XY(671.060785, 194.582318)),
            Edge(XY(671.060785, 194.582318), XY(672.181960, 193.873301)),
            Edge(XY(672.181960, 193.873301), XY(673.287719, 193.130140)),
            Edge(XY(673.287719, 193.130140), XY(674.378307, 192.353280)),
            Edge(XY(674.378307, 192.353280), XY(675.452237, 191.542022)),
            Edge(XY(675.452237, 191.542022), XY(676.509460, 190.696620)),
            Edge(XY(676.509460, 190.696620), XY(677.548701, 189.816648)),
            Edge(XY(677.548701, 189.816648), XY(678.569584, 188.902210)),
            Edge(XY(678.569584, 188.902210), XY(679.571087, 187.953120)),
            Edge(XY(679.571087, 187.953120), XY(678.264858, 192.517368), XY(679.205017, 189.482860), XY(678.769585, 191.005083)),
            Edge(XY(678.264858, 192.517368), XY(676.572434, 196.953336), XY(677.767880, 194.006430), XY(677.203719, 195.485857)),
            Edge(XY(676.572434, 196.953336), XY(674.520097, 201.236840), XY(675.952941, 198.393405), XY(675.268809, 199.821967)),
            Edge(XY(674.520097, 201.236840), XY(669.216846, 198.430502)),
            Edge(XY(669.216846, 198.430502), XY(670.397963, 197.807359)),
            Edge(XY(670.397963, 197.807359), XY(671.565620, 197.151160)),
            Edge(XY(671.565620, 197.151160), XY(672.720007, 196.462378)),
            Edge(XY(672.720007, 196.462378), XY(673.816421, 195.767746)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);

        Contour element(Path{
            Edge(XY(676.572770, 196.952556), XY(672.140299, 205.343292), XY(675.347861, 199.800084), XY(673.870221, 202.602626)),
            Edge(XY(672.140299, 205.343292), XY(667.066509, 202.140690)),
            Edge(XY(667.066509, 202.140690), XY(669.518766, 201.035637)),
            Edge(XY(669.518766, 201.035637), XY(671.922596, 199.802871)),
            Edge(XY(671.922596, 199.802871), XY(674.275851, 198.442795)),
            Edge(XY(674.275851, 198.442795), XY(676.572770, 196.952556)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour comb = ret+element;
        Draw(4502, ret, element, comb, "Critical during arrowhead 2", 0);
        Contour exp = comb.CreateExpand(1, EXPAND_MITER, EXPAND_MITER, 2, 2);
        Draw(4503, comb, exp, "Critical during arrowhead 2 exo", 0);
    }
    if (!shouldnt_do(4501) || !shouldnt_do(4500)) {
        Contour band(Path{
            Edge(XY(244.452010, 125.997362), XY(256.167193, 103.138561), XY(250.454638, 118.711733), XY(254.238430, 111.198503)),
            Edge(XY(256.167193, 103.138561), XY(259.571087, 103.953120), true, 1),
            Edge(XY(259.571087, 103.953120), XY(247.153275, 128.222933), XY(257.547552, 112.409094), XY(253.404645, 120.635396)),
            Edge(XY(247.153275, 128.222933), XY(244.452010, 125.997362), true, 1)
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Contour circle(Path{
            Edge(XY(243.500446, 132.325905), XY(242.908430, 124.725607), XY(241.297183, 130.374889), XY(241.022041, 127.015193)),
            Edge(XY(242.908430, 124.725607), XY(250.474064, 123.847007), XY(244.792808, 122.438462), XY(248.137950, 122.064102)),
            Edge(XY(250.474064, 123.847007), XY(243.500446, 132.325905), XY(248.444759, 126.759019), XY(246.120914, 129.592698)),
        }, ECloseType::IGNORE_OPEN_PATH, EForceClockwise::DONT);
        Draw(4500, band, circle, band+circle, "Critical during arrowhead 1", 0);
        band.Scale(10);
        circle.Scale(10);
        Draw(4501, band, circle, band+circle, "Critical during arrowhead 1, scaled by 10");
    }
}

/** A set of drawing operations drawing interesting test cases. */
void contour_test(int max_threads)
{
    generate_forms();

    class counter
    {
        const size_t maxval;
        std::list<std::future<void>> tasks;
    public:
        counter(int m = std::numeric_limits<int>::max()) : maxval(m) {}
        void Add(std::future<void> &&o)
        {
            tasks.push_back(std::move(o));
            WaitAndCheck(maxval);
        }
        void WaitAndCheck(size_t until_less_than = 1) {
            while (tasks.size() >= until_less_than) {
                using namespace std::chrono_literals;
                for (auto &f : tasks)
                    if (f.valid() && f.wait_for(200ms) == std::future_status::ready)
                        f.get(); //if an exception was thrown in a task, we re-throw here
                tasks.remove_if([](const auto& f) { return !f.valid(); });
            }
        }
    };
    counter tasks(max_threads);
    tasks.Add(std::async(std::launch::async, contour_unit_tests, true));
    tasks.Add(std::async(std::launch::async, contour_test_basic));
    tasks.Add(std::async(std::launch::async, contour_test_expand));
    tasks.Add(std::async(std::launch::async, contour_test_lohere));
    tasks.Add(std::async(std::launch::async, contour_test_expand_edge));
    tasks.Add(std::async(std::launch::async, contour_test_area));
    tasks.Add(std::async(std::launch::async, contour_test_additonal));
    tasks.Add(std::async(std::launch::async, contour_test_relations));
    tasks.Add(std::async(std::launch::async, contour_test_distance));
    tasks.Add(std::async(std::launch::async, contour_test_cut));
    tasks.Add(std::async(std::launch::async, test_arrowheads));
    tasks.Add(std::async(std::launch::async, contour_test_tangent));
    tasks.Add(std::async(std::launch::async, contour_test_bezier));
    tasks.Add(std::async(std::launch::async, contour_test_path_untangle));
    tasks.Add(std::async(std::launch::async, contour_test_path_linear));
    tasks.Add(std::async(std::launch::async, contour_test_expand2D));
    tasks.Add(std::async(std::launch::async, contour_test_transform));
    tasks.Add(std::async(std::launch::async, test_createrounded));
    tasks.Add(std::async(std::launch::async, test_convexhull));
    tasks.WaitAndCheck();
}
